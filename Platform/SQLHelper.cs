﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Diagnostics;


namespace Platform
{
    public class SQLHelper
    {
        public struct StateInfo
        {
            public string TableName;
            public MySqlDataAdapter da;
            public DataTable data;
        }
        private MySqlConnection m_conn;
        private ArrayList m_StateInfoList;

        private string _ConnString = "";

        public SQLHelper()
        {
            m_StateInfoList = new ArrayList();
        }

        public void SetConnectionString(string ConnString)
        {
            _ConnString = ConnString;
        }

        MySqlConnection _testconn;
        public void CheckConnection()
        {
            try
            {
                if (_testconn == null)
                    _testconn = new MySqlConnection(_ConnString);
                _testconn.Open();
                _testconn.Close();
            }
            catch (MySqlException ex)
            {
                Debug.WriteLine("Error connecting to the server: " + ex.Message);

               
                throw (ex);
            }

            //SET NAMES cp1251
        }

        public void OpenConnection()
        {
            if (m_conn != null)
                m_conn.Close();

//           string connStr = String.Format("server={0};user id={1}; password={2}; database={3}; pooling=false;CharSet=utf8",
//              _ServerIP, "root", "atanor", "linguaphone");
            try
            {
                m_conn = new MySqlConnection(_ConnString);
                m_conn.Open();
            }
            catch (MySqlException ex)
            {
                Debug.WriteLine("Error connecting to the server: " + ex.Message);

                string Err = "Ошибка соединения с сервером базы данных\n\r" + ex.Message;
                Err = Err + "\n\rПриложение будет закрыто.";
//              System.Windows.Forms.MessageBox.Show(Err, "Ошибка");

                throw (new Exception(Err));
//              System.Windows.Forms.Application.Exit();
            }

            //SET NAMES cp1251
        }

        public bool ReOpenConnection()
        {
            try
            {
                if (m_conn != null)
                    m_conn.Close();

                m_conn.Open();
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }


        public MySqlDataReader SelectFromTable(string TableName)
        {
/*
            DataTable data = new DataTable();

            MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM " + TableName, m_conn);
            MySqlCommandBuilder cb = new MySqlCommandBuilder(da);
*/
            try
            {
                MySqlCommand myCommand = new MySqlCommand("SELECT * FROM " + TableName);
                myCommand.Connection = m_conn;
                myCommand.CommandType = CommandType.Text;

                MySqlDataReader reader = myCommand.ExecuteReader();

                return reader;
//                myCommand.ExecuteNonQuery();

//              da.Fill(data);
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка извлечения данных из таблицы " + TableName + "\n\r" + Ex.Message;
                Err = Err + "\n\rПриложение будет закрыто";
                System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
//              throw (new Exception(Err));
                System.Windows.Forms.Application.Exit();
            }
                
            return null;
        }


        public void SelectFromTable(string tablename, ref DataTable datatable)
        {
//           DataTable data = new DataTable();

            MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM " + tablename, m_conn);
            MySqlCommandBuilder cb = new MySqlCommandBuilder(da);
            try
            {
                da.Fill(datatable);

                datatable.TableName = tablename;
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка извлечения данных из таблицы " + tablename + "\n\r" + Ex.Message;
                Err = Err + "\n\rПриложение будет закрыто";
                System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
//              throw (new Exception(Err));
                System.Windows.Forms.Application.Exit();
            }

//          adapter = (DbDataAdapter)da;
            return;
        }

        public DataTable SelectFromTable(string TableName, string FilterField, string FilterValue)
        {
            DataTable data = new DataTable();

            MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM " + TableName + " WHERE " + FilterField + " = '" + FilterValue + "'", m_conn);
            MySqlCommandBuilder cb = new MySqlCommandBuilder(da);
            try
            {

                da.Fill(data);
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка извлечения данных из таблицы " + TableName + "\n\r" + Ex.Message;
                Err = Err + "\n\rПриложение будет закрыто";
                System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
//              throw (new Exception(Err));
                System.Windows.Forms.Application.Exit();
            }

            return data;
        }

        public bool UpdateData(DataTable table)
        {
            MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM " + table.TableName, m_conn);
            MySqlCommandBuilder cb = new MySqlCommandBuilder(da);
            try
            {
                da.Update(table);
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка обновления данных в таблице " + table.TableName + "\n\r" + Ex.Message;
                Err = Err + "\n\rПриложение будет закрыто";
                System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
//              throw (new Exception(Err));
                System.Windows.Forms.Application.Exit();
            }
            return true;
        }


        public int InsertRow(string TableName, ArrayList fieldlist, ArrayList valuelist)
        {
            string myInsertQuery = "INSERT INTO " + TableName+ " (";
            bool bfirst = true;
            foreach (string fname in fieldlist)
            {
                if (!bfirst)
                    myInsertQuery = myInsertQuery + ", ";
                bfirst = false;

                myInsertQuery = myInsertQuery + fname;
            }
            myInsertQuery = myInsertQuery + ") Values (";

            bfirst = true;
            foreach (string value in valuelist)
            {
                if (!bfirst)
                    myInsertQuery = myInsertQuery + ", ";
                bfirst = false;

                myInsertQuery = myInsertQuery + value;
            }
            myInsertQuery = myInsertQuery + ")";

            MySqlCommand myCommand = new MySqlCommand(myInsertQuery);
            myCommand.Connection = m_conn;
            try
            {
                myCommand.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка вставки данных в таблице " + TableName + "\n\r" + Ex.Message;
                Err = Err + "\n\rПриложение будет закрыто";
                System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
//              throw (new Exception(Err));
                System.Windows.Forms.Application.Exit();
            }

            return 1;

        }

        public int InsertRowWithBLOB(string TableName, ArrayList fieldlist, ArrayList valuelist)
        {
            DataTable data = new DataTable();

            MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM " + TableName, m_conn);
            MySqlCommandBuilder cb = new MySqlCommandBuilder(da);

            try
            {
                da.Fill(data);

                DataRow dr = data.NewRow();

                for (int i = 0; i < fieldlist.Count; i++)
                {
                    dr[(string)fieldlist[i]] = valuelist[i];
                }
                data.Rows.Add(dr);
                da.Update(data);

                return (int)0;//dr["id"];
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка вставки данных в таблице " + TableName + "\n\r" + Ex.Message;
                Err = Err + "\n\rПриложение будет закрыто";
                System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
//              throw (new Exception(Err));
                System.Windows.Forms.Application.Exit();
            }
            return 0;
        }

        public void ExecuteSP(string ProcName, SqlParameter param1, ref DataTable result_table)
        {
            MySqlCommand myCommand = new MySqlCommand(ProcName);
            myCommand.Connection = m_conn;
            myCommand.CommandType = CommandType.StoredProcedure;

            if (param1 != null)
            {
                MySqlParameter p1 = new MySqlParameter();
                p1.ParameterName = param1.ParameterName;
                p1.DbType = param1.DbType;
                p1.Size = param1.Size;
                p1.Value = param1.Value;
                myCommand.Parameters.Add(p1);
            }
            try
            {
                MySqlDataReader reader = myCommand.ExecuteReader();

                result_table.Load(reader);

                reader.Close();
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка выполнения процедуры " + ProcName + "\n\r" + Ex.Message;
//              Err = Err + "\n\rПриложение будет закрыто";
//              System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
                throw (new Exception(Err));
//              System.Windows.Forms.Application.Exit();
            }
        }

        public MySqlDataReader ExecuteSP(string ProcName, SqlParameter param1)
        {
            MySqlCommand myCommand = new MySqlCommand(ProcName);
            myCommand.Connection = m_conn;
            myCommand.CommandType = CommandType.StoredProcedure;

            if (param1 != null)
            {
                MySqlParameter p1 = new MySqlParameter();
                p1.ParameterName = param1.ParameterName;
                p1.DbType = param1.DbType;
                p1.Size = param1.Size;
                p1.Value = param1.Value;
                myCommand.Parameters.Add(p1);
            }
            try
            {
                MySqlDataReader reader = myCommand.ExecuteReader();
                return reader;
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка выполнения процедуры " + ProcName + "\n\r" + Ex.Message;
                //              Err = Err + "\n\rПриложение будет закрыто";
                //              System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
                throw (new Exception(Err));
                //              System.Windows.Forms.Application.Exit();
            }
        }


        public void ExecuteSP(string ProcName, SqlParameter param1, SqlParameter param2)
        {
            MySqlCommand myCommand = new MySqlCommand(ProcName);
            myCommand.Connection = m_conn;
            myCommand.CommandType = CommandType.StoredProcedure;

            if (param1 != null)
            {
                MySqlParameter p1 = new MySqlParameter();
                p1.ParameterName = param1.ParameterName;
                p1.DbType = param1.DbType;
                p1.Size = param1.Size;
                p1.Value = param1.Value;
                myCommand.Parameters.Add(p1);
            }

            if (param2 != null)
            {
                MySqlParameter p2 = new MySqlParameter();
                p2.ParameterName = param2.ParameterName;
                p2.DbType = param2.DbType;
                p2.Size = param2.Size;
                p2.Value = param2.Value;
                myCommand.Parameters.Add(p2);
            }

            try
            {
                myCommand.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка выполнения процедуры " + ProcName + "\n\r" + Ex.Message;
                Err = Err + "\n\rПриложение будет закрыто";
                System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
//              throw (new Exception(Err));
                System.Windows.Forms.Application.Exit();
            }

        }

        public void ExecuteSP(string ProcName, SqlParameter[] sql_params, DataTable result_table, Dictionary<string, object> OutParams)
        {
            MySqlCommand myCommand = new MySqlCommand(ProcName);
            myCommand.Connection = m_conn;
            myCommand.CommandType = CommandType.StoredProcedure;
            try
            {

                foreach (SqlParameter sql_param in sql_params)
                {
                    MySqlParameter p = new MySqlParameter();
                    p.ParameterName = sql_param.ParameterName;
                    p.DbType = sql_param.DbType;
                    p.Size = sql_param.Size;
                    p.Value = sql_param.Value;
                    p.Direction = sql_param.Direction;

                    myCommand.Parameters.Add(p);
                }

                if (result_table != null)
                {
                    MySqlDataReader reader = myCommand.ExecuteReader();

                    result_table.Load(reader);

                    reader.Close();
                }
                else
                {
                    myCommand.ExecuteNonQuery();
                    if (OutParams != null)
                    {
                        foreach (MySqlParameter p in myCommand.Parameters)
                        {
                            if (p.Direction == ParameterDirection.Output || p.Direction == ParameterDirection.InputOutput)
                            {
                                OutParams[p.ParameterName] = p.Value;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                string Err = "";
                if (Ex.Message == "Connection must be valid and open.")
                    Err = "Отсутствует соединение с базой данных";
                else
                {
                    Err = "Ошибка выполнения процедуры " + ProcName + "\n\r";
                    Err = Err + Ex.Message ;
                }

//              Err = Err + "\n\rПриложение будет закрыто";
//              System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
                throw (new Exception(Err));
//              System.Windows.Forms.Application.Exit();
            }

        }

        public void ExecuteCmd(string CmdText)
        {
            try
            {
                MySqlCommand myCommand = new MySqlCommand(CmdText);
                myCommand.Connection = m_conn;
                myCommand.CommandType = CommandType.Text;
                myCommand.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                string Err = "Ошибка выполнения команды " + CmdText + "\n\r" + Ex.Message;
                Err = Err + "\n\rПриложение будет закрыто";
                System.Windows.Forms.MessageBox.Show(Err, "Ошибка");
//              throw (new Exception(Err));
                System.Windows.Forms.Application.Exit();
            }

        }

        public string GetCodeNameFromEnum(string tblname, int EnumID)
        {
            string s = "";
            MySqlDataReader dr = SelectFromTable(tblname);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if ((int)dr["id"] == EnumID)
                        {
                            s = (string)dr["CodeName"];
                            break;
                        }
                    }
                }

                dr.Close();
            }

            return s;
        }


        public int GetIdFromEnum(string tblname, string name)
        {
            int id = 0;
            MySqlDataReader dr = SelectFromTable(tblname);

            if (dr != null)
            {
                if (dr.HasRows )
                {
                    while (dr.Read())
                    {
                        if ((string)dr["CodeName"] == name)
                        {
                            id = (int)dr["id"];
                        }
                    }
                }

                dr.Close();
            }

            return id;
        }


    }

}
