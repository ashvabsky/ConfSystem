﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Threading;
using System.Text;

namespace Platform
{
    // дравйвер для взаимодействия с конференцсистемой через сокет на палантире
    //  по специальному протоколу команд
    public class ConfDriver
    {
        public class StateObject
        {
            // Client socket.
            public Socket workSocket = null;
            public WebSocket webSocket = null;

            // Size of receive buffer.
            public const int BufferSize = 10240;
            // Receive buffer.
            public byte[] buffer = new byte[BufferSize];
            // Received data string.
            public StringBuilder sb = new StringBuilder();
        }

        public void GetMicState()
        {
        }

        public delegate void OnReceiveDataDelegate(string data);

        private OnReceiveDataDelegate _OnReceiveData;
        private int _port = 10500;
        IPEndPoint _remoteEP;
        private string _initstring = "";

        // ManualResetEvent instances signal completion.
        private ManualResetEvent connectDone =
            new ManualResetEvent(false);
        private ManualResetEvent sendDone =
            new ManualResetEvent(false);
        private ManualResetEvent receiveDone =
            new ManualResetEvent(false);

        private ManualResetEvent connectStart =
            new ManualResetEvent(false);

        private Socket _client;
        public bool WorkOffline = false;
        public bool _firstInit = true; // признак самой первой инициализации и установки соединения

        //        public Queue _commandStack = new Queue();

        private Thread socketThread;
        private Thread connectThread;
        private Thread checkThread;
        

        public void Init(OnReceiveDataDelegate ProcessMethod, string serverIP, string sysname)
        {
                _OnReceiveData = ProcessMethod;

                IPAddress ipAddress = IPAddress.Parse(serverIP);
                _remoteEP = new IPEndPoint(ipAddress, _port);
                _initstring = "ms:name=" + sysname;

                // Create a TCP/IP socket.
//                _client = new Socket(AddressFamily.InterNetwork,
//                    SocketType.Stream, ProtocolType.Tcp);

                WorkOffline = false;
                _firstInit = true;

        }

        private void Connect2()
        {
            WorkOffline = false;

            if (connectThread != null) // && connectThread.ThreadState == ThreadState.Running)
                connectThread.Abort();

            _client = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);


            connectThread = new Thread(new ThreadStart(StartConnect));
            connectThread.Start();
            connectStart.Set();
            Thread.Sleep(1000);

            /*
                        if (_client.Connected == true)
                            return;

                        // Connect to a remote device.
                        try
                        {
                            // Connect to the remote endpoint.
                            _client.BeginConnect(_remoteEP,
                                new AsyncCallback(ConnectCallback), _client);

                            connectDone.WaitOne();
                            // Send init data to the remote device.
                            Send(_initstring);
                            sendDone.WaitOne();

                            if (socketThread.ThreadState == ThreadState.Running)
                                socketThread.Abort();

                            socketThread.Start();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.ToString());
                        }
             */
        }
        public void Connect()
        {
            socketThread = new Thread(new ThreadStart(RecieveData));
            checkThread = new Thread(new ThreadStart(CheckConnect));

            Connect2();

            socketThread.Start();
            checkThread.Start();

        }

        private void StartConnect()
        {

            // Connect to a remote device.
            try
            {
                while (true)
                {
                    connectStart.WaitOne();
                    if (_client.Connected == false && WorkOffline == false)
                    {

                        // Connect to the remote endpoint.
                        connectDone.Reset();
                        _client.BeginConnect(_remoteEP,
                            new AsyncCallback(ConnectCallback), _client);

                        if (connectDone.WaitOne(5000) == false)
                        {
                            if (_firstInit == true)
                                WorkOffline = true;
                            _client.Close();
                            System.Windows.Forms.MessageBox.Show("Отсутствует соединение с центральным контроллером! \nПроверьте сетевое соединение и перезапустите это приложение.");
                            //                          connectThread.Abort();
                        }
                        else
                        {
                            if (_firstInit == false)
                                System.Windows.Forms.MessageBox.Show("Соединение с центральным контроллером успешно установлено!");

                            _firstInit = false; // в следующий раз выдавать сообщение
                            // Send init data to the remote device.
                            Send(_initstring);
                            //              sendDone.WaitOne();

/*
                            if (socketThread.ThreadState == ThreadState.Running)
                                socketThread.Abort();
 * 
                            socketThread.Start();
  
*/
                        }
                    }

                    connectStart.Reset();

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.
                client.EndConnect(ar);

                Console.WriteLine("Socket connected to {0}",
                    client.RemoteEndPoint.ToString());

                WorkOffline = false;
                // Signal that the connection has been made.
                connectDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void ShutDownClient()
        {
            if (checkThread != null)
                checkThread.Abort();

            if (socketThread != null)
                socketThread.Abort();

            if (connectThread != null)
                connectThread.Abort();

            if (_client == null || _client.Connected == false)
                return;

            // Connect to a remote device.
            try
            {
                // Release the socket.
                _client.Shutdown(SocketShutdown.Both);
                _client.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
/*
        public void StartRecieve()
        {

            // Connect to a remote device.
            if (socketThread.ThreadState ==  ThreadState.Unstarted)
                socketThread.Start();
            else if (socketThread.ThreadState == ThreadState.Running)
                return;
            else if (socketThread.ThreadState == ThreadState.Stopped || socketThread.ThreadState == ThreadState.Suspended)
                socketThread.Resume();
        }

        public void StopRecieve()
        {
            socketThread.Suspend();
        }
*/

        public void RecieveData()
        {
            while (true)
            {
                    // Connect to a remote device.
                if (WorkOffline == false)
                {
                    try
                    {
                        if (_client.Connected == true)
                        {

                            // Create the state object.
                            StateObject state = new StateObject();
                            state.workSocket = _client;

                            // Begin receiving the data from the remote device.
                            receiveDone.Reset();
                            _client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                                new AsyncCallback(ReceiveCallback), state);
                            receiveDone.WaitOne();
                        }
                        else
                        {
                            Thread.Sleep(1000);
                        }
                    }
                    catch (ThreadAbortException abort)
                    {
                    }
                    catch (Exception e)
                    {
                        receiveDone.Set();
//                        string err = "Ошибка получения данных из контроллера!\n" + "Проверьте сетевое соединение, IP-адреса, работоспособность контроллера.\n" + e.ToString();
//                        System.Windows.Forms.MessageBox.Show(err);
                    }
                }

//              Thread.Sleep(100);
            }
        }

        public bool IsConnected()
        {
            return connectDone.WaitOne(1000);
        }

        public void CheckConnect()
        {
            while (true)
            {
                // Connect to a remote device.
                if (WorkOffline == false)
                {
                    try
                    {
                        //                      Send("Test");
                        byte[] newbyte = new byte[1];
                        _client.BeginSend(newbyte, 0, newbyte.Length, 0,
                            new AsyncCallback(SendCallback), _client);

                        if (_client.Connected == false)
                            throw new Exception();


                    }
                    catch (ThreadAbortException abort)
                    {
                    }
                    catch (Exception e)
                    {
//                      string err = "Ошибка получения данных из контроллера!\n" + "Проверьте сетевое соединение, IP-адреса, работоспособность контроллера.\n" + e.ToString();
//                      System.Windows.Forms.MessageBox.Show(err);
                        Reconnect();
                    }
                }

                Thread.Sleep(10000);
            }
        }


        private void Reconnect()
        {
            string qst = "Отсутствует соединение c контроллером! \nВыберите действие: прервать - закрыть программу, Повтор - повторить попытку соединения, Пропустить - продолжить работу без контроллера";
            System.Windows.Forms.DialogResult dr = System.Windows.Forms.MessageBox.Show(qst, "Ошибка соединения", System.Windows.Forms.MessageBoxButtons.AbortRetryIgnore);

            if (dr == System.Windows.Forms.DialogResult.Abort)
            {
                ShutDownClient();
                WorkOffline = true;
                System.Windows.Forms.Application.Exit();
            }
            else if (dr == System.Windows.Forms.DialogResult.Retry)
            {
/*
                _client.Shutdown(SocketShutdown.Both);
                _client.Close();
 */ 
                Connect2();
/*
                if (_client.Connected == true)
                    _client.Close();
 */ 
//              connectStart.Set();
            }
            else
            {
                WorkOffline = true;
            }
        }


        private void ReceiveCallback(IAsyncResult ar)
        {
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            StateObject state = (StateObject)ar.AsyncState;
            Socket client = state.workSocket;
            int bytesRead = 0;

            try
            {
                // Read data from the remote device.
                bytesRead = client.EndReceive(ar);
            }
            catch (Exception e)
            {
                string err = "Ошибка получения данных из контроллера!\n" + "Проверьте сетевое соединение, IP-адреса, работоспособность контроллера.\n" + e.ToString();
//              System.Windows.Forms.MessageBox.Show(err);
                //              Reconnect();
            }
            finally
            {
                receiveDone.Set();
            }

            if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.
                    string s = Encoding.ASCII.GetString(state.buffer, 0, bytesRead);

//                  string[] stringSeparators = new string[] { "0d\r", "0d", "\r" };
                    string[] stringSeparators = new string[] { "\r" };
                    string[] result;

                    result = s.Split(stringSeparators, 1000, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string response in result)
                    {
                        int ind_dv = response.IndexOf(":");
                        if (ind_dv > -1)
                        {
                            string r = response.Substring(ind_dv+1);

//                          NetHelper._commandStack.Enqueue(r);
                            _OnReceiveData(r);
                        }
                    }
                }
        }

        public void Send(String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            if (WorkOffline == true)
                return;

            string s = "\r";
            if (!data.Contains(":"))
            {
                data = "creator_big:" + data;
            }

            data = data + s; // добавить перевод каретки

            byte[] byteData = Encoding.ASCII.GetBytes(data);

          byte[] byteData2 = Encoding.Unicode.GetBytes(data);


            // Begin sending the data to the remote device.

            byte[] newbyte = new byte[byteData2.Length/2];
            int j = 0;
            for (int i = 0; i < byteData2.Length; i++)
            {
                if ((i % 2) == 0)
                {
                    newbyte[j] = (byte)(byteData2[i + 1] * 44 + byteData2[i]);
                    j++;
                }
            }

            try
            {
                if (_client.Connected == true)
                {
                    sendDone.Reset();
                    _client.BeginSend(newbyte, 0, newbyte.Length, 0,
                        new AsyncCallback(SendCallback), _client);

                    if (sendDone.WaitOne(5000) == false)
                        throw new Exception();
                }
            }
            catch (Exception ex)
            {
                string err = "Ошибка отправки данных в контроллер!\n" + "Проверьте сетевое соединение, IP-адреса, работоспособность контроллера.\n" + ex.ToString();
                System.Windows.Forms.MessageBox.Show(err);
//              Reconnect();
            }
         }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                //              Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.
                sendDone.Set();
            }
            catch (Exception e)
            {
//                sendDone.Set();
                  string err = "Ошибка отправки данных в контроллер!\n" + "Проверьте сетевое соединение, IP-адреса, работоспособность контроллера и перезапустите это приложение.\n" + e.ToString();
                  System.Windows.Forms.MessageBox.Show(err);
            }
        }
    }

}
