using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;

namespace Platform
{
    public partial class ProgressForm : DevExpress.XtraEditors.XtraForm
    {
        public ManualResetEvent ProgressDone =
            new ManualResetEvent(false);

        public int _maxTime = 0;

        private int step = 10;

        public string ProgressText = "";
        public string ProgressFinishText = "";
        public string ProgressFailedText = "";

        public bool IsNeedFinishEvent = false;
        public bool IsAutoCloseMode = false;

        private int _ticks = 0;
        public ProgressForm()
        {
            InitializeComponent();
        }

        public int MaxTime
        {
            set
            {
                _maxTime = value;
                step = (tmrProgress.Interval * 100) / (value);
                step = step * 2;
            }
        }

        private void tmrProgress_Tick(object sender, EventArgs e)
        {
            _ticks++;
            if (progressBarControl1.Position < 100 && ((_ticks % 2) == 0))
            {
                //progressBarControl1.Position = progressBarControl1.Position + progressBarControl1.Properties.Step;
                progressBarControl1.Increment(step);
            }

            if (IsNeedFinishEvent && ProgressDone.WaitOne(1))
            {
                tmrProgress.Stop();
                progressBarControl1.Position = 100;
                lblText.Text = ProgressFinishText;
                bntOk.Enabled = true;

                if (IsAutoCloseMode)
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            else if (progressBarControl1.Position == 100)
            {
                tmrProgress.Stop();
                bntOk.Enabled = true;


                if (IsNeedFinishEvent)
                {
                    lblText.Text = ProgressFailedText;
                    tmrProgress.Start();
                }
                else
                {
                    lblText.Text = ProgressFinishText;
                    if (IsAutoCloseMode)
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                        return;
                    }
                }


            }
        }

        private void ProgressForm_Load(object sender, EventArgs e)
        {

            tmrProgress.Start();

            lblText.Text = ProgressText;
            bntOk.Enabled = false;
        }

        public void PrepareStart()
        {

            progressBarControl1.Position = 0;
            tmrProgress.Start();

            lblText.Text = ProgressText;
            bntOk.Enabled = false;
        }

    }
}