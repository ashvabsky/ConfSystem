﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading;

using System.Data.SqlClient;
using MySql.Data.MySqlClient;


namespace Platform
{
    public enum EnNetMessageTypes
    {
        theTextMsg,
        theScreenCommand,
        theStateMsg
    }

    public class NetObject
    {
        public int ID;
        public int enCmdType;
        public int enCmdStateMaxi;
        public int enCmdStateMini;
        public byte[] Info;
        public string ScreenType;

        public NetObject()
        {
            enCmdStateMaxi = 0;
            enCmdStateMini = 0;
            enCmdType = 1;
            ScreenType = "None";
        }
    }


    public class NetHelper: IDisposable
    {

        SQLHelper m_SQL;

        public static Queue _commandStack = new Queue();

        private Thread connectThread;
        public bool _IsConnectedState = false;
        public MySqlException Exception;
        public NetHelper()
        {
            m_SQL = new SQLHelper();
            m_SQL.SetConnectionString("server=localhost; user id=root; password=atanor; database=votesystem; pooling=false;CharSet=utf8");
            m_SQL.OpenConnection();
            _IsConnectedState = true;

//            if (connectThread != null && connectThread.ThreadState == ThreadState.Running)
//                connectThread.Abort();

            connectThread = new Thread(new ThreadStart(CheckConnect));
            connectThread.Start();

        }

        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {

            // Check to see if Dispose has already been called.
            if (connectThread != null)
                connectThread.Abort();
/*
            if(!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if(disposing)
                {
                    // Dispose managed resources.
                    component.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                CloseHandle(handle);
                handle = IntPtr.Zero;

                // Note disposing has been done.
                disposed = true;

            }
 */ 
        }

        ~NetHelper()
        {
            Dispose();
        }

        public void CheckConnect()
        {
            try
            {

                while (true)
                {
                    try
                    {
                        m_SQL.CheckConnection();
                        Thread.Sleep(10000);
                    }
                    catch (MySqlException ex)
                    {
                        try
                        {
                            Thread.Sleep(3000);
                            m_SQL.CheckConnection();
                        }
                        catch (MySqlException iex)
                        {
                            _IsConnectedState = false;
                            Exception = iex;
                            break;
                        }
                    }

                }
            }

            catch (ThreadAbortException abort)
            {

            }
        }


        public bool TryGetNetCommand(ref NetObject netObj, bool IsMaxi)
        {
            if (_IsConnectedState == false)
                return false;


            SqlParameter ParamSizeType = new SqlParameter();
            ParamSizeType.ParameterName = "SizeType";
            ParamSizeType.DbType = DbType.Int32;
            if (IsMaxi == true)
                ParamSizeType.Value = 1;
            else
                ParamSizeType.Value = 2;


            MySqlDataReader reader = null;
            try
            {
                reader = m_SQL.ExecuteSP("GetNetMessage", ParamSizeType);

                if (reader.HasRows == false)
                {
                    return false;
                }

                int idScreen = 0;
                while (reader.Read())
                {
                    netObj.ID = (int)reader["id"];


                    if (reader["Info"] != System.DBNull.Value)
                        netObj.Info = (byte[])reader["Info"];


                    netObj.enCmdType = Convert.ToInt32(reader["enCmdType"]); //GetMsgType((int)dr["enType"]); 
                    netObj.enCmdStateMaxi = Convert.ToInt32(reader["StateMaxi"]);
                    netObj.enCmdStateMini = Convert.ToInt32(reader["StateMini"]);
                    idScreen = (int)reader["enScreen"];
                }

                // Call Close when done reading.
                    reader.Close();

                netObj.ScreenType = GetScreenType(idScreen);


            }
            catch (Exception Ex)
            {
                return false;
            }

            finally
            {
                if (reader != null)
                    reader.Close();
            }


 // Call Read before accessing data.


            // установить флаг, что сообщение принято на обработку
            if (netObj.ID != 0)
            {
                SqlParameter Param = new SqlParameter("idCmd", SqlDbType.Int);
                Param.Value = netObj.ID;
                m_SQL.ExecuteSP("AcceptNetMessage", Param, ParamSizeType);
            }
 
            return true;
        }


        public void SendNetObject(ref NetObject obj)
        {
            if (obj == null)
                return;
            if (_IsConnectedState == false)
                return;

            int enScreenType = GetScreenTypeID(obj.ScreenType);

            ArrayList fieldlist = new ArrayList();
            ArrayList valuelist = new ArrayList();

            fieldlist.Add("enScreen");
            fieldlist.Add("enCmdType");
            //          fieldlist.Add("ReceiverIP");
//            fieldlist.Add("State");
            fieldlist.Add("Info");
            fieldlist.Add("CmdDateTime");


            valuelist.Add(enScreenType);
            valuelist.Add(obj.enCmdType);
//          valuelist.Add(obj.enCmdState);
            valuelist.Add(obj.Info);
            valuelist.Add(DateTime.Now);

            obj.ID = m_SQL.InsertRowWithBLOB("screencommands", fieldlist, valuelist);
        }

        public void ClearScreenCommands(bool IsAll)
        {
            if (_IsConnectedState == false)
                return;

            if (IsAll)
                m_SQL.ExecuteCmd("delete from screencommands;");
            else
            {
                SqlParameter Param1 = null;
                SqlParameter Param2 = null;
                m_SQL.ExecuteSP("ScreenCommandsClear", Param1, Param2);
            }

        }

        public string GetScreenType(int enScreenID)
        {
            if (_IsConnectedState == false)
                return "None";

//            return m_SQL.GetCodeNameFromEnum("enscreens", enScreenID);
            string scr = "";
            switch (enScreenID)
            {
                case 0: scr = "None"; break;
                case 1: scr = "screen_titul"; break;
                case 2: scr = "screen_session"; break;
                case 3: scr = "screen_delegate"; break;
                case 4: scr = "screen_reginfo"; break;
                case 5: scr = "screen_regstart"; break;
                case 6: scr = "screen_regprepare"; break;
                case 7: scr = "screen_reginfo"; break;
                case 8: scr = "screen_question"; break;
                case 9: scr = "screen_voteprepare"; break;
                case 10: scr = "screen_votestart"; break;
                case 11: scr = "screen_votefinish"; break;
                case 12: scr = "screen_speechstart"; break;
            }

            return scr;
        }
        public int GetScreenTypeID(string codenameScreen)
        {
            if (_IsConnectedState == false)
                return -1;

            return m_SQL.GetIdFromEnum("enscreens", codenameScreen);
        }
    }
}
