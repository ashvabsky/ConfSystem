﻿namespace Predsedatel
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tmrData = new System.Windows.Forms.Timer();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtInfo = new DevExpress.XtraEditors.LabelControl();
            this.lblInfo = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerLeft = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridDelegates = new DevExpress.XtraGrid.GridControl();
            this.gridviewSpeechDelegates = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFraction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMicNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelQuestions = new DevExpress.XtraEditors.PanelControl();
            this.gridQuestions = new DevExpress.XtraGrid.GridControl();
            this.gridQuestionsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).BeginInit();
            this.splitContainerLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewSpeechDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelQuestions)).BeginInit();
            this.panelQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQuestionsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrData
            // 
            this.tmrData.Enabled = true;
            this.tmrData.Interval = 1500;
            this.tmrData.Tick += new System.EventHandler(this.tmrData_Tick);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.splitContainerControl1.Panel1.AppearanceCaption.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.splitContainerControl1.Panel1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.splitContainerControl1.Panel1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue;
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseBackColor = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseBorderColor = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseForeColor = true;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerLeft);
            this.splitContainerControl1.Panel1.Text = "Список выступающих";
            this.splitContainerControl1.Panel2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseFont = true;
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Экраны";
            this.splitContainerControl1.Size = new System.Drawing.Size(1014, 736);
            this.splitContainerControl1.SplitterPosition = 531;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 678);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(527, 54);
            this.panelControl1.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.Navy;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControl2.ContentImage")));
            this.panelControl2.Controls.Add(this.txtInfo);
            this.panelControl2.Controls.Add(this.lblInfo);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(2, 5);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(523, 47);
            this.panelControl2.TabIndex = 18;
            // 
            // txtInfo
            // 
            this.txtInfo.Appearance.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtInfo.Appearance.ForeColor = System.Drawing.Color.Yellow;
            this.txtInfo.Appearance.Options.UseFont = true;
            this.txtInfo.Appearance.Options.UseForeColor = true;
            this.txtInfo.Location = new System.Drawing.Point(217, 12);
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(22, 22);
            this.txtInfo.TabIndex = 1;
            this.txtInfo.Text = "50";
            // 
            // lblInfo
            // 
            this.lblInfo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblInfo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInfo.Appearance.Options.UseFont = true;
            this.lblInfo.Appearance.Options.UseForeColor = true;
            this.lblInfo.Location = new System.Drawing.Point(38, 14);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(160, 19);
            this.lblInfo.TabIndex = 0;
            this.lblInfo.Text = "Карточек на пультах:";
            // 
            // splitContainerLeft
            // 
            this.splitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerLeft.Horizontal = false;
            this.splitContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.splitContainerLeft.Name = "splitContainerLeft";
            this.splitContainerLeft.Panel1.Controls.Add(this.gridDelegates);
            this.splitContainerLeft.Panel1.Text = "Panel1";
            this.splitContainerLeft.Panel2.Controls.Add(this.panelQuestions);
            this.splitContainerLeft.Panel2.Text = "Panel2";
            this.splitContainerLeft.Size = new System.Drawing.Size(527, 732);
            this.splitContainerLeft.SplitterPosition = 380;
            this.splitContainerLeft.TabIndex = 0;
            this.splitContainerLeft.Text = "splitContainerControl3";
            // 
            // gridDelegates
            // 
            this.gridDelegates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDelegates.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridDelegates.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridDelegates.EmbeddedNavigator.Enabled = false;
            this.gridDelegates.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridDelegates.Location = new System.Drawing.Point(0, 0);
            this.gridDelegates.MainView = this.gridviewSpeechDelegates;
            this.gridDelegates.Name = "gridDelegates";
            this.gridDelegates.Size = new System.Drawing.Size(527, 380);
            this.gridDelegates.TabIndex = 18;
            this.gridDelegates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewSpeechDelegates});
            this.gridDelegates.DoubleClick += new System.EventHandler(this.gridDelegates_DoubleClick);
            // 
            // gridviewSpeechDelegates
            // 
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray;
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(218)))), ((int)(((byte)(228)))));
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Blue;
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridviewSpeechDelegates.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.Empty.BackColor = System.Drawing.Color.Navy;
            this.gridviewSpeechDelegates.Appearance.Empty.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.EvenRow.BackColor2 = System.Drawing.Color.GhostWhite;
            this.gridviewSpeechDelegates.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridviewSpeechDelegates.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridviewSpeechDelegates.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.gridviewSpeechDelegates.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridviewSpeechDelegates.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridviewSpeechDelegates.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridviewSpeechDelegates.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.gridviewSpeechDelegates.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridviewSpeechDelegates.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridviewSpeechDelegates.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(58)))), ((int)(((byte)(81)))));
            this.gridviewSpeechDelegates.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(128)))), ((int)(((byte)(151)))));
            this.gridviewSpeechDelegates.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(178)))), ((int)(((byte)(201)))));
            this.gridviewSpeechDelegates.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridviewSpeechDelegates.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridviewSpeechDelegates.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridviewSpeechDelegates.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridviewSpeechDelegates.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(195)))), ((int)(((byte)(210)))));
            this.gridviewSpeechDelegates.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(195)))), ((int)(((byte)(210)))));
            this.gridviewSpeechDelegates.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridviewSpeechDelegates.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.gridviewSpeechDelegates.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridviewSpeechDelegates.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridviewSpeechDelegates.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridviewSpeechDelegates.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupPanel.Options.UseFont = true;
            this.gridviewSpeechDelegates.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(128)))), ((int)(((byte)(151)))));
            this.gridviewSpeechDelegates.Appearance.GroupRow.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridviewSpeechDelegates.Appearance.GroupRow.ForeColor = System.Drawing.Color.Silver;
            this.gridviewSpeechDelegates.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.GroupRow.Options.UseFont = true;
            this.gridviewSpeechDelegates.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridviewSpeechDelegates.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridviewSpeechDelegates.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridviewSpeechDelegates.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridviewSpeechDelegates.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray;
            this.gridviewSpeechDelegates.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(220)))), ((int)(((byte)(227)))));
            this.gridviewSpeechDelegates.Appearance.OddRow.BackColor2 = System.Drawing.Color.White;
            this.gridviewSpeechDelegates.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridviewSpeechDelegates.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridviewSpeechDelegates.Appearance.OddRow.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.OddRow.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.gridviewSpeechDelegates.Appearance.Preview.BackColor2 = System.Drawing.Color.White;
            this.gridviewSpeechDelegates.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(128)))), ((int)(((byte)(151)))));
            this.gridviewSpeechDelegates.Appearance.Preview.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.Preview.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridviewSpeechDelegates.Appearance.Row.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridviewSpeechDelegates.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridviewSpeechDelegates.Appearance.Row.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.Row.Options.UseFont = true;
            this.gridviewSpeechDelegates.Appearance.Row.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridviewSpeechDelegates.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridviewSpeechDelegates.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(138)))), ((int)(((byte)(161)))));
            this.gridviewSpeechDelegates.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridviewSpeechDelegates.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridviewSpeechDelegates.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridviewSpeechDelegates.Appearance.VertLine.Options.UseBackColor = true;
            this.gridviewSpeechDelegates.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFullName,
            this.colRegionName,
            this.colFraction,
            this.colStatus,
            this.colMicNum,
            this.colQNum});
            this.gridviewSpeechDelegates.CustomizationFormBounds = new System.Drawing.Rectangle(862, 678, 208, 191);
            this.gridviewSpeechDelegates.GridControl = this.gridDelegates;
            this.gridviewSpeechDelegates.GroupCount = 1;
            this.gridviewSpeechDelegates.Name = "gridviewSpeechDelegates";
            this.gridviewSpeechDelegates.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridviewSpeechDelegates.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridviewSpeechDelegates.OptionsBehavior.Editable = false;
            this.gridviewSpeechDelegates.OptionsDetail.EnableMasterViewMode = false;
            this.gridviewSpeechDelegates.OptionsPrint.PrintPreview = true;
            this.gridviewSpeechDelegates.OptionsView.EnableAppearanceEvenRow = true;
            this.gridviewSpeechDelegates.OptionsView.EnableAppearanceOddRow = true;
            this.gridviewSpeechDelegates.OptionsView.ShowColumnHeaders = false;
            this.gridviewSpeechDelegates.OptionsView.ShowGroupPanel = false;
            this.gridviewSpeechDelegates.OptionsView.ShowIndicator = false;
            this.gridviewSpeechDelegates.RowHeight = 40;
            this.gridviewSpeechDelegates.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStatus, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colFullName
            // 
            this.colFullName.Caption = "ФИО";
            this.colFullName.FieldName = "idDelegate.FullName";
            this.colFullName.MinWidth = 100;
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.ReadOnly = true;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 1;
            this.colFullName.Width = 450;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Регион";
            this.colRegionName.FieldName = "idDelegate.idRegion.Name";
            this.colRegionName.MinWidth = 100;
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Width = 192;
            // 
            // colFraction
            // 
            this.colFraction.Caption = "Фракция";
            this.colFraction.FieldName = "idDelegate.idFraction.Name";
            this.colFraction.MinWidth = 100;
            this.colFraction.Name = "colFraction";
            this.colFraction.OptionsColumn.ReadOnly = true;
            this.colFraction.Width = 278;
            // 
            // colStatus
            // 
            this.colStatus.Caption = " ";
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 100;
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsColumn.ShowCaption = false;
            this.colStatus.Width = 100;
            // 
            // colMicNum
            // 
            this.colMicNum.Caption = "Мик";
            this.colMicNum.FieldName = "idSeat.MicNum";
            this.colMicNum.Name = "colMicNum";
            this.colMicNum.OptionsColumn.ReadOnly = true;
            this.colMicNum.Width = 180;
            // 
            // colQNum
            // 
            this.colQNum.Caption = "№";
            this.colQNum.FieldName = "QNum";
            this.colQNum.MinWidth = 40;
            this.colQNum.Name = "colQNum";
            this.colQNum.OptionsColumn.FixedWidth = true;
            this.colQNum.OptionsFilter.AllowAutoFilter = false;
            this.colQNum.OptionsFilter.AllowFilter = false;
            this.colQNum.Visible = true;
            this.colQNum.VisibleIndex = 0;
            // 
            // panelQuestions
            // 
            this.panelQuestions.Controls.Add(this.gridQuestions);
            this.panelQuestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQuestions.Location = new System.Drawing.Point(0, 0);
            this.panelQuestions.Name = "panelQuestions";
            this.panelQuestions.Size = new System.Drawing.Size(527, 346);
            this.panelQuestions.TabIndex = 17;
            // 
            // gridQuestions
            // 
            this.gridQuestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridQuestions.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridQuestions.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridQuestions.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridQuestions.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridQuestions.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridQuestions.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridQuestions.EmbeddedNavigator.Enabled = false;
            this.gridQuestions.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridQuestions.Location = new System.Drawing.Point(2, 2);
            this.gridQuestions.MainView = this.gridQuestionsView;
            this.gridQuestions.Name = "gridQuestions";
            this.gridQuestions.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.gridQuestions.Size = new System.Drawing.Size(523, 342);
            this.gridQuestions.TabIndex = 13;
            this.gridQuestions.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridQuestionsView});
            // 
            // gridQuestionsView
            // 
            this.gridQuestionsView.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridQuestionsView.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray;
            this.gridQuestionsView.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridQuestionsView.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridQuestionsView.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(218)))), ((int)(((byte)(228)))));
            this.gridQuestionsView.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridQuestionsView.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Blue;
            this.gridQuestionsView.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridQuestionsView.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.Empty.BackColor = System.Drawing.Color.Navy;
            this.gridQuestionsView.Appearance.Empty.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.EvenRow.BackColor2 = System.Drawing.Color.GhostWhite;
            this.gridQuestionsView.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridQuestionsView.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridQuestionsView.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.gridQuestionsView.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gridQuestionsView.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridQuestionsView.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridQuestionsView.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.gridQuestionsView.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.gridQuestionsView.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridQuestionsView.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(58)))), ((int)(((byte)(81)))));
            this.gridQuestionsView.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(128)))), ((int)(((byte)(151)))));
            this.gridQuestionsView.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(178)))), ((int)(((byte)(201)))));
            this.gridQuestionsView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridQuestionsView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridQuestionsView.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridQuestionsView.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.gridQuestionsView.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridQuestionsView.Appearance.GroupButton.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(195)))), ((int)(((byte)(210)))));
            this.gridQuestionsView.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(195)))), ((int)(((byte)(210)))));
            this.gridQuestionsView.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridQuestionsView.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridQuestionsView.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.gridQuestionsView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridQuestionsView.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridQuestionsView.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridQuestionsView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridQuestionsView.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(128)))), ((int)(((byte)(151)))));
            this.gridQuestionsView.Appearance.GroupRow.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridQuestionsView.Appearance.GroupRow.ForeColor = System.Drawing.Color.Transparent;
            this.gridQuestionsView.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.GroupRow.Options.UseFont = true;
            this.gridQuestionsView.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridQuestionsView.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridQuestionsView.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridQuestionsView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridQuestionsView.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray;
            this.gridQuestionsView.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(220)))), ((int)(((byte)(227)))));
            this.gridQuestionsView.Appearance.OddRow.BackColor2 = System.Drawing.Color.White;
            this.gridQuestionsView.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridQuestionsView.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridQuestionsView.Appearance.OddRow.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.OddRow.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.gridQuestionsView.Appearance.Preview.BackColor2 = System.Drawing.Color.White;
            this.gridQuestionsView.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(128)))), ((int)(((byte)(151)))));
            this.gridQuestionsView.Appearance.Preview.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.Preview.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridQuestionsView.Appearance.Row.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridQuestionsView.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridQuestionsView.Appearance.Row.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.Row.Options.UseFont = true;
            this.gridQuestionsView.Appearance.Row.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.gridQuestionsView.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(205)))), ((int)(((byte)(220)))));
            this.gridQuestionsView.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(138)))), ((int)(((byte)(161)))));
            this.gridQuestionsView.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridQuestionsView.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridQuestionsView.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridQuestionsView.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(185)))), ((int)(((byte)(200)))));
            this.gridQuestionsView.Appearance.VertLine.Options.UseBackColor = true;
            this.gridQuestionsView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridQuestionsView.CustomizationFormBounds = new System.Drawing.Rectangle(862, 678, 208, 191);
            this.gridQuestionsView.GridControl = this.gridQuestions;
            this.gridQuestionsView.GroupCount = 1;
            this.gridQuestionsView.Name = "gridQuestionsView";
            this.gridQuestionsView.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridQuestionsView.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridQuestionsView.OptionsBehavior.Editable = false;
            this.gridQuestionsView.OptionsDetail.EnableMasterViewMode = false;
            this.gridQuestionsView.OptionsPrint.PrintPreview = true;
            this.gridQuestionsView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridQuestionsView.OptionsView.EnableAppearanceOddRow = true;
            this.gridQuestionsView.OptionsView.ShowColumnHeaders = false;
            this.gridQuestionsView.OptionsView.ShowGroupPanel = false;
            this.gridQuestionsView.OptionsView.ShowIndicator = false;
            this.gridQuestionsView.RowHeight = 80;
            this.gridQuestionsView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn1.Caption = "№ пункта";
            this.gridColumn1.FieldName = "ItemNum";
            this.gridColumn1.MinWidth = 40;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn1.OptionsFilter.AllowFilter = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Название";
            this.gridColumn2.ColumnEdit = this.repositoryItemMemoEdit1;
            this.gridColumn2.FieldName = "Caption";
            this.gridColumn2.MinWidth = 100;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 426;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.repositoryItemMemoEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemMemoEdit1.LinesCount = 10;
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            this.repositoryItemMemoEdit1.ReadOnly = true;
            this.repositoryItemMemoEdit1.WordWrap = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Повестка дня";
            this.gridColumn3.FieldName = "EmptyValue";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "# по списку";
            this.gridColumn4.FieldName = "QueueNum";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.pictureEdit2);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel1.DoubleClick += new System.EventHandler(this.splitContainerControl2_Panel1_DoubleClick);
            this.splitContainerControl2.Panel2.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.splitContainerControl2.Panel2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("splitContainerControl2.Panel2.Appearance.Image")));
            this.splitContainerControl2.Panel2.Appearance.Options.UseBorderColor = true;
            this.splitContainerControl2.Panel2.Appearance.Options.UseImage = true;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel2.Controls.Add(this.pictureEdit1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(477, 736);
            this.splitContainerControl2.SplitterPosition = 377;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit2.BackgroundImage")));
            this.pictureEdit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit2.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Size = new System.Drawing.Size(473, 377);
            this.pictureEdit2.TabIndex = 1;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit1.BackgroundImage")));
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Size = new System.Drawing.Size(469, 345);
            this.pictureEdit1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 736);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.DoubleClick += new System.EventHandler(this.Form1_DoubleClick);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).EndInit();
            this.splitContainerLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewSpeechDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelQuestions)).EndInit();
            this.panelQuestions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQuestionsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer tmrData;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerLeft;
        private DevExpress.XtraEditors.PanelControl panelQuestions;
        private DevExpress.XtraGrid.GridControl gridQuestions;
        private DevExpress.XtraGrid.Views.Grid.GridView gridQuestionsView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.GridControl gridDelegates;
        private DevExpress.XtraGrid.Views.Grid.GridView gridviewSpeechDelegates;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colFraction;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colMicNum;
        private DevExpress.XtraGrid.Columns.GridColumn colQNum;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl txtInfo;
        private DevExpress.XtraEditors.LabelControl lblInfo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;

    }
}

