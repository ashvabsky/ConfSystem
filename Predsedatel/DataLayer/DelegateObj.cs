using System;
using DevExpress.Xpo;
namespace VoteSystem
{
    [Persistent("votesystem.endelegatetype")]
    public class DelegateTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(50)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fValue;
        [Size(150)]
        public string Value
        {
            get { return fValue; }
            set { SetPropertyValue<string>("Value", ref fValue, value); }
        }

        public DelegateTypeObj(Session session) : base(session) { }
        public DelegateTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.delegates")]
    public class DelegateObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fLastName;
        [Size(150)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fFirstName;
        [Size(150)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fSecondName;
        [Size(150)]
        public string SecondName
        {
            get { return fSecondName; }
            set { SetPropertyValue<string>("SecondName", ref fSecondName, value); }
        }

        [NonPersistent]
        public string FullName { get { return String.Format("{0}, {1} {2}", fLastName, fFirstName, fSecondName); } }

        PartyObj fidParty;
        [Association("PartyObj-DelegateObj")]
        public PartyObj idParty
        {
            get { return fidParty; }
            set { SetPropertyValue<PartyObj>("idParty", ref fidParty, value); }
        }

        FractionObj fidFraction;
        [Association("FractionObj-DelegateObj")]
        public FractionObj idFraction
        {
            get { return fidFraction; }
            set { SetPropertyValue<FractionObj>("idFraction", ref fidFraction, value); }
        }

        RegionObj fidRegion;
        [Association("RegionObj-DelegateObj")]
        public RegionObj idRegion
        {
            get { return fidRegion; }
            set { SetPropertyValue<RegionObj>("idRegion", ref fidRegion, value); }
        }

        bool fisActive;
        public bool isActive
        {
            get { return fisActive; }
            set { SetPropertyValue<bool>("isActive", ref fisActive, value); }
        }

        DelegateTypeObj fidType;
        public DelegateTypeObj idType
        {
            get { return fidType; }
            set { SetPropertyValue<DelegateTypeObj>("idType", ref fidType, value); }
        }

        SeatObj fidSeat;
        public SeatObj idSeat
        {
            get { return fidSeat; }
            set { SetPropertyValue<SeatObj>("idSeat", ref fidSeat, value); }
        }


        string fNotes;
        [Size(255)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }

        [NonPersistent]
        public string MicNum { get { return String.Format("{0}", fidSeat.MicNum.ToString()); } }

        [NonPersistent]
        public string RowNum { get { return String.Format("{0}", fidSeat.RowNum.ToString()); } }

        [NonPersistent]
        public string SeatNum { get { return String.Format("{0}", fidSeat.SeatNum.ToString()); } }

        public DelegateObj(Session session) : base(session) { }
        public DelegateObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.parties")]
    public class PartyObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        bool fInUse;
        public bool InUse
        {
            get { return fInUse; }
            set { SetPropertyValue<bool>("InUse", ref fInUse, value); }
        }

        string fDescription;
        [Size(255)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }


        [Association("PartyObj-DelegateObj", typeof(DelegateObj)), Aggregated]
        public XPCollection<DelegateObj> Delegates { get { return GetCollection<DelegateObj>("Delegates"); } }


        public PartyObj(Session session) : base(session) { }
        public PartyObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.fractions")]
    public class FractionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fDescription;
        [Size(255)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }

        bool fInUse;
        public bool InUse
        {
            get { return fInUse; }
            set { SetPropertyValue<bool>("InUse", ref fInUse, value); }
        }


        [Association("FractionObj-DelegateObj", typeof(DelegateObj)), Aggregated]
        public XPCollection<DelegateObj> Delegates { get { return GetCollection<DelegateObj>("Delegates"); } }


        public FractionObj(Session session) : base(session) { }
        public FractionObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.regions")]
    public class RegionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fNumber;
        [Size(50)]
        public string Number
        {
            get { return fNumber; }
            set { SetPropertyValue<string>("Number", ref fNumber, value); }
        }

        bool fInUse;
        public bool InUse
        {
            get { return fInUse; }
            set { SetPropertyValue<bool>("InUse", ref fInUse, value); }
        }


        string fDescription;
        [Size(255)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }

        [Association("RegionObj-DelegateObj", typeof(DelegateObj)), Aggregated]
        public XPCollection<DelegateObj> Delegates { get { return GetCollection<DelegateObj>("Delegates"); } }


        public RegionObj(Session session) : base(session) { }
        public RegionObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
