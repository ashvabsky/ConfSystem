﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Predsedatel
{
    [Persistent("votesystem.enresultvalues")]
    public class QuestResultType : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(50)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fValue;
        [Size(150)]
        public string Value
        {
            get { return fValue; }
            set { SetPropertyValue<string>("Value", ref fValue, value); }
        }

        public QuestResultType(Session session) : base(session) { }
        public QuestResultType() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.s_questions3")]
    public class S_QuestionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        SessionObj fidSession;
        public SessionObj idSession
        {
            get { return fidSession; }
            set { SetPropertyValue<SessionObj>("idSession", ref fidSession, value); }
        }

        QuestObj fidQuestion;
        public QuestObj idQuestion
        {
            get { return fidQuestion; }
            set { SetPropertyValue<QuestObj>("idQuestion", ref fidQuestion, value); }
        }

        string fDescription;
        [Size(255)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }

        string fNotes;
        [Size(255)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }


        VoteTypeObj fidVoteType;
        public VoteTypeObj idVoteType
        {
            get { return fidVoteType; }
            set { SetPropertyValue<VoteTypeObj>("idVoteType", ref fidVoteType, value); }
        }
        VoteKindObj fidVoteKind;
        public VoteKindObj idVoteKind
        {
            get { return fidVoteKind; }
            set { SetPropertyValue<VoteKindObj>("idVoteKind", ref fidVoteKind, value); }
        }

        VoteQntyObj fidVoteQnty;
        public VoteQntyObj idVoteQnty
        {
            get { return fidVoteQnty; }
            set { SetPropertyValue<VoteQntyObj>("idVoteQnty", ref fidVoteQnty, value); }
        }

        DecisionProcTypeObj fidDecisionProcType;
        public DecisionProcTypeObj idDecisionProcType
        {
            get { return fidDecisionProcType; }
            set { SetPropertyValue<DecisionProcTypeObj>("idDecisionProcType", ref fidDecisionProcType, value); }
        }

        int fDecisionProcValue;
        public int DecisionProcValue
        {
            get { return fDecisionProcValue; }
            set { SetPropertyValue<int>("DecisionProcValue", ref fDecisionProcValue, value); }
        }

        QuotaProcTypeObj fidQuotaProcentType;
        public QuotaProcTypeObj idQuotaProcentType
        {
            get { return fidQuotaProcentType; }
            set { SetPropertyValue<QuotaProcTypeObj>("idQuotaProcentType", ref fidQuotaProcentType, value); }
        }

        int fQuotaProcentValue;
        public int QuotaProcentValue
        {
            get { return fQuotaProcentValue; }
            set { SetPropertyValue<int>("QuotaProcentValue", ref fQuotaProcentValue, value); }
        }


        int fReadNum;
        public int ReadNum
        {
            get { return fReadNum; }
            set { SetPropertyValue<int>("ReadNum", ref fReadNum, value); }
        }

/*
        bool fIsResult;
        public bool IsResult
        {
            get { return fIsResult; }
            set { SetPropertyValue<bool>("IsResult", ref fIsResult, value); }
        }
*/

        QuestResultType fidResultValue;
        public QuestResultType idResultValue
        {
            get { return fidResultValue; }
            set { SetPropertyValue<QuestResultType>("idResultValue", ref fidResultValue, value); }
        }

        int fidResultValueInt;
        public int idResultValueInt
        {
            get { return fidResultValueInt; }
            set { SetPropertyValue<int>("idResultValue", ref fidResultValueInt, value); }
        }

        DateTime fVoteDateTime;
        public DateTime VoteDateTime
        {
            get { return fVoteDateTime; }
            set { SetPropertyValue<DateTime>("VoteDateTime", ref fVoteDateTime, value); }
        }

        [NonPersistent]
        public bool IsResult 
        { 
            get 
            {
                if (fidResultValue.Name == "NoResult")
                    return false;
                else
                    return true;
            } 
        }

        public S_QuestionObj(Session session) : base(session) { }
        public S_QuestionObj()
            : base(Session.DefaultSession)
        {
        }

        public S_QuestionObj(S_QuestionObj copy) : base(Session.DefaultSession) 
        {
            this.id = 0;
            this.idDecisionProcType = copy.idDecisionProcType;
            this.idQuestion = copy.idQuestion;
            this.idQuotaProcentType = copy.idQuotaProcentType;
            this.idResultValue = copy.idResultValue;
            this.idSession = copy.idSession;
            this.idVoteKind = copy.idVoteKind;
            this.idVoteQnty = copy.idVoteQnty;
            this.idVoteType = copy.idVoteType;
            this.Notes = copy.Notes;
            this.QuotaProcentValue = copy.QuotaProcentValue;
            this.ReadNum = copy.ReadNum + 1;
            this.VoteDateTime = DateTime.Now;
        }

        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
