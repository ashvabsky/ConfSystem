﻿using System;
using DevExpress.Xpo;
namespace Predsedatel
{
    [Persistent("votesystem.fractions")]
    public class FractionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fDescription;
        [Size(255)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }

        bool fInUse;
        public bool InUse
        {
            get { return fInUse; }
            set { SetPropertyValue<bool>("InUse", ref fInUse, value); }
        }


        [Association("FractionObj-DelegateObj", typeof(DelegateObj)), Aggregated]
        public XPCollection<DelegateObj> Delegates { get { return GetCollection<DelegateObj>("Delegates"); } }


        public FractionObj(Session session) : base(session) { }
        public FractionObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
