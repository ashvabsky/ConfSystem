using System;
using DevExpress.Xpo;
namespace VoteSystem
{
    [Persistent("votesystem.sessions")]
    public class SessionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fCode;
        [Size(50)]
        public string Code
        {
            get { return fCode; }
            set { SetPropertyValue<string>("Code", ref fCode, value); }
        }
        string fCaption;
        [Size(255)]
        public string Caption
        {
            get { return fCaption; }
            set { SetPropertyValue<string>("Caption", ref fCaption, value); }
        }
        string fContent;
        [Size(255)]
        public string Content
        {
            get { return fContent; }
            set { SetPropertyValue<string>("Content", ref fContent, value); }
        }
        DateTime fStartDate;
        public DateTime StartDate
        {
            get { return fStartDate; }
            set { SetPropertyValue<DateTime>("StartDate", ref fStartDate, value); }
        }

        DateTime fFinishDate;
        public DateTime FinishDate
        {
            get { return fFinishDate; }
            set { SetPropertyValue<DateTime>("FinishDate", ref fFinishDate, value); }
        }

        string fHallName;
        [Size(255)]
        public string HallName
        {
            get { return fHallName; }
            set { SetPropertyValue<string>("HallName", ref fHallName, value); }
        }
        bool fRegAfterTime;
        public bool RegAfterTime
        {
            get { return fRegAfterTime; }
            set { SetPropertyValue<bool>("RegAfterTime", ref fRegAfterTime, value); }
        }

        bool fIsFinished;
        public bool IsFinished
        {
            get { return fIsFinished; }
            set { SetPropertyValue<bool>("IsFinished", ref fIsFinished, value); }
        }

        public SessionObj(Session session) : base(session) 
        {
        }
        public SessionObj() : base(Session.DefaultSession) 
        {
            fCaption = "";
            fCode = "";
            fContent = "";
            fHallName = "";
            fIsFinished = false;
            fRegAfterTime = false;
        }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
