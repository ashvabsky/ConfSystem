﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Predsedatel
{
    [Persistent("votesystem.s_delegates")]
    public class S_DelegateObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        SessionObj fidSession;
        public SessionObj idSession
        {
            get { return fidSession; }
            set { SetPropertyValue<SessionObj>("idSession", ref fidSession, value); }
        }

        DelegateObj fidDelegate;
        public DelegateObj idDelegate
        {
            get { return fidDelegate; }
            set { SetPropertyValue<DelegateObj>("idDelegate", ref fidDelegate, value); }
        }

        SeatObj fidSeat;
        public SeatObj idSeat
        {
            get { return fidSeat; }
            set { SetPropertyValue<SeatObj>("idSeat", ref fidSeat, value); }
        }

        bool fIsRegistered;
        public bool IsRegistered
        {
            get { return fIsRegistered; }
            set { SetPropertyValue<bool>("IsRegistered", ref fIsRegistered, value); }
        }

        public S_DelegateObj(Session session) : base(session) { }
        public S_DelegateObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
