﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.enresultvalues")]
    public class ResultValueObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        public ResultValueObj(Session session) : base(session) { }
        public ResultValueObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.questions")]
    public class QuestionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        SessionObj fidSession;
        public SessionObj idSession
        {
            get { return fidSession; }
            set { SetPropertyValue<SessionObj>("idSession", ref fidSession, value); }
        }

        SesQuestionObj fidQuestion;
        public SesQuestionObj idQuestion
        {
            get { return fidQuestion; }
            set { SetPropertyValue<SesQuestionObj>("idQuestion", ref fidQuestion, value); }
        }

        string fNotes;
        [Size(255)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }


        ResultValueObj fidResultValue;
        public ResultValueObj idResultValue
        {
            get { return fidResultValue; }
            set { SetPropertyValue<ResultValueObj>("idResultValue", ref fidResultValue, value); }
        }

        int fidResultValueInt;
        public int idResultValueInt
        {
            get { return fidResultValueInt; }
            set { SetPropertyValue<int>("idResultValue", ref fidResultValueInt, value); }
        }

        VoteResultObj fidVoteResult;
        public VoteResultObj idVoteResult
        {
            get { return fidVoteResult; }
            set { SetPropertyValue<VoteResultObj>("idVoteResult", ref fidVoteResult, value); }
        }

        DateTime fVoteDateTime;
        public DateTime VoteDateTime
        {
            get { return fVoteDateTime; }
            set { SetPropertyValue<DateTime>("VoteDateTime", ref fVoteDateTime, value); }
        }

        [NonPersistent]
        public bool IsResult 
        { 
            get 
            {
                if (fidVoteResult == null)
                    return false;

                if (fidVoteResult.idResultValue == null)
                    return false;

                if (fidVoteResult.idResultValue.CodeName == "Parlament_Accept")
                    return true;
                else
                    return false;
            } 
        }

        public QuestionObj(Session session) : base(session) { }
        public QuestionObj()
            : base(Session.DefaultSession)
        {
        }

        public QuestionObj(SesQuestionObj q)
            : base(Session.DefaultSession)
        {
            this.idQuestion = q;
            this.idSession = q.idSession;
            this.Notes = "";
            this.VoteDateTime = DateTime.Now;
        }

/*
        public QuestionObj(QuestionObj copy)
            : base(Session.DefaultSession) 
        {
            this.id = 0;
            this.idResultValue = copy.idResultValue;
            this.idSession = copy.idSession;
            this.Notes = "";
            this.VoteDateTime = DateTime.Now;

        }
*/
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
