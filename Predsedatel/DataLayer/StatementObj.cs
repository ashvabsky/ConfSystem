﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.statements")]
    public class StatementObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fName;
        [Size(255)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fNotes;
        [Size(255)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }


        DateTime fCrDate;
        public DateTime CrDate
        {
            get { return fCrDate; }
            set { SetPropertyValue<DateTime>("CrDate", ref fCrDate, value); }
        }

        int fStateTime;
        public int StateTime
        {
            get { return fStateTime; }
            set { SetPropertyValue<int>("StateTime", ref fStateTime, value); }
        }

        int fMicOffMode;
        public int MicOffMode
        {
            get { return fMicOffMode; }
            set { SetPropertyValue<int>("MicOffMode", ref fMicOffMode, value); }
        }

        bool fState;
        public bool State
        {
            get { return fState; }
            set { SetPropertyValue<bool>("State", ref fState, value); }
        }

        [NonPersistent]
        public string Status 
        { 
            get
            {
                if (fState == true)
                    return "завершено";
                else
                    return "к обсуждению";
            } 
        }

        public StatementObj(Session session) : base(session) { }
        public StatementObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
