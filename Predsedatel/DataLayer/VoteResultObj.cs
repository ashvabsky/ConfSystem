﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.voteresults")]
    public class VoteResultObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        ResultValueObj fidResultValue;
        public ResultValueObj idResultValue
        {
            get { return fidResultValue; }
            set { SetPropertyValue<ResultValueObj>("idResultValue", ref fidResultValue, value); }
        }

        [NonPersistent]
        public bool IsResult
        {
            get
            {
                if (fidResultValue.Name == "NoResult")
                    return false;
                else
                    return true;
            }
        }

        DateTime fVoteDateTime;
        public DateTime VoteDateTime
        {
            get { return fVoteDateTime; }
            set { SetPropertyValue<DateTime>("VoteDateTime", ref fVoteDateTime, value); }
        }

        int fAnsQnty1;
        public int AnsQnty1
        {
            get { return fAnsQnty1; }
            set { SetPropertyValue<int>("AnsQnty1", ref fAnsQnty1, value); }
        }

        int fAnsQnty2;
        public int AnsQnty2
        {
            get { return fAnsQnty2; }
            set { SetPropertyValue<int>("AnsQnty2", ref fAnsQnty2, value); }
        }

        int fAnsQnty3;
        public int AnsQnty3
        {
            get { return fAnsQnty3; }
            set { SetPropertyValue<int>("AnsQnty3", ref fAnsQnty3, value); }
        }

        int fAnsQnty4;
        public int AnsQnty4
        {
            get { return fAnsQnty4; }
            set { SetPropertyValue<int>("AnsQnty4", ref fAnsQnty4, value); }
        }

        int fAnsQnty5;
        public int AnsQnty5
        {
            get { return fAnsQnty5; }
            set { SetPropertyValue<int>("AnsQnty5", ref fAnsQnty4, value); }
        }

        int fAvailableQnty;
        public int AvailableQnty
        {
            get { return fAvailableQnty; }
            set { SetPropertyValue<int>("AvailableQnty", ref fAvailableQnty, value); }
        }

        public VoteResultObj(Session session) : base(session) { }
        public VoteResultObj() : base(Session.DefaultSession) 
        {
            AnsQnty1 = 0;
            AnsQnty2 = 0;
            AnsQnty3 = 0;
            AnsQnty4 = 0;
            AnsQnty5 = 0;
        }

        public override void AfterConstruction() { base.AfterConstruction(); }

    }
}
