﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Predsedatel
{
    [Persistent("votesystem.questions")]
    public class QuestObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fCaption;
        [Size(255)]
        public string Caption
        {
            get { return fCaption; }
            set { SetPropertyValue<string>("Caption", ref fCaption, value); }
        }

        string fDescription;
        [Size(255)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }

        VoteTypeObj fidType;
//      [Association("VoteTypeObj-QuestObj")]
        public VoteTypeObj idType
        {
            get { return fidType; }
            set { SetPropertyValue<VoteTypeObj>("idType", ref fidType, value); }
        }

        VoteKindObj fidKind;
//      [Association("VoteKindObj-QuestObj")]
        public VoteKindObj idKind
        {
            get { return fidKind; }
            set { SetPropertyValue<VoteKindObj>("idKind", ref fidKind, value); }
        }

        DateTime fCrDate;
        public DateTime CrDate
        {
            get { return fCrDate; }
            set { SetPropertyValue<DateTime>("CrDate", ref fCrDate, value); }
        }

        public QuestObj(Session session) : base(session) { }
        public QuestObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.envotetypes")]
    public class VoteTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(255)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
/*
        [Association("VoteTypeObj-QuestObj", typeof(VoteTypeObj)), Aggregated]
        public XPCollection<VoteTypeObj> Quests { get { return GetCollection<DelegateObj>("Quests"); } }
*/

        public VoteTypeObj(Session session) : base(session) { }
        public VoteTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.envotekinds")]
    public class VoteKindObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(255)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        /*
                [Association("VoteTypeObj-QuestObj", typeof(VoteTypeObj)), Aggregated]
                public XPCollection<VoteTypeObj> Quests { get { return GetCollection<DelegateObj>("Quests"); } }
        */

        public VoteKindObj(Session session) : base(session) { }
        public VoteKindObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.envoteqnty")]
    public class VoteQntyObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(50)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fValue;
        [Size(150)]
        public string Value
        {
            get { return fValue; }
            set { SetPropertyValue<string>("Value", ref fValue, value); }
        }

        public VoteQntyObj(Session session) : base(session) { }
        public VoteQntyObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


    [Persistent("votesystem.endecisionproctype")]
    public class DecisionProcTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(50)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fValue;
        [Size(150)]
        public string Value
        {
            get { return fValue; }
            set { SetPropertyValue<string>("Value", ref fValue, value); }
        }

        public DecisionProcTypeObj(Session session) : base(session) { }
        public DecisionProcTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.enquotaproctype")]
    public class QuotaProcTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(50)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fValue;
        [Size(150)]
        public string Value
        {
            get { return fValue; }
            set { SetPropertyValue<string>("Value", ref fValue, value); }
        }

        public QuotaProcTypeObj(Session session) : base(session) { }
        public QuotaProcTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
