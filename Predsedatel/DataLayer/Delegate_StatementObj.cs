﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.delegates_statements")]
    public class DelegateStatementObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        SesDelegateObj fidDelegate;
        public SesDelegateObj idDelegate
        {
            get { return fidDelegate; }
            set { SetPropertyValue<SesDelegateObj>("idDelegate", ref fidDelegate, value); }
        }

        StatementObj fidStatement;
        public StatementObj idStatement
        {
            get { return fidStatement; }
            set { SetPropertyValue<StatementObj>("idStatement", ref fidStatement, value); }
        }

        int fState;
        public int State
        {
            get { return fState; }
            set { SetPropertyValue<int>("State", ref fState, value); }
        }

        int fStatementTime;
        public int StatementTime
        {
            get { return fStatementTime; }
            set { SetPropertyValue<int>("StatementTime", ref fStatementTime, value); }
        }

        int fQNum;
        public int QNum
        {
            get { return fQNum; }
            set { SetPropertyValue<int>("QNum", ref fQNum, value); }
        }

        [NonPersistent]
        public string Status
        {
            get
            {
                if (fState == 1)
                    return "III. Выступившие";
                else if (fState == 2)
                    return "I. Выступает";
                else 
                    return "II. Готовятся";
            }
        }

        public DelegateStatementObj(Session session) : base(session) { }
        public DelegateStatementObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
