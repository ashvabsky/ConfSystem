using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public interface IVoteModeForm
    {
        IMsgMethods GetShowInterface();

        void ShowView();

        void InitControls(string Caption);
        void InitSeats();
        void SetDelegateProperties(Dictionary<string, string> properties);
        void SetQuestionProperties(Dictionary<string, object> properties);
        void ShowVoteResults(VoteResultObj vro);
        void StartVoting(int VotTime);
        void StopVoting();
        int VoteQnty { set; }
        void InitParams(int VoteTime, int DelegatesQnty);
    }

    public partial class VoteModeForm : DevExpress.XtraEditors.XtraForm, IVoteModeForm
    {
        VoteModeCntrler _Controller;
        MainCntrler _mainCntrler;
        MsgForm _msgForm;

        public VoteModeForm()
        {
            InitializeComponent();
        }

        public VoteModeForm(VoteModeCntrler cntler, MainCntrler mcontr, MainForm mform)
        {
            InitializeComponent();
            _Controller = cntler;
            _mainCntrler = mcontr;
            _msgForm = new MsgForm(this);
        }

        IMsgMethods IVoteModeForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        int _CurrentImageNum = 0;



#region ���������� ���������� IVoteModeForm

        public void ShowView()
        {
            ShowDialog();
        }

        void IVoteModeForm.InitControls(string Caption)
        {
            _CurrentImageNum = 0;
            picVoteTime.Image = TimerImages.Images[_CurrentImageNum];
            picVoteTime.Enabled = false;
            lblVoting.Enabled = false;
            progressBarVoting.Enabled = false;

            txtDelegateQnty.Properties.ReadOnly = true;

            txtVoteQnty.Properties.ReadOnly = true;

            btnVoteCancel.Text = "�������� ����� �����������";
            btnVoteCancel.Enabled = true;
            btnVoteFinish.Enabled = false;
            btnVoteStart.Enabled = true;

            grpSeates.Text = "����������� �� �������: " + Caption;
        }

        void IVoteModeForm.InitSeats()
        {
            foreach (Control c in grpSeates.Controls)
            {
                if (c.Tag == null)
                    continue;

                if (c.Tag.ToString().StartsWith("Mic"))
                {
                    string s = c.Tag.ToString();
                    int MicNum = Convert.ToInt32(s.Substring(3));
                    DevExpress.XtraEditors.PictureEdit pic = (DevExpress.XtraEditors.PictureEdit)c;
                    VoteModeCntrler.SeatStates seatstate = _Controller.GetSeatState(MicNum);

                    if (seatstate == VoteModeCntrler.SeatStates.None)
                        pic.Image = ManImages.Images[7];
                    else if (seatstate == VoteModeCntrler.SeatStates.Disabled)
                        pic.Image = ManImages.Images[6];
                    else if (seatstate == VoteModeCntrler.SeatStates.NoAnswer)
                        pic.Image = ManImages.Images[5];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer1)
                        pic.Image = ManImages.Images[0];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer2)
                        pic.Image = ManImages.Images[1];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer3)
                        pic.Image = ManImages.Images[2];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer4)
                        pic.Image = ManImages.Images[5];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer5)
                        pic.Image = ManImages.Images[5];
                }
            }

        }

        void IVoteModeForm.SetDelegateProperties(Dictionary<string, string> properties)
        {
            PropertiesControlDelegate.Rows["row_FirstName"].Properties.Value = properties["FirstName"];
            PropertiesControlDelegate.Rows["row_SecondName"].Properties.Value = properties["SecondName"];
            PropertiesControlDelegate.Rows["row_LastName"].Properties.Value = properties["LastName"];

            PropertiesControlDelegate.Rows["row_PartyName"].Properties.Value = properties["PartyName"];
            PropertiesControlDelegate.Rows["row_Fraction"].Properties.Value = properties["FractionName"];
            PropertiesControlDelegate.Rows["row_RegionName"].Properties.Value = properties["RegionName"];
            PropertiesControlDelegate.Rows["row_RegionNum"].Properties.Value = properties["RegionNum"];

            PropertiesControlDelegate.Rows["row_MicNum"].Properties.Value = properties["MicNum"];
            PropertiesControlDelegate.Rows["row_RowNum"].Properties.Value = properties["RowNum"];
            PropertiesControlDelegate.Rows["row_SeatNum"].Properties.Value = properties["SeatNum"];
        }

        void IVoteModeForm.SetQuestionProperties(Dictionary<string, object> properties)
        {
            PropertiesControlQuest.Rows["row_sq_Number"].Properties.Value = (string)properties["Number"].ToString();
            PropertiesControlQuest.Rows["row_sq_Caption"].Properties.Value = (string)properties["Caption"];
            PropertiesControlQuest.Rows["row_sq_CrDate"].Properties.Value = properties["CrDate"];

            PropertiesControlQuest.Rows["row_sq_Notes"].Properties.Value = properties["Notes"];
            PropertiesControlQuest.Rows["row_sq_VoteType"].Properties.Value = properties["VoteType_Value"];

            PropertiesControlQuest.Rows["row_sq_VoteKind"].Properties.Value = properties["VoteKind_Value"];

            PropertiesControlQuest.Rows["row_sq_ReadNum"].Properties.Value = properties["ReadNum"];

            PropertiesControlQuest.Rows["row_sq_VoteQnty"].Properties.Value = properties["VoteQntyType_Value"];

            PropertiesControlQuest.Rows["row_sq_DecisionProcValue"].Properties.Value = properties["DecisionProc_Value"];

            PropertiesControlQuest.Rows["row_sq_DecisionProcType"].Properties.Value = properties["DecisionProcType_Value"];

            PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = false;
            PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = false;

            if ((string)properties["QuotaProcType_Name"] == "Total_Qnty")
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = true;
            else if ((string)properties["QuotaProcType_Name"] == "Present_Qnty")
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = true;

        }

        void IVoteModeForm.ShowVoteResults(VoteResultObj vro)
        {
            if (vro != null && vro.id != 0)
            {
                grpVoteResultsTable.Visible = true;
                grpVoteResults.Visible = true;

                if (_Controller.VoteMode != true)
                {
                    if (vro.idResultValue.id != 0)
                    {
                        btnVoteCancel.Enabled = true;
                        btnVoteStart.Enabled = false;
                    }
                    else
                    {
                        btnVoteCancel.Enabled = false;
                        btnVoteStart.Enabled = true;
                    }
                }

                lblDecision.Text = vro.idResultValue.Value;

                lblVoteResDate.Text = vro.VoteDateTime.ToString("dd.MM.yy");
                lblVoteResTime.Text = vro.VoteDateTime.ToString("t");

                lblVoteAye.Text = vro.AnsQnty1.ToString();
                lblVoteAgainst.Text = vro.AnsQnty2.ToString();
                lblVoteAbstain.Text = vro.AnsQnty3.ToString();

                int votes = vro.AnsQnty1 + vro.AnsQnty2 + vro.AnsQnty3;
                int votes_proc = (votes * 100) / vro.AvailableQnty;

                int procAye = (vro.AnsQnty1 * 100) / vro.AvailableQnty;
                int procAgainst = (vro.AnsQnty2 * 100) / vro.AvailableQnty;
                int procAbstain = (vro.AnsQnty3 * 100) / vro.AvailableQnty;

                lblProcAye.Text = procAye.ToString();
                lblProcAgainst.Text = procAgainst.ToString();
                lblProcAbstain.Text = procAbstain.ToString();

                int notvoted = vro.AvailableQnty - votes;
                int notvoted_proc = (notvoted * 100) / vro.AvailableQnty;

                lblNonVoteQnty.Text = notvoted.ToString();
                lblNonProcQnty.Text = notvoted_proc.ToString();

                //                  lblVoteQnty.Text = string.Format("{0} �� {1}", votes, vro.AvailableQnty);
                txtProcQnty.Text = string.Format("{0}", votes_proc);
                txtVoteQnty.Text = votes.ToString();
            }

            if ((vro == null || vro.id == 0) || (_Controller.VoteMode == false && vro != null && vro.idResultValue.id == 0))
            {
                grpVoteResults.Visible = false;

                //              lblVoteQnty.Text = "";
                txtProcQnty.Text = "";
                txtVoteQnty.Text = "";

                btnVoteStart.Enabled = true;
                btnVoteCancel.Enabled = false;

            }

            if (_Controller.VoteMode == true) // ���� ����������
            {
                btnVoteStart.Enabled = false;
                btnVoteCancel.Enabled = false;
            }

            btnClose.Enabled = !_Controller.VoteMode;

        }

        void IVoteModeForm.StartVoting(int VotTime)
        {
            btnVoteCancel.Enabled = false;
            btnVoteFinish.Enabled = true;
            btnVoteStart.Enabled = false;
            picVoteTime.Enabled = true;
            lblVoting.Visible = true;
            lblVoting.Enabled = true;
            progressBarVoting.Enabled = true;
            progressBarVoting.Visible = true;
            btnVoteCancel.Enabled = false;

            btnVoteStart.Text = "��������� �����������";
            //          btnVoteStart.ImageIndex = 9;

            _VoteTime = VotTime;
            _startvotetime = DateTime.Now;

            tmrVoteStart.Start();
            tmrRotation.Start();
        }

        void IVoteModeForm.StopVoting()
        {
            if (tmrVoteStart.Enabled == true)
            {
                tmrVoteStart.Stop();
                tmrRotation.Stop();
            }

            progressBarVoting.Position = -1;
            progressBarVoting.Enabled = false;

            lblVoting.Enabled = false;

            _CurrentImageNum = 0;
            picVoteTime.Image = TimerImages.Images[_CurrentImageNum];

            picVoteTime.Enabled = false;
            lblVoteTime.Enabled = false;

            btnVoteStart.Text = "������ �����������";
            btnVoteStart.ImageIndex = 0;

            btnVoteCancel.Enabled = true;
            btnVoteFinish.Enabled = false;
            btnVoteStart.Enabled = true;
        }

        int IVoteModeForm.VoteQnty
        {
            set { txtVoteQnty.Text = value.ToString(); }
        }

        void IVoteModeForm.InitParams(int VoteTime, int DelegatesQnty)
        {
            lblVoteTime.Text = String.Format("{0} ���.", VoteTime);
            txtDelegateQnty.Text = DelegatesQnty.ToString();
        }
#endregion

#region private methods
        private void VoteModeForm_Load(object sender, EventArgs e)
        {
            _Controller.OnLoadForm();
        }


        private void Mic_MouseEnter(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            if (c.Tag == null)
            {
                _Controller.ShowDelegateProp(-1); // �������� ������ ����
                return;
            }

            string s = c.Tag.ToString();
            int MicNum = Convert.ToInt32(s.Substring(3));

            _Controller.ShowDelegateProp(MicNum);
        }


        private void btnVoteStart_Click(object sender, EventArgs e)
        {
            _Controller.Vote_PrepareToStart();
        }


        DateTime _startvotetime;
        int _VoteTime;
        int _VoteQnty = 0;

        private void tmrVoteStart_Tick(object sender, EventArgs e)
        {

            System.TimeSpan time = DateTime.Now - _startvotetime;
            DateTime voteTime = new DateTime(1900, 12, 13, 0, _VoteTime, 0);

            DateTime t = voteTime.AddTicks(-time.Ticks);
            lblVoteTime.Text = t.ToString("T");

            int VoteSecs = voteTime.Minute * 60 + voteTime.Second;
            DateTime t2 = new DateTime(time.Ticks);
            int Secs = t2.Minute * 60 + t2.Second;

            int pos = (Secs * 100) / VoteSecs;

            _Controller.OnVoteProcess();

            progressBarVoting.Position = pos;
            if (t.Minute == 0 && t.Second == 0)
            {
                _Controller.Vote_Stop();
            }
        }



        private void tmrRotation_Tick(object sender, EventArgs e)
        {
            _CurrentImageNum++;
            if (_CurrentImageNum > TimerImages.Images.Count - 1)
                _CurrentImageNum = 0;

            picVoteTime.Image = TimerImages.Images[_CurrentImageNum];
        }


        private void btnRegCancel_Click(object sender, EventArgs e)
        {
            _Controller.Vote_Clear_Ask();
        }

        private void btnVoteFinish_Click(object sender, EventArgs e)
        {
            _Controller.Vote_Stop_Ask();
        }

        private void btnVoteCancel_Click(object sender, EventArgs e)
        {
            _Controller.Vote_Clear_Ask();
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _Controller.ShowMonitorPreview("VoteResults");
        }

        #endregion

    }
}