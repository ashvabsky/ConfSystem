﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.seats")]
    public class SeatObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        int fMicNum;
        public int MicNum
        {
            get { return fMicNum; }
            set { SetPropertyValue<int>("MicNum", ref fMicNum, value); }
        }

        int fRowNum;
        public int RowNum
        {
            get { return fRowNum; }
            set { SetPropertyValue<int>("RowNum", ref fRowNum, value); }
        }

        int fSeatNum;
        public int SeatNum
        {
            get { return fSeatNum; }
            set { SetPropertyValue<int>("SeatNum", ref fSeatNum, value); }
        }


        bool fPhysicalState;
        public bool PhysicalState
        {
            get { return fPhysicalState; }
            set { SetPropertyValue<bool>("PhysicalState", ref fPhysicalState, value); }
        }

        bool fLogicalState;
        public bool LogicalState
        {
            get { return fLogicalState; }
            set { SetPropertyValue<bool>("LogicalState", ref fLogicalState, value); }
        }
/*
        [Association("SeatObj-SesDelegateObj", typeof(SesDelegateObj)), Aggregated]
        public XPCollection<SesDelegateObj> Delegates { get { return GetCollection<SesDelegateObj>("Delegates"); } }
*/

        public SeatObj(Session session) : base(session) { }
        public SeatObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

    }
}
