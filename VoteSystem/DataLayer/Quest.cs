using System;
using DevExpress.Xpo;
namespace VoteSystem
{

    public class questions : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fCaption;
        [Size(255)]
        public string Caption
        {
            get { return fCaption; }
            set { SetPropertyValue<string>("Caption", ref fCaption, value); }
        }
        DateTime fCrDate;
        public DateTime CrDate
        {
            get { return fCrDate; }
            set { SetPropertyValue<DateTime>("CrDate", ref fCrDate, value); }
        }
        envotetypes fidType;
        public envotetypes idType
        {
            get { return fidType; }
            set { SetPropertyValue<envotetypes>("idType", ref fidType, value); }
        }
        envotekinds fidKind;
        public envotekinds idKind
        {
            get { return fidKind; }
            set { SetPropertyValue<envotekinds>("idKind", ref fidKind, value); }
        }

        public questions(Session session) : base(session) { }
        public questions() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class envotekinds : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fName;
        [Size(255)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        public envotekinds(Session session) : base(session) { }
        public envotekinds() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class envotetypes : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fName;
        [Size(255)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        public envotetypes(Session session) : base(session) { }
        public envotetypes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
