using System;
using DevExpress.Xpo;
namespace VoteSystem
{
    [Persistent("votesystem.sessions")]
    public class SessionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fCode;
        [Size(50)]
        public string Code
        {
            get { return fCode; }
            set { SetPropertyValue<string>("Code", ref fCode, value); }
        }
        string fCaption;
        [Size(255)]
        public string Caption
        {
            get { return fCaption; }
            set { SetPropertyValue<string>("Caption", ref fCaption, value); }
        }
        string fContent;
        public string Content
        {
            get { return fContent; }
            set { SetPropertyValue<string>("Content", ref fContent, value); }
        }
        DateTime fStartDate;
        public DateTime StartDate
        {
            get { return fStartDate; }
            set { SetPropertyValue<DateTime>("StartDate", ref fStartDate, value); }
        }

        DateTime fRegDate;
        public DateTime RegDate
        {
            get { return fRegDate; }
            set { SetPropertyValue<DateTime>("RegDate", ref fRegDate, value); }
        }

        int fRegQnty;
        public int RegQnty
        {
            get { return fRegQnty; }
            set { SetPropertyValue<int>("RegQnty", ref fRegQnty, value); }
        }

        DateTime fFinishDate;
        public DateTime FinishDate
        {
            get { return fFinishDate; }
            set { SetPropertyValue<DateTime>("FinishDate", ref fFinishDate, value); }
        }

        string fHallName;
        [Size(255)]
        public string HallName
        {
            get { return fHallName; }
            set { SetPropertyValue<string>("HallName", ref fHallName, value); }
        }

        bool fIsQuorum;
        public bool IsQuorum
        {
            get { return fIsQuorum; }
            set { SetPropertyValue<bool>("IsQuorum", ref fIsQuorum, value); }
        }

        bool fIsFinished;
        public bool IsFinished
        {
            get { return fIsFinished; }
            set { SetPropertyValue<bool>("IsFinished", ref fIsFinished, value); }
        }

        bool fIsDel;
        public bool IsDel
        {
            get { return fIsDel; }
            set { SetPropertyValue<bool>("IsDel", ref fIsDel, value); }
        }

        [NonPersistent]
        public bool IsActive
        {
            get
            {
                return !IsFinished;
            }
        }

        public SessionObj(Session session) : base(session) 
        {
            fCaption = "";
            fCode = "";
            fContent = "";
            fHallName = "";
            fIsFinished = false;
        }
        public SessionObj() : base(Session.DefaultSession) 
        {
            fCaption = "";
            fCode = "";
            fContent = "";
            fHallName = "";
            fIsFinished = false;
        }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
