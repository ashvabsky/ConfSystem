﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.settings")]
    public class SettingsObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        int fRegTime;
        public int RegTime
        {
            get { return fRegTime; }
            set { SetPropertyValue<int>("RegTime", ref fRegTime, value); }
        }

        bool fRegAfterTime;
        public bool RegAfterTime
        {
            get { return fRegAfterTime; }
            set { SetPropertyValue<bool>("RegAfterTime", ref fRegAfterTime, value); }
        }

        bool fIsCardMode;
        public bool IsCardMode
        {
            get { return fIsCardMode; }
            set { SetPropertyValue<bool>("IsCardMode", ref fIsCardMode, value); }
        }

        int fVoteTime;
        public int VoteTime
        {
            get { return fVoteTime; }
            set { SetPropertyValue<int>("VoteTime", ref fVoteTime, value); }
        }

        int fAssignedQnty;
        public int AssignedQnty
        {
            get { return fAssignedQnty; }
            set { SetPropertyValue<int>("AssignedQnty", ref fAssignedQnty, value); }
        }

        int fSelectedQnty;
        public int SelectedQnty
        {
            get { return fSelectedQnty; }
            set { SetPropertyValue<int>("SelectedQnty", ref fSelectedQnty, value); }
        }

        string fHallName;
        public string HallName
        {
            get { return fHallName; }
            set { SetPropertyValue<string>("HallName", ref fHallName, value); }
        }

        string fPath_Doc;
        public string Path_Doc
        {
            get { return fPath_Doc; }
            set { SetPropertyValue<string>("Path_Doc", ref fPath_Doc, value); }
        }

        string fPath_Excel;
        public string Path_Excel
        {
            get { return fPath_Excel; }
            set { SetPropertyValue<string>("Path_Excel", ref fPath_Excel, value); }
        }

        string fPath_DBase;
        public string Path_DBase
        {
            get { return fPath_DBase; }
            set { SetPropertyValue<string>("Path_DBase", ref fPath_DBase, value); }
        }

        string fNewConvocDocFile;
        public string NewConvocDocFile
        {
            get { return fNewConvocDocFile; }
            set { SetPropertyValue<string>("NewConvocDocFile", ref fNewConvocDocFile, value); }
        }

        string fLoadConvocDocFile;
        public string LoadConvocDocFile
        {
            get { return fLoadConvocDocFile; }
            set { SetPropertyValue<string>("LoadConvocDocFile", ref fLoadConvocDocFile, value); }
        }

        QntyTypeObj fidQuorumQnty;
        //      [Association("VoteKindObj-QuestObj")]
        public QntyTypeObj idQuorumQnty
        {
            get { return fidQuorumQnty; }
            set { SetPropertyValue<QntyTypeObj>("idQuorumQnty", ref fidQuorumQnty, value); }
        }


        bool fPred_IsAgenda;
        public bool Pred_IsAgenda
        {
            get { return fPred_IsAgenda; }
            set { SetPropertyValue<bool>("Pred_IsAgenda", ref fPred_IsAgenda, value); }
        }

        public SettingsObj()
            : base(Session.DefaultSession) 
        {
            fIsCardMode = true;
        }
    }


//--------------------------------------------------------

    [Persistent("votesystem.settings_report")]
    public class SettingsReportObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }


        string fTemplateFile;
        public string TemplateFile
        {
            get { return fTemplateFile; }
            set { SetPropertyValue<string>("TemplateFile", ref fTemplateFile, value); }
        }

        string fTemplateFile_Q;
        public string TemplateFile_Q
        {
            get { return fTemplateFile_Q; }
            set { SetPropertyValue<string>("TemplateFile_Q", ref fTemplateFile_Q, value); }
        }

        string fTemplateFile_T;
        public string TemplateFile_T
        {
            get { return fTemplateFile_T; }
            set { SetPropertyValue<string>("TemplateFile_T", ref fTemplateFile_T, value); }
        }

        string fTemplateFile_D;
        public string TemplateFile_D
        {
            get { return fTemplateFile_D; }
            set { SetPropertyValue<string>("TemplateFile_D", ref fTemplateFile_D, value); }
        }

        string fReportPath;
        public string ReportPath
        {
            get { return fReportPath; }
            set { SetPropertyValue<string>("ReportPath", ref fReportPath, value); }
        }

        bool fReport_ShowDetails;
        public bool Report_ShowDetails
        {
            get { return fReport_ShowDetails; }
            set { SetPropertyValue<bool>("Report_ShowDetails", ref fReport_ShowDetails, value); }
        }

        bool fReport_ShowDetails_T;
        public bool Report_ShowDetails_T
        {
            get { return fReport_ShowDetails_T; }
            set { SetPropertyValue<bool>("Report_ShowDetails_T", ref fReport_ShowDetails_T, value); }
        }

        bool fReport_ExcludedList;
        public bool Report_ExcludedList
        {
            get { return fReport_ExcludedList; }
            set { SetPropertyValue<bool>("Report_ExcludedList", ref fReport_ExcludedList, value); }
        }

        bool fReport_ShowNonVotes;
        public bool Report_ShowNonVotes
        {
            get { return fReport_ShowNonVotes; }
            set { SetPropertyValue<bool>("Report_ShowNonVotes", ref fReport_ShowNonVotes, value); }
        }

        bool fReport_ShowNonSpeeches;
        public bool Report_ShowNonSpeeches
        {
            get { return fReport_ShowNonSpeeches; }
            set { SetPropertyValue<bool>("Report_ShowNonSpeeches", ref fReport_ShowNonSpeeches, value); }
        }

        public SettingsReportObj()
            : base(Session.DefaultSession)
        {
        }
    }


    [Persistent("votesystem.enreportkinds")]
    public class ReportKindObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }


        string fTemplateFile;
        public string TemplateFile
        {
            get { return fTemplateFile; }
            set { SetPropertyValue<string>("TemplateFile", ref fTemplateFile, value); }
        }


        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        string fName;
        [Size(255)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        public ReportKindObj()
            : base(Session.DefaultSession)
        {
        }

    }
}
