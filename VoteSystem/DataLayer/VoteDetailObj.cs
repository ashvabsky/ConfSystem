﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using VoteSystem;

namespace VoteSystem
{
    [Persistent("votesystem.enanswertypes")]
    public class AnswerTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        VoteTypeObj fidVoteType;
        public VoteTypeObj idVoteType
        {
            get { return fidVoteType; }
            set { SetPropertyValue<VoteTypeObj>("idVoteType", ref fidVoteType, value); }
        }

        public AnswerTypeObj(Session session) : base(session) { }
        public AnswerTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

    }


    [Persistent("votesystem.votedetails")]
    public class VoteDetailObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        QuestionObj fidQuestion;
        public QuestionObj idQuestion
        {
            get { return fidQuestion; }
            set { SetPropertyValue<QuestionObj>("idQuestion", ref fidQuestion, value); }
        }

        SesDelegateObj fidDelegate;
        public SesDelegateObj idDelegate
        {
            get { return fidDelegate; }
            set { SetPropertyValue<SesDelegateObj>("idDelegate", ref fidDelegate, value); }
        }

        AnswerTypeObj fidAnswer;
        public AnswerTypeObj idAnswer
        {
            get { return fidAnswer; }
            set { SetPropertyValue<AnswerTypeObj>("idAnswer", ref fidAnswer, value); }
        }

        SeatObj fidSeat;
        public SeatObj idSeat
        {
            get { return fidSeat; }
            set { SetPropertyValue<SeatObj>("idSeat", ref fidSeat, value); }
        }

        public VoteDetailObj(Session session) : base(session) { }
        public VoteDetailObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

    }
}
