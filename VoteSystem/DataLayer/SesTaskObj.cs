﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.sestasks")]
    public class SesTaskObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fCaption;
        [Size(1024)]
        public string Caption
        {
            get { return fCaption; }
            set { SetPropertyValue<string>("Caption", ref fCaption, value); }
        }

        string fDescription;
        [Size(1024)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }

        string fItemNum;
        [Size(20)]
        public string ItemNum
        {
            get { return fItemNum; }
            set { SetPropertyValue<string>("ItemNum", ref fItemNum, value); }
        }

        SessionObj fidSession;
        public SessionObj idSession
        {
            get { return fidSession; }
            set { SetPropertyValue<SessionObj>("idSession", ref fidSession, value); }
        }

        TaskKindObj fidKind;
//      [Association("VoteKindObj-QuestObj")]
        public TaskKindObj idKind
        {
            get { return fidKind; }
            set { SetPropertyValue<TaskKindObj>("idKind", ref fidKind, value); }
        }

        DateTime fCrDate;
        public DateTime CrDate
        {
            get { return fCrDate; }
            set { SetPropertyValue<DateTime>("CrDate", ref fCrDate, value); }
        }

        int fQueueNum;
        public int QueueNum
        {
            get { return fQueueNum; }
            set { SetPropertyValue<int>("QueueNum", ref fQueueNum, value); }
        }
/*
        int fReadNum;
        public int ReadNum
        {
            get { return fReadNum; }
            set { SetPropertyValue<int>("ReadNum", ref fReadNum, value); }
        }
*/
/*
        int fidBaseTask;
        //      [Association("VoteKindObj-QuestObj")]
        public int idBaseTask
        {
            get { return fidBaseTask; }
            set { SetPropertyValue<int>("idBaseTask", ref fidBaseTask, value); }
        }
*/
        int fState;
        public int State
        {
            get { return fState; }
            set { SetPropertyValue<int>("State", ref fState, value); }
        }

        bool fIsDel;
        public bool IsDel
        {
            get { return fIsDel; }
            set { SetPropertyValue<bool>("IsDel", ref fIsDel, value); }
        }

        bool fIsHot;
        public bool IsHot
        {
            get { return fIsHot; }
            set { SetPropertyValue<bool>("IsHot", ref fIsHot, value); }
        }

        [NonPersistent]
        public string VoteStatus
        {
            get
            {
                string res;
                if (fState >= 1 && fState < 3)
                {
                    res = " чт. " + fState.ToString();
                }
                else if (fState == 0)
                    res = " ";
                else if (fState == 3)
                    res = "завершено";
                else 
                    res = "идет голосование";

                return res;
            }
        }

        [NonPersistent]
        public string SortCriteria // Поле используется для правильной сортировки в окне создания вопросов для голосования
        {
            get
            {
                string res;
                if (fState >= 1 && fState < 3)
                {
                    res = " " + fState.ToString();
                }
                else if (fState == 0)
                    res = " ";
                else if (fState == 3)
                    res = "завершено";
                else
                    res = "";

                res = res + " - " + (fQueueNum < 10 ? "0" : "") + fQueueNum.ToString();

                return res;
            }
        }

        [NonPersistent]
        public string CaptionItem
        {
            get
            {
//                return fItemNum + " " + "\"" + fCaption + "\"";
                if (fItemNum != null && fItemNum.Equals("") != true)
                    return "(п. " + fItemNum + ") " + fCaption;
                else
                    return fCaption;

            }
        }

        public string ItemNumStr
        {
            get
            {
                //                return fItemNum + " " + "\"" + fCaption + "\"";
                if (fItemNum != null && fItemNum.Equals("") != true)
                    return "(п. " + fItemNum + ") ";

                return "";
            }
        }


        [NonPersistent]
        public string EmptyValue
        {
            get
            {
                return " ";

            }
        }

        [NonPersistent]
        public bool TaskState
        {
            get
            {
                if (fState >= 1)
                    return true;
                else 
                    return false;
            }
        }



        public SesTaskObj(Session session) : base(session) 
        {
            this.fCrDate = DateTime.Now;
            this.fCaption = "";
            if (DataLayer.DataModel.QuestKinds != null)
                this.fidKind = DataLayer.DataModel.QuestKinds[1];
            this.fDescription = "";
        }
        public SesTaskObj() : base(Session.DefaultSession) 
        {
            this.fCrDate = DateTime.Now;
            this.fCaption = "";
            if (DataLayer.DataModel.QuestKinds != null)
                this.fidKind = DataLayer.DataModel.QuestKinds[1];
            this.fDescription = "";
            this.ItemNum = "";
        }

        public SesTaskObj(QuestionObj q) : base(Session.DefaultSession) 
        {
            this.Caption = q.Name;
            this.fCrDate = DateTime.Now;
            this.Description = q.idTask.Description;
//          this.idBaseTask = q.id;
            this.idKind = q.idKind;
            this.idSession = q.idSession;
            this.ItemNum = q.idTask.ItemNum;
        }

        public SesTaskObj(SesTaskObj t)
            : base(Session.DefaultSession)
        {
            this.Caption = t.Caption;
            this.fCrDate = DateTime.Now;
            this.Description = t.Description;
            this.idKind = t.idKind;
            this.idSession = t.idSession;
            this.ItemNum = t.ItemNum;
        }

        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.reftaskkinds")]
    public class TaskKindObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fCodeName;
        [Size(150)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        VoteKindObj fidVoteKind;
        //      [Association("VoteKindObj-QuestObj")]
        public VoteKindObj idVoteKind
        {
            get { return fidVoteKind; }
            set { SetPropertyValue<VoteKindObj>("idVoteKind", ref fidVoteKind, value); }
        }

        VoteTypeObj fidVoteType;
        //      [Association("VoteKindObj-QuestObj")]
        public VoteTypeObj idVoteType
        {
            get { return fidVoteType; }
            set { SetPropertyValue<VoteTypeObj>("idVoteType", ref fidVoteType, value); }
        }

        QntyTypeObj fidQntyType;
        //      [Association("VoteKindObj-QuestObj")]
        public QntyTypeObj idQntyType
        {
            get { return fidQntyType; }
            set { SetPropertyValue<QntyTypeObj>("idQntyType", ref fidQntyType, value); }
        }

        bool fIsDel;
        public bool IsDel
        {
            get { return fIsDel; }
            set { SetPropertyValue<bool>("IsDel", ref fIsDel, value); }
        }

        public TaskKindObj(Session session) : base(session) { }
        public TaskKindObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.envotetypes")]
    public class VoteTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(50)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fCodeName;
        [Size(150)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }
        
/*
        [Association("VoteTypeObj-QuestObj", typeof(VoteTypeObj)), Aggregated]
        public XPCollection<VoteTypeObj> Quests { get { return GetCollection<DelegateObj>("Quests"); } }
*/

        public VoteTypeObj(Session session) : base(session) { }
        public VoteTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.envotekinds")]
    public class VoteKindObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(50)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        /*
                [Association("VoteTypeObj-QuestObj", typeof(VoteTypeObj)), Aggregated]
                public XPCollection<VoteTypeObj> Quests { get { return GetCollection<DelegateObj>("Quests"); } }
        */


        string fCodeName;
        [Size(150)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        public VoteKindObj(Session session) : base(session) { }
        public VoteKindObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.refqntytypes")]
    public class QntyTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        int fProcentValue;
        public int ProcentValue
        {
            get { return fProcentValue; }
            set { SetPropertyValue<int>("ProcentValue", ref fProcentValue, value); }
        }

        DelegateQntyTypeObj fidDelegateQntyType;
        //      [Association("VoteKindObj-QuestObj")]
        public DelegateQntyTypeObj idDelegateQntyType
        {
            get { return fidDelegateQntyType; }
            set { SetPropertyValue<DelegateQntyTypeObj>("idDelegateQntyType", ref fidDelegateQntyType, value); }
        }

        [NonPersistent]
        public string Details
        {
            get
            {
                string s = ProcentValue.ToString() + "%";
                s = s + " от " + fidDelegateQntyType.Name;
                return s;
            }
        }

        public QntyTypeObj(Session session) : base(session) { }
        public QntyTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


    [Persistent("votesystem.endelegatesqntytype")]
    public class DelegateQntyTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        int fValueInt;
        public int ValueInt
        {
            get { return fValueInt; }
            set { SetPropertyValue<int>("ValueInt", ref fValueInt, value); }
        }

        public DelegateQntyTypeObj(Session session) : base(session) { }
        public DelegateQntyTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
