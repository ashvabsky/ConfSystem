﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace VoteSystem
{
    partial class SeatComponent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SeatComponent));
            this.picture = new DevExpress.XtraEditors.PictureEdit();
            this.persons = new DevExpress.Utils.ImageCollection(this.components);
            this.label = new System.Windows.Forms.TextBox();
            this.menuMicSeat = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picture.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.persons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuMicSeat)).BeginInit();
            this.SuspendLayout();
            // 
            // picture
            // 
            this.picture.AllowDrop = true;
            this.picture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picture.EditValue = global::VoteSystem.Properties.Resources.User;
            this.picture.Location = new System.Drawing.Point(0, 0);
            this.picture.Name = "picture";
            this.picture.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picture.Properties.Appearance.Options.UseBackColor = true;
            this.picture.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picture.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.picture.Properties.ShowMenu = false;
            this.picture.Size = new System.Drawing.Size(40, 40);
            this.picture.TabIndex = 49;
            this.picture.Tag = "";
            this.picture.DragDrop += new System.Windows.Forms.DragEventHandler(this.picture_DragDrop);
            this.picture.DragEnter += new System.Windows.Forms.DragEventHandler(this.Picture_DragEnter);
            this.picture.DragOver += new System.Windows.Forms.DragEventHandler(this.picture_DragOver);
            this.picture.DragLeave += new System.EventHandler(this.picture_DragLeave);
            this.picture.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picture_MouseDown);
            this.picture.MouseEnter += new System.EventHandler(this.picture_MouseEnter);
            // 
            // persons
            // 
            this.persons.ImageSize = new System.Drawing.Size(24, 24);
            this.persons.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("persons.ImageStream")));
            this.persons.Images.SetKeyName(7, "geen ball gray.png");
            // 
            // label
            // 
            this.label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label.Location = new System.Drawing.Point(-1, 22);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(42, 14);
            this.label.TabIndex = 52;
            this.label.Text = "1";
            this.label.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // menuMicSeat
            // 
            this.menuMicSeat.Name = "menuMicSeat";
            // 
            // SeatComponent
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.Controls.Add(this.label);
            this.Controls.Add(this.picture);
            this.Name = "SeatComponent";
            this.Size = new System.Drawing.Size(40, 40);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.SeatComponent_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.SeatComponent_DragEnter);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.SeatComponent_DragOver);
            this.DragLeave += new System.EventHandler(this.SeatComponent_DragLeave);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SeatComponent_MouseDown);
            this.MouseEnter += new System.EventHandler(this.SeatComponent_MouseEnter);
            ((System.ComponentModel.ISupportInitialize)(this.picture.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.persons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuMicSeat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.PictureEdit picture;
        private TextBox label;
        private DevExpress.XtraBars.PopupMenu menuMicSeat;
        private DevExpress.Utils.ImageCollection persons;

        [Category("Appearance")]
        [Description("Gets or sets the seat number")]
        public string SeatNumber
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        [Category("Appearance")]
        [Description("Gets or sets the seat image")]
        public DevExpress.XtraEditors.PictureEdit SeatPicture
        {
            get { return picture; }
            set { picture = value; }
        }

        [Category("Action")]
        [Description("Fires when the Drag start.")]
        public event System.Windows.Forms.DragEventHandler MyDragDrop;

        [Category("Action")]
        [Description("Fires when the Drag start.")]
        public event System.EventHandler MyMouseEnter;

        /*
                public delegate void DragEnterHandler();
                [Category("Action")]
                [Description("Fires when the Drag enter.")]
                public event DragEnterHandler MyDragEnter;
        */
    }
}
