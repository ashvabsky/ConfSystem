namespace VoteSystem
{
    partial class MonitorPreviewForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonitorPreviewForm2));
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit12 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit14 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit17 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit20 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit21 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit6 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit5 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit4 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit3 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit31 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit26 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit10 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit7 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit8 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit9 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.tmrForm = new System.Windows.Forms.Timer(this.components);
            this.memoEdit11 = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit14.Properties)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit17.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit21.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit26.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit11.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage5;
            this.xtraTabControl1.Size = new System.Drawing.Size(801, 540);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5});
            this.xtraTabControl1.Text = "xtraTabControl1";
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.panelControl3);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage5.Text = "������ �� �����������";
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl3.ContentImage = global::VoteSystem.Properties.Resources.ScreenBackground;
            this.panelControl3.Controls.Add(this.labelControl19);
            this.panelControl3.Controls.Add(this.labelControl12);
            this.panelControl3.Controls.Add(this.labelControl13);
            this.panelControl3.Controls.Add(this.memoEdit12);
            this.panelControl3.Controls.Add(this.labelControl14);
            this.panelControl3.Controls.Add(this.memoEdit14);
            this.panelControl3.Controls.Add(this.labelControl15);
            this.panelControl3.Controls.Add(this.labelControl16);
            this.panelControl3.Controls.Add(this.labelControl17);
            this.panelControl3.Controls.Add(this.labelControl18);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(792, 458);
            this.panelControl3.TabIndex = 13;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl19.Appearance.Options.UseBackColor = true;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Location = new System.Drawing.Point(61, 369);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(468, 19);
            this.labelControl19.TabIndex = 43;
            this.labelControl19.Text = "��� ���������� �� ������  - ������� ������ \"������\"";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(61, 334);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(571, 19);
            this.labelControl12.TabIndex = 42;
            this.labelControl12.Text = "��� ��������� � ������ �����������  - ������� ������ \"������\"";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Location = new System.Drawing.Point(61, 157);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(49, 19);
            this.labelControl13.TabIndex = 41;
            this.labelControl13.Text = "����:";
            // 
            // memoEdit12
            // 
            this.memoEdit12.EditValue = "���������� ��������������������� �������";
            this.memoEdit12.Location = new System.Drawing.Point(89, 179);
            this.memoEdit12.Name = "memoEdit12";
            this.memoEdit12.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit12.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit12.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.memoEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit12.Properties.Appearance.Options.UseFont = true;
            this.memoEdit12.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit12.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit12.Properties.ReadOnly = true;
            this.memoEdit12.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit12.Size = new System.Drawing.Size(619, 56);
            this.memoEdit12.TabIndex = 40;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Location = new System.Drawing.Point(171, 86);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(447, 25);
            this.labelControl14.TabIndex = 39;
            this.labelControl14.Text = "��������, ���� ������ �� �����������!";
            // 
            // memoEdit14
            // 
            this.memoEdit14.EditValue = "2 ���.";
            this.memoEdit14.Location = new System.Drawing.Point(267, 263);
            this.memoEdit14.Name = "memoEdit14";
            this.memoEdit14.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit14.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit14.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.memoEdit14.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit14.Properties.Appearance.Options.UseFont = true;
            this.memoEdit14.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit14.Properties.ReadOnly = true;
            this.memoEdit14.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit14.Size = new System.Drawing.Size(162, 23);
            this.memoEdit14.TabIndex = 32;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl15.Appearance.Options.UseBackColor = true;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(61, 264);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(192, 19);
            this.labelControl15.TabIndex = 31;
            this.labelControl15.Text = "����� �� �����������";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl16.Appearance.Options.UseBackColor = true;
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(434, 417);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(55, 23);
            this.labelControl16.TabIndex = 17;
            this.labelControl16.Text = "16:04";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl17.Appearance.Options.UseBackColor = true;
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Appearance.Options.UseForeColor = true;
            this.labelControl17.Location = new System.Drawing.Point(266, 417);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(136, 23);
            this.labelControl17.TabIndex = 16;
            this.labelControl17.Text = "16 ��� 2010�.";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl18.Appearance.Options.UseBackColor = true;
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Location = new System.Drawing.Point(156, 11);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(479, 23);
            this.labelControl18.TabIndex = 13;
            this.labelControl18.Text = "��������������� ����� ���������� ����������";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.panelControl5);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage1.Text = "�����������";
            // 
            // panelControl5
            // 
            this.panelControl5.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl5.Appearance.Options.UseBackColor = true;
            this.panelControl5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl5.ContentImage = global::VoteSystem.Properties.Resources.ScreenBackground;
            this.panelControl5.Controls.Add(this.memoEdit11);
            this.panelControl5.Controls.Add(this.memoEdit17);
            this.panelControl5.Controls.Add(this.labelControl26);
            this.panelControl5.Controls.Add(this.labelControl27);
            this.panelControl5.Controls.Add(this.labelControl29);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(792, 509);
            this.panelControl5.TabIndex = 10;
            // 
            // memoEdit17
            // 
            this.memoEdit17.EditValue = "������������� � �����������!";
            this.memoEdit17.Location = new System.Drawing.Point(204, 115);
            this.memoEdit17.Name = "memoEdit17";
            this.memoEdit17.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit17.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit17.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.memoEdit17.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit17.Properties.Appearance.Options.UseFont = true;
            this.memoEdit17.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit17.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit17.Properties.ReadOnly = true;
            this.memoEdit17.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit17.Size = new System.Drawing.Size(385, 23);
            this.memoEdit17.TabIndex = 35;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl26.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl26.Appearance.Options.UseBackColor = true;
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.Appearance.Options.UseForeColor = true;
            this.labelControl26.Location = new System.Drawing.Point(395, 429);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(37, 16);
            this.labelControl26.TabIndex = 17;
            this.labelControl26.Text = "16:04";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl27.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl27.Appearance.Options.UseBackColor = true;
            this.labelControl27.Appearance.Options.UseFont = true;
            this.labelControl27.Appearance.Options.UseForeColor = true;
            this.labelControl27.Location = new System.Drawing.Point(274, 429);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(92, 16);
            this.labelControl27.TabIndex = 16;
            this.labelControl27.Text = "16 ��� 2010�.";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl29.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl29.Appearance.Options.UseBackColor = true;
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.Appearance.Options.UseForeColor = true;
            this.labelControl29.Location = new System.Drawing.Point(183, 10);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(406, 19);
            this.labelControl29.TabIndex = 13;
            this.labelControl29.Text = "��������������� ����� ���������� ����������";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.panelControl6);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage2.Text = "�����������";
            // 
            // panelControl6
            // 
            this.panelControl6.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl6.Appearance.Options.UseBackColor = true;
            this.panelControl6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl6.ContentImage = global::VoteSystem.Properties.Resources.ScreenBackground;
            this.panelControl6.Controls.Add(this.memoEdit2);
            this.panelControl6.Controls.Add(this.labelControl1);
            this.panelControl6.Controls.Add(this.memoEdit1);
            this.panelControl6.Controls.Add(this.memoEdit20);
            this.panelControl6.Controls.Add(this.labelControl30);
            this.panelControl6.Controls.Add(this.memoEdit21);
            this.panelControl6.Controls.Add(this.labelControl31);
            this.panelControl6.Controls.Add(this.labelControl34);
            this.panelControl6.Controls.Add(this.labelControl35);
            this.panelControl6.Controls.Add(this.labelControl36);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(0, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(792, 509);
            this.panelControl6.TabIndex = 11;
            // 
            // memoEdit2
            // 
            this.memoEdit2.EditValue = "23 ��������";
            this.memoEdit2.Location = new System.Drawing.Point(316, 202);
            this.memoEdit2.Name = "memoEdit2";
            this.memoEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit2.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit2.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit2.Properties.Appearance.Options.UseFont = true;
            this.memoEdit2.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit2.Properties.ReadOnly = true;
            this.memoEdit2.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit2.Size = new System.Drawing.Size(275, 23);
            this.memoEdit2.TabIndex = 38;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(126, 202);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 16);
            this.labelControl1.TabIndex = 37;
            this.labelControl1.Text = "��������";
            // 
            // memoEdit1
            // 
            this.memoEdit1.EditValue = "���� �����������...";
            this.memoEdit1.Location = new System.Drawing.Point(245, 95);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.memoEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit1.Size = new System.Drawing.Size(385, 23);
            this.memoEdit1.TabIndex = 36;
            // 
            // memoEdit20
            // 
            this.memoEdit20.EditValue = "53 ���.";
            this.memoEdit20.Location = new System.Drawing.Point(316, 240);
            this.memoEdit20.Name = "memoEdit20";
            this.memoEdit20.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit20.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit20.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit20.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit20.Properties.Appearance.Options.UseFont = true;
            this.memoEdit20.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit20.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit20.Properties.ReadOnly = true;
            this.memoEdit20.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit20.Size = new System.Drawing.Size(275, 23);
            this.memoEdit20.TabIndex = 32;
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl30.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl30.Appearance.Options.UseBackColor = true;
            this.labelControl30.Appearance.Options.UseFont = true;
            this.labelControl30.Appearance.Options.UseForeColor = true;
            this.labelControl30.Location = new System.Drawing.Point(126, 240);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(123, 16);
            this.labelControl30.TabIndex = 31;
            this.labelControl30.Text = "�������� �������";
            // 
            // memoEdit21
            // 
            this.memoEdit21.EditValue = "27 ���������";
            this.memoEdit21.Location = new System.Drawing.Point(316, 170);
            this.memoEdit21.Name = "memoEdit21";
            this.memoEdit21.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit21.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit21.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit21.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit21.Properties.Appearance.Options.UseFont = true;
            this.memoEdit21.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit21.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit21.Properties.ReadOnly = true;
            this.memoEdit21.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit21.Size = new System.Drawing.Size(244, 23);
            this.memoEdit21.TabIndex = 30;
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl31.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl31.Appearance.Options.UseBackColor = true;
            this.labelControl31.Appearance.Options.UseFont = true;
            this.labelControl31.Appearance.Options.UseForeColor = true;
            this.labelControl31.Location = new System.Drawing.Point(126, 170);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(142, 16);
            this.labelControl31.TabIndex = 29;
            this.labelControl31.Text = "������������������:";
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl34.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl34.Appearance.Options.UseBackColor = true;
            this.labelControl34.Appearance.Options.UseFont = true;
            this.labelControl34.Appearance.Options.UseForeColor = true;
            this.labelControl34.Location = new System.Drawing.Point(395, 429);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(37, 16);
            this.labelControl34.TabIndex = 17;
            this.labelControl34.Text = "16:04";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl35.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl35.Appearance.Options.UseBackColor = true;
            this.labelControl35.Appearance.Options.UseFont = true;
            this.labelControl35.Appearance.Options.UseForeColor = true;
            this.labelControl35.Location = new System.Drawing.Point(274, 429);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(92, 16);
            this.labelControl35.TabIndex = 16;
            this.labelControl35.Text = "16 ��� 2010�.";
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl36.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl36.Appearance.Options.UseBackColor = true;
            this.labelControl36.Appearance.Options.UseFont = true;
            this.labelControl36.Appearance.Options.UseForeColor = true;
            this.labelControl36.Location = new System.Drawing.Point(183, 10);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(406, 19);
            this.labelControl36.TabIndex = 13;
            this.labelControl36.Text = "��������������� ����� ���������� ����������";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.panelControl7);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage3.Text = "����������";
            // 
            // panelControl7
            // 
            this.panelControl7.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl7.Appearance.Options.UseBackColor = true;
            this.panelControl7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl7.ContentImage = global::VoteSystem.Properties.Resources.ScreenBackground;
            this.panelControl7.Controls.Add(this.memoEdit6);
            this.panelControl7.Controls.Add(this.labelControl4);
            this.panelControl7.Controls.Add(this.memoEdit5);
            this.panelControl7.Controls.Add(this.labelControl3);
            this.panelControl7.Controls.Add(this.memoEdit4);
            this.panelControl7.Controls.Add(this.labelControl2);
            this.panelControl7.Controls.Add(this.memoEdit3);
            this.panelControl7.Controls.Add(this.memoEdit31);
            this.panelControl7.Controls.Add(this.memoEdit26);
            this.panelControl7.Controls.Add(this.labelControl37);
            this.panelControl7.Controls.Add(this.labelControl39);
            this.panelControl7.Controls.Add(this.labelControl40);
            this.panelControl7.Controls.Add(this.labelControl41);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(792, 509);
            this.panelControl7.TabIndex = 12;
            // 
            // memoEdit6
            // 
            this.memoEdit6.EditValue = "53";
            this.memoEdit6.Location = new System.Drawing.Point(383, 256);
            this.memoEdit6.Name = "memoEdit6";
            this.memoEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit6.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit6.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit6.Properties.Appearance.Options.UseFont = true;
            this.memoEdit6.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit6.Properties.ReadOnly = true;
            this.memoEdit6.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit6.Size = new System.Drawing.Size(244, 23);
            this.memoEdit6.TabIndex = 43;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(193, 256);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(157, 16);
            this.labelControl4.TabIndex = 42;
            this.labelControl4.Text = "���� ��� �����������:";
            // 
            // memoEdit5
            // 
            this.memoEdit5.EditValue = "23 ��������";
            this.memoEdit5.Location = new System.Drawing.Point(383, 211);
            this.memoEdit5.Name = "memoEdit5";
            this.memoEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit5.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit5.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit5.Properties.Appearance.Options.UseFont = true;
            this.memoEdit5.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit5.Properties.ReadOnly = true;
            this.memoEdit5.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit5.Size = new System.Drawing.Size(244, 23);
            this.memoEdit5.TabIndex = 41;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(193, 211);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(92, 16);
            this.labelControl3.TabIndex = 40;
            this.labelControl3.Text = "�����������:";
            // 
            // memoEdit4
            // 
            this.memoEdit4.EditValue = "53";
            this.memoEdit4.Location = new System.Drawing.Point(383, 168);
            this.memoEdit4.Name = "memoEdit4";
            this.memoEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit4.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit4.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit4.Properties.Appearance.Options.UseFont = true;
            this.memoEdit4.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit4.Properties.ReadOnly = true;
            this.memoEdit4.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit4.Size = new System.Drawing.Size(244, 23);
            this.memoEdit4.TabIndex = 39;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(193, 168);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(129, 16);
            this.labelControl2.TabIndex = 38;
            this.labelControl2.Text = "���������� ����:";
            // 
            // memoEdit3
            // 
            this.memoEdit3.EditValue = "���������� �����������:";
            this.memoEdit3.Location = new System.Drawing.Point(193, 61);
            this.memoEdit3.Name = "memoEdit3";
            this.memoEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit3.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit3.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.memoEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit3.Properties.Appearance.Options.UseFont = true;
            this.memoEdit3.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit3.Properties.ReadOnly = true;
            this.memoEdit3.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit3.Size = new System.Drawing.Size(385, 23);
            this.memoEdit3.TabIndex = 37;
            // 
            // memoEdit31
            // 
            this.memoEdit31.EditValue = "������ ����";
            this.memoEdit31.Location = new System.Drawing.Point(193, 329);
            this.memoEdit31.Name = "memoEdit31";
            this.memoEdit31.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit31.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit31.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit31.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit31.Properties.Appearance.Options.UseFont = true;
            this.memoEdit31.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit31.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit31.Properties.ReadOnly = true;
            this.memoEdit31.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit31.Size = new System.Drawing.Size(244, 23);
            this.memoEdit31.TabIndex = 35;
            // 
            // memoEdit26
            // 
            this.memoEdit26.EditValue = "27 ��������� �� 53";
            this.memoEdit26.Location = new System.Drawing.Point(383, 129);
            this.memoEdit26.Name = "memoEdit26";
            this.memoEdit26.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit26.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit26.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit26.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit26.Properties.Appearance.Options.UseFont = true;
            this.memoEdit26.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit26.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit26.Properties.ReadOnly = true;
            this.memoEdit26.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit26.Size = new System.Drawing.Size(244, 23);
            this.memoEdit26.TabIndex = 30;
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl37.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl37.Appearance.Options.UseBackColor = true;
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.Appearance.Options.UseForeColor = true;
            this.labelControl37.Location = new System.Drawing.Point(193, 129);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(142, 16);
            this.labelControl37.TabIndex = 29;
            this.labelControl37.Text = "������������������:";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl39.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl39.Appearance.Options.UseBackColor = true;
            this.labelControl39.Appearance.Options.UseFont = true;
            this.labelControl39.Appearance.Options.UseForeColor = true;
            this.labelControl39.Location = new System.Drawing.Point(395, 429);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(37, 16);
            this.labelControl39.TabIndex = 17;
            this.labelControl39.Text = "16:04";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl40.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl40.Appearance.Options.UseBackColor = true;
            this.labelControl40.Appearance.Options.UseFont = true;
            this.labelControl40.Appearance.Options.UseForeColor = true;
            this.labelControl40.Location = new System.Drawing.Point(274, 429);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(92, 16);
            this.labelControl40.TabIndex = 16;
            this.labelControl40.Text = "16 ��� 2010�.";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl41.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl41.Appearance.Options.UseBackColor = true;
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Appearance.Options.UseForeColor = true;
            this.labelControl41.Location = new System.Drawing.Point(183, 10);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(406, 19);
            this.labelControl41.TabIndex = 13;
            this.labelControl41.Text = "��������������� ����� ���������� ����������";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.panelControl1);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage4.Text = "�����������";
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl1.ContentImage = global::VoteSystem.Properties.Resources.ScreenBackground;
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.memoEdit10);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.memoEdit7);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.memoEdit8);
            this.panelControl1.Controls.Add(this.memoEdit9);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(792, 509);
            this.panelControl1.TabIndex = 12;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(60, 273);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(91, 19);
            this.labelControl7.TabIndex = 43;
            this.labelControl7.Text = "���������:";
            // 
            // memoEdit10
            // 
            this.memoEdit10.EditValue = "����������, ������ ���������, ���. � 16";
            this.memoEdit10.Location = new System.Drawing.Point(88, 295);
            this.memoEdit10.Name = "memoEdit10";
            this.memoEdit10.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit10.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit10.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.memoEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit10.Properties.Appearance.Options.UseFont = true;
            this.memoEdit10.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit10.Properties.ReadOnly = true;
            this.memoEdit10.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit10.Size = new System.Drawing.Size(550, 23);
            this.memoEdit10.TabIndex = 42;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(60, 180);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(49, 19);
            this.labelControl5.TabIndex = 41;
            this.labelControl5.Text = "����:";
            // 
            // memoEdit7
            // 
            this.memoEdit7.EditValue = "���������� ��������������������� �������";
            this.memoEdit7.Location = new System.Drawing.Point(88, 202);
            this.memoEdit7.Name = "memoEdit7";
            this.memoEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit7.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit7.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.memoEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit7.Properties.Appearance.Options.UseFont = true;
            this.memoEdit7.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit7.Properties.ReadOnly = true;
            this.memoEdit7.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit7.Size = new System.Drawing.Size(619, 56);
            this.memoEdit7.TabIndex = 40;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Location = new System.Drawing.Point(60, 63);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(95, 19);
            this.labelControl11.TabIndex = 39;
            this.labelControl11.Text = "���������:";
            // 
            // memoEdit8
            // 
            this.memoEdit8.EditValue = "�����������, ������ ������������, ��� � 7";
            this.memoEdit8.Location = new System.Drawing.Point(88, 85);
            this.memoEdit8.Name = "memoEdit8";
            this.memoEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit8.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit8.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.memoEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit8.Properties.Appearance.Options.UseFont = true;
            this.memoEdit8.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit8.Properties.ReadOnly = true;
            this.memoEdit8.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit8.Size = new System.Drawing.Size(550, 23);
            this.memoEdit8.TabIndex = 36;
            // 
            // memoEdit9
            // 
            this.memoEdit9.EditValue = "53 ���.";
            this.memoEdit9.Location = new System.Drawing.Point(240, 136);
            this.memoEdit9.Name = "memoEdit9";
            this.memoEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit9.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit9.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit9.Properties.Appearance.Options.UseFont = true;
            this.memoEdit9.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit9.Properties.ReadOnly = true;
            this.memoEdit9.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit9.Size = new System.Drawing.Size(162, 23);
            this.memoEdit9.TabIndex = 32;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Location = new System.Drawing.Point(60, 136);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(153, 19);
            this.labelControl6.TabIndex = 31;
            this.labelControl6.Text = "�������� �������";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(434, 417);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(55, 23);
            this.labelControl8.TabIndex = 17;
            this.labelControl8.Text = "16:04";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Location = new System.Drawing.Point(266, 417);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(136, 23);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "16 ��� 2010�.";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl10.Appearance.Options.UseBackColor = true;
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Location = new System.Drawing.Point(183, 10);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(479, 23);
            this.labelControl10.TabIndex = 13;
            this.labelControl10.Text = "��������������� ����� ���������� ����������";
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.btnClose);
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 485);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(801, 55);
            this.panelControl2.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ImageIndex = 1;
            this.btnClose.ImageList = this.ButtonImages;
            this.btnClose.Location = new System.Drawing.Point(649, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(145, 43);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "�������";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton1.ImageIndex = 0;
            this.simpleButton1.ImageList = this.ButtonImages;
            this.simpleButton1.Location = new System.Drawing.Point(6, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(162, 43);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "������� �� ��������";
            this.simpleButton1.Visible = false;
            // 
            // tmrForm
            // 
            this.tmrForm.Interval = 5000;
            this.tmrForm.Tick += new System.EventHandler(this.tmrForm_Tick);
            // 
            // memoEdit11
            // 
            this.memoEdit11.EditValue = "����� ������������ ���������� �������� ����� � �������.\r\n�� ����� ����������� ���" +
                "���� ���� ��� ������ ������ \"1\" �� ������.";
            this.memoEdit11.Location = new System.Drawing.Point(73, 177);
            this.memoEdit11.Name = "memoEdit11";
            this.memoEdit11.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit11.Properties.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.memoEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit11.Properties.Appearance.Options.UseFont = true;
            this.memoEdit11.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit11.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit11.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit11.Size = new System.Drawing.Size(601, 96);
            this.memoEdit11.TabIndex = 41;
            // 
            // MonitorPreviewForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 540);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.xtraTabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MonitorPreviewForm2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������������� �������� ��������� � ����";
            this.VisibleChanged += new System.EventHandler(this.MonitorPreviewForm2_VisibleChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MonitorPreviewForm2_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit14.Properties)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit17.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit21.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit26.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit11.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.MemoEdit memoEdit20;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.MemoEdit memoEdit21;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEdit26;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.MemoEdit memoEdit31;
        private DevExpress.XtraEditors.MemoEdit memoEdit17;
        private DevExpress.XtraEditors.MemoEdit memoEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.MemoEdit memoEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.MemoEdit memoEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.MemoEdit memoEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.MemoEdit memoEdit3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit memoEdit8;
        private DevExpress.XtraEditors.MemoEdit memoEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.MemoEdit memoEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEdit10;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.MemoEdit memoEdit12;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.MemoEdit memoEdit14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private System.Windows.Forms.Timer tmrForm;
        private DevExpress.XtraEditors.MemoEdit memoEdit11;
    }
}