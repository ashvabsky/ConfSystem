using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class MonitorPreviewForm : DevExpress.XtraEditors.XtraForm
    {
        int ShowTab = 0;
        public MonitorPreviewForm()
        {
            InitializeComponent();
        }

        private void MonitorPreviewForm_Load(object sender, EventArgs e)
        {
            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
        }

        public void ShowSession(SessionObj session)
        {
            ShowPage("xtraTabPage1");
            Show();
        }

        public void ShowDelegate(SesDelegateObj Delegate)
        {
            ShowPage("xtraTabPage2");
            Show();
        }

        public void ShowQuestion(SesTaskObj question)
        {
            ShowPage("xtraTabPage3");
            Show();
        }

        public void ShowQuestionResults(VoteResultObj vro)
        {
            ShowPage("xtraTabPage6");
            Show();
        }


        public void ShowPrepareToVote(QuestionObj question)
        {
            ShowPage("xtraTabPage4");
            Show();
        }

        public void ShowVoteStart(QuestionObj question)
        {
            ShowPage("xtraTabPage5");
            Show();
        }

        public void SetDelay(int delay)
        {
            tmrForm.Interval = delay;
        }

        public void ShowPage(string PageName)
        {
            xtraTabPage1.PageVisible = false;
            xtraTabPage2.PageVisible = false;
            xtraTabPage3.PageVisible = false;
            xtraTabPage4.PageVisible = false;
            xtraTabPage5.PageVisible = false;
            xtraTabPage6.PageVisible = false;

            if (PageName == "xtraTabPage1")
            {
                xtraTabPage1.PageVisible = true;
            }
            else if (PageName == "xtraTabPage2")
            {
                xtraTabPage2.PageVisible = true;
            }
            else if (PageName == "xtraTabPage3")
            {
                xtraTabPage3.PageVisible = true;
            }
            else if (PageName == "xtraTabPage4")
            {
                xtraTabPage4.PageVisible = true;
            }
            else if (PageName == "xtraTabPage5")
            {
                xtraTabPage5.PageVisible = true;
            }
            else if (PageName == "xtraTabPage6")
            {
                xtraTabPage6.PageVisible = true;
            }
        }

        private void MonitorPreviewForm_MouseClick(object sender, MouseEventArgs e)
        {
            Hide();
        }

        private void tmrForm_Tick(object sender, EventArgs e)
        {
            tmrForm.Stop();
            Hide();
        }

        private void MonitorPreviewForm_Shown(object sender, EventArgs e)
        {
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void MonitorPreviewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            tmrForm.Stop();
            e.Cancel = true;
            Hide();
        }

        private void MonitorPreviewForm_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                tmrForm.Start();
            }
            else
            {
                tmrForm.Stop();
            }
        }


    }
}