﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public class ResQuestionsCntrler
    {
        SessionObj _TheSession;
        IResultsPage _View;
        IQuestProperties _questSheetView;

        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        MainCntrler _mainController;

        S_QuestionObj _theQuestion;
        NestedUnitOfWork _nuow;

        XPCollection _Questions;
        XPCollection<VoteTypeObj> _VoteTypes;
        XPCollection<VoteKindObj> _VoteKinds;
        XPCollection<VoteQntyObj> _VoteQntyTypes;
        XPCollection<DecisionProcTypeObj> _DecisionProcTypes;
        XPCollection<QuotaProcTypeObj> _QuotaProcTypes;
        XPCollection<VoteResultObj> _VoteResults;

        private DevExpress.Xpo.XPCollection _ResultValTypes = new DevExpress.Xpo.XPCollection();

        public S_QuestionObj CurrentQuestion
        {
            get { return _theQuestion; }
        }

        public ResQuestionsCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IResultsPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
            _questSheetView = _View.GetQuestPropertiesInterface();
        }

        public void Init(ref XPCollection session_questions)
        {
            _TheSession = DataModel.TheSession;
            _Questions = session_questions;
            _VoteTypes = DataModel.VoteTypes;
            _VoteKinds = DataModel.VoteKinds;
            _VoteQntyTypes = DataModel.VoteQntyTypes;
            _DecisionProcTypes = DataModel.DecisionProcTypes;
            _QuotaProcTypes = DataModel.QuotaProcTypes;
            _VoteResults = DataModel.VoteResults;


            ((System.ComponentModel.ISupportInitialize)(_ResultValTypes)).BeginInit();
            _ResultValTypes.DeleteObjectOnRemove = true;
            _ResultValTypes.ObjectType = typeof(QuestResultType);
            _ResultValTypes.DisplayableProperties = @"This;id;Name;Value";
            ((System.ComponentModel.ISupportInitialize)(_ResultValTypes)).EndInit();

            _questSheetView.FillDropDowns();
        }

        public void InitResQuestionsPage()
        {
            _Questions.Reload();
            _VoteResults.Reload();
            _View.InitResQuestPage();

            _questSheetView.SetViewMode();
            UpdatePropData(_View.NavigatorPosition);

            _mainController.CurrentActivity = MainCntrler.Activities.None;
        }

        public void Save()
        {

        }

        public void UpdatePropData(int pos)
        {
            _theQuestion = null;

            if (pos < 0)
                return;

            if (pos >= _Questions.Count)
                return;

            _theQuestion = _Questions[pos] as S_QuestionObj;

            ShowRecProperties(_theQuestion);
            ShowResults();

            _mainController.CurrentActivity = MainCntrler.Activities.QuestionResults;
        }

        public void ShowRecProperties(S_QuestionObj _theQuestion)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties["Number"] = _theQuestion.id; 
            properties["Caption"] = _theQuestion.idQuestion.Caption;
            properties["CrDate"] = _theQuestion.idQuestion.CrDate;
            properties["Description"] = _theQuestion.Description;
            properties["Notes"] = _theQuestion.Notes;

            properties["VoteType_Value"] = _theQuestion.idVoteType.Name;
            properties["VoteType_Name"] = _theQuestion.idVoteType.Name;
            properties["VoteKind_Value"] = _theQuestion.idVoteKind.Name;
            properties["VoteKind_Name"] = _theQuestion.idVoteKind.Name;

            if (_theQuestion.ReadNum == 1)
                properties["ReadNum"] = "первое чтение";
            else if (_theQuestion.ReadNum == 2)
                properties["ReadNum"] = "второе чтение";
            else if (_theQuestion.ReadNum == 3)
                properties["ReadNum"] = "третье чтение";

            properties["VoteQntyType_Value"] = _theQuestion.idVoteQnty.Value;
            properties["VoteQntyType_Name"] = _theQuestion.idVoteQnty.Name;

            properties["DecisionProc_Value"] = _theQuestion.DecisionProcValue;
            properties["DecisionProcType_Name"] = _theQuestion.idDecisionProcType.Name;
            properties["DecisionProcType_Value"] = _theQuestion.idDecisionProcType.Value;

            properties["QuotaProc_Value"] = _theQuestion.QuotaProcentValue;
            properties["QuotaProcType_Name"] = _theQuestion.idQuotaProcentType.Name;
            properties["QuotaProcType_Value"] = _theQuestion.idQuotaProcentType.Value;

            _questSheetView.SetPropertiesQuestions(properties);
        }


        public void ShowResults()
        {
            CriteriaOperator criteria1 = CriteriaOperator.Parse("idSesQuestion == ?", _theQuestion.id);

            _VoteResults.Filter = criteria1;
            if (_VoteResults.Count == 0) 
                _VoteResults.Filter = null;

            VoteResultObj vres = _VoteResults[0] as VoteResultObj;

            _View.ShowVoteResults(vres);
            _VoteResults.Filter = null;
        }

        public void ResultCancel()
        {
            DialogResult dr = _msgForm.ShowQuestion("Вы намерены отменить итоги голосования по выбранному вопросу?");

            if (dr != DialogResult.Yes)
                return;

            CriteriaOperator criteria = CriteriaOperator.Parse("idSesQuestion.id = ?", _theQuestion.id);
            DataModel.VoteDetails.Filter = criteria;

            List<VoteDetailObj> delList = new List<VoteDetailObj>();
            foreach (VoteDetailObj vdo in DataModel.VoteDetails)
            {
                delList.Add(vdo);
            }

            foreach (VoteDetailObj vdo in delList)
            {
                DataModel.VoteDetails.Remove(vdo);
            }

            DataModel.VoteDetails.Filter = null;

            QuestResultType rt = _ResultValTypes[0] as QuestResultType;

           _theQuestion.idResultValue = rt;

            CriteriaOperator criteria1 = CriteriaOperator.Parse("idSesQuestion == ?", _theQuestion.id);

            _VoteResults.Filter = criteria1;
            if (_VoteResults.Count == 1)
            {
                VoteResultObj vres = _VoteResults[0] as VoteResultObj;
                _VoteResults.Filter = null;
                _VoteResults.Remove(vres);
            }

            _theQuestion.Save();
            _Questions.Reload();
            _VoteResults.Reload();
        }


        public void CreateNewReading()
        {
            DialogResult dr = _msgForm.ShowQuestion("Вы намерены создать новое чтение по выбранному вопросу?");
            if (dr != DialogResult.Yes)
                return;

            S_QuestionObj q = new S_QuestionObj(_theQuestion);
            QuestResultType rt = _ResultValTypes[0] as QuestResultType;

            q.idResultValue = rt;
            bool bRes = _mainController.AddNewVoteQuestion(q);
        }

        public void SetPosition(S_QuestionObj selQuestion)
        {
            _Questions.Reload();
            int index = _Questions.IndexOf(selQuestion);

            _View.SetNavigatorPos_ResQuestPage(index);
            UpdatePropData(_View.NavigatorPosition);
        }
    }
}
