using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class DelegateEditForm : DevExpress.XtraEditors.XtraForm
    {
        DelegateObj _theDelegate;
        public DelegateEditForm()
        {
            InitializeComponent();
        }
        public DelegateEditForm(DelegateObj theDelegate)
        {
            InitializeComponent();
//            this.DataBindings.Add("Delegate", theDelegate, ""); 
            _theDelegate = theDelegate;
        }

        private void DelegateEditForm_Load(object sender, EventArgs e)
        {
            txtLastName.DataBindings.Add("Text", _theDelegate, "LastName");
        }

    }
}