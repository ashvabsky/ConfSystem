﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;

namespace VoteSystem
{
    /*
     * Контроллер предназначен для управления списком вопросов для голосования
     * на страничке главного окна MainForm.SelQuestionsPage (далее, основная страничка)
     */
    public class SelStatementCntrler
    {
#region private секция данных

        SessionObj _TheSession;         // данные о текущей сессии
        ISelStatementsPage _View;        // интерфейс для управления основной страничкой 

        MainCntrler _mainController;    // главный контроллер
        IMsgMethods _msgForm;           // интерфейс, для вывода сообщений на форму

        StatementObj _theStatement;     // текущий (выбранный или выделенный в данный момент) вопрос из списка

        XPCollection<StatementObj> _Statements;        // вопросы сессии 
        NestedUnitOfWork _nuow;


#endregion private секция данных

#region основные методы
        public SelStatementCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(ISelStatementsPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }

        // первоначальная инициализация
        public void Init(ref XPCollection<StatementObj> session_statements)
        {
            _TheSession = DataModel.TheSession;
            session_statements.Criteria = DataModel.SesStatements.Criteria;

            _Statements = session_statements;

            _View.Init();
        }

        // инициализация странички при ее активации
        public void InitPage()
        {
            _Statements.Reload();

            _View.InitControls(ref _Statements);

            UpdatePropData();
        }

        public void StartTheme()
        {
            _Statements.Reload();
            bool IsFound = false;
            int maxid = 0;
            StatementObj currSt = null;
            foreach (StatementObj st in _Statements)
            {
                if (st.State == 2) // оппа, есть открытая темка - откроем ее
                {
                    IsFound = true;
                    currSt = st;
                    break;
                }
                else if (st.State == 0)
                {
                    if (maxid < st.id)
                    {
                        maxid = st.id;
                        currSt = st;
                    }
                }
            }

            if (currSt != null)
            {
                int currPos = _Statements.IndexOf(currSt);
                _View.NavigatorPosition = currPos;
                SpeechModeRun(false);
            }
        }

#endregion основные методы

#region public methods, which is called from the view

        // вывести информацию о текущем вопросе в окно свойств
        public void UpdatePropData()
        {
            int pos = _View.NavigatorPosition;

            _theStatement = null;

            if (pos < 0)
                return;

            if (pos >= _Statements.Count)
                return;

            _theStatement = _Statements[pos] as StatementObj;

//          _mainController.CurrentActivity = MainCntrler.Activities.SelectQuestion;

            bool IsFinished = _theStatement.IsFinished;
            _View.EnableButtons(IsFinished);
        }

        // пользователь пытается удалить текущий вопрос
        public void RemoveCurrentItem()
        {
            if (_theStatement == null)
                return;

            RemoveItem(ref _theStatement);
            UpdatePropData();
        }

        public void AppendItem()
        {
            int idMax =  _mainController.GetMaxID("SesTasks");

            int new_index = -1;

            Session ses = _Statements.Session;
/*
            SesTaskObj newTask = new SesTaskObj(ses);

            newTask.idSession = DataModel.Sessions.Lookup(_TheSession.id);
            newTask.id = idMax + 1;
            newTask.Caption = "Новая тема";


            newTask.QueueNum = 0;
            newTask.IsHot = true;
            newTask.idKind = DataModel.TaskKinds_GetByCodeName("Theme");
*/
            SesTaskObj newTask = DataModel.SesTasks_GetEmptyTask();

            StatementObj newItem = new StatementObj(Session.DefaultSession);
            newItem.QueueNum = GetMaxQueue() + 1;
            newItem.id = _mainController.GetMaxID("Statements") + 1;
            newItem.idSession = DataModel.Sessions.Lookup(_TheSession.id);
            newItem.Caption = "Новая тема";
            newItem.idTask = newTask;

            ThemeEditForm d = new ThemeEditForm(newItem);
            DialogResult dr = d.ShowDialog(_View.GetView());

            if (dr == DialogResult.OK)
            {
                _Statements.Add(newItem);
                newItem.Save();

                // добавляем выступающего с трибуны
                
                int idDelegateStatementsMax = _mainController.GetMaxID("DelegateStatements");

                DelegateStatementObj dso = new DelegateStatementObj(Session.DefaultSession);
                DelegateObj d_tribune = DataModel.GetTribuneDelegate();
                dso.id = idDelegateStatementsMax + 1;
                dso.idDelegate = d_tribune;
                dso.idStatement = newItem;
                dso.idSeat = d_tribune.idSeat;
                dso.QNum = 1;
                dso.State = 1;
                DataModel.StatemDetails.Add(dso);
                dso.Save();

                SetPosition(newItem);

                SpeechModeRun(true);
            }
        }

        // пользователь пытается отредактировать текущий вопрос
        public void EditCurrRecord()
        {
            int Pos = _View.NavigatorPosition;
            EditRecord(Pos);
        }

/*
        public void ShowResult()
        {

//          QuestionObj quest = new QuestionObj(_theQuestion);
            StatementObj st = DataModel.GetFinishedStatement(_theStatement);

            if (st.id != 0)
                _mainController.StatementDetailsRun(st);
        }
*/

        // пользователь пытается изменить номер очередности вопроса
        public void ChangeQueuePos(string command)
        {

            int Pos = _View.NavigatorPosition;

            _theStatement = null;

            if (Pos <= -1)
                return;

//           _nuow = _Statements.Session.BeginNestedUnitOfWork();

//            _theQuestion = _nuow.GetNestedObject(_Questions[Pos]) as SesQuestionObj;

            _theStatement = (StatementObj)_Statements[Pos];

            int newQNum = 0;
            int step = 0;

            int minQueueNum = 0;// GetMinQueue(-1);

            if (command == "Queue_Up")
            {
                if (_theStatement.QueueNum > minQueueNum + 1)
                {
                    newQNum = _theStatement.QueueNum - 1;
                    step = 1;
                }
            }
            else if (command == "Queue_Down")
            {
                newQNum = _theStatement.QueueNum + 1;
                step = -1;
            }

            if (newQNum > 0 && newQNum <= GetMaxQueue())
            {

                foreach (StatementObj st in _Statements)
                {
                    if (st.State == 1)
                        continue;
                    if (st.QueueNum == newQNum)
                    {
                        st.QueueNum = st.QueueNum + step;
                        st.Save();
                    }
                }

                _theStatement.QueueNum = newQNum;
                _theStatement.Save();
                _Statements.Reload();
                _View.SelectPos(_theStatement.id);
            }
         }

        // запустить режим голосования по вопросу
        public void SpeechModeRun(bool forceSpeechMode)
        {
//            StatementObj st = DataModel.GetFinishedStatement(_theStatement);

            if (_theStatement == null)
                AppendItem();

            if (_theStatement == null)
                return;

            if (_theStatement.id != 0)
            {
                if (_theStatement.State == 1 && forceSpeechMode == false)
                    _mainController.StatementDetailsRun(_theStatement, true);
                else
                    _mainController.StatementDetailsRun(_theStatement, false);

            }
/*
            else
                _mainController.StatementDetailsRun(CurrentStatement);
*/
            _Statements.Reload();
        }

        public void ViewModeRun()
        {
            if (_theStatement.id != 0)
                _mainController.StatementDetailsRun(_theStatement, true);

            _Statements.Reload();
        }

        public void refresh()
        {
            Session newSes = new Session();
            XPCollection<StatementObj> statements = new XPCollection<StatementObj>(newSes);
            statements.Criteria = _Statements.Criteria;
            statements.Load();

            foreach(StatementObj statemRefresh in statements) 
            {
                StatementObj statemExst = (StatementObj)_Statements.Lookup(statemRefresh.id);
                if (statemExst != null && statemExst.idTask.State != statemRefresh.idTask.State)
                    statemExst.idTask.State = statemRefresh.idTask.State;
            }
            _Statements.Reload();
        }

#endregion public methods, which is called from the view


#region public methods
        public StatementObj CurrentStatement
        {
            get { return _theStatement; }
        }


        // программно выбрать вопрос из списка и отобразить его свойства
        public void SetCurrentPos(int currPos)
        {
            _View.NavigatorPosition = currPos;
            UpdatePropData();
        }

        // найти заданный вопрос в списке и выделить его
        public void SetPosition(StatementObj selStatement)
        {
            _Statements.Reload();
            int currPos = _Statements.IndexOf(selStatement);

            _View.NavigatorPosition = currPos;
        }


        // найти заданный вопрос в списке и поместить его в список проголосованных
        public void MoveQuestion(StatementObj resStatement)
        {
/*
            int vQNum = GetMinQueue(resStatement.idQuestion.id); // max voted Queue Num, excluding resQuestion.idQuestion.id

            int nQNum = resQuestion.idQuestion.QueueNum; // not voted start Queue Num 
            resQuestion.idQuestion.QueueNum = vQNum + 1;
            resQuestion.idQuestion.Save();

            foreach (SesQuestionObj eachQ in DataModel.SesQuestions)
            {
                if (eachQ.IsVoted == false)
                {
                    if (eachQ.QueueNum < nQNum)
                    {
                        eachQ.QueueNum++;
                        eachQ.Save();
                    }
                }
            }
*/
        }

        public StatementObj findOpenStatement()
        {
            foreach (StatementObj statem in _Statements)
            {
                if (statem.State == 2)
                    return statem;
            }

            return null;
        }

#endregion  public methods

#region private methods


        // просто удаляет вопрос из списка
        private void RemoveItem(ref StatementObj q)
        {
            foreach (StatementObj eachS in _Statements)
            {
                if (eachS.QueueNum > q.QueueNum && eachS.State != 1)
                {
                    eachS.QueueNum--;
                    eachS.Save();
                }
            }

//          _Statements.Remove(q);
            if (q.idTask.IsHot == true)
            {
                q.idTask.IsDel = true;
                q.idTask.Save();
            }
            q.IsDel = true;
            q.Save();

            _Statements.Reload();
        }

        // подготавливает выбранный вопрос для редактирования через панель свойств
        private void EditRecord(int pos)
        {
            _theStatement = null;

            if (pos <= -1)
                return;

            _nuow = _Statements.Session.BeginNestedUnitOfWork();

            _theStatement = _nuow.GetNestedObject(_Statements[pos]) as StatementObj;

            ThemeEditForm tedt = new ThemeEditForm(_theStatement);
            if (_theStatement.State == 1)
                tedt._readMode = true;

            tedt.ShowDialog();
        }

        // получить максимальный идентификатор вопроса из списка
        private int GetMaxID()
        {

            int idMax = _mainController.GetMaxID("Statements");

/*
            List<int> arr_Max = new List<int>();
            foreach (SesQuestionObj o in DataModel.SesQuestions)
            {
                arr_Max.Add(o.id);
            }

            if (arr_Max.Count > 0)
                idMax = arr_Max.Max();

            return idMax;
 */
             return idMax;
        }

        // получить максимальный идентификатор вопроса из списка
        private int GetMaxQueue()
        {
            List<int> arr_Max = new List<int>();
            foreach (StatementObj o in _Statements)
            {
                   arr_Max.Add(o.QueueNum);
            }

            int Max = 0;
            if (arr_Max.Count > 0)
                Max = arr_Max.Max();

            return Max;
        }

        // получить минимальный идентификатор вопроса из списка
        private int GetMinQueue(int excludedID)
        {

            int minQNum = 0; // max voted Queue Num 

            foreach (StatementObj eachQ in _Statements)
            {
                if (eachQ.IsFinished && eachQ.id != excludedID)
                {
                    if (minQNum < eachQ.QueueNum)
                        minQNum = eachQ.QueueNum;
                }
            }

            return minQNum;
        }

#endregion private methods

    }
}
 