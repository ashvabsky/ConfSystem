﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    /*
     * Контроллер предназначен для управления списком вопросов с результатами голосования
     * на страничке главного окна MainForm.ResultsPage (далее, основная страничка)
     */
    public class VoteQuestionsCntrler
    {
#region private секция данных

        SessionObj _TheSession;  // данные о текущей сессии
        IVoteQuestsPage _View; // интерфейс для управления основной страничкой 
//      IQuestProperties _questSheetView; // интерфейс для управления панелью свойств на форме

        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        MainCntrler _mainController; // главный контроллер

        QuestionObj _theQuestion; // текущий (выбранный или выделенный в данный момент) вопрос из списка
        NestedUnitOfWork _nuow;
        SesTaskObj _currTask; // текущая задача, поля которой заполняются вверху

        XPCollection<QuestionObj> _Questions; // список вопросов с результатами 
        XPCollection<VoteTypeObj> _VoteTypes; // типы голосваний
        XPCollection<VoteKindObj> _VoteKinds; // виды голосваний
/*
        XPCollection<VoteQntyObj> _VoteQntyTypes; // 
        XPCollection<DecisionProcTypeObj> _DecisionProcTypes;
        XPCollection<QuotaProcTypeObj> _QuotaProcTypes;
 */ 
        XPCollection<VoteResultObj> _VoteResults; // список результатов голосований

        HallCntrler _hall; // контроллер управления залом

//      XPCollection<QuestResultType> _ResultValTypes;

#endregion private секция данных

#region основные методы
        public VoteQuestionsCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IVoteQuestsPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
//          _questSheetView = _View.GetQuestPropertiesInterface();
        }

        // первоначальная инициализация
        public void Init()
        {
            _TheSession = DataModel.TheSession;
            _Questions = DataModel.Questions;
            _VoteTypes = DataModel.VoteTypes;
            _VoteKinds = DataModel.VoteKinds;
            _VoteResults = DataModel.VoteResults;

            _Questions.Load();
            _currTask = new SesTaskObj();

            _View.Init();

            _hall = (HallCntrler)_mainController.GetSubController("HallPage Controller");

        }

        // инициализация странички при ее активации
        public void InitPage()
        {
            _Questions.Reload();
            _VoteResults.Reload();
            _View.InitControls(ref _Questions, DataModel.QuestKinds, _currTask, ref DataModel.SesTasks, DataModel._ReadNumDeafault, DataModel._AmendmentTypeDeafault, DataModel.TheSettingsHall.IsAutoCardMode);

            UpdatePropData();

            _mainController.CurrentActivity = MainCntrler.Activities.None;
        }

#endregion основные методы

#region public methods, which is called from the view

        public QuestionObj CurrQuestion
        {
            get { return _theQuestion; }
        }
        public SesTaskObj CurrTask
        {
            get { return _currTask; }
        }

        public void ReInitCurrTask()
        {
            SesTaskObj newtask = new SesTaskObj();
            _currTask = newtask;

            _currTask.Caption = "";
        }
        // вывести информацию о текущем вопросе в окно свойств
        public void UpdatePropData()
        {
            int pos = _View.NavigatorPosition;
            _theQuestion = null;

            if (pos < 0)
                return;

            if (pos >= _Questions.Count)
                return;

            _theQuestion = _Questions[pos] as QuestionObj;

            ShowResults();

            bool IsVoted = false;
            if (_theQuestion.idResult.IsResult == true)
                IsVoted = true;

            _View.EnableButtons(IsVoted);

        }

        // попытка создать новое чтение существующего вопроса
        public void CreateNewReading()
        {
/*
            if (_theQuestion.idKind.CodeName != "Law")
            {
                DialogResult dr = _msgForm.ShowQuestion("Выбранный вопрос не является \"законом\". Вы попрежнему намерены создать новое чтение для этого вопроса?");
                if (dr != DialogResult.Yes)
                    return;

            }

            else
            {
                DialogResult dr = _msgForm.ShowQuestion("Вы намерены создать новое чтение по выбранному вопросу?");
                if (dr != DialogResult.Yes)
                    return;
            }
*/
            SesTaskObj Task = _theQuestion.idTask;

//          string newcaption = _theQuestion.idTask.Caption;
/*
            int ind = newcaption.IndexOf("Чтение №");
            if (ind >= 0)
            {
                newcaption = newcaption.Substring(0, ind);
            }
*/

//          newcaption = newcaption + " Чтение №" + newTask.ReadNum.ToString();

//          DataModel.SesTasks.Add(newTask);
//          newTask.Save();

//          ReadNumObj readnum = DataModel.GetNextReadNum(_theQuestion.idReadNum);

            _View.SelectTask(Task);
            _View.SetReadNum(_theQuestion.idReadNum);

/*
            if (bRes == false)
            {
                _msgForm.ShowWarningMsg("Не удалось создать новое чтение по данному вопросу.");
            }
 */ 
        }


        public void CreateQuestionFromCurrentTask(string txtVoteCaption, TaskKindObj taskkind, string Description, ReadNumObj readnum, AmendmentTypeObj amendtype, int amend)
        {
//          _currTask.idKind = taskkind;
            _currTask.Description = Description;

            CreateNewQuestion(ref _currTask, txtVoteCaption, taskkind, readnum, amendtype, amend);
//            ReInitCurrTask();
            _View.InitQFields(_currTask, DataModel._ReadNumDeafault, DataModel._AmendmentTypeDeafault);
        }

        private void CreateNewQuestion(ref SesTaskObj newTask)
        {
            CreateNewQuestion(ref newTask, null, null, null, null, 0);
        }

        private void CreateNewQuestion(ref SesTaskObj newTask, string txtVoteCaption, TaskKindObj taskkind, ReadNumObj readnum, AmendmentTypeObj amendtype, int amend)
        {
            if (newTask.id == 0)
            {
                newTask.id = _mainController.GetMaxID("SesTasks") + 1;
                newTask.idSession = DataModel.TheSession;
                if (txtVoteCaption != null)
                    newTask.Caption = txtVoteCaption;

                if (taskkind != null)
                    newTask.idKind = taskkind;

                newTask.IsHot = true;
                if (DataModel.SesTasks.Count > 0)
                    newTask.QueueNum = DataModel.SesTasks.Max<SesTaskObj>(v => v.QueueNum) + 1;
                else
                    newTask.QueueNum = 1;

                DataModel.SesTasks.Add(newTask);
                newTask.Save();
            }

            QuestionObj q = new QuestionObj(newTask);
            q.id = _mainController.GetMaxID("Questions") + 1;

            if (txtVoteCaption != null)
                q.Name = txtVoteCaption;

            if (taskkind != null)
            {
                q.idKind = taskkind;
            }

            if (readnum != null)
            {
                q.idReadNum = readnum;
                if (readnum.Name != "")
                    q.Name = q.Name + ". " + readnum.Name; 
            }

            if (amendtype != null)
            {
                q.idAmendmentType = amendtype;
            }
            q.Amendment = amend;

            if (amend != 0)
            {
                q.Name = q.Name + "." + q.AmendmentStr; 
            }

            q.idResult = new QuestionResultObj();
            q.idResult.id = _mainController.GetMaxID("questions_results") + 1;
            q.idResult.idResultValue = DataModel.ResValues_GetObject("NoResult");
            q.idResult.idResultValueInt = 0;
            q.idResult.idVoteResult = DataModel.VoteResultNull;
            _Questions.Add(q);
            q.QueueNum = GetMaxQueue() + 1;
            q.Save();
            q.Reload();
            _Questions.Reload();

            _View.UpdateControls();
            SetPosition(q);
        }

        public void EditCurrRecord()
        {
            _nuow = _Questions.Session.BeginNestedUnitOfWork();

            QuestionObj Q = _nuow.GetNestedObject(_theQuestion) as QuestionObj;

            QuestionEditForm qedt = new QuestionEditForm(Q);
            if (_theQuestion.idResult.IsResult == true)
                qedt._readMode = true;
            else
                qedt._readMode = false;

            qedt.ShowDialog();
        }

        // просмотреть подробные итоги голосования по выбранному вопросу
        public void VoteDetailsRun()
        {
            _mainController.VoteModeRun(CurrentQuestion, false);
        }

        // запустить режим голосования по вопросу
        public void VoteModeRun()
        {
            bool IdentifyCardsMode = DataModel.TheSettingsHall.IsAutoCardMode;
            _mainController.VoteModeRun(CurrentQuestion, IdentifyCardsMode);
        }
        public void VoteModeRunFast()
        {
            bool IdentifyCardsMode = DataModel.TheSettingsHall.IsAutoCardMode;
            _mainController.VoteModeRun(CurrentQuestion, IdentifyCardsMode, true);
        }

        // выбор вопроса из списка
        public void OnSelectNewPosition()
        {
            _mainController.ClearMonitor();
            UpdatePropData();
        }

        // пользователь пытается удалить текущий вопрос
        public void RemoveCurrentItem()
        {
            if (_theQuestion == null)
                return;

            if (_theQuestion.idResult.IsResult == false)
            {
                RemoveItem(ref _theQuestion);
                UpdatePropData();
            }
        }

        // добавляет вопрос из глобального списка в список для голосования
        public int AppendItem()
        {
            int idMax = _mainController.GetMaxID("SesTasks");

            if (idMax == -1)
                return -1;

            int new_index = -1;

            Session ses = _Questions.Session;
            TaskKindObj kind;
            if (_theQuestion != null)
            {
                //                ses = _theQuestion.Session;
                kind = _theQuestion.idTask.idKind;
                //              ses = Session.DefaultSession; //_theQuestion.Session;
            }
            else
            {
                kind = (TaskKindObj)DataModel.QuestKinds[1];
                //              ses = Session.DefaultSession;
            }

            SesTaskObj newTask = new SesTaskObj(ses);
            //            XPCollection<QuestKindObj> Kinds = new XPCollection<QuestKindObj>(_theQuestion.Session);

            //            QuestKindObj kind = (QuestKindObj)Kinds[1];

            newTask.idSession = DataModel.Sessions.Lookup(_TheSession.id);
            newTask.id = idMax + 1;
            newTask.idKind = DataModel.QuestKinds.Lookup(kind.id);
            newTask.Description = "";
            newTask.Caption = "";

            newTask.QueueNum = 0;//GetMaxQueue() + 1;
            newTask.IsHot = true;


            TaskEditForm d = new TaskEditForm(newTask);
            DialogResult dr = d.ShowDialog(_View.GetView());

            if (dr == DialogResult.OK)
            {
                DataModel.SesTasks.Add(newTask);
                newTask.Save();

                CreateNewQuestion(ref newTask);
            }

            return new_index;
        }


        // пользователь пытается изменить номер очередности вопроса
        public void ChangeQueuePos(string command)
        {
            int Pos = _View.NavigatorPosition;

            _theQuestion = null;

            if (Pos <= -1)
                return;


            _theQuestion = (QuestionObj)_Questions[Pos];

            int newQNum = 0;
            int step = 0;

            int minQueueNum = 0; //GetMinQueue(-1);

            if (command == "Queue_Up")
            {
                if (_theQuestion.QueueNum > minQueueNum + 1)
                {
                    newQNum = _theQuestion.QueueNum - 1;
                    step = 1;
                }
            }
            else if (command == "Queue_Down")
            {
                newQNum = _theQuestion.QueueNum + 1;
                step = -1;
            }

            if (newQNum > 0 && newQNum <= GetMaxQueue())
            {
                foreach (QuestionObj q in DataModel.Questions)
                {
                    if (q.idTask.State >= 1)
                        continue;
                    if (q.QueueNum == newQNum)
                    {
                        q.QueueNum = q.QueueNum + step;
                        q.Save();
                    }
                }

                _theQuestion.QueueNum = newQNum;
                _theQuestion.Save();
//              _View.SelectPos(_theQuestion.id);
                SetPosition(_theQuestion);
            }
        }


        public void OnSelectTask(int id) // вызывается, когда пользователь выбирает пункт повестки дня из выпадающего списка
        {
            _hall.SetTextOutAutoProcessPause();

            if (id > 0)
            {
                _currTask = DataModel.SesTasks.Lookup(id);
            }
            else
                ReInitCurrTask();

            if (_currTask != null)
                _View.InitQFields(_currTask, DataModel._ReadNumDeafault, DataModel._AmendmentTypeDeafault);
            else
                OnSelectTask(0);

        }

#endregion public methods, which is called from the view

#region public methods

        public QuestionObj CurrentQuestion
        {
            get { return _theQuestion; }
        }

        // находит указанные вопрос в списке и выделяет его как текущий
        public void SetPosition(QuestionObj Q)
        {
            _mainController.ClearMonitor();

            int index = _Questions.IndexOf(Q);

            _View.NavigatorPosition = index;
            UpdatePropData();
        }
#endregion  public methods

#region private methods


        // передает данные по итогам голосвания по текущему вопросу для отображения на форме
        private void ShowResults()
        {

            VoteResultObj vres = _theQuestion.idResult.idVoteResult;

            _View.ShowVoteResults(vres);
            _VoteResults.Filter = null;

            Platform.VoteInfoResult info = new Platform.VoteInfoResult();
            if (vres != null)
            {
                info.VotedQnty_A = vres.AnsQnty1;
                info.VotedQnty_B = vres.AnsQnty2;
                info.VotedQnty_C = vres.AnsQnty3;
                info.DelegateQnty = vres.AvailableQnty;
                bool isaccept = false;
                if (vres.idResultValue.CodeName == "Parlament_Accept")
                    isaccept = true;
                info.IsResult = isaccept;
            }
            _mainController._MonitorInfo = info;
            _mainController.CurrentActivity = MainCntrler.Activities.VoteResults;
        }

        // просто удаляет вопрос из списка
        private void RemoveItem(ref QuestionObj q)
        {
            if (q == null) return;

            SesTaskObj task = q.idTask;
            bool taskdel = false;
            int deletedQ = q.id;
            try
            {
                _Questions.Remove(q);
                taskdel = true;

            }
            catch
            {
                _Questions.Reload();
                QuestionObj q2 = _Questions.Lookup(deletedQ);
                if (q2 == null) return;
                q2.IsDel = true;
                q2.Save();
            }

            _Questions.Reload();

            if (task.IsHot == true && taskdel == true)
            {
                SelQuestionsCntrler _selquestionsCntrler = (SelQuestionsCntrler)_mainController.GetSubController("Select Questions Page Controller");
                _selquestionsCntrler.RemoveItem(ref task);
            }

/*
            foreach (SesTaskObj eachT in DataModel.SesTasks)
            {
                if (eachT.QueueNum > q.idTask.QueueNum)
                {
                    eachQ.idTask.QueueNum--;
                    eachQ.idTask.Save();
                }
            }
*/
        }

        // получить максимальный идентификатор вопроса из списка
        private int GetMaxQueue()
        {
            List<int> arr_Max = new List<int>();
            foreach (QuestionObj q in DataModel.Questions)
            {
                arr_Max.Add(q.QueueNum);
            }

            int Max = 0;
            if (arr_Max.Count > 0)
                Max = arr_Max.Max();

            return Max;
        }

        // получить минимальный идентификатор вопроса из списка
        private int GetMinQueue(int excludedID)
        {
            int minQNum = 0; // max voted Queue Num 
            foreach (QuestionObj eachQ in DataModel.Questions)
            {
                if (eachQ.idTask.State >= 1 && eachQ.id != excludedID)
                {
                    if (minQNum < eachQ.QueueNum)
                        minQNum = eachQ.QueueNum;
                }
            }

            return minQNum;
        }

#endregion private methods
    }
}
