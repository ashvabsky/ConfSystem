﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;

namespace VoteSystem
{
    public class QuestionsCntrler
    {
        public enum QuestsFormMode
        {
            Manage,
            Select
        }

        public class DataResults
        {
            int Total = 0;

        }

        QuestionObj _theQuestion;
        NestedUnitOfWork _nuow;

        IQuestionsForm _View;
        IResultShow _ViewResults; // интерфейс для вывода детальной информации по результатам
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        XPCollection _Questions;
        XPCollection _VoteKinds;
        XPCollection _VoteTypes;

        QuestsFormMode _Mode = QuestsFormMode.Manage;

        List<int> _filterArray;

        MainCntrler _mainController;

        public QuestsFormMode Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        public QuestionsCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IQuestionsForm viewInterface, IResultShow viewresInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
            _ViewResults = viewresInterface;

        }

        public void Init(ref XPCollection Questions, ref XPCollection VoteTypes, ref XPCollection VoteKinds)
        {
            _Questions = Questions;
            _VoteKinds = VoteKinds;
            _VoteTypes = VoteTypes;
        }


        public void DataPositionChanged(int pos)
        {
            _theQuestion = null;

            if (pos < 0)
                return;

            if (pos >= _Questions.Count)
                return;

            _theQuestion = _Questions[pos] as QuestionObj;

            _ViewResults.ShowVoteResults(_theQuestion.idResult.idVoteResult);

            bool IsVoted = false;
            if (_theQuestion.idResult.IsResult == true)
                IsVoted = true;

            _View.EnableButtons(IsVoted);

        }

        public void ShowSelQuestionDetails()
        {
            if (_theQuestion != null)
            {
                if (_theQuestion.idResult.IsResult == true)
                {
                    _mainController.VoteModeRun(_theQuestion, false);
                }
            }
        }

        public void FillQuestionList(ArrayList _filterArray)
        {
            CriteriaOperator cr = new InOperator("idSession.id", _filterArray.ToArray());
            _Questions.Filter = cr;
        }

        public QuestionObj GetSelectedItem(List<int> filterlist)
        {
//          _View = new QuestionsForm(this);

            List<int> list = new List<int>();
            foreach (QuestionObj o in _Questions)
            {
                list.Add(o.id);
            }

            _filterArray = new List<int>(list.Except<int>(filterlist).ToArray<int>());

            CriteriaOperator cr = new InOperator("id", _filterArray.ToArray());
            _Questions.Filter = cr;
            _Mode = QuestsFormMode.Select;

            DialogResult dr = _View.ShowView();
            if (dr == DialogResult.OK)
            {
                return _theQuestion;
            }

            return null;
        }

        public void ShowView()
        {
            _View.ShowView();
        }
    }

}
