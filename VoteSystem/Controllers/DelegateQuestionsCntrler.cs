﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public class DelegateQuestionsCntrler: IDisposable
    {


        VoteDetailObj _theQuestDetails;
        DelegateObj _theDelegate;
        SessionObj _theSession;
        NestedUnitOfWork _nuow;

        IDelegateQuestionsForm _View;
        IResultShow _ViewResults; // интерфейс для вывода детальной информации по результатам
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        XPCollection<VoteDetailObj> _VoteDetails;
        XPCollection<DelegateObj> _DelegateList;

        MainCntrler _mainController;


        public DelegateQuestionsCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void Dispose()
        {

        }

        public void AssignView(IDelegateQuestionsForm viewInterface, IResultShow viewresInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
            _ViewResults = viewresInterface;

        }

        public void Init(DelegateObj seldelegate)
        {

            _theDelegate = seldelegate;

            if (seldelegate == null)
                return;

            CriteriaOperator crVote = new BinaryOperator(
                new OperandProperty("idDelegate.idDelegate.id"), new OperandValue(seldelegate.id),
                BinaryOperatorType.Equal);

/*
            List<int> _filterArray = new List<int>();

            CriteriaOperator crVote = new InOperator("id", _filterArray.ToArray());
*/ 
             
            _VoteDetails = new XPCollection<VoteDetailObj>(Session.DefaultSession, crVote);

            _theSession = DataModel.TheSession;

            _DelegateList = null; //new XPCollection<DelegateObj>(Session.DefaultSession, false);

//            _DelegateList.Add(_theDelegate);
        }

        public void InitList(ArrayList dlgtList)
        {

            if (dlgtList.Count == 0)
                return;

            CriteriaOperator crVote = new InOperator("idDelegate.idDelegate.id", dlgtList.ToArray());

            _VoteDetails = new XPCollection<VoteDetailObj>(Session.DefaultSession, crVote);

            _theSession = DataModel.TheSession;

            CriteriaOperator crDlgts = new InOperator("id", dlgtList.ToArray());
            _DelegateList = new XPCollection<DelegateObj>(Session.DefaultSession, crDlgts);


        }


        public void DataPositionChanged(int pos)
        {
            _theQuestDetails = null;

            if (pos < 0)
                return;

            if (pos >= _VoteDetails.Count)
                return;

            _theQuestDetails = _VoteDetails[pos] as VoteDetailObj;

            _ViewResults.ShowVoteResults(_theQuestDetails.idQuestion.idResult.idVoteResult);

            bool IsVoted = false;
            if (_theQuestDetails.idQuestion.idResult.IsResult == true)
                IsVoted = true;

            _View.EnableButtons(IsVoted);
        }

        public void ShowSelQuestionDetails()
        {
            if (_theQuestDetails != null)
            {
                if (_theQuestDetails.idQuestion.idResult.IsResult == true)
                {
                    _mainController.VoteModeRun(_theQuestDetails.idQuestion, false);
                }
            }
        }

        public void ShowView()
        {
            _View.ShowView();
        }

        public void OnInitForm()
        {
            if (_VoteDetails == null)
            {
                _msgForm.ShowError("Ошибка инициализации спсика результатов голосований депутата");
                return;
            }

            _View.InitControls(_VoteDetails, _theDelegate);
        }

        public void PrintReport()
        {
            ReportCntrler c = (ReportCntrler)_mainController.GetSubController("Report Controller");
            c.ShowReport_D(_theDelegate, _theSession, _DelegateList);
        }

    }
}
