﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public class DelegatesCntrler
    {
        public enum DelegatesFormMode
        {
            Manage,
            Select,
            SeatSelect, // выбора депутатов без мест
            SelectStatem,
            ReportSelect
        }

        DelegateObj _theDelegate;
        NestedUnitOfWork _nuow;

        IDelegatesForm _View;
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        XPCollection _Parties;
        XPCollection _Delegates;
        XPCollection _Fractions;
        XPCollection _Regions;
        XPCollection _Seats;
        XPCollection<DelegateTypeObj> _DelegateTypes; 

        DelegatesFormMode _Mode = DelegatesFormMode.Manage;
        List<int> _filterArray;

        MainCntrler _mainController;

        public DelegatesFormMode Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        public DelegatesCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IDelegatesForm viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }


        public void Init(ref XPCollection Delegates, ref XPCollection Parties, ref XPCollection Fractions, 
            ref XPCollection Regions, ref XPCollection Seats)
        {
            _Parties = Parties;
            _Delegates = Delegates;
            _Regions = Regions;
            _Fractions = Fractions;
            _Seats = Seats;

            _DelegateTypes = new XPCollection<DelegateTypeObj>();
        }

        public void InitShow()
        {
            CriteriaOperator cr = CriteriaOperator.Parse("id <> 0 AND (IsDel == false || IsDel is NULL)");
            _Delegates.Criteria = cr;
            _Delegates.Reload();
        }

        public void EditRecord(int pos)
        {
            _theDelegate = null;

            if (pos <= -1)
                return;

            _nuow = _Delegates.Session.BeginNestedUnitOfWork();

            _theDelegate = _nuow.GetNestedObject(_Delegates[pos]) as DelegateObj;

//          _nuow.BeginTransaction();

            DelegateEditForm d = new DelegateEditForm(_theDelegate, this);
            DialogResult dr = d.ShowDialog(_View.GetView());
            if (dr == DialogResult.OK)
            {
                if (_theDelegate.isActive == true)
                {
                    AddSesDelegate(_theDelegate);
                }
            }
        }

        public void DataPositionChanged(int pos)
        {
            _theDelegate = null;

            if (pos < 0)
                return;

            if (pos >= _Delegates.Count)
                return;

            _theDelegate = _Delegates[pos] as DelegateObj;

            _View.EnableButtons(_theDelegate.isActive, _theDelegate.IsDel);
        }


        public int AddNewDelegate()
        {
            DelegateObj newDelegate = new DelegateObj(Session.DefaultSession);

            PartyObj party = (PartyObj)_Parties[0];
            FractionObj fraction = (FractionObj)_Fractions[0];
            RegionObj region = (RegionObj)_Regions[0];
            SeatObj seat = (SeatObj)_Seats[0];

            newDelegate.idParty = party;
            newDelegate.idFraction = fraction;
            newDelegate.idRegion = region;
            newDelegate.idSeat = seat;
            newDelegate.idType = _DelegateTypes[0];

            newDelegate.LastName = "Новый";
            newDelegate.FirstName = "";
            newDelegate.SecondName = "";

            newDelegate.id = deleagetsMaxId() + 1;

            if (Mode == DelegatesFormMode.Select)
            {
                if (_filterArray != null)
                {
                    _filterArray.Add(newDelegate.id);
                    CriteriaOperator cr = new InOperator("id", _filterArray.ToArray());
                    _Delegates.Filter = cr;
                }
            }

            int i = -1;
            DelegateEditForm d = new DelegateEditForm(newDelegate, this);
            DialogResult dr = d.ShowDialog(_View.GetView());
            if (dr == DialogResult.OK)
            {
                i = _Delegates.Add(newDelegate);
                newDelegate.Save();

                if (newDelegate.isActive == true)
                {
                    AddSesDelegate(newDelegate);
                }

            }

            return i;
        }

        public int deleagetsMaxId()
        {
            DevExpress.Xpo.DB.SelectedData resultSet = Session.DefaultSession.ExecuteQuery("select max(id) from delegates");
            DevExpress.Xpo.DB.SelectStatementResult res = (DevExpress.Xpo.DB.SelectStatementResult)resultSet.ResultSet.GetValue(0);
            DevExpress.Xpo.DB.SelectStatementResultRow row = (DevExpress.Xpo.DB.SelectStatementResultRow)res.Rows.GetValue(0);
            int maxId = (int)row.Values[0];
            return maxId;
        }

        public void AddSesDelegate(DelegateObj d)
        {
            if ((d == null || d.id <= 0))
                return;

            SesDelegateObj sesd = DataModel.SesDelegates_GetByDelegateID(d.id); // проверить, нет ли депутат в списках депутатов текущей сессии
            if (sesd == null) // если депутат нет в списке, предложить добавить
            {
                DialogResult dr = _msgForm.ShowQuestion("Добавить депутата в список депутатов текущей сессии?");
                if (dr == DialogResult.Yes)
                {
                    DelegatesSesCntrler sesc = (DelegatesSesCntrler)_mainController.GetSubController("Session Delegates Controller");
                    SesDelegateObj s = sesc.CreateSesDelegate(d, -1);
                    DataModel.SesDelegates.Add(s);
                    s.Save();
                }
            }
        }

        public void HideOrRemoveDelegate()
        {
            if (_theDelegate == null)
                return;

            if (_theDelegate.IsDel)
                RemoveDelegate();
            else
                HideDelegate();
        }

        public void RemoveDelegate()
        {
            if (_theDelegate == null)
                return;

            DialogResult dr = _msgForm.ShowQuestion("Вы уверены, что хотите удалить выделенную запись?");
            if (dr != DialogResult.Yes)
                return;

            try
            {
              _Delegates.DeleteObjectOnRemove = true;
              
              _theDelegate.IsDel = true;
              _theDelegate.Save();

                if (DataModel.TryRemoveDelegate(_theDelegate))
                  _msgForm.ShowError("Удаление депутата");    //                    _Delegates.Remove(_theDelegate);
                else
                    _msgForm.ShowError("Невозможно удалить запись, так депутат находится в списке текущей сессии или участвовал в голосовании(выступлениях).");
            }
            catch (Exception ex)
            {
                _Delegates.Load();

                _msgForm.ShowError("Невозможно удалить запись. Возможно этот депутат находится в списке сессии или в списке выступающих.");
            }
        }

        public void HideDelegate()
        {
            if (_theDelegate == null)
                return;

            _theDelegate.IsDel = true;
            _theDelegate.isActive = false;
            _theDelegate.Save();
        }

        public void RestoreDeletedItem()
        {
            if (_theDelegate == null)
                return;

            _theDelegate.IsDel = false;
            _theDelegate.Save();
        }

        public void ShowAll()
        {
            CriteriaOperator cr = CriteriaOperator.Parse("id <> 0");
            _Delegates.Criteria = cr;
            _Delegates.Reload();

        }
/*
        public string GetRegionNumByValue(string RegionName)
        {
            // регионы
            CriteriaOperator criteria3 = new BinaryOperator(
                new OperandProperty("Name"), new OperandValue(RegionName),
                BinaryOperatorType.Equal);

            _Regions.Filter = criteria3;
            if (_Regions.Count == 0)
                _Regions.Filter = null;

            RegionObj region = _nuow.GetNestedObject(_Regions[0]) as RegionObj;
            return region.Number;
        }
*/

        public bool SelectItems(List<int> filterlist)
        {
//          _View = new DelegatesForm(this);
//          _Delegates.Reload();
            _Delegates.Filter = null;

            List<int> list = new List<int>();
            foreach (DelegateObj o in _Delegates )
            {
                list.Add(o.id);
            }

            _filterArray = new List<int>(list.Except<int>(filterlist).ToArray<int>());

            CriteriaOperator cr = new InOperator("id", _filterArray.ToArray());
            _Delegates.Filter = cr;

            _View.SetSelectMode();

            DialogResult dr = _View.ShowView();

            DataModel.ReloadSesDelegates();

            if (dr == DialogResult.OK)
            {
                return true;
            }

            return false;
        }

        public void SelectItemsReport()
        {
            Mode = DelegatesCntrler.DelegatesFormMode.ReportSelect;
            _View.SetSelectMode();
            ShowView();
        }
        public void ShowViewManageMode()
        {
            Mode = DelegatesCntrler.DelegatesFormMode.Manage;
            ShowView();
        }

        private void ShowView()
        {
            _Delegates.Filter = null;
            _View.ShowView();

            DataModel.ReloadSesDelegates();
        }

        public DelegateObj GetSelectedItemFromList(List<int> selectedList)
        {

            CriteriaOperator cr = new InOperator("id", selectedList.ToArray());
            _Delegates.Filter = cr;

            DialogResult dr = _View.ShowView();
            if (dr == DialogResult.OK)
            {
                return _theDelegate;
            }

            return null;
        }

        List<int> _selItems = new List<int>();
        int _currIndex = 0;
        public DelegateObj GetFirstSelItem()
        {
            _currIndex = 0;
            DelegateObj d = new DelegateObj(Session.DefaultSession);
            _selItems = new List<int>(_View.GetSelectedItems());
            if (_selItems.Count > 0)
            {
                int id = _selItems[_currIndex];

                d = (DelegateObj)_Delegates.Lookup(id);
                _currIndex++;
            }
            else
            {
                d = null;
            }

            return d;
        }

        public DelegateObj GetNextSelItem()
        {
            DelegateObj d = new DelegateObj(Session.DefaultSession);
            if (_currIndex < _selItems.Count && _selItems.Count > 0)
            {
                int id = _selItems[_currIndex];

                d = (DelegateObj)_Delegates.Lookup(id);
                _currIndex++;
            }
            else
                d = null;

            return d;
        }

        public void ShowDetails()
        {
            if (_Mode == DelegatesFormMode.Manage || _Mode == DelegatesFormMode.ReportSelect)
            {
                using (DelegateQuestionsCntrler c = new DelegateQuestionsCntrler(_mainController))
                {
                    using (DelegateQuestionsForm f = new DelegateQuestionsForm(c, _mainController, null))
                    {
                        c.AssignView((IDelegateQuestionsForm)f, (IResultShow)f);

                        if (_Mode == DelegatesFormMode.Manage)
                        {
                            if (_theDelegate == null)
                                return;

                            c.Init(_theDelegate);
                            c.ShowView();
                        }
                        else if (_Mode == DelegatesFormMode.ReportSelect)
                        {
                            ArrayList list = new ArrayList();
                            DelegateObj d = GetFirstSelItem();
                            while (d != null)
                            {
                                list.Add(d.id);
                                d = GetNextSelItem();
                            }

                            c.InitList(list);
                            c.ShowView();
                        }
                    }
                }
            }

        }

        DelegateObj _DelegateCardCode;
        public void ReadCardCode(DelegateObj d, int micnum)
        {
            if (micnum <= 0)
                return;

            _DelegateCardCode = d;
            ConfHelper.OnProcessCard = ProccessCardNumber;
            ConfHelper.SendMicCommand_rfid(micnum);
        }

        private void ProccessCardNumber(ConfHelper.MicState ms) // процедура для определения номеров карточек
        {
            if (ms.MicNum < 1)
                return;


            if (ms.Code != "ffffffffff")
            {
                if (ms.Code.Length == 8) //надо добавить 0d, поскольку она была съедена
                    ms.Code = ms.Code + "0d";

                _DelegateCardCode.CardCode = ms.Code;
                _DelegateCardCode.Save();
            }

            ConfHelper.OnProcessCard -= ProccessCardNumber;
        }

        public void SendCardCodeToDriver()
        {
            ConfHelper.SendCommand_RFIDData("0", "clear"); // сначала очистить всю базу

            foreach (DelegateObj dlgt in _Delegates)
            {
                if (dlgt.CardCode != null && dlgt.CardCode != "" && dlgt.CardCode != "0")
                    ConfHelper.SendCommand_RFIDData(dlgt.CardCode, dlgt.ShortName);
            }

            ConfHelper.SendCommand_RFIDData("0", "off");
        }


        public void GetDelegateIDsByFraction(int fractionID, ref List<int> IDs)
        {
            if (fractionID <= 0)
                return;

            CriteriaOperator cr = new InOperator("idFraction.id", fractionID);
            _Delegates.Filter = cr;

            foreach (DelegateObj d in _Delegates)
            {
                IDs.Add(d.id);
            }

            _Delegates.Filter = null;
        }

        public void SyncCardDBase()
        {
            SendCardCodeToDriver();
            _View.GetMessages().ShowWarningMsg("Синхронизация базы данных участников с конференц-системой успешно завершена");
        }
       
    }
}
