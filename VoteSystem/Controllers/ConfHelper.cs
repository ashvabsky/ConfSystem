﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Platform;
using VoteSystem.DataLayer;
using System.Drawing;

namespace VoteSystem
{
    // класс для взаимодействия с универсальным дравером конференц-системы
    public class ConfHelper
    {
        public enum ModeEnum
        {
            RegPrepare, VotePrepare, VoteMode, RegMode, ApplyMode, None
        }
        public enum CmdEnum
        {
            GetRFID
        }

        public struct MicState
        {
            public int MicNum;
            public string State;
            public string Code;
        }

        public struct MsgState
        {
            public int LineNum;
            public string Caption;
            public string Text;
        }

        public delegate void ProccessCmd(CmdEnum cmdType);
        public delegate void ProccessData(MicState ms);
        public delegate void ProccessMsg(int msgLevel, string caption, string msg);
        public delegate void FinishProccess();

        public static Queue _regList = new Queue();
        public static Queue _applyList = new Queue();
        public static Queue _voteList = new Queue();
        public static Queue _msgList = new Queue();
        public static Queue _cardList = new Queue();
        public static ModeEnum _mode = ModeEnum.None;

        public static ConfDriver _driver = new ConfDriver();
        public static ConfDriver _driver2 = new ConfDriver();


        public static ProccessData      OnProcessCard;
        public static ProccessMsg       OnProcessMsg;
        public static FinishProccess    OnFinishProcessText;
        public static FinishProccess    OnFinishIdentifyCards;
        public static FinishProccess    OnVoteReady;
        public static ProccessCmd OnGetDelegatesRFID;


        private static bool _IsDataRecieve = true; // флаг указывает надо ли считывать команды из палантира
        private static Graphics _measureGraphic;
        private static Font _arialfont;
        private static Dictionary<int, string> _mictextList = new Dictionary<int, string>();


        public static void Init(string sysname, string sysname2)
        {
            _driver.Init(OnReceiveData, "192.168.1.80", sysname);
            //Твой рисунок
            Bitmap newImage = new Bitmap(316, 100);
            //Твой шрифт
            FontFamily fontik = new FontFamily("Arial");
            _arialfont = new Font(fontik, 8);
            //График для рисунка
            _measureGraphic = Graphics.FromImage(newImage);
            //Твой текст

            _IsDataRecieve = true;
        }

        public static void Connect()
        {
            _driver.Connect();
/*
            if (_driver2 != null)
                _driver2.Connect();
 */
        }

        public static bool IsConnected()
        {
            return _driver.IsConnected();
        }

        public static void ClearAll()
        {
            _applyList.Clear();
            _regList.Clear();
            _cardList.Clear();
            _voteList.Clear();
            _msgList.Clear();
        }

        public static void StartDataRecieve()
        {
            _IsDataRecieve = true;
        }

        public static void StopDataRecieve()
        {
            _IsDataRecieve = false;
        }

        public static void SendCommand_MicOn(int MicNum)
        {
            if (MicNum > 0)
                SendMicCommand(MicNum.ToString(), "on");
        }


        public static void SendCommand_MicApply(int MicNum)
        {
            if (MicNum > 0)
                SendMicCommand(MicNum.ToString(), "apply");
        }

        public static void SendCommand_MicOff(int MicNum)
        {
            if (MicNum > 0)
                SendMicCommand(MicNum.ToString(), "off");
        }

        public static void SendCommand_VoteStart()
        {
            SendCommand("vote", "on");
            _mode = ModeEnum.VoteMode;
        }

        public static void SendCommand_VotePrepareStart()
        {
            _voteList.Clear();
            SendCommand("voteprep", "on");
            _mode = ModeEnum.VotePrepare;
        }

        public static void SendCommand_VotePrepareOff()
        {
            SendCommand("voteprep", "off");
            _mode = ModeEnum.None;
        }

        public static void SendCommand_RegPrepareStart()
        {
            _regList.Clear();
            SendCommand("regprep", "on");
            _mode = ModeEnum.RegPrepare;
        }

        public static void SendCommand_RegPrepareOff()
        {
            SendCommand("regprep", "off");
            _regList.Clear();
            _mode = ModeEnum.None;
        }

        public static void SendCommand_VoteStop()
        {
            SendCommand("vote", "off");
            _mode = ModeEnum.None;
        }

        public static void SendCommand_RegStart()
        {
            SendCommand("reg", "on");
            _mode = ModeEnum.RegMode;
        }

        public static void SendCommand_rfid_Start()
        {
            _cardList.Clear();
            SendCommand("rfid", "on");
        }

        public static void SendCommand_Text_Start()
        {
            SendCommand("text", "on");
        }

        public static void SendCommand_Text_Stop()
        {
            SendCommand("text", "off");
        }

        public static void SendMicCommand_rfid(int MicNum)
        {
            SendMicCommand(MicNum.ToString(), "rfid");
        }

        public static void SendMicCommand_str_line1(int MicNum, string text)
        {
            SendMicCommand_str_line(MicNum, text, "1");
        }

        public static void SendMicCommand_str_line2(int MicNum, string text)
        {
            SendMicCommand_str_line(MicNum, text, "2");
        }

        public static void SendMicCommand_FIO(int MicNum, string Name)
        {
            string text = "";
            if (Name != "")
            {
                text = Name;
            }
            else
                text = "ГС УР";


            SendMicCommand_Text(MicNum, text);
        }

        public static void SendMicCommand_Text(int MicNum, string text)
        {

            if (_mictextList.ContainsKey(MicNum) == true)
            {
                if (_mictextList[MicNum] == text) // такой текст уже выводился
                {
                    return; // текст уже выведен - ничего не делать
                }
            }

            SendMicCommand_str_line(MicNum, text, "2");
            _mictextList[MicNum] = text;
        }

        public static void InitMicText()
        {
            _mictextList.Clear();
        }

        private static void SendMicCommand_str_line(int MicNum, string text, string strnum)
        {
/*
            SizeF sizetext = _measureGraphic.MeasureString(text, _arialfont);
            SizeF sizespace = _measureGraphic.MeasureString(" ", _arialfont);

            if (sizetext.Width < 318)
            {
                int leftspacepixels = 318 - Convert.ToInt32(sizetext.Width);
                int leftspaces = Convert.ToInt32(leftspacepixels / sizespace.Width);

                string spaces = new string(' ', leftspaces/2);
                text = spaces + text;
            }
            else
            {
                while (true)
                {
                    text = text.Substring(0, text.Length - 2);
                    SizeF newsizetext = _measureGraphic.MeasureString(text, _arialfont);
                    if (newsizetext.Width < 318)
                        break;
                }
            }
*/
            text = "      " + text;

            string s = "str" + strnum+ "_" + text;
            SendMicCommand(MicNum.ToString(), s);
        }


        public static void SendCommand_MicReset(int MicNum)
        {
            if (MicNum >= 0)
                SendMicCommand(MicNum.ToString(), "reset");
        }

        public static void SendCommand_MicResetAll()
        {
            if (IsConnected())
                SendCommand_MicReset(0);
            _applyList.Clear();
        }


        public static void SendCommand_RegStop()
        {
            SendCommand("reg", "off");
            _mode = ModeEnum.None;
        }

        public static void SendCommand_SpeechStart()
        {
            if (_mode != ModeEnum.ApplyMode)
            {
                SendCommand_MicResetAll();
                SendCommand("speech", "on");
                _mode = ModeEnum.ApplyMode;
            }
        }

        public static void SendCommand_SpeechStop()
        {
            //          _netHelper.SendCommand("speech", "off");
            _applyList.Clear();
            SendCommand("speech", "off");
            _mode = ModeEnum.None;
        }


        public static void SendCommand(string cmd, string act)
        {
            // через сокет

            cmd = "cmd_" + cmd + "_" + act; // пример: "cmd_reg_on" - включить регистрацию, "cmd_reg_off" - включить голосование; cmd_vote_on, cmd_vote_off
            _driver.Send(cmd);
        }


        public static void SendCommand_RFIDData(string rfid, string name)
        {
            string data = "rfiddata" + "_" + rfid + "_"+ name;
            _driver.Send(data);

        }

        public static void SendMicCommand(string MicNum, string act)
        {
            // через сокет

            string cmd = "mic_" + MicNum + "_" + act; // пример: "mic_27_on", "mic_27_off", "mic_all_off" - выключить все, "reg_on_-1" - включить регистрацию, "vote_on_-1" - включить голосование
            _driver.Send(cmd);
        }

        public static bool GetData_Reg(ref MicState ms)
        {
            if (_regList.Count == 0)
                return false;

            lock (_regList.SyncRoot)
            {
                ms = (MicState)_regList.Dequeue();
            }
            return true;
        }

        public static bool GetData_Card(ref MicState ms)
        {
            if (_cardList.Count == 0)
                return false;

            lock (_cardList.SyncRoot)
            {
                ms = (MicState)_cardList.Dequeue();
            }
            return true;
        }

        public static bool GetData_Apply(ref MicState ms)
        {
            if (_applyList.Count == 0)
                return false;

            lock (_applyList.SyncRoot)
            {
                ms = (MicState)_applyList.Dequeue();
            }
            return true;
        }

        public static bool GetData_Vote(ref MicState ms)
        {
            if (_voteList.Count == 0)
                return false;

            lock (_voteList.SyncRoot)
            {
                ms = (MicState)_voteList.Dequeue();
            }
            return true;
        }

        public static bool GetData_Msg(ref MsgState ms)
        {
            if (_msgList.Count == 0)
                return false;

            lock (_msgList.SyncRoot)
            {
                ms = (MsgState)_msgList.Dequeue();
            }
            return true;
        }

        public static void Finish()
        {
            StopDataRecieve();
            ClearAll();
            _driver.ShutDownClient();
            if (_driver2 != null)
                _driver2.ShutDownClient();
        }


        public static void OnReceiveData(string data)
        {
            if (_IsDataRecieve == false)
                return;

            string cmd = "";
            int micnum = -1;
            string state = "";
            string code = "";

            ParseData(data, ref cmd, ref micnum, ref state, ref code);

            switch (_mode)
            {
                case ModeEnum.VoteMode:
                case ModeEnum.VotePrepare:
                    {
                        if (cmd == "mic")
                        {
                            MicState ms = new MicState();
                            ms.MicNum = micnum;
                            ms.State = state;

                            if (ms.MicNum == 0 && state == "vote" && code == "ready")
                            {
                                if (OnVoteReady != null)
                                    OnVoteReady();
                            }
                            else
                            {
                                lock (_voteList.SyncRoot)
                                {
                                    _voteList.Enqueue(ms);
                                }
                            }
                        }
                        break;
                    }
                case ModeEnum.RegMode:
                case ModeEnum.RegPrepare:
                    {
                        if (cmd == "mic")
                        {
                            MicState ms = new MicState();
                            ms.MicNum = micnum;
                            ms.State = "reg";

                            lock (_regList.SyncRoot)
                            {
                                _regList.Enqueue(ms);
                            }
                        }
                        break;
                    }

                case ModeEnum.ApplyMode:
                    {
                        if (cmd == "mic")
                        {
                            MicState ms = new MicState();
                            ms.MicNum = micnum;
                            if (state.Equals("apply", StringComparison.OrdinalIgnoreCase) == true)
                                ms.State = "apply";
                            else if (state.Equals("off", StringComparison.OrdinalIgnoreCase) == true)
                                ms.State = "off";
                            else if (state.Equals("unapply", StringComparison.OrdinalIgnoreCase) == true)
                                ms.State = "unapply";
                            else
                                goto case ModeEnum.None;


                            if (ms.State != null)
                            {
                                lock (_applyList.SyncRoot)
                                {
                                    _applyList.Enqueue(ms);
                                }
                                break;
                            }
                        }

                        break;
                    }

                case ModeEnum.None:
                default:
                    {
                        if (cmd == "mic")
                        {
                            if (state.Equals("rfid", StringComparison.OrdinalIgnoreCase) == true)
                            {
                                MicState ms = new MicState();

                                ms.MicNum = micnum;
                                ms.State = "rfid";
                                ms.Code = code;

                                if (ms.Code == "off")
                                    OnFinishIdentifyCards();// ;OnFinishProcessCard();
                                else if (ms.Code == "get")
                                    OnGetDelegatesRFID(CmdEnum.GetRFID);// Драйвер просит прислать ему всю информацию по rfid участников 
                                else
                                {
                                    lock (_cardList.SyncRoot)
                                    {
                                        _cardList.Enqueue(ms);
                                    }

                                    if (OnProcessCard != null)
                                        OnProcessCard(ms);
                                }
                            }
                            else if (state.Equals("text", StringComparison.OrdinalIgnoreCase) == true)
                            {
                                MicState ms = new MicState();

                                ms.MicNum = micnum;
                                ms.State = "text";
                                ms.Code = code;

                                if (ms.Code == "off")
                                {
                                    if (OnFinishProcessText != null)
                                        OnFinishProcessText();
                                }
                                else
                                    if (OnProcessCard != null)
                                        OnProcessCard(ms); // зачем???
                            }

                        }
                        else if (cmd == "msg")
                        {
                            int lineNum = micnum;

                            string caption = "";
                            if (lineNum > 0)
                                caption = "сообщение от линии №" + lineNum.ToString(); 
                            else
                                caption = "Новости аппаратной части"; 


                            string text = DeCode(code);

                            int msgLevel = 1; // 0 - серъезная ошибка(сразу вывести на экраны) 1 - сообщение, видимое в истории
                            if (state == "err")
                                msgLevel = 0;
                            else
                                msgLevel = 1;

                            if (OnProcessMsg != null)
                                OnProcessMsg(msgLevel, caption, text);

                            MsgState ms = new MsgState();
                            ms.Caption = caption;
                            ms.LineNum = lineNum;
                            ms.Text = text;

                            lock (_msgList.SyncRoot)
                            {
                                _msgList.Enqueue(ms);
                            }
                        }
                        break;
                    }

            }
        }

        public static void OnReceiveData2(string data)
        {
            if (_IsDataRecieve == false)
                return;

            string cmd = "";
            int micnum = -1;
            string state = "";
            string code = "";

            ParseData(data, ref cmd, ref micnum, ref state, ref code);

            if (cmd == "mic")
            {
                if (state.Equals("rfid", StringComparison.OrdinalIgnoreCase) == true)
                {
                    MicState ms = new MicState();

                    ms.MicNum = micnum;
                    ms.State = "rfid";
                    ms.Code = code;

                    if (ms.Code == "off")
                        OnFinishIdentifyCards();// ;OnFinishProcessCard();
                    else if (ms.Code == "get")
                        ;
                    else
                    {
                        lock (_cardList.SyncRoot)
                        {
                            _cardList.Enqueue(ms);
                        }
                    }
                }

            }
        }

        private static void ParseData(string data, ref string s_cmdname, ref int micnum, ref string state, ref string code)
        {
            if (data == "")
                return;

            string[] stringSeparators = new string[] { "_"};
            string[] result;

            result = data.Split(stringSeparators, 4, StringSplitOptions.RemoveEmptyEntries);

            string s_micnum = "";
            string s_micstate = "";
            for (int i = 0; i < result.Length; i++)
            {
                if (i == 0) // первая часть команды
                    s_cmdname = result[i];
                else if (i == 1) // первая часть команды
                    s_micnum = result[i];
                else if (i == 2) // первая часть команды
                    s_micstate = result[i];
                else if (i == 3) // первая часть команды
                    code = result[i];
            }

            try
            {
                if (s_micnum != "")
                {
                    micnum = Convert.ToInt32(s_micnum);
                }
            }
            catch (Exception ex)
            {
            }

            state = s_micstate;
        }

        private static string DeCode(string source) // метод кодирует из ansii в unicode
        {
            return source;
        }
    }
}
