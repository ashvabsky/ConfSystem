﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;
using System.Threading;

namespace VoteSystem
{
    /*
     * субконтроллер - координирует работу формы VoteModeForm - "Режим голосвания по вопросу".
     * Управление формой осущестлвляется через интерфейс IVoteModeForm
     * Дополнителльно координирует работу формы VoteStartForm
     */
    public class VoteModeCntrler
    {
#region private секция данных
        // перечень значений, которые предоставляет пульт при голосовании
        public enum SeatStates {Answer1 /*yes*/, Answer2 /*abstain*/, Answer3 /*no*/, Answer4, Answer5, NoAnswer, Disabled, None };

        IVoteModeForm _View; // главный интерфейс, с которым работаем
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        MainCntrler _mainController; // главный контроллер

        QuestionObj _TheQuestion; // вопрос, по которому производится голосование
        VoteResultObj _TheVoteResult = null; // результаты голосования, выводимые на форму

        HallCntrler _hall; // контроллер управления залом

        XPCollection<QuestionObj> _QuestCollection;
        XPCollection<VoteDetailObj> _localdelegates;
        XPCollection<VoteDetailObj> _votedetails;

        // сопоставление номера пульта и привязанного к этоу пульту депутата
        Dictionary<int, SesDelegateObj> _SeatDelegatesList = new Dictionary<int, SesDelegateObj>();

        /*readonly*/ ResultValueObj _NoResult;// = DataModel.ResValues_GetObject("NoResult");

        // список, сопоставляет номеру пульта - значение результат голосования на этом пульте
        Dictionary<int, SeatStates> _States = new Dictionary<int, SeatStates>();

        Platform.ProgressForm _autoProgressForm = new Platform.ProgressForm();
        private ManualResetEvent voteprepDone =
            new ManualResetEvent(true);

        int _DelegatesQnty = 0;

        int _VoteTime = 0; // время отводимое на голосование

        bool _voteMode = false; // признак, указывает, идет ли процесс голосования в данный момент
        bool _votePrepareMode = false; // признак, указывает, идет ли процесс подготовки голосования в данный момент
        bool _IsvoteInfoToScreen = true; // флаг указывает автоматически выводить информацию на мониторы
        bool _prepareToStart = false; // флаг указывает, надо ли автоматически выводить окно -приглашение для голосования при запуске режима

        int _maxVoteDetailsID = 0;

        bool _fastMode = false; // указывает, включать ли возможность быстрого запуска голосования (т.е. следует ли ожидать перевода системы в режим голосования или сразу голосовать)
#endregion private секция данных

        public bool IdentifyCardsMode = true;

        public bool VoteMode
        {
            get { return _voteMode; }
        }

        public QuestionObj CurrentQuestion
        {
            get { return _TheQuestion; }
        }

#region основные методы

        public VoteModeCntrler(MainCntrler controller)
        {
            _mainController = controller;
        }

        public void AssignView(IVoteModeForm viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }

        public void ShowForm()
        {
            _View.ShowView();
        }

        // первоначальная инициализация
        public void Init(SesTaskObj theQuestion)
        {
            _TheQuestion = new QuestionObj(theQuestion);
            Init();


            _TheVoteResult = null;


            // сразу вызвать окно для старта голосования
            _prepareToStart = true;

            if (_prepareToStart == false)
                InitVoteDetails();
        }

        public void Init(QuestionObj theQuestion, bool fastmode)
        {
            _TheQuestion = theQuestion;
            Init();
            _fastMode = fastmode;

            _TheVoteResult = theQuestion.idResult.idVoteResult;
            if (theQuestion.idResult.idResultValue.CodeName == "NoResult")
            {
                _TheVoteResult.AvailableQnty = _DelegatesQnty;
            }

            // вызывать/не вызывать окно для старта голосования
            if (theQuestion.idResult.idResultValue != null && theQuestion.idResult.idResultValue.CodeName != "NoResult")
                _prepareToStart = false;
            else 
                _prepareToStart = true;

            if (_prepareToStart == false)
                InitVoteDetails();
        }

        public void Init()
        {
            _localdelegates = null;
            _votedetails = null;
            CriteriaOperator crVD = CriteriaOperator.Parse("idQuestion.id == ?", _TheQuestion.id);
            _votedetails = new XPCollection<VoteDetailObj>(Session.DefaultSession, true); // 
            _votedetails.Criteria = crVD;
            _votedetails.Load();

            _VoteTime = DataModel.TheSettings.VoteTime; // DataModel.TheSession.VoteTime;

            _States.Clear();

            _TheVoteResult = null;

            DataModel.VoteResults.Filter = null;
            _voteMode = false;

            _maxVoteDetailsID = 0;

            _hall = (HallCntrler)_mainController.GetSubController("HallPage Controller");
            _NoResult = DataModel.ResValues_GetObject("NoResult");

            voteprepDone.Reset();
            ConfHelper.OnVoteReady = OnVoteReady;

            _fastMode = false;
           
        }

#endregion основные методы

#region public methods, which is called from the view

        public void OnLoadForm()
        {
            _autoProgressForm = new Platform.ProgressForm();
            _autoProgressForm.Owner = _View.GetView();
            _autoProgressForm.ProgressDone = voteprepDone;
            _autoProgressForm.MaxTime = 8000;
            _autoProgressForm.Text = "Подготовка к голосованию";
            _autoProgressForm.ProgressText = "Перевод системы в режим голосования.";
            //          _autoProgressForm.ProgressFinishText = "Процедура определения карточек завершена.";
            _autoProgressForm.ProgressFailedText = "Ошибка перевода системы в режим голосования!";

            _View.InitControls(_TheQuestion.Name);

            ShowQuestProperties(_TheQuestion); // вывести инфу о вопросе в панель свойств
            ShowDelegateProp(-1); // инфа о выбранном депутате пока отсутствует
            InitSeats(); // инициализировать пульты
//          UpdateSeats(); // отрисовать пульты
            InitVoteParams(); // получить параметры голосования

            _View.ShowVoteResults(_TheVoteResult); // вывести результаты голосвания на форму (если есть)
        }

        public void OnFormShown()
        {
//          _localdelegates.Load();
            _View.SetDelegateProperties(_localdelegates);

            if (_prepareToStart == true)
                Vote_PrepareToStart();
        }

        // выисляет значение результата для пульта с указанным номером
        public SeatStates GetSeatState(int MicNum)
        {
/*
            CriteriaOperator criteria1 = CriteriaOperator.Parse("MicNum == ?", MicNum);

            DataModel.Seats.Filter = criteria1;
            if (DataModel.Seats.Count == 0)
            {
                DataModel.Seats.Filter = null;
                return SeatStates.None;
            }

            SeatObj seat = DataModel.Seats[0];
            DataModel.Seats.Filter = null;

            if (seat.PhysicalState == false)
                return SeatStates.None;

            CriteriaOperator criteria2 = CriteriaOperator.Parse("idSeat.id == ?", seat.id);

            DataModel.SesDelegates.Filter = criteria2;
            if (DataModel.SesDelegates.Count == 0)
            {
                DataModel.SesDelegates.Filter = null;
                return SeatStates.None;
            }

            SesDelegateObj d = DataModel.SesDelegates[0];
            DataModel.SesDelegates.Filter = null;
*/
//          SesDelegateObj d = _hall.GetDelegateByMic(MicNum);
//          SeatObj seat = _hall.GetSeatByMic(MicNum);
/*
            if (d!= null)
            {
                if  (d.IsCardRegistered == false)
                return SeatStates.None;
            }
            if (seat != null)
            {
                if (seat.LogicalState == false)
                    return SeatStates.Disabled;
            }
*/
            if (_States.ContainsKey(MicNum))
                return _States[MicNum];

            SeatStates res = SeatStates.NoAnswer;
            return res;
        }


        public SesDelegateObj ShowDelegateProp(int MicNum)
        {

            SesDelegateObj Delegate = GetDelegate(MicNum);
            ShowDelegateProp(Delegate);

            return Delegate;

        }

        private void IdentifyCards()
        {
            if (IdentifyCardsMode == true)
            {
                bool res = _hall.IdentifyCards();
            }

            InitVoteDetails();
            InitSeats();


            return;
        }

        // вывести форму для старта голосования по текущему вопросу

        public bool Vote_PrepareToStart()
        {
//              IdentifyCardsMode = DataModel.TheSettingsHall.IsAutoCardMode;
            _votePrepareMode = true;
            IdentifyCards();

            voteprepDone.Reset();
            ConfHelper.SendCommand_VotePrepareStart();

            if (_fastMode == false)
            {
                _autoProgressForm = new Platform.ProgressForm();
                _autoProgressForm.Owner = _View.GetView();
                _autoProgressForm.ProgressDone = voteprepDone;
                _autoProgressForm.MaxTime = 8000;
                _autoProgressForm.Text = "Подготовка к голосованию";
                _autoProgressForm.ProgressText = "Перевод системы в режим голосования.";
                //          _autoProgressForm.ProgressFinishText = "Процедура определения карточек завершена.";
                _autoProgressForm.ProgressFailedText = "Ошибка перевода системы в режим голосования!";

                _autoProgressForm.IsAutoCloseMode = true;
                _autoProgressForm.IsNeedFinishEvent = true;
                _autoProgressForm.ShowDialog();
            }
            //          System.Threading.Thread.Sleep(2000);

            {
                // сначала вывести на мониторы информацию о подготовке к голосованию
                Platform.QuestionInfo info = new Platform.QuestionInfo();
                info.Caption = _TheQuestion.Name;
                info.Description = _TheQuestion.idTask.Description;
                info.Type = _TheQuestion.idKind.Name;
                info.VoteTime = _VoteTime;
                _mainController._MonitorInfo = info;
                _mainController.CurrentActivity = MainCntrler.Activities.VotePrepare;
                _mainController.ShowMonitorPreview();

                VoteStartForm _VoteStartForm;

                _VoteStartForm = new VoteStartForm(this);
                _VoteStartForm.VoteTime = _VoteTime;
                _VoteStartForm.DelegatesQnty = _DelegatesQnty;

                DialogResult dr = _VoteStartForm.ShowDialog();
                if (dr == DialogResult.OK)
                {

                    _VoteTime = _VoteStartForm.VoteTime;
                    DataModel.TheSettings.VoteTime = _VoteTime;

                    _VoteStartForm = null;
                    _voteMode = true;

                    // послать на контроллер команду старта голосования
                    ConfHelper.SendCommand_VoteStart();

                    InitVoteResult();
                    InitVoteParams();


                    _View.StartVoting(_VoteTime);
                    string text = "Голосование по вопросу \"" + _TheQuestion.Name + "\"";
                    _mainController.HistoryAdd("СГ", text);
                }
                else
                {
                    ConfHelper.SendCommand_VotePrepareOff();
                    _mainController.ShowSplahToMonitors();
                }

                return (dr == DialogResult.OK);
            }
            return false;
        }

        public void OnVoteReady()
        {
            if (_votePrepareMode == true)
                _votePrepareMode = false;

            voteprepDone.Set();

        }

        public void Vote_Clear_Ask()
        {
            if (_TheVoteResult != null)
            {
                DialogResult dr = _msgForm.ShowQuestion("Отменить голосование?");

                if (dr == DialogResult.Yes)
                {
                    if (_voteMode == true)
                    {
                        _View.StopVoting();
                        Vote_Clear();
                        // послать на контроллер команду старта голосования
                        ConfHelper.SendCommand_VoteStop();

                        _mainController._MonitorInfo = null;
                        _mainController.CurrentActivity = MainCntrler.Activities.None;
                        _mainController.ShowMonitorPreview();

                        _msgForm.ShowWarningMsg("Голосование было отменено, а результаты анулированы.");
                    }
                    else
                    {
                        _msgForm.ShowWarningMsg("Невозможно отменить голосование, так как процесс голосования уже завершен.");
                    }
                }
            }
        }

        public bool Vote_Stop_Ask()
        {
            if (_voteMode == true)
            {
                DialogResult dr = _msgForm.ShowQuestion("Завершить голосование?");
                if (dr != DialogResult.Yes)
                {
                    return false;
                }

                Vote_Stop();
            }

            return true;
        }

        public bool OnClose()
        {
            if (Vote_Stop_Ask() == false)
            {
                return false;
            }

            ConfHelper.OnVoteReady = null;
            return true;

        }

        // Обработка процесса голосования. вызывается по таймеру
        public void OnVoteProcess(DateTime leftTime, int progress)
        {
            bool IsAnswer = false; 

            ConfHelper.MicState ms = new ConfHelper.MicState();
            bool b = ConfHelper.GetData_Vote(ref ms);
            while (b)
            {
                string btnState = ms.State;
                int micNum = ms.MicNum;

                if (btnState != "" && micNum > 0)
                {
                    SesDelegateObj d = _hall.GetDelegateByMic(micNum);

                    if (d != null && d.id > 0 /*&& d.idSeat.MicNum > 0  && d.idSeat.LogicalState == true && d.IsCardRegistered == true*/)
                    // учитывается голос только тех депутатов, которые представлены в списке _votedetails, см. процедуру InitVoteDetails
                    {
                        VoteDetailObj vdo = new VoteDetailObj();

                        foreach (VoteDetailObj v in _votedetails)
                        {
                            if (v.idDelegate.id == d.id && v.idQuestion.id == _TheQuestion.id && v.idSeat.MicNum > 0)
                            {
                                vdo = v;
                                break;
                            }
                        }

                        if (vdo.id > 0)
                        {
                            int btnNum = 0;

                            if (btnState == "B")
                                btnNum = 1;
                            else if (btnState == "C")
                                btnNum = 2;
                            else if (btnState == "D")
                                btnNum = 3;

                            if (btnNum >= 1 && btnNum <= 3)
                            {
                                string ans = "Answer_";
                                if (btnNum == 1)
                                    ans = ans + "Yes";
                                else if (btnNum == 2)
                                    ans = ans + "Abst";
                                else if (btnNum == 3)
                                    ans = ans + "No";

                                AnswerTypeObj ato = DataModel.AnswerTypes_GetByCodeName(ans);
                                vdo.idAnswer = ato;
                                IsAnswer = true;
                            }
                        }
                    }
                }

                b = ConfHelper.GetData_Vote(ref ms); 
            }


            if (IsAnswer)
            {
                UpdateVoteResult();

                InitSeats();
//                UpdateSeats();

                _View.ShowVoteResults(_TheVoteResult);
/*
                if (VoteQnty == _DelegatesQnty) // если кол-во проголосовавших равно кол-ву голосующих делегатов, то голосвание закончено
                {
                    Vote_Stop();
                }
 */ 
            }

            int VoteQnty = GetVoteDelegatesQnty();

            if (_voteMode == true)
            {
                // здесь следует сохранить текущую информацию о процессе регистрации
                Platform.VoteInfoStart info = new Platform.VoteInfoStart();

                DateTime voteTime = new DateTime(1900, 12, 13, 1, 1, 1);

                if (leftTime.Minute == 0 && leftTime.Second == 0 || leftTime.Day < voteTime.Day)
                {
                    progress = -1;

//                  _Controller.Vote_Stop();
                }

                info.DelegateQnty = _DelegatesQnty;
                info.VotedQnty = VoteQnty;
                info.leftTime = leftTime.ToString("mm:ss");
                info.Progress = progress;
                _mainController._MonitorInfo = info;
                ShowMonitorPreview("VoteStart");
            }

        }

        // открытие формы для старта голосования
        public void OnVoteStartFormLoad()
        {
            _View.EnableMonitor(_mainController.PanelEnabled);
//            ShowMonitorPreview("VotePrepare");
            _IsvoteInfoToScreen = true;
        }

        public void EnableMonitor(bool IsEnable)
        {
            _mainController.SwitchMonitors(IsEnable);
            _View.EnableMonitorButtons(IsEnable);
        }

        // метод сохраняет настроку по выводу информации о голосовании на мониторы
        public void SetRegInfoToScreen(bool value)
        {
            _IsvoteInfoToScreen = value;
        }

        public void PrintQuestionReport()
        {
            ReportCntrler c = (ReportCntrler)_mainController.GetSubController("Report Controller");
            c.ShowReport_Q(_TheQuestion);
        }

#endregion public methods, which is called from the view

#region private methods
        // заполняет список _States с результатми голований по каждому из пультов 
        // и передает его форме для правильной отрисовки пультов

        private void InitSeats()
        {
            _States.Clear();

            foreach (SeatObj s in DataModel.Seats)
            {
                _States[s.MicNum] = SeatStates.None;
            }

            if (_votedetails.Count > 0)
            {
                foreach (VoteDetailObj vdo in _votedetails)
                {
                    SeatStates state = SeatStates.None;

                    if (vdo.idAnswer.CodeName == "Answer_Yes")
                        state = SeatStates.Answer1;
                    else if (vdo.idAnswer.CodeName == "Answer_Abst")
                        state = SeatStates.Answer2;
                    else if (vdo.idAnswer.CodeName == "Answer_No")
                        state = SeatStates.Answer3;
                    else if (vdo.idAnswer.CodeName == "Answer4")
                        state = SeatStates.Answer4;
                    else if (vdo.idAnswer.CodeName == "Answer5")
                        state = SeatStates.Answer5;
                    else if (vdo.idAnswer.CodeName == "None")
                        state = SeatStates.NoAnswer;

                    _States[vdo.idSeat.MicNum] = state;
                }
            }
            _View.InitSeats();
        }
        /*
                private void UpdateSeats()
                {

                    for (int i = 0; i< DataModel.Seats.Count; i++) // пройтись по всем номерам микрофонов
                    {
                        if (_States.ContainsKey(i))
                        {
                            SeatStates st = _States[i];
                            if (st == SeatStates.Answer1 || st == SeatStates.Answer2 || st == SeatStates.Answer3 || st == SeatStates.Answer4 || st == SeatStates.Answer5 || st == SeatStates.NoAnswer)
                                _States[i] = SeatStates.NoAnswer;
                            else
                                _States[i] = SeatStates.None;
                        }
                        else
                            _States[i] = SeatStates.None;
                    }

                    if (_votedetails.Filter != null)
                    { 
                        foreach (VoteDetailObj vdo in _votedetails)
                        {
                                SeatStates state = SeatStates.None;

                                if (vdo.idAnswer.CodeName == "Answer_Yes")
                                    state = SeatStates.Answer1;
                                else if (vdo.idAnswer.CodeName == "Answer_Abst")
                                    state = SeatStates.Answer2;
                                else if (vdo.idAnswer.CodeName == "Answer_No")
                                    state = SeatStates.Answer3;
                                else if (vdo.idAnswer.CodeName == "Answer4")
                                    state = SeatStates.Answer4;
                                else if (vdo.idAnswer.CodeName == "Answer5")
                                    state = SeatStates.Answer5;
                                else if (vdo.idAnswer.CodeName == "None")
                                    state = SeatStates.NoAnswer;

                                _States[vdo.idDelegate.idSeat.MicNum] = state;
                        }
                    }

                    _View.InitSeats();
                }
        */
        // найти депутата, привязанного к пульту с указанным номером
        private SesDelegateObj GetDelegate(int MicNum)
        {

            if (_SeatDelegatesList.ContainsKey(MicNum) == false)
                return null;

            return _SeatDelegatesList[MicNum];
        }

        // очистить результаты голосования
        private void InitVoteResult()
        {
            _TheVoteResult = new VoteResultObj();
            _TheVoteResult.AvailableQnty = _DelegatesQnty;
            _TheVoteResult.idResultValue = _NoResult;
            _TheVoteResult.VoteDateTime = DateTime.Now;

            _TheQuestion.idResult.id = _mainController.GetMaxID("questions_results") + 1;
            _TheQuestion.idResult.idResultValue = _TheVoteResult.idResultValue;
            _TheQuestion.idResult.idVoteResult = _TheVoteResult;
            _TheQuestion.idTask.State = 0;

            int maxid = 0;
            if (DataModel.VoteResults.Count > 0)
                maxid = DataModel.VoteResults.Max<VoteResultObj>(v => v.id);

            _TheVoteResult.id = maxid + 1;


            DataModel.VoteResults.Add(_TheVoteResult);

            if (_TheQuestion.id <= 0)
            {
                int idMax = _mainController.GetMaxID("Questions");

                _TheQuestion.id = idMax + 1;
            }

            _View.ShowVoteResults(_TheVoteResult);
        }


        private void InitVoteDetails()
        {
            int idQ = _TheQuestion.id;
            if (idQ == 0)
                idQ = -1;

            // подготовить список голосующих
            // для начала - обязательно опустошить локальный список депутатов-результатов
            _localdelegates = null;
            _View.SetDelegateProperties(null);

            _votedetails.Reload();


            if (_TheQuestion.idResult.IsResult == false && _votedetails.Count > 0)
            {
                while (_votedetails.Count > 0)
                {
                    VoteDetailObj vdo = _votedetails[0];
                    vdo.Delete();
                }
            }

            _localdelegates = new XPCollection<VoteDetailObj>(Session.DefaultSession, false);
            foreach (VoteDetailObj vdo in _votedetails)
            {
                _localdelegates.Add(vdo);
            }

            if (_votedetails.Count == 0)
            {
                // теперь перезаполняем локальный список депутатов-результатов на основе данных из списка депутатов сессии

                _maxVoteDetailsID = _mainController.GetMaxID("votedetails");

                string ans = "None";
                AnswerTypeObj ato = DataModel.AnswerTypes_GetByCodeName(ans);


                foreach (SesDelegateObj d in DataModel.SesDelegates)
                {
                    if (d.id > 0 && d.idSeat.MicNum > 0 && d.idSeat.LogicalState == true && d.IsCardRegistered == true && d.idDelegate.idType.IsVoteRight == true)
                    {
                        VoteDetailObj vdo = new VoteDetailObj(Session.DefaultSession);
                        _maxVoteDetailsID = _maxVoteDetailsID + 1;

                        vdo.id = _maxVoteDetailsID;
                        vdo.idAnswer = ato;
                        vdo.idDelegate = d;
                        vdo.idSeat = d.idSeat;
                        vdo.idQuestion = _TheQuestion;

                        _votedetails.Add(vdo);
                        _localdelegates.Add(vdo); // в локальный спсиок тоже добавляем - понадобится для отображения
                    }
                }

                _View.SetDelegateProperties(_localdelegates);
            }
            _DelegatesQnty = _votedetails.Count; // очень важно. особый контроль. здесь вычисляется колчиство депутатов, готовых к голосованию. Это количество влияет на результаты

            _SeatDelegatesList.Clear();

            foreach (VoteDetailObj vdo in _votedetails)
            {
                if (vdo.idSeat.MicNum > 0)
                    _SeatDelegatesList[vdo.idSeat.MicNum] = vdo.idDelegate;
            }

        }

        private void InitVoteParams()
        {
/*
            if (_TheVoteResult != null)
            {
                _DelegatesQnty = _TheVoteResult.AvailableQnty;
            }
            else
                _DelegatesQnty = DataModel.GetVoteDelegatesQnty();//DataModel.RegDelegateQnty;
*/

            _View.InitParams(_VoteTime, _DelegatesQnty);
        }

        private void UpdateVoteResult()
        {
            if (_TheVoteResult == null)
            {
                _TheVoteResult = new VoteResultObj();

                _TheVoteResult.idResultValue = _NoResult;
                _TheVoteResult.AvailableQnty = _DelegatesQnty;
            }

            _TheVoteResult.AnsQnty1 = 0;
            _TheVoteResult.AnsQnty2 = 0;
            _TheVoteResult.AnsQnty3 = 0;
            _TheVoteResult.AnsQnty4 = 0;
            _TheVoteResult.AnsQnty5 = 0;

            foreach (VoteDetailObj vdo in _votedetails)
            {
                switch (vdo.idAnswer.CodeName)
                {
                    case "Answer_Yes": _TheVoteResult.AnsQnty1 = _TheVoteResult.AnsQnty1 + 1; break;
                    case "Answer_Abst": _TheVoteResult.AnsQnty2 = _TheVoteResult.AnsQnty2 + 1; break;
                    case "Answer_No": _TheVoteResult.AnsQnty3 = _TheVoteResult.AnsQnty3 + 1; break;
                    case "Answer4": _TheVoteResult.AnsQnty4 = _TheVoteResult.AnsQnty4 + 1; break;
                    case "Answer5": _TheVoteResult.AnsQnty5 = _TheVoteResult.AnsQnty5 + 1; break;
                }
            }
        }


        private ResultValueObj CalcResult(VoteResultObj vr, QuestionObj quest, int regQnty)
        {

            ResultValueObj res;

            int calcQnty = -1;
            switch (quest.idKind.idQntyType.idDelegateQntyType.CodeName)
            {
                case "AssignedQnty":
                    {
                        calcQnty = DataModel.TheSettings.AssignedQnty;
                        break;
                    }

                case "SelectedQnty":
                    {
                        calcQnty = DataModel.TheSettings.SelectedQnty;
                        break;
                    }
                case "RegisteredQnty":
                    {
                        calcQnty = regQnty;
                        break;
                    }
            }

            int needProc = quest.idKind.idQntyType.ProcentValue;
            int qntyZa = vr.AnsQnty1;
            int calcProc = 0;


            double d = (double)0;
           if (calcQnty!= 0)
           {
               d = (((double)qntyZa * (double)100) / (double)calcQnty);
           }

           calcProc = Convert.ToInt32(d);
           if ((d - calcProc) > 0) // если округлил в меньшую сторону, то округлить в большую!
               calcProc = calcProc + 1;

           string codename = "NoResult";
            if (calcProc >= needProc)
            {
                codename = "Parlament_Accept";

            }
            else
            {
                codename = "Parlament_Decline";
            }

            res = DataModel.ResValues_GetObject(codename);

            return res;
        }

#endregion private methods

#region public methods
        // вывести на форму на панель свойств информацию об указанном депутате
        public void ShowDelegateProp(SesDelegateObj theDelegate)
        {
            int idDelegate = -1;
            if (theDelegate != null)
                idDelegate = theDelegate.id;

            CriteriaOperator criteria1 = CriteriaOperator.Parse("idDelegate.id == ?", idDelegate);

            if (_localdelegates != null)
                _localdelegates.Filter = criteria1;
        }

        // вывести на форму на панель свойств информацию об указанном вопросе
        public void ShowQuestProperties(QuestionObj _theQuestion)
        {


            CriteriaOperator cr = new BinaryOperator(
                new OperandProperty("id"), new OperandValue(_TheQuestion.id),
                BinaryOperatorType.Equal);


            _QuestCollection = new XPCollection<QuestionObj>(_TheQuestion.Session, cr);
            _QuestCollection.Load();

            _View.SetQuestionProperties(_QuestCollection);
        }

        // очистить и отменить итоги голосования
        public void Vote_Clear()
        {

            // очищаем список: далее идет странный способ удаления из XPO коллекци, но иного пока не нашел
            if (_TheVoteResult != null)
            {
                _TheVoteResult.idResultValue = _NoResult;
                _TheVoteResult.VoteDateTime = DateTime.Now;
                _TheVoteResult.AnsQnty1 = 0;
                _TheVoteResult.AnsQnty2 = 0;
                _TheVoteResult.AnsQnty3 = 0;
                _TheVoteResult.AnsQnty4 = 0;
                _TheVoteResult.AnsQnty5 = 0;
                _TheVoteResult.AvailableQnty = 0;

                _TheQuestion.idResult.idResultValue = _TheVoteResult.idResultValue;
            }

            _TheQuestion.idTask.State = 0;

/*
            if (_TheVoteResult != null && _TheVoteResult.id != 0)
            {
                DataModel.VoteResults.Remove(_TheVoteResult);
            }
*/
//          InitVoteResult();
            _voteMode = false;

            _View.ShowVoteResults(_TheVoteResult);

//          UpdateSeats();

//          SaveQuestion();

            InitVoteDetails();
            InitSeats();
        }

        // остановить процесс голосования
        public void Vote_Stop()
        {

            // послать на контроллер команду остановки голосования
            ConfHelper.SendCommand_VoteStop();

            _View.StopVoting();

            _voteMode = false;

            if (_TheVoteResult == null)
                return;

            // рассчитать результат голосования
            ResultValueObj rv = CalcResult(_TheVoteResult, _TheQuestion, _DelegatesQnty);

            _TheVoteResult.idResultValue = rv;
            _TheVoteResult.VoteDateTime = DateTime.Now;

            _TheQuestion.idResult.idResultValue = _TheVoteResult.idResultValue;
            _TheQuestion.idResult.VoteDateTime = _TheVoteResult.VoteDateTime;
            _TheQuestion.idTask.State = _TheQuestion.idReadNum.id != 0 ? _TheQuestion.idReadNum.id : 3; // влияет на сортировку привыборе вопросов

            _View.InitParams(_VoteTime, _DelegatesQnty);

            _View.ShowVoteResults(_TheVoteResult);

            Platform.VoteInfoResult info = new Platform.VoteInfoResult();
            info.VotedQnty_A = _TheVoteResult.AnsQnty1;
            info.VotedQnty_B = _TheVoteResult.AnsQnty2;
            info.VotedQnty_C = _TheVoteResult.AnsQnty3;
            info.DelegateQnty = _TheVoteResult.AvailableQnty;

            bool isaccept = false;
            if (_TheVoteResult.idResultValue.CodeName == "Parlament_Accept")
                isaccept = true;

            info.IsResult = isaccept;


            _mainController._MonitorInfo = info;
            _mainController.CurrentActivity = MainCntrler.Activities.VoteResults;
            ShowMonitorPreview("VoteResults", 5000);

            SaveResults();
            MessageBox.Show("Голосование завершено!");
        }

        private void SaveQuestion()
        {
            try
            {
                _TheVoteResult.Save();
                _TheQuestion.idResult.Save();
                _TheQuestion.idTask.Save();
                _TheQuestion.Save();

            }
            catch (Exception ex)
            {
                string err = "Ошибка сохранения в базу данных. Повторите операцию!\n";
                err = err + ex.Message;
                _msgForm.ShowError(err);
            }

            DataModel.Questions.Reload();
        }

        private void SaveResults()
        {

            SaveQuestion();

            try
            {
                DataModel.TheSettings.Save();

                foreach (VoteDetailObj vdo in _votedetails)
                {
                    vdo.Save();
                }
            }
            catch (Exception ex)
            {
                string err = "Ошибка сохранения в базу данных. Повторите операцию!\n";
                err = err + ex.Message;
                _msgForm.ShowError(err);
            }

            DataModel.Questions.Reload();
        }

        // вычислить количество проголосовавших депутатов
        public int GetVoteDelegatesQnty()
        {
            int i = 0;
            foreach (VoteDetailObj vdo in _votedetails)
            {
                if (vdo.idAnswer.CodeName != "None")
                    i++;
            }
            return i;
        }

        public void ShowMonitorPreview(string action, int delay)
        {
            if (action == "VotePrepare")
            {
                _mainController.CurrentActivity = MainCntrler.Activities.VotePrepare;
                _mainController.ShowMonitorPreview();
            }
            else if (action == "VoteStart")
            {
                if (_IsvoteInfoToScreen)
                {
                    _mainController.CurrentActivity = MainCntrler.Activities.VoteStart;
                    _mainController.ShowMonitorPreview();
                }
            }
            else if (action == "VoteResults")
            {
                if (_IsvoteInfoToScreen)
                {
                    _mainController.CurrentActivity = MainCntrler.Activities.VoteResults;
                    _mainController.ShowMonitorPreview();
                }
            }
            else if (action == "QuestionInfo")
            {
/*
                if (_TheQuestion.id > 0)
                    _mainController.CurrentActivity = MainCntrler.Activities.SelectQuestion;
                else
                    _mainController.CurrentActivity = MainCntrler.Activities.VoteResults;
*/
                if (_TheQuestion != null)
                {
                    Platform.QuestionInfo info = new Platform.QuestionInfo();
                    info.Caption = _TheQuestion.Name;
                    info.Description = _TheQuestion.idTask.Description;
                    info.Type = _TheQuestion.idKind.Name;
                    info.VoteTime = _VoteTime;
                    _mainController._MonitorInfo = info;
                    _mainController.ShowMonitorPreview();
                }
            }
            else if (action == "")
            {
                /*
                                if (_TheQuestion.id > 0)
                                    _mainController.CurrentActivity = MainCntrler.Activities.SelectQuestion;
                                else
                                    _mainController.CurrentActivity = MainCntrler.Activities.VoteResults;
                */
                _mainController.CurrentActivity = MainCntrler.Activities.None;
                _mainController.ShowMonitorPreview();
            }
        }

        public void ShowMonitorPreview(string action)
        {
            ShowMonitorPreview(action, 5000);
        }

#endregion  public methods

    }
}
