﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;
using Platform;
using System.Runtime.Serialization;
using System.IO;
using System.Threading;

namespace VoteSystem
{
    public class MainCntrler: IDisposable
    {
        NetHelper _netHelper;

        MainForm _View;
        StartMenuForm _StartMenuForm;

        // пул контроллеров
        SelQuestionsCntrler _selquestionsCntrler;
        SelStatementCntrler _selstatementsCntrler;

        VoteQuestionsCntrler _votequestController;
        DelegatesSesCntrler _sesdelegatesController;
        HallCntrler _hallCntrler;
        DelegatesCntrler _delegatesCntrler;
        QuestionsCntrler _questCntrler;
        VoteModeCntrler _votemodeCntrler;
        SpeechModeCntrler _speechmodeCntrler;
        SessionCntrler _sessionCntrler;
        SettingsCntrler _settingsCntrler;
        ReportCntrler _reportCntrler;

        // пул форм и въюшек
        VoteModeForm _votemodeForm;
        SpeechModeForm _speechmodeForm;
        QuestionsForm _questForm;
        DelegatesForm _delegatesForm;
        MonitorPreviewForm _mpf;
        MonitorPreviewForm2 _mpf2;
        SettingsForm _settingsForm;
        ReportForm _reportForm;
        ReportQuest _reportQForm;
        ReportThemes _reportTForm;
        ReportDelegate _reportDForm;

        bool _panelEnabled = true;
        int _generalticks = 0;

        public enum Activities {None, Session, SelectDelegate, SelectQuestion, RegistrationInfo, VoteResults, VotePrepare, VoteStart, RegPrepare, RegStart, SpeechStart }

        Activities _CurrentActivity = new Activities();
        public Object _MonitorInfo; // структура, которая содержит текущую информацию для вывода на мониторы

        public MainCntrler(MainForm view)
        {
            try
            {
                string conn = DevExpress.Xpo.DB.MySqlConnectionProvider.GetConnectionString("localhost", "root",
          "atanor", "votesystem");
                XpoDefault.DataLayer = XpoDefault.GetDataLayer(conn, AutoCreateOption.DatabaseAndSchema);
                XpoDefault.Session = new Session();
                var ses = Session.DefaultSession;
            }
            catch (Exception Ex)
            {
                System.Windows.Forms.MessageBox.Show("Отсутствует соединение с сервером базы данных.\n Проверьте ip-адреса, соединение и работосопосбность сервера голосования.");
            }

            _View = view;

            if (System.Reflection.Assembly.GetEntryAssembly().GetName().Name == "StateSystem")
            {
                DataModel.IsSpeechApp = true;
            }

            _votemodeCntrler        = new VoteModeCntrler(this);
            _speechmodeCntrler = new SpeechModeCntrler(this);

            _votequestController     = new VoteQuestionsCntrler(this);
            _selquestionsCntrler    = new SelQuestionsCntrler(this);
            _selstatementsCntrler    = new SelStatementCntrler(this);

            _sesdelegatesController = new DelegatesSesCntrler(this);
            _hallCntrler            = new HallCntrler(this);
            _delegatesCntrler       = new DelegatesCntrler(this);
            _questCntrler           = new QuestionsCntrler(this);
            _sesdelegatesController = new DelegatesSesCntrler(this);
            _sessionCntrler         = new SessionCntrler(this);
            _settingsCntrler = new SettingsCntrler(this);
            _reportCntrler = new ReportCntrler(this);

            _votemodeForm   = new VoteModeForm(_votemodeCntrler, this, view);
            _speechmodeForm = new SpeechModeForm(_speechmodeCntrler, this, view);
            _questForm      = new QuestionsForm(_questCntrler, this, view);
            _delegatesForm  = new DelegatesForm(_delegatesCntrler, this, view);
            _settingsForm = new SettingsForm(_settingsCntrler);
            _reportForm = new ReportForm(_reportCntrler);
            _reportQForm = new ReportQuest(_reportCntrler);
            _reportTForm = new ReportThemes(_reportCntrler);
            _reportDForm = new ReportDelegate(_reportCntrler);

            _StartMenuForm = new StartMenuForm(this);

            _mpf             = new MonitorPreviewForm();
            _mpf2            = new MonitorPreviewForm2();

            _votemodeCntrler.AssignView((IVoteModeForm)_votemodeForm);
            _speechmodeCntrler.AssignView((ISpeechModeForm)_speechmodeForm);

            _delegatesCntrler.AssignView((IDelegatesForm)_delegatesForm);
            _questCntrler.AssignView((IQuestionsForm)_questForm, (IResultShow)_questForm);

            _votequestController.AssignView(_View);
            _selquestionsCntrler.AssignView(_View);
            _selstatementsCntrler.AssignView(_View);
            _hallCntrler.AssignView(_View);
            _sesdelegatesController.AssignView(_View);
            _sessionCntrler.AssignView(_View);
            _settingsCntrler.AssignView((ISettingsForm)_settingsForm);
            _reportCntrler.AssignView((IReportForm)_reportForm, (IReportQuest)_reportQForm, (IReportThemes)_reportTForm, (IReportDelegate)_reportDForm);
            _CurrentActivity = Activities.None;
        }

        public object GetSubController(string ControllerName)
        {
            switch (ControllerName)
            {
                case "HallPage Controller":
                    return _hallCntrler;
                case "Delegate List Controller":
                    return _delegatesCntrler;
                case "Questions List Controller":
                    return _questCntrler;
                case "VoteQuestions Page Controller":
                    return _votequestController;
                case "Select Questions Page Controller":
                    return _selquestionsCntrler;
                case "Select Statements Page Controller":
                    return _selstatementsCntrler;
                case "Session Delegates Controller":
                    return _sesdelegatesController;
                case "Session Page Controller":
                    return _sessionCntrler;
                case "Report Controller":
                    return _reportCntrler;

            }

            return null;
        }


        public object GetSubForm(string FormName)
        {
            switch (FormName)
            {
                case "SpeechModeForm":
                    {
                        _speechmodeForm = new SpeechModeForm(_speechmodeCntrler, this, _View);
                        return _speechmodeForm;
                    }
            }

            return null;
        }

        public Activities CurrentActivity
        {
            get { return _CurrentActivity; }
            set {_CurrentActivity = value;}
        }

        public bool PanelEnabled
        {
            get { return _panelEnabled; }
            set { _panelEnabled = value; }
        }

        public void Init(ref XPCollection SessionQuestions/*, ref XPCollection SessionStatements*/)
        {
            DataModel.Init();
            DataModel.LoadSession();

            _sesdelegatesController.Init(ref DataModel.SesDelegates);
            _selquestionsCntrler.Init(ref DataModel.SesTasks);
            _votequestController.Init();
            _selstatementsCntrler.Init(ref DataModel.SesStatements/*ref SessionStatements*/);

            _hallCntrler.Init();
            _sessionCntrler.Init();

            _settingsCntrler.Init();

            _netHelper = new NetHelper();

            if (DataModel.IsSpeechApp == true)
            {
                ConfHelper.Init("statem", "vote");
//              ConfHelper.SendCommand_MicResetAll(); // при запуске приложения сбрасываем микрофоны
            }
            else
            {
                ConfHelper.Init("vote", null);

                ConfHelper.OnProcessMsg = Conf_ProccessMsg;
                ConfHelper.OnGetDelegatesRFID = Conf_ProccessCmd;
            }


            ConfHelper.StartDataRecieve();

            ShowSplahToMonitors();

        }

        public void OnMainFormShown()
        {
            if (DataModel.IsSpeechApp == true)
            {
                _View.SetStatementAppView();
                _View.ShowPage("itmRegistration");
                _View.ShowPage("itmQuestions");
            }
            else
            {
                _View.ShowPage("itmRegistration");
            }

            ConfHelper.Connect();

            if (DataModel.IsSpeechApp == true)
            {
                _selstatementsCntrler.StartTheme();
            }
            else
            {
//                Conf_ProccessCmd(ConfHelper.CmdEnum.GetRFID); // послать на палантир всю таблицу rfid, не дожидаясь запроса от него

//                _StartMenuForm.ShowDialog();
            }

            string s = "Система голосования v 1.1 - " + DataModel.TheSession.Code;
            _View.SetCaption(s);
        }

        public void ShowStartMenu()
        {
            _StartMenuForm.ShowDialog();
        }

        public void SetMessageOwner(Form ownWnd)
        {
            _View._messages.SetMessageOwner(ownWnd);
        }

        public bool OnGeneralTimer()
        {
            if (_netHelper._IsConnectedState == false) // ВОТ ЭТО ДА! и куда делось соединение с сервером???
            {
                string Err = "";
                Err = "DBase connect error";
                _netHelper.Dispose();

                string err = "Прервалось соединение с базой данных!\nПроверьте сервер базы данных, наличие сетевого соединения и перезапустите приложение.";
                //              err = "errorcode = " + ex.ErrorCode.ToString() + "   Number = " + ex.Number.ToString();
                _View._messages.ShowError(err);

                Application.Exit();
//              _View.Close();
                return false;

/*
                if (_netHelper.Exception != null)
                    throw (_netHelper.Exception);
 */ 
            }

            _generalticks++;

            if ((_generalticks % 10) == 0)
            {

                if (DataModel.IsSpeechApp == true && DataModel.TheSettings.IsCardMode == false) // проверять, только если режим карточек отключен
                {
                    bool needrestart = false;
                    try
                    {
                        bool IsChanged = DataModel.CheckDBaseChanges(ref needrestart);

                        if (IsChanged == true)
                        {
                            if (needrestart)
                            {
                                ShowWarningInfo("Изменился состав депутатов сессии!");
                                _speechmodeCntrler.ShowWarningInfo("Изменился состав депутатов сессии! Требуется перезагрузка программы.");
                            }
                            else
                            {
                                ShowWarningInfo("Обнаружено несоответствие в рассадке депутатов.");
                                _speechmodeCntrler.ShowWarningInfo("Обнаружено несоответствие в рассадке депутатов. Требуется перезагрузка программы.");

//                              _hallCntrler.IdentifyCards_auto();
//                              _View._messages.ShowWarningMsg("Обнаружено несоответствие в рассадке депутатов.\n Требуется перезагрузка программы.");
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        _View._messages.ShowError("Произошла ошибка при проверке изменений рассадки депутатов.");
                    }
                }
 
            }

            if ((_generalticks % 5) == 0)
            {
                if (DataModel.IsSpeechApp == true ) 
                {
                    _selstatementsCntrler.refresh(); // обновить состояния вопросов в списке тем
                }
            }


            ConfHelper.MsgState ms = new ConfHelper.MsgState();
            bool IsMsg = ConfHelper.GetData_Msg(ref ms);
            if (IsMsg)
            {
                HistoryAdd(ms.Caption, ms.Text);
            }
            return true;

        }

        public void ShowWarningInfo(string warning)
        {
            _View._messages.ShowWarningMsg("Произошли изменения в рассадке депутатов!\nТребуется перезагрузка программы.");
        }

        public void OnChangeSeating()
        {
            DataModel._IsChangedSeating = true;
            if (DataModel.IsSpeechApp == true)
            {
                _speechmodeCntrler.Reload();
            }
        }

        public void DriverStop()
        {
            ConfHelper.SendCommand("driver", "stop");
            ConfHelper.InitMicText();

            _View._messages.ShowWarningMsg("Драйвер конференц-системы остановлен!");
            HistoryAdd("СГ", "Драйвер конференц-системы остановлен");
        }

        public void DriverStart()
        {
            ConfHelper.SendCommand("driver", "start");
            ConfHelper.InitMicText();

            _View._messages.ShowWarningMsg("Драйвер конференц-системы запущен!");
            HistoryAdd("СГ", "Драйвер конференц-системы запущен");
        }

        public void DriverMicHit()
        {
            ConfHelper.SendCommand("allmic", "on");

            _View._messages.ShowWarningMsg("Прогрев микрофонов включен!");

        }

        public void ShowGlobalDelegates()
        {
            _delegatesCntrler.ShowViewManageMode();
        }

        public void ShowReportGlobalDelegates()
        {
            _delegatesCntrler.SelectItemsReport();
        }

        public void ShowGlobalQuestions()
        {
            _questCntrler.ShowView();
        }

        public void PrintReport()
        {
            if (DataModel.IsSpeechApp == false)
            {
                _reportCntrler.ShowView();
            }
            else
            {
                _reportCntrler.ShowReport_T();
            }
        }

        public void Save()
        {
            _sessionCntrler.SaveSession();
        }


        public void ShowMonitorPreview()
        {
            if (PanelEnabled == false)
                return;


            NetObject no = new NetObject();

            if (_CurrentActivity == Activities.Session)
            {
//              _mpf.ShowSession(DataModel.TheSession);

                no.ScreenType = "screen_session";
                no.Info = Serialize(_MonitorInfo);
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.SelectDelegate)
            {
                no.ScreenType = "screen_delegate";
                no.Info = Serialize(_MonitorInfo);
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.RegistrationInfo)
            {
                no.ScreenType = "screen_reginfo";
                no.Info = Serialize(_MonitorInfo);
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.SelectQuestion)
            {
                no.ScreenType = "screen_question";
                no.Info = Serialize(_MonitorInfo);
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.VoteResults)
            {
                no.ScreenType = "screen_votefinish";
                no.Info = Serialize(_MonitorInfo);
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.VotePrepare)
            {
                no.ScreenType = "screen_voteprepare";
                no.Info = Serialize(_MonitorInfo);
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.VoteStart)
            {
                no.ScreenType = "screen_votestart";
                no.Info = Serialize(_MonitorInfo);
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.RegPrepare)
            {
                no.ScreenType = "screen_regprepare";
                no.Info = new byte[0];
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.RegStart)
            {
                no.ScreenType = "screen_regstart";
                no.Info = Serialize(_MonitorInfo);
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.SpeechStart)
            {
                no.ScreenType = "screen_speechstart";
                no.Info = Serialize(_MonitorInfo);
                _netHelper.SendNetObject(ref no);
            }
            else if (_CurrentActivity == Activities.None)
            {
                ShowSplahToMonitors();
                return;
            }
        }

        public void ClearMonitor()
        {
            _View.ClearMonitor();
        }

        public void InitPage(string pageName)
        {
            if (PanelEnabled)
                _View.EnableInfoToMonitor(true);

            _View.ClearMonitor();

            if (pageName == "pgSession")
            {
                _sessionCntrler.InitPage();
            }
            else if (pageName == "pgDelegates")
            {
                _sesdelegatesController.InitPage();
            }
            else if (pageName == "pgSelQuestions")
            {
                    _selquestionsCntrler.InitPage();
            }
            else if (pageName == "pgStatements")
            {
                if (DataModel.IsSpeechApp == true)
                    _selstatementsCntrler.InitPage();
            }
            else if (pageName == "pgQuorumGraphic")
            {
                _hallCntrler.InitPage();
            }
            else if (pageName == "pgQuestions")
            {
                if (DataModel.IsSpeechApp == false)
                    _votequestController.InitPage();
/*
                else
                    _resspeechController.InitPage();
 */ 
            }
        }

        public void ShowSettings()
        {
            if (DataModel.IsSpeechApp == false)
                _settingsCntrler.ShowView();
        }

        public void Dispose()
        {
            _CurrentActivity = Activities.None;
            ConfHelper.Finish();
            if (_netHelper != null)
                _netHelper.Dispose();
            ShowSplahToMonitors();
        }

        public void OnClosing()
        {
            _hallCntrler.OnClosing();
        }

        public void CreateNewSession(string sesName)
        {
            NewSessionForm ns = new NewSessionForm();
            DialogResult dr = ns.ShowDialog();
            if (dr == DialogResult.OK)
            {
                sesName = ns.SessionName;
            }
            else
                return;

            DataModel.Sessions_Finish(DataModel.TheSession);

            SessionObj ses = new SessionObj(Session.DefaultSession);
            ses.Code = sesName;
            ses.StartDate = DateTime.Now;
            ses.IsFinished = false;

            int maxID = GetMaxID("sessions");
            DataModel.Sessions_AddNew(ses, maxID);

            maxID = GetMaxID("SesDelegates");
            DataModel.Sessions_CreateSesDelegates(ses, maxID);
            RestartApplication();
        }

        public void ShowSessions()
        {
            LoadSessionForm viewses = new LoadSessionForm(this);
            viewses.ShowDialog();
        }

        private void Conf_ProccessMsg(int msgLevel, string caption, string msg)
        {
            if (msgLevel == 0)
            {
                MsgForm msgform = new MsgForm();

                IMsgMethods messages = (IMsgMethods)msgform;
                messages.ShowWarningMsg(msg, caption);
            }

//          HistoryAdd(caption, msg);
        }

        private void Conf_ProccessCmd(ConfHelper.CmdEnum cmdType)
        {
            switch (cmdType)
            {
                case ConfHelper.CmdEnum.GetRFID:
                    {
                        _delegatesCntrler.SendCardCodeToDriver();
                        break;
                    }
            }
        }

        public void HistoryAdd(string caption, string msg)
        {
            _sessionCntrler.HistoryAdd(caption, msg);

            string histmsg = DateTime.Now.ToString("t") + " "+ caption + ": <<" + msg + ">>";
            _View.ShowHistoryMsg(histmsg);
        }

#region Функции для поддержки и взаимодействия субконтроллеров

/*
        public void VoteModeRun(SesTaskObj questionToVote)
        {
            _View.EnableInfoToMonitor(false);

            if (questionToVote != null)
            {
                _votemodeCntrler.Init(questionToVote);
                _votemodeCntrler.ShowForm();

                QuestionObj ResultQuestion = _votemodeCntrler.CurrentQuestion;

                if (ResultQuestion.idResult.IsResult)
                {
                    // необходимо вопрос тогда убрать из повестки дня
                    _selquestionsCntrler.MoveQuestion(ResultQuestion);
                    _votequestController.SetPosition(ResultQuestion);
                }

                _selquestionsCntrler.AppendItem();

//              _View.ShowPage("itmQuestions");
            }
        }
*/
        public void VoteModeRun(QuestionObj q, bool IdentifyCardsMode)
        {
            VoteModeRun(q, IdentifyCardsMode, false);
        }

        public void VoteModeRun(QuestionObj q, bool IdentifyCardsMode, bool fastMode)
        {
            _hallCntrler.SetTextOutAutoProcessOff();
            if (q != null)
            {
                if (DataModel.TheSettingsHall.IsAutoCardMode == false)
                    IdentifyCardsMode = false;
                _votemodeCntrler.IdentifyCardsMode = IdentifyCardsMode;
                _votemodeCntrler.Init(q, fastMode);
                _votemodeCntrler.ShowForm();
            }
            _hallCntrler.SetTextOutAutoProcessOn();
        }

        public void SetTextOutAutoProcessPause()
        {
            _hallCntrler.SetTextOutAutoProcessPause();
        }

/*
        public void StatementDetailsRun(SesStatementObj s)
        {
            if (s != null)
            {
                _speechmodeCntrler.Init(s);
                _speechmodeCntrler.ShowForm();
            }
        }
*/
        public void StatementDetailsRun(StatementObj s, bool IsViewMode)
        {
            if (s != null)
            {
                _speechmodeCntrler.ViewMode = IsViewMode;
                bool b = _speechmodeCntrler.Init(s);
                if (b)
                    _speechmodeCntrler.ShowForm();
            }
        }

        public void CloseOpenStatements()
        {
            StatementObj statem = _selstatementsCntrler.findOpenStatement();
            _speechmodeCntrler.finishStatem(statem);
        }

        public int CalcQuorumQnty()
        {
            return _hallCntrler.CalcQuorumQnty();
        }
        // получить максимальный идентификатор вопроса из списка
        public int GetMaxID(string tablename)
        {
            int tableid = 0;

            switch (tablename)
            {
                case "SesQuestions":
                    tableid = 1; break;
                case "Questions":
                    tableid = 2; break;
                case "DelegateStatements":
                    tableid = 3; break;
                case "Statements":
                    tableid = 4; break;
                case "SesTasks":
                    tableid = 5; break;
                case "SesDelegates":
                    tableid = 6; break;
                case "votedetails":
                    tableid = 7; break;
                case "questions_results":
                    tableid = 8; break;
                case "sessions":
                    tableid = 9; break;

            }

            SqlParameter[] sql_params = new SqlParameter[2];

            SqlParameter param = new SqlParameter();
            param.ParameterName = "tableID";
            param.SqlDbType = SqlDbType.Int;
            param.Value = tableid;
            param.Direction = ParameterDirection.Input;

            sql_params[0] = param;

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "maxID";
            param2.SqlDbType = SqlDbType.Int;
            param2.Value = -1;
            param2.Direction = ParameterDirection.Output;

            sql_params[1] = param2;

            Dictionary<string, object> OutParams = new Dictionary<string, object>();

            ExecuteSP("GetMaxID", sql_params, OutParams);

            int idMax = 0;
            if (OutParams["maxID"] != DBNull.Value)
                idMax = (int)OutParams["maxID"];

            return idMax;
        }

        public void EnableMonitors(bool IsEnable)
        {
            PanelEnabled = IsEnable;
            _View.EnableMonitorButtons(IsEnable);
        }

        public void SwitchMonitors(bool IsEnable)
        {
            _View.EnableMonitor(IsEnable);
        }

        public void ShowSplahToMonitors()
        {
            NetObject no = new NetObject();
            no.enCmdType = 3;

            Platform.SessionInfo info = new Platform.SessionInfo();
            //          info.CurrDelegate = curr_delegate.idDelegate.FullName;
            info.Caption = DataModel.TheSession.Code;// + ". " + DataModel.TheSession.Caption;
//          info.Description = DataModel.TheSession.Content;
            _MonitorInfo = info;

            no.Info = Serialize(_MonitorInfo);
            _netHelper.SendNetObject(ref no);
/*
            if (DataModel.IsSpeechApp == false)
            {
                ConfHelper.SendCommand_SpeechStart();
            }
 */ 
        }

        public string GetDocFile_NewConvoc()
        {
            string s = DataModel.TheSettings.NewConvocDocFile;
            if (DataModel.TheSettings.Path_Doc != "")
                s = DataModel.TheSettings.Path_Doc + s;
            return s;

        }

        public string GetDocFile_LoadConvoc()
        {
            string s = DataModel.TheSettings.LoadConvocDocFile;
            if (DataModel.TheSettings.Path_Doc != "")
                s = DataModel.TheSettings.Path_Doc + s;
            return s;

        }

        public void RestartApplication()
        {
            try
            {
                Application.Restart();
                Application.Exit();
            }
            catch (Exception Ex)
            {
                Application.Restart();
                Application.Exit();
            }
        }

        public void ExecuteSP(string ProcName, SqlParameter[] sql_params, Dictionary<string, object> OutParams)
        {
            MySqlCommand myCommand = new MySqlCommand(ProcName);
            myCommand.Connection = new MySqlConnection(Session.DefaultSession.Connection.ConnectionString);
            myCommand.Connection.Open();
            myCommand.CommandType = CommandType.StoredProcedure;
            try
            {

                foreach (SqlParameter sql_param in sql_params)
                {
                    MySqlParameter p = new MySqlParameter();
                    p.ParameterName = sql_param.ParameterName;
                    p.DbType = sql_param.DbType;
                    p.Size = sql_param.Size;
                    p.Value = sql_param.Value;
                    p.Direction = sql_param.Direction;

                    myCommand.Parameters.Add(p);
                }

                    myCommand.ExecuteNonQuery();
                    if (OutParams != null)
                    {
                        foreach (MySqlParameter p in myCommand.Parameters)
                        {
                            if (p.Direction == ParameterDirection.Output || p.Direction == ParameterDirection.InputOutput)
                            {
                                OutParams[p.ParameterName] = p.Value;
                            }
                        }
                    }
            }
            catch (Exception Ex)
            {
                string Err = "";
                if (Ex.Message == "Connection must be valid and open")
                    Err = "Отсутствует соединение с базой данных";
                else
                {
                    Err = "Ошибка выполнения процедуры " + ProcName + "\n\r";
                    Err = Err + Ex.Message;
                }
                throw (new Exception(Err));
            }

            myCommand.Connection.Close();
        }

        static public byte[] Serialize(Object ObjToSerialize)
        {
            if (ObjToSerialize == null)
                return null;

            // теперь потребуется получить байтовый массив 

            // To serialize the hashtable and its key/value pairs,  
            // you must first open a stream for writing. 
            // In this case, use a file stream.
            MemoryStream ms = new MemoryStream();

            // Construct a BinaryFormatter and use it to serialize the data to the stream.
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            try
            {
                formatter.Serialize(ms, ObjToSerialize);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                ms.Close();
            }

            byte[] result = ms.ToArray();

            return result;
        }

        #endregion
    }
}
