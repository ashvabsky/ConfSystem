namespace VoteSystem
{
    partial class StartMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartMenuForm));
            this.btnDelegates = new DevExpress.XtraEditors.SimpleButton();
            this.RibbonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNewSession = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewSession = new DevExpress.XtraEditors.SimpleButton();
            this.btnNewConvoc = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadConvoc = new DevExpress.XtraEditors.SimpleButton();
            this.btnSettings = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuestions = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDelegates
            // 
            this.btnDelegates.Appearance.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDelegates.Appearance.Options.UseFont = true;
            this.btnDelegates.ImageIndex = 8;
            this.btnDelegates.ImageList = this.RibbonImages;
            this.btnDelegates.Location = new System.Drawing.Point(62, 364);
            this.btnDelegates.Name = "btnDelegates";
            this.btnDelegates.Size = new System.Drawing.Size(327, 45);
            this.btnDelegates.TabIndex = 0;
            this.btnDelegates.Text = "����� �� ���������";
            this.btnDelegates.Click += new System.EventHandler(this.btnDelegates_Click);
            // 
            // RibbonImages
            // 
            this.RibbonImages.ImageSize = new System.Drawing.Size(36, 36);
            this.RibbonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("RibbonImages.ImageStream")));
            this.RibbonImages.Images.SetKeyName(7, "folder_document.png");
            this.RibbonImages.Images.SetKeyName(8, "folder_personal.png");
            // 
            // btnNewSession
            // 
            this.btnNewSession.Appearance.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNewSession.Appearance.Options.UseFont = true;
            this.btnNewSession.ImageIndex = 5;
            this.btnNewSession.ImageList = this.RibbonImages;
            this.btnNewSession.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnNewSession.Location = new System.Drawing.Point(62, 40);
            this.btnNewSession.Name = "btnNewSession";
            this.btnNewSession.Size = new System.Drawing.Size(327, 47);
            this.btnNewSession.TabIndex = 1;
            this.btnNewSession.Text = "������ ����� ������";
            this.btnNewSession.Click += new System.EventHandler(this.btnNewSession_Click);
            // 
            // btnViewSession
            // 
            this.btnViewSession.Appearance.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnViewSession.Appearance.Options.UseFont = true;
            this.btnViewSession.ImageIndex = 2;
            this.btnViewSession.ImageList = this.RibbonImages;
            this.btnViewSession.Location = new System.Drawing.Point(62, 106);
            this.btnViewSession.Name = "btnViewSession";
            this.btnViewSession.Size = new System.Drawing.Size(327, 47);
            this.btnViewSession.TabIndex = 2;
            this.btnViewSession.Text = "��������� ������";
            this.btnViewSession.Click += new System.EventHandler(this.btnViewSession_Click);
            // 
            // btnNewConvoc
            // 
            this.btnNewConvoc.Appearance.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNewConvoc.Appearance.Options.UseFont = true;
            this.btnNewConvoc.ImageIndex = 1;
            this.btnNewConvoc.ImageList = this.RibbonImages;
            this.btnNewConvoc.Location = new System.Drawing.Point(62, 172);
            this.btnNewConvoc.Name = "btnNewConvoc";
            this.btnNewConvoc.Size = new System.Drawing.Size(327, 45);
            this.btnNewConvoc.TabIndex = 3;
            this.btnNewConvoc.Text = "������ ����� �����";
            this.btnNewConvoc.Click += new System.EventHandler(this.btnNewConvoc_Click);
            // 
            // btnLoadConvoc
            // 
            this.btnLoadConvoc.Appearance.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnLoadConvoc.Appearance.Options.UseFont = true;
            this.btnLoadConvoc.ImageIndex = 6;
            this.btnLoadConvoc.ImageList = this.RibbonImages;
            this.btnLoadConvoc.Location = new System.Drawing.Point(62, 236);
            this.btnLoadConvoc.Name = "btnLoadConvoc";
            this.btnLoadConvoc.Size = new System.Drawing.Size(327, 45);
            this.btnLoadConvoc.TabIndex = 4;
            this.btnLoadConvoc.Text = "��������� ����� �� ������";
            this.btnLoadConvoc.Click += new System.EventHandler(this.btnLoadConvoc_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Appearance.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSettings.Appearance.Options.UseFont = true;
            this.btnSettings.ImageIndex = 3;
            this.btnSettings.ImageList = this.RibbonImages;
            this.btnSettings.Location = new System.Drawing.Point(62, 300);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(327, 45);
            this.btnSettings.TabIndex = 5;
            this.btnSettings.Text = "���������";
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton7.ImageIndex = 0;
            this.simpleButton7.ImageList = this.RibbonImages;
            this.simpleButton7.Location = new System.Drawing.Point(62, 523);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(327, 45);
            this.simpleButton7.TabIndex = 6;
            this.simpleButton7.Text = "�������";
            // 
            // btnQuestions
            // 
            this.btnQuestions.Appearance.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnQuestions.Appearance.Options.UseFont = true;
            this.btnQuestions.ImageIndex = 7;
            this.btnQuestions.ImageList = this.RibbonImages;
            this.btnQuestions.Location = new System.Drawing.Point(62, 427);
            this.btnQuestions.Name = "btnQuestions";
            this.btnQuestions.Size = new System.Drawing.Size(327, 45);
            this.btnQuestions.TabIndex = 7;
            this.btnQuestions.Text = "����� �� ��������";
            this.btnQuestions.Click += new System.EventHandler(this.btnQuestions_Click);
            // 
            // StartMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 596);
            this.Controls.Add(this.btnQuestions);
            this.Controls.Add(this.simpleButton7);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnLoadConvoc);
            this.Controls.Add(this.btnNewConvoc);
            this.Controls.Add(this.btnViewSession);
            this.Controls.Add(this.btnNewSession);
            this.Controls.Add(this.btnDelegates);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StartMenuForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "������� ����������� v 1.1";
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnDelegates;
        private DevExpress.XtraEditors.SimpleButton btnNewSession;
        private DevExpress.XtraEditors.SimpleButton btnViewSession;
        private DevExpress.XtraEditors.SimpleButton btnNewConvoc;
        private DevExpress.XtraEditors.SimpleButton btnLoadConvoc;
        private DevExpress.XtraEditors.SimpleButton btnSettings;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.Utils.ImageCollection RibbonImages;
        private DevExpress.XtraEditors.SimpleButton btnQuestions;
    }
}