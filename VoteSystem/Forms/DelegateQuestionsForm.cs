﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;

namespace VoteSystem
{
//    public interface IResultShow
//    IResultShow is defined in QuestionsForm.cs

    public interface IDelegateQuestionsForm
    {
        IMsgMethods GetShowInterface();
        void EnableButtons(bool IsVoted);
        void InitControls(XPCollection<VoteDetailObj> questions, DelegateObj currdelegate);

        DialogResult ShowView();
    }

    public partial class DelegateQuestionsForm : DevExpress.XtraEditors.XtraForm, IDelegateQuestionsForm, IResultShow
    {
        DelegateQuestionsCntrler _Controller;
        IMsgMethods _msgForm;

        public DelegateQuestionsForm()
        {
            InitializeComponent();
        }

        public DelegateQuestionsForm(DelegateQuestionsCntrler cntrler, MainCntrler mcontr, MainForm mform)
        {
            InitializeComponent();
            _Controller = cntrler;

            _msgForm = (IMsgMethods) new MsgForm(this);
        }

        #region реализация интерфейса IQuestionsForm

        IMsgMethods IDelegateQuestionsForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        DialogResult IDelegateQuestionsForm.ShowView()
        {
            return ShowDialog();
        }


        void IDelegateQuestionsForm.EnableButtons(bool IsVoted)
        {
        }

        void IDelegateQuestionsForm.InitControls(XPCollection<VoteDetailObj> questions, DelegateObj currdelegate)
        {
            gridQuestions.DataSource = questions;
            dataNavigator1.DataSource = questions;
            PropertiesControl.DataSource = questions;
            if (currdelegate != null)
                this.Text = "Статистика по депутату: " + currdelegate.FullName;
            else
                this.Text = "Статистика по группе депутатов";
        }

        #endregion

        #region реализация интерфейса IResultShow
        void IResultShow.ShowVoteResults(VoteResultObj vro)
        {
            if (vro.id != 0)
            {
                grpVoteResults.Visible = true;
                lblVoteQntytxt.Visible = true;

//              lblVoteDateTime.Visible = true;

//              lblDecision.Text = vro.idResultValue.Name;
//              lblVoteDateTime.Text = vro.VoteDateTime.ToString("t");

                lblVoteAye.Text = vro.AnsQnty1.ToString();
                lblVoteAgainst.Text = vro.AnsQnty3.ToString();
                lblVoteAbstain.Text = vro.AnsQnty2.ToString();

                int votes = vro.AnsQnty1 + vro.AnsQnty2 + vro.AnsQnty3;

                if (vro.AvailableQnty != 0)
                {
                    int votes_proc = (votes * 100) / vro.AvailableQnty;

                    int procAye = (vro.AnsQnty1 * 100) / vro.AvailableQnty;
                    int procAgainst = (vro.AnsQnty3 * 100) / vro.AvailableQnty;
                    int procAbstain = (vro.AnsQnty2 * 100) / vro.AvailableQnty;

                    lblProcAye.Text = procAye.ToString();
                    lblProcAgainst.Text = procAgainst.ToString();
                    lblProcAbstain.Text = procAbstain.ToString();

                    int notvoted = vro.AvailableQnty - votes;
                    int notvoted_proc = (notvoted * 100) / vro.AvailableQnty;

                    lblNonVoteQnty.Text = notvoted.ToString();
                    lblNonProcQnty.Text = notvoted_proc.ToString();

                    lblVoteQnty.Text = string.Format("{0} из {1}", votes, vro.AvailableQnty);
                    lblProcQnty.Text = string.Format("{0}", votes_proc);
                }
                else
                {
                    lblProcAye.Text = "0";
                    lblProcAgainst.Text = "0";
                    lblProcAbstain.Text = "0";

                    lblNonVoteQnty.Text = "0";
                    lblNonProcQnty.Text = "0";

                    lblVoteQnty.Text = "0";
                    lblProcQnty.Text = "0";
                }
            }
            else
            {
//              lblDecision.Text = "Нет решения";
                grpVoteResults.Visible = false;
                lblVoteQntytxt.Visible = false;

//              lblVoteDateTime.Visible = false;

                lblVoteQnty.Text = "";
                lblProcQnty.Text = "";
            }

        }
        #endregion

        private void DelegateQuestionsForm_Load(object sender, EventArgs e)
        {
            _Controller.OnInitForm();
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            DataNavigator dn = (DataNavigator)sender;
            if (dn == null)
                return;

            _Controller.DataPositionChanged(dn.Position);
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            _Controller.ShowSelQuestionDetails();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            _Controller.PrintReport();
        }
    }
}