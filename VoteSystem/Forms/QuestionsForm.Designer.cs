﻿namespace VoteSystem
{
    partial class QuestionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuestionsForm));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.xpQuestions = new DevExpress.Xpo.XPCollection();
            this.gridViewQuestions = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCaption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VoteKind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSessionCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsResult = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.grdSessions = new DevExpress.XtraGrid.GridControl();
            this.xpSessions = new DevExpress.Xpo.XPCollection();
            this.gridViewSession = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnQuestDetails = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.dataNavigator2 = new DevExpress.XtraEditors.DataNavigator();
            this.panelbtnSelect = new DevExpress.XtraEditors.PanelControl();
            this.btnSelect = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.PropertiesControl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.catCharacteristics = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowCaption = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTaskItem_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowidKind_Name = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowCrDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowReadNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmendment_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDescription = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catNotes = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowNotes = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catResult = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowResult = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowResDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.lblProcQnty = new System.Windows.Forms.Label();
            this.grpVoteResults = new System.Windows.Forms.GroupBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.lblProcAye = new System.Windows.Forms.Label();
            this.lblVoteAye = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.lblNonProcQnty = new System.Windows.Forms.Label();
            this.lblNonVoteQnty = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.lblProcAgainst = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.lblProcAbstain = new System.Windows.Forms.Label();
            this.lblVoteAgainst = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.lblVoteAbstain = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.domainUpDown5 = new System.Windows.Forms.DomainUpDown();
            this.label91 = new System.Windows.Forms.Label();
            this.lblVoteQntytxt = new System.Windows.Forms.Label();
            this.lblVoteQnty = new System.Windows.Forms.Label();
            this.session1 = new DevExpress.Xpo.Session();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem2 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem3 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barLargeButtonItem4 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barEditItem3 = new DevExpress.XtraBars.BarEditItem();
            this.barEditItem4 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barLargeButtonItem5 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.xpVoteTypes = new DevExpress.Xpo.XPCollection();
            this.xpVoteKinds = new DevExpress.Xpo.XPCollection();
            this.bar2 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSessions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpSessions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSession)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelbtnSelect)).BeginInit();
            this.panelbtnSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            this.grpVoteResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpVoteTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpVoteKinds)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl5);
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl4);
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlTop);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlBottom);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlLeft);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlRight);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Top;
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl6);
            this.splitContainerControl1.Panel2.Text = "Информация";
            this.splitContainerControl1.Size = new System.Drawing.Size(1394, 801);
            this.splitContainerControl1.SplitterPosition = 1003;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.groupControl2);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 180);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1003, 572);
            this.panelControl5.TabIndex = 17;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControl1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(2, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(999, 568);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Список вопросов выбранной сессии:";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.xpQuestions;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridControl1.Location = new System.Drawing.Point(2, 22);
            this.gridControl1.MainView = this.gridViewQuestions;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(995, 544);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewQuestions});
            this.gridControl1.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick);
            // 
            // xpQuestions
            // 
            this.xpQuestions.DeleteObjectOnRemove = true;
            this.xpQuestions.ObjectType = typeof(VoteSystem.QuestionObj);
            // 
            // gridViewQuestions
            // 
            this.gridViewQuestions.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridViewQuestions.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewQuestions.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemNum,
            this.colCaption,
            this.VoteKind,
            this.colCrDate,
            this.colSessionCode,
            this.colIsResult});
            this.gridViewQuestions.GridControl = this.gridControl1;
            this.gridViewQuestions.GroupCount = 1;
            this.gridViewQuestions.Name = "gridViewQuestions";
            this.gridViewQuestions.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridViewQuestions.OptionsBehavior.Editable = false;
            this.gridViewQuestions.OptionsDetail.AutoZoomDetail = true;
            this.gridViewQuestions.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewQuestions.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSessionCode, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewQuestions.VertScrollTipFieldName = "idQuestion.Caption";
            this.gridViewQuestions.ViewCaption = "111111111111111";
            // 
            // colItemNum
            // 
            this.colItemNum.Caption = "№ п. повестки";
            this.colItemNum.FieldName = "idTask.ItemNum";
            this.colItemNum.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colItemNum.Name = "colItemNum";
            this.colItemNum.Visible = true;
            this.colItemNum.VisibleIndex = 0;
            this.colItemNum.Width = 86;
            // 
            // colCaption
            // 
            this.colCaption.Caption = "Заголовок";
            this.colCaption.FieldName = "Name";
            this.colCaption.MinWidth = 255;
            this.colCaption.Name = "colCaption";
            this.colCaption.Visible = true;
            this.colCaption.VisibleIndex = 1;
            this.colCaption.Width = 501;
            // 
            // VoteKind
            // 
            this.VoteKind.Caption = "Вид";
            this.VoteKind.FieldName = "idKind.Name";
            this.VoteKind.Name = "VoteKind";
            this.VoteKind.Visible = true;
            this.VoteKind.VisibleIndex = 2;
            this.VoteKind.Width = 158;
            // 
            // colCrDate
            // 
            this.colCrDate.Caption = "Дата/время созд.";
            this.colCrDate.DisplayFormat.FormatString = "H:mm (dd/MM/yy)";
            this.colCrDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCrDate.FieldName = "CrDate";
            this.colCrDate.Name = "colCrDate";
            this.colCrDate.Visible = true;
            this.colCrDate.VisibleIndex = 3;
            this.colCrDate.Width = 110;
            // 
            // colSessionCode
            // 
            this.colSessionCode.Caption = "Сессия";
            this.colSessionCode.FieldName = "idSession.Code";
            this.colSessionCode.MinWidth = 50;
            this.colSessionCode.Name = "colSessionCode";
            this.colSessionCode.Visible = true;
            this.colSessionCode.VisibleIndex = 4;
            // 
            // colIsResult
            // 
            this.colIsResult.Caption = "Решение";
            this.colIsResult.FieldName = "idResult.idResultValue.Name";
            this.colIsResult.Name = "colIsResult";
            this.colIsResult.Visible = true;
            this.colIsResult.VisibleIndex = 4;
            this.colIsResult.Width = 121;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.groupControl4);
            this.panelControl4.Controls.Add(this.labelControl4);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1003, 180);
            this.panelControl4.TabIndex = 16;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.grdSessions);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(2, 2);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(999, 176);
            this.groupControl4.TabIndex = 23;
            this.groupControl4.Text = "Список сессий";
            // 
            // grdSessions
            // 
            this.grdSessions.DataSource = this.xpSessions;
            this.grdSessions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdSessions.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdSessions.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdSessions.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdSessions.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdSessions.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdSessions.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.grdSessions.Location = new System.Drawing.Point(2, 22);
            this.grdSessions.MainView = this.gridViewSession;
            this.grdSessions.Name = "grdSessions";
            this.grdSessions.Size = new System.Drawing.Size(995, 152);
            this.grdSessions.TabIndex = 16;
            this.grdSessions.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSession});
            // 
            // xpSessions
            // 
            this.xpSessions.CriteriaString = "[id] > 0 And [IsDeleted] = False";
            this.xpSessions.DeleteObjectOnRemove = true;
            this.xpSessions.ObjectType = typeof(VoteSystem.SessionObj);
            // 
            // gridViewSession
            // 
            this.gridViewSession.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridViewSession.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewSession.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.colID1});
            this.gridViewSession.GridControl = this.grdSessions;
            this.gridViewSession.Name = "gridViewSession";
            this.gridViewSession.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridViewSession.OptionsBehavior.Editable = false;
            this.gridViewSession.OptionsCustomization.AllowGroup = false;
            this.gridViewSession.OptionsDetail.AutoZoomDetail = true;
            this.gridViewSession.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewSession.OptionsSelection.MultiSelect = true;
            this.gridViewSession.OptionsView.ShowGroupPanel = false;
            this.gridViewSession.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewSession.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewSession_SelectionChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "№";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 47;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Код сессии";
            this.gridColumn2.FieldName = "Code";
            this.gridColumn2.MinWidth = 100;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 349;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Дата начала";
            this.gridColumn3.FieldName = "StartDate";
            this.gridColumn3.MinWidth = 30;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 192;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Тема";
            this.gridColumn4.FieldName = "Caption";
            this.gridColumn4.MinWidth = 200;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 388;
            // 
            // colID1
            // 
            this.colID1.Caption = "id";
            this.colID1.FieldName = "id";
            this.colID1.Name = "colID1";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl4.Location = new System.Drawing.Point(10, 138);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(0, 14);
            this.labelControl4.TabIndex = 2;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnQuestDetails);
            this.panelControl1.Controls.Add(this.dataNavigator2);
            this.panelControl1.Controls.Add(this.panelbtnSelect);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.dataNavigator1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 752);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1003, 49);
            this.panelControl1.TabIndex = 10;
            // 
            // btnQuestDetails
            // 
            this.btnQuestDetails.ImageIndex = 12;
            this.btnQuestDetails.ImageList = this.ButtonImages;
            this.btnQuestDetails.Location = new System.Drawing.Point(24, 6);
            this.btnQuestDetails.Name = "btnQuestDetails";
            this.btnQuestDetails.Size = new System.Drawing.Size(161, 34);
            this.btnQuestDetails.TabIndex = 17;
            this.btnQuestDetails.Text = "Итоги голосований...";
            this.btnQuestDetails.Click += new System.EventHandler(this.btnQuestDetails_Click);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // dataNavigator2
            // 
            this.dataNavigator2.DataSource = this.xpSessions;
            this.dataNavigator2.Location = new System.Drawing.Point(223, 5);
            this.dataNavigator2.Name = "dataNavigator2";
            this.dataNavigator2.Size = new System.Drawing.Size(192, 24);
            this.dataNavigator2.TabIndex = 16;
            this.dataNavigator2.Text = "dataNavigator2";
            this.dataNavigator2.Visible = false;
            this.dataNavigator2.PositionChanged += new System.EventHandler(this.dataNavigator2_PositionChanged);
            // 
            // panelbtnSelect
            // 
            this.panelbtnSelect.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelbtnSelect.Controls.Add(this.btnSelect);
            this.panelbtnSelect.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelbtnSelect.Location = new System.Drawing.Point(640, 0);
            this.panelbtnSelect.Name = "panelbtnSelect";
            this.panelbtnSelect.Size = new System.Drawing.Size(181, 49);
            this.panelbtnSelect.TabIndex = 15;
            // 
            // btnSelect
            // 
            this.btnSelect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSelect.ImageIndex = 12;
            this.btnSelect.ImageList = this.ButtonImages;
            this.btnSelect.Location = new System.Drawing.Point(16, 6);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(145, 34);
            this.btnSelect.TabIndex = 13;
            this.btnSelect.Text = "Выбрать";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.simpleButton1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl3.Location = new System.Drawing.Point(821, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(182, 49);
            this.panelControl3.TabIndex = 14;
            // 
            // simpleButton1
            // 
            this.simpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton1.ImageIndex = 8;
            this.simpleButton1.ImageList = this.ButtonImages;
            this.simpleButton1.Location = new System.Drawing.Point(18, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(149, 34);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Text = "Закрыть";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.DataSource = this.xpQuestions;
            this.dataNavigator1.Location = new System.Drawing.Point(223, 25);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(192, 24);
            this.dataNavigator1.TabIndex = 11;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.Visible = false;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 0);
            this.barDockControlBottom.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Location = new System.Drawing.Point(0, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 0);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.PropertiesControl);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(385, 552);
            this.groupControl1.TabIndex = 22;
            this.groupControl1.Text = "Информация";
            // 
            // PropertiesControl
            // 
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControl.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControl.DataSource = this.xpQuestions;
            this.PropertiesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesControl.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControl.Location = new System.Drawing.Point(2, 22);
            this.PropertiesControl.Name = "PropertiesControl";
            this.PropertiesControl.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControl.RecordWidth = 134;
            this.PropertiesControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4,
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6,
            this.repositoryItemMemoEdit1,
            this.repositoryItemMemoEdit2,
            this.repositoryItemTextEdit2});
            this.PropertiesControl.RowHeaderWidth = 66;
            this.PropertiesControl.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catCharacteristics,
            this.catNotes,
            this.catResult});
            this.PropertiesControl.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControl.Size = new System.Drawing.Size(381, 528);
            this.PropertiesControl.TabIndex = 0;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ImmediatePopup = true;
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox2.ImmediatePopup = true;
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.Sorted = true;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.ImmediatePopup = true;
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.repositoryItemTextEdit2.Appearance.Options.UseForeColor = true;
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // catCharacteristics
            // 
            this.catCharacteristics.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowCaption,
            this.rowTaskItem_R,
            this.rowidKind_Name,
            this.rowCrDate,
            this.rowReadNum,
            this.rowAmendment_R,
            this.rowDescription});
            this.catCharacteristics.Height = 19;
            this.catCharacteristics.Name = "catCharacteristics";
            this.catCharacteristics.Properties.Caption = "Характеристики";
            // 
            // rowCaption
            // 
            this.rowCaption.Height = 113;
            this.rowCaption.Name = "rowCaption";
            this.rowCaption.Properties.Caption = "Заголовок";
            this.rowCaption.Properties.FieldName = "idTask.Caption";
            this.rowCaption.Properties.RowEdit = this.repositoryItemMemoEdit2;
            this.rowCaption.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowTaskItem_R
            // 
            this.rowTaskItem_R.Height = 26;
            this.rowTaskItem_R.Name = "rowTaskItem_R";
            this.rowTaskItem_R.Properties.Caption = "Пункт повестки";
            this.rowTaskItem_R.Properties.FieldName = "idTask.CaptionItem";
            this.rowTaskItem_R.Properties.ReadOnly = true;
            // 
            // rowidKind_Name
            // 
            this.rowidKind_Name.Height = 28;
            this.rowidKind_Name.Name = "rowidKind_Name";
            this.rowidKind_Name.Properties.Caption = "Вид";
            this.rowidKind_Name.Properties.FieldName = "idTask.idKind.Name";
            this.rowidKind_Name.Properties.ReadOnly = true;
            this.rowidKind_Name.Properties.RowEdit = this.repositoryItemComboBox6;
            // 
            // rowCrDate
            // 
            this.rowCrDate.Height = 37;
            this.rowCrDate.Name = "rowCrDate";
            this.rowCrDate.Properties.Caption = "Дата/время";
            this.rowCrDate.Properties.FieldName = "idTask.CrDate";
            this.rowCrDate.Properties.Format.FormatString = "H:mm (dd/MM/yy)";
            this.rowCrDate.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rowCrDate.Properties.ReadOnly = true;
            // 
            // rowReadNum
            // 
            this.rowReadNum.Height = 60;
            this.rowReadNum.Name = "rowReadNum";
            this.rowReadNum.Properties.Caption = "Чтение";
            this.rowReadNum.Properties.FieldName = "idReadNum.Name";
            this.rowReadNum.Properties.ReadOnly = true;
            // 
            // rowAmendment_R
            // 
            this.rowAmendment_R.Name = "rowAmendment_R";
            this.rowAmendment_R.Properties.Caption = "Поправка";
            this.rowAmendment_R.Properties.FieldName = "AmendmentStr";
            this.rowAmendment_R.Properties.ReadOnly = true;
            // 
            // rowDescription
            // 
            this.rowDescription.Height = 46;
            this.rowDescription.Name = "rowDescription";
            this.rowDescription.Properties.Caption = "Описание";
            this.rowDescription.Properties.FieldName = "idTask.Description";
            this.rowDescription.Properties.ReadOnly = true;
            this.rowDescription.Properties.RowEdit = this.repositoryItemMemoEdit1;
            // 
            // catNotes
            // 
            this.catNotes.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowNotes});
            this.catNotes.Height = 19;
            this.catNotes.Name = "catNotes";
            this.catNotes.Properties.Caption = "Заметки";
            // 
            // rowNotes
            // 
            this.rowNotes.Height = 62;
            this.rowNotes.Name = "rowNotes";
            this.rowNotes.Properties.FieldName = "Notes";
            this.rowNotes.Properties.ReadOnly = true;
            this.rowNotes.Properties.RowEdit = this.repositoryItemMemoEdit1;
            // 
            // catResult
            // 
            this.catResult.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.catResult.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowResult,
            this.rowResDate});
            this.catResult.Height = 20;
            this.catResult.Name = "catResult";
            this.catResult.Properties.Caption = "Результат";
            // 
            // rowResult
            // 
            this.rowResult.Appearance.BackColor = System.Drawing.Color.OldLace;
            this.rowResult.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rowResult.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.rowResult.Appearance.Options.UseBackColor = true;
            this.rowResult.Appearance.Options.UseFont = true;
            this.rowResult.Appearance.Options.UseForeColor = true;
            this.rowResult.Height = 36;
            this.rowResult.Name = "rowResult";
            this.rowResult.Properties.Caption = "Решение";
            this.rowResult.Properties.FieldName = "idResult.idResultValue.Name";
            this.rowResult.Properties.ReadOnly = true;
            this.rowResult.Properties.RowEdit = this.repositoryItemTextEdit2;
            // 
            // rowResDate
            // 
            this.rowResDate.Appearance.BackColor = System.Drawing.Color.OldLace;
            this.rowResDate.Appearance.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rowResDate.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.rowResDate.Appearance.Options.UseBackColor = true;
            this.rowResDate.Appearance.Options.UseFont = true;
            this.rowResDate.Appearance.Options.UseForeColor = true;
            this.rowResDate.Height = 28;
            this.rowResDate.Name = "rowResDate";
            this.rowResDate.Properties.Caption = "Дата/время";
            this.rowResDate.Properties.FieldName = "idResult.idVoteResult.VoteDateTime";
            this.rowResDate.Properties.Format.FormatString = "H:mm (dd/MM/yy)";
            this.rowResDate.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rowResDate.Properties.ReadOnly = true;
            this.rowResDate.Properties.Value = "111";
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.lblProcQnty);
            this.groupControl6.Controls.Add(this.grpVoteResults);
            this.groupControl6.Controls.Add(this.lblVoteQntytxt);
            this.groupControl6.Controls.Add(this.lblVoteQnty);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl6.Location = new System.Drawing.Point(0, 554);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(385, 247);
            this.groupControl6.TabIndex = 21;
            this.groupControl6.Text = "Итоги голосования";
            // 
            // lblProcQnty
            // 
            this.lblProcQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcQnty.Location = new System.Drawing.Point(253, 220);
            this.lblProcQnty.Name = "lblProcQnty";
            this.lblProcQnty.Size = new System.Drawing.Size(46, 17);
            this.lblProcQnty.TabIndex = 212;
            this.lblProcQnty.Text = "70 %";
            this.lblProcQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // grpVoteResults
            // 
            this.grpVoteResults.Controls.Add(this.label107);
            this.grpVoteResults.Controls.Add(this.label106);
            this.grpVoteResults.Controls.Add(this.label105);
            this.grpVoteResults.Controls.Add(this.label104);
            this.grpVoteResults.Controls.Add(this.label103);
            this.grpVoteResults.Controls.Add(this.lblProcAye);
            this.grpVoteResults.Controls.Add(this.lblVoteAye);
            this.grpVoteResults.Controls.Add(this.label101);
            this.grpVoteResults.Controls.Add(this.lblNonProcQnty);
            this.grpVoteResults.Controls.Add(this.lblNonVoteQnty);
            this.grpVoteResults.Controls.Add(this.label95);
            this.grpVoteResults.Controls.Add(this.lblProcAgainst);
            this.grpVoteResults.Controls.Add(this.label79);
            this.grpVoteResults.Controls.Add(this.label80);
            this.grpVoteResults.Controls.Add(this.lblProcAbstain);
            this.grpVoteResults.Controls.Add(this.lblVoteAgainst);
            this.grpVoteResults.Controls.Add(this.label83);
            this.grpVoteResults.Controls.Add(this.label84);
            this.grpVoteResults.Controls.Add(this.label85);
            this.grpVoteResults.Controls.Add(this.label86);
            this.grpVoteResults.Controls.Add(this.label87);
            this.grpVoteResults.Controls.Add(this.label88);
            this.grpVoteResults.Controls.Add(this.lblVoteAbstain);
            this.grpVoteResults.Controls.Add(this.label90);
            this.grpVoteResults.Controls.Add(this.domainUpDown5);
            this.grpVoteResults.Controls.Add(this.label91);
            this.grpVoteResults.Location = new System.Drawing.Point(24, 41);
            this.grpVoteResults.Name = "grpVoteResults";
            this.grpVoteResults.Size = new System.Drawing.Size(321, 171);
            this.grpVoteResults.TabIndex = 203;
            this.grpVoteResults.TabStop = false;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Enabled = false;
            this.label107.ForeColor = System.Drawing.Color.DimGray;
            this.label107.Location = new System.Drawing.Point(6, 127);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(17, 13);
            this.label107.TabIndex = 184;
            this.label107.Text = "5.";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Enabled = false;
            this.label106.ForeColor = System.Drawing.Color.DimGray;
            this.label106.Location = new System.Drawing.Point(6, 106);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(17, 13);
            this.label106.TabIndex = 183;
            this.label106.Text = "4.";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 84);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(17, 13);
            this.label105.TabIndex = 182;
            this.label105.Text = "3.";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 62);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(17, 13);
            this.label104.TabIndex = 181;
            this.label104.Text = "2.";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 39);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(17, 13);
            this.label103.TabIndex = 180;
            this.label103.Text = "1.";
            // 
            // lblProcAye
            // 
            this.lblProcAye.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAye.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblProcAye.Location = new System.Drawing.Point(230, 39);
            this.lblProcAye.Name = "lblProcAye";
            this.lblProcAye.Size = new System.Drawing.Size(46, 17);
            this.lblProcAye.TabIndex = 179;
            this.lblProcAye.Text = "8";
            this.lblProcAye.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVoteAye
            // 
            this.lblVoteAye.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAye.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblVoteAye.Location = new System.Drawing.Point(139, 39);
            this.lblVoteAye.Name = "lblVoteAye";
            this.lblVoteAye.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAye.TabIndex = 178;
            this.lblVoteAye.Text = "8";
            this.lblVoteAye.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label101.ForeColor = System.Drawing.Color.DarkGreen;
            this.label101.Location = new System.Drawing.Point(44, 40);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(23, 14);
            this.label101.TabIndex = 177;
            this.label101.Text = "ЗА";
            // 
            // lblNonProcQnty
            // 
            this.lblNonProcQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNonProcQnty.Location = new System.Drawing.Point(230, 149);
            this.lblNonProcQnty.Name = "lblNonProcQnty";
            this.lblNonProcQnty.Size = new System.Drawing.Size(46, 17);
            this.lblNonProcQnty.TabIndex = 176;
            this.lblNonProcQnty.Text = "8";
            this.lblNonProcQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblNonVoteQnty
            // 
            this.lblNonVoteQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNonVoteQnty.Location = new System.Drawing.Point(156, 150);
            this.lblNonVoteQnty.Name = "lblNonVoteQnty";
            this.lblNonVoteQnty.Size = new System.Drawing.Size(10, 16);
            this.lblNonVoteQnty.TabIndex = 175;
            this.lblNonVoteQnty.Text = "8";
            this.lblNonVoteQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label95.ForeColor = System.Drawing.Color.Purple;
            this.label95.Location = new System.Drawing.Point(44, 150);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(94, 14);
            this.label95.TabIndex = 174;
            this.label95.Text = "Не голосовали";
            // 
            // lblProcAgainst
            // 
            this.lblProcAgainst.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAgainst.ForeColor = System.Drawing.Color.Maroon;
            this.lblProcAgainst.Location = new System.Drawing.Point(230, 61);
            this.lblProcAgainst.Name = "lblProcAgainst";
            this.lblProcAgainst.Size = new System.Drawing.Size(46, 17);
            this.lblProcAgainst.TabIndex = 173;
            this.lblProcAgainst.Text = "8";
            this.lblProcAgainst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label79.Location = new System.Drawing.Point(230, 127);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(46, 17);
            this.label79.TabIndex = 172;
            this.label79.Text = "9";
            this.label79.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label79.Visible = false;
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label80.Location = new System.Drawing.Point(230, 105);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(46, 17);
            this.label80.TabIndex = 171;
            this.label80.Text = "8";
            this.label80.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label80.Visible = false;
            // 
            // lblProcAbstain
            // 
            this.lblProcAbstain.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAbstain.ForeColor = System.Drawing.Color.Orange;
            this.lblProcAbstain.Location = new System.Drawing.Point(230, 83);
            this.lblProcAbstain.Name = "lblProcAbstain";
            this.lblProcAbstain.Size = new System.Drawing.Size(46, 17);
            this.lblProcAbstain.TabIndex = 170;
            this.lblProcAbstain.Text = "8";
            this.lblProcAbstain.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVoteAgainst
            // 
            this.lblVoteAgainst.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAgainst.ForeColor = System.Drawing.Color.Maroon;
            this.lblVoteAgainst.Location = new System.Drawing.Point(139, 61);
            this.lblVoteAgainst.Name = "lblVoteAgainst";
            this.lblVoteAgainst.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAgainst.TabIndex = 169;
            this.lblVoteAgainst.Text = "8";
            this.lblVoteAgainst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label83.Location = new System.Drawing.Point(139, 127);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(46, 17);
            this.label83.TabIndex = 168;
            this.label83.Text = "9";
            this.label83.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label83.Visible = false;
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label84.Location = new System.Drawing.Point(139, 105);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(46, 17);
            this.label84.TabIndex = 167;
            this.label84.Text = "8";
            this.label84.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label84.Visible = false;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label85.Location = new System.Drawing.Point(230, 15);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(62, 14);
            this.label85.TabIndex = 166;
            this.label85.Text = "Процент:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label86.Location = new System.Drawing.Point(139, 15);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(51, 14);
            this.label86.TabIndex = 165;
            this.label86.Text = "Голоса:";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(44, 127);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(48, 13);
            this.label87.TabIndex = 164;
            this.label87.Text = "Ответ 5";
            this.label87.Visible = false;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(44, 106);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(48, 13);
            this.label88.TabIndex = 163;
            this.label88.Text = "Ответ 4";
            this.label88.Visible = false;
            // 
            // lblVoteAbstain
            // 
            this.lblVoteAbstain.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAbstain.ForeColor = System.Drawing.Color.Orange;
            this.lblVoteAbstain.Location = new System.Drawing.Point(139, 83);
            this.lblVoteAbstain.Name = "lblVoteAbstain";
            this.lblVoteAbstain.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAbstain.TabIndex = 155;
            this.lblVoteAbstain.Text = "8";
            this.lblVoteAbstain.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label90.ForeColor = System.Drawing.Color.Maroon;
            this.label90.Location = new System.Drawing.Point(44, 62);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(57, 14);
            this.label90.TabIndex = 153;
            this.label90.Text = "ПРОТИВ";
            // 
            // domainUpDown5
            // 
            this.domainUpDown5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.domainUpDown5.Location = new System.Drawing.Point(80, 400);
            this.domainUpDown5.Name = "domainUpDown5";
            this.domainUpDown5.Size = new System.Drawing.Size(77, 21);
            this.domainUpDown5.TabIndex = 145;
            this.domainUpDown5.Text = "2";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label91.ForeColor = System.Drawing.Color.Orange;
            this.label91.Location = new System.Drawing.Point(44, 84);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(57, 14);
            this.label91.TabIndex = 139;
            this.label91.Text = "ВОЗДЕР.";
            // 
            // lblVoteQntytxt
            // 
            this.lblVoteQntytxt.AutoSize = true;
            this.lblVoteQntytxt.Location = new System.Drawing.Point(68, 220);
            this.lblVoteQntytxt.Name = "lblVoteQntytxt";
            this.lblVoteQntytxt.Size = new System.Drawing.Size(70, 13);
            this.lblVoteQntytxt.TabIndex = 209;
            this.lblVoteQntytxt.Text = "Голосовали:";
            // 
            // lblVoteQnty
            // 
            this.lblVoteQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteQnty.Location = new System.Drawing.Point(151, 220);
            this.lblVoteQnty.Name = "lblVoteQnty";
            this.lblVoteQnty.Size = new System.Drawing.Size(64, 17);
            this.lblVoteQnty.TabIndex = 210;
            this.lblVoteQnty.Text = "8 из 12";
            this.lblVoteQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barLargeButtonItem1.Appearance.Options.UseFont = true;
            this.barLargeButtonItem1.Caption = "  Фракции  ";
            this.barLargeButtonItem1.Id = 0;
            this.barLargeButtonItem1.LargeImageIndex = 9;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            // 
            // barLargeButtonItem2
            // 
            this.barLargeButtonItem2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.barLargeButtonItem2.Appearance.Options.UseFont = true;
            this.barLargeButtonItem2.Caption = "  Партии  ";
            this.barLargeButtonItem2.Id = 2;
            this.barLargeButtonItem2.LargeImageIndex = 10;
            this.barLargeButtonItem2.Name = "barLargeButtonItem2";
            // 
            // barLargeButtonItem3
            // 
            this.barLargeButtonItem3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.barLargeButtonItem3.Appearance.Options.UseFont = true;
            this.barLargeButtonItem3.Caption = "  Регионы  ";
            this.barLargeButtonItem3.Id = 3;
            this.barLargeButtonItem3.LargeImageIndex = 11;
            this.barLargeButtonItem3.Name = "barLargeButtonItem3";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Images = this.ButtonImages;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barLargeButtonItem4,
            this.barButtonItem1,
            this.barEditItem1,
            this.barEditItem2,
            this.barEditItem3,
            this.barEditItem4,
            this.barLargeButtonItem5});
            this.barManager1.LargeImages = this.imageCollection1;
            this.barManager1.MaxItemId = 10;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDuration1,
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(1394, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 801);
            this.barDockControl2.Size = new System.Drawing.Size(1394, 22);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 801);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1394, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 801);
            // 
            // barLargeButtonItem4
            // 
            this.barLargeButtonItem4.Caption = "Типы голосований";
            this.barLargeButtonItem4.Id = 1;
            this.barLargeButtonItem4.Name = "barLargeButtonItem4";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Виды голосований";
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.LargeImageIndex = 4;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemDuration1;
            this.barEditItem1.Id = 3;
            this.barEditItem1.LargeImageIndex = 0;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.Items.AddRange(new object[] {
            System.TimeSpan.Parse("00:00:00"),
            System.TimeSpan.Parse("00:05:00"),
            System.TimeSpan.Parse("00:10:00"),
            System.TimeSpan.Parse("00:15:00"),
            System.TimeSpan.Parse("00:30:00"),
            System.TimeSpan.Parse("01:00:00"),
            System.TimeSpan.Parse("02:00:00"),
            System.TimeSpan.Parse("03:00:00"),
            System.TimeSpan.Parse("04:00:00"),
            System.TimeSpan.Parse("05:00:00"),
            System.TimeSpan.Parse("06:00:00"),
            System.TimeSpan.Parse("07:00:00"),
            System.TimeSpan.Parse("08:00:00"),
            System.TimeSpan.Parse("09:00:00"),
            System.TimeSpan.Parse("10:00:00"),
            System.TimeSpan.Parse("11:00:00"),
            System.TimeSpan.Parse("12:00:00"),
            System.TimeSpan.Parse("1.00:00:00"),
            System.TimeSpan.Parse("2.00:00:00"),
            System.TimeSpan.Parse("00:00:00")});
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemDateEdit1;
            this.barEditItem2.Id = 4;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // barEditItem3
            // 
            this.barEditItem3.Edit = null;
            this.barEditItem3.Id = 9;
            this.barEditItem3.Name = "barEditItem3";
            // 
            // barEditItem4
            // 
            this.barEditItem4.Caption = "barEditItem4";
            this.barEditItem4.Edit = this.repositoryItemDateEdit2;
            this.barEditItem4.Id = 6;
            this.barEditItem4.Name = "barEditItem4";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            this.repositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // barLargeButtonItem5
            // 
            this.barLargeButtonItem5.Caption = "  Виды вопросов  ";
            this.barLargeButtonItem5.Id = 8;
            this.barLargeButtonItem5.LargeImageIndex = 5;
            this.barLargeButtonItem5.Name = "barLargeButtonItem5";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // xpVoteTypes
            // 
            this.xpVoteTypes.CriteriaString = "[id] > 0";
            this.xpVoteTypes.DeleteObjectOnRemove = true;
            this.xpVoteTypes.ObjectType = typeof(VoteSystem.VoteTypeObj);
            // 
            // xpVoteKinds
            // 
            this.xpVoteKinds.CriteriaString = "[id] > 0";
            this.xpVoteKinds.DeleteObjectOnRemove = true;
            this.xpVoteKinds.ObjectType = typeof(VoteSystem.VoteKindObj);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem2)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // QuestionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1394, 823);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "QuestionsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "База данных вопросов для голосования";
            this.Load += new System.EventHandler(this.QuestionsForm_Load);
            this.Shown += new System.EventHandler(this.QuestionsForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdSessions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpSessions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSession)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelbtnSelect)).EndInit();
            this.panelbtnSelect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            this.grpVoteResults.ResumeLayout(false);
            this.grpVoteResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpVoteTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpVoteKinds)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.Xpo.Session session1;
        private DevExpress.Xpo.XPCollection xpQuestions;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem2;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem3;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.Xpo.XPCollection xpVoteTypes;
        private DevExpress.Xpo.XPCollection xpVoteKinds;
        private DevExpress.XtraEditors.SimpleButton btnSelect;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelbtnSelect;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraBars.BarEditItem barEditItem3;
        private DevExpress.XtraBars.BarEditItem barEditItem4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem5;
        private DevExpress.Xpo.XPCollection xpSessions;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DataNavigator dataNavigator2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewQuestions;
        private DevExpress.XtraGrid.Columns.GridColumn colItemNum;
        private DevExpress.XtraGrid.Columns.GridColumn colCaption;
        private DevExpress.XtraGrid.Columns.GridColumn VoteKind;
        private DevExpress.XtraGrid.Columns.GridColumn colCrDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSessionCode;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private System.Windows.Forms.Label lblProcQnty;
        private System.Windows.Forms.GroupBox grpVoteResults;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label lblProcAye;
        private System.Windows.Forms.Label lblVoteAye;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label lblNonProcQnty;
        private System.Windows.Forms.Label lblNonVoteQnty;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label lblProcAgainst;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label lblProcAbstain;
        private System.Windows.Forms.Label lblVoteAgainst;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label lblVoteAbstain;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.DomainUpDown domainUpDown5;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label lblVoteQntytxt;
        private System.Windows.Forms.Label lblVoteQnty;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControl;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catCharacteristics;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCaption;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowidKind_Name;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCrDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowReadNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDescription;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catNotes;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNotes;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catResult;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowResult;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowResDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colIsResult;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraGrid.GridControl grdSessions;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSession;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.SimpleButton btnQuestDetails;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTaskItem_R;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmendment_R;
    }
}