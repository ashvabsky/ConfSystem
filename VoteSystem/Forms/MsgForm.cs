﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VoteSystem
{
    public interface IMsgMethods
    {
        DialogResult ShowWarningQuiestion(string Text);
        void ShowWarningMsg(string Text);
        void ShowWarningMsg(string Text, string Caption);
        DialogResult ShowQuestion(string Quest);
        void ShowError(string ErrText);
        bool ShowRetryError(string ErrText);
        void SetMessageOwner(Form Owner);
    }

    public class MsgForm : IMsgMethods
    {
        Form _ownWnd;
        public MsgForm(Form ownWnd)
        {
            _ownWnd = ownWnd;
        }

        public MsgForm()
        {
            
            _ownWnd = null;
        }

        void IMsgMethods.SetMessageOwner(Form ownWnd)
        {
            _ownWnd = ownWnd;
        }

        DialogResult IMsgMethods.ShowWarningQuiestion(string Text)
        {
            DialogResult dr = DialogResult.OK;
            if (_ownWnd != null)
                dr = MessageBox.Show(_ownWnd, Text, "Предупреждение", MessageBoxButtons.YesNoCancel);
            else
                dr = MessageBox.Show(Text, "Предупреждение", MessageBoxButtons.YesNoCancel);

            return dr;
        }

        void IMsgMethods.ShowWarningMsg(string Text)
        {
            MessageBox.Show(_ownWnd, Text, "Внимание!", MessageBoxButtons.OK);
        }

        void IMsgMethods.ShowWarningMsg(string Text, string Caption)
        {
            MessageBox.Show(_ownWnd, Text, Caption, MessageBoxButtons.OK);
        }

        DialogResult IMsgMethods.ShowQuestion(string Quest)
        {
            return MessageBox.Show(_ownWnd, Quest, "Вопрос", MessageBoxButtons.YesNoCancel);
        }

        void IMsgMethods.ShowError(string ErrText)
        {
            MessageBox.Show(_ownWnd, ErrText, "Ошибка!", MessageBoxButtons.OK);
        }

        bool IMsgMethods.ShowRetryError(string ErrText)
        {
            DialogResult dr = MessageBox.Show(_ownWnd, ErrText, "Ошибка!", MessageBoxButtons.RetryCancel);
            if (dr == DialogResult.Cancel)
                return false;
            else
                return true;
        }
    }
}
