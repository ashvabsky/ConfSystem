using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Shell32;
using DevExpress.Xpo;

namespace VoteSystem
{
    public interface IReportForm
    {
        IMsgMethods GetShowInterface();

        DialogResult ShowView();

        XtraForm GetView();

        void InitControls(SettingsReportObj settings, ReportKindObj curr_report, XPCollection<ReportKindObj> rkinds);
    }

    public partial class ReportForm : DevExpress.XtraEditors.XtraForm, IReportForm
    {
        ReportCntrler _Controller;
        MsgForm _msgForm;
        IMsgMethods _messages;


        public ReportForm(ReportCntrler cntrler)
        {
            InitializeComponent();

            _Controller = cntrler;
            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;

        }

        #region ���������� ���������� ISettingsForm

        IMsgMethods IReportForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        DialogResult IReportForm.ShowView()
        {
            DialogResult dr = (DialogResult)this.ShowDialog();
            return dr;
        }

        void IReportForm.InitControls(SettingsReportObj settings, ReportKindObj curr_report, XPCollection<ReportKindObj> rkinds)
        {
            txtResultFile.Text = _Controller._ResultFile;
            txtResultPath.Text = settings.ReportPath;

            txtTemplateFile.Text = curr_report.TemplateFile;

            grpReportKinds.Properties.Items.Clear();
            int currind = 0;
            foreach (ReportKindObj report in rkinds)
            {
                DevExpress.XtraEditors.Controls.RadioGroupItem item = new DevExpress.XtraEditors.Controls.RadioGroupItem(report.CodeName, report.Name);
                grpReportKinds.Properties.Items.Add(item);

                if (curr_report.CodeName == report.CodeName)
                    currind = grpReportKinds.Properties.Items.Count - 1;
            }

            grpReportKinds.SelectedIndex = currind;
        }

        XtraForm IReportForm.GetView()
        {
            return this;
        }


        #endregion ���������� ���������� ISettingsForm

        private void btnReport_Click(object sender, EventArgs e)
        {
            _Controller.SaveParams(txtResultPath.Text, txtResultFile.Text);
            Cursor = Cursors.WaitCursor;
            _Controller.PrintReport();
            Cursor = Cursors.Default;
        }

        private void ReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _Controller.OnClosing();
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
            _Controller.OnInitForm();
        }

        private void txtTemplateFile_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fname = "";
            
            openFileDialog.Filter = "template files (*.xls)|*.xls;|All files(*.*)|*.*";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fname = openFileDialog.FileName;
                txtTemplateFile.Text = fname;
//              m_Controller.AssignFileName(fname, null);
            }
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            _Controller.SaveParams(txtResultPath.Text, txtResultFile.Text);
        }

        private void txtResultPath_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Shell shell = new Shell();
            Folder folder = shell.BrowseForFolder(0, "�������� ����� ��� ���������� �����������", 0, ShellSpecialFolderConstants.ssfDESKTOP);
//          Folder Parent = folder.ParentFolder;
            txtResultPath.Text = "";
            string result = "";
            string title = "";
            string disk = "";
            while (folder != null)
            {
                title = folder.Title;
                folder = folder.ParentFolder;
                if (folder != null && folder.Title != "������� ����")
                {
                    result = title + "\\" + result;
                    disk = title;
                }
                else
                    break;
//              title = Parent.Title;
            }
            if (result != "")
            {
                int b = disk.IndexOf("(");
                int c = disk.IndexOf(")", b);
                string diskname = "";
                if (b >= 0 && c > b)
                    diskname = disk.Substring(b + 1, c - b - 1);
                txtResultPath.Text = result.Replace(disk, diskname);
            }

            string resultfile = txtResultPath.Text + txtResultFile.Text;
            System.IO.FileInfo f = new System.IO.FileInfo(resultfile);

        }

        private void grpReportKinds_SelectedIndexChanged(object sender, EventArgs e)
        {
            string codename = (string)grpReportKinds.Properties.Items[grpReportKinds.SelectedIndex].Value;
            _Controller.OnSelectReport(codename);
        }

        private void txtTemplateFile_EditValueChanged(object sender, EventArgs e)
        {
            _Controller.SaveTemplateFile(txtTemplateFile.Text);
        }
    }
}