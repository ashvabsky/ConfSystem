using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Shell32;
using DevExpress.Xpo;

namespace VoteSystem
{
    public interface IReportDelegate
    {
        IMsgMethods GetShowInterface();

        DialogResult ShowView();

        XtraForm GetView();

        void InitControls(SettingsReportObj settings);
    }

    public partial class ReportDelegate : DevExpress.XtraEditors.XtraForm, IReportDelegate
    {
        ReportCntrler _Controller;
        MsgForm _msgForm;
        IMsgMethods _messages;


        public ReportDelegate(ReportCntrler cntrler)
        {
            InitializeComponent();

            _Controller = cntrler;
            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;

        }

        #region ���������� ���������� IReportDelegate

        IMsgMethods IReportDelegate.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        DialogResult IReportDelegate.ShowView()
        {
            DialogResult dr = (DialogResult)this.ShowDialog();
            return dr;
        }

        void IReportDelegate.InitControls(SettingsReportObj settings)
        {
            chkShowCurrSession.EditValue = true;
            chkShowAllSessions.EditValue = false;
        }

        XtraForm IReportDelegate.GetView()
        {
            return this;
        }

        #endregion ���������� ���������� ISettingsForm

        private void btnReport_Click(object sender, EventArgs e)
        {
            _Controller.ShowReport_D2(chkShowCurrSession.Checked);
        }

        private void ReportDelegate_FormClosing(object sender, FormClosingEventArgs e)
        {
            _Controller.OnClosing();
        }

        private void ReportDelegate_Load(object sender, EventArgs e)
        {
            _Controller.D_OnInitForm();
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
        }


        private void chkShowCurrSession_CheckStateChanged(object sender, EventArgs e)
        {


            if (chkShowCurrSession.CheckState == CheckState.Checked)
                chkShowAllSessions.Checked = false;
            else
                chkShowAllSessions.Checked = true;
 
        }

    }
}