namespace VoteSystem
{
    partial class Settings_portion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings_portion));
            this.listQuantTypes = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.edtProcent = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.listDQuantTypes = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.listQuantTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtProcent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDQuantTypes)).BeginInit();
            this.SuspendLayout();
            // 
            // listQuantTypes
            // 
            this.listQuantTypes.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listQuantTypes.Appearance.Options.UseFont = true;
            this.listQuantTypes.DisplayMember = "Name";
            this.listQuantTypes.Location = new System.Drawing.Point(12, 44);
            this.listQuantTypes.Name = "listQuantTypes";
            this.listQuantTypes.Size = new System.Drawing.Size(240, 146);
            this.listQuantTypes.TabIndex = 0;
            this.listQuantTypes.ValueMember = "ProcentValue";
            this.listQuantTypes.SelectedValueChanged += new System.EventHandler(this.listQuantTypes_SelectedValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.labelControl1.Location = new System.Drawing.Point(22, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(149, 16);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "��� �������� �������:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.labelControl2.Location = new System.Drawing.Point(297, 44);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 16);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "�������:";
            // 
            // edtProcent
            // 
            this.edtProcent.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.edtProcent.Location = new System.Drawing.Point(380, 41);
            this.edtProcent.Name = "edtProcent";
            this.edtProcent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtProcent.Properties.IsFloatValue = false;
            this.edtProcent.Properties.Mask.EditMask = "N00";
            this.edtProcent.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.edtProcent.Size = new System.Drawing.Size(122, 20);
            this.edtProcent.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.labelControl3.Location = new System.Drawing.Point(297, 79);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(18, 16);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "��:";
            // 
            // listDQuantTypes
            // 
            this.listDQuantTypes.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listDQuantTypes.Appearance.Options.UseFont = true;
            this.listDQuantTypes.DisplayMember = "Name";
            this.listDQuantTypes.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("�� �������������� ����� ���."),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("�� ���������� ����� ���."),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("�� ��������������������")});
            this.listDQuantTypes.Location = new System.Drawing.Point(297, 101);
            this.listDQuantTypes.Name = "listDQuantTypes";
            this.listDQuantTypes.Size = new System.Drawing.Size(205, 83);
            this.listDQuantTypes.TabIndex = 5;
            this.listDQuantTypes.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.listDQuantTypes_ItemCheck);
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.ImageIndex = 0;
            this.btnOk.Location = new System.Drawing.Point(400, 198);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(102, 34);
            this.btnOk.TabIndex = 10;
            this.btnOk.Text = "�������";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // Settings_portion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 242);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.listDQuantTypes);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.edtProcent);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.listQuantTypes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings_portion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������� �����";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Settings_portion_FormClosing);
            this.Load += new System.EventHandler(this.Settings_portion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listQuantTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtProcent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDQuantTypes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ListBoxControl listQuantTypes;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit edtProcent;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CheckedListBoxControl listDQuantTypes;
        private DevExpress.XtraEditors.SimpleButton btnOk;
    }
}