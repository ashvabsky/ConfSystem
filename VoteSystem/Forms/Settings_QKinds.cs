using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;

namespace VoteSystem
{
    public interface ISettingsQKindsForm
    {
        //      IMsgMethods GetShowInterface();

        DialogResult ShowView();

        XtraForm GetView();

        void InitControls(ref  XPCollection<TaskKindObj> QKinds, string QKind_Name);
    }

    public partial class Settings_QKinds : DevExpress.XtraEditors.XtraForm, ISettingsQKindsForm
    {
        SettingsCntrler _Controller;
        MsgForm _msgForm;
        IMsgMethods _messages;

        public Settings_QKinds(SettingsCntrler cntrler)
        {
            InitializeComponent();
            _Controller = cntrler;
            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;
        }

        #region ���������� ���������� ISettingsForm


        DialogResult ISettingsQKindsForm.ShowView()
        {
            DialogResult dr = (DialogResult)this.ShowDialog();
            return dr;
        }

        void ISettingsQKindsForm.InitControls(ref  XPCollection<TaskKindObj> QKinds, string QKind_Name)
        {
            txtQKindName.Text = QKind_Name;
            lstQntyTypes.Properties.DataSource = QKinds;
        }

        XtraForm ISettingsQKindsForm.GetView()
        {
            return this;
        }

        #endregion ���������� ���������� ISettingsForm

        private void Settings_QKinds_Load(object sender, EventArgs e)
        {
              _Controller.OnInitQKindsForm();
        }

    }
}