using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using Shell32;

namespace VoteSystem
{
    public interface ISettingsForm
    {
        IMsgMethods GetShowInterface();

        DialogResult ShowView();

        XtraForm GetView();

        void InitControls(ref XPCollection<TaskKindObj> QKinds, ref  XPCollection<QntyTypeObj> QTypes, CriteriaOperator crQT, ref  XPCollection<DelegateQntyTypeObj> DQTypes, QntyTypeObj quorumQntyType, SettingsObj settings);
        ISettingsPortionsForm CreatePortionsView();

        void InitQuorumPageControls(QntyTypeObj quorumQntyType, SettingsObj settings);
    }

    public partial class SettingsForm : DevExpress.XtraEditors.XtraForm, ISettingsForm
    {
        SettingsCntrler _Controller;
        MsgForm _msgForm;
        IMsgMethods _messages;

        int _lastSelectedItemDQTypes = -1;
        int _lastSelectedItemQTypes = -1;
        bool _savemode = false; // ����: ��������� ��� ��������� ������ � ��

        public SettingsForm( SettingsCntrler cntrler)
        {
            InitializeComponent();

            _Controller = cntrler;
            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;

        }

        #region ���������� ���������� ISettingsForm

        IMsgMethods ISettingsForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        DialogResult ISettingsForm.ShowView()
        {
            DialogResult dr = (DialogResult)this.ShowDialog();
            return dr;
        }

        void ISettingsForm.InitControls(ref XPCollection<TaskKindObj> QKinds, ref  XPCollection<QntyTypeObj> QTypes,  CriteriaOperator crQT, ref  XPCollection<DelegateQntyTypeObj> DQTypes, QntyTypeObj quorumQntyType, SettingsObj settings)
        {
//            gridQuestKinds.DataSource = QKinds;
//            GridLookUpEdit_QTypes.DataSource = QTypes;
//            GridLookUpEdit_QTypes.DisplayMember = "Name";
            xpQTypes.Filter = crQT;
            xpQKinds.Filter = QKinds.Filter;
            listQuantTypes.DataSource = QTypes;
            listDQuantTypes.DataSource = DQTypes;

            listDQuantTypes.SelectionMode = SelectionMode.One;

            listQuantTypes.SelectedItem = quorumQntyType;

            listQuantTypes.SetItemChecked(listQuantTypes.SelectedIndex, true);
            _lastSelectedItemDQTypes = listDQuantTypes.SelectedIndex;

            edtAssignedQnty.Value = settings.AssignedQnty;
            edtSelectedQnty.Value = settings.SelectedQnty;

            txtPath_Docs.Text = settings.Path_Doc;
            txtPath_Excel.Text = settings.Path_Excel;
            txtPath_DBase.Text = settings.Path_DBase;

            // ������������
            chkPredsedatel_ViewAgenda.Checked = settings.Pred_IsAgenda;

            chkCardMode.Checked = settings.IsCardMode;
        }

        void ISettingsForm.InitQuorumPageControls(QntyTypeObj quorumQntyType, SettingsObj settings)
        {

            listDQuantTypes.SelectionMode = SelectionMode.One;

            listQuantTypes.SelectedItem = quorumQntyType;

            listQuantTypes.SetItemChecked(listQuantTypes.SelectedIndex, true);
            _lastSelectedItemDQTypes = listDQuantTypes.SelectedIndex;
        }

        XtraForm ISettingsForm.GetView()
        {
            return this;
        }


        ISettingsPortionsForm ISettingsForm.CreatePortionsView()
        {
            Settings_portion f = new Settings_portion(_Controller); 
            return (ISettingsPortionsForm)f; 
        }

        #endregion ���������� ���������� ISettingsForm

        private void btnSetPortions_Click(object sender, EventArgs e)
        {
            _Controller.ShowPortionsView();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            _Controller.OnInitForm();
            listQuantTypes_SelectedValueChanged(null, null);
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
//            _Controller.ShowQKinds();
        }

        private void listQuantTypes_SelectedValueChanged(object sender, EventArgs e)
        {
            QntyTypeObj qto = (QntyTypeObj)listQuantTypes.SelectedItem;
            _Controller.OnSelValueChanged_ListQuantTypes(ref qto);

            listDQuantTypes.SelectedItem = qto.idDelegateQntyType;
            edtProcent.Text = qto.ProcentValue.ToString();


            if (listDQuantTypes.SelectedIndex >= 0)
            {
                listDQuantTypes.SetItemChecked(listDQuantTypes.SelectedIndex, true);
            }
        }

        private void listDQuantTypes_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            if (listDQuantTypes.SelectedIndex >= 0)
            {
                if (_lastSelectedItemDQTypes != listDQuantTypes.SelectedIndex && _lastSelectedItemDQTypes >=0 )
                    listDQuantTypes.SetItemChecked(_lastSelectedItemDQTypes, false);
                //                listDQuantTypes.SetItemChecked(listDQuantTypes.SelectedIndex, true);
                if (_lastSelectedItemDQTypes != listDQuantTypes.SelectedIndex)
                {
                    _lastSelectedItemDQTypes = listDQuantTypes.SelectedIndex;
                    int currproc = -1;
                    if (listDQuantTypes.SelectedItem != null)
                    {
                        DelegateQntyTypeObj dq = (DelegateQntyTypeObj)listDQuantTypes.CheckedItems[0];
                        _Controller.UpdateQntyTypeValues_QuorumSettings(dq, currproc);
                    }
                }
            }
        }

        private void listQuantTypes_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            if (e.State == CheckState.Unchecked)
                return;

            if (listQuantTypes.SelectedIndex >= 0)
            {
                if (_lastSelectedItemQTypes != listQuantTypes.SelectedIndex)
                {
                    if (_lastSelectedItemQTypes >= 0)
                        listQuantTypes.SetItemChecked(_lastSelectedItemQTypes, false);

                    _lastSelectedItemQTypes = listQuantTypes.SelectedIndex;
                    _Controller.UpdateQntyType_QuorumSettings();
                }
            }
        }

        private void edtProcent_ValueChanged(object sender, EventArgs e)
        {
            int currproc = Convert.ToInt32(edtProcent.Value);
            _Controller.UpdateQntyTypeValues_QuorumSettings(null, currproc);
        }

        private void edtAssignedQnty_ValueChanged(object sender, EventArgs e)
        {
            _Controller.UpdateAssignedQnty(Convert.ToInt32(edtAssignedQnty.Value));
        }

        private void edtSelectedQnty_ValueChanged(object sender, EventArgs e)
        {
            _Controller.UpdateSelectedQnty(Convert.ToInt32(edtSelectedQnty.Value));
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            _Controller.TKinds_InsertNewItem();
            xpQKinds.Reload();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int pos = dataNavigator1.Position;
            if (pos >= 0 && pos < xpQKinds.Count)
            {
                TaskKindObj tk = (TaskKindObj)xpQKinds[dataNavigator1.Position];
                DialogResult dr = MessageBox.Show(this, "�� �������, ��� ������ ��������� ��������� ��� �������?", "��������", MessageBoxButtons.YesNoCancel);
                if (dr == DialogResult.Yes)
                {
                    _Controller.TKinds_RemoveItem(tk);
                    xpQKinds.Reload();
                }
            }
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page.Name == "pageQuorumSettings")
            {
                _Controller.OnInitQuorumPage();
            }
        }

        private void chkCardMode_CheckedChanged(object sender, EventArgs e)
        {
            if (_Controller.IsCardMode != chkCardMode.Checked)
            {
                if (chkCardMode.Checked == false)
                {
                    DialogResult dr = _messages.ShowWarningQuiestion("��������� ������ � ����� ���������������� ��� �������� ���������?");
                    if (dr == DialogResult.No)
                    {
                        chkCardMode.Checked = _Controller.IsCardMode;
                    }
                    else
                    {
                        _Controller.SetCardMode(chkCardMode.Checked);
                    }
                }
                else
                {
                    _Controller.SetCardMode(chkCardMode.Checked);
                }

            }

        }


        private string SelectPath()
        {
            Shell shell = new Shell();
            Folder folder = shell.BrowseForFolder(0, "�������� �����", 0, ShellSpecialFolderConstants.ssfDESKTOP);
            //          Folder Parent = folder.ParentFolder;
            string ResultPath = "";
            string result = "";
            string title = "";
            string disk = "";
            while (folder != null)
            {
                title = folder.Title;
                folder = folder.ParentFolder;
                if (folder != null && folder.Title != "������� ����")
                {
                    result = title + "\\" + result;
                    disk = title;
                }
                else
                    break;
                //              title = Parent.Title;
            }

            int b = disk.IndexOf("(");
            int c = disk.IndexOf(")", b);
            string diskname = "";
            if (b >= 0 && c > b)
                diskname = disk.Substring(b + 1, c - b - 1);
            ResultPath = result.Replace(disk, diskname);

            return ResultPath;
        }

        private void txtPath_Docs_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string s = SelectPath();
            if (s != "")
            {
                _Controller.SetPath("docs", s);
                txtPath_Docs.Text = s;
            }
        }

        private void txtPath_Excel_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string s = SelectPath();
            if (s != "")
            {
                _Controller.SetPath("excel", s);
                txtPath_Excel.Text = s;
            }

        }

        private void txtPath_DBase_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string s = SelectPath();
            if (s != "")
            {
                _Controller.SetPath("dbase", s);
                txtPath_DBase.Text = s;
            }
        }

        private void chkPredsedatel_ViewAgenda_CheckedChanged(object sender, EventArgs e)
        {
            _Controller.SetPredsedatelViewAgenda(chkPredsedatel_ViewAgenda.Checked);
        }
    }
}

