using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace VoteSystem
{
    public partial class DelegateEditForm : DevExpress.XtraEditors.XtraForm
    {

        DelegateObj _theDelegate;
        NestedUnitOfWork _nuow;
        public XPCollection<DelegateObj> _Delegates;
        public XPCollection<FractionObj> _Fractions;
        private DelegatesCntrler _Controller;

        public DelegateEditForm()
        {
            InitializeComponent();
        }
        public DelegateEditForm(DelegateObj theDelegate, DelegatesCntrler c)
        {
            InitializeComponent();
//            this.DataBindings.Add("Delegate", theDelegate, ""); 
            _theDelegate = theDelegate;
            _Controller = c;
         }

        private void DelegateEditForm_Load(object sender, EventArgs e)
        {
            _theDelegate.Session.BeginTransaction();

            _Delegates = new XPCollection<DelegateObj>(_theDelegate.Session, false);

            _Delegates.LoadingEnabled = false;

            _Delegates.Add(_theDelegate);

            PropertiesControl.DataSource = _Delegates;

            CriteriaOperator criteriaInUse = new BinaryOperator(
                new OperandProperty("InUse"), new OperandValue(1),
                BinaryOperatorType.Equal);


            XPCollection<FractionObj> Fractions = new XPCollection<FractionObj>(_theDelegate.Session);
            Fractions.Criteria = criteriaInUse;
            Fractions.DisplayableProperties = "This;Name;Description;";

            repositoryItemGridLookUpFractions.DataSource = Fractions;
            repositoryItemGridLookUpFractions.DisplayMember = "Name";

            repositoryItemGridLookUpFractions.PopulateViewColumns();
            repositoryItemGridLookUpFractions.View.Columns[0].Caption = "������������";
            repositoryItemGridLookUpFractions.View.Columns[1].Caption = "��������";

            XPCollection<PartyObj> Parties = new XPCollection<PartyObj>(_theDelegate.Session);
            Parties.DisplayableProperties = "This;Name;Description;";
            Parties.Criteria = criteriaInUse;

            repositoryItemGridLookUpParties.DataSource = Parties;
            repositoryItemGridLookUpParties.DisplayMember = "Name";
            repositoryItemGridLookUpParties.PopulateViewColumns();
            repositoryItemGridLookUpParties.View.Columns[0].Caption = "������������";
            repositoryItemGridLookUpParties.View.Columns[1].Caption = "��������";

            XPCollection<DelegateTypeObj> Types = new XPCollection<DelegateTypeObj>(_theDelegate.Session);
            Types.DisplayableProperties = "This;Name;";


            repositoryItemGridLookUpTypes.DataSource = Types;
            repositoryItemGridLookUpTypes.DisplayMember = "Name";
            repositoryItemGridLookUpTypes.PopulateViewColumns();

            repositoryItemGridLookUpTypes.View.Columns[0].Caption = "��� ���������";
            //            repositoryItemGridLookUpParties.View.Columns[1].Caption = "��������";

            XPCollection<SeatObj> Seats = new XPCollection<SeatObj>(_theDelegate.Session);
            Seats.DisplayableProperties = "This;MicNum;RowNum;SeatNum";


            repositoryItemGridLookUpSeat.DataSource = Seats;
            repositoryItemGridLookUpSeat.DisplayMember = "MicNum";
            repositoryItemGridLookUpSeat.PopulateViewColumns();

            repositoryItemGridLookUpSeat.View.Columns[0].Caption = "�������� �";
            repositoryItemGridLookUpSeat.View.Columns[1].Caption = "��� �";
            repositoryItemGridLookUpSeat.View.Columns[2].Caption = "����� �";


            //

            XPCollection<RegionObj> regions = new XPCollection<RegionObj>(_theDelegate.Session);
            regions.Criteria = criteriaInUse;
            regions.DisplayableProperties = "This;Name;Number;Description";


            repositoryItemGridLookUpRegion.DataSource = regions;
            repositoryItemGridLookUpRegion.DisplayMember = "Name";
            repositoryItemGridLookUpRegion.PopulateViewColumns();

            repositoryItemGridLookUpRegion.View.Columns[0].Caption = "������������";
            repositoryItemGridLookUpRegion.View.Columns[1].Caption = "�����";
            repositoryItemGridLookUpRegion.View.Columns[2].Caption = "��������";
        }

        private void DelegateEditForm_Shown(object sender, EventArgs e)
        {

        }

        private void btnPropApply_Click(object sender, EventArgs e)
        {
        }

        private void btnPropCancel_Click(object sender, EventArgs e)
        {
        }

        private void PropertiesControl_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {

        }

        private void PropertiesControl_CellValueChanging(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            PropertiesControl.UpdateFocusedRecord();
            PropertiesControl.Update();

            rowRowNum.Properties.Value = 10;
        }

        private void btnPropCancel_Click_1(object sender, EventArgs e)
        {
            _theDelegate.Session.RollbackTransaction();
        }

        private void btnPropApply_Click_1(object sender, EventArgs e)
        {
            _theDelegate.Session.CommitTransaction();
        }

        private void btnCardRead_Click(object sender, EventArgs e)
        {
            string s = "�� �������� ������� ��� ����� � ������ ����� - " + _theDelegate.idSeat.MicNum + " ?";  
            DialogResult dr = MessageBox.Show(s, "������", MessageBoxButtons.YesNoCancel);
            if (dr == DialogResult.Yes)
            {
                ConfHelper.OnFinishProcessText = FinishProccessCardNumber;

                _Controller.ReadCardCode(_theDelegate, _theDelegate.idSeat.MicNum);
            }
        }

        private void FinishProccessCardNumber()
        {
            _theDelegate.Reload();
            _Delegates.Reload();
        }
    }
}