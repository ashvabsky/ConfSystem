namespace VoteSystem
{
    partial class DelegateEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DelegateEditForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.PropertiesControl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.xpDelegates = new DevExpress.Xpo.XPCollection();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpParties = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemGridLookUpFractions = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.xpFractions = new DevExpress.Xpo.XPCollection();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemGridLookUpTypes = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemGridLookUpSeat = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemGridLookUpRegion = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.catFIO = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowLastName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFirstName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSecondName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catRegion = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowRegionName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRegionNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catInfo = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowFraction = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowidParty_Name = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUse = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catSeat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMicNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRowNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSeatNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowCardNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catNotes = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowNotes = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnCardRead = new DevExpress.XtraEditors.SimpleButton();
            this.btnPropCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnPropApply = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpParties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpFractions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpFractions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpSeat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.PropertiesControl);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(529, 470);
            this.panelControl1.TabIndex = 6;
            // 
            // PropertiesControl
            // 
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControl.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControl.DataSource = this.xpDelegates;
            this.PropertiesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesControl.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControl.Location = new System.Drawing.Point(2, 2);
            this.PropertiesControl.Name = "PropertiesControl";
            this.PropertiesControl.OptionsBehavior.UseEnterAsTab = true;
            this.PropertiesControl.RecordWidth = 126;
            this.PropertiesControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpParties,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4,
            this.repositoryItemGridLookUpFractions,
            this.repositoryItemMemoEdit1,
            this.repositoryItemGridLookUpTypes,
            this.repositoryItemCheckEdit1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemGridLookUpSeat,
            this.repositoryItemGridLookUpRegion,
            this.repositoryItemButtonEdit1});
            this.PropertiesControl.RowHeaderWidth = 74;
            this.PropertiesControl.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catFIO,
            this.catRegion,
            this.catInfo,
            this.catSeat,
            this.catNotes});
            this.PropertiesControl.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControl.Size = new System.Drawing.Size(525, 466);
            this.PropertiesControl.TabIndex = 2;
            // 
            // xpDelegates
            // 
            this.xpDelegates.BindingBehavior = DevExpress.Xpo.CollectionBindingBehavior.AllowNone;
            this.xpDelegates.CriteriaString = "[id] = 5";
            this.xpDelegates.DeleteObjectOnRemove = true;
            this.xpDelegates.DisplayableProperties = "This;id;LastName;FirstName;SecondName;FullName;idParty;idFraction;idRegion;isActi" +
                "ve;idType;idSeat;idFraction.Name;Notes;idSeat.MicNum";
            this.xpDelegates.ObjectType = typeof(VoteSystem.DelegateObj);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemGridLookUpParties
            // 
            this.repositoryItemGridLookUpParties.AutoHeight = false;
            this.repositoryItemGridLookUpParties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpParties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemGridLookUpParties.DisplayMember = "Name";
            this.repositoryItemGridLookUpParties.ImmediatePopup = true;
            this.repositoryItemGridLookUpParties.Name = "repositoryItemGridLookUpParties";
            this.repositoryItemGridLookUpParties.View = this.gridView1;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ImmediatePopup = true;
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox2.ImmediatePopup = true;
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.Sorted = true;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemComboBox4.ImmediatePopup = true;
            this.repositoryItemComboBox4.Items.AddRange(new object[] {
            "tyty"});
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // repositoryItemGridLookUpFractions
            // 
            this.repositoryItemGridLookUpFractions.AutoHeight = false;
            this.repositoryItemGridLookUpFractions.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpFractions.DataSource = this.xpFractions;
            this.repositoryItemGridLookUpFractions.DisplayMember = "Name";
            this.repositoryItemGridLookUpFractions.ImmediatePopup = true;
            this.repositoryItemGridLookUpFractions.Name = "repositoryItemGridLookUpFractions";
            this.repositoryItemGridLookUpFractions.NullText = "";
            this.repositoryItemGridLookUpFractions.View = this.repositoryItemGridLookUpEdit2View;
            // 
            // xpFractions
            // 
            this.xpFractions.CriteriaString = "[id] > 0";
            this.xpFractions.DeleteObjectOnRemove = true;
            this.xpFractions.DisplayableProperties = "This;Name;Description;Delegates";
            this.xpFractions.ObjectType = typeof(VoteSystem.FractionObj);
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsBehavior.AllowIncrementalSearch = true;
            this.repositoryItemGridLookUpEdit2View.OptionsBehavior.FocusLeaveOnTab = true;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.ViewCaption = "������ �������";
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // repositoryItemGridLookUpTypes
            // 
            this.repositoryItemGridLookUpTypes.AutoHeight = false;
            this.repositoryItemGridLookUpTypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpTypes.ImmediatePopup = true;
            this.repositoryItemGridLookUpTypes.Name = "repositoryItemGridLookUpTypes";
            this.repositoryItemGridLookUpTypes.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsBehavior.AllowIncrementalSearch = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.AutoWidth = true;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemSpinEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemSpinEdit1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.HideSelection = false;
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemGridLookUpSeat
            // 
            this.repositoryItemGridLookUpSeat.AutoHeight = false;
            this.repositoryItemGridLookUpSeat.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpSeat.ImmediatePopup = true;
            this.repositoryItemGridLookUpSeat.Name = "repositoryItemGridLookUpSeat";
            this.repositoryItemGridLookUpSeat.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView2.OptionsBehavior.CacheValuesOnRowUpdating = DevExpress.Data.CacheRowValuesMode.Disabled;
            this.gridView2.OptionsBehavior.FocusLeaveOnTab = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemGridLookUpRegion
            // 
            this.repositoryItemGridLookUpRegion.AutoHeight = false;
            this.repositoryItemGridLookUpRegion.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpRegion.ImmediatePopup = true;
            this.repositoryItemGridLookUpRegion.Name = "repositoryItemGridLookUpRegion";
            this.repositoryItemGridLookUpRegion.View = this.gridView3;
            // 
            // gridView3
            // 
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // catFIO
            // 
            this.catFIO.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowLastName,
            this.rowFirstName,
            this.rowSecondName});
            this.catFIO.Height = 19;
            this.catFIO.Name = "catFIO";
            this.catFIO.Properties.Caption = "���";
            // 
            // rowLastName
            // 
            this.rowLastName.Height = 27;
            this.rowLastName.Name = "rowLastName";
            this.rowLastName.Properties.Caption = "�������";
            this.rowLastName.Properties.FieldName = "LastName";
            this.rowLastName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowLastName.Properties.Value = "";
            // 
            // rowFirstName
            // 
            this.rowFirstName.Height = 20;
            this.rowFirstName.Name = "rowFirstName";
            this.rowFirstName.Properties.Caption = "���";
            this.rowFirstName.Properties.FieldName = "FirstName";
            this.rowFirstName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowSecondName
            // 
            this.rowSecondName.Height = 20;
            this.rowSecondName.Name = "rowSecondName";
            this.rowSecondName.Properties.Caption = "��������";
            this.rowSecondName.Properties.FieldName = "SecondName";
            // 
            // catRegion
            // 
            this.catRegion.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowRegionName,
            this.rowRegionNum});
            this.catRegion.Name = "catRegion";
            this.catRegion.Properties.Caption = "������";
            // 
            // rowRegionName
            // 
            this.rowRegionName.Height = 20;
            this.rowRegionName.Name = "rowRegionName";
            this.rowRegionName.Properties.Caption = "������";
            this.rowRegionName.Properties.FieldName = "idRegion!";
            this.rowRegionName.Properties.RowEdit = this.repositoryItemGridLookUpRegion;
            this.rowRegionName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowRegionNum
            // 
            this.rowRegionNum.Height = 20;
            this.rowRegionNum.Name = "rowRegionNum";
            this.rowRegionNum.Properties.Caption = "� �������";
            this.rowRegionNum.Properties.FieldName = "idRegion.Number";
            this.rowRegionNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // catInfo
            // 
            this.catInfo.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowFraction,
            this.rowidParty_Name,
            this.rowType,
            this.rowUse});
            this.catInfo.Name = "catInfo";
            this.catInfo.Properties.Caption = "�������������� ����������";
            // 
            // rowFraction
            // 
            this.rowFraction.Height = 25;
            this.rowFraction.Name = "rowFraction";
            this.rowFraction.Properties.Caption = "�������";
            this.rowFraction.Properties.FieldName = "idFraction!";
            this.rowFraction.Properties.RowEdit = this.repositoryItemGridLookUpFractions;
            // 
            // rowidParty_Name
            // 
            this.rowidParty_Name.Height = 23;
            this.rowidParty_Name.Name = "rowidParty_Name";
            this.rowidParty_Name.Properties.Caption = "������";
            this.rowidParty_Name.Properties.FieldName = "idParty!";
            this.rowidParty_Name.Properties.RowEdit = this.repositoryItemGridLookUpParties;
            this.rowidParty_Name.Properties.Value = "������ 1";
            // 
            // rowType
            // 
            this.rowType.Name = "rowType";
            this.rowType.Properties.Caption = "���";
            this.rowType.Properties.FieldName = "idType!";
            this.rowType.Properties.RowEdit = this.repositoryItemGridLookUpTypes;
            // 
            // rowUse
            // 
            this.rowUse.Name = "rowUse";
            this.rowUse.Properties.Caption = "��������� � �������";
            this.rowUse.Properties.FieldName = "isActive";
            this.rowUse.Properties.RowEdit = this.repositoryItemCheckEdit1;
            // 
            // catSeat
            // 
            this.catSeat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMicNum,
            this.rowRowNum,
            this.rowSeatNum,
            this.rowCardNum});
            this.catSeat.Name = "catSeat";
            this.catSeat.Properties.Caption = "���������� � ����";
            // 
            // rowMicNum
            // 
            this.rowMicNum.Height = 26;
            this.rowMicNum.Name = "rowMicNum";
            this.rowMicNum.Properties.Caption = "����� �";
            this.rowMicNum.Properties.FieldName = "idSeat!";
            this.rowMicNum.Properties.RowEdit = this.repositoryItemGridLookUpSeat;
            // 
            // rowRowNum
            // 
            this.rowRowNum.Height = 16;
            this.rowRowNum.Name = "rowRowNum";
            this.rowRowNum.Properties.Caption = "���";
            this.rowRowNum.Properties.FieldName = "RowNum";
            this.rowRowNum.Properties.ReadOnly = true;
            this.rowRowNum.Properties.RowEdit = this.repositoryItemTextEdit1;
            this.rowRowNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // rowSeatNum
            // 
            this.rowSeatNum.Height = 18;
            this.rowSeatNum.Name = "rowSeatNum";
            this.rowSeatNum.Properties.Caption = "�����";
            this.rowSeatNum.Properties.FieldName = "SeatNum";
            this.rowSeatNum.Properties.ReadOnly = true;
            this.rowSeatNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowCardNum
            // 
            this.rowCardNum.Height = 22;
            this.rowCardNum.Name = "rowCardNum";
            this.rowCardNum.Properties.Caption = "��� ��������";
            this.rowCardNum.Properties.FieldName = "CardCode";
            this.rowCardNum.Properties.ReadOnly = true;
            // 
            // catNotes
            // 
            this.catNotes.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowNotes});
            this.catNotes.Name = "catNotes";
            this.catNotes.Properties.Caption = "�������";
            // 
            // rowNotes
            // 
            this.rowNotes.Height = 60;
            this.rowNotes.Name = "rowNotes";
            this.rowNotes.Properties.FieldName = "Notes";
            this.rowNotes.Properties.RowEdit = this.repositoryItemMemoEdit1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnCardRead);
            this.panelControl2.Controls.Add(this.btnPropCancel);
            this.panelControl2.Controls.Add(this.btnPropApply);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 418);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(529, 52);
            this.panelControl2.TabIndex = 7;
            // 
            // btnCardRead
            // 
            this.btnCardRead.ImageIndex = 15;
            this.btnCardRead.ImageList = this.ButtonImages;
            this.btnCardRead.Location = new System.Drawing.Point(204, 7);
            this.btnCardRead.Name = "btnCardRead";
            this.btnCardRead.Size = new System.Drawing.Size(147, 33);
            this.btnCardRead.TabIndex = 8;
            this.btnCardRead.Text = "������� ��������";
            this.btnCardRead.Click += new System.EventHandler(this.btnCardRead_Click);
            // 
            // btnPropCancel
            // 
            this.btnPropCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPropCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPropCancel.ImageIndex = 1;
            this.btnPropCancel.ImageList = this.ButtonImages;
            this.btnPropCancel.Location = new System.Drawing.Point(394, 7);
            this.btnPropCancel.Name = "btnPropCancel";
            this.btnPropCancel.Size = new System.Drawing.Size(123, 33);
            this.btnPropCancel.TabIndex = 7;
            this.btnPropCancel.Text = "�������� ";
            this.btnPropCancel.Click += new System.EventHandler(this.btnPropCancel_Click_1);
            // 
            // btnPropApply
            // 
            this.btnPropApply.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnPropApply.ImageIndex = 13;
            this.btnPropApply.ImageList = this.ButtonImages;
            this.btnPropApply.Location = new System.Drawing.Point(12, 7);
            this.btnPropApply.Name = "btnPropApply";
            this.btnPropApply.Size = new System.Drawing.Size(175, 33);
            this.btnPropApply.TabIndex = 6;
            this.btnPropApply.Text = "��������� ���������";
            this.btnPropApply.Click += new System.EventHandler(this.btnPropApply_Click_1);
            // 
            // DelegateEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 470);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "DelegateEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "�������� ���������";
            this.Load += new System.EventHandler(this.DelegateEditForm_Load);
            this.Shown += new System.EventHandler(this.DelegateEditForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpParties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpFractions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpFractions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpSeat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Xpo.XPCollection xpDelegates;
        public DevExpress.Xpo.XPCollection xpFractions;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControl;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpParties;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpFractions;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpTypes;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpSeat;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpRegion;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catFIO;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLastName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFirstName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSecondName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catRegion;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catInfo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFraction;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowidParty_Name;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUse;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catSeat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMicNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRowNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSeatNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catNotes;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNotes;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnPropCancel;
        private DevExpress.XtraEditors.SimpleButton btnPropApply;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCardNum;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SimpleButton btnCardRead;

    }
}