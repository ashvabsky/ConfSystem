﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class RegionsForm : DevExpress.XtraEditors.XtraForm
    {
        public RegionsForm()
        {
            InitializeComponent();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            gridControl.MainView.ShowEditor();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int i = AddNewRegion();
            if (i >= 0)
                dataNavigator1.Position = i;

            gridControl.MainView.ShowEditor();
        }

        public int AddNewRegion()
        {
            RegionObj newRegion = new RegionObj();
            newRegion.Name = "Новый";
            newRegion.Number = "";
            newRegion.InUse = true;
            newRegion.Description = "";

            RegionObj last = (RegionObj)xpRegions[xpRegions.Count - 1];
            newRegion.id = last.id + 1;
            int i = xpRegions.Add(newRegion);
            newRegion.Save();
            return i;
        }

        public DialogResult ShowQuestion(string Quest)
        {
            return MessageBox.Show(this, Quest, "Вопрос", MessageBoxButtons.YesNoCancel);
        }

        public void ShowWarningMsg(string Text)
        {
            MessageBox.Show(this, Text, "Внимание!", MessageBoxButtons.OK);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            DialogResult dr = ShowQuestion("Вы уверены, что хотите удалить выделенную запись?");
            if (dr == DialogResult.Yes)
            {
                int i = dataNavigator1.Position;
                if (i >= 0)
                {
                    RegionObj o = (RegionObj)xpRegions[i];
                    if (o.Delegates.Count == 0)
                    {
                        //TODO: embrace in try...catch
                        xpRegions.Remove(o);
                    }
                    else
                    {
                        ShowWarningMsg("Невозможно удалить данную запись, так как на нее ссылаются другие записи.");
                    }
                }
            }
        }
 
    }
}