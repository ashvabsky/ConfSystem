using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class SpeechStartForm : DevExpress.XtraEditors.XtraForm
    {
        SpeechModeCntrler _Controller;

        public SpeechTypeObj _SpeechType;

        public SpeechStartForm()
        {
            InitializeComponent();
        }

        public SpeechStartForm(SpeechModeCntrler controller)
        {
            InitializeComponent();
            _Controller = controller;
        }

        public int SpeechTime
        {
            set { txtSpeechTime.Text = value.ToString(); }
            get { return Int32.Parse(txtSpeechTime.Text); }
        }

        public SpeechTypeObj SpeechType
        {
            set {_SpeechType = value;}
            get { return _SpeechType; }
        }

        public bool IsTribune
        {
            get { return chkTribune.Checked; }
        }

        private void SpeechStartForm_Load(object sender, EventArgs e)
        {
            edtSpeechType.EditValue = 1;
            chkTribune.Checked = true;
        }

        private void SpeechStartForm_Shown(object sender, EventArgs e)
        {
            _Controller.OnSpeechStartFormLoad();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {

        }

        private void edtSpeechType_EditValueChanged(object sender, EventArgs e)
        {
            int id = (int)edtSpeechType.EditValue;
            _SpeechType = (SpeechTypeObj)xpTypes.Lookup(id);
            txtSpeechTime.Text = _SpeechType.Value.ToString();
        }

        private void chkTribune_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkTribune_EditValueChanged(object sender, EventArgs e)
        {
            if (chkTribune.Checked == true)
                chkSeat.Checked = false;
        }

        private void chkSeat_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkSeat_EditValueChanged(object sender, EventArgs e)
        {
            if (chkSeat.Checked == true)
                chkTribune.Checked = false;
        }

    }
}