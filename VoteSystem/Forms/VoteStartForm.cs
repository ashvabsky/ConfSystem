using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class VoteStartForm : DevExpress.XtraEditors.XtraForm
    {
        VoteModeCntrler _Controller;

        public VoteStartForm()
        {
            InitializeComponent();
        }

        public VoteStartForm(VoteModeCntrler controller)
        {
            InitializeComponent();
            _Controller = controller;
        }

        public int VoteTime
        {
            set { txtVoteTime.Text = value.ToString(); }
            get { return Int32.Parse(txtVoteTime.Text); }
        }

        public int DelegatesQnty
        {
            set { txtDelegateQnty.Text = value.ToString(); }
        }

        public bool IsQuorum
        {
            set { if (value == true) txtIsQuorum.Text = "����"; else txtIsQuorum.Text = "���"; }
        }

        public bool IsVoteProccess
        {
            set { chkVoteToScreen.Checked = value; }
            get { return chkVoteToScreen.Checked; }
        }

        public bool IsVoteToScreen
        {
            set { chkVoteToScreen.Checked = value; }
            get { return chkVoteToScreen.Checked; }
        }

/*
        private void btnMonitorPrepare_CheckedChanged(object sender, EventArgs e)
        {
            if (btnMonitorVotePrepare.Checked == true)
            {
                _Controller.ShowMonitorPreview("VotePrepare");
            }
        }
*/

        private void VoteStartForm_Load(object sender, EventArgs e)
        {
        }

        private void VoteStartForm_Shown(object sender, EventArgs e)
        {
            _Controller.OnVoteStartFormLoad();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {

        }


    }
}