namespace VoteSystem
{
    partial class SpeechStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpeechStartForm));
            this.chkVoteToScreen = new DevExpress.XtraEditors.CheckEdit();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtSpeechTime = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.edtSpeechType = new DevExpress.XtraEditors.LookUpEdit();
            this.xpTypes = new DevExpress.Xpo.XPCollection();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.chkTribune = new DevExpress.XtraEditors.CheckEdit();
            this.chkSeat = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoteToScreen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpeechTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtSpeechType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTribune.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSeat.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // chkVoteToScreen
            // 
            this.chkVoteToScreen.EditValue = true;
            this.chkVoteToScreen.Location = new System.Drawing.Point(28, 223);
            this.chkVoteToScreen.Name = "chkVoteToScreen";
            this.chkVoteToScreen.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.chkVoteToScreen.Properties.Appearance.Options.UseFont = true;
            this.chkVoteToScreen.Properties.Caption = "�������� ���������� �� ��������";
            this.chkVoteToScreen.Size = new System.Drawing.Size(270, 21);
            this.chkVoteToScreen.TabIndex = 8;
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ImageIndex = 2;
            this.btnClose.ImageList = this.ButtonImages;
            this.btnClose.Location = new System.Drawing.Point(217, 260);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(113, 37);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "��������";
            // 
            // btnStart
            // 
            this.btnStart.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnStart.ImageIndex = 0;
            this.btnStart.ImageList = this.ButtonImages;
            this.btnStart.Location = new System.Drawing.Point(28, 260);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(148, 37);
            this.btnStart.TabIndex = 23;
            this.btnStart.Text = "������ �����������";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(290, 74);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(22, 13);
            this.labelControl2.TabIndex = 22;
            this.labelControl2.Text = "���.";
            // 
            // txtSpeechTime
            // 
            this.txtSpeechTime.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSpeechTime.Location = new System.Drawing.Point(223, 67);
            this.txtSpeechTime.Name = "txtSpeechTime";
            this.txtSpeechTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtSpeechTime.Properties.IsFloatValue = false;
            this.txtSpeechTime.Properties.MaxValue = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.txtSpeechTime.Size = new System.Drawing.Size(61, 20);
            this.txtSpeechTime.TabIndex = 21;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(30, 70);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(153, 16);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "����� �� �����������:";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(28, 190);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Caption = "��������� �������� �� ��������� �������";
            this.checkEdit1.Size = new System.Drawing.Size(327, 21);
            this.checkEdit1.TabIndex = 25;
            // 
            // edtSpeechType
            // 
            this.edtSpeechType.Location = new System.Drawing.Point(116, 21);
            this.edtSpeechType.Name = "edtSpeechType";
            this.edtSpeechType.Properties.AutoHeight = false;
            this.edtSpeechType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtSpeechType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "����� ������", 33, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.edtSpeechType.Properties.DataSource = this.xpTypes;
            this.edtSpeechType.Properties.DisplayMember = "Name";
            this.edtSpeechType.Properties.NullText = "";
            this.edtSpeechType.Properties.ShowHeader = false;
            this.edtSpeechType.Properties.ValueMember = "id";
            this.edtSpeechType.Size = new System.Drawing.Size(168, 24);
            this.edtSpeechType.TabIndex = 258;
            this.edtSpeechType.EditValueChanged += new System.EventHandler(this.edtSpeechType_EditValueChanged);
            // 
            // xpTypes
            // 
            this.xpTypes.DeleteObjectOnRemove = true;
            this.xpTypes.ObjectType = typeof(VoteSystem.SpeechTypeObj);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(30, 24);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(28, 16);
            this.labelControl3.TabIndex = 259;
            this.labelControl3.Text = "���:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(30, 113);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(80, 16);
            this.labelControl4.TabIndex = 262;
            this.labelControl4.Text = "��������� �";
            // 
            // chkTribune
            // 
            this.chkTribune.Location = new System.Drawing.Point(66, 144);
            this.chkTribune.Name = "chkTribune";
            this.chkTribune.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkTribune.Properties.Appearance.Options.UseFont = true;
            this.chkTribune.Properties.Caption = "� �������";
            this.chkTribune.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkTribune.Size = new System.Drawing.Size(96, 21);
            this.chkTribune.TabIndex = 264;
            this.chkTribune.EditValueChanged += new System.EventHandler(this.chkTribune_EditValueChanged);
            this.chkTribune.CheckedChanged += new System.EventHandler(this.chkTribune_CheckedChanged);
            // 
            // chkSeat
            // 
            this.chkSeat.Location = new System.Drawing.Point(209, 144);
            this.chkSeat.Name = "chkSeat";
            this.chkSeat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkSeat.Properties.Appearance.Options.UseFont = true;
            this.chkSeat.Properties.Caption = "� �����";
            this.chkSeat.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkSeat.Size = new System.Drawing.Size(75, 21);
            this.chkSeat.TabIndex = 265;
            this.chkSeat.EditValueChanged += new System.EventHandler(this.chkSeat_EditValueChanged);
            this.chkSeat.CheckedChanged += new System.EventHandler(this.chkSeat_CheckedChanged);
            // 
            // SpeechStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 319);
            this.Controls.Add(this.chkSeat);
            this.Controls.Add(this.chkTribune);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.edtSpeechType);
            this.Controls.Add(this.checkEdit1);
            this.Controls.Add(this.chkVoteToScreen);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtSpeechTime);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SpeechStartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������� �����������";
            this.Load += new System.EventHandler(this.SpeechStartForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chkVoteToScreen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpeechTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtSpeechType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTribune.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSeat.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit chkVoteToScreen;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit txtSpeechTime;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LookUpEdit edtSpeechType;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.Xpo.XPCollection xpTypes;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit chkTribune;
        private DevExpress.XtraEditors.CheckEdit chkSeat;
    }
}