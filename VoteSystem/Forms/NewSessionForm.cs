using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class NewSessionForm : DevExpress.XtraEditors.XtraForm
    {
        public NewSessionForm()
        {
            InitializeComponent();
        }

        public string SessionName
        {
            get { return txtSessionName.Text; }
            set { txtSessionName.Text = value; }
        }

    }
}