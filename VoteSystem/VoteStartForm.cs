using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class VoteStartForm : DevExpress.XtraEditors.XtraForm
    {
        VoteModeCntrler _Controller;

        public VoteStartForm()
        {
            InitializeComponent();
        }

        public VoteStartForm(VoteModeCntrler controller)
        {
            InitializeComponent();
            _Controller = controller;
        }

        public int VoteTime
        {
            set { txtVoteTime.Text = value.ToString(); }
            get { return Int32.Parse(txtVoteTime.Text); }
        }

        public int DelegatesQnty
        {
            set { txtDelegateQnty.Text = value.ToString(); }
        }

        public bool IsQuorum
        {
            set { if (value == true) txtIsQuorum.Text = "����"; else txtIsQuorum.Text = "���"; }
        }

        public bool IsVoteProccess
        {
            set { chkVoteProccess.Checked = value; }
            get { return chkVoteProccess.Checked; }
        }

        public bool IsTotalsToScreen
        {
            set { chkTotalsToScreen.Checked = value; }
            get { return chkTotalsToScreen.Checked; }
        }

        private void btnMonitorPrepare_CheckedChanged(object sender, EventArgs e)
        {
            if (btnMonitorVotePrepare.Checked == true)
            {
                _Controller.ShowMonitorPreview("VotePrepare");
            }
        }

        private void chkVoteProccess_CheckedChanged(object sender, EventArgs e)
        {
            if (chkVoteProccess.Checked == true)
            {
                _Controller.ShowMonitorPreview("VoteStart");
            }
        }

        private void chkTotalsToScreen_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTotalsToScreen.Checked == true)
            {
                _Controller.ShowMonitorPreview("VoteResults");
            }
        }


    }
}