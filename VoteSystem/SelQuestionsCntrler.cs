﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public class SelQuestionsCntrler
    {
        SessionObj _TheSession;
        IVoteQuestPage _View;
        MainCntrler _mainController;
        IQuestProperties _questSheetView;
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        S_QuestionObj _theQuestion;
        NestedUnitOfWork _nuow;

        XPCollection _Questions; // вопросы сессии 

//        XPCollection<S_QuestionObj> _Questions2 = new XPCollection<S_QuestionObj>(Session.DefaultSession);

        XPCollection<VoteTypeObj> _VoteTypes;
        XPCollection<VoteKindObj> _VoteKinds;
        XPCollection<VoteQntyObj> _VoteQntyTypes;
        XPCollection<DecisionProcTypeObj> _DecisionProcTypes;
        XPCollection<QuotaProcTypeObj> _QuotaProcTypes;

        public S_QuestionObj CurrentQuestion
        {
            get { return _theQuestion; }
        }

        public SelQuestionsCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IVoteQuestPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
            _questSheetView = _View.GetQuestPropertiesInterface();
        }

        public void Init(ref XPCollection session_questions)
        {
            _TheSession = DataModel.TheSession;
            _Questions = session_questions;
            _VoteTypes = DataModel.VoteTypes;
            _VoteKinds = DataModel.VoteKinds;
            _VoteQntyTypes = DataModel.VoteQntyTypes;
            _DecisionProcTypes = DataModel.DecisionProcTypes;
            _QuotaProcTypes = DataModel.QuotaProcTypes;
/*
            ((System.ComponentModel.ISupportInitialize)(_Questions2)).BeginInit();
            _Questions2.DeleteObjectOnRemove = true;
            _Questions2.ObjectType = typeof(VoteSystem.S_QuestionObj);
            _Questions2.DisplayableProperties = "This;id";
            ((System.ComponentModel.ISupportInitialize)(_Questions2)).EndInit();
*/
            _questSheetView.FillDropDowns();
        }

        public void InitSelQuestionsPage()
        {
            _Questions.Reload();
            
            _View.InitSelQuestPage();

            _questSheetView.SetViewMode();
            UpdatePropData(_View.NavigatorPosition);
        }

        public void Save()
        {

        }

        public void UpdatePropData(int pos)
        {
            _theQuestion = null;

            if (pos < 0)
                return;

            if (pos >= _Questions.Count)
                return;

            _theQuestion = _Questions[pos] as S_QuestionObj;

            ShowRecProperties(_theQuestion);

            _mainController.CurrentActivity = MainCntrler.Activities.SelectQuestion;
  

        }

        public void ShowRecProperties(S_QuestionObj _theQuestion)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties["Number"] = _theQuestion.id; 
            properties["Caption"] = _theQuestion.idQuestion.Caption;
            properties["CrDate"] = _theQuestion.idQuestion.CrDate;
            properties["Notes"] = _theQuestion.Notes;
            properties["Description"] = _theQuestion.Description;

            properties["VoteType_Value"] = _theQuestion.idVoteType.Name;
            properties["VoteType_Name"] = _theQuestion.idVoteType.Name;
            properties["VoteKind_Value"] = _theQuestion.idVoteKind.Name;
            properties["VoteKind_Name"] = _theQuestion.idVoteKind.Name;

            if (_theQuestion.ReadNum == 1)
                properties["ReadNum"] = "первое чтение";
            else if (_theQuestion.ReadNum == 2)
                properties["ReadNum"] = "второе чтение";
            else if (_theQuestion.ReadNum == 3)
                properties["ReadNum"] = "третье чтение";

            properties["VoteQntyType_Value"] = _theQuestion.idVoteQnty.Value;
            properties["VoteQntyType_Name"] = _theQuestion.idVoteQnty.Name;

            properties["DecisionProc_Value"] = _theQuestion.DecisionProcValue;
            properties["DecisionProcType_Name"] = _theQuestion.idDecisionProcType.Name;
            properties["DecisionProcType_Value"] = _theQuestion.idDecisionProcType.Value;

            properties["QuotaProc_Value"] = _theQuestion.QuotaProcentValue;
            properties["QuotaProcType_Name"] = _theQuestion.idQuotaProcentType.Name;
            properties["QuotaProcType_Value"] = _theQuestion.idQuotaProcentType.Value;

            _questSheetView.SetPropertiesQuestions(properties);
        }

        public void RemoveItem()
        {
            if (_theQuestion == null)
                return;

            _Questions.Remove(_theQuestion);
        }

        public int AppendItem()
        {
            List<int> arr = new List<int>();
            foreach (S_QuestionObj o in _Questions)
            {
                arr.Add(o.idQuestion.id);
            }

            int idMax =  GetMaxID();

            QuestionsCntrler c = (QuestionsCntrler)_mainController.GetSubController("Questions List Controller");
            QuestObj Question = c.GetSelectedItem(arr);

            int new_index = 0;
            if (Question != null)
            {
                S_QuestionObj S_Question = new S_QuestionObj();

                S_Question.idQuestion = Question;
                S_Question.idSession = _TheSession;
                S_Question.id = idMax + 1;
                S_Question.idVoteKind = Question.idKind;
                S_Question.idVoteType = Question.idType;
                S_Question.ReadNum = 1;
                S_Question.idVoteQnty = (VoteQntyObj)_VoteQntyTypes[0];
                S_Question.QuotaProcentValue = 0;
                S_Question.idDecisionProcType = (DecisionProcTypeObj)_DecisionProcTypes[0];
                S_Question.idQuotaProcentType = (QuotaProcTypeObj)_QuotaProcTypes[0];
                S_Question.Notes = "";
                S_Question.Description = Question.Description;
                S_Question.idResultValue = DataModel.QResTypes[0];
                S_Question.VoteDateTime = DateTime.Now;
                new_index = _Questions.Add(S_Question);
                S_Question.Save();
            }

            return new_index;

        }

        public void EditRecord(int pos)
        {
            _theQuestion = null;

            if (pos <= -1)
                return;

            _nuow = _Questions.Session.BeginNestedUnitOfWork();

            _theQuestion = _nuow.GetNestedObject(_Questions[pos]) as S_QuestionObj;

            _nuow.BeginTransaction();
            _questSheetView.PropertiesQuestions_StartEdit();
        }


        public void ApplyChanges(Dictionary<string, object> properties)
        {
            _theQuestion.Notes = (string)properties["Notes"];

            // тип голосования
            CriteriaOperator criteria1 = CriteriaOperator.Parse("Name == ?", properties["VoteType_Name"].ToString());

            _VoteTypes.Filter = criteria1;
            if (_VoteTypes.Count == 0) _VoteTypes.Filter = null;

            VoteTypeObj vtype = _nuow.GetNestedObject(_VoteTypes[0]) as VoteTypeObj;

            _theQuestion.idVoteType = vtype;

            _VoteTypes.Filter = null;

            // вид голосования

            CriteriaOperator criteria2 = CriteriaOperator.Parse("Name == ?", properties["VoteKind_Name"].ToString());
            _VoteKinds.Filter = criteria2;
            if (_VoteKinds.Count == 0) _VoteKinds.Filter = null;

            VoteKindObj vkind = _nuow.GetNestedObject(_VoteKinds[0]) as VoteKindObj;

            _theQuestion.idVoteKind = vkind;
            _VoteKinds.Filter = null;

            // Порог принятия решения. Число голосов
            CriteriaOperator criteria3 = CriteriaOperator.Parse("Name == ?", properties["VoteQntyType_Name"].ToString());
            _VoteQntyTypes.Filter = criteria3;
            if (_VoteQntyTypes.Count == 0) _VoteQntyTypes.Filter = null;

            VoteQntyObj vqnty = _nuow.GetNestedObject(_VoteQntyTypes[0]) as VoteQntyObj;

            _theQuestion.idVoteQnty = vqnty;
            _VoteQntyTypes.Filter = null;

            // Порог принятия решения. В процентах
            _theQuestion.DecisionProcValue = (int)properties["DecisionProc_Value"];

            // Порог принятия решения. От числа депутатов
            CriteriaOperator criteria4 = CriteriaOperator.Parse("Name == ?", properties["DecisionProcType_Name"].ToString());
            _DecisionProcTypes.Filter = criteria4;
            if (_DecisionProcTypes.Count == 0) _DecisionProcTypes.Filter = null;

            DecisionProcTypeObj dpt = _nuow.GetNestedObject(_DecisionProcTypes[0]) as DecisionProcTypeObj;

            _theQuestion.idDecisionProcType = dpt;
            _DecisionProcTypes.Filter = null;

            // Тип квоты на кворум
            CriteriaOperator criteria5 = CriteriaOperator.Parse("Name == ?", properties["QuotaProcType_Name"].ToString());
            _QuotaProcTypes.Filter = criteria5;
            if (_QuotaProcTypes.Count == 0) _QuotaProcTypes.Filter = null;

            QuotaProcTypeObj qpt = _nuow.GetNestedObject(_QuotaProcTypes[0]) as QuotaProcTypeObj;

            _theQuestion.idQuotaProcentType = qpt;
            _QuotaProcTypes.Filter = null;

            _nuow.CommitChanges();

            _questSheetView.Properties_FinishEdit();
        }

        public void CancelChanges()
        {
            _nuow.RollbackTransaction();
            _questSheetView.Properties_FinishEdit();
        }

        public int GetMaxID()
        {
            List<int> arr_Max = new List<int>();
            foreach (S_QuestionObj o in DataModel.SesQuestions)
            {
                arr_Max.Add(o.id);
            }

            int idMax = 0;
            if (arr_Max.Count > 0)
                idMax = arr_Max.Max();

            return idMax;
        }

        public int AddNewVoteQuestion(S_QuestionObj q)
        {
            CriteriaOperator criteria = CriteriaOperator.Parse("idQuestion == ? AND idSession = ?", q.idQuestion.ToString(), q.idSession.ToString());
            _Questions.Filter = criteria;
            if (_Questions.Count != 0)
            {
                S_QuestionObj qo = _Questions[0] as S_QuestionObj;
                _Questions.Filter = null;
//                return 0; // вопрос с новым чтением уже есть в списке, новый не добавляем тогда
                return _Questions.IndexOf(qo);
            }

            _Questions.Filter = null;

            q.id = GetMaxID() + 1;
            int new_index = _Questions.Add(q);
            q.Save();
            return new_index;
        }

        public void SetCurrentPos(int currPos)
        {
            _View.SetNavigatorPos_QuestPage(currPos);
            UpdatePropData(_View.NavigatorPosition);
        }

        public void SetPosition(S_QuestionObj selQuestion)
        {
            _Questions.Reload();
            int currPos = _Questions.IndexOf(selQuestion);

            _View.SetNavigatorPos_QuestPage(currPos);
        }
    }
}
 