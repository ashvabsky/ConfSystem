namespace VoteSystem
{
    partial class MonitorPreviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonitorPreviewForm));
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit4 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit3 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit5 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit10 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit9 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit7 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit8 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit12 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit13 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit14 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit16 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit6 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit11 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit15 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit19 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit18 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit17 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit20 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit34 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit33 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit32 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit31 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit30 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit29 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit25 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit26 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit27 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit28 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit23 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit21 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit22 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit24 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.tmrForm = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit8.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit14.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit15.Properties)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit20.Properties)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(801, 540);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6});
            this.xtraTabControl1.Text = "xtraTabControl1";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.panelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage1.Text = "������";
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl1.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControl1.ContentImage")));
            this.panelControl1.Controls.Add(this.memoEdit4);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.memoEdit3);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.memoEdit2);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl31);
            this.panelControl1.Controls.Add(this.labelControl14);
            this.panelControl1.Controls.Add(this.memoEdit1);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(792, 509);
            this.panelControl1.TabIndex = 7;
            // 
            // memoEdit4
            // 
            this.memoEdit4.EditValue = "����";
            this.memoEdit4.Location = new System.Drawing.Point(528, 338);
            this.memoEdit4.Name = "memoEdit4";
            this.memoEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit4.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit4.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit4.Properties.Appearance.Options.UseFont = true;
            this.memoEdit4.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit4.Properties.ReadOnly = true;
            this.memoEdit4.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit4.Size = new System.Drawing.Size(72, 23);
            this.memoEdit4.TabIndex = 23;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(423, 338);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(81, 23);
            this.labelControl7.TabIndex = 22;
            this.labelControl7.Text = "������:";
            // 
            // memoEdit3
            // 
            this.memoEdit3.EditValue = "53";
            this.memoEdit3.Location = new System.Drawing.Point(303, 339);
            this.memoEdit3.Name = "memoEdit3";
            this.memoEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit3.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit3.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit3.Properties.Appearance.Options.UseFont = true;
            this.memoEdit3.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit3.Properties.ReadOnly = true;
            this.memoEdit3.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit3.Size = new System.Drawing.Size(72, 23);
            this.memoEdit3.TabIndex = 21;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Location = new System.Drawing.Point(109, 338);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(179, 23);
            this.labelControl6.TabIndex = 20;
            this.labelControl6.Text = "��������� � ����:";
            // 
            // memoEdit2
            // 
            this.memoEdit2.EditValue = "��������������� ������� ��������� ����� �������������� ���������� � ����� �������" +
                "����";
            this.memoEdit2.Location = new System.Drawing.Point(38, 211);
            this.memoEdit2.Name = "memoEdit2";
            this.memoEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit2.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit2.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit2.Properties.Appearance.Options.UseFont = true;
            this.memoEdit2.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit2.Properties.ReadOnly = true;
            this.memoEdit2.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit2.Size = new System.Drawing.Size(728, 108);
            this.memoEdit2.TabIndex = 19;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(28, 189);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(102, 23);
            this.labelControl5.TabIndex = 18;
            this.labelControl5.Text = "��������:";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl31.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl31.Appearance.Options.UseBackColor = true;
            this.labelControl31.Appearance.Options.UseFont = true;
            this.labelControl31.Appearance.Options.UseForeColor = true;
            this.labelControl31.Location = new System.Drawing.Point(423, 429);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(55, 23);
            this.labelControl31.TabIndex = 17;
            this.labelControl31.Text = "16:04";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Location = new System.Drawing.Point(239, 429);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(136, 23);
            this.labelControl14.TabIndex = 16;
            this.labelControl14.Text = "16 ��� 2010�.";
            // 
            // memoEdit1
            // 
            this.memoEdit1.EditValue = "\"������������� �������������� ���������� � ����� �����������\"";
            this.memoEdit1.Location = new System.Drawing.Point(38, 98);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit1.Size = new System.Drawing.Size(728, 52);
            this.memoEdit1.TabIndex = 15;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(28, 76);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(126, 23);
            this.labelControl2.TabIndex = 14;
            this.labelControl2.Text = "���� ������:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(154, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(479, 23);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "��������������� ����� ���������� ����������";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.PageClient.BackColor = System.Drawing.Color.Maroon;
            this.xtraTabPage2.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage2.Controls.Add(this.panelControl3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage2.Text = "�������";
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl3.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControl3.ContentImage")));
            this.panelControl3.Controls.Add(this.memoEdit5);
            this.panelControl3.Controls.Add(this.labelControl8);
            this.panelControl3.Controls.Add(this.memoEdit10);
            this.panelControl3.Controls.Add(this.labelControl16);
            this.panelControl3.Controls.Add(this.memoEdit9);
            this.panelControl3.Controls.Add(this.labelControl15);
            this.panelControl3.Controls.Add(this.memoEdit7);
            this.panelControl3.Controls.Add(this.labelControl10);
            this.panelControl3.Controls.Add(this.labelControl11);
            this.panelControl3.Controls.Add(this.labelControl12);
            this.panelControl3.Controls.Add(this.memoEdit8);
            this.panelControl3.Controls.Add(this.labelControl13);
            this.panelControl3.Controls.Add(this.labelControl30);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(792, 509);
            this.panelControl3.TabIndex = 8;
            // 
            // memoEdit5
            // 
            this.memoEdit5.EditValue = "��� � 1 ����� �54 �������� � 73";
            this.memoEdit5.Location = new System.Drawing.Point(155, 343);
            this.memoEdit5.Name = "memoEdit5";
            this.memoEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit5.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit5.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit5.Properties.Appearance.Options.UseFont = true;
            this.memoEdit5.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit5.Properties.ReadOnly = true;
            this.memoEdit5.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit5.Size = new System.Drawing.Size(326, 23);
            this.memoEdit5.TabIndex = 29;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(145, 321);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(67, 23);
            this.labelControl8.TabIndex = 28;
            this.labelControl8.Text = "�����:";
            // 
            // memoEdit10
            // 
            this.memoEdit10.EditValue = "������-��������";
            this.memoEdit10.Location = new System.Drawing.Point(155, 277);
            this.memoEdit10.Name = "memoEdit10";
            this.memoEdit10.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit10.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit10.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit10.Properties.Appearance.Options.UseFont = true;
            this.memoEdit10.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit10.Properties.ReadOnly = true;
            this.memoEdit10.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit10.Size = new System.Drawing.Size(175, 23);
            this.memoEdit10.TabIndex = 27;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl16.Appearance.Options.UseBackColor = true;
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(145, 250);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(94, 23);
            this.labelControl16.TabIndex = 26;
            this.labelControl16.Text = "�������:";
            // 
            // memoEdit9
            // 
            this.memoEdit9.EditValue = "������-��������";
            this.memoEdit9.Location = new System.Drawing.Point(155, 216);
            this.memoEdit9.Name = "memoEdit9";
            this.memoEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit9.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit9.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit9.Properties.Appearance.Options.UseFont = true;
            this.memoEdit9.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit9.Properties.ReadOnly = true;
            this.memoEdit9.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit9.Size = new System.Drawing.Size(175, 23);
            this.memoEdit9.TabIndex = 25;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl15.Appearance.Options.UseBackColor = true;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(145, 194);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(79, 23);
            this.labelControl15.TabIndex = 24;
            this.labelControl15.Text = "������:";
            // 
            // memoEdit7
            // 
            this.memoEdit7.EditValue = "������-��������";
            this.memoEdit7.Location = new System.Drawing.Point(155, 164);
            this.memoEdit7.Name = "memoEdit7";
            this.memoEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit7.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit7.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit7.Properties.Appearance.Options.UseFont = true;
            this.memoEdit7.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit7.Properties.ReadOnly = true;
            this.memoEdit7.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit7.Size = new System.Drawing.Size(175, 23);
            this.memoEdit7.TabIndex = 19;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl10.Appearance.Options.UseBackColor = true;
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Location = new System.Drawing.Point(145, 142);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(75, 23);
            this.labelControl10.TabIndex = 18;
            this.labelControl10.Text = "������:";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Location = new System.Drawing.Point(395, 424);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(55, 23);
            this.labelControl11.TabIndex = 17;
            this.labelControl11.Text = "16:04";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(218, 424);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(136, 23);
            this.labelControl12.TabIndex = 16;
            this.labelControl12.Text = "16 ��� 2010�.";
            // 
            // memoEdit8
            // 
            this.memoEdit8.EditValue = "������� ������� ��������";
            this.memoEdit8.Location = new System.Drawing.Point(167, 85);
            this.memoEdit8.Name = "memoEdit8";
            this.memoEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit8.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit8.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit8.Properties.Appearance.Options.UseFont = true;
            this.memoEdit8.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit8.Properties.ReadOnly = true;
            this.memoEdit8.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit8.Size = new System.Drawing.Size(560, 30);
            this.memoEdit8.TabIndex = 15;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Location = new System.Drawing.Point(145, 56);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(87, 23);
            this.labelControl13.TabIndex = 14;
            this.labelControl13.Text = "�������:";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl30.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl30.Appearance.Options.UseBackColor = true;
            this.labelControl30.Appearance.Options.UseFont = true;
            this.labelControl30.Appearance.Options.UseForeColor = true;
            this.labelControl30.Location = new System.Drawing.Point(145, 10);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(479, 23);
            this.labelControl30.TabIndex = 13;
            this.labelControl30.Text = "��������������� ����� ���������� ����������";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.panelControl4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage3.Text = "������";
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl4.ContentImage = global::VoteSystem.Properties.Resources.ScreenBackground;
            this.panelControl4.Controls.Add(this.memoEdit12);
            this.panelControl4.Controls.Add(this.labelControl18);
            this.panelControl4.Controls.Add(this.memoEdit13);
            this.panelControl4.Controls.Add(this.labelControl19);
            this.panelControl4.Controls.Add(this.labelControl20);
            this.panelControl4.Controls.Add(this.labelControl21);
            this.panelControl4.Controls.Add(this.memoEdit14);
            this.panelControl4.Controls.Add(this.labelControl22);
            this.panelControl4.Controls.Add(this.labelControl23);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(792, 509);
            this.panelControl4.TabIndex = 9;
            // 
            // memoEdit12
            // 
            this.memoEdit12.EditValue = "�������������, ��������";
            this.memoEdit12.Location = new System.Drawing.Point(218, 349);
            this.memoEdit12.Name = "memoEdit12";
            this.memoEdit12.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit12.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit12.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit12.Properties.Appearance.Options.UseFont = true;
            this.memoEdit12.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit12.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit12.Properties.ReadOnly = true;
            this.memoEdit12.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit12.Size = new System.Drawing.Size(364, 23);
            this.memoEdit12.TabIndex = 25;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl18.Appearance.Options.UseBackColor = true;
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Location = new System.Drawing.Point(56, 347);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(134, 23);
            this.labelControl18.TabIndex = 24;
            this.labelControl18.Text = "�����������:";
            // 
            // memoEdit13
            // 
            this.memoEdit13.EditValue = resources.GetString("memoEdit13.EditValue");
            this.memoEdit13.Location = new System.Drawing.Point(66, 184);
            this.memoEdit13.Name = "memoEdit13";
            this.memoEdit13.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit13.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit13.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit13.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit13.Properties.Appearance.Options.UseFont = true;
            this.memoEdit13.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit13.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit13.Properties.ReadOnly = true;
            this.memoEdit13.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit13.Size = new System.Drawing.Size(677, 125);
            this.memoEdit13.TabIndex = 19;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl19.Appearance.Options.UseBackColor = true;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Location = new System.Drawing.Point(56, 159);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(102, 23);
            this.labelControl19.TabIndex = 18;
            this.labelControl19.Text = "��������:";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl20.Appearance.Options.UseBackColor = true;
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Appearance.Options.UseForeColor = true;
            this.labelControl20.Location = new System.Drawing.Point(427, 422);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(55, 23);
            this.labelControl20.TabIndex = 17;
            this.labelControl20.Text = "16:04";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl21.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl21.Appearance.Options.UseBackColor = true;
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Appearance.Options.UseForeColor = true;
            this.labelControl21.Location = new System.Drawing.Point(263, 422);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(136, 23);
            this.labelControl21.TabIndex = 16;
            this.labelControl21.Text = "16 ��� 2010�.";
            // 
            // memoEdit14
            // 
            this.memoEdit14.EditValue = " �������� ���������������� ���������� � ������ ����� ���������� �� 30.11.2006�.";
            this.memoEdit14.Location = new System.Drawing.Point(66, 91);
            this.memoEdit14.Name = "memoEdit14";
            this.memoEdit14.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit14.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit14.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit14.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit14.Properties.Appearance.Options.UseFont = true;
            this.memoEdit14.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit14.Properties.ReadOnly = true;
            this.memoEdit14.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit14.Size = new System.Drawing.Size(677, 53);
            this.memoEdit14.TabIndex = 15;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl22.Appearance.Options.UseBackColor = true;
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Appearance.Options.UseForeColor = true;
            this.labelControl22.Location = new System.Drawing.Point(56, 69);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(78, 23);
            this.labelControl22.TabIndex = 14;
            this.labelControl22.Text = "������:";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl23.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl23.Appearance.Options.UseBackColor = true;
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.Appearance.Options.UseForeColor = true;
            this.labelControl23.Location = new System.Drawing.Point(145, 5);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(479, 23);
            this.labelControl23.TabIndex = 13;
            this.labelControl23.Text = "��������������� ����� ���������� ����������";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.panelControl5);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage4.Text = "�����������";
            // 
            // panelControl5
            // 
            this.panelControl5.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl5.Appearance.Options.UseBackColor = true;
            this.panelControl5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl5.ContentImage = global::VoteSystem.Properties.Resources.ScreenBackground;
            this.panelControl5.Controls.Add(this.memoEdit16);
            this.panelControl5.Controls.Add(this.labelControl29);
            this.panelControl5.Controls.Add(this.labelControl28);
            this.panelControl5.Controls.Add(this.memoEdit6);
            this.panelControl5.Controls.Add(this.labelControl9);
            this.panelControl5.Controls.Add(this.memoEdit11);
            this.panelControl5.Controls.Add(this.labelControl17);
            this.panelControl5.Controls.Add(this.labelControl24);
            this.panelControl5.Controls.Add(this.labelControl25);
            this.panelControl5.Controls.Add(this.memoEdit15);
            this.panelControl5.Controls.Add(this.labelControl26);
            this.panelControl5.Controls.Add(this.labelControl27);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(792, 509);
            this.panelControl5.TabIndex = 10;
            // 
            // memoEdit16
            // 
            this.memoEdit16.EditValue = "2 ���.";
            this.memoEdit16.Location = new System.Drawing.Point(222, 383);
            this.memoEdit16.Name = "memoEdit16";
            this.memoEdit16.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit16.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit16.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit16.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit16.Properties.Appearance.Options.UseFont = true;
            this.memoEdit16.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit16.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit16.Properties.ReadOnly = true;
            this.memoEdit16.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit16.Size = new System.Drawing.Size(364, 23);
            this.memoEdit16.TabIndex = 42;
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl29.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl29.Appearance.Options.UseBackColor = true;
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.Appearance.Options.UseForeColor = true;
            this.labelControl29.Location = new System.Drawing.Point(56, 383);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(69, 23);
            this.labelControl29.TabIndex = 41;
            this.labelControl29.Text = "�����:";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl28.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl28.Appearance.Options.UseBackColor = true;
            this.labelControl28.Appearance.Options.UseFont = true;
            this.labelControl28.Appearance.Options.UseForeColor = true;
            this.labelControl28.Location = new System.Drawing.Point(206, 52);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(343, 25);
            this.labelControl28.TabIndex = 40;
            this.labelControl28.Text = "������������� � �����������!";
            // 
            // memoEdit6
            // 
            this.memoEdit6.EditValue = "�������������, ��������";
            this.memoEdit6.Location = new System.Drawing.Point(222, 347);
            this.memoEdit6.Name = "memoEdit6";
            this.memoEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit6.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit6.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit6.Properties.Appearance.Options.UseFont = true;
            this.memoEdit6.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit6.Properties.ReadOnly = true;
            this.memoEdit6.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit6.Size = new System.Drawing.Size(364, 23);
            this.memoEdit6.TabIndex = 25;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Location = new System.Drawing.Point(56, 347);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(134, 23);
            this.labelControl9.TabIndex = 24;
            this.labelControl9.Text = "�����������:";
            // 
            // memoEdit11
            // 
            this.memoEdit11.EditValue = resources.GetString("memoEdit11.EditValue");
            this.memoEdit11.Location = new System.Drawing.Point(66, 216);
            this.memoEdit11.Name = "memoEdit11";
            this.memoEdit11.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit11.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit11.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit11.Properties.Appearance.Options.UseFont = true;
            this.memoEdit11.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit11.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit11.Properties.ReadOnly = true;
            this.memoEdit11.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit11.Size = new System.Drawing.Size(677, 125);
            this.memoEdit11.TabIndex = 19;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl17.Appearance.Options.UseBackColor = true;
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Appearance.Options.UseForeColor = true;
            this.labelControl17.Location = new System.Drawing.Point(56, 191);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(102, 23);
            this.labelControl17.TabIndex = 18;
            this.labelControl17.Text = "��������:";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl24.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl24.Appearance.Options.UseBackColor = true;
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Appearance.Options.UseForeColor = true;
            this.labelControl24.Location = new System.Drawing.Point(427, 422);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(55, 23);
            this.labelControl24.TabIndex = 17;
            this.labelControl24.Text = "16:04";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl25.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl25.Appearance.Options.UseBackColor = true;
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.Appearance.Options.UseForeColor = true;
            this.labelControl25.Location = new System.Drawing.Point(263, 422);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(136, 23);
            this.labelControl25.TabIndex = 16;
            this.labelControl25.Text = "16 ��� 2010�.";
            // 
            // memoEdit15
            // 
            this.memoEdit15.EditValue = " �������� ���������������� ���������� � ������ ����� ���������� �� 30.11.2006�.";
            this.memoEdit15.Location = new System.Drawing.Point(66, 123);
            this.memoEdit15.Name = "memoEdit15";
            this.memoEdit15.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit15.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit15.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit15.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit15.Properties.Appearance.Options.UseFont = true;
            this.memoEdit15.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit15.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit15.Properties.ReadOnly = true;
            this.memoEdit15.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit15.Size = new System.Drawing.Size(677, 53);
            this.memoEdit15.TabIndex = 15;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl26.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl26.Appearance.Options.UseBackColor = true;
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.Appearance.Options.UseForeColor = true;
            this.labelControl26.Location = new System.Drawing.Point(56, 101);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(78, 23);
            this.labelControl26.TabIndex = 14;
            this.labelControl26.Text = "������:";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl27.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl27.Appearance.Options.UseBackColor = true;
            this.labelControl27.Appearance.Options.UseFont = true;
            this.labelControl27.Appearance.Options.UseForeColor = true;
            this.labelControl27.Location = new System.Drawing.Point(145, 5);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(479, 23);
            this.labelControl27.TabIndex = 13;
            this.labelControl27.Text = "��������������� ����� ���������� ����������";
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.panelControl6);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage5.Text = "�����������";
            // 
            // panelControl6
            // 
            this.panelControl6.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl6.Appearance.Options.UseBackColor = true;
            this.panelControl6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl6.ContentImage = global::VoteSystem.Properties.Resources.ScreenBackground;
            this.panelControl6.Controls.Add(this.memoEdit19);
            this.panelControl6.Controls.Add(this.labelControl33);
            this.panelControl6.Controls.Add(this.memoEdit18);
            this.panelControl6.Controls.Add(this.labelControl32);
            this.panelControl6.Controls.Add(this.memoEdit17);
            this.panelControl6.Controls.Add(this.labelControl3);
            this.panelControl6.Controls.Add(this.labelControl4);
            this.panelControl6.Controls.Add(this.labelControl34);
            this.panelControl6.Controls.Add(this.labelControl35);
            this.panelControl6.Controls.Add(this.memoEdit20);
            this.panelControl6.Controls.Add(this.labelControl36);
            this.panelControl6.Controls.Add(this.labelControl37);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(0, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(792, 509);
            this.panelControl6.TabIndex = 11;
            // 
            // memoEdit19
            // 
            this.memoEdit19.EditValue = "53";
            this.memoEdit19.Location = new System.Drawing.Point(263, 258);
            this.memoEdit19.Name = "memoEdit19";
            this.memoEdit19.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit19.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit19.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit19.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit19.Properties.Appearance.Options.UseFont = true;
            this.memoEdit19.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit19.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit19.Properties.ReadOnly = true;
            this.memoEdit19.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit19.Size = new System.Drawing.Size(364, 23);
            this.memoEdit19.TabIndex = 46;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl33.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl33.Appearance.Options.UseBackColor = true;
            this.labelControl33.Appearance.Options.UseFont = true;
            this.labelControl33.Appearance.Options.UseForeColor = true;
            this.labelControl33.Location = new System.Drawing.Point(56, 256);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(160, 23);
            this.labelControl33.TabIndex = 45;
            this.labelControl33.Text = "�������������:";
            // 
            // memoEdit18
            // 
            this.memoEdit18.EditValue = "53";
            this.memoEdit18.Location = new System.Drawing.Point(263, 215);
            this.memoEdit18.Name = "memoEdit18";
            this.memoEdit18.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit18.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit18.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit18.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit18.Properties.Appearance.Options.UseFont = true;
            this.memoEdit18.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit18.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit18.Properties.ReadOnly = true;
            this.memoEdit18.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit18.Size = new System.Drawing.Size(364, 23);
            this.memoEdit18.TabIndex = 44;
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl32.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl32.Appearance.Options.UseBackColor = true;
            this.labelControl32.Appearance.Options.UseFont = true;
            this.labelControl32.Appearance.Options.UseForeColor = true;
            this.labelControl32.Location = new System.Drawing.Point(56, 213);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(179, 23);
            this.labelControl32.TabIndex = 43;
            this.labelControl32.Text = "��������� � ����:";
            // 
            // memoEdit17
            // 
            this.memoEdit17.EditValue = "2 ���.";
            this.memoEdit17.Location = new System.Drawing.Point(260, 304);
            this.memoEdit17.Name = "memoEdit17";
            this.memoEdit17.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit17.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit17.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit17.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit17.Properties.Appearance.Options.UseFont = true;
            this.memoEdit17.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit17.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit17.Properties.ReadOnly = true;
            this.memoEdit17.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit17.Size = new System.Drawing.Size(364, 23);
            this.memoEdit17.TabIndex = 42;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(56, 304);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(188, 23);
            this.labelControl3.TabIndex = 41;
            this.labelControl3.Text = "�������� �������:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(238, 52);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(328, 25);
            this.labelControl4.TabIndex = 40;
            this.labelControl4.Text = "��������, ���� �����������!";
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl34.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl34.Appearance.Options.UseBackColor = true;
            this.labelControl34.Appearance.Options.UseFont = true;
            this.labelControl34.Appearance.Options.UseForeColor = true;
            this.labelControl34.Location = new System.Drawing.Point(427, 422);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(55, 23);
            this.labelControl34.TabIndex = 17;
            this.labelControl34.Text = "16:04";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl35.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl35.Appearance.Options.UseBackColor = true;
            this.labelControl35.Appearance.Options.UseFont = true;
            this.labelControl35.Appearance.Options.UseForeColor = true;
            this.labelControl35.Location = new System.Drawing.Point(263, 422);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(136, 23);
            this.labelControl35.TabIndex = 16;
            this.labelControl35.Text = "16 ��� 2010�.";
            // 
            // memoEdit20
            // 
            this.memoEdit20.EditValue = " �������� ���������������� ���������� � ������ ����� ���������� �� 30.11.2006�.";
            this.memoEdit20.Location = new System.Drawing.Point(66, 123);
            this.memoEdit20.Name = "memoEdit20";
            this.memoEdit20.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit20.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit20.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit20.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit20.Properties.Appearance.Options.UseFont = true;
            this.memoEdit20.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit20.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit20.Properties.ReadOnly = true;
            this.memoEdit20.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit20.Size = new System.Drawing.Size(677, 53);
            this.memoEdit20.TabIndex = 15;
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl36.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl36.Appearance.Options.UseBackColor = true;
            this.labelControl36.Appearance.Options.UseFont = true;
            this.labelControl36.Appearance.Options.UseForeColor = true;
            this.labelControl36.Location = new System.Drawing.Point(56, 101);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(78, 23);
            this.labelControl36.TabIndex = 14;
            this.labelControl36.Text = "������:";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl37.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl37.Appearance.Options.UseBackColor = true;
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.Appearance.Options.UseForeColor = true;
            this.labelControl37.Location = new System.Drawing.Point(145, 5);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(479, 23);
            this.labelControl37.TabIndex = 13;
            this.labelControl37.Text = "��������������� ����� ���������� ����������";
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.panelControl7);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(792, 509);
            this.xtraTabPage6.Text = "����� ����������";
            // 
            // panelControl7
            // 
            this.panelControl7.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl7.Appearance.Options.UseBackColor = true;
            this.panelControl7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl7.Controls.Add(this.panelControl8);
            this.panelControl7.Controls.Add(this.memoEdit23);
            this.panelControl7.Controls.Add(this.labelControl40);
            this.panelControl7.Controls.Add(this.memoEdit21);
            this.panelControl7.Controls.Add(this.labelControl38);
            this.panelControl7.Controls.Add(this.memoEdit22);
            this.panelControl7.Controls.Add(this.labelControl39);
            this.panelControl7.Controls.Add(this.labelControl41);
            this.panelControl7.Controls.Add(this.labelControl42);
            this.panelControl7.Controls.Add(this.labelControl43);
            this.panelControl7.Controls.Add(this.memoEdit24);
            this.panelControl7.Controls.Add(this.labelControl44);
            this.panelControl7.Controls.Add(this.labelControl45);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(792, 509);
            this.panelControl7.TabIndex = 12;
            // 
            // panelControl8
            // 
            this.panelControl8.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl8.Appearance.Options.UseBackColor = true;
            this.panelControl8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl8.ContentImage = global::VoteSystem.Properties.Resources.ScreenBackground;
            this.panelControl8.Controls.Add(this.memoEdit34);
            this.panelControl8.Controls.Add(this.memoEdit33);
            this.panelControl8.Controls.Add(this.memoEdit32);
            this.panelControl8.Controls.Add(this.memoEdit31);
            this.panelControl8.Controls.Add(this.labelControl56);
            this.panelControl8.Controls.Add(this.memoEdit30);
            this.panelControl8.Controls.Add(this.labelControl55);
            this.panelControl8.Controls.Add(this.memoEdit29);
            this.panelControl8.Controls.Add(this.labelControl54);
            this.panelControl8.Controls.Add(this.memoEdit25);
            this.panelControl8.Controls.Add(this.labelControl46);
            this.panelControl8.Controls.Add(this.memoEdit26);
            this.panelControl8.Controls.Add(this.labelControl47);
            this.panelControl8.Controls.Add(this.memoEdit27);
            this.panelControl8.Controls.Add(this.labelControl48);
            this.panelControl8.Controls.Add(this.labelControl49);
            this.panelControl8.Controls.Add(this.labelControl50);
            this.panelControl8.Controls.Add(this.labelControl51);
            this.panelControl8.Controls.Add(this.memoEdit28);
            this.panelControl8.Controls.Add(this.labelControl52);
            this.panelControl8.Controls.Add(this.labelControl53);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(2, 2);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(788, 505);
            this.panelControl8.TabIndex = 49;
            // 
            // memoEdit34
            // 
            this.memoEdit34.EditValue = "5 %";
            this.memoEdit34.Location = new System.Drawing.Point(462, 366);
            this.memoEdit34.Name = "memoEdit34";
            this.memoEdit34.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit34.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit34.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit34.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit34.Properties.Appearance.Options.UseFont = true;
            this.memoEdit34.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit34.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit34.Properties.ReadOnly = true;
            this.memoEdit34.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit34.Size = new System.Drawing.Size(49, 23);
            this.memoEdit34.TabIndex = 57;
            // 
            // memoEdit33
            // 
            this.memoEdit33.EditValue = "8 %";
            this.memoEdit33.Location = new System.Drawing.Point(462, 322);
            this.memoEdit33.Name = "memoEdit33";
            this.memoEdit33.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit33.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit33.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit33.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit33.Properties.Appearance.Options.UseFont = true;
            this.memoEdit33.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit33.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit33.Properties.ReadOnly = true;
            this.memoEdit33.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit33.Size = new System.Drawing.Size(49, 23);
            this.memoEdit33.TabIndex = 56;
            // 
            // memoEdit32
            // 
            this.memoEdit32.EditValue = "34 %";
            this.memoEdit32.Location = new System.Drawing.Point(462, 283);
            this.memoEdit32.Name = "memoEdit32";
            this.memoEdit32.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit32.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit32.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit32.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit32.Properties.Appearance.Options.UseFont = true;
            this.memoEdit32.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit32.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit32.Properties.ReadOnly = true;
            this.memoEdit32.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit32.Size = new System.Drawing.Size(49, 23);
            this.memoEdit32.TabIndex = 55;
            // 
            // memoEdit31
            // 
            this.memoEdit31.EditValue = "56 %";
            this.memoEdit31.Location = new System.Drawing.Point(462, 242);
            this.memoEdit31.Name = "memoEdit31";
            this.memoEdit31.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit31.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit31.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit31.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit31.Properties.Appearance.Options.UseFont = true;
            this.memoEdit31.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit31.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit31.Properties.ReadOnly = true;
            this.memoEdit31.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit31.Size = new System.Drawing.Size(49, 23);
            this.memoEdit31.TabIndex = 54;
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl56.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl56.Appearance.Options.UseBackColor = true;
            this.labelControl56.Appearance.Options.UseFont = true;
            this.labelControl56.Appearance.Options.UseForeColor = true;
            this.labelControl56.Location = new System.Drawing.Point(437, 189);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(91, 23);
            this.labelControl56.TabIndex = 53;
            this.labelControl56.Text = "�������:";
            // 
            // memoEdit30
            // 
            this.memoEdit30.EditValue = "7";
            this.memoEdit30.Location = new System.Drawing.Point(259, 324);
            this.memoEdit30.Name = "memoEdit30";
            this.memoEdit30.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit30.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit30.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit30.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit30.Properties.Appearance.Options.UseFont = true;
            this.memoEdit30.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit30.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit30.Properties.ReadOnly = true;
            this.memoEdit30.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit30.Size = new System.Drawing.Size(364, 23);
            this.memoEdit30.TabIndex = 52;
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl55.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl55.Appearance.Options.UseBackColor = true;
            this.labelControl55.Appearance.Options.UseFont = true;
            this.labelControl55.Appearance.Options.UseForeColor = true;
            this.labelControl55.Location = new System.Drawing.Point(52, 322);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(150, 23);
            this.labelControl55.TabIndex = 51;
            this.labelControl55.Text = "������������:";
            // 
            // memoEdit29
            // 
            this.memoEdit29.EditValue = "17";
            this.memoEdit29.Location = new System.Drawing.Point(261, 283);
            this.memoEdit29.Name = "memoEdit29";
            this.memoEdit29.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit29.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit29.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit29.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit29.Properties.Appearance.Options.UseFont = true;
            this.memoEdit29.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit29.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit29.Properties.ReadOnly = true;
            this.memoEdit29.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit29.Size = new System.Drawing.Size(364, 23);
            this.memoEdit29.TabIndex = 50;
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl54.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl54.Appearance.Options.UseBackColor = true;
            this.labelControl54.Appearance.Options.UseFont = true;
            this.labelControl54.Appearance.Options.UseForeColor = true;
            this.labelControl54.Location = new System.Drawing.Point(54, 281);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(80, 23);
            this.labelControl54.TabIndex = 49;
            this.labelControl54.Text = "������:";
            // 
            // memoEdit25
            // 
            this.memoEdit25.EditValue = "28";
            this.memoEdit25.Location = new System.Drawing.Point(263, 244);
            this.memoEdit25.Name = "memoEdit25";
            this.memoEdit25.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit25.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit25.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit25.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit25.Properties.Appearance.Options.UseFont = true;
            this.memoEdit25.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit25.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit25.Properties.ReadOnly = true;
            this.memoEdit25.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit25.Size = new System.Drawing.Size(364, 23);
            this.memoEdit25.TabIndex = 48;
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl46.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl46.Appearance.Options.UseBackColor = true;
            this.labelControl46.Appearance.Options.UseFont = true;
            this.labelControl46.Appearance.Options.UseForeColor = true;
            this.labelControl46.Location = new System.Drawing.Point(56, 242);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(30, 23);
            this.labelControl46.TabIndex = 47;
            this.labelControl46.Text = "��:";
            // 
            // memoEdit26
            // 
            this.memoEdit26.EditValue = "4";
            this.memoEdit26.Location = new System.Drawing.Point(259, 368);
            this.memoEdit26.Name = "memoEdit26";
            this.memoEdit26.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit26.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit26.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit26.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit26.Properties.Appearance.Options.UseFont = true;
            this.memoEdit26.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit26.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit26.Properties.ReadOnly = true;
            this.memoEdit26.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit26.Size = new System.Drawing.Size(364, 23);
            this.memoEdit26.TabIndex = 46;
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl47.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl47.Appearance.Options.UseBackColor = true;
            this.labelControl47.Appearance.Options.UseFont = true;
            this.labelControl47.Appearance.Options.UseForeColor = true;
            this.labelControl47.Location = new System.Drawing.Point(52, 366);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(146, 23);
            this.labelControl47.TabIndex = 45;
            this.labelControl47.Tag = "";
            this.labelControl47.Text = "�� ����������";
            // 
            // memoEdit27
            // 
            this.memoEdit27.EditValue = "53";
            this.memoEdit27.Location = new System.Drawing.Point(261, 189);
            this.memoEdit27.Name = "memoEdit27";
            this.memoEdit27.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit27.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit27.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit27.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit27.Properties.Appearance.Options.UseFont = true;
            this.memoEdit27.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit27.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit27.Properties.ReadOnly = true;
            this.memoEdit27.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit27.Size = new System.Drawing.Size(364, 23);
            this.memoEdit27.TabIndex = 44;
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl48.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl48.Appearance.Options.UseBackColor = true;
            this.labelControl48.Appearance.Options.UseFont = true;
            this.labelControl48.Appearance.Options.UseForeColor = true;
            this.labelControl48.Location = new System.Drawing.Point(54, 187);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(179, 23);
            this.labelControl48.TabIndex = 43;
            this.labelControl48.Text = "��������� � ����:";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl49.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl49.Appearance.Options.UseBackColor = true;
            this.labelControl49.Appearance.Options.UseFont = true;
            this.labelControl49.Appearance.Options.UseForeColor = true;
            this.labelControl49.Location = new System.Drawing.Point(238, 48);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(275, 25);
            this.labelControl49.TabIndex = 40;
            this.labelControl49.Text = "���������� �����������";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl50.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl50.Appearance.Options.UseBackColor = true;
            this.labelControl50.Appearance.Options.UseFont = true;
            this.labelControl50.Appearance.Options.UseForeColor = true;
            this.labelControl50.Location = new System.Drawing.Point(427, 422);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(55, 23);
            this.labelControl50.TabIndex = 17;
            this.labelControl50.Text = "16:04";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl51.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl51.Appearance.Options.UseBackColor = true;
            this.labelControl51.Appearance.Options.UseFont = true;
            this.labelControl51.Appearance.Options.UseForeColor = true;
            this.labelControl51.Location = new System.Drawing.Point(263, 422);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(136, 23);
            this.labelControl51.TabIndex = 16;
            this.labelControl51.Text = "16 ��� 2010�.";
            // 
            // memoEdit28
            // 
            this.memoEdit28.EditValue = " �������� ���������������� ���������� � ������ ����� ���������� �� 30.11.2006�.";
            this.memoEdit28.Location = new System.Drawing.Point(66, 108);
            this.memoEdit28.Name = "memoEdit28";
            this.memoEdit28.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit28.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit28.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit28.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit28.Properties.Appearance.Options.UseFont = true;
            this.memoEdit28.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit28.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit28.Properties.ReadOnly = true;
            this.memoEdit28.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit28.Size = new System.Drawing.Size(677, 53);
            this.memoEdit28.TabIndex = 15;
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl52.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl52.Appearance.Options.UseBackColor = true;
            this.labelControl52.Appearance.Options.UseFont = true;
            this.labelControl52.Appearance.Options.UseForeColor = true;
            this.labelControl52.Location = new System.Drawing.Point(56, 86);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(78, 23);
            this.labelControl52.TabIndex = 14;
            this.labelControl52.Text = "������:";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl53.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl53.Appearance.Options.UseBackColor = true;
            this.labelControl53.Appearance.Options.UseFont = true;
            this.labelControl53.Appearance.Options.UseForeColor = true;
            this.labelControl53.Location = new System.Drawing.Point(145, 5);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(479, 23);
            this.labelControl53.TabIndex = 13;
            this.labelControl53.Text = "��������������� ����� ���������� ����������";
            // 
            // memoEdit23
            // 
            this.memoEdit23.EditValue = "53";
            this.memoEdit23.Location = new System.Drawing.Point(263, 311);
            this.memoEdit23.Name = "memoEdit23";
            this.memoEdit23.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit23.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit23.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit23.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit23.Properties.Appearance.Options.UseFont = true;
            this.memoEdit23.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit23.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit23.Properties.ReadOnly = true;
            this.memoEdit23.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit23.Size = new System.Drawing.Size(364, 23);
            this.memoEdit23.TabIndex = 48;
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl40.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl40.Appearance.Options.UseBackColor = true;
            this.labelControl40.Appearance.Options.UseFont = true;
            this.labelControl40.Appearance.Options.UseForeColor = true;
            this.labelControl40.Location = new System.Drawing.Point(56, 309);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(30, 23);
            this.labelControl40.TabIndex = 47;
            this.labelControl40.Text = "��:";
            // 
            // memoEdit21
            // 
            this.memoEdit21.EditValue = "53";
            this.memoEdit21.Location = new System.Drawing.Point(263, 258);
            this.memoEdit21.Name = "memoEdit21";
            this.memoEdit21.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit21.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit21.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit21.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit21.Properties.Appearance.Options.UseFont = true;
            this.memoEdit21.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit21.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit21.Properties.ReadOnly = true;
            this.memoEdit21.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit21.Size = new System.Drawing.Size(364, 23);
            this.memoEdit21.TabIndex = 46;
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl38.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl38.Appearance.Options.UseBackColor = true;
            this.labelControl38.Appearance.Options.UseFont = true;
            this.labelControl38.Appearance.Options.UseForeColor = true;
            this.labelControl38.Location = new System.Drawing.Point(56, 256);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(160, 23);
            this.labelControl38.TabIndex = 45;
            this.labelControl38.Text = "�������������:";
            // 
            // memoEdit22
            // 
            this.memoEdit22.EditValue = "53";
            this.memoEdit22.Location = new System.Drawing.Point(263, 215);
            this.memoEdit22.Name = "memoEdit22";
            this.memoEdit22.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit22.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit22.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit22.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit22.Properties.Appearance.Options.UseFont = true;
            this.memoEdit22.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit22.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit22.Properties.ReadOnly = true;
            this.memoEdit22.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit22.Size = new System.Drawing.Size(364, 23);
            this.memoEdit22.TabIndex = 44;
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl39.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl39.Appearance.Options.UseBackColor = true;
            this.labelControl39.Appearance.Options.UseFont = true;
            this.labelControl39.Appearance.Options.UseForeColor = true;
            this.labelControl39.Location = new System.Drawing.Point(56, 213);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(179, 23);
            this.labelControl39.TabIndex = 43;
            this.labelControl39.Text = "��������� � ����:";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl41.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl41.Appearance.Options.UseBackColor = true;
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Appearance.Options.UseForeColor = true;
            this.labelControl41.Location = new System.Drawing.Point(238, 52);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(275, 25);
            this.labelControl41.TabIndex = 40;
            this.labelControl41.Text = "���������� �����������";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl42.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl42.Appearance.Options.UseBackColor = true;
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.Appearance.Options.UseForeColor = true;
            this.labelControl42.Location = new System.Drawing.Point(427, 422);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(55, 23);
            this.labelControl42.TabIndex = 17;
            this.labelControl42.Text = "16:04";
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl43.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl43.Appearance.Options.UseBackColor = true;
            this.labelControl43.Appearance.Options.UseFont = true;
            this.labelControl43.Appearance.Options.UseForeColor = true;
            this.labelControl43.Location = new System.Drawing.Point(263, 422);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(136, 23);
            this.labelControl43.TabIndex = 16;
            this.labelControl43.Text = "16 ��� 2010�.";
            // 
            // memoEdit24
            // 
            this.memoEdit24.EditValue = " �������� ���������������� ���������� � ������ ����� ���������� �� 30.11.2006�.";
            this.memoEdit24.Location = new System.Drawing.Point(66, 123);
            this.memoEdit24.Name = "memoEdit24";
            this.memoEdit24.Properties.Appearance.BackColor = System.Drawing.Color.Navy;
            this.memoEdit24.Properties.Appearance.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memoEdit24.Properties.Appearance.ForeColor = System.Drawing.Color.Gold;
            this.memoEdit24.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit24.Properties.Appearance.Options.UseFont = true;
            this.memoEdit24.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit24.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit24.Properties.ReadOnly = true;
            this.memoEdit24.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit24.Size = new System.Drawing.Size(677, 53);
            this.memoEdit24.TabIndex = 15;
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl44.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl44.Appearance.Options.UseBackColor = true;
            this.labelControl44.Appearance.Options.UseFont = true;
            this.labelControl44.Appearance.Options.UseForeColor = true;
            this.labelControl44.Location = new System.Drawing.Point(56, 101);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(78, 23);
            this.labelControl44.TabIndex = 14;
            this.labelControl44.Text = "������:";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.BackColor = System.Drawing.Color.Navy;
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl45.Appearance.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelControl45.Appearance.Options.UseBackColor = true;
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.Appearance.Options.UseForeColor = true;
            this.labelControl45.Location = new System.Drawing.Point(145, 5);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(479, 23);
            this.labelControl45.TabIndex = 13;
            this.labelControl45.Text = "��������������� ����� ���������� ����������";
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.btnClose);
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 485);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(801, 55);
            this.panelControl2.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.ImageIndex = 1;
            this.btnClose.ImageList = this.ButtonImages;
            this.btnClose.Location = new System.Drawing.Point(649, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(145, 43);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "�������";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton1.ImageIndex = 0;
            this.simpleButton1.ImageList = this.ButtonImages;
            this.simpleButton1.Location = new System.Drawing.Point(6, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(162, 43);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "������� �� ��������";
            this.simpleButton1.Visible = false;
            // 
            // tmrForm
            // 
            this.tmrForm.Interval = 5000;
            this.tmrForm.Tick += new System.EventHandler(this.tmrForm_Tick);
            // 
            // MonitorPreviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 540);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.xtraTabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MonitorPreviewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������������� �������� ��������� � ����";
            this.Load += new System.EventHandler(this.MonitorPreviewForm_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MonitorPreviewForm_MouseClick);
            this.Shown += new System.EventHandler(this.MonitorPreviewForm_Shown);
            this.VisibleChanged += new System.EventHandler(this.MonitorPreviewForm_VisibleChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MonitorPreviewForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit8.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit14.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit15.Properties)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit20.Properties)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.MemoEdit memoEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.MemoEdit memoEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.MemoEdit memoEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.MemoEdit memoEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.MemoEdit memoEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.MemoEdit memoEdit12;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.MemoEdit memoEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.MemoEdit memoEdit14;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.MemoEdit memoEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.MemoEdit memoEdit11;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.MemoEdit memoEdit15;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.MemoEdit memoEdit16;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit memoEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.MemoEdit memoEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.MemoEdit memoEdit17;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.MemoEdit memoEdit20;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.MemoEdit memoEdit19;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.MemoEdit memoEdit18;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEdit21;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.MemoEdit memoEdit22;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.MemoEdit memoEdit24;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.MemoEdit memoEdit30;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.MemoEdit memoEdit29;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.MemoEdit memoEdit25;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.MemoEdit memoEdit26;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.MemoEdit memoEdit27;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.MemoEdit memoEdit28;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.MemoEdit memoEdit23;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.MemoEdit memoEdit34;
        private DevExpress.XtraEditors.MemoEdit memoEdit33;
        private DevExpress.XtraEditors.MemoEdit memoEdit32;
        private DevExpress.XtraEditors.MemoEdit memoEdit31;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private System.Windows.Forms.Timer tmrForm;
    }
}