namespace VoteSystem
{
    partial class DelegatesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DelegatesForm));
            this.xpDelegates = new DevExpress.Xpo.XPCollection();
            this.session1 = new DevExpress.Xpo.Session();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnSelect = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.btnChange = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFraction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnPropCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnPropApply = new DevExpress.XtraEditors.SimpleButton();
            this.propertyDescriptionControl1 = new DevExpress.XtraVerticalGrid.PropertyDescriptionControl();
            this.PropertiesControl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.catFIO = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowLastName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFirstName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSecondName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catRegion = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowRegionName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRegionNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catInfo = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowFraction = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowidParty_Name = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catSeat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMicNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRowNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSeatNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.xpParties = new DevExpress.Xpo.XPCollection();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem2 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem3 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.xpFractions = new DevExpress.Xpo.XPCollection();
            this.xpRegions = new DevExpress.Xpo.XPCollection();
            this.xpSeats = new DevExpress.Xpo.XPCollection();
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpParties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpFractions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpRegions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpSeats)).BeginInit();
            this.SuspendLayout();
            // 
            // xpDelegates
            // 
            this.xpDelegates.DeleteObjectOnRemove = true;
            this.xpDelegates.ObjectType = typeof(VoteSystem.DelegateObj);
            this.xpDelegates.Sorting.AddRange(new DevExpress.Xpo.SortProperty[] {
            new DevExpress.Xpo.SortProperty("id", DevExpress.Xpo.DB.SortingDirection.Ascending)});
            this.xpDelegates.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.xpCollection1_ListChanged);
            this.xpDelegates.DisplayableProperties = "id;LastName;FirstName;SecondName;RegionName;RegionNum;Fraction;idParty.Name;FullN" +
                "ame;idFraction.Name;idRegion.Name;idRegion.Number;idSeat.MicNum";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 53);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Top;
            this.splitContainerControl1.Panel2.Controls.Add(this.btnPropCancel);
            this.splitContainerControl1.Panel2.Controls.Add(this.btnPropApply);
            this.splitContainerControl1.Panel2.Controls.Add(this.propertyDescriptionControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.PropertiesControl);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "����������";
            this.splitContainerControl1.Size = new System.Drawing.Size(1242, 598);
            this.splitContainerControl1.SplitterPosition = 888;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnSelect);
            this.panelControl1.Controls.Add(this.dataNavigator1);
            this.panelControl1.Controls.Add(this.btnChange);
            this.panelControl1.Controls.Add(this.btnRemove);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 539);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(884, 55);
            this.panelControl1.TabIndex = 10;
            // 
            // btnSelect
            // 
            this.btnSelect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSelect.ImageIndex = 13;
            this.btnSelect.ImageList = this.ButtonImages;
            this.btnSelect.Location = new System.Drawing.Point(630, 9);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(107, 34);
            this.btnSelect.TabIndex = 12;
            this.btnSelect.Text = "�������";
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.DataSource = this.xpDelegates;
            this.dataNavigator1.Location = new System.Drawing.Point(354, 19);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(192, 24);
            this.dataNavigator1.TabIndex = 11;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.Visible = false;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            // 
            // btnChange
            // 
            this.btnChange.ImageIndex = 14;
            this.btnChange.ImageList = this.ButtonImages;
            this.btnChange.Location = new System.Drawing.Point(252, 9);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(96, 34);
            this.btnChange.TabIndex = 11;
            this.btnChange.Text = "��������";
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.ImageIndex = 2;
            this.btnRemove.ImageList = this.ButtonImages;
            this.btnRemove.Location = new System.Drawing.Point(138, 9);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(96, 34);
            this.btnRemove.TabIndex = 10;
            this.btnRemove.Text = "�������";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ImageIndex = 3;
            this.btnAdd.ImageList = this.ButtonImages;
            this.btnAdd.Location = new System.Drawing.Point(24, 9);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(96, 34);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "��������";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ImageIndex = 12;
            this.btnClose.ImageList = this.ButtonImages;
            this.btnClose.Location = new System.Drawing.Point(757, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(107, 34);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "�������";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.xpDelegates;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridControl1.EmbeddedNavigator.Name = "";
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(884, 536);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick_1);
            this.gridControl1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gridControl1_KeyPress);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFullName,
            this.colRegionName,
            this.colRegionNum,
            this.colFraction,
            this.colPartyName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView1.OptionsBehavior.Editable = false;
            // 
            // colFullName
            // 
            this.colFullName.Caption = "���";
            this.colFullName.FieldName = "FullName";
            this.colFullName.MinWidth = 175;
            this.colFullName.Name = "colFullName";
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 0;
            this.colFullName.Width = 200;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "�������� �������";
            this.colRegionName.FieldName = "idRegion.Name";
            this.colRegionName.MinWidth = 100;
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 1;
            this.colRegionName.Width = 171;
            // 
            // colRegionNum
            // 
            this.colRegionNum.Caption = "����� �������";
            this.colRegionNum.FieldName = "idRegion.Number";
            this.colRegionNum.MinWidth = 50;
            this.colRegionNum.Name = "colRegionNum";
            this.colRegionNum.OptionsColumn.ReadOnly = true;
            this.colRegionNum.Visible = true;
            this.colRegionNum.VisibleIndex = 2;
            this.colRegionNum.Width = 167;
            // 
            // colFraction
            // 
            this.colFraction.Caption = "�������";
            this.colFraction.FieldName = "idFraction.Name";
            this.colFraction.MinWidth = 100;
            this.colFraction.Name = "colFraction";
            this.colFraction.OptionsColumn.ReadOnly = true;
            this.colFraction.Visible = true;
            this.colFraction.VisibleIndex = 3;
            this.colFraction.Width = 133;
            // 
            // colPartyName
            // 
            this.colPartyName.Caption = "������";
            this.colPartyName.FieldName = "idParty.Name";
            this.colPartyName.MinWidth = 130;
            this.colPartyName.Name = "colPartyName";
            this.colPartyName.OptionsColumn.ReadOnly = true;
            this.colPartyName.Visible = true;
            this.colPartyName.VisibleIndex = 4;
            this.colPartyName.Width = 180;
            // 
            // btnPropCancel
            // 
            this.btnPropCancel.ImageIndex = 1;
            this.btnPropCancel.ImageList = this.ButtonImages;
            this.btnPropCancel.Location = new System.Drawing.Point(230, 314);
            this.btnPropCancel.Name = "btnPropCancel";
            this.btnPropCancel.Size = new System.Drawing.Size(104, 30);
            this.btnPropCancel.TabIndex = 3;
            this.btnPropCancel.Text = "�������� ";
            this.btnPropCancel.Click += new System.EventHandler(this.btnPropCancel_Click);
            // 
            // btnPropApply
            // 
            this.btnPropApply.ImageIndex = 13;
            this.btnPropApply.ImageList = this.ButtonImages;
            this.btnPropApply.Location = new System.Drawing.Point(5, 314);
            this.btnPropApply.Name = "btnPropApply";
            this.btnPropApply.Size = new System.Drawing.Size(164, 30);
            this.btnPropApply.TabIndex = 2;
            this.btnPropApply.Text = "��������� ���������";
            this.btnPropApply.Click += new System.EventHandler(this.btnPropApply_Click);
            // 
            // propertyDescriptionControl1
            // 
            this.propertyDescriptionControl1.Location = new System.Drawing.Point(5, 372);
            this.propertyDescriptionControl1.Name = "propertyDescriptionControl1";
            this.propertyDescriptionControl1.Size = new System.Drawing.Size(329, 147);
            this.propertyDescriptionControl1.TabIndex = 1;
            this.propertyDescriptionControl1.TabStop = false;
            // 
            // PropertiesControl
            // 
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControl.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControl.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControl.Location = new System.Drawing.Point(5, 5);
            this.PropertiesControl.Name = "PropertiesControl";
            this.PropertiesControl.RecordWidth = 137;
            this.PropertiesControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4});
            this.PropertiesControl.RowHeaderWidth = 63;
            this.PropertiesControl.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catFIO,
            this.catRegion,
            this.catInfo,
            this.catSeat});
            this.PropertiesControl.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControl.Size = new System.Drawing.Size(329, 300);
            this.PropertiesControl.TabIndex = 0;
            this.PropertiesControl.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.PropertiesControl_CellValueChanged);
            this.PropertiesControl.CellValueChanging += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.PropertiesControl_CellValueChanging);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ImmediatePopup = true;
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox2.ImmediatePopup = true;
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.Sorted = true;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.ImmediatePopup = true;
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // catFIO
            // 
            this.catFIO.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowLastName,
            this.rowFirstName,
            this.rowSecondName});
            this.catFIO.Name = "catFIO";
            this.catFIO.Properties.Caption = "���";
            // 
            // rowLastName
            // 
            this.rowLastName.Height = 27;
            this.rowLastName.Name = "rowLastName";
            this.rowLastName.Properties.Caption = "�������";
            this.rowLastName.Properties.FieldName = "LastName";
            this.rowLastName.Properties.ReadOnly = true;
            this.rowLastName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowLastName.Properties.Value = "1111";
            // 
            // rowFirstName
            // 
            this.rowFirstName.Height = 20;
            this.rowFirstName.Name = "rowFirstName";
            this.rowFirstName.Properties.Caption = "���";
            this.rowFirstName.Properties.FieldName = "FirstName";
            this.rowFirstName.Properties.ReadOnly = true;
            this.rowFirstName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowSecondName
            // 
            this.rowSecondName.Height = 20;
            this.rowSecondName.Name = "rowSecondName";
            this.rowSecondName.Properties.Caption = "��������";
            this.rowSecondName.Properties.FieldName = "SecondName";
            this.rowSecondName.Properties.ReadOnly = true;
            // 
            // catRegion
            // 
            this.catRegion.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowRegionName,
            this.rowRegionNum});
            this.catRegion.Name = "catRegion";
            this.catRegion.Properties.Caption = "������";
            // 
            // rowRegionName
            // 
            this.rowRegionName.Height = 20;
            this.rowRegionName.Name = "rowRegionName";
            this.rowRegionName.Properties.Caption = "������";
            this.rowRegionName.Properties.FieldName = "RegionName";
            this.rowRegionName.Properties.ReadOnly = true;
            this.rowRegionName.Properties.RowEdit = this.repositoryItemComboBox2;
            this.rowRegionName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowRegionNum
            // 
            this.rowRegionNum.Height = 20;
            this.rowRegionNum.Name = "rowRegionNum";
            this.rowRegionNum.Properties.Caption = "� �������";
            this.rowRegionNum.Properties.FieldName = "RegionNum";
            this.rowRegionNum.Properties.ReadOnly = true;
            this.rowRegionNum.Properties.RowEdit = this.repositoryItemComboBox3;
            this.rowRegionNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // catInfo
            // 
            this.catInfo.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowFraction,
            this.rowidParty_Name});
            this.catInfo.Name = "catInfo";
            this.catInfo.Properties.Caption = "�������������� ����������";
            // 
            // rowFraction
            // 
            this.rowFraction.Height = 25;
            this.rowFraction.Name = "rowFraction";
            this.rowFraction.Properties.Caption = "�������";
            this.rowFraction.Properties.FieldName = "Fraction";
            this.rowFraction.Properties.ReadOnly = true;
            this.rowFraction.Properties.RowEdit = this.repositoryItemComboBox4;
            this.rowFraction.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowidParty_Name
            // 
            this.rowidParty_Name.Height = 23;
            this.rowidParty_Name.Name = "rowidParty_Name";
            this.rowidParty_Name.Properties.Caption = "������";
            this.rowidParty_Name.Properties.FieldName = "idParty.Name";
            this.rowidParty_Name.Properties.ReadOnly = true;
            this.rowidParty_Name.Properties.RowEdit = this.repositoryItemComboBox1;
            this.rowidParty_Name.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowidParty_Name.Properties.Value = "������ 1";
            // 
            // catSeat
            // 
            this.catSeat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMicNum,
            this.rowRowNum,
            this.rowSeatNum});
            this.catSeat.Name = "catSeat";
            this.catSeat.Properties.Caption = "���������� � ����";
            // 
            // rowMicNum
            // 
            this.rowMicNum.Name = "rowMicNum";
            this.rowMicNum.Properties.Caption = "��������";
            this.rowMicNum.Properties.ReadOnly = true;
            this.rowMicNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // rowRowNum
            // 
            this.rowRowNum.Name = "rowRowNum";
            this.rowRowNum.Properties.Caption = "���";
            this.rowRowNum.Properties.ReadOnly = true;
            this.rowRowNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // rowSeatNum
            // 
            this.rowSeatNum.Name = "rowSeatNum";
            this.rowSeatNum.Properties.Caption = "�����";
            this.rowSeatNum.Properties.ReadOnly = true;
            this.rowSeatNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // xpParties
            // 
            this.xpParties.CriteriaString = "[id] <> 0";
            this.xpParties.ObjectType = typeof(VoteSystem.PartyObj);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.ButtonImages;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barLargeButtonItem1,
            this.barLargeButtonItem2,
            this.barLargeButtonItem3});
            this.barManager1.LargeImages = this.ButtonImages;
            this.barManager1.MaxItemId = 4;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barLargeButtonItem1, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem3)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "  �������  ";
            this.barLargeButtonItem1.Id = 0;
            this.barLargeButtonItem1.LargeImageIndex = 9;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.OwnFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barLargeButtonItem1.UseOwnFont = true;
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // barLargeButtonItem2
            // 
            this.barLargeButtonItem2.Caption = "  ������  ";
            this.barLargeButtonItem2.Id = 2;
            this.barLargeButtonItem2.LargeImageIndex = 10;
            this.barLargeButtonItem2.Name = "barLargeButtonItem2";
            this.barLargeButtonItem2.OwnFont = new System.Drawing.Font("Tahoma", 9.75F);
            this.barLargeButtonItem2.UseOwnFont = true;
            this.barLargeButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem2_ItemClick);
            // 
            // barLargeButtonItem3
            // 
            this.barLargeButtonItem3.Caption = "  �������  ";
            this.barLargeButtonItem3.Id = 3;
            this.barLargeButtonItem3.LargeImageIndex = 11;
            this.barLargeButtonItem3.Name = "barLargeButtonItem3";
            this.barLargeButtonItem3.OwnFont = new System.Drawing.Font("Tahoma", 9.75F);
            this.barLargeButtonItem3.UseOwnFont = true;
            this.barLargeButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem3_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // xpFractions
            // 
            this.xpFractions.CriteriaString = "[id] <> 0";
            this.xpFractions.ObjectType = typeof(VoteSystem.FractionObj);
            // 
            // xpRegions
            // 
            this.xpRegions.CriteriaString = "[id] <> 0";
            this.xpRegions.ObjectType = typeof(VoteSystem.RegionObj);
            // 
            // xpSeats
            // 
            this.xpSeats.DeleteObjectOnRemove = true;
            this.xpSeats.ObjectType = typeof(VoteSystem.SeatObj);
            // 
            // DelegatesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 673);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DelegatesForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "���� ������ ���������";
            this.Load += new System.EventHandler(this.DelegatesForm_Load);
            this.Shown += new System.EventHandler(this.DelegatesForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpParties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpFractions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpRegions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpSeats)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Xpo.Session session1;
        private DevExpress.Xpo.XPCollection xpDelegates;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControl;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLastName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFirstName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSecondName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFraction;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowidParty_Name;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catFIO;
        private DevExpress.XtraVerticalGrid.PropertyDescriptionControl propertyDescriptionControl1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catRegion;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catInfo;
        private DevExpress.XtraEditors.SimpleButton btnPropApply;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraEditors.SimpleButton btnPropCancel;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        public DevExpress.Xpo.XPCollection xpParties;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btnChange;
        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNum;
        private DevExpress.XtraGrid.Columns.GridColumn colFraction;
        private DevExpress.XtraGrid.Columns.GridColumn colPartyName;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem2;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        public DevExpress.Xpo.XPCollection xpFractions;
        public DevExpress.Xpo.XPCollection xpRegions;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.SimpleButton btnSelect;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catSeat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMicNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRowNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSeatNum;
        private DevExpress.Xpo.XPCollection xpSeats;
    }
}