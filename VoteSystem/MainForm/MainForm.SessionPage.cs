﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    /* интерфейс для управления таб-страничкой "Сессия" на главном окне
     * 
     */ 
    public interface ISessionPage
    {
        IMsgMethods GetShowInterface();

        void Init();
        void InitControls(SessionObj sessionObj, SettingsObj settingsObj, bool IsStatementApp);
        DateTime GetFinishdate();

        void AddHistoryLine(string msg);
    }

    partial class MainForm : ISessionPage
    {
        SessionCntrler _Controller_S;
        SessionObj _viewSessionObj;
        SettingsObj _viewSettingsObj;

        #region реализация интерфейса ISessionPage
        IMsgMethods ISessionPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        void ISessionPage.Init()
        {
            _Controller_S = (SessionCntrler)_Controller.GetSubController("Session Page Controller");
        }

        void ISessionPage.AddHistoryLine(string msg)
        {
            lbHistory.Items.Add(msg);
        }

        // установить начальное значение всех контролов на страничке
        public void InitControls(SessionObj sessionObj, SettingsObj settingsObj, bool IsStatementApp)
        {
            if (sessionObj == null)
                return;

            _viewSessionObj = sessionObj;
            _viewSettingsObj = settingsObj;

            txtCode.DataBindings.Clear();
            txtCaption.DataBindings.Clear();
            txtStartDate.DataBindings.Clear();
//          txtFinishDate.DataBindings.Clear();
//          txtHall.DataBindings.Clear();

            if (txtCode.DataBindings.Count == 0)
                txtCode.DataBindings.Add(new Binding("Text", _viewSessionObj, "Code"));

            if (txtCaption.DataBindings.Count == 0)
                txtCaption.DataBindings.Add(new Binding("Text", _viewSessionObj, "Caption"));

            try
            {
                richContent.Rtf = _viewSessionObj.Content;
            }
            catch
            {
            }
/*
            if (richContent.DataBindings.Count == 0)
                richContent.DataBindings.Add(new Binding("Text", _viewSessionObj, "Content"));
*/
            if (txtStartDate.DataBindings.Count == 0)
                txtStartDate.DataBindings.Add(new Binding("DateTime", _viewSessionObj, "StartDate"));

            if (txtRegTime.DataBindings.Count == 0)
                txtRegTime.DataBindings.Add(new Binding("Text", _viewSettingsObj, "RegTime"));

            if (txtVoteTime.DataBindings.Count == 0)
                txtVoteTime.DataBindings.Add(new Binding("Text", _viewSettingsObj, "VoteTime"));


            if (sessionObj.IsFinished == false)
                txtFinishDate.Text = "";
            else
                txtFinishDate.DateTime = sessionObj.FinishDate;

/*
  
            if (txtQourum.DataBindings.Count == 0)
                txtQourum.DataBindings.Add(new Binding("Text", sessionObj, "Quorum"));
*/
/*
            if (txtHall.DataBindings.Count == 0)
                txtHall.DataBindings.Add(new Binding("Text", _viewSessionObj, "HallName"));
*/

 
/*
            if (txtRegTime.DataBindings.Count == 0)
                txtRegTime.DataBindings.Add(new Binding("Text", sessionObj, "RegTime"));

            if (txtVoteTime.DataBindings.Count == 0)
                txtVoteTime.DataBindings.Add(new Binding("Text", sessionObj, "VoteTime"));
 */

//          if (sessionObj.IsFinished == false)
            {
                txtCode.Properties.ReadOnly = sessionObj.IsFinished;
                txtCaption.Properties.ReadOnly = sessionObj.IsFinished;
                txtDelegateQnty.Properties.ReadOnly = sessionObj.IsFinished;
                txtFinishDate.Properties.ReadOnly = sessionObj.IsFinished;
//              txtHall.Properties.ReadOnly = sessionObj.IsFinished;
                txtQuorumQnty.Properties.ReadOnly = sessionObj.IsFinished;
                txtRegQnty.Properties.ReadOnly = sessionObj.IsFinished;
                txtRegTime.Properties.ReadOnly = sessionObj.IsFinished;
                txtStartDate.Properties.ReadOnly = sessionObj.IsFinished;
                txtVoteTime.Properties.ReadOnly = sessionObj.IsFinished;

            }

            if (IsStatementApp)
            {
                txtCode.Properties.ReadOnly = true;
//              txtHall.Properties.ReadOnly = true;
                txtCaption.Properties.ReadOnly = true;
                txtStartDate.Properties.ReadOnly = true;
                txtFinishDate.Properties.ReadOnly = true;
                txtRegTime.Properties.ReadOnly = true;
                txtVoteTime.Properties.ReadOnly = true;
            }

        }

        DateTime ISessionPage.GetFinishdate()
        {
            return txtFinishDate.DateTime;
        }

        #endregion

        // событие - переход на другую страничку
        private void pgSession_Leave(object sender, EventArgs e)
        {
            _viewSessionObj.Content = richContent.Rtf;
            _Controller_S.SaveSession();
        }

        // событие - нажата кнопка "окончить сессию"
        private void btnSessionFinish_Click(object sender, EventArgs e)
        {
            DialogResult dr = _messages.ShowQuestion("Завершить текущую сессию?");
            if (dr == DialogResult.Yes)
                _Controller_S.SessionFinish();
        }

        // событие - нажата кнопка "новая сессия"
        private void btnSessionNew_Click(object sender, EventArgs e)
        {
            DialogResult dr = _messages.ShowQuestion("Завершить текущую сессию и начать новую?");
            if (dr == DialogResult.Yes)
                _Controller_S.SessionNew();
        }
   }
}