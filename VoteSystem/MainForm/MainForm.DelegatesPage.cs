﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.Xpo;

namespace VoteSystem
{
    public interface IDelegatesPage
    {
        IMsgMethods GetShowInterface();
        void Init();
        void InitControls(ref XPCollection<SesDelegateObj> SesDelegates, bool IsStatementApp);
        int NavigatorPosition { get; set; }
        void SetPropertySheet(Dictionary<string, string> properties);
    }

    partial class MainForm : IDelegatesPage
    {
        DelegatesSesCntrler _Controller_SD;

        #region реализация интерфейса IDelegatesPage
        IMsgMethods IDelegatesPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        void IDelegatesPage.Init()
        {
            _Controller_SD = (DelegatesSesCntrler)_Controller.GetSubController("Session Delegates Controller");
            gridViewDelegates.ExpandAllGroups();
        }

        void IDelegatesPage.InitControls(ref XPCollection<SesDelegateObj> SesDelegates, bool IsStatementApp)
        {
            propertiesDelegate.DataSource = SesDelegates;
            gridDelegates.DataSource = SesDelegates;
            navigatorDelegates.DataSource = SesDelegates;

            if (IsStatementApp == true)
            {
                foreach (NavigatorCustomButton b in navigatorDelegates.Buttons.CustomButtons)
                {
                    b.Visible = false;
                }

//               navigatorDelegates.Enabled = false;
            }
        }

        int IDelegatesPage.NavigatorPosition
        {
            get
            {
                return navigatorDelegates.Position;
            }
            set
            {
                navigatorDelegates.Position = value;
            }
        }

        public void SetPropertySheet(Dictionary<string, string> properties)
        {
/*
            propertiesDelegate.Rows["rowFirstName"].Properties.Value = properties["FirstName"];
            propertiesDelegate.Rows["rowSecondName"].Properties.Value = properties["SecondName"];
            propertiesDelegate.Rows["rowLastName"].Properties.Value = properties["LastName"];

            propertiesDelegate.Rows["rowPartyName"].Properties.Value = properties["PartyName"];
            propertiesDelegate.Rows["rowFraction"].Properties.Value = properties["FractionName"];
            propertiesDelegate.Rows["rowRegionName"].Properties.Value = properties["RegionName"];
            propertiesDelegate.Rows["rowRegionNum"].Properties.Value = properties["RegionNum"];

            propertiesDelegate.Rows["rowMicNum"].Properties.Value = properties["MicNum"];
            propertiesDelegate.Rows["rowRowNum"].Properties.Value = properties["RowNum"];
            propertiesDelegate.Rows["rowSeatNum"].Properties.Value = properties["SeatNum"];
 */ 
        }

        #endregion

        private void navigatorDelegates_PositionChanged(object sender, EventArgs e)
        {
            if (_Controller_SD != null)
                _Controller_SD.UpdatePropData();

        }

        private void navigatorDelegates_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Append")
                {
                    _Controller_SD.AppendDelegate();
                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    DialogResult dr = _messages.ShowQuestion("Вы уверены, что хотите удалить выделенную запись?");
                    if (dr == DialogResult.Yes)
                    {
                        _Controller_SD.RemoveDelegate();
                    }
                }
                else if (e.Button.Tag.ToString() == "RemoveAll")
                {
                    DialogResult dr = _messages.ShowQuestion("Вы уверены, что хотите удалить все записи?");
                    if (dr == DialogResult.Yes)
                    {
                        _Controller_SD.RemoveDelegates();
                    }
                }

            }
        }
   }
}