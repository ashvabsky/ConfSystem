﻿namespace VoteSystem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            if (_IsDisposed == false)
            {
                if (disposing)
                {
                    _Controller.Dispose();
                    _IsDisposed = true;
                }
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.barSubItemSession = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem17 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem18 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem19 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem6 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem24 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem25 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem26 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.RibbonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.barbtnPredsedSendMsg = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnMonitorMsg = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnDelegates = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnQuestions = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnExit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.cntSeatFree = new DevExpress.XtraBars.BarButtonItem();
            this.cntSeatSelect = new DevExpress.XtraBars.BarButtonItem();
            this.cntSwitherPhysical = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.cntSwitherLogical = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barbtnInfoToMonitor = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnSplashToMonitor = new DevExpress.XtraBars.BarButtonItem();
            this.chkPanels = new DevExpress.XtraBars.BarCheckItem();
            this.btnSettings = new DevExpress.XtraBars.BarButtonItem();
            this.cntRegDelegate = new DevExpress.XtraBars.BarButtonItem();
            this.cntRegCard = new DevExpress.XtraBars.BarButtonItem();
            this.btndrvStart = new DevExpress.XtraBars.BarButtonItem();
            this.btndrvStop = new DevExpress.XtraBars.BarButtonItem();
            this.btndrvHitMics = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnMainMenu = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticText = new DevExpress.XtraBars.BarStaticItem();
            this.barLinkContainerItem1 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.cntSeatFix = new DevExpress.XtraBars.BarButtonItem();
            this.barProcesText = new DevExpress.XtraBars.BarStaticItem();
            this.barHistoryList = new DevExpress.XtraBars.BarListItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.seatComponent1 = new VoteSystem.SeatComponent();
            this.menuMicSeat = new DevExpress.XtraBars.PopupMenu(this.components);
            this.seatComponent4 = new VoteSystem.SeatComponent();
            this.seatComponent3 = new VoteSystem.SeatComponent();
            this.seatComponent2 = new VoteSystem.SeatComponent();
            this.seatComponent5 = new VoteSystem.SeatComponent();
            this.seatComponent6 = new VoteSystem.SeatComponent();
            this.seatComponent7 = new VoteSystem.SeatComponent();
            this.seatComponent8 = new VoteSystem.SeatComponent();
            this.seatComponent9 = new VoteSystem.SeatComponent();
            this.seatComponent10 = new VoteSystem.SeatComponent();
            this.seatComponent11 = new VoteSystem.SeatComponent();
            this.seatComponent12 = new VoteSystem.SeatComponent();
            this.seatComponent13 = new VoteSystem.SeatComponent();
            this.seatComponent14 = new VoteSystem.SeatComponent();
            this.seatComponent15 = new VoteSystem.SeatComponent();
            this.seatComponent16 = new VoteSystem.SeatComponent();
            this.seatComponent17 = new VoteSystem.SeatComponent();
            this.seatComponent18 = new VoteSystem.SeatComponent();
            this.seatComponent19 = new VoteSystem.SeatComponent();
            this.seatComponent20 = new VoteSystem.SeatComponent();
            this.seatComponent21 = new VoteSystem.SeatComponent();
            this.seatComponent22 = new VoteSystem.SeatComponent();
            this.seatComponent23 = new VoteSystem.SeatComponent();
            this.seatComponent31 = new VoteSystem.SeatComponent();
            this.seatComponent27 = new VoteSystem.SeatComponent();
            this.seatComponent30 = new VoteSystem.SeatComponent();
            this.seatComponent26 = new VoteSystem.SeatComponent();
            this.seatComponent29 = new VoteSystem.SeatComponent();
            this.seatComponent25 = new VoteSystem.SeatComponent();
            this.seatComponent28 = new VoteSystem.SeatComponent();
            this.seatComponent24 = new VoteSystem.SeatComponent();
            this.seatComponent33 = new VoteSystem.SeatComponent();
            this.seatComponent32 = new VoteSystem.SeatComponent();
            this.seatComponent139 = new VoteSystem.SeatComponent();
            this.seatComponent121 = new VoteSystem.SeatComponent();
            this.seatComponent86 = new VoteSystem.SeatComponent();
            this.seatComponent138 = new VoteSystem.SeatComponent();
            this.seatComponent137 = new VoteSystem.SeatComponent();
            this.seatComponent120 = new VoteSystem.SeatComponent();
            this.seatComponent119 = new VoteSystem.SeatComponent();
            this.seatComponent136 = new VoteSystem.SeatComponent();
            this.seatComponent87 = new VoteSystem.SeatComponent();
            this.seatComponent118 = new VoteSystem.SeatComponent();
            this.seatComponent135 = new VoteSystem.SeatComponent();
            this.seatComponent88 = new VoteSystem.SeatComponent();
            this.seatComponent117 = new VoteSystem.SeatComponent();
            this.seatComponent134 = new VoteSystem.SeatComponent();
            this.seatComponent89 = new VoteSystem.SeatComponent();
            this.seatComponent116 = new VoteSystem.SeatComponent();
            this.seatComponent133 = new VoteSystem.SeatComponent();
            this.seatComponent90 = new VoteSystem.SeatComponent();
            this.seatComponent115 = new VoteSystem.SeatComponent();
            this.seatComponent132 = new VoteSystem.SeatComponent();
            this.seatComponent91 = new VoteSystem.SeatComponent();
            this.seatComponent114 = new VoteSystem.SeatComponent();
            this.seatComponent131 = new VoteSystem.SeatComponent();
            this.seatComponent92 = new VoteSystem.SeatComponent();
            this.seatComponent113 = new VoteSystem.SeatComponent();
            this.seatComponent130 = new VoteSystem.SeatComponent();
            this.seatComponent93 = new VoteSystem.SeatComponent();
            this.seatComponent112 = new VoteSystem.SeatComponent();
            this.seatComponent129 = new VoteSystem.SeatComponent();
            this.seatComponent94 = new VoteSystem.SeatComponent();
            this.seatComponent111 = new VoteSystem.SeatComponent();
            this.seatComponent128 = new VoteSystem.SeatComponent();
            this.seatComponent95 = new VoteSystem.SeatComponent();
            this.seatComponent110 = new VoteSystem.SeatComponent();
            this.seatComponent127 = new VoteSystem.SeatComponent();
            this.seatComponent96 = new VoteSystem.SeatComponent();
            this.seatComponent109 = new VoteSystem.SeatComponent();
            this.seatComponent126 = new VoteSystem.SeatComponent();
            this.seatComponent97 = new VoteSystem.SeatComponent();
            this.seatComponent108 = new VoteSystem.SeatComponent();
            this.seatComponent125 = new VoteSystem.SeatComponent();
            this.seatComponent98 = new VoteSystem.SeatComponent();
            this.seatComponent107 = new VoteSystem.SeatComponent();
            this.seatComponent124 = new VoteSystem.SeatComponent();
            this.seatComponent99 = new VoteSystem.SeatComponent();
            this.seatComponent106 = new VoteSystem.SeatComponent();
            this.seatComponent123 = new VoteSystem.SeatComponent();
            this.seatComponent100 = new VoteSystem.SeatComponent();
            this.seatComponent105 = new VoteSystem.SeatComponent();
            this.seatComponent122 = new VoteSystem.SeatComponent();
            this.seatComponent101 = new VoteSystem.SeatComponent();
            this.seatComponent104 = new VoteSystem.SeatComponent();
            this.seatComponent102 = new VoteSystem.SeatComponent();
            this.seatComponent103 = new VoteSystem.SeatComponent();
            this.seatComponent68 = new VoteSystem.SeatComponent();
            this.seatComponent69 = new VoteSystem.SeatComponent();
            this.seatComponent70 = new VoteSystem.SeatComponent();
            this.seatComponent71 = new VoteSystem.SeatComponent();
            this.seatComponent72 = new VoteSystem.SeatComponent();
            this.seatComponent73 = new VoteSystem.SeatComponent();
            this.seatComponent74 = new VoteSystem.SeatComponent();
            this.seatComponent75 = new VoteSystem.SeatComponent();
            this.seatComponent76 = new VoteSystem.SeatComponent();
            this.seatComponent77 = new VoteSystem.SeatComponent();
            this.seatComponent78 = new VoteSystem.SeatComponent();
            this.seatComponent79 = new VoteSystem.SeatComponent();
            this.seatComponent80 = new VoteSystem.SeatComponent();
            this.seatComponent81 = new VoteSystem.SeatComponent();
            this.seatComponent82 = new VoteSystem.SeatComponent();
            this.seatComponent83 = new VoteSystem.SeatComponent();
            this.seatComponent84 = new VoteSystem.SeatComponent();
            this.seatComponent85 = new VoteSystem.SeatComponent();
            this.seatComponent67 = new VoteSystem.SeatComponent();
            this.seatComponent66 = new VoteSystem.SeatComponent();
            this.seatComponent65 = new VoteSystem.SeatComponent();
            this.seatComponent49 = new VoteSystem.SeatComponent();
            this.seatComponent64 = new VoteSystem.SeatComponent();
            this.seatComponent48 = new VoteSystem.SeatComponent();
            this.seatComponent63 = new VoteSystem.SeatComponent();
            this.seatComponent47 = new VoteSystem.SeatComponent();
            this.seatComponent62 = new VoteSystem.SeatComponent();
            this.seatComponent46 = new VoteSystem.SeatComponent();
            this.seatComponent61 = new VoteSystem.SeatComponent();
            this.seatComponent45 = new VoteSystem.SeatComponent();
            this.seatComponent60 = new VoteSystem.SeatComponent();
            this.seatComponent44 = new VoteSystem.SeatComponent();
            this.seatComponent59 = new VoteSystem.SeatComponent();
            this.seatComponent43 = new VoteSystem.SeatComponent();
            this.seatComponent58 = new VoteSystem.SeatComponent();
            this.seatComponent42 = new VoteSystem.SeatComponent();
            this.seatComponent57 = new VoteSystem.SeatComponent();
            this.seatComponent41 = new VoteSystem.SeatComponent();
            this.seatComponent56 = new VoteSystem.SeatComponent();
            this.seatComponent40 = new VoteSystem.SeatComponent();
            this.seatComponent55 = new VoteSystem.SeatComponent();
            this.seatComponent39 = new VoteSystem.SeatComponent();
            this.seatComponent54 = new VoteSystem.SeatComponent();
            this.seatComponent38 = new VoteSystem.SeatComponent();
            this.seatComponent53 = new VoteSystem.SeatComponent();
            this.seatComponent37 = new VoteSystem.SeatComponent();
            this.seatComponent52 = new VoteSystem.SeatComponent();
            this.seatComponent36 = new VoteSystem.SeatComponent();
            this.seatComponent51 = new VoteSystem.SeatComponent();
            this.seatComponent50 = new VoteSystem.SeatComponent();
            this.seatComponent35 = new VoteSystem.SeatComponent();
            this.seatComponent34 = new VoteSystem.SeatComponent();
            this.clientPanel = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.itmAgenda = new DevExpress.XtraNavBar.NavBarItem();
            this.itmDelegates = new DevExpress.XtraNavBar.NavBarItem();
            this.itmAgendaItems = new DevExpress.XtraNavBar.NavBarItem();
            this.itmRegistration = new DevExpress.XtraNavBar.NavBarItem();
            this.itmQuestions = new DevExpress.XtraNavBar.NavBarItem();
            this.itmReport = new DevExpress.XtraNavBar.NavBarItem();
            this.itmStatements = new DevExpress.XtraNavBar.NavBarItem();
            this.itmProtocol = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem10 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem11 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem12 = new DevExpress.XtraNavBar.NavBarItem();
            this.NavBarImages = new DevExpress.Utils.ImageCollection(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.MainTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.pgSession = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.richContent = new System.Windows.Forms.RichTextBox();
            this.grpSesCharacteristics = new DevExpress.XtraEditors.GroupControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.txtSpeechTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txtCoReportTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtReportTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtFinishDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtVoteTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl145 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtRegTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtStartDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtCaption = new DevExpress.XtraEditors.MemoEdit();
            this.styleController1 = new DevExpress.XtraEditors.StyleController(this.components);
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.lbHistory = new DevExpress.XtraEditors.ListBoxControl();
            this.pgDelegates = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridDelegates = new DevExpress.XtraGrid.GridControl();
            this.gridViewDelegates = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFraction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsRegistered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCardRegistered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMicNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.navigatorDelegates = new DevExpress.XtraEditors.DataNavigator();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.xpDelegates = new DevExpress.Xpo.XPCollection(this.components);
            this.propertiesDelegate = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.catFIO = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowLastName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFirstName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSecondName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catRegion = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowRegionName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRegionNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catInfo = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowidParty_Name = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDelegateType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowVoteType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catSeat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMicNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRowNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSeatNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catNotes = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowNotes = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow3 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.listBoxControl1 = new DevExpress.XtraEditors.ListBoxControl();
            this.pgSelQuestions = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.navControlQuestions = new DevExpress.XtraEditors.ControlNavigator();
            this.SelQuestGrid = new DevExpress.XtraGrid.GridControl();
            this.xpTasks = new DevExpress.Xpo.XPCollection(this.components);
            this.gridViewSelQuest = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQueueNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestCaption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsHot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.navigatorQuestions = new DevExpress.XtraEditors.DataNavigator();
            this.grpSelQuestPropsIn = new DevExpress.XtraEditors.GroupControl();
            this.PropertiesControlQuest = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox9 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox10 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox11 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox12 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox13 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox14 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.catCharacteristics = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowCaption = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowidKind_Name = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowCrDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowReadNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDescription = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow8 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow26 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catResult = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowResult = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowResDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow5 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.pgQuorumGraphic = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pnlHallSettings = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.btnCardStart = new DevExpress.XtraEditors.SimpleButton();
            this.btnTextOut = new DevExpress.XtraEditors.SimpleButton();
            this.spinBackCardMode = new DevExpress.XtraEditors.SpinEdit();
            this.chkBackCardMode = new DevExpress.XtraEditors.CheckEdit();
            this.btnClearSeats = new DevExpress.XtraEditors.SimpleButton();
            this.chkAutoCardMode = new DevExpress.XtraEditors.CheckEdit();
            this.chkDelegateSeat = new DevExpress.XtraEditors.CheckEdit();
            this.btnActAllCards = new DevExpress.XtraEditors.SimpleButton();
            this.pnlHall = new DevExpress.XtraEditors.GroupControl();
            this.info_1 = new DevExpress.XtraEditors.LabelControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.PropertiesControl_QPage = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox7 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox8 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox17 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_q_LastName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_FirstName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_SecondName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_q_Region = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_q_RegionName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_RegionNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_q_Info = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_q_Fraction = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_PartyName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_q_Seat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_q_MicNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_RowNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_SeatNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.pnlQuorumButtons = new DevExpress.XtraEditors.PanelControl();
            this.lblQuorumFinished = new DevExpress.XtraEditors.LabelControl();
            this.btnRegFinish = new DevExpress.XtraEditors.SimpleButton();
            this.txtCardQnty = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lblRegTime = new DevExpress.XtraEditors.LabelControl();
            this.progressBarRegistration = new DevExpress.XtraEditors.ProgressBarControl();
            this.txtQuorumQnty = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtRegQnty = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtDelegateQnty = new DevExpress.XtraEditors.TextEdit();
            this.lblCardQnty = new DevExpress.XtraEditors.LabelControl();
            this.lblRegistration = new DevExpress.XtraEditors.LabelControl();
            this.picRegTime = new DevExpress.XtraEditors.PictureEdit();
            this.btnRegStart = new DevExpress.XtraEditors.SimpleButton();
            this.pgQuestions = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.VoteQuestGrid = new DevExpress.XtraGrid.GridControl();
            this.GridViewQuestions = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_R = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCaption_R = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoteTime_R = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResult = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.navControlResQuestions = new DevExpress.XtraEditors.ControlNavigator();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.edtAmendmentType = new DevExpress.XtraEditors.LookUpEdit();
            this.xpAmendmentType = new DevExpress.Xpo.XPCollection(this.components);
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.edtAmendment = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl149 = new DevExpress.XtraEditors.LabelControl();
            this.edtReadingNum = new DevExpress.XtraEditors.LookUpEdit();
            this.xpReadNum = new DevExpress.Xpo.XPCollection(this.components);
            this.btnNewAgendaItem = new DevExpress.XtraEditors.SimpleButton();
            this.edtTaskItem = new DevExpress.XtraEditors.LookUpEdit();
            this.txtVoteCaption = new DevExpress.XtraEditors.TextEdit();
            this.labelControl148 = new DevExpress.XtraEditors.LabelControl();
            this.btnNewVoteFast = new DevExpress.XtraEditors.SimpleButton();
            this.cmbQuestKind = new DevExpress.XtraEditors.LookUpEdit();
            this.xpTaskKinds = new DevExpress.Xpo.XPCollection(this.components);
            this.labelControl147 = new DevExpress.XtraEditors.LabelControl();
            this.memoDescription = new DevExpress.XtraEditors.MemoExEdit();
            this.labelControl146 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.btnNewAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuestVoteStart = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuestVoteDetail = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuestNewRead = new DevExpress.XtraEditors.SimpleButton();
            this.navigatorResQuestions = new DevExpress.XtraEditors.DataNavigator();
            this.xpResQuestions = new DevExpress.Xpo.XPCollection(this.components);
            this.grpResQuestProps = new DevExpress.XtraEditors.GroupControl();
            this.grpResQuestPropsIn = new DevExpress.XtraEditors.GroupControl();
            this.PropertiesControlQuest2 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox15 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox16 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox18 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox19 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox20 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemTextEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.catCharacteristics_R = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowCaption_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTaskItem_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowidKind_Name_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowCrDate_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowReadNum_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmendment_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDescription_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catNotes_R = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowNotes_R = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catResult_R = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.categoryRow2 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.lblVoteDateTime = new System.Windows.Forms.Label();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.lblProcQnty = new System.Windows.Forms.Label();
            this.grpVoteResults = new System.Windows.Forms.GroupBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.lblProcAye = new System.Windows.Forms.Label();
            this.lblVoteAye = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.lblNonProcQnty = new System.Windows.Forms.Label();
            this.lblNonVoteQnty = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.lblProcAgainst = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.lblProcAbstain = new System.Windows.Forms.Label();
            this.lblVoteAgainst = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.lblVoteAbstain = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.domainUpDown5 = new System.Windows.Forms.DomainUpDown();
            this.label91 = new System.Windows.Forms.Label();
            this.lblDecision = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.lblVoteQnty = new System.Windows.Forms.Label();
            this.pgStatements = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btnStateClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnStateResult = new DevExpress.XtraEditors.SimpleButton();
            this.navControlStatements = new DevExpress.XtraEditors.ControlNavigator();
            this.SelStateGrid = new DevExpress.XtraGrid.GridControl();
            this.gridViewSelState = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskCaption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQueueNum_ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSortCriteria = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IsFinished = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStatementMode = new DevExpress.XtraEditors.SimpleButton();
            this.navigatorStatements = new DevExpress.XtraEditors.DataNavigator();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.PropertiesControlState = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox30 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox31 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox32 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox33 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox34 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox35 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemDateEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox36 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox37 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox38 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox39 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox40 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox41 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox42 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.catCharacteristics_st = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowID_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowName_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowStartDate_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDescription_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowStatus_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catTotals_st = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowDelegQnty_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSpeechTime_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSpeechTimeAvrg_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow6 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow25 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow4 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.dataSet1 = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataTable3 = new System.Data.DataTable();
            this.QuestNum = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.domainUpDown3 = new System.Windows.Forms.DomainUpDown();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.domainUpDown4 = new System.Windows.Forms.DomainUpDown();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.PropertyDataSet = new System.Data.DataSet();
            this.dataTable4 = new System.Data.DataTable();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.repositoryItemPictureEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.editorRow35 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow36 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow37 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow38 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow39 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow40 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow41 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow42 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow43 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow44 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow45 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow46 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow47 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.propertyGridControl4 = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.tmrRegStart = new System.Windows.Forms.Timer(this.components);
            this.TimerImages = new DevExpress.Utils.ImageCollection(this.components);
            this.tmrRotation = new System.Windows.Forms.Timer(this.components);
            this.xpSessions = new DevExpress.Xpo.XPCollection(this.components);
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.tmrRegCardStart = new System.Windows.Forms.Timer(this.components);
            this.tmrGeneral = new System.Windows.Forms.Timer(this.components);
            this.tmrCardBackMode = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuMicSeat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientPanel)).BeginInit();
            this.clientPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainTabControl)).BeginInit();
            this.MainTabControl.SuspendLayout();
            this.pgSession.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpSesCharacteristics)).BeginInit();
            this.grpSesCharacteristics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpeechTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoReportTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFinishDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFinishDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbHistory)).BeginInit();
            this.pgDelegates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertiesDelegate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).BeginInit();
            this.pgSelQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelQuestGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSelQuest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSelQuestPropsIn)).BeginInit();
            this.grpSelQuestPropsIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.pgQuorumGraphic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHallSettings)).BeginInit();
            this.pnlHallSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinBackCardMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBackCardMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoCardMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDelegateSeat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHall)).BeginInit();
            this.pnlHall.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl_QPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlQuorumButtons)).BeginInit();
            this.pnlQuorumButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarRegistration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuorumQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRegTime.Properties)).BeginInit();
            this.pgQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VoteQuestGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtAmendmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpAmendmentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtAmendment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtReadingNum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpReadNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtTaskItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteCaption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuestKind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpTaskKinds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpResQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpResQuestProps)).BeginInit();
            this.grpResQuestProps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpResQuestPropsIn)).BeginInit();
            this.grpResQuestPropsIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            this.grpVoteResults.SuspendLayout();
            this.pgStatements.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelStateGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSelState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimerImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpSessions)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit3.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbon.ApplicationButtonText = null;
            this.ribbon.ApplicationIcon = ((System.Drawing.Bitmap)(resources.GetObject("ribbon.ApplicationIcon")));
            this.ribbon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Images = this.RibbonImages;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.barbtnPredsedSendMsg,
            this.barbtnMonitorMsg,
            this.barbtnDelegates,
            this.barbtnQuestions,
            this.barbtnExit,
            this.barButtonItem11,
            this.barButtonItem12,
            this.cntSeatFree,
            this.cntSeatSelect,
            this.cntSwitherPhysical,
            this.cntSwitherLogical,
            this.barStaticItem1,
            this.barbtnInfoToMonitor,
            this.barSubItem1,
            this.barButtonItem1,
            this.barSubItemSession,
            this.barButtonItem2,
            this.barSubItem2,
            this.barSubItem3,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem9,
            this.barButtonItem15,
            this.barButtonItem16,
            this.barSubItem4,
            this.barSubItem5,
            this.barSubItem6,
            this.barButtonItem17,
            this.barButtonItem18,
            this.barButtonItem19,
            this.barButtonItem20,
            this.barButtonItem21,
            this.barButtonItem22,
            this.barButtonItem23,
            this.barButtonItem24,
            this.barButtonItem25,
            this.barButtonItem26,
            this.barbtnReport,
            this.barButtonItem10,
            this.barbtnSplashToMonitor,
            this.chkPanels,
            this.btnSettings,
            this.cntRegDelegate,
            this.cntRegCard,
            this.btndrvStart,
            this.btndrvStop,
            this.btndrvHitMics,
            this.barbtnMainMenu,
            this.barStaticText,
            this.barLinkContainerItem1,
            this.barStaticItem2,
            this.cntSeatFix,
            this.barProcesText,
            this.barHistoryList});
            this.ribbon.LargeImages = this.RibbonImages;
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 87;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.QuickToolbarItemLinks.Add(this.barSubItem1);
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3});
            this.ribbon.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1884, 114);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            this.ribbon.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            this.ribbon.SizeChanged += new System.EventHandler(this.ribbon_SizeChanged);
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.ItemLinks.Add(this.barSubItemSession);
            this.applicationMenu1.ItemLinks.Add(this.barSubItem2);
            this.applicationMenu1.ItemLinks.Add(this.barSubItem3);
            this.applicationMenu1.ItemLinks.Add(this.barSubItem4);
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbon;
            // 
            // barSubItemSession
            // 
            this.barSubItemSession.Caption = "Сессия";
            this.barSubItemSession.Id = 35;
            this.barSubItemSession.ImageOptions.ImageIndex = 5;
            this.barSubItemSession.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem6)});
            this.barSubItemSession.Name = "barSubItemSession";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Новая...";
            this.barButtonItem2.Id = 36;
            this.barButtonItem2.ImageOptions.ImageIndex = 5;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Загрузить...";
            this.barButtonItem3.Id = 39;
            this.barButtonItem3.ImageOptions.ImageIndex = 5;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "Эксопрт в файл";
            this.barSubItem5.Id = 45;
            this.barSubItem5.ImageOptions.ImageIndex = 8;
            this.barSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem17),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem18),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem19)});
            this.barSubItem5.Name = "barSubItem5";
            // 
            // barButtonItem17
            // 
            this.barButtonItem17.Caption = "Список делегатов";
            this.barButtonItem17.Id = 47;
            this.barButtonItem17.Name = "barButtonItem17";
            // 
            // barButtonItem18
            // 
            this.barButtonItem18.Caption = "Список вопросов";
            this.barButtonItem18.Id = 48;
            this.barButtonItem18.Name = "barButtonItem18";
            // 
            // barButtonItem19
            // 
            this.barButtonItem19.Caption = "Итоги голосований";
            this.barButtonItem19.Id = 49;
            this.barButtonItem19.Name = "barButtonItem19";
            // 
            // barSubItem6
            // 
            this.barSubItem6.Caption = "Импорт из файла";
            this.barSubItem6.Id = 46;
            this.barSubItem6.ImageOptions.ImageIndex = 8;
            this.barSubItem6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem20),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem22)});
            this.barSubItem6.Name = "barSubItem6";
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Caption = "Список делегатов";
            this.barButtonItem20.Id = 50;
            this.barButtonItem20.Name = "barButtonItem20";
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Caption = "Список вопросов";
            this.barButtonItem21.Id = 51;
            this.barButtonItem21.Name = "barButtonItem21";
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Caption = "Итоги голосований";
            this.barButtonItem22.Id = 52;
            this.barButtonItem22.Name = "barButtonItem22";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "База данных";
            this.barSubItem2.Id = 37;
            this.barSubItem2.ImageOptions.ImageIndex = 12;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem15)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Создать резервную копию";
            this.barButtonItem4.Id = 40;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Загрузить резервную копию";
            this.barButtonItem9.Id = 41;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "Очистить";
            this.barButtonItem15.Id = 42;
            this.barButtonItem15.Name = "barButtonItem15";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Настройки";
            this.barSubItem3.Id = 38;
            this.barSubItem3.ImageOptions.ImageIndex = 6;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem23),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem24),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem25),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem26)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Caption = "Сетевые настройки";
            this.barButtonItem23.Id = 53;
            this.barButtonItem23.Name = "barButtonItem23";
            // 
            // barButtonItem24
            // 
            this.barButtonItem24.Caption = "Система голосования";
            this.barButtonItem24.Id = 54;
            this.barButtonItem24.Name = "barButtonItem24";
            // 
            // barButtonItem25
            // 
            this.barButtonItem25.Caption = "Общие";
            this.barButtonItem25.Id = 55;
            this.barButtonItem25.Name = "barButtonItem25";
            // 
            // barButtonItem26
            // 
            this.barButtonItem26.Caption = "Тест системы";
            this.barButtonItem26.Id = 56;
            this.barButtonItem26.Name = "barButtonItem26";
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "Отчеты";
            this.barSubItem4.Id = 44;
            this.barSubItem4.ImageOptions.ImageIndex = 8;
            this.barSubItem4.Name = "barSubItem4";
            // 
            // RibbonImages
            // 
            this.RibbonImages.ImageSize = new System.Drawing.Size(48, 48);
            this.RibbonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("RibbonImages.ImageStream")));
            this.RibbonImages.Images.SetKeyName(22, "Clock_Pos1.png");
            // 
            // barbtnPredsedSendMsg
            // 
            this.barbtnPredsedSendMsg.Caption = "Председателю";
            this.barbtnPredsedSendMsg.Id = 0;
            this.barbtnPredsedSendMsg.ImageOptions.LargeImageIndex = 0;
            this.barbtnPredsedSendMsg.LargeWidth = 90;
            this.barbtnPredsedSendMsg.Name = "barbtnPredsedSendMsg";
            this.barbtnPredsedSendMsg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnPredsedSendMsg_ItemClick);
            // 
            // barbtnMonitorMsg
            // 
            this.barbtnMonitorMsg.Caption = "На мониторы";
            this.barbtnMonitorMsg.Id = 1;
            this.barbtnMonitorMsg.ImageOptions.LargeImageIndex = 0;
            this.barbtnMonitorMsg.LargeWidth = 90;
            this.barbtnMonitorMsg.Name = "barbtnMonitorMsg";
            this.barbtnMonitorMsg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnMonitorMsg_ItemClick);
            // 
            // barbtnDelegates
            // 
            this.barbtnDelegates.Caption = "Депутаты";
            this.barbtnDelegates.Id = 5;
            this.barbtnDelegates.ImageOptions.LargeImageIndex = 3;
            this.barbtnDelegates.LargeWidth = 120;
            this.barbtnDelegates.Name = "barbtnDelegates";
            this.barbtnDelegates.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnDelegates_ItemClick);
            // 
            // barbtnQuestions
            // 
            this.barbtnQuestions.Caption = "Вопросы";
            this.barbtnQuestions.Id = 6;
            this.barbtnQuestions.ImageOptions.LargeImageIndex = 4;
            this.barbtnQuestions.LargeWidth = 120;
            this.barbtnQuestions.Name = "barbtnQuestions";
            this.barbtnQuestions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnQuestions_ItemClick);
            // 
            // barbtnExit
            // 
            this.barbtnExit.Caption = "Выход";
            this.barbtnExit.Id = 9;
            this.barbtnExit.ImageOptions.LargeImageIndex = 7;
            this.barbtnExit.LargeWidth = 98;
            this.barbtnExit.Name = "barbtnExit";
            this.barbtnExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnExit_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "По депутатам";
            this.barButtonItem11.Id = 12;
            this.barButtonItem11.LargeWidth = 90;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "Статистика";
            this.barButtonItem12.Id = 13;
            this.barButtonItem12.LargeWidth = 90;
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // cntSeatFree
            // 
            this.cntSeatFree.Caption = "Освободить место";
            this.cntSeatFree.Id = 24;
            this.cntSeatFree.Name = "cntSeatFree";
            this.cntSeatFree.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.cntSeatFree_ItemPress);
            // 
            // cntSeatSelect
            // 
            this.cntSeatSelect.Caption = "Выбрать депутата на место...";
            this.cntSeatSelect.Id = 25;
            this.cntSeatSelect.Name = "cntSeatSelect";
            this.cntSeatSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cntSeatSelect_ItemClick);
            // 
            // cntSwitherPhysical
            // 
            this.cntSwitherPhysical.Caption = "Вкл\\Выкл физически";
            this.cntSwitherPhysical.Edit = this.repositoryItemCheckEdit1;
            this.cntSwitherPhysical.EditValue = true;
            this.cntSwitherPhysical.Id = 26;
            this.cntSwitherPhysical.Name = "cntSwitherPhysical";
            this.cntSwitherPhysical.EditValueChanged += new System.EventHandler(this.cntSwitherPhysical_EditValueChanged);
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style16;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // cntSwitherLogical
            // 
            this.cntSwitherLogical.Caption = "Вкл\\Выкл логически";
            this.cntSwitherLogical.Edit = this.repositoryItemCheckEdit2;
            this.cntSwitherLogical.EditValue = true;
            this.cntSwitherLogical.Id = 28;
            this.cntSwitherLogical.Name = "cntSwitherLogical";
            this.cntSwitherLogical.EditValueChanged += new System.EventHandler(this.cntSwitherLogical_EditValueChanged);
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style15;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "___________________________";
            this.barStaticItem1.Id = 29;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barbtnInfoToMonitor
            // 
            this.barbtnInfoToMonitor.Caption = "Вывод информации";
            this.barbtnInfoToMonitor.Id = 32;
            this.barbtnInfoToMonitor.ImageOptions.LargeImageIndex = 10;
            this.barbtnInfoToMonitor.LargeWidth = 80;
            this.barbtnInfoToMonitor.Name = "barbtnInfoToMonitor";
            this.barbtnInfoToMonitor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnInfoToMonitor_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "barSubItem1";
            this.barSubItem1.Id = 33;
            this.barSubItem1.MenuCaption = "hjh";
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.ShowMenuCaption = true;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 34;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "barButtonItem16";
            this.barButtonItem16.Id = 43;
            this.barButtonItem16.Name = "barButtonItem16";
            // 
            // barbtnReport
            // 
            this.barbtnReport.Caption = "Отчеты";
            this.barbtnReport.Id = 57;
            this.barbtnReport.ImageOptions.LargeImageIndex = 8;
            this.barbtnReport.LargeWidth = 90;
            this.barbtnReport.Name = "barbtnReport";
            this.barbtnReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnReport_ItemClick);
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Протокол";
            this.barButtonItem10.Id = 58;
            this.barButtonItem10.ImageOptions.LargeImageIndex = 19;
            this.barButtonItem10.LargeWidth = 80;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // barbtnSplashToMonitor
            // 
            this.barbtnSplashToMonitor.Caption = "Вывести заставку";
            this.barbtnSplashToMonitor.Id = 64;
            this.barbtnSplashToMonitor.ImageOptions.LargeImageIndex = 9;
            this.barbtnSplashToMonitor.LargeWidth = 80;
            this.barbtnSplashToMonitor.Name = "barbtnSplashToMonitor";
            this.barbtnSplashToMonitor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnSplashToMonitor_ItemClick);
            // 
            // chkPanels
            // 
            this.chkPanels.Caption = "Включить";
            this.chkPanels.Id = 65;
            this.chkPanels.ImageOptions.DisabledLargeImageIndex = 14;
            this.chkPanels.ImageOptions.ImageIndex = 14;
            this.chkPanels.LargeWidth = 80;
            this.chkPanels.Name = "chkPanels";
            this.chkPanels.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.chkPanels_CheckedChanged);
            // 
            // btnSettings
            // 
            this.btnSettings.Caption = "Настройки";
            this.btnSettings.Id = 66;
            this.btnSettings.ImageOptions.LargeImageIndex = 6;
            this.btnSettings.LargeWidth = 90;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSettings_ItemClick);
            // 
            // cntRegDelegate
            // 
            this.cntRegDelegate.Caption = "Регистрировать депутата";
            this.cntRegDelegate.Id = 67;
            this.cntRegDelegate.Name = "cntRegDelegate";
            this.cntRegDelegate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cntRegDelegate_ItemClick);
            // 
            // cntRegCard
            // 
            this.cntRegCard.Caption = "Регистрировать карточку";
            this.cntRegCard.Id = 68;
            this.cntRegCard.Name = "cntRegCard";
            this.cntRegCard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cntRegCard_ItemClick);
            // 
            // btndrvStart
            // 
            this.btndrvStart.Caption = "Старт";
            this.btndrvStart.Id = 69;
            this.btndrvStart.ImageOptions.LargeImageIndex = 16;
            this.btndrvStart.LargeWidth = 100;
            this.btndrvStart.Name = "btndrvStart";
            this.btndrvStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btndrvStart_ItemClick);
            // 
            // btndrvStop
            // 
            this.btndrvStop.Caption = "Стоп";
            this.btndrvStop.Id = 70;
            this.btndrvStop.ImageOptions.LargeImageIndex = 17;
            this.btndrvStop.LargeWidth = 100;
            this.btndrvStop.Name = "btndrvStop";
            this.btndrvStop.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btndrvStop_ItemClick);
            // 
            // btndrvHitMics
            // 
            this.btndrvHitMics.Caption = "Тест микрофонов";
            this.btndrvHitMics.Id = 71;
            this.btndrvHitMics.ImageOptions.LargeImageIndex = 18;
            this.btndrvHitMics.LargeWidth = 100;
            this.btndrvHitMics.Name = "btndrvHitMics";
            this.btndrvHitMics.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btndrvHitMics_ItemClick);
            // 
            // barbtnMainMenu
            // 
            this.barbtnMainMenu.Caption = "Главное меню";
            this.barbtnMainMenu.Id = 72;
            this.barbtnMainMenu.ImageOptions.LargeImageIndex = 20;
            this.barbtnMainMenu.LargeWidth = 110;
            this.barbtnMainMenu.Name = "barbtnMainMenu";
            this.barbtnMainMenu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnMainMenu_ItemClick);
            // 
            // barStaticText
            // 
            this.barStaticText.Id = 76;
            this.barStaticText.ImageOptions.ImageIndex = 21;
            this.barStaticText.Name = "barStaticText";
            this.barStaticText.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.barStaticText.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barLinkContainerItem1
            // 
            this.barLinkContainerItem1.Caption = "barLinkContainerItem1";
            this.barLinkContainerItem1.Id = 77;
            this.barLinkContainerItem1.Name = "barLinkContainerItem1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "___________________________";
            this.barStaticItem2.Id = 78;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // cntSeatFix
            // 
            this.cntSeatFix.Caption = "Закрепить место";
            this.cntSeatFix.Id = 79;
            this.cntSeatFix.Name = "cntSeatFix";
            this.cntSeatFix.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cntSeatFix_ItemClick);
            // 
            // barProcesText
            // 
            this.barProcesText.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barProcesText.Caption = "Идет идентификация карточек...                                                   " +
    "   ";
            this.barProcesText.Id = 82;
            this.barProcesText.ImageOptions.ImageIndex = 22;
            this.barProcesText.ImageOptions.LargeImageIndex = 17;
            this.barProcesText.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barProcesText.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Red;
            this.barProcesText.ItemAppearance.Normal.Options.UseFont = true;
            this.barProcesText.ItemAppearance.Normal.Options.UseForeColor = true;
            this.barProcesText.Name = "barProcesText";
            this.barProcesText.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barHistoryList
            // 
            this.barHistoryList.Caption = "         История событий        ";
            this.barHistoryList.Id = 84;
            this.barHistoryList.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barHistoryList.ItemAppearance.Normal.Options.UseFont = true;
            this.barHistoryList.Name = "barHistoryList";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup8,
            this.ribbonPageGroup2,
            this.ribbonPageGroup1,
            this.ribbonPageGroup3,
            this.ribbonPageGroup4,
            this.ribbonPageGroup6});
            this.ribbonPage1.Name = "ribbonPage1";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.barbtnMainMenu);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.chkPanels);
            this.ribbonPageGroup2.ItemLinks.Add(this.barbtnSplashToMonitor);
            this.ribbonPageGroup2.ItemLinks.Add(this.barbtnInfoToMonitor);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Мониторы";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barbtnPredsedSendMsg);
            this.ribbonPageGroup1.ItemLinks.Add(this.barbtnMonitorMsg);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Сообщения";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barbtnDelegates);
            this.ribbonPageGroup3.ItemLinks.Add(this.barbtnQuestions);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Базы данных, отчеты, статистика";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btndrvStop);
            this.ribbonPageGroup4.ItemLinks.Add(this.btndrvHitMics, true);
            this.ribbonPageGroup4.ItemLinks.Add(this.btndrvStart);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Работа с драйвером";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barbtnReport);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnSettings);
            this.ribbonPageGroup6.ItemLinks.Add(this.barbtnExit);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Сервис";
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ribbonStatusBar.ForeColor = System.Drawing.Color.Red;
            this.ribbonStatusBar.ItemLinks.Add(this.barHistoryList);
            this.ribbonStatusBar.ItemLinks.Add(this.barStaticText);
            this.ribbonStatusBar.ItemLinks.Add(this.barProcesText);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 977);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1884, 24);
            // 
            // seatComponent1
            // 
            this.seatComponent1.AllowDrop = true;
            this.seatComponent1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent1.Location = new System.Drawing.Point(419, 38);
            this.seatComponent1.Name = "seatComponent1";
            this.ribbon.SetPopupContextMenu(this.seatComponent1, this.menuMicSeat);
            this.seatComponent1.SeatNumber = "1";
            this.seatComponent1.Size = new System.Drawing.Size(36, 51);
            this.seatComponent1.TabIndex = 0;
            this.seatComponent1.Tag = "Mic001";
            this.seatComponent1.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent1.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // menuMicSeat
            // 
            this.menuMicSeat.ItemLinks.Add(this.cntSwitherPhysical);
            this.menuMicSeat.ItemLinks.Add(this.cntSwitherLogical);
            this.menuMicSeat.ItemLinks.Add(this.barStaticItem1);
            this.menuMicSeat.ItemLinks.Add(this.cntSeatFree);
            this.menuMicSeat.ItemLinks.Add(this.cntSeatSelect);
            this.menuMicSeat.ItemLinks.Add(this.cntSeatFix);
            this.menuMicSeat.ItemLinks.Add(this.barStaticItem2);
            this.menuMicSeat.ItemLinks.Add(this.cntRegDelegate);
            this.menuMicSeat.ItemLinks.Add(this.cntRegCard);
            this.menuMicSeat.Name = "menuMicSeat";
            this.menuMicSeat.Ribbon = this.ribbon;
            this.menuMicSeat.CloseUp += new System.EventHandler(this.menuMicSeat_CloseUp);
            this.menuMicSeat.Popup += new System.EventHandler(this.menuMicSeat_Popup);
            // 
            // seatComponent4
            // 
            this.seatComponent4.AllowDrop = true;
            this.seatComponent4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent4.Location = new System.Drawing.Point(617, 38);
            this.seatComponent4.Name = "seatComponent4";
            this.ribbon.SetPopupContextMenu(this.seatComponent4, this.menuMicSeat);
            this.seatComponent4.SeatNumber = "4";
            this.seatComponent4.Size = new System.Drawing.Size(36, 51);
            this.seatComponent4.TabIndex = 553;
            this.seatComponent4.Tag = "Mic004";
            this.seatComponent4.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent4.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // seatComponent3
            // 
            this.seatComponent3.AllowDrop = true;
            this.seatComponent3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent3.Location = new System.Drawing.Point(551, 38);
            this.seatComponent3.Name = "seatComponent3";
            this.ribbon.SetPopupContextMenu(this.seatComponent3, this.menuMicSeat);
            this.seatComponent3.SeatNumber = "3";
            this.seatComponent3.Size = new System.Drawing.Size(36, 51);
            this.seatComponent3.TabIndex = 552;
            this.seatComponent3.Tag = "Mic003";
            this.seatComponent3.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent3.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // seatComponent2
            // 
            this.seatComponent2.AllowDrop = true;
            this.seatComponent2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent2.Location = new System.Drawing.Point(485, 38);
            this.seatComponent2.Name = "seatComponent2";
            this.ribbon.SetPopupContextMenu(this.seatComponent2, this.menuMicSeat);
            this.seatComponent2.SeatNumber = "2";
            this.seatComponent2.Size = new System.Drawing.Size(36, 51);
            this.seatComponent2.TabIndex = 551;
            this.seatComponent2.Tag = "Mic002";
            this.seatComponent2.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent2.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // seatComponent5
            // 
            this.seatComponent5.AllowDrop = true;
            this.seatComponent5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent5.Location = new System.Drawing.Point(768, 38);
            this.seatComponent5.Name = "seatComponent5";
            this.ribbon.SetPopupContextMenu(this.seatComponent5, this.menuMicSeat);
            this.seatComponent5.SeatNumber = "5";
            this.seatComponent5.Size = new System.Drawing.Size(36, 51);
            this.seatComponent5.TabIndex = 554;
            this.seatComponent5.Tag = "Mic005";
            this.seatComponent5.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent5.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // seatComponent6
            // 
            this.seatComponent6.AllowDrop = true;
            this.seatComponent6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent6.Location = new System.Drawing.Point(527, 89);
            this.seatComponent6.Name = "seatComponent6";
            this.ribbon.SetPopupContextMenu(this.seatComponent6, this.menuMicSeat);
            this.seatComponent6.SeatNumber = "6";
            this.seatComponent6.Size = new System.Drawing.Size(36, 51);
            this.seatComponent6.TabIndex = 555;
            this.seatComponent6.Tag = "Mic006";
            this.seatComponent6.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent6.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent6.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent6.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // seatComponent7
            // 
            this.seatComponent7.AllowDrop = true;
            this.seatComponent7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent7.Location = new System.Drawing.Point(267, 88);
            this.seatComponent7.Name = "seatComponent7";
            this.ribbon.SetPopupContextMenu(this.seatComponent7, this.menuMicSeat);
            this.seatComponent7.SeatNumber = "7";
            this.seatComponent7.Size = new System.Drawing.Size(36, 51);
            this.seatComponent7.TabIndex = 556;
            this.seatComponent7.Tag = "Mic007";
            this.seatComponent7.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent7.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent7.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent8
            // 
            this.seatComponent8.AllowDrop = true;
            this.seatComponent8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent8.Location = new System.Drawing.Point(228, 89);
            this.seatComponent8.Name = "seatComponent8";
            this.ribbon.SetPopupContextMenu(this.seatComponent8, this.menuMicSeat);
            this.seatComponent8.SeatNumber = "8";
            this.seatComponent8.Size = new System.Drawing.Size(36, 51);
            this.seatComponent8.TabIndex = 557;
            this.seatComponent8.Tag = "Mic008";
            this.seatComponent8.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent8.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent8.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent9
            // 
            this.seatComponent9.AllowDrop = true;
            this.seatComponent9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent9.Location = new System.Drawing.Point(189, 89);
            this.seatComponent9.Name = "seatComponent9";
            this.ribbon.SetPopupContextMenu(this.seatComponent9, this.menuMicSeat);
            this.seatComponent9.SeatNumber = "9";
            this.seatComponent9.Size = new System.Drawing.Size(36, 51);
            this.seatComponent9.TabIndex = 558;
            this.seatComponent9.Tag = "Mic009";
            this.seatComponent9.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent9.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent9.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent10
            // 
            this.seatComponent10.AllowDrop = true;
            this.seatComponent10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent10.Location = new System.Drawing.Point(288, 147);
            this.seatComponent10.Name = "seatComponent10";
            this.ribbon.SetPopupContextMenu(this.seatComponent10, this.menuMicSeat);
            this.seatComponent10.SeatNumber = "10";
            this.seatComponent10.Size = new System.Drawing.Size(36, 51);
            this.seatComponent10.TabIndex = 559;
            this.seatComponent10.Tag = "Mic010";
            this.seatComponent10.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent10.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent10.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent11
            // 
            this.seatComponent11.AllowDrop = true;
            this.seatComponent11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent11.Location = new System.Drawing.Point(327, 147);
            this.seatComponent11.Name = "seatComponent11";
            this.ribbon.SetPopupContextMenu(this.seatComponent11, this.menuMicSeat);
            this.seatComponent11.SeatNumber = "11";
            this.seatComponent11.Size = new System.Drawing.Size(36, 51);
            this.seatComponent11.TabIndex = 560;
            this.seatComponent11.Tag = "Mic011";
            this.seatComponent11.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent11.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent11.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent12
            // 
            this.seatComponent12.AllowDrop = true;
            this.seatComponent12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent12.Location = new System.Drawing.Point(366, 147);
            this.seatComponent12.Name = "seatComponent12";
            this.ribbon.SetPopupContextMenu(this.seatComponent12, this.menuMicSeat);
            this.seatComponent12.SeatNumber = "12";
            this.seatComponent12.Size = new System.Drawing.Size(36, 51);
            this.seatComponent12.TabIndex = 561;
            this.seatComponent12.Tag = "Mic012";
            this.seatComponent12.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent12.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent12.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent13
            // 
            this.seatComponent13.AllowDrop = true;
            this.seatComponent13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent13.Location = new System.Drawing.Point(406, 147);
            this.seatComponent13.Name = "seatComponent13";
            this.ribbon.SetPopupContextMenu(this.seatComponent13, this.menuMicSeat);
            this.seatComponent13.SeatNumber = "13";
            this.seatComponent13.Size = new System.Drawing.Size(36, 51);
            this.seatComponent13.TabIndex = 562;
            this.seatComponent13.Tag = "Mic013";
            this.seatComponent13.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent13.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent13.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent14
            // 
            this.seatComponent14.AllowDrop = true;
            this.seatComponent14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent14.Location = new System.Drawing.Point(839, 147);
            this.seatComponent14.Name = "seatComponent14";
            this.ribbon.SetPopupContextMenu(this.seatComponent14, this.menuMicSeat);
            this.seatComponent14.SeatNumber = "17";
            this.seatComponent14.Size = new System.Drawing.Size(36, 51);
            this.seatComponent14.TabIndex = 566;
            this.seatComponent14.Tag = "Mic017";
            this.seatComponent14.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent14.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent14.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent15
            // 
            this.seatComponent15.AllowDrop = true;
            this.seatComponent15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent15.Location = new System.Drawing.Point(794, 147);
            this.seatComponent15.Name = "seatComponent15";
            this.ribbon.SetPopupContextMenu(this.seatComponent15, this.menuMicSeat);
            this.seatComponent15.SeatNumber = "16";
            this.seatComponent15.Size = new System.Drawing.Size(36, 51);
            this.seatComponent15.TabIndex = 565;
            this.seatComponent15.Tag = "Mic016";
            this.seatComponent15.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent15.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent15.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent16
            // 
            this.seatComponent16.AllowDrop = true;
            this.seatComponent16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent16.Location = new System.Drawing.Point(749, 147);
            this.seatComponent16.Name = "seatComponent16";
            this.ribbon.SetPopupContextMenu(this.seatComponent16, this.menuMicSeat);
            this.seatComponent16.SeatNumber = "15";
            this.seatComponent16.Size = new System.Drawing.Size(36, 51);
            this.seatComponent16.TabIndex = 564;
            this.seatComponent16.Tag = "Mic015";
            this.seatComponent16.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent16.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent16.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent17
            // 
            this.seatComponent17.AllowDrop = true;
            this.seatComponent17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent17.Location = new System.Drawing.Point(704, 147);
            this.seatComponent17.Name = "seatComponent17";
            this.ribbon.SetPopupContextMenu(this.seatComponent17, this.menuMicSeat);
            this.seatComponent17.SeatNumber = "14";
            this.seatComponent17.Size = new System.Drawing.Size(36, 51);
            this.seatComponent17.TabIndex = 563;
            this.seatComponent17.Tag = "Mic014";
            this.seatComponent17.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent17.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent17.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent18
            // 
            this.seatComponent18.AllowDrop = true;
            this.seatComponent18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent18.Location = new System.Drawing.Point(975, 204);
            this.seatComponent18.Name = "seatComponent18";
            this.ribbon.SetPopupContextMenu(this.seatComponent18, this.menuMicSeat);
            this.seatComponent18.SeatNumber = "18";
            this.seatComponent18.Size = new System.Drawing.Size(36, 51);
            this.seatComponent18.TabIndex = 568;
            this.seatComponent18.Tag = "Mic018";
            this.seatComponent18.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent18.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent18.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent19
            // 
            this.seatComponent19.AllowDrop = true;
            this.seatComponent19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent19.Location = new System.Drawing.Point(933, 204);
            this.seatComponent19.Name = "seatComponent19";
            this.ribbon.SetPopupContextMenu(this.seatComponent19, this.menuMicSeat);
            this.seatComponent19.SeatNumber = "19";
            this.seatComponent19.Size = new System.Drawing.Size(36, 51);
            this.seatComponent19.TabIndex = 567;
            this.seatComponent19.Tag = "Mic019";
            this.seatComponent19.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent19.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent19.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent20
            // 
            this.seatComponent20.AllowDrop = true;
            this.seatComponent20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent20.Location = new System.Drawing.Point(841, 204);
            this.seatComponent20.Name = "seatComponent20";
            this.ribbon.SetPopupContextMenu(this.seatComponent20, this.menuMicSeat);
            this.seatComponent20.SeatNumber = "20";
            this.seatComponent20.Size = new System.Drawing.Size(36, 51);
            this.seatComponent20.TabIndex = 572;
            this.seatComponent20.Tag = "Mic020";
            this.seatComponent20.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent20.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent20.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent21
            // 
            this.seatComponent21.AllowDrop = true;
            this.seatComponent21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent21.Location = new System.Drawing.Point(796, 204);
            this.seatComponent21.Name = "seatComponent21";
            this.ribbon.SetPopupContextMenu(this.seatComponent21, this.menuMicSeat);
            this.seatComponent21.SeatNumber = "21";
            this.seatComponent21.Size = new System.Drawing.Size(36, 51);
            this.seatComponent21.TabIndex = 571;
            this.seatComponent21.Tag = "Mic021";
            this.seatComponent21.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent21.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent21.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent22
            // 
            this.seatComponent22.AllowDrop = true;
            this.seatComponent22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent22.Location = new System.Drawing.Point(751, 204);
            this.seatComponent22.Name = "seatComponent22";
            this.ribbon.SetPopupContextMenu(this.seatComponent22, this.menuMicSeat);
            this.seatComponent22.SeatNumber = "22";
            this.seatComponent22.Size = new System.Drawing.Size(36, 51);
            this.seatComponent22.TabIndex = 570;
            this.seatComponent22.Tag = "Mic022";
            this.seatComponent22.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent22.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent22.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent23
            // 
            this.seatComponent23.AllowDrop = true;
            this.seatComponent23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent23.Location = new System.Drawing.Point(706, 204);
            this.seatComponent23.Name = "seatComponent23";
            this.ribbon.SetPopupContextMenu(this.seatComponent23, this.menuMicSeat);
            this.seatComponent23.SeatNumber = "23";
            this.seatComponent23.Size = new System.Drawing.Size(36, 51);
            this.seatComponent23.TabIndex = 569;
            this.seatComponent23.Tag = "Mic023";
            this.seatComponent23.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent23.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent23.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent31
            // 
            this.seatComponent31.AllowDrop = true;
            this.seatComponent31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent31.Location = new System.Drawing.Point(417, 204);
            this.seatComponent31.Name = "seatComponent31";
            this.ribbon.SetPopupContextMenu(this.seatComponent31, this.menuMicSeat);
            this.seatComponent31.SeatNumber = "28";
            this.seatComponent31.Size = new System.Drawing.Size(36, 51);
            this.seatComponent31.TabIndex = 572;
            this.seatComponent31.Tag = "Mic028";
            this.seatComponent31.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent31.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent31.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent27
            // 
            this.seatComponent27.AllowDrop = true;
            this.seatComponent27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent27.Location = new System.Drawing.Point(626, 204);
            this.seatComponent27.Name = "seatComponent27";
            this.ribbon.SetPopupContextMenu(this.seatComponent27, this.menuMicSeat);
            this.seatComponent27.SeatNumber = "24";
            this.seatComponent27.Size = new System.Drawing.Size(36, 51);
            this.seatComponent27.TabIndex = 572;
            this.seatComponent27.Tag = "Mic024";
            this.seatComponent27.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent27.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent27.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent30
            // 
            this.seatComponent30.AllowDrop = true;
            this.seatComponent30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent30.Location = new System.Drawing.Point(374, 204);
            this.seatComponent30.Name = "seatComponent30";
            this.ribbon.SetPopupContextMenu(this.seatComponent30, this.menuMicSeat);
            this.seatComponent30.SeatNumber = "29";
            this.seatComponent30.Size = new System.Drawing.Size(36, 51);
            this.seatComponent30.TabIndex = 571;
            this.seatComponent30.Tag = "Mic029";
            this.seatComponent30.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent30.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent30.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent26
            // 
            this.seatComponent26.AllowDrop = true;
            this.seatComponent26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent26.Location = new System.Drawing.Point(582, 204);
            this.seatComponent26.Name = "seatComponent26";
            this.ribbon.SetPopupContextMenu(this.seatComponent26, this.menuMicSeat);
            this.seatComponent26.SeatNumber = "25";
            this.seatComponent26.Size = new System.Drawing.Size(36, 51);
            this.seatComponent26.TabIndex = 571;
            this.seatComponent26.Tag = "Mic025";
            this.seatComponent26.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent26.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent26.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent29
            // 
            this.seatComponent29.AllowDrop = true;
            this.seatComponent29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent29.Location = new System.Drawing.Point(331, 204);
            this.seatComponent29.Name = "seatComponent29";
            this.ribbon.SetPopupContextMenu(this.seatComponent29, this.menuMicSeat);
            this.seatComponent29.SeatNumber = "30";
            this.seatComponent29.Size = new System.Drawing.Size(36, 51);
            this.seatComponent29.TabIndex = 570;
            this.seatComponent29.Tag = "Mic030";
            this.seatComponent29.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent29.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent29.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent25
            // 
            this.seatComponent25.AllowDrop = true;
            this.seatComponent25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent25.Location = new System.Drawing.Point(538, 204);
            this.seatComponent25.Name = "seatComponent25";
            this.ribbon.SetPopupContextMenu(this.seatComponent25, this.menuMicSeat);
            this.seatComponent25.SeatNumber = "26";
            this.seatComponent25.Size = new System.Drawing.Size(36, 51);
            this.seatComponent25.TabIndex = 570;
            this.seatComponent25.Tag = "Mic026";
            this.seatComponent25.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent25.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent25.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent28
            // 
            this.seatComponent28.AllowDrop = true;
            this.seatComponent28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent28.Location = new System.Drawing.Point(288, 204);
            this.seatComponent28.Name = "seatComponent28";
            this.ribbon.SetPopupContextMenu(this.seatComponent28, this.menuMicSeat);
            this.seatComponent28.SeatNumber = "31";
            this.seatComponent28.Size = new System.Drawing.Size(36, 51);
            this.seatComponent28.TabIndex = 569;
            this.seatComponent28.Tag = "Mic031";
            this.seatComponent28.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent28.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent28.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent24
            // 
            this.seatComponent24.AllowDrop = true;
            this.seatComponent24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent24.Location = new System.Drawing.Point(494, 204);
            this.seatComponent24.Name = "seatComponent24";
            this.ribbon.SetPopupContextMenu(this.seatComponent24, this.menuMicSeat);
            this.seatComponent24.SeatNumber = "27";
            this.seatComponent24.Size = new System.Drawing.Size(36, 51);
            this.seatComponent24.TabIndex = 569;
            this.seatComponent24.Tag = "Mic027";
            this.seatComponent24.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent24.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent24.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent33
            // 
            this.seatComponent33.AllowDrop = true;
            this.seatComponent33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent33.Location = new System.Drawing.Point(226, 204);
            this.seatComponent33.Name = "seatComponent33";
            this.ribbon.SetPopupContextMenu(this.seatComponent33, this.menuMicSeat);
            this.seatComponent33.SeatNumber = "32";
            this.seatComponent33.Size = new System.Drawing.Size(36, 51);
            this.seatComponent33.TabIndex = 568;
            this.seatComponent33.Tag = "Mic032";
            this.seatComponent33.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent33.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent33.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent32
            // 
            this.seatComponent32.AllowDrop = true;
            this.seatComponent32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent32.Location = new System.Drawing.Point(182, 204);
            this.seatComponent32.Name = "seatComponent32";
            this.ribbon.SetPopupContextMenu(this.seatComponent32, this.menuMicSeat);
            this.seatComponent32.SeatNumber = "33";
            this.seatComponent32.Size = new System.Drawing.Size(36, 51);
            this.seatComponent32.TabIndex = 567;
            this.seatComponent32.Tag = "Mic033";
            this.seatComponent32.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent32.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent32.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent139
            // 
            this.seatComponent139.AllowDrop = true;
            this.seatComponent139.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent139.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent139.Location = new System.Drawing.Point(1017, 548);
            this.seatComponent139.Name = "seatComponent139";
            this.ribbon.SetPopupContextMenu(this.seatComponent139, this.menuMicSeat);
            this.seatComponent139.SeatNumber = "122";
            this.seatComponent139.Size = new System.Drawing.Size(36, 51);
            this.seatComponent139.TabIndex = 618;
            this.seatComponent139.Tag = "Mic122";
            this.seatComponent139.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent139.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent139.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent121
            // 
            this.seatComponent121.AllowDrop = true;
            this.seatComponent121.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent121.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent121.Location = new System.Drawing.Point(1017, 491);
            this.seatComponent121.Name = "seatComponent121";
            this.ribbon.SetPopupContextMenu(this.seatComponent121, this.menuMicSeat);
            this.seatComponent121.SeatNumber = "121";
            this.seatComponent121.Size = new System.Drawing.Size(36, 51);
            this.seatComponent121.TabIndex = 618;
            this.seatComponent121.Tag = "Mic121";
            this.seatComponent121.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent121.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent121.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent86
            // 
            this.seatComponent86.AllowDrop = true;
            this.seatComponent86.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent86.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent86.Location = new System.Drawing.Point(1017, 434);
            this.seatComponent86.Name = "seatComponent86";
            this.ribbon.SetPopupContextMenu(this.seatComponent86, this.menuMicSeat);
            this.seatComponent86.SeatNumber = "86";
            this.seatComponent86.Size = new System.Drawing.Size(36, 51);
            this.seatComponent86.TabIndex = 618;
            this.seatComponent86.Tag = "Mic086";
            this.seatComponent86.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent86.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent86.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent138
            // 
            this.seatComponent138.AllowDrop = true;
            this.seatComponent138.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent138.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent138.Location = new System.Drawing.Point(138, 545);
            this.seatComponent138.Name = "seatComponent138";
            this.ribbon.SetPopupContextMenu(this.seatComponent138, this.menuMicSeat);
            this.seatComponent138.SeatNumber = "139";
            this.seatComponent138.Size = new System.Drawing.Size(36, 51);
            this.seatComponent138.TabIndex = 617;
            this.seatComponent138.Tag = "Mic139";
            this.seatComponent138.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent138.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent138.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent137
            // 
            this.seatComponent137.AllowDrop = true;
            this.seatComponent137.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent137.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent137.Location = new System.Drawing.Point(417, 546);
            this.seatComponent137.Name = "seatComponent137";
            this.ribbon.SetPopupContextMenu(this.seatComponent137, this.menuMicSeat);
            this.seatComponent137.SeatNumber = "133";
            this.seatComponent137.Size = new System.Drawing.Size(36, 51);
            this.seatComponent137.TabIndex = 616;
            this.seatComponent137.Tag = "Mic133";
            this.seatComponent137.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent137.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent137.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent120
            // 
            this.seatComponent120.AllowDrop = true;
            this.seatComponent120.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent120.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent120.Location = new System.Drawing.Point(138, 489);
            this.seatComponent120.Name = "seatComponent120";
            this.ribbon.SetPopupContextMenu(this.seatComponent120, this.menuMicSeat);
            this.seatComponent120.SeatNumber = "104";
            this.seatComponent120.Size = new System.Drawing.Size(36, 51);
            this.seatComponent120.TabIndex = 617;
            this.seatComponent120.Tag = "Mic104";
            this.seatComponent120.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent120.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent120.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent119
            // 
            this.seatComponent119.AllowDrop = true;
            this.seatComponent119.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent119.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent119.Location = new System.Drawing.Point(417, 489);
            this.seatComponent119.Name = "seatComponent119";
            this.ribbon.SetPopupContextMenu(this.seatComponent119, this.menuMicSeat);
            this.seatComponent119.SeatNumber = "110";
            this.seatComponent119.Size = new System.Drawing.Size(36, 51);
            this.seatComponent119.TabIndex = 616;
            this.seatComponent119.Tag = "Mic110";
            this.seatComponent119.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent119.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent119.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent136
            // 
            this.seatComponent136.AllowDrop = true;
            this.seatComponent136.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent136.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent136.Location = new System.Drawing.Point(626, 546);
            this.seatComponent136.Name = "seatComponent136";
            this.ribbon.SetPopupContextMenu(this.seatComponent136, this.menuMicSeat);
            this.seatComponent136.SeatNumber = "129";
            this.seatComponent136.Size = new System.Drawing.Size(36, 51);
            this.seatComponent136.TabIndex = 615;
            this.seatComponent136.Tag = "Mic129";
            this.seatComponent136.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent136.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent136.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent87
            // 
            this.seatComponent87.AllowDrop = true;
            this.seatComponent87.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent87.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent87.Location = new System.Drawing.Point(138, 433);
            this.seatComponent87.Name = "seatComponent87";
            this.ribbon.SetPopupContextMenu(this.seatComponent87, this.menuMicSeat);
            this.seatComponent87.SeatNumber = "103";
            this.seatComponent87.Size = new System.Drawing.Size(36, 51);
            this.seatComponent87.TabIndex = 617;
            this.seatComponent87.Tag = "Mic103";
            this.seatComponent87.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent87.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent87.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent118
            // 
            this.seatComponent118.AllowDrop = true;
            this.seatComponent118.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent118.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent118.Location = new System.Drawing.Point(626, 489);
            this.seatComponent118.Name = "seatComponent118";
            this.ribbon.SetPopupContextMenu(this.seatComponent118, this.menuMicSeat);
            this.seatComponent118.SeatNumber = "114";
            this.seatComponent118.Size = new System.Drawing.Size(36, 51);
            this.seatComponent118.TabIndex = 615;
            this.seatComponent118.Tag = "Mic114";
            this.seatComponent118.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent118.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent118.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent135
            // 
            this.seatComponent135.AllowDrop = true;
            this.seatComponent135.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent135.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent135.Location = new System.Drawing.Point(841, 546);
            this.seatComponent135.Name = "seatComponent135";
            this.ribbon.SetPopupContextMenu(this.seatComponent135, this.menuMicSeat);
            this.seatComponent135.SeatNumber = "125";
            this.seatComponent135.Size = new System.Drawing.Size(36, 51);
            this.seatComponent135.TabIndex = 614;
            this.seatComponent135.Tag = "Mic125";
            this.seatComponent135.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent135.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent135.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent88
            // 
            this.seatComponent88.AllowDrop = true;
            this.seatComponent88.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent88.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent88.Location = new System.Drawing.Point(417, 432);
            this.seatComponent88.Name = "seatComponent88";
            this.ribbon.SetPopupContextMenu(this.seatComponent88, this.menuMicSeat);
            this.seatComponent88.SeatNumber = "97";
            this.seatComponent88.Size = new System.Drawing.Size(36, 51);
            this.seatComponent88.TabIndex = 616;
            this.seatComponent88.Tag = "Mic097";
            this.seatComponent88.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent88.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent88.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent117
            // 
            this.seatComponent117.AllowDrop = true;
            this.seatComponent117.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent117.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent117.Location = new System.Drawing.Point(841, 489);
            this.seatComponent117.Name = "seatComponent117";
            this.ribbon.SetPopupContextMenu(this.seatComponent117, this.menuMicSeat);
            this.seatComponent117.SeatNumber = "118";
            this.seatComponent117.Size = new System.Drawing.Size(36, 51);
            this.seatComponent117.TabIndex = 614;
            this.seatComponent117.Tag = "Mic118";
            this.seatComponent117.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent117.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent117.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent134
            // 
            this.seatComponent134.AllowDrop = true;
            this.seatComponent134.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent134.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent134.Location = new System.Drawing.Point(374, 546);
            this.seatComponent134.Name = "seatComponent134";
            this.ribbon.SetPopupContextMenu(this.seatComponent134, this.menuMicSeat);
            this.seatComponent134.SeatNumber = "134";
            this.seatComponent134.Size = new System.Drawing.Size(36, 51);
            this.seatComponent134.TabIndex = 613;
            this.seatComponent134.Tag = "Mic134";
            this.seatComponent134.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent134.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent134.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent89
            // 
            this.seatComponent89.AllowDrop = true;
            this.seatComponent89.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent89.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent89.Location = new System.Drawing.Point(626, 432);
            this.seatComponent89.Name = "seatComponent89";
            this.ribbon.SetPopupContextMenu(this.seatComponent89, this.menuMicSeat);
            this.seatComponent89.SeatNumber = "93";
            this.seatComponent89.Size = new System.Drawing.Size(36, 51);
            this.seatComponent89.TabIndex = 615;
            this.seatComponent89.Tag = "Mic093";
            this.seatComponent89.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent89.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent89.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent116
            // 
            this.seatComponent116.AllowDrop = true;
            this.seatComponent116.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent116.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent116.Location = new System.Drawing.Point(374, 489);
            this.seatComponent116.Name = "seatComponent116";
            this.ribbon.SetPopupContextMenu(this.seatComponent116, this.menuMicSeat);
            this.seatComponent116.SeatNumber = "109";
            this.seatComponent116.Size = new System.Drawing.Size(36, 51);
            this.seatComponent116.TabIndex = 613;
            this.seatComponent116.Tag = "Mic109";
            this.seatComponent116.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent116.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent116.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent133
            // 
            this.seatComponent133.AllowDrop = true;
            this.seatComponent133.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent133.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent133.Location = new System.Drawing.Point(582, 546);
            this.seatComponent133.Name = "seatComponent133";
            this.ribbon.SetPopupContextMenu(this.seatComponent133, this.menuMicSeat);
            this.seatComponent133.SeatNumber = "130";
            this.seatComponent133.Size = new System.Drawing.Size(36, 51);
            this.seatComponent133.TabIndex = 612;
            this.seatComponent133.Tag = "Mic130";
            this.seatComponent133.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent133.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent133.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent90
            // 
            this.seatComponent90.AllowDrop = true;
            this.seatComponent90.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent90.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent90.Location = new System.Drawing.Point(841, 432);
            this.seatComponent90.Name = "seatComponent90";
            this.ribbon.SetPopupContextMenu(this.seatComponent90, this.menuMicSeat);
            this.seatComponent90.SeatNumber = "89";
            this.seatComponent90.Size = new System.Drawing.Size(36, 51);
            this.seatComponent90.TabIndex = 614;
            this.seatComponent90.Tag = "Mic089";
            this.seatComponent90.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent90.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent90.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent115
            // 
            this.seatComponent115.AllowDrop = true;
            this.seatComponent115.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent115.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent115.Location = new System.Drawing.Point(582, 489);
            this.seatComponent115.Name = "seatComponent115";
            this.ribbon.SetPopupContextMenu(this.seatComponent115, this.menuMicSeat);
            this.seatComponent115.SeatNumber = "113";
            this.seatComponent115.Size = new System.Drawing.Size(36, 51);
            this.seatComponent115.TabIndex = 612;
            this.seatComponent115.Tag = "Mic113";
            this.seatComponent115.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent115.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent115.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent132
            // 
            this.seatComponent132.AllowDrop = true;
            this.seatComponent132.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent132.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent132.Location = new System.Drawing.Point(796, 546);
            this.seatComponent132.Name = "seatComponent132";
            this.ribbon.SetPopupContextMenu(this.seatComponent132, this.menuMicSeat);
            this.seatComponent132.SeatNumber = "126";
            this.seatComponent132.Size = new System.Drawing.Size(36, 51);
            this.seatComponent132.TabIndex = 611;
            this.seatComponent132.Tag = "Mic126";
            this.seatComponent132.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent132.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent132.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent91
            // 
            this.seatComponent91.AllowDrop = true;
            this.seatComponent91.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent91.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent91.Location = new System.Drawing.Point(374, 432);
            this.seatComponent91.Name = "seatComponent91";
            this.ribbon.SetPopupContextMenu(this.seatComponent91, this.menuMicSeat);
            this.seatComponent91.SeatNumber = "98";
            this.seatComponent91.Size = new System.Drawing.Size(36, 51);
            this.seatComponent91.TabIndex = 613;
            this.seatComponent91.Tag = "Mic098";
            this.seatComponent91.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent91.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent91.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent114
            // 
            this.seatComponent114.AllowDrop = true;
            this.seatComponent114.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent114.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent114.Location = new System.Drawing.Point(796, 489);
            this.seatComponent114.Name = "seatComponent114";
            this.ribbon.SetPopupContextMenu(this.seatComponent114, this.menuMicSeat);
            this.seatComponent114.SeatNumber = "117";
            this.seatComponent114.Size = new System.Drawing.Size(36, 51);
            this.seatComponent114.TabIndex = 611;
            this.seatComponent114.Tag = "Mic117";
            this.seatComponent114.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent114.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent114.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent131
            // 
            this.seatComponent131.AllowDrop = true;
            this.seatComponent131.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent131.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent131.Location = new System.Drawing.Point(331, 546);
            this.seatComponent131.Name = "seatComponent131";
            this.ribbon.SetPopupContextMenu(this.seatComponent131, this.menuMicSeat);
            this.seatComponent131.SeatNumber = "135";
            this.seatComponent131.Size = new System.Drawing.Size(36, 51);
            this.seatComponent131.TabIndex = 609;
            this.seatComponent131.Tag = "Mic135";
            this.seatComponent131.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent131.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent131.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent92
            // 
            this.seatComponent92.AllowDrop = true;
            this.seatComponent92.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent92.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent92.Location = new System.Drawing.Point(582, 432);
            this.seatComponent92.Name = "seatComponent92";
            this.ribbon.SetPopupContextMenu(this.seatComponent92, this.menuMicSeat);
            this.seatComponent92.SeatNumber = "94";
            this.seatComponent92.Size = new System.Drawing.Size(36, 51);
            this.seatComponent92.TabIndex = 612;
            this.seatComponent92.Tag = "Mic094";
            this.seatComponent92.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent92.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent92.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent113
            // 
            this.seatComponent113.AllowDrop = true;
            this.seatComponent113.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent113.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent113.Location = new System.Drawing.Point(331, 489);
            this.seatComponent113.Name = "seatComponent113";
            this.ribbon.SetPopupContextMenu(this.seatComponent113, this.menuMicSeat);
            this.seatComponent113.SeatNumber = "108";
            this.seatComponent113.Size = new System.Drawing.Size(36, 51);
            this.seatComponent113.TabIndex = 609;
            this.seatComponent113.Tag = "Mic108";
            this.seatComponent113.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent113.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent113.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent130
            // 
            this.seatComponent130.AllowDrop = true;
            this.seatComponent130.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent130.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent130.Location = new System.Drawing.Point(538, 546);
            this.seatComponent130.Name = "seatComponent130";
            this.ribbon.SetPopupContextMenu(this.seatComponent130, this.menuMicSeat);
            this.seatComponent130.SeatNumber = "131";
            this.seatComponent130.Size = new System.Drawing.Size(36, 51);
            this.seatComponent130.TabIndex = 610;
            this.seatComponent130.Tag = "Mic131";
            this.seatComponent130.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent130.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent130.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent93
            // 
            this.seatComponent93.AllowDrop = true;
            this.seatComponent93.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent93.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent93.Location = new System.Drawing.Point(796, 432);
            this.seatComponent93.Name = "seatComponent93";
            this.ribbon.SetPopupContextMenu(this.seatComponent93, this.menuMicSeat);
            this.seatComponent93.SeatNumber = "90";
            this.seatComponent93.Size = new System.Drawing.Size(36, 51);
            this.seatComponent93.TabIndex = 611;
            this.seatComponent93.Tag = "Mic090";
            this.seatComponent93.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent93.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent93.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent112
            // 
            this.seatComponent112.AllowDrop = true;
            this.seatComponent112.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent112.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent112.Location = new System.Drawing.Point(538, 489);
            this.seatComponent112.Name = "seatComponent112";
            this.ribbon.SetPopupContextMenu(this.seatComponent112, this.menuMicSeat);
            this.seatComponent112.SeatNumber = "112";
            this.seatComponent112.Size = new System.Drawing.Size(36, 51);
            this.seatComponent112.TabIndex = 610;
            this.seatComponent112.Tag = "Mic112";
            this.seatComponent112.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent112.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent112.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent129
            // 
            this.seatComponent129.AllowDrop = true;
            this.seatComponent129.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent129.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent129.Location = new System.Drawing.Point(751, 546);
            this.seatComponent129.Name = "seatComponent129";
            this.ribbon.SetPopupContextMenu(this.seatComponent129, this.menuMicSeat);
            this.seatComponent129.SeatNumber = "127";
            this.seatComponent129.Size = new System.Drawing.Size(36, 51);
            this.seatComponent129.TabIndex = 608;
            this.seatComponent129.Tag = "Mic127";
            this.seatComponent129.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent129.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent129.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent94
            // 
            this.seatComponent94.AllowDrop = true;
            this.seatComponent94.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent94.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent94.Location = new System.Drawing.Point(331, 432);
            this.seatComponent94.Name = "seatComponent94";
            this.ribbon.SetPopupContextMenu(this.seatComponent94, this.menuMicSeat);
            this.seatComponent94.SeatNumber = "99";
            this.seatComponent94.Size = new System.Drawing.Size(36, 51);
            this.seatComponent94.TabIndex = 609;
            this.seatComponent94.Tag = "Mic099";
            this.seatComponent94.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent94.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent94.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent111
            // 
            this.seatComponent111.AllowDrop = true;
            this.seatComponent111.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent111.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent111.Location = new System.Drawing.Point(751, 489);
            this.seatComponent111.Name = "seatComponent111";
            this.ribbon.SetPopupContextMenu(this.seatComponent111, this.menuMicSeat);
            this.seatComponent111.SeatNumber = "116";
            this.seatComponent111.Size = new System.Drawing.Size(36, 51);
            this.seatComponent111.TabIndex = 608;
            this.seatComponent111.Tag = "Mic116";
            this.seatComponent111.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent111.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent111.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent128
            // 
            this.seatComponent128.AllowDrop = true;
            this.seatComponent128.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent128.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent128.Location = new System.Drawing.Point(288, 546);
            this.seatComponent128.Name = "seatComponent128";
            this.ribbon.SetPopupContextMenu(this.seatComponent128, this.menuMicSeat);
            this.seatComponent128.SeatNumber = "136";
            this.seatComponent128.Size = new System.Drawing.Size(36, 51);
            this.seatComponent128.TabIndex = 607;
            this.seatComponent128.Tag = "Mic136";
            this.seatComponent128.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent128.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent128.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent95
            // 
            this.seatComponent95.AllowDrop = true;
            this.seatComponent95.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent95.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent95.Location = new System.Drawing.Point(538, 432);
            this.seatComponent95.Name = "seatComponent95";
            this.ribbon.SetPopupContextMenu(this.seatComponent95, this.menuMicSeat);
            this.seatComponent95.SeatNumber = "95";
            this.seatComponent95.Size = new System.Drawing.Size(36, 51);
            this.seatComponent95.TabIndex = 610;
            this.seatComponent95.Tag = "Mic095";
            this.seatComponent95.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent95.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent95.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent110
            // 
            this.seatComponent110.AllowDrop = true;
            this.seatComponent110.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent110.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent110.Location = new System.Drawing.Point(288, 489);
            this.seatComponent110.Name = "seatComponent110";
            this.ribbon.SetPopupContextMenu(this.seatComponent110, this.menuMicSeat);
            this.seatComponent110.SeatNumber = "107";
            this.seatComponent110.Size = new System.Drawing.Size(36, 51);
            this.seatComponent110.TabIndex = 607;
            this.seatComponent110.Tag = "Mic107";
            this.seatComponent110.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent110.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent110.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent127
            // 
            this.seatComponent127.AllowDrop = true;
            this.seatComponent127.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent127.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent127.Location = new System.Drawing.Point(494, 546);
            this.seatComponent127.Name = "seatComponent127";
            this.ribbon.SetPopupContextMenu(this.seatComponent127, this.menuMicSeat);
            this.seatComponent127.SeatNumber = "132";
            this.seatComponent127.Size = new System.Drawing.Size(36, 51);
            this.seatComponent127.TabIndex = 606;
            this.seatComponent127.Tag = "Mic132";
            this.seatComponent127.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent127.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent127.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent96
            // 
            this.seatComponent96.AllowDrop = true;
            this.seatComponent96.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent96.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent96.Location = new System.Drawing.Point(751, 432);
            this.seatComponent96.Name = "seatComponent96";
            this.ribbon.SetPopupContextMenu(this.seatComponent96, this.menuMicSeat);
            this.seatComponent96.SeatNumber = "91";
            this.seatComponent96.Size = new System.Drawing.Size(36, 51);
            this.seatComponent96.TabIndex = 608;
            this.seatComponent96.Tag = "Mic091";
            this.seatComponent96.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent96.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent96.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent109
            // 
            this.seatComponent109.AllowDrop = true;
            this.seatComponent109.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent109.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent109.Location = new System.Drawing.Point(494, 489);
            this.seatComponent109.Name = "seatComponent109";
            this.ribbon.SetPopupContextMenu(this.seatComponent109, this.menuMicSeat);
            this.seatComponent109.SeatNumber = "111";
            this.seatComponent109.Size = new System.Drawing.Size(36, 51);
            this.seatComponent109.TabIndex = 606;
            this.seatComponent109.Tag = "Mic111";
            this.seatComponent109.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent109.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent109.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent126
            // 
            this.seatComponent126.AllowDrop = true;
            this.seatComponent126.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent126.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent126.Location = new System.Drawing.Point(706, 546);
            this.seatComponent126.Name = "seatComponent126";
            this.ribbon.SetPopupContextMenu(this.seatComponent126, this.menuMicSeat);
            this.seatComponent126.SeatNumber = "128";
            this.seatComponent126.Size = new System.Drawing.Size(36, 51);
            this.seatComponent126.TabIndex = 605;
            this.seatComponent126.Tag = "Mic128";
            this.seatComponent126.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent126.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent126.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent97
            // 
            this.seatComponent97.AllowDrop = true;
            this.seatComponent97.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent97.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent97.Location = new System.Drawing.Point(288, 432);
            this.seatComponent97.Name = "seatComponent97";
            this.ribbon.SetPopupContextMenu(this.seatComponent97, this.menuMicSeat);
            this.seatComponent97.SeatNumber = "100";
            this.seatComponent97.Size = new System.Drawing.Size(36, 51);
            this.seatComponent97.TabIndex = 607;
            this.seatComponent97.Tag = "Mic100";
            this.seatComponent97.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent97.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent97.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent108
            // 
            this.seatComponent108.AllowDrop = true;
            this.seatComponent108.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent108.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent108.Location = new System.Drawing.Point(706, 489);
            this.seatComponent108.Name = "seatComponent108";
            this.ribbon.SetPopupContextMenu(this.seatComponent108, this.menuMicSeat);
            this.seatComponent108.SeatNumber = "115";
            this.seatComponent108.Size = new System.Drawing.Size(36, 51);
            this.seatComponent108.TabIndex = 605;
            this.seatComponent108.Tag = "Mic115";
            this.seatComponent108.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent108.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent108.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent125
            // 
            this.seatComponent125.AllowDrop = true;
            this.seatComponent125.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent125.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent125.Location = new System.Drawing.Point(226, 546);
            this.seatComponent125.Name = "seatComponent125";
            this.ribbon.SetPopupContextMenu(this.seatComponent125, this.menuMicSeat);
            this.seatComponent125.SeatNumber = "137";
            this.seatComponent125.Size = new System.Drawing.Size(36, 51);
            this.seatComponent125.TabIndex = 604;
            this.seatComponent125.Tag = "Mic137";
            this.seatComponent125.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent125.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent125.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent98
            // 
            this.seatComponent98.AllowDrop = true;
            this.seatComponent98.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent98.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent98.Location = new System.Drawing.Point(494, 432);
            this.seatComponent98.Name = "seatComponent98";
            this.ribbon.SetPopupContextMenu(this.seatComponent98, this.menuMicSeat);
            this.seatComponent98.SeatNumber = "96";
            this.seatComponent98.Size = new System.Drawing.Size(36, 51);
            this.seatComponent98.TabIndex = 606;
            this.seatComponent98.Tag = "Mic096";
            this.seatComponent98.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent98.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent98.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent107
            // 
            this.seatComponent107.AllowDrop = true;
            this.seatComponent107.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent107.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent107.Location = new System.Drawing.Point(226, 489);
            this.seatComponent107.Name = "seatComponent107";
            this.ribbon.SetPopupContextMenu(this.seatComponent107, this.menuMicSeat);
            this.seatComponent107.SeatNumber = "106";
            this.seatComponent107.Size = new System.Drawing.Size(36, 51);
            this.seatComponent107.TabIndex = 604;
            this.seatComponent107.Tag = "Mic106";
            this.seatComponent107.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent107.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent107.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent124
            // 
            this.seatComponent124.AllowDrop = true;
            this.seatComponent124.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent124.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent124.Location = new System.Drawing.Point(975, 548);
            this.seatComponent124.Name = "seatComponent124";
            this.ribbon.SetPopupContextMenu(this.seatComponent124, this.menuMicSeat);
            this.seatComponent124.SeatNumber = "123";
            this.seatComponent124.Size = new System.Drawing.Size(36, 51);
            this.seatComponent124.TabIndex = 603;
            this.seatComponent124.Tag = "Mic123";
            this.seatComponent124.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent124.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent124.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent99
            // 
            this.seatComponent99.AllowDrop = true;
            this.seatComponent99.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent99.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent99.Location = new System.Drawing.Point(706, 432);
            this.seatComponent99.Name = "seatComponent99";
            this.ribbon.SetPopupContextMenu(this.seatComponent99, this.menuMicSeat);
            this.seatComponent99.SeatNumber = "92";
            this.seatComponent99.Size = new System.Drawing.Size(36, 51);
            this.seatComponent99.TabIndex = 605;
            this.seatComponent99.Tag = "Mic092";
            this.seatComponent99.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent99.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent99.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent106
            // 
            this.seatComponent106.AllowDrop = true;
            this.seatComponent106.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent106.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent106.Location = new System.Drawing.Point(975, 491);
            this.seatComponent106.Name = "seatComponent106";
            this.ribbon.SetPopupContextMenu(this.seatComponent106, this.menuMicSeat);
            this.seatComponent106.SeatNumber = "120";
            this.seatComponent106.Size = new System.Drawing.Size(36, 51);
            this.seatComponent106.TabIndex = 603;
            this.seatComponent106.Tag = "Mic120";
            this.seatComponent106.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent106.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent106.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent123
            // 
            this.seatComponent123.AllowDrop = true;
            this.seatComponent123.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent123.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent123.Location = new System.Drawing.Point(182, 546);
            this.seatComponent123.Name = "seatComponent123";
            this.ribbon.SetPopupContextMenu(this.seatComponent123, this.menuMicSeat);
            this.seatComponent123.SeatNumber = "138";
            this.seatComponent123.Size = new System.Drawing.Size(36, 51);
            this.seatComponent123.TabIndex = 602;
            this.seatComponent123.Tag = "Mic138";
            this.seatComponent123.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent123.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent123.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent100
            // 
            this.seatComponent100.AllowDrop = true;
            this.seatComponent100.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent100.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent100.Location = new System.Drawing.Point(226, 432);
            this.seatComponent100.Name = "seatComponent100";
            this.ribbon.SetPopupContextMenu(this.seatComponent100, this.menuMicSeat);
            this.seatComponent100.SeatNumber = "101";
            this.seatComponent100.Size = new System.Drawing.Size(36, 51);
            this.seatComponent100.TabIndex = 604;
            this.seatComponent100.Tag = "Mic101";
            this.seatComponent100.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent100.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent100.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent105
            // 
            this.seatComponent105.AllowDrop = true;
            this.seatComponent105.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent105.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent105.Location = new System.Drawing.Point(182, 489);
            this.seatComponent105.Name = "seatComponent105";
            this.ribbon.SetPopupContextMenu(this.seatComponent105, this.menuMicSeat);
            this.seatComponent105.SeatNumber = "105";
            this.seatComponent105.Size = new System.Drawing.Size(36, 51);
            this.seatComponent105.TabIndex = 602;
            this.seatComponent105.Tag = "Mic105";
            this.seatComponent105.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent105.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent105.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent122
            // 
            this.seatComponent122.AllowDrop = true;
            this.seatComponent122.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent122.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent122.Location = new System.Drawing.Point(933, 548);
            this.seatComponent122.Name = "seatComponent122";
            this.ribbon.SetPopupContextMenu(this.seatComponent122, this.menuMicSeat);
            this.seatComponent122.SeatNumber = "124";
            this.seatComponent122.Size = new System.Drawing.Size(36, 51);
            this.seatComponent122.TabIndex = 601;
            this.seatComponent122.Tag = "Mic124";
            this.seatComponent122.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent122.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent122.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent101
            // 
            this.seatComponent101.AllowDrop = true;
            this.seatComponent101.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent101.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent101.Location = new System.Drawing.Point(975, 434);
            this.seatComponent101.Name = "seatComponent101";
            this.ribbon.SetPopupContextMenu(this.seatComponent101, this.menuMicSeat);
            this.seatComponent101.SeatNumber = "87";
            this.seatComponent101.Size = new System.Drawing.Size(36, 51);
            this.seatComponent101.TabIndex = 603;
            this.seatComponent101.Tag = "Mic087";
            this.seatComponent101.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent101.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent101.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent104
            // 
            this.seatComponent104.AllowDrop = true;
            this.seatComponent104.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent104.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent104.Location = new System.Drawing.Point(933, 491);
            this.seatComponent104.Name = "seatComponent104";
            this.ribbon.SetPopupContextMenu(this.seatComponent104, this.menuMicSeat);
            this.seatComponent104.SeatNumber = "119";
            this.seatComponent104.Size = new System.Drawing.Size(36, 51);
            this.seatComponent104.TabIndex = 601;
            this.seatComponent104.Tag = "Mic119";
            this.seatComponent104.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent104.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent104.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent102
            // 
            this.seatComponent102.AllowDrop = true;
            this.seatComponent102.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent102.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent102.Location = new System.Drawing.Point(182, 432);
            this.seatComponent102.Name = "seatComponent102";
            this.ribbon.SetPopupContextMenu(this.seatComponent102, this.menuMicSeat);
            this.seatComponent102.SeatNumber = "102";
            this.seatComponent102.Size = new System.Drawing.Size(36, 51);
            this.seatComponent102.TabIndex = 602;
            this.seatComponent102.Tag = "Mic102";
            this.seatComponent102.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent102.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent102.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent103
            // 
            this.seatComponent103.AllowDrop = true;
            this.seatComponent103.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent103.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent103.Location = new System.Drawing.Point(933, 434);
            this.seatComponent103.Name = "seatComponent103";
            this.ribbon.SetPopupContextMenu(this.seatComponent103, this.menuMicSeat);
            this.seatComponent103.SeatNumber = "88";
            this.seatComponent103.Size = new System.Drawing.Size(36, 51);
            this.seatComponent103.TabIndex = 601;
            this.seatComponent103.Tag = "Mic088";
            this.seatComponent103.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent103.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent103.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent68
            // 
            this.seatComponent68.AllowDrop = true;
            this.seatComponent68.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent68.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent68.Location = new System.Drawing.Point(1017, 377);
            this.seatComponent68.Name = "seatComponent68";
            this.ribbon.SetPopupContextMenu(this.seatComponent68, this.menuMicSeat);
            this.seatComponent68.SeatNumber = "85";
            this.seatComponent68.Size = new System.Drawing.Size(36, 51);
            this.seatComponent68.TabIndex = 596;
            this.seatComponent68.Tag = "Mic085";
            this.seatComponent68.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent68.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent68.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent69
            // 
            this.seatComponent69.AllowDrop = true;
            this.seatComponent69.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent69.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent69.Location = new System.Drawing.Point(138, 377);
            this.seatComponent69.Name = "seatComponent69";
            this.ribbon.SetPopupContextMenu(this.seatComponent69, this.menuMicSeat);
            this.seatComponent69.SeatNumber = "68";
            this.seatComponent69.Size = new System.Drawing.Size(36, 51);
            this.seatComponent69.TabIndex = 595;
            this.seatComponent69.Tag = "Mic068";
            this.seatComponent69.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent69.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent69.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent70
            // 
            this.seatComponent70.AllowDrop = true;
            this.seatComponent70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent70.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent70.Location = new System.Drawing.Point(417, 375);
            this.seatComponent70.Name = "seatComponent70";
            this.ribbon.SetPopupContextMenu(this.seatComponent70, this.menuMicSeat);
            this.seatComponent70.SeatNumber = "74";
            this.seatComponent70.Size = new System.Drawing.Size(36, 51);
            this.seatComponent70.TabIndex = 594;
            this.seatComponent70.Tag = "Mic074";
            this.seatComponent70.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent70.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent70.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent71
            // 
            this.seatComponent71.AllowDrop = true;
            this.seatComponent71.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent71.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent71.Location = new System.Drawing.Point(626, 375);
            this.seatComponent71.Name = "seatComponent71";
            this.ribbon.SetPopupContextMenu(this.seatComponent71, this.menuMicSeat);
            this.seatComponent71.SeatNumber = "78";
            this.seatComponent71.Size = new System.Drawing.Size(36, 51);
            this.seatComponent71.TabIndex = 593;
            this.seatComponent71.Tag = "Mic078";
            this.seatComponent71.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent71.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent71.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent72
            // 
            this.seatComponent72.AllowDrop = true;
            this.seatComponent72.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent72.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent72.Location = new System.Drawing.Point(841, 375);
            this.seatComponent72.Name = "seatComponent72";
            this.ribbon.SetPopupContextMenu(this.seatComponent72, this.menuMicSeat);
            this.seatComponent72.SeatNumber = "82";
            this.seatComponent72.Size = new System.Drawing.Size(36, 51);
            this.seatComponent72.TabIndex = 592;
            this.seatComponent72.Tag = "Mic082";
            this.seatComponent72.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent72.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent72.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent73
            // 
            this.seatComponent73.AllowDrop = true;
            this.seatComponent73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent73.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent73.Location = new System.Drawing.Point(374, 375);
            this.seatComponent73.Name = "seatComponent73";
            this.ribbon.SetPopupContextMenu(this.seatComponent73, this.menuMicSeat);
            this.seatComponent73.SeatNumber = "73";
            this.seatComponent73.Size = new System.Drawing.Size(36, 51);
            this.seatComponent73.TabIndex = 591;
            this.seatComponent73.Tag = "Mic073";
            this.seatComponent73.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent73.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent73.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent74
            // 
            this.seatComponent74.AllowDrop = true;
            this.seatComponent74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent74.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent74.Location = new System.Drawing.Point(582, 375);
            this.seatComponent74.Name = "seatComponent74";
            this.ribbon.SetPopupContextMenu(this.seatComponent74, this.menuMicSeat);
            this.seatComponent74.SeatNumber = "77";
            this.seatComponent74.Size = new System.Drawing.Size(36, 51);
            this.seatComponent74.TabIndex = 590;
            this.seatComponent74.Tag = "Mic077";
            this.seatComponent74.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent74.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent74.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent75
            // 
            this.seatComponent75.AllowDrop = true;
            this.seatComponent75.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent75.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent75.Location = new System.Drawing.Point(796, 375);
            this.seatComponent75.Name = "seatComponent75";
            this.ribbon.SetPopupContextMenu(this.seatComponent75, this.menuMicSeat);
            this.seatComponent75.SeatNumber = "81";
            this.seatComponent75.Size = new System.Drawing.Size(36, 51);
            this.seatComponent75.TabIndex = 589;
            this.seatComponent75.Tag = "Mic081";
            this.seatComponent75.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent75.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent75.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent76
            // 
            this.seatComponent76.AllowDrop = true;
            this.seatComponent76.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent76.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent76.Location = new System.Drawing.Point(331, 375);
            this.seatComponent76.Name = "seatComponent76";
            this.ribbon.SetPopupContextMenu(this.seatComponent76, this.menuMicSeat);
            this.seatComponent76.SeatNumber = "72";
            this.seatComponent76.Size = new System.Drawing.Size(36, 51);
            this.seatComponent76.TabIndex = 587;
            this.seatComponent76.Tag = "Mic072";
            this.seatComponent76.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent76.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent76.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent77
            // 
            this.seatComponent77.AllowDrop = true;
            this.seatComponent77.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent77.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent77.Location = new System.Drawing.Point(538, 375);
            this.seatComponent77.Name = "seatComponent77";
            this.ribbon.SetPopupContextMenu(this.seatComponent77, this.menuMicSeat);
            this.seatComponent77.SeatNumber = "76";
            this.seatComponent77.Size = new System.Drawing.Size(36, 51);
            this.seatComponent77.TabIndex = 588;
            this.seatComponent77.Tag = "Mic076";
            this.seatComponent77.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent77.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent77.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent78
            // 
            this.seatComponent78.AllowDrop = true;
            this.seatComponent78.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent78.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent78.Location = new System.Drawing.Point(751, 375);
            this.seatComponent78.Name = "seatComponent78";
            this.ribbon.SetPopupContextMenu(this.seatComponent78, this.menuMicSeat);
            this.seatComponent78.SeatNumber = "80";
            this.seatComponent78.Size = new System.Drawing.Size(36, 51);
            this.seatComponent78.TabIndex = 586;
            this.seatComponent78.Tag = "Mic080";
            this.seatComponent78.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent78.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent78.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent79
            // 
            this.seatComponent79.AllowDrop = true;
            this.seatComponent79.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent79.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent79.Location = new System.Drawing.Point(288, 375);
            this.seatComponent79.Name = "seatComponent79";
            this.ribbon.SetPopupContextMenu(this.seatComponent79, this.menuMicSeat);
            this.seatComponent79.SeatNumber = "71";
            this.seatComponent79.Size = new System.Drawing.Size(36, 51);
            this.seatComponent79.TabIndex = 585;
            this.seatComponent79.Tag = "Mic071";
            this.seatComponent79.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent79.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent79.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent80
            // 
            this.seatComponent80.AllowDrop = true;
            this.seatComponent80.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent80.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent80.Location = new System.Drawing.Point(494, 375);
            this.seatComponent80.Name = "seatComponent80";
            this.ribbon.SetPopupContextMenu(this.seatComponent80, this.menuMicSeat);
            this.seatComponent80.SeatNumber = "75";
            this.seatComponent80.Size = new System.Drawing.Size(36, 51);
            this.seatComponent80.TabIndex = 584;
            this.seatComponent80.Tag = "Mic075";
            this.seatComponent80.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent80.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent80.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent81
            // 
            this.seatComponent81.AllowDrop = true;
            this.seatComponent81.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent81.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent81.Location = new System.Drawing.Point(706, 375);
            this.seatComponent81.Name = "seatComponent81";
            this.ribbon.SetPopupContextMenu(this.seatComponent81, this.menuMicSeat);
            this.seatComponent81.SeatNumber = "79";
            this.seatComponent81.Size = new System.Drawing.Size(36, 51);
            this.seatComponent81.TabIndex = 583;
            this.seatComponent81.Tag = "Mic079";
            this.seatComponent81.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent81.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent81.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent82
            // 
            this.seatComponent82.AllowDrop = true;
            this.seatComponent82.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent82.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent82.Location = new System.Drawing.Point(226, 375);
            this.seatComponent82.Name = "seatComponent82";
            this.ribbon.SetPopupContextMenu(this.seatComponent82, this.menuMicSeat);
            this.seatComponent82.SeatNumber = "70";
            this.seatComponent82.Size = new System.Drawing.Size(36, 51);
            this.seatComponent82.TabIndex = 582;
            this.seatComponent82.Tag = "Mic070";
            this.seatComponent82.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent82.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent82.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent83
            // 
            this.seatComponent83.AllowDrop = true;
            this.seatComponent83.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent83.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent83.Location = new System.Drawing.Point(975, 377);
            this.seatComponent83.Name = "seatComponent83";
            this.ribbon.SetPopupContextMenu(this.seatComponent83, this.menuMicSeat);
            this.seatComponent83.SeatNumber = "84";
            this.seatComponent83.Size = new System.Drawing.Size(36, 51);
            this.seatComponent83.TabIndex = 581;
            this.seatComponent83.Tag = "Mic084";
            this.seatComponent83.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent83.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent83.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent84
            // 
            this.seatComponent84.AllowDrop = true;
            this.seatComponent84.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent84.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent84.Location = new System.Drawing.Point(182, 375);
            this.seatComponent84.Name = "seatComponent84";
            this.ribbon.SetPopupContextMenu(this.seatComponent84, this.menuMicSeat);
            this.seatComponent84.SeatNumber = "69";
            this.seatComponent84.Size = new System.Drawing.Size(36, 51);
            this.seatComponent84.TabIndex = 580;
            this.seatComponent84.Tag = "Mic069";
            this.seatComponent84.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent84.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent84.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent85
            // 
            this.seatComponent85.AllowDrop = true;
            this.seatComponent85.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent85.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent85.Location = new System.Drawing.Point(933, 377);
            this.seatComponent85.Name = "seatComponent85";
            this.ribbon.SetPopupContextMenu(this.seatComponent85, this.menuMicSeat);
            this.seatComponent85.SeatNumber = "83";
            this.seatComponent85.Size = new System.Drawing.Size(36, 51);
            this.seatComponent85.TabIndex = 579;
            this.seatComponent85.Tag = "Mic083";
            this.seatComponent85.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent85.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent85.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent67
            // 
            this.seatComponent67.AllowDrop = true;
            this.seatComponent67.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent67.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent67.Location = new System.Drawing.Point(1017, 321);
            this.seatComponent67.Name = "seatComponent67";
            this.ribbon.SetPopupContextMenu(this.seatComponent67, this.menuMicSeat);
            this.seatComponent67.SeatNumber = "50";
            this.seatComponent67.Size = new System.Drawing.Size(36, 51);
            this.seatComponent67.TabIndex = 574;
            this.seatComponent67.Tag = "Mic050";
            this.seatComponent67.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent67.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent67.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent66
            // 
            this.seatComponent66.AllowDrop = true;
            this.seatComponent66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent66.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent66.Location = new System.Drawing.Point(138, 321);
            this.seatComponent66.Name = "seatComponent66";
            this.ribbon.SetPopupContextMenu(this.seatComponent66, this.menuMicSeat);
            this.seatComponent66.SeatNumber = "67";
            this.seatComponent66.Size = new System.Drawing.Size(36, 51);
            this.seatComponent66.TabIndex = 573;
            this.seatComponent66.Tag = "Mic067";
            this.seatComponent66.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent66.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent66.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent65
            // 
            this.seatComponent65.AllowDrop = true;
            this.seatComponent65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent65.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent65.Location = new System.Drawing.Point(417, 318);
            this.seatComponent65.Name = "seatComponent65";
            this.ribbon.SetPopupContextMenu(this.seatComponent65, this.menuMicSeat);
            this.seatComponent65.SeatNumber = "61";
            this.seatComponent65.Size = new System.Drawing.Size(36, 51);
            this.seatComponent65.TabIndex = 572;
            this.seatComponent65.Tag = "Mic061";
            this.seatComponent65.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent65.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent65.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent49
            // 
            this.seatComponent49.AllowDrop = true;
            this.seatComponent49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent49.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent49.Location = new System.Drawing.Point(417, 261);
            this.seatComponent49.Name = "seatComponent49";
            this.ribbon.SetPopupContextMenu(this.seatComponent49, this.menuMicSeat);
            this.seatComponent49.SeatNumber = "39";
            this.seatComponent49.Size = new System.Drawing.Size(36, 51);
            this.seatComponent49.TabIndex = 572;
            this.seatComponent49.Tag = "Mic039";
            this.seatComponent49.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent49.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent49.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent64
            // 
            this.seatComponent64.AllowDrop = true;
            this.seatComponent64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent64.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent64.Location = new System.Drawing.Point(626, 318);
            this.seatComponent64.Name = "seatComponent64";
            this.ribbon.SetPopupContextMenu(this.seatComponent64, this.menuMicSeat);
            this.seatComponent64.SeatNumber = "57";
            this.seatComponent64.Size = new System.Drawing.Size(36, 51);
            this.seatComponent64.TabIndex = 572;
            this.seatComponent64.Tag = "Mic057";
            this.seatComponent64.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent64.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent64.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent48
            // 
            this.seatComponent48.AllowDrop = true;
            this.seatComponent48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent48.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent48.Location = new System.Drawing.Point(626, 261);
            this.seatComponent48.Name = "seatComponent48";
            this.ribbon.SetPopupContextMenu(this.seatComponent48, this.menuMicSeat);
            this.seatComponent48.SeatNumber = "43";
            this.seatComponent48.Size = new System.Drawing.Size(36, 51);
            this.seatComponent48.TabIndex = 572;
            this.seatComponent48.Tag = "Mic043";
            this.seatComponent48.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent48.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent48.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent63
            // 
            this.seatComponent63.AllowDrop = true;
            this.seatComponent63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent63.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent63.Location = new System.Drawing.Point(841, 318);
            this.seatComponent63.Name = "seatComponent63";
            this.ribbon.SetPopupContextMenu(this.seatComponent63, this.menuMicSeat);
            this.seatComponent63.SeatNumber = "53";
            this.seatComponent63.Size = new System.Drawing.Size(36, 51);
            this.seatComponent63.TabIndex = 572;
            this.seatComponent63.Tag = "Mic053";
            this.seatComponent63.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent63.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent63.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent47
            // 
            this.seatComponent47.AllowDrop = true;
            this.seatComponent47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent47.Location = new System.Drawing.Point(841, 261);
            this.seatComponent47.Name = "seatComponent47";
            this.ribbon.SetPopupContextMenu(this.seatComponent47, this.menuMicSeat);
            this.seatComponent47.SeatNumber = "47";
            this.seatComponent47.Size = new System.Drawing.Size(36, 51);
            this.seatComponent47.TabIndex = 572;
            this.seatComponent47.Tag = "Mic047";
            this.seatComponent47.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent47.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent47.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent62
            // 
            this.seatComponent62.AllowDrop = true;
            this.seatComponent62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent62.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent62.Location = new System.Drawing.Point(374, 318);
            this.seatComponent62.Name = "seatComponent62";
            this.ribbon.SetPopupContextMenu(this.seatComponent62, this.menuMicSeat);
            this.seatComponent62.SeatNumber = "62";
            this.seatComponent62.Size = new System.Drawing.Size(36, 51);
            this.seatComponent62.TabIndex = 571;
            this.seatComponent62.Tag = "Mic062";
            this.seatComponent62.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent62.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent62.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent46
            // 
            this.seatComponent46.AllowDrop = true;
            this.seatComponent46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent46.Location = new System.Drawing.Point(374, 261);
            this.seatComponent46.Name = "seatComponent46";
            this.ribbon.SetPopupContextMenu(this.seatComponent46, this.menuMicSeat);
            this.seatComponent46.SeatNumber = "38";
            this.seatComponent46.Size = new System.Drawing.Size(36, 51);
            this.seatComponent46.TabIndex = 571;
            this.seatComponent46.Tag = "Mic038";
            this.seatComponent46.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent46.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent46.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent61
            // 
            this.seatComponent61.AllowDrop = true;
            this.seatComponent61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent61.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent61.Location = new System.Drawing.Point(582, 318);
            this.seatComponent61.Name = "seatComponent61";
            this.ribbon.SetPopupContextMenu(this.seatComponent61, this.menuMicSeat);
            this.seatComponent61.SeatNumber = "58";
            this.seatComponent61.Size = new System.Drawing.Size(36, 51);
            this.seatComponent61.TabIndex = 571;
            this.seatComponent61.Tag = "Mic058";
            this.seatComponent61.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent61.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent61.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent45
            // 
            this.seatComponent45.AllowDrop = true;
            this.seatComponent45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent45.Location = new System.Drawing.Point(582, 261);
            this.seatComponent45.Name = "seatComponent45";
            this.ribbon.SetPopupContextMenu(this.seatComponent45, this.menuMicSeat);
            this.seatComponent45.SeatNumber = "42";
            this.seatComponent45.Size = new System.Drawing.Size(36, 51);
            this.seatComponent45.TabIndex = 571;
            this.seatComponent45.Tag = "Mic042";
            this.seatComponent45.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent45.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent45.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent60
            // 
            this.seatComponent60.AllowDrop = true;
            this.seatComponent60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent60.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent60.Location = new System.Drawing.Point(796, 318);
            this.seatComponent60.Name = "seatComponent60";
            this.ribbon.SetPopupContextMenu(this.seatComponent60, this.menuMicSeat);
            this.seatComponent60.SeatNumber = "54";
            this.seatComponent60.Size = new System.Drawing.Size(36, 51);
            this.seatComponent60.TabIndex = 571;
            this.seatComponent60.Tag = "Mic054";
            this.seatComponent60.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent60.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent60.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent44
            // 
            this.seatComponent44.AllowDrop = true;
            this.seatComponent44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent44.Location = new System.Drawing.Point(796, 261);
            this.seatComponent44.Name = "seatComponent44";
            this.ribbon.SetPopupContextMenu(this.seatComponent44, this.menuMicSeat);
            this.seatComponent44.SeatNumber = "46";
            this.seatComponent44.Size = new System.Drawing.Size(36, 51);
            this.seatComponent44.TabIndex = 571;
            this.seatComponent44.Tag = "Mic046";
            this.seatComponent44.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent44.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent44.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent59
            // 
            this.seatComponent59.AllowDrop = true;
            this.seatComponent59.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent59.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent59.Location = new System.Drawing.Point(331, 318);
            this.seatComponent59.Name = "seatComponent59";
            this.ribbon.SetPopupContextMenu(this.seatComponent59, this.menuMicSeat);
            this.seatComponent59.SeatNumber = "63";
            this.seatComponent59.Size = new System.Drawing.Size(36, 51);
            this.seatComponent59.TabIndex = 570;
            this.seatComponent59.Tag = "Mic063";
            this.seatComponent59.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent59.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent59.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent43
            // 
            this.seatComponent43.AllowDrop = true;
            this.seatComponent43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent43.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent43.Location = new System.Drawing.Point(331, 261);
            this.seatComponent43.Name = "seatComponent43";
            this.ribbon.SetPopupContextMenu(this.seatComponent43, this.menuMicSeat);
            this.seatComponent43.SeatNumber = "37";
            this.seatComponent43.Size = new System.Drawing.Size(36, 51);
            this.seatComponent43.TabIndex = 570;
            this.seatComponent43.Tag = "Mic037";
            this.seatComponent43.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent43.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent43.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent58
            // 
            this.seatComponent58.AllowDrop = true;
            this.seatComponent58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent58.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent58.Location = new System.Drawing.Point(538, 318);
            this.seatComponent58.Name = "seatComponent58";
            this.ribbon.SetPopupContextMenu(this.seatComponent58, this.menuMicSeat);
            this.seatComponent58.SeatNumber = "59";
            this.seatComponent58.Size = new System.Drawing.Size(36, 51);
            this.seatComponent58.TabIndex = 570;
            this.seatComponent58.Tag = "Mic059";
            this.seatComponent58.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent58.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent58.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent42
            // 
            this.seatComponent42.AllowDrop = true;
            this.seatComponent42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent42.Location = new System.Drawing.Point(538, 261);
            this.seatComponent42.Name = "seatComponent42";
            this.ribbon.SetPopupContextMenu(this.seatComponent42, this.menuMicSeat);
            this.seatComponent42.SeatNumber = "41";
            this.seatComponent42.Size = new System.Drawing.Size(36, 51);
            this.seatComponent42.TabIndex = 570;
            this.seatComponent42.Tag = "Mic041";
            this.seatComponent42.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent42.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent42.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent57
            // 
            this.seatComponent57.AllowDrop = true;
            this.seatComponent57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent57.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent57.Location = new System.Drawing.Point(751, 318);
            this.seatComponent57.Name = "seatComponent57";
            this.ribbon.SetPopupContextMenu(this.seatComponent57, this.menuMicSeat);
            this.seatComponent57.SeatNumber = "55";
            this.seatComponent57.Size = new System.Drawing.Size(36, 51);
            this.seatComponent57.TabIndex = 570;
            this.seatComponent57.Tag = "Mic055";
            this.seatComponent57.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent57.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent57.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent41
            // 
            this.seatComponent41.AllowDrop = true;
            this.seatComponent41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent41.Location = new System.Drawing.Point(751, 261);
            this.seatComponent41.Name = "seatComponent41";
            this.ribbon.SetPopupContextMenu(this.seatComponent41, this.menuMicSeat);
            this.seatComponent41.SeatNumber = "45";
            this.seatComponent41.Size = new System.Drawing.Size(36, 51);
            this.seatComponent41.TabIndex = 570;
            this.seatComponent41.Tag = "Mic045";
            this.seatComponent41.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent41.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent41.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent56
            // 
            this.seatComponent56.AllowDrop = true;
            this.seatComponent56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent56.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent56.Location = new System.Drawing.Point(288, 318);
            this.seatComponent56.Name = "seatComponent56";
            this.ribbon.SetPopupContextMenu(this.seatComponent56, this.menuMicSeat);
            this.seatComponent56.SeatNumber = "64";
            this.seatComponent56.Size = new System.Drawing.Size(36, 51);
            this.seatComponent56.TabIndex = 569;
            this.seatComponent56.Tag = "Mic064";
            this.seatComponent56.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent56.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent56.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent40
            // 
            this.seatComponent40.AllowDrop = true;
            this.seatComponent40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent40.Location = new System.Drawing.Point(288, 261);
            this.seatComponent40.Name = "seatComponent40";
            this.ribbon.SetPopupContextMenu(this.seatComponent40, this.menuMicSeat);
            this.seatComponent40.SeatNumber = "36";
            this.seatComponent40.Size = new System.Drawing.Size(36, 51);
            this.seatComponent40.TabIndex = 569;
            this.seatComponent40.Tag = "Mic036";
            this.seatComponent40.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent40.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent40.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent55
            // 
            this.seatComponent55.AllowDrop = true;
            this.seatComponent55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent55.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent55.Location = new System.Drawing.Point(494, 318);
            this.seatComponent55.Name = "seatComponent55";
            this.ribbon.SetPopupContextMenu(this.seatComponent55, this.menuMicSeat);
            this.seatComponent55.SeatNumber = "60";
            this.seatComponent55.Size = new System.Drawing.Size(36, 51);
            this.seatComponent55.TabIndex = 569;
            this.seatComponent55.Tag = "Mic060";
            this.seatComponent55.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent55.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent55.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent39
            // 
            this.seatComponent39.AllowDrop = true;
            this.seatComponent39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent39.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent39.Location = new System.Drawing.Point(494, 261);
            this.seatComponent39.Name = "seatComponent39";
            this.ribbon.SetPopupContextMenu(this.seatComponent39, this.menuMicSeat);
            this.seatComponent39.SeatNumber = "40";
            this.seatComponent39.Size = new System.Drawing.Size(36, 51);
            this.seatComponent39.TabIndex = 569;
            this.seatComponent39.Tag = "Mic040";
            this.seatComponent39.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent39.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent39.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent54
            // 
            this.seatComponent54.AllowDrop = true;
            this.seatComponent54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent54.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent54.Location = new System.Drawing.Point(706, 318);
            this.seatComponent54.Name = "seatComponent54";
            this.ribbon.SetPopupContextMenu(this.seatComponent54, this.menuMicSeat);
            this.seatComponent54.SeatNumber = "56";
            this.seatComponent54.Size = new System.Drawing.Size(36, 51);
            this.seatComponent54.TabIndex = 569;
            this.seatComponent54.Tag = "Mic056";
            this.seatComponent54.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent54.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent54.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent38
            // 
            this.seatComponent38.AllowDrop = true;
            this.seatComponent38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent38.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent38.Location = new System.Drawing.Point(706, 261);
            this.seatComponent38.Name = "seatComponent38";
            this.ribbon.SetPopupContextMenu(this.seatComponent38, this.menuMicSeat);
            this.seatComponent38.SeatNumber = "44";
            this.seatComponent38.Size = new System.Drawing.Size(36, 51);
            this.seatComponent38.TabIndex = 569;
            this.seatComponent38.Tag = "Mic044";
            this.seatComponent38.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent38.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent38.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent53
            // 
            this.seatComponent53.AllowDrop = true;
            this.seatComponent53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent53.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent53.Location = new System.Drawing.Point(226, 318);
            this.seatComponent53.Name = "seatComponent53";
            this.ribbon.SetPopupContextMenu(this.seatComponent53, this.menuMicSeat);
            this.seatComponent53.SeatNumber = "65";
            this.seatComponent53.Size = new System.Drawing.Size(36, 51);
            this.seatComponent53.TabIndex = 568;
            this.seatComponent53.Tag = "Mic065";
            this.seatComponent53.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent53.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent53.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent37
            // 
            this.seatComponent37.AllowDrop = true;
            this.seatComponent37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent37.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent37.Location = new System.Drawing.Point(226, 261);
            this.seatComponent37.Name = "seatComponent37";
            this.ribbon.SetPopupContextMenu(this.seatComponent37, this.menuMicSeat);
            this.seatComponent37.SeatNumber = "35";
            this.seatComponent37.Size = new System.Drawing.Size(36, 51);
            this.seatComponent37.TabIndex = 568;
            this.seatComponent37.Tag = "Mic035";
            this.seatComponent37.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent37.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent37.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent52
            // 
            this.seatComponent52.AllowDrop = true;
            this.seatComponent52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent52.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent52.Location = new System.Drawing.Point(975, 321);
            this.seatComponent52.Name = "seatComponent52";
            this.ribbon.SetPopupContextMenu(this.seatComponent52, this.menuMicSeat);
            this.seatComponent52.SeatNumber = "51";
            this.seatComponent52.Size = new System.Drawing.Size(36, 51);
            this.seatComponent52.TabIndex = 568;
            this.seatComponent52.Tag = "Mic051";
            this.seatComponent52.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent52.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent52.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent36
            // 
            this.seatComponent36.AllowDrop = true;
            this.seatComponent36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent36.Location = new System.Drawing.Point(975, 260);
            this.seatComponent36.Name = "seatComponent36";
            this.ribbon.SetPopupContextMenu(this.seatComponent36, this.menuMicSeat);
            this.seatComponent36.SeatNumber = "49";
            this.seatComponent36.Size = new System.Drawing.Size(36, 51);
            this.seatComponent36.TabIndex = 568;
            this.seatComponent36.Tag = "Mic049";
            this.seatComponent36.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent36.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent36.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent51
            // 
            this.seatComponent51.AllowDrop = true;
            this.seatComponent51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent51.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent51.Location = new System.Drawing.Point(182, 318);
            this.seatComponent51.Name = "seatComponent51";
            this.ribbon.SetPopupContextMenu(this.seatComponent51, this.menuMicSeat);
            this.seatComponent51.SeatNumber = "66";
            this.seatComponent51.Size = new System.Drawing.Size(36, 51);
            this.seatComponent51.TabIndex = 567;
            this.seatComponent51.Tag = "Mic066";
            this.seatComponent51.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent51.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent51.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent50
            // 
            this.seatComponent50.AllowDrop = true;
            this.seatComponent50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent50.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent50.Location = new System.Drawing.Point(933, 321);
            this.seatComponent50.Name = "seatComponent50";
            this.ribbon.SetPopupContextMenu(this.seatComponent50, this.menuMicSeat);
            this.seatComponent50.SeatNumber = "52";
            this.seatComponent50.Size = new System.Drawing.Size(36, 51);
            this.seatComponent50.TabIndex = 567;
            this.seatComponent50.Tag = "Mic052";
            this.seatComponent50.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent50.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent50.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent35
            // 
            this.seatComponent35.AllowDrop = true;
            this.seatComponent35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent35.Location = new System.Drawing.Point(182, 261);
            this.seatComponent35.Name = "seatComponent35";
            this.ribbon.SetPopupContextMenu(this.seatComponent35, this.menuMicSeat);
            this.seatComponent35.SeatNumber = "34";
            this.seatComponent35.Size = new System.Drawing.Size(36, 51);
            this.seatComponent35.TabIndex = 567;
            this.seatComponent35.Tag = "Mic034";
            this.seatComponent35.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent35.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent35.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // seatComponent34
            // 
            this.seatComponent34.AllowDrop = true;
            this.seatComponent34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.seatComponent34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.seatComponent34.Location = new System.Drawing.Point(933, 260);
            this.seatComponent34.Name = "seatComponent34";
            this.ribbon.SetPopupContextMenu(this.seatComponent34, this.menuMicSeat);
            this.seatComponent34.SeatNumber = "48";
            this.seatComponent34.Size = new System.Drawing.Size(36, 51);
            this.seatComponent34.TabIndex = 567;
            this.seatComponent34.Tag = "Mic048";
            this.seatComponent34.MyDragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.seatComponent34.MyMouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.seatComponent34.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            // 
            // clientPanel
            // 
            this.clientPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.clientPanel.Controls.Add(this.panelControl7);
            this.clientPanel.Controls.Add(this.panelControl1);
            this.clientPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientPanel.Location = new System.Drawing.Point(0, 114);
            this.clientPanel.Name = "clientPanel";
            this.clientPanel.Size = new System.Drawing.Size(1884, 863);
            this.clientPanel.TabIndex = 2;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.navBarControl1);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(264, 863);
            this.panelControl7.TabIndex = 2;
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.navBarGroup1;
            this.navBarControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.navBarControl1.ContentButtonHint = null;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.itmAgenda,
            this.itmDelegates,
            this.itmAgendaItems,
            this.itmRegistration,
            this.itmQuestions,
            this.itmReport,
            this.itmStatements,
            this.itmProtocol,
            this.navBarItem10,
            this.navBarItem11,
            this.navBarItem12});
            this.navBarControl1.LargeImages = this.NavBarImages;
            this.navBarControl1.LinkInterval = 30;
            this.navBarControl1.Location = new System.Drawing.Point(2, 2);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 260;
            this.navBarControl1.Size = new System.Drawing.Size(260, 859);
            this.navBarControl1.SmallImages = this.NavBarImages;
            this.navBarControl1.TabIndex = 1;
            this.navBarControl1.Text = "navBarControl1";
            this.navBarControl1.View = new DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("Caramel");
            this.navBarControl1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarControl1_LinkClicked);
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.navBarGroup1.Appearance.Options.UseFont = true;
            this.navBarGroup1.AppearanceHotTracked.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.navBarGroup1.AppearanceHotTracked.Options.UseFont = true;
            this.navBarGroup1.Caption = "Сессия голосования";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.GroupClientHeight = 400;
            this.navBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // itmAgenda
            // 
            this.itmAgenda.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmAgenda.Appearance.Options.UseFont = true;
            this.itmAgenda.AppearanceHotTracked.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmAgenda.AppearanceHotTracked.Options.UseFont = true;
            this.itmAgenda.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmAgenda.AppearancePressed.Options.UseFont = true;
            this.itmAgenda.Caption = "Настройка сессии";
            this.itmAgenda.Hint = "Информация и настройки, относящиеся к данной сессии";
            this.itmAgenda.ImageOptions.LargeImageIndex = 0;
            this.itmAgenda.ImageOptions.SmallImageIndex = 0;
            this.itmAgenda.Name = "itmAgenda";
            // 
            // itmDelegates
            // 
            this.itmDelegates.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmDelegates.Appearance.Options.UseFont = true;
            this.itmDelegates.AppearanceHotTracked.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.itmDelegates.AppearanceHotTracked.Options.UseFont = true;
            this.itmDelegates.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmDelegates.AppearancePressed.Options.UseFont = true;
            this.itmDelegates.Caption = "Депутаты сессии";
            this.itmDelegates.Hint = "Список депутатов и приглашенных сессии";
            this.itmDelegates.ImageOptions.LargeImageIndex = 1;
            this.itmDelegates.ImageOptions.SmallImageIndex = 1;
            this.itmDelegates.Name = "itmDelegates";
            // 
            // itmAgendaItems
            // 
            this.itmAgendaItems.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmAgendaItems.Appearance.Options.UseFont = true;
            this.itmAgendaItems.AppearanceHotTracked.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.itmAgendaItems.AppearanceHotTracked.Options.UseFont = true;
            this.itmAgendaItems.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmAgendaItems.AppearancePressed.Options.UseFont = true;
            this.itmAgendaItems.Caption = "Повестка дня";
            this.itmAgendaItems.Hint = "Список пунктов повестки дня данной сессии";
            this.itmAgendaItems.ImageOptions.LargeImageIndex = 5;
            this.itmAgendaItems.ImageOptions.SmallImageIndex = 5;
            this.itmAgendaItems.Name = "itmAgendaItems";
            // 
            // itmRegistration
            // 
            this.itmRegistration.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmRegistration.Appearance.Options.UseFont = true;
            this.itmRegistration.AppearanceHotTracked.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.itmRegistration.AppearanceHotTracked.Options.UseFont = true;
            this.itmRegistration.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmRegistration.AppearancePressed.Options.UseFont = true;
            this.itmRegistration.Caption = "Управление залом";
            this.itmRegistration.Hint = "рассадка депутатов, идентификация карточек, определение кворума";
            this.itmRegistration.ImageOptions.LargeImageIndex = 3;
            this.itmRegistration.ImageOptions.SmallImageIndex = 3;
            this.itmRegistration.Name = "itmRegistration";
            // 
            // itmQuestions
            // 
            this.itmQuestions.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmQuestions.Appearance.Options.UseFont = true;
            this.itmQuestions.AppearanceHotTracked.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.itmQuestions.AppearanceHotTracked.Options.UseFont = true;
            this.itmQuestions.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmQuestions.AppearancePressed.Options.UseFont = true;
            this.itmQuestions.Caption = "Вопросы голосования";
            this.itmQuestions.Hint = "Добавление вопросов для голосования";
            this.itmQuestions.ImageOptions.LargeImageIndex = 4;
            this.itmQuestions.ImageOptions.SmallImageIndex = 4;
            this.itmQuestions.Name = "itmQuestions";
            // 
            // itmReport
            // 
            this.itmReport.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmReport.Appearance.Options.UseFont = true;
            this.itmReport.AppearanceHotTracked.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.itmReport.AppearanceHotTracked.Options.UseFont = true;
            this.itmReport.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmReport.AppearancePressed.Options.UseFont = true;
            this.itmReport.Caption = "Отчет по голосованию";
            this.itmReport.ImageOptions.LargeImageIndex = 5;
            this.itmReport.ImageOptions.SmallImageIndex = 5;
            this.itmReport.Name = "itmReport";
            // 
            // itmStatements
            // 
            this.itmStatements.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmStatements.Appearance.Options.UseFont = true;
            this.itmStatements.AppearanceHotTracked.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.itmStatements.AppearanceHotTracked.Options.UseFont = true;
            this.itmStatements.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmStatements.AppearancePressed.Options.UseFont = true;
            this.itmStatements.Caption = "Выступления";
            this.itmStatements.ImageOptions.LargeImageIndex = 6;
            this.itmStatements.Name = "itmStatements";
            // 
            // itmProtocol
            // 
            this.itmProtocol.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmProtocol.Appearance.Options.UseFont = true;
            this.itmProtocol.AppearanceHotTracked.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmProtocol.AppearanceHotTracked.Options.UseFont = true;
            this.itmProtocol.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itmProtocol.AppearancePressed.Options.UseFont = true;
            this.itmProtocol.Caption = "Печать протокола";
            this.itmProtocol.ImageOptions.LargeImageIndex = 8;
            this.itmProtocol.Name = "itmProtocol";
            // 
            // navBarItem10
            // 
            this.navBarItem10.Caption = "navBarItem10";
            this.navBarItem10.Name = "navBarItem10";
            // 
            // navBarItem11
            // 
            this.navBarItem11.Caption = "navBarItem11";
            this.navBarItem11.Name = "navBarItem11";
            // 
            // navBarItem12
            // 
            this.navBarItem12.Caption = "navBarItem12";
            this.navBarItem12.Name = "navBarItem12";
            // 
            // NavBarImages
            // 
            this.NavBarImages.ImageSize = new System.Drawing.Size(36, 36);
            this.NavBarImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("NavBarImages.ImageStream")));
            this.NavBarImages.Images.SetKeyName(7, "DocExcel.png");
            this.NavBarImages.Images.SetKeyName(8, "HP PSC 2200 Series.png");
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl1.Controls.Add(this.MainTabControl);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl1.Location = new System.Drawing.Point(264, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1620, 863);
            this.panelControl1.TabIndex = 1;
            // 
            // MainTabControl
            // 
            this.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTabControl.Location = new System.Drawing.Point(2, 2);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedTabPage = this.pgSession;
            this.MainTabControl.Size = new System.Drawing.Size(1616, 859);
            this.MainTabControl.TabIndex = 1;
            this.MainTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pgSession,
            this.pgDelegates,
            this.pgSelQuestions,
            this.pgQuorumGraphic,
            this.pgQuestions,
            this.pgStatements});
            this.MainTabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // pgSession
            // 
            this.pgSession.Controls.Add(this.groupControl2);
            this.pgSession.Controls.Add(this.grpSesCharacteristics);
            this.pgSession.Controls.Add(this.groupControl9);
            this.pgSession.Name = "pgSession";
            this.pgSession.PageVisible = false;
            this.pgSession.Size = new System.Drawing.Size(1609, 831);
            this.pgSession.Text = "Сессия";
            this.pgSession.Leave += new System.EventHandler(this.pgSession_Leave);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.richContent);
            this.groupControl2.Location = new System.Drawing.Point(572, 346);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(691, 408);
            this.groupControl2.TabIndex = 18;
            this.groupControl2.Text = "Повестка дня";
            // 
            // richContent
            // 
            this.richContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richContent.Location = new System.Drawing.Point(2, 22);
            this.richContent.Name = "richContent";
            this.richContent.Size = new System.Drawing.Size(687, 384);
            this.richContent.TabIndex = 29;
            this.richContent.Text = "";
            // 
            // grpSesCharacteristics
            // 
            this.grpSesCharacteristics.Controls.Add(this.labelControl38);
            this.grpSesCharacteristics.Controls.Add(this.labelControl37);
            this.grpSesCharacteristics.Controls.Add(this.labelControl36);
            this.grpSesCharacteristics.Controls.Add(this.txtSpeechTime);
            this.grpSesCharacteristics.Controls.Add(this.labelControl35);
            this.grpSesCharacteristics.Controls.Add(this.txtCoReportTime);
            this.grpSesCharacteristics.Controls.Add(this.labelControl7);
            this.grpSesCharacteristics.Controls.Add(this.txtReportTime);
            this.grpSesCharacteristics.Controls.Add(this.labelControl34);
            this.grpSesCharacteristics.Controls.Add(this.labelControl24);
            this.grpSesCharacteristics.Controls.Add(this.txtFinishDate);
            this.grpSesCharacteristics.Controls.Add(this.labelControl21);
            this.grpSesCharacteristics.Controls.Add(this.txtVoteTime);
            this.grpSesCharacteristics.Controls.Add(this.labelControl145);
            this.grpSesCharacteristics.Controls.Add(this.labelControl3);
            this.grpSesCharacteristics.Controls.Add(this.txtCode);
            this.grpSesCharacteristics.Controls.Add(this.labelControl17);
            this.grpSesCharacteristics.Controls.Add(this.txtRegTime);
            this.grpSesCharacteristics.Controls.Add(this.labelControl5);
            this.grpSesCharacteristics.Controls.Add(this.labelControl2);
            this.grpSesCharacteristics.Controls.Add(this.txtStartDate);
            this.grpSesCharacteristics.Controls.Add(this.labelControl1);
            this.grpSesCharacteristics.Controls.Add(this.txtCaption);
            this.grpSesCharacteristics.Location = new System.Drawing.Point(67, 15);
            this.grpSesCharacteristics.Name = "grpSesCharacteristics";
            this.grpSesCharacteristics.Size = new System.Drawing.Size(1196, 275);
            this.grpSesCharacteristics.TabIndex = 28;
            this.grpSesCharacteristics.Text = "Настройки сессии";
            // 
            // labelControl38
            // 
            this.labelControl38.Enabled = false;
            this.labelControl38.Location = new System.Drawing.Point(1002, 148);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(22, 13);
            this.labelControl38.TabIndex = 57;
            this.labelControl38.Text = "мин.";
            // 
            // labelControl37
            // 
            this.labelControl37.Enabled = false;
            this.labelControl37.Location = new System.Drawing.Point(1002, 102);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(22, 13);
            this.labelControl37.TabIndex = 56;
            this.labelControl37.Text = "мин.";
            // 
            // labelControl36
            // 
            this.labelControl36.Enabled = false;
            this.labelControl36.Location = new System.Drawing.Point(1002, 51);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(22, 13);
            this.labelControl36.TabIndex = 55;
            this.labelControl36.Text = "мин.";
            // 
            // txtSpeechTime
            // 
            this.txtSpeechTime.Enabled = false;
            this.txtSpeechTime.Location = new System.Drawing.Point(923, 142);
            this.txtSpeechTime.Name = "txtSpeechTime";
            this.txtSpeechTime.Size = new System.Drawing.Size(73, 20);
            this.txtSpeechTime.TabIndex = 54;
            // 
            // labelControl35
            // 
            this.labelControl35.Enabled = false;
            this.labelControl35.Location = new System.Drawing.Point(812, 145);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(88, 13);
            this.labelControl35.TabIndex = 53;
            this.labelControl35.Text = "Время на прении:";
            // 
            // txtCoReportTime
            // 
            this.txtCoReportTime.Enabled = false;
            this.txtCoReportTime.Location = new System.Drawing.Point(923, 96);
            this.txtCoReportTime.Name = "txtCoReportTime";
            this.txtCoReportTime.Size = new System.Drawing.Size(73, 20);
            this.txtCoReportTime.TabIndex = 51;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Enabled = false;
            this.labelControl7.Location = new System.Drawing.Point(799, 99);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(102, 13);
            this.labelControl7.TabIndex = 50;
            this.labelControl7.Text = "Время на содоклад:";
            // 
            // txtReportTime
            // 
            this.txtReportTime.Enabled = false;
            this.txtReportTime.Location = new System.Drawing.Point(923, 48);
            this.txtReportTime.Name = "txtReportTime";
            this.txtReportTime.Size = new System.Drawing.Size(73, 20);
            this.txtReportTime.TabIndex = 48;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl34.Appearance.Options.UseFont = true;
            this.labelControl34.Enabled = false;
            this.labelControl34.Location = new System.Drawing.Point(810, 51);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(90, 13);
            this.labelControl34.TabIndex = 47;
            this.labelControl34.Text = "Время на доклад:";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(81, 241);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(87, 13);
            this.labelControl24.TabIndex = 46;
            this.labelControl24.Text = "Дата окончания:";
            // 
            // txtFinishDate
            // 
            this.txtFinishDate.EditValue = null;
            this.txtFinishDate.Location = new System.Drawing.Point(183, 238);
            this.txtFinishDate.Name = "txtFinishDate";
            this.txtFinishDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFinishDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtFinishDate.Properties.DisplayFormat.FormatString = "f";
            this.txtFinishDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtFinishDate.Properties.EditFormat.FormatString = "f";
            this.txtFinishDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtFinishDate.Properties.Mask.EditMask = "f";
            this.txtFinishDate.Size = new System.Drawing.Size(257, 20);
            this.txtFinishDate.TabIndex = 45;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(732, 103);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(21, 13);
            this.labelControl21.TabIndex = 44;
            this.labelControl21.Text = "сек.";
            // 
            // txtVoteTime
            // 
            this.txtVoteTime.Location = new System.Drawing.Point(654, 96);
            this.txtVoteTime.Name = "txtVoteTime";
            this.txtVoteTime.Size = new System.Drawing.Size(72, 20);
            this.txtVoteTime.TabIndex = 43;
            // 
            // labelControl145
            // 
            this.labelControl145.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl145.Appearance.Options.UseFont = true;
            this.labelControl145.Location = new System.Drawing.Point(508, 99);
            this.labelControl145.Name = "labelControl145";
            this.labelControl145.Size = new System.Drawing.Size(119, 13);
            this.labelControl145.TabIndex = 42;
            this.labelControl145.Text = "Время на голосование:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(732, 51);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(21, 13);
            this.labelControl3.TabIndex = 41;
            this.labelControl3.Text = "сек.";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(183, 44);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(257, 20);
            this.txtCode.TabIndex = 40;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Location = new System.Drawing.Point(81, 44);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(61, 13);
            this.labelControl17.TabIndex = 39;
            this.labelControl17.Text = "Код сессии:";
            // 
            // txtRegTime
            // 
            this.txtRegTime.Location = new System.Drawing.Point(654, 48);
            this.txtRegTime.Name = "txtRegTime";
            this.txtRegTime.Size = new System.Drawing.Size(72, 20);
            this.txtRegTime.TabIndex = 33;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(508, 51);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(71, 13);
            this.labelControl5.TabIndex = 32;
            this.labelControl5.Text = "Время на рег:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(81, 200);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(67, 13);
            this.labelControl2.TabIndex = 31;
            this.labelControl2.Text = "Дата начала:";
            // 
            // txtStartDate
            // 
            this.txtStartDate.EditValue = new System.DateTime(2010, 10, 27, 15, 27, 31, 875);
            this.txtStartDate.Location = new System.Drawing.Point(183, 197);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtStartDate.Properties.DisplayFormat.FormatString = "f";
            this.txtStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtStartDate.Properties.Mask.EditMask = "f";
            this.txtStartDate.Size = new System.Drawing.Size(257, 20);
            this.txtStartDate.TabIndex = 30;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(81, 84);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(30, 13);
            this.labelControl1.TabIndex = 29;
            this.labelControl1.Text = "Тема:";
            // 
            // txtCaption
            // 
            this.txtCaption.Location = new System.Drawing.Point(183, 84);
            this.txtCaption.Name = "txtCaption";
            this.txtCaption.Size = new System.Drawing.Size(257, 77);
            this.txtCaption.StyleController = this.styleController1;
            this.txtCaption.TabIndex = 28;
            // 
            // styleController1
            // 
            this.styleController1.LookAndFeel.SkinName = "Caramel";
            // 
            // groupControl9
            // 
            this.groupControl9.Controls.Add(this.lbHistory);
            this.groupControl9.Location = new System.Drawing.Point(67, 348);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(448, 408);
            this.groupControl9.TabIndex = 17;
            this.groupControl9.Text = "История событий";
            // 
            // lbHistory
            // 
            this.lbHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbHistory.Location = new System.Drawing.Point(2, 22);
            this.lbHistory.Name = "lbHistory";
            this.lbHistory.Size = new System.Drawing.Size(444, 384);
            this.lbHistory.TabIndex = 0;
            // 
            // pgDelegates
            // 
            this.pgDelegates.Controls.Add(this.splitContainerControl3);
            this.pgDelegates.Name = "pgDelegates";
            this.pgDelegates.Size = new System.Drawing.Size(1609, 831);
            this.pgDelegates.Text = "Делегаты";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.gridDelegates);
            this.splitContainerControl3.Panel1.Controls.Add(this.navigatorDelegates);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.propertiesDelegate);
            this.splitContainerControl3.Panel2.Controls.Add(this.listBoxControl1);
            this.splitContainerControl3.Panel2.ShowCaption = true;
            this.splitContainerControl3.Panel2.Text = "Информация";
            this.splitContainerControl3.Size = new System.Drawing.Size(1609, 831);
            this.splitContainerControl3.SplitterPosition = 916;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridDelegates
            // 
            this.gridDelegates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDelegates.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridDelegates.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridDelegates.Location = new System.Drawing.Point(0, 0);
            this.gridDelegates.MainView = this.gridViewDelegates;
            this.gridDelegates.Name = "gridDelegates";
            this.gridDelegates.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit8,
            this.repositoryItemCheckEdit9,
            this.repositoryItemCheckEdit10});
            this.gridDelegates.Size = new System.Drawing.Size(916, 793);
            this.gridDelegates.TabIndex = 7;
            this.gridDelegates.UseEmbeddedNavigator = true;
            this.gridDelegates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDelegates});
            // 
            // gridViewDelegates
            // 
            this.gridViewDelegates.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFullName,
            this.colRegionName,
            this.colFraction,
            this.colPartyName,
            this.colIsRegistered,
            this.colCardRegistered,
            this.colMicNum});
            this.gridViewDelegates.GridControl = this.gridDelegates;
            this.gridViewDelegates.GroupCount = 1;
            this.gridViewDelegates.Name = "gridViewDelegates";
            this.gridViewDelegates.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridViewDelegates.OptionsDetail.AllowZoomDetail = false;
            this.gridViewDelegates.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewDelegates.OptionsDetail.SmartDetailExpand = false;
            this.gridViewDelegates.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPartyName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFullName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colFullName
            // 
            this.colFullName.Caption = "ФИО";
            this.colFullName.FieldName = "idDelegate.FullName";
            this.colFullName.MinWidth = 280;
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.AllowEdit = false;
            this.colFullName.OptionsColumn.FixedWidth = true;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 0;
            this.colFullName.Width = 359;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Регион";
            this.colRegionName.FieldName = "idDelegate.idRegion.Name";
            this.colRegionName.MinWidth = 150;
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 1;
            this.colRegionName.Width = 201;
            // 
            // colFraction
            // 
            this.colFraction.Caption = "Фракция";
            this.colFraction.FieldName = "idDelegate.idFraction.Name";
            this.colFraction.MinWidth = 100;
            this.colFraction.Name = "colFraction";
            this.colFraction.OptionsColumn.ReadOnly = true;
            this.colFraction.Visible = true;
            this.colFraction.VisibleIndex = 2;
            this.colFraction.Width = 161;
            // 
            // colPartyName
            // 
            this.colPartyName.Caption = "Партия";
            this.colPartyName.FieldName = "idDelegate.idParty.Name";
            this.colPartyName.MinWidth = 95;
            this.colPartyName.Name = "colPartyName";
            this.colPartyName.OptionsColumn.AllowEdit = false;
            this.colPartyName.OptionsColumn.ReadOnly = true;
            this.colPartyName.Width = 95;
            // 
            // colIsRegistered
            // 
            this.colIsRegistered.Caption = "Рег-ция";
            this.colIsRegistered.ColumnEdit = this.repositoryItemCheckEdit9;
            this.colIsRegistered.FieldName = "IsRegistered";
            this.colIsRegistered.MinWidth = 60;
            this.colIsRegistered.Name = "colIsRegistered";
            this.colIsRegistered.OptionsColumn.AllowEdit = false;
            this.colIsRegistered.Visible = true;
            this.colIsRegistered.VisibleIndex = 4;
            this.colIsRegistered.Width = 70;
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.repositoryItemCheckEdit9.ImageOptions.ImageChecked = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit9.ImageOptions.ImageChecked")));
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            // 
            // colCardRegistered
            // 
            this.colCardRegistered.Caption = "Карт.";
            this.colCardRegistered.ColumnEdit = this.repositoryItemCheckEdit10;
            this.colCardRegistered.FieldName = "IsCardRegistered";
            this.colCardRegistered.MinWidth = 60;
            this.colCardRegistered.Name = "colCardRegistered";
            this.colCardRegistered.Visible = true;
            this.colCardRegistered.VisibleIndex = 5;
            this.colCardRegistered.Width = 78;
            // 
            // repositoryItemCheckEdit10
            // 
            this.repositoryItemCheckEdit10.AutoHeight = false;
            this.repositoryItemCheckEdit10.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.repositoryItemCheckEdit10.ImageOptions.ImageChecked = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit10.ImageOptions.ImageChecked")));
            this.repositoryItemCheckEdit10.Name = "repositoryItemCheckEdit10";
            // 
            // colMicNum
            // 
            this.colMicNum.Caption = "Мик.";
            this.colMicNum.FieldName = "idSeat.MicNum";
            this.colMicNum.Name = "colMicNum";
            this.colMicNum.Visible = true;
            this.colMicNum.VisibleIndex = 3;
            this.colMicNum.Width = 90;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            // 
            // navigatorDelegates
            // 
            this.navigatorDelegates.Buttons.Append.Visible = false;
            this.navigatorDelegates.Buttons.CancelEdit.Visible = false;
            this.navigatorDelegates.Buttons.EndEdit.Visible = false;
            this.navigatorDelegates.Buttons.ImageList = this.ButtonImages;
            this.navigatorDelegates.Buttons.Remove.Visible = false;
            this.navigatorDelegates.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "", "Append"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "", "Remove"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "", "RemoveAll")});
            this.navigatorDelegates.DataSource = this.xpDelegates;
            this.navigatorDelegates.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.navigatorDelegates.Location = new System.Drawing.Point(0, 793);
            this.navigatorDelegates.Name = "navigatorDelegates";
            this.navigatorDelegates.Size = new System.Drawing.Size(916, 38);
            this.navigatorDelegates.TabIndex = 6;
            this.navigatorDelegates.Text = "dataNavigator1";
            this.navigatorDelegates.PositionChanged += new System.EventHandler(this.navigatorDelegates_PositionChanged);
            this.navigatorDelegates.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.navigatorDelegates_ButtonClick);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // xpDelegates
            // 
            this.xpDelegates.DeleteObjectOnRemove = true;
            this.xpDelegates.DisplayableProperties = "This;idSession.id;idDelegate.FullName;idDelegate.idFraction.Name;idDelegate.idPar" +
    "ty.Name;idDelegate.idRegion.Name;idDelegate.idRegion.Number;IsRegistered";
            this.xpDelegates.ObjectType = typeof(VoteSystem.SesDelegateObj);
            // 
            // propertiesDelegate
            // 
            this.propertiesDelegate.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.propertiesDelegate.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.propertiesDelegate.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.propertiesDelegate.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.propertiesDelegate.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.propertiesDelegate.Appearance.RecordValue.Options.UseForeColor = true;
            this.propertiesDelegate.Dock = System.Windows.Forms.DockStyle.Top;
            this.propertiesDelegate.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.propertiesDelegate.Location = new System.Drawing.Point(0, 0);
            this.propertiesDelegate.Name = "propertiesDelegate";
            this.propertiesDelegate.RecordWidth = 137;
            this.propertiesDelegate.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit6,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox4,
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemMemoEdit8,
            this.repositoryItemMemoEdit9});
            this.propertiesDelegate.RowHeaderWidth = 63;
            this.propertiesDelegate.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catFIO,
            this.catRegion,
            this.catInfo,
            this.catSeat,
            this.catNotes,
            this.categoryRow3});
            this.propertiesDelegate.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.propertiesDelegate.Size = new System.Drawing.Size(687, 420);
            this.propertiesDelegate.TabIndex = 11;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.PopupView = this.gridView4;
            // 
            // gridView4
            // 
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.ImmediatePopup = true;
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox4.ImmediatePopup = true;
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            this.repositoryItemComboBox4.Sorted = true;
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.ImmediatePopup = true;
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            // 
            // repositoryItemMemoEdit8
            // 
            this.repositoryItemMemoEdit8.Name = "repositoryItemMemoEdit8";
            // 
            // repositoryItemMemoEdit9
            // 
            this.repositoryItemMemoEdit9.Name = "repositoryItemMemoEdit9";
            // 
            // catFIO
            // 
            this.catFIO.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowLastName,
            this.rowFirstName,
            this.rowSecondName});
            this.catFIO.Height = 19;
            this.catFIO.Name = "catFIO";
            this.catFIO.Properties.Caption = "ФИО";
            // 
            // rowLastName
            // 
            this.rowLastName.Height = 27;
            this.rowLastName.Name = "rowLastName";
            this.rowLastName.Properties.Caption = "Фамилия";
            this.rowLastName.Properties.FieldName = "idDelegate.LastName";
            this.rowLastName.Properties.ReadOnly = true;
            this.rowLastName.Properties.Value = "1111";
            // 
            // rowFirstName
            // 
            this.rowFirstName.Height = 20;
            this.rowFirstName.Name = "rowFirstName";
            this.rowFirstName.Properties.Caption = "Имя";
            this.rowFirstName.Properties.FieldName = "idDelegate.FirstName";
            this.rowFirstName.Properties.ReadOnly = true;
            // 
            // rowSecondName
            // 
            this.rowSecondName.Height = 20;
            this.rowSecondName.Name = "rowSecondName";
            this.rowSecondName.Properties.Caption = "Отчество";
            this.rowSecondName.Properties.FieldName = "idDelegate.SecondName";
            this.rowSecondName.Properties.ReadOnly = true;
            // 
            // catRegion
            // 
            this.catRegion.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowRegionName,
            this.rowRegionNum});
            this.catRegion.Height = 19;
            this.catRegion.Name = "catRegion";
            this.catRegion.Properties.Caption = "Регион";
            // 
            // rowRegionName
            // 
            this.rowRegionName.Height = 20;
            this.rowRegionName.Name = "rowRegionName";
            this.rowRegionName.Properties.Caption = "Регион";
            this.rowRegionName.Properties.FieldName = "idDelegate.idRegion.Name";
            this.rowRegionName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowRegionNum
            // 
            this.rowRegionNum.Height = 20;
            this.rowRegionNum.Name = "rowRegionNum";
            this.rowRegionNum.Properties.Caption = "№ региона";
            this.rowRegionNum.Properties.FieldName = "idDelegate.idRegion.Number";
            this.rowRegionNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // catInfo
            // 
            this.catInfo.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowidParty_Name,
            this.rowDelegateType,
            this.rowVoteType});
            this.catInfo.Name = "catInfo";
            this.catInfo.Properties.Caption = "Дополнительная информация";
            // 
            // rowidParty_Name
            // 
            this.rowidParty_Name.Height = 23;
            this.rowidParty_Name.Name = "rowidParty_Name";
            this.rowidParty_Name.Properties.Caption = "Партия";
            this.rowidParty_Name.Properties.FieldName = "idDelegate.idParty.Name";
            this.rowidParty_Name.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowDelegateType
            // 
            this.rowDelegateType.Height = 24;
            this.rowDelegateType.Name = "rowDelegateType";
            this.rowDelegateType.Properties.Caption = "Тип участника";
            this.rowDelegateType.Properties.FieldName = "idDelegate.idType.Name";
            // 
            // rowVoteType
            // 
            this.rowVoteType.Height = 22;
            this.rowVoteType.Name = "rowVoteType";
            this.rowVoteType.Properties.Caption = "Голосующий";
            this.rowVoteType.Properties.FieldName = "idDelegate.idType.IsVoteRight";
            // 
            // catSeat
            // 
            this.catSeat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMicNum,
            this.rowRowNum,
            this.rowSeatNum});
            this.catSeat.Name = "catSeat";
            this.catSeat.Properties.Caption = "Размещение в зале";
            // 
            // rowMicNum
            // 
            this.rowMicNum.Name = "rowMicNum";
            this.rowMicNum.Properties.Caption = "Микрофон";
            this.rowMicNum.Properties.FieldName = "MicNum";
            this.rowMicNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowMicNum.Properties.Value = "dfdfd";
            // 
            // rowRowNum
            // 
            this.rowRowNum.Name = "rowRowNum";
            this.rowRowNum.Properties.Caption = "Ряд";
            this.rowRowNum.Properties.FieldName = "RowNum";
            this.rowRowNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // rowSeatNum
            // 
            this.rowSeatNum.Name = "rowSeatNum";
            this.rowSeatNum.Properties.Caption = "Место";
            this.rowSeatNum.Properties.FieldName = "SeatNum";
            this.rowSeatNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // catNotes
            // 
            this.catNotes.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowNotes});
            this.catNotes.Height = 19;
            this.catNotes.Name = "catNotes";
            this.catNotes.Properties.Caption = "Заметки";
            // 
            // rowNotes
            // 
            this.rowNotes.Height = 80;
            this.rowNotes.Name = "rowNotes";
            this.rowNotes.Properties.FieldName = "idDelegate.Notes";
            this.rowNotes.Properties.RowEdit = this.repositoryItemMemoEdit8;
            this.rowNotes.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // categoryRow3
            // 
            this.categoryRow3.Name = "categoryRow3";
            // 
            // listBoxControl1
            // 
            this.listBoxControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBoxControl1.Location = new System.Drawing.Point(0, 502);
            this.listBoxControl1.Name = "listBoxControl1";
            this.listBoxControl1.Size = new System.Drawing.Size(687, 329);
            this.listBoxControl1.TabIndex = 10;
            // 
            // pgSelQuestions
            // 
            this.pgSelQuestions.Controls.Add(this.splitContainerControl4);
            this.pgSelQuestions.Name = "pgSelQuestions";
            this.pgSelQuestions.PageVisible = false;
            this.pgSelQuestions.Size = new System.Drawing.Size(1609, 831);
            this.pgSelQuestions.Text = "Повестка дня";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.simpleButton2);
            this.splitContainerControl4.Panel1.Controls.Add(this.simpleButton1);
            this.splitContainerControl4.Panel1.Controls.Add(this.navControlQuestions);
            this.splitContainerControl4.Panel1.Controls.Add(this.navigatorQuestions);
            this.splitContainerControl4.Panel1.Controls.Add(this.SelQuestGrid);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.grpSelQuestPropsIn);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(1609, 831);
            this.splitContainerControl4.SplitterPosition = 1023;
            this.splitContainerControl4.TabIndex = 0;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(336, 737);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(175, 45);
            this.simpleButton2.TabIndex = 240;
            this.simpleButton2.Text = "Импортировать из Exel...";
            this.simpleButton2.Visible = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.ImageIndex = 14;
            this.simpleButton1.ImageOptions.ImageList = this.ButtonImages;
            this.simpleButton1.Location = new System.Drawing.Point(56, 737);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(209, 45);
            this.simpleButton1.TabIndex = 239;
            this.simpleButton1.Text = "Загрузить из файла...";
            this.simpleButton1.Visible = false;
            // 
            // navControlQuestions
            // 
            this.navControlQuestions.Buttons.Append.Visible = false;
            this.navControlQuestions.Buttons.CancelEdit.Visible = false;
            this.navControlQuestions.Buttons.Edit.Visible = false;
            this.navControlQuestions.Buttons.EndEdit.Visible = false;
            this.navControlQuestions.Buttons.ImageList = this.ButtonImages;
            this.navControlQuestions.Buttons.Remove.Visible = false;
            this.navControlQuestions.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Добавить вопрос", "Append"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Удалить из списка", "Remove"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Внести изменения", "Edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "вверх по списку", "Queue_Up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 12, true, true, "вниз по списку", "Queue_Down")});
            this.navControlQuestions.Dock = System.Windows.Forms.DockStyle.Top;
            this.navControlQuestions.Location = new System.Drawing.Point(0, 782);
            this.navControlQuestions.Name = "navControlQuestions";
            this.navControlQuestions.NavigatableControl = this.SelQuestGrid;
            this.navControlQuestions.Size = new System.Drawing.Size(1023, 34);
            this.navControlQuestions.TabIndex = 238;
            this.navControlQuestions.Text = "controlNavigator1";
            this.navControlQuestions.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.navControlQuestions_ButtonClick);
            // 
            // SelQuestGrid
            // 
            this.SelQuestGrid.DataSource = this.xpTasks;
            this.SelQuestGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.SelQuestGrid.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SelQuestGrid.Location = new System.Drawing.Point(0, 0);
            this.SelQuestGrid.MainView = this.gridViewSelQuest;
            this.SelQuestGrid.Name = "SelQuestGrid";
            this.SelQuestGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit7});
            this.SelQuestGrid.Size = new System.Drawing.Size(1023, 782);
            this.SelQuestGrid.TabIndex = 234;
            this.SelQuestGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSelQuest});
            this.SelQuestGrid.DoubleClick += new System.EventHandler(this.SelQuestGrid_DoubleClick);
            this.SelQuestGrid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SelQuestGrid_KeyPress);
            // 
            // xpTasks
            // 
            this.xpTasks.CriteriaString = "[id] > 0 And [IsDel] = False";
            this.xpTasks.DeleteObjectOnRemove = true;
            this.xpTasks.DisplayableProperties = "This;id;Caption;ItemNum;idKind.Name";
            this.xpTasks.ObjectType = typeof(VoteSystem.SesTaskObj);
            // 
            // gridViewSelQuest
            // 
            this.gridViewSelQuest.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQueueNum,
            this.colQuestCaption,
            this.colid,
            this.colState,
            this.colStatus,
            this.colItemNum,
            this.colIsHot});
            this.gridViewSelQuest.GridControl = this.SelQuestGrid;
            this.gridViewSelQuest.Name = "gridViewSelQuest";
            this.gridViewSelQuest.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridViewSelQuest.OptionsBehavior.Editable = false;
            this.gridViewSelQuest.OptionsDetail.AutoZoomDetail = true;
            this.gridViewSelQuest.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewSelQuest.OptionsView.ShowGroupPanel = false;
            this.gridViewSelQuest.RowHeight = 25;
            this.gridViewSelQuest.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQueueNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colQueueNum
            // 
            this.colQueueNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colQueueNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQueueNum.Caption = "№ в списке";
            this.colQueueNum.FieldName = "QueueNum";
            this.colQueueNum.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colQueueNum.Name = "colQueueNum";
            this.colQueueNum.Visible = true;
            this.colQueueNum.VisibleIndex = 3;
            this.colQueueNum.Width = 96;
            // 
            // colQuestCaption
            // 
            this.colQuestCaption.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuestCaption.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuestCaption.Caption = "Заголовок";
            this.colQuestCaption.FieldName = "Caption";
            this.colQuestCaption.MinWidth = 255;
            this.colQuestCaption.Name = "colQuestCaption";
            this.colQuestCaption.Visible = true;
            this.colQuestCaption.VisibleIndex = 1;
            this.colQuestCaption.Width = 639;
            // 
            // colid
            // 
            this.colid.Caption = "Код";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.OptionsColumn.FixedWidth = true;
            this.colid.Width = 63;
            // 
            // colState
            // 
            this.colState.AppearanceHeader.Options.UseTextOptions = true;
            this.colState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colState.Caption = "Статус";
            this.colState.ColumnEdit = this.repositoryItemCheckEdit7;
            this.colState.FieldName = "TaskState";
            this.colState.Name = "colState";
            this.colState.Visible = true;
            this.colState.VisibleIndex = 2;
            this.colState.Width = 129;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // colStatus
            // 
            this.colStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatus.Caption = "Статус";
            this.colStatus.FieldName = "VoteStatus";
            this.colStatus.Name = "colStatus";
            // 
            // colItemNum
            // 
            this.colItemNum.AppearanceCell.Options.UseTextOptions = true;
            this.colItemNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colItemNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colItemNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colItemNum.Caption = "№ пункта";
            this.colItemNum.FieldName = "ItemNumStr";
            this.colItemNum.Name = "colItemNum";
            this.colItemNum.Visible = true;
            this.colItemNum.VisibleIndex = 0;
            this.colItemNum.Width = 84;
            // 
            // colIsHot
            // 
            this.colIsHot.Caption = "gridColumn2";
            this.colIsHot.FieldName = "IsHot";
            this.colIsHot.Name = "colIsHot";
            // 
            // navigatorQuestions
            // 
            this.navigatorQuestions.Buttons.Append.Hint = "папап";
            this.navigatorQuestions.Buttons.Append.Visible = false;
            this.navigatorQuestions.Buttons.CancelEdit.Visible = false;
            this.navigatorQuestions.Buttons.EndEdit.Visible = false;
            this.navigatorQuestions.Buttons.ImageList = this.ButtonImages;
            this.navigatorQuestions.Buttons.Next.Hint = "апапа";
            this.navigatorQuestions.Buttons.Remove.Visible = false;
            this.navigatorQuestions.DataSource = this.xpTasks;
            this.navigatorQuestions.Location = new System.Drawing.Point(713, 713);
            this.navigatorQuestions.Name = "navigatorQuestions";
            this.navigatorQuestions.Size = new System.Drawing.Size(117, 31);
            this.navigatorQuestions.TabIndex = 235;
            this.navigatorQuestions.Text = "dataNavigator1";
            this.navigatorQuestions.Visible = false;
            this.navigatorQuestions.PositionChanged += new System.EventHandler(this.navigatorQuestions_PositionChanged);
            // 
            // grpSelQuestPropsIn
            // 
            this.grpSelQuestPropsIn.Controls.Add(this.PropertiesControlQuest);
            this.grpSelQuestPropsIn.Controls.Add(this.panelControl3);
            this.grpSelQuestPropsIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSelQuestPropsIn.Location = new System.Drawing.Point(0, 0);
            this.grpSelQuestPropsIn.Name = "grpSelQuestPropsIn";
            this.grpSelQuestPropsIn.Size = new System.Drawing.Size(580, 831);
            this.grpSelQuestPropsIn.TabIndex = 238;
            this.grpSelQuestPropsIn.Tag = "";
            this.grpSelQuestPropsIn.Text = "Панель свойств";
            // 
            // PropertiesControlQuest
            // 
            this.PropertiesControlQuest.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlQuest.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlQuest.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest.DataSource = this.xpTasks;
            this.PropertiesControlQuest.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControlQuest.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlQuest.Location = new System.Drawing.Point(2, 22);
            this.PropertiesControlQuest.Name = "PropertiesControlQuest";
            this.PropertiesControlQuest.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControlQuest.RecordWidth = 142;
            this.PropertiesControlQuest.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpEdit2,
            this.repositoryItemComboBox9,
            this.repositoryItemComboBox10,
            this.repositoryItemComboBox11,
            this.repositoryItemComboBox12,
            this.repositoryItemComboBox13,
            this.repositoryItemComboBox14,
            this.repositoryItemMemoEdit2,
            this.repositoryItemMemoEdit3,
            this.repositoryItemTextEdit7});
            this.PropertiesControlQuest.RowHeaderWidth = 58;
            this.PropertiesControlQuest.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catCharacteristics,
            this.categoryRow8,
            this.catResult,
            this.categoryRow5});
            this.PropertiesControlQuest.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlQuest.Size = new System.Drawing.Size(576, 497);
            this.PropertiesControlQuest.TabIndex = 17;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.PopupView = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox9
            // 
            this.repositoryItemComboBox9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox9.ImmediatePopup = true;
            this.repositoryItemComboBox9.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox9.Name = "repositoryItemComboBox9";
            // 
            // repositoryItemComboBox10
            // 
            this.repositoryItemComboBox10.AutoHeight = false;
            this.repositoryItemComboBox10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox10.ImmediatePopup = true;
            this.repositoryItemComboBox10.Name = "repositoryItemComboBox10";
            this.repositoryItemComboBox10.Sorted = true;
            // 
            // repositoryItemComboBox11
            // 
            this.repositoryItemComboBox11.AutoHeight = false;
            this.repositoryItemComboBox11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox11.Name = "repositoryItemComboBox11";
            // 
            // repositoryItemComboBox12
            // 
            this.repositoryItemComboBox12.AutoHeight = false;
            this.repositoryItemComboBox12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox12.ImmediatePopup = true;
            this.repositoryItemComboBox12.Name = "repositoryItemComboBox12";
            // 
            // repositoryItemComboBox13
            // 
            this.repositoryItemComboBox13.AutoHeight = false;
            this.repositoryItemComboBox13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox13.Name = "repositoryItemComboBox13";
            // 
            // repositoryItemComboBox14
            // 
            this.repositoryItemComboBox14.AutoHeight = false;
            this.repositoryItemComboBox14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox14.Name = "repositoryItemComboBox14";
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // repositoryItemMemoEdit3
            // 
            this.repositoryItemMemoEdit3.Name = "repositoryItemMemoEdit3";
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.repositoryItemTextEdit7.Appearance.Options.UseForeColor = true;
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // catCharacteristics
            // 
            this.catCharacteristics.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowCaption,
            this.rowidKind_Name,
            this.rowCrDate,
            this.rowReadNum,
            this.rowDescription});
            this.catCharacteristics.Height = 19;
            this.catCharacteristics.Name = "catCharacteristics";
            this.catCharacteristics.Properties.Caption = "Характеристики";
            // 
            // rowCaption
            // 
            this.rowCaption.Height = 102;
            this.rowCaption.Name = "rowCaption";
            this.rowCaption.Properties.Caption = "Заголовок";
            this.rowCaption.Properties.FieldName = "Caption";
            this.rowCaption.Properties.ReadOnly = true;
            this.rowCaption.Properties.RowEdit = this.repositoryItemMemoEdit3;
            this.rowCaption.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowidKind_Name
            // 
            this.rowidKind_Name.Height = 28;
            this.rowidKind_Name.Name = "rowidKind_Name";
            this.rowidKind_Name.Properties.Caption = "Вид";
            this.rowidKind_Name.Properties.FieldName = "idKind.Name";
            this.rowidKind_Name.Properties.ReadOnly = true;
            this.rowidKind_Name.Properties.RowEdit = this.repositoryItemComboBox14;
            // 
            // rowCrDate
            // 
            this.rowCrDate.Height = 37;
            this.rowCrDate.Name = "rowCrDate";
            this.rowCrDate.Properties.Caption = "Дата/время";
            this.rowCrDate.Properties.FieldName = "CrDate";
            this.rowCrDate.Properties.ReadOnly = true;
            // 
            // rowReadNum
            // 
            this.rowReadNum.Height = 26;
            this.rowReadNum.Name = "rowReadNum";
            this.rowReadNum.Properties.Caption = "Чтение";
            this.rowReadNum.Properties.FieldName = "ReadNum";
            this.rowReadNum.Properties.ReadOnly = true;
            // 
            // rowDescription
            // 
            this.rowDescription.Height = 126;
            this.rowDescription.Name = "rowDescription";
            this.rowDescription.Properties.Caption = "Описание";
            this.rowDescription.Properties.FieldName = "Description";
            this.rowDescription.Properties.ReadOnly = true;
            this.rowDescription.Properties.RowEdit = this.repositoryItemMemoEdit2;
            // 
            // categoryRow8
            // 
            this.categoryRow8.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow26});
            this.categoryRow8.Height = 19;
            this.categoryRow8.Name = "categoryRow8";
            this.categoryRow8.Properties.Caption = "Заметки";
            // 
            // editorRow26
            // 
            this.editorRow26.Height = 109;
            this.editorRow26.Name = "editorRow26";
            this.editorRow26.Properties.ReadOnly = true;
            this.editorRow26.Properties.RowEdit = this.repositoryItemMemoEdit2;
            // 
            // catResult
            // 
            this.catResult.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.catResult.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowResult,
            this.rowResDate});
            this.catResult.Height = 20;
            this.catResult.Name = "catResult";
            this.catResult.Properties.Caption = "Результат";
            this.catResult.Visible = false;
            // 
            // rowResult
            // 
            this.rowResult.Appearance.BackColor = System.Drawing.Color.OldLace;
            this.rowResult.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rowResult.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.rowResult.Appearance.Options.UseBackColor = true;
            this.rowResult.Appearance.Options.UseFont = true;
            this.rowResult.Appearance.Options.UseForeColor = true;
            this.rowResult.Height = 36;
            this.rowResult.Name = "rowResult";
            this.rowResult.Properties.Caption = "Решение";
            this.rowResult.Properties.ReadOnly = true;
            this.rowResult.Properties.RowEdit = this.repositoryItemTextEdit7;
            // 
            // rowResDate
            // 
            this.rowResDate.Appearance.BackColor = System.Drawing.Color.OldLace;
            this.rowResDate.Appearance.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rowResDate.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.rowResDate.Appearance.Options.UseBackColor = true;
            this.rowResDate.Appearance.Options.UseFont = true;
            this.rowResDate.Appearance.Options.UseForeColor = true;
            this.rowResDate.Height = 28;
            this.rowResDate.Name = "rowResDate";
            this.rowResDate.Properties.Caption = "Дата/время";
            this.rowResDate.Properties.ReadOnly = true;
            // 
            // categoryRow5
            // 
            this.categoryRow5.Name = "categoryRow5";
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 728);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(576, 101);
            this.panelControl3.TabIndex = 15;
            // 
            // pgQuorumGraphic
            // 
            this.pgQuorumGraphic.Controls.Add(this.splitContainerControl2);
            this.pgQuorumGraphic.Controls.Add(this.pnlQuorumButtons);
            this.pgQuorumGraphic.Name = "pgQuorumGraphic";
            this.pgQuorumGraphic.PageVisible = false;
            this.pgQuorumGraphic.Size = new System.Drawing.Size(1609, 831);
            this.pgQuorumGraphic.Text = "Управление залом";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.pnlHallSettings);
            this.splitContainerControl2.Panel1.Controls.Add(this.pnlHall);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.groupControl3);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1609, 715);
            this.splitContainerControl2.SplitterPosition = 1180;
            this.splitContainerControl2.TabIndex = 32;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // pnlHallSettings
            // 
            this.pnlHallSettings.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.pnlHallSettings.Appearance.Options.UseForeColor = true;
            this.pnlHallSettings.Controls.Add(this.labelControl6);
            this.pnlHallSettings.Controls.Add(this.btnCardStart);
            this.pnlHallSettings.Controls.Add(this.btnTextOut);
            this.pnlHallSettings.Controls.Add(this.spinBackCardMode);
            this.pnlHallSettings.Controls.Add(this.chkBackCardMode);
            this.pnlHallSettings.Controls.Add(this.btnClearSeats);
            this.pnlHallSettings.Controls.Add(this.chkAutoCardMode);
            this.pnlHallSettings.Controls.Add(this.chkDelegateSeat);
            this.pnlHallSettings.Controls.Add(this.btnActAllCards);
            this.pnlHallSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHallSettings.Location = new System.Drawing.Point(0, 0);
            this.pnlHallSettings.Name = "pnlHallSettings";
            this.pnlHallSettings.Size = new System.Drawing.Size(1180, 84);
            this.pnlHallSettings.TabIndex = 1;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(327, -266);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(21, 13);
            this.labelControl6.TabIndex = 270;
            this.labelControl6.Text = "сек.";
            // 
            // btnCardStart
            // 
            this.btnCardStart.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCardStart.Appearance.Options.UseFont = true;
            this.btnCardStart.ImageOptions.ImageList = this.ButtonImages;
            this.btnCardStart.Location = new System.Drawing.Point(959, 12);
            this.btnCardStart.Name = "btnCardStart";
            this.btnCardStart.Size = new System.Drawing.Size(151, 43);
            this.btnCardStart.TabIndex = 259;
            this.btnCardStart.Text = "Определение карточек";
            this.btnCardStart.Click += new System.EventHandler(this.btnCardStart_Click);
            // 
            // btnTextOut
            // 
            this.btnTextOut.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnTextOut.Appearance.Options.UseFont = true;
            this.btnTextOut.ImageOptions.ImageList = this.ButtonImages;
            this.btnTextOut.Location = new System.Drawing.Point(619, 44);
            this.btnTextOut.Name = "btnTextOut";
            this.btnTextOut.Size = new System.Drawing.Size(271, 29);
            this.btnTextOut.TabIndex = 260;
            this.btnTextOut.Text = "Вывод имен на пульты согласно рассадке";
            this.btnTextOut.Click += new System.EventHandler(this.btnTextOut_Click);
            // 
            // spinBackCardMode
            // 
            this.spinBackCardMode.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinBackCardMode.Location = new System.Drawing.Point(311, 46);
            this.spinBackCardMode.MenuManager = this.ribbon;
            this.spinBackCardMode.Name = "spinBackCardMode";
            this.spinBackCardMode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinBackCardMode.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.spinBackCardMode.Properties.IsFloatValue = false;
            this.spinBackCardMode.Properties.Mask.EditMask = "N00";
            this.spinBackCardMode.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinBackCardMode.Size = new System.Drawing.Size(45, 20);
            this.spinBackCardMode.TabIndex = 269;
            this.spinBackCardMode.EditValueChanged += new System.EventHandler(this.spinBackCardMode_EditValueChanged);
            // 
            // chkBackCardMode
            // 
            this.chkBackCardMode.Location = new System.Drawing.Point(16, 46);
            this.chkBackCardMode.Name = "chkBackCardMode";
            this.chkBackCardMode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkBackCardMode.Properties.Appearance.Options.UseFont = true;
            this.chkBackCardMode.Properties.Caption = "Отслеживание изменений карточек каждые ";
            this.chkBackCardMode.Size = new System.Drawing.Size(277, 19);
            this.chkBackCardMode.TabIndex = 268;
            this.chkBackCardMode.ToolTip = "Включить или выключить режим автоматического определения карточек";
            this.chkBackCardMode.CheckedChanged += new System.EventHandler(this.chkBackCardMode_CheckedChanged);
            // 
            // btnClearSeats
            // 
            this.btnClearSeats.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClearSeats.Appearance.Options.UseFont = true;
            this.btnClearSeats.ImageOptions.ImageList = this.ButtonImages;
            this.btnClearSeats.Location = new System.Drawing.Point(766, 11);
            this.btnClearSeats.Name = "btnClearSeats";
            this.btnClearSeats.Size = new System.Drawing.Size(124, 25);
            this.btnClearSeats.TabIndex = 266;
            this.btnClearSeats.Text = "Очистить места";
            this.btnClearSeats.ToolTip = "Освободить места от всех депутатов";
            this.btnClearSeats.Click += new System.EventHandler(this.btnClearSeats_Click);
            // 
            // chkAutoCardMode
            // 
            this.chkAutoCardMode.Location = new System.Drawing.Point(16, 10);
            this.chkAutoCardMode.Name = "chkAutoCardMode";
            this.chkAutoCardMode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkAutoCardMode.Properties.Appearance.Options.UseFont = true;
            this.chkAutoCardMode.Properties.Caption = "Предварительное определение карточек";
            this.chkAutoCardMode.Size = new System.Drawing.Size(280, 19);
            this.chkAutoCardMode.TabIndex = 264;
            this.chkAutoCardMode.ToolTip = "Включить или выключить режим автоматического определения карточек перед регистрац" +
    "ией и перед голосованием. Рекомендуется включить";
            this.chkAutoCardMode.CheckedChanged += new System.EventHandler(this.chkAutoCardMode_CheckedChanged);
            // 
            // chkDelegateSeat
            // 
            this.chkDelegateSeat.Location = new System.Drawing.Point(311, 10);
            this.chkDelegateSeat.Name = "chkDelegateSeat";
            this.chkDelegateSeat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkDelegateSeat.Properties.Appearance.Options.UseFont = true;
            this.chkDelegateSeat.Properties.Caption = "Ручной режим рассадки";
            this.chkDelegateSeat.Size = new System.Drawing.Size(178, 19);
            this.chkDelegateSeat.TabIndex = 263;
            this.chkDelegateSeat.ToolTip = "Показать рассадку всех депутатов, включая не имеющих карточек";
            this.chkDelegateSeat.CheckedChanged += new System.EventHandler(this.chkDelegateSeat_CheckedChanged);
            // 
            // btnActAllCards
            // 
            this.btnActAllCards.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnActAllCards.Appearance.Options.UseFont = true;
            this.btnActAllCards.ImageOptions.ImageList = this.ButtonImages;
            this.btnActAllCards.Location = new System.Drawing.Point(619, 12);
            this.btnActAllCards.Name = "btnActAllCards";
            this.btnActAllCards.Size = new System.Drawing.Size(121, 24);
            this.btnActAllCards.TabIndex = 261;
            this.btnActAllCards.Text = "Активир. всех";
            this.btnActAllCards.ToolTip = "Установить наличие карточек у всех рассаженных депутатов";
            this.btnActAllCards.Click += new System.EventHandler(this.btnActAllCards_Click);
            // 
            // pnlHall
            // 
            this.pnlHall.ContentImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.pnlHall.Controls.Add(this.info_1);
            this.pnlHall.Controls.Add(this.seatComponent139);
            this.pnlHall.Controls.Add(this.seatComponent121);
            this.pnlHall.Controls.Add(this.seatComponent86);
            this.pnlHall.Controls.Add(this.seatComponent138);
            this.pnlHall.Controls.Add(this.seatComponent137);
            this.pnlHall.Controls.Add(this.seatComponent120);
            this.pnlHall.Controls.Add(this.seatComponent119);
            this.pnlHall.Controls.Add(this.seatComponent136);
            this.pnlHall.Controls.Add(this.seatComponent87);
            this.pnlHall.Controls.Add(this.seatComponent118);
            this.pnlHall.Controls.Add(this.seatComponent135);
            this.pnlHall.Controls.Add(this.seatComponent88);
            this.pnlHall.Controls.Add(this.seatComponent117);
            this.pnlHall.Controls.Add(this.seatComponent134);
            this.pnlHall.Controls.Add(this.seatComponent89);
            this.pnlHall.Controls.Add(this.seatComponent116);
            this.pnlHall.Controls.Add(this.seatComponent133);
            this.pnlHall.Controls.Add(this.seatComponent90);
            this.pnlHall.Controls.Add(this.seatComponent115);
            this.pnlHall.Controls.Add(this.seatComponent132);
            this.pnlHall.Controls.Add(this.seatComponent91);
            this.pnlHall.Controls.Add(this.seatComponent114);
            this.pnlHall.Controls.Add(this.seatComponent131);
            this.pnlHall.Controls.Add(this.seatComponent92);
            this.pnlHall.Controls.Add(this.seatComponent113);
            this.pnlHall.Controls.Add(this.seatComponent130);
            this.pnlHall.Controls.Add(this.seatComponent93);
            this.pnlHall.Controls.Add(this.seatComponent112);
            this.pnlHall.Controls.Add(this.seatComponent129);
            this.pnlHall.Controls.Add(this.seatComponent94);
            this.pnlHall.Controls.Add(this.seatComponent111);
            this.pnlHall.Controls.Add(this.seatComponent128);
            this.pnlHall.Controls.Add(this.seatComponent95);
            this.pnlHall.Controls.Add(this.seatComponent110);
            this.pnlHall.Controls.Add(this.seatComponent127);
            this.pnlHall.Controls.Add(this.seatComponent96);
            this.pnlHall.Controls.Add(this.seatComponent109);
            this.pnlHall.Controls.Add(this.seatComponent126);
            this.pnlHall.Controls.Add(this.seatComponent97);
            this.pnlHall.Controls.Add(this.seatComponent108);
            this.pnlHall.Controls.Add(this.seatComponent125);
            this.pnlHall.Controls.Add(this.seatComponent98);
            this.pnlHall.Controls.Add(this.seatComponent107);
            this.pnlHall.Controls.Add(this.seatComponent124);
            this.pnlHall.Controls.Add(this.seatComponent99);
            this.pnlHall.Controls.Add(this.seatComponent106);
            this.pnlHall.Controls.Add(this.seatComponent123);
            this.pnlHall.Controls.Add(this.seatComponent100);
            this.pnlHall.Controls.Add(this.seatComponent105);
            this.pnlHall.Controls.Add(this.seatComponent122);
            this.pnlHall.Controls.Add(this.seatComponent101);
            this.pnlHall.Controls.Add(this.seatComponent104);
            this.pnlHall.Controls.Add(this.seatComponent102);
            this.pnlHall.Controls.Add(this.seatComponent103);
            this.pnlHall.Controls.Add(this.seatComponent68);
            this.pnlHall.Controls.Add(this.seatComponent69);
            this.pnlHall.Controls.Add(this.seatComponent70);
            this.pnlHall.Controls.Add(this.seatComponent71);
            this.pnlHall.Controls.Add(this.seatComponent72);
            this.pnlHall.Controls.Add(this.seatComponent73);
            this.pnlHall.Controls.Add(this.seatComponent74);
            this.pnlHall.Controls.Add(this.seatComponent75);
            this.pnlHall.Controls.Add(this.seatComponent76);
            this.pnlHall.Controls.Add(this.seatComponent77);
            this.pnlHall.Controls.Add(this.seatComponent78);
            this.pnlHall.Controls.Add(this.seatComponent79);
            this.pnlHall.Controls.Add(this.seatComponent80);
            this.pnlHall.Controls.Add(this.seatComponent81);
            this.pnlHall.Controls.Add(this.seatComponent82);
            this.pnlHall.Controls.Add(this.seatComponent83);
            this.pnlHall.Controls.Add(this.seatComponent84);
            this.pnlHall.Controls.Add(this.seatComponent85);
            this.pnlHall.Controls.Add(this.seatComponent67);
            this.pnlHall.Controls.Add(this.seatComponent66);
            this.pnlHall.Controls.Add(this.seatComponent65);
            this.pnlHall.Controls.Add(this.seatComponent49);
            this.pnlHall.Controls.Add(this.seatComponent31);
            this.pnlHall.Controls.Add(this.seatComponent64);
            this.pnlHall.Controls.Add(this.seatComponent48);
            this.pnlHall.Controls.Add(this.seatComponent27);
            this.pnlHall.Controls.Add(this.seatComponent63);
            this.pnlHall.Controls.Add(this.seatComponent47);
            this.pnlHall.Controls.Add(this.seatComponent20);
            this.pnlHall.Controls.Add(this.seatComponent62);
            this.pnlHall.Controls.Add(this.seatComponent46);
            this.pnlHall.Controls.Add(this.seatComponent30);
            this.pnlHall.Controls.Add(this.seatComponent61);
            this.pnlHall.Controls.Add(this.seatComponent45);
            this.pnlHall.Controls.Add(this.seatComponent26);
            this.pnlHall.Controls.Add(this.seatComponent60);
            this.pnlHall.Controls.Add(this.seatComponent44);
            this.pnlHall.Controls.Add(this.seatComponent21);
            this.pnlHall.Controls.Add(this.seatComponent59);
            this.pnlHall.Controls.Add(this.seatComponent43);
            this.pnlHall.Controls.Add(this.seatComponent29);
            this.pnlHall.Controls.Add(this.seatComponent58);
            this.pnlHall.Controls.Add(this.seatComponent42);
            this.pnlHall.Controls.Add(this.seatComponent25);
            this.pnlHall.Controls.Add(this.seatComponent57);
            this.pnlHall.Controls.Add(this.seatComponent41);
            this.pnlHall.Controls.Add(this.seatComponent22);
            this.pnlHall.Controls.Add(this.seatComponent56);
            this.pnlHall.Controls.Add(this.seatComponent40);
            this.pnlHall.Controls.Add(this.seatComponent28);
            this.pnlHall.Controls.Add(this.seatComponent55);
            this.pnlHall.Controls.Add(this.seatComponent39);
            this.pnlHall.Controls.Add(this.seatComponent24);
            this.pnlHall.Controls.Add(this.seatComponent54);
            this.pnlHall.Controls.Add(this.seatComponent38);
            this.pnlHall.Controls.Add(this.seatComponent23);
            this.pnlHall.Controls.Add(this.seatComponent53);
            this.pnlHall.Controls.Add(this.seatComponent37);
            this.pnlHall.Controls.Add(this.seatComponent33);
            this.pnlHall.Controls.Add(this.seatComponent52);
            this.pnlHall.Controls.Add(this.seatComponent36);
            this.pnlHall.Controls.Add(this.seatComponent18);
            this.pnlHall.Controls.Add(this.seatComponent51);
            this.pnlHall.Controls.Add(this.seatComponent50);
            this.pnlHall.Controls.Add(this.seatComponent35);
            this.pnlHall.Controls.Add(this.seatComponent34);
            this.pnlHall.Controls.Add(this.seatComponent32);
            this.pnlHall.Controls.Add(this.seatComponent19);
            this.pnlHall.Controls.Add(this.seatComponent14);
            this.pnlHall.Controls.Add(this.seatComponent15);
            this.pnlHall.Controls.Add(this.seatComponent16);
            this.pnlHall.Controls.Add(this.seatComponent17);
            this.pnlHall.Controls.Add(this.seatComponent13);
            this.pnlHall.Controls.Add(this.seatComponent12);
            this.pnlHall.Controls.Add(this.seatComponent11);
            this.pnlHall.Controls.Add(this.seatComponent10);
            this.pnlHall.Controls.Add(this.seatComponent9);
            this.pnlHall.Controls.Add(this.seatComponent8);
            this.pnlHall.Controls.Add(this.seatComponent7);
            this.pnlHall.Controls.Add(this.seatComponent6);
            this.pnlHall.Controls.Add(this.seatComponent5);
            this.pnlHall.Controls.Add(this.seatComponent4);
            this.pnlHall.Controls.Add(this.seatComponent3);
            this.pnlHall.Controls.Add(this.seatComponent2);
            this.pnlHall.Controls.Add(this.seatComponent1);
            this.pnlHall.Controls.Add(this.label3);
            this.pnlHall.Controls.Add(this.label2);
            this.pnlHall.Controls.Add(this.label92);
            this.pnlHall.Controls.Add(this.label15);
            this.pnlHall.Controls.Add(this.label24);
            this.pnlHall.Controls.Add(this.label27);
            this.pnlHall.Controls.Add(this.label36);
            this.pnlHall.Controls.Add(this.label37);
            this.pnlHall.Controls.Add(this.label38);
            this.pnlHall.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlHall.Location = new System.Drawing.Point(0, 84);
            this.pnlHall.Name = "pnlHall";
            this.pnlHall.Size = new System.Drawing.Size(1180, 631);
            this.pnlHall.TabIndex = 0;
            this.pnlHall.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // info_1
            // 
            this.info_1.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_1.Appearance.Options.UseFont = true;
            this.info_1.Location = new System.Drawing.Point(53, 600);
            this.info_1.Name = "info_1";
            this.info_1.Size = new System.Drawing.Size(20, 16);
            this.info_1.TabIndex = 550;
            this.info_1.Text = "info";
            this.info_1.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(50, 565);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 16);
            this.label3.TabIndex = 279;
            this.label3.Text = "Ряд 8";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(51, 506);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 278;
            this.label2.Text = "Ряд 7";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label92.Location = new System.Drawing.Point(50, 448);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(43, 16);
            this.label92.TabIndex = 277;
            this.label92.Text = "Ряд 6";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(51, 388);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 16);
            this.label15.TabIndex = 276;
            this.label15.Text = "Ряд 5";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(51, 338);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 16);
            this.label24.TabIndex = 275;
            this.label24.Text = "Ряд 4";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(50, 272);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 16);
            this.label27.TabIndex = 274;
            this.label27.Text = "Ряд 3";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(51, 219);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(43, 16);
            this.label36.TabIndex = 273;
            this.label36.Text = "Ряд 2";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(51, 159);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 16);
            this.label37.TabIndex = 272;
            this.label37.Text = "Ряд 1";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(36, 69);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(81, 16);
            this.label38.TabIndex = 271;
            this.label38.Text = "Президиум";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.PropertiesControl_QPage);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(423, 715);
            this.groupControl3.TabIndex = 32;
            this.groupControl3.Text = "Окно свойств";
            // 
            // PropertiesControl_QPage
            // 
            this.PropertiesControl_QPage.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControl_QPage.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControl_QPage.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControl_QPage.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControl_QPage.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControl_QPage.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControl_QPage.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.PropertiesControl_QPage.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControl_QPage.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControl_QPage.Location = new System.Drawing.Point(2, 22);
            this.PropertiesControl_QPage.Name = "PropertiesControl_QPage";
            this.PropertiesControl_QPage.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControl_QPage.RecordWidth = 116;
            this.PropertiesControl_QPage.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2,
            this.repositoryItemGridLookUpEdit3,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox7,
            this.repositoryItemComboBox8,
            this.repositoryItemComboBox17});
            this.PropertiesControl_QPage.RowHeaderWidth = 84;
            this.PropertiesControl_QPage.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow1,
            this.cat_q_Region,
            this.cat_q_Info,
            this.cat_q_Seat});
            this.PropertiesControl_QPage.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControl_QPage.Size = new System.Drawing.Size(419, 359);
            this.PropertiesControl_QPage.TabIndex = 10;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemGridLookUpEdit3
            // 
            this.repositoryItemGridLookUpEdit3.AutoHeight = false;
            this.repositoryItemGridLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit3.Name = "repositoryItemGridLookUpEdit3";
            this.repositoryItemGridLookUpEdit3.PopupView = this.gridView8;
            // 
            // gridView8
            // 
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ImmediatePopup = true;
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox7
            // 
            this.repositoryItemComboBox7.AutoHeight = false;
            this.repositoryItemComboBox7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox7.ImmediatePopup = true;
            this.repositoryItemComboBox7.Name = "repositoryItemComboBox7";
            this.repositoryItemComboBox7.Sorted = true;
            // 
            // repositoryItemComboBox8
            // 
            this.repositoryItemComboBox8.AutoHeight = false;
            this.repositoryItemComboBox8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox8.Name = "repositoryItemComboBox8";
            // 
            // repositoryItemComboBox17
            // 
            this.repositoryItemComboBox17.AutoHeight = false;
            this.repositoryItemComboBox17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox17.ImmediatePopup = true;
            this.repositoryItemComboBox17.Name = "repositoryItemComboBox17";
            // 
            // categoryRow1
            // 
            this.categoryRow1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_q_LastName,
            this.row_q_FirstName,
            this.row_q_SecondName});
            this.categoryRow1.Height = 19;
            this.categoryRow1.Name = "categoryRow1";
            this.categoryRow1.Properties.Caption = "ФИО";
            // 
            // row_q_LastName
            // 
            this.row_q_LastName.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_LastName.Appearance.Options.UseFont = true;
            this.row_q_LastName.Height = 38;
            this.row_q_LastName.Name = "row_q_LastName";
            this.row_q_LastName.Properties.Caption = "Фамилия";
            this.row_q_LastName.Properties.FieldName = "LastName";
            this.row_q_LastName.Properties.ReadOnly = true;
            this.row_q_LastName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.row_q_LastName.Properties.Value = "1111";
            // 
            // row_q_FirstName
            // 
            this.row_q_FirstName.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_FirstName.Appearance.Options.UseFont = true;
            this.row_q_FirstName.Height = 30;
            this.row_q_FirstName.Name = "row_q_FirstName";
            this.row_q_FirstName.Properties.Caption = "Имя";
            this.row_q_FirstName.Properties.FieldName = "FirstName";
            this.row_q_FirstName.Properties.ReadOnly = true;
            this.row_q_FirstName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_q_SecondName
            // 
            this.row_q_SecondName.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_SecondName.Appearance.Options.UseFont = true;
            this.row_q_SecondName.Height = 34;
            this.row_q_SecondName.Name = "row_q_SecondName";
            this.row_q_SecondName.Properties.Caption = "Отчество";
            this.row_q_SecondName.Properties.FieldName = "SecondName";
            this.row_q_SecondName.Properties.ReadOnly = true;
            // 
            // cat_q_Region
            // 
            this.cat_q_Region.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_q_RegionName,
            this.row_q_RegionNum});
            this.cat_q_Region.Name = "cat_q_Region";
            this.cat_q_Region.Properties.Caption = "Регион";
            // 
            // row_q_RegionName
            // 
            this.row_q_RegionName.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_RegionName.Appearance.Options.UseFont = true;
            this.row_q_RegionName.Height = 25;
            this.row_q_RegionName.Name = "row_q_RegionName";
            this.row_q_RegionName.Properties.Caption = "Регион";
            this.row_q_RegionName.Properties.FieldName = "RegionName";
            this.row_q_RegionName.Properties.ReadOnly = true;
            this.row_q_RegionName.Properties.RowEdit = this.repositoryItemComboBox7;
            this.row_q_RegionName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_q_RegionNum
            // 
            this.row_q_RegionNum.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_RegionNum.Appearance.Options.UseFont = true;
            this.row_q_RegionNum.Height = 21;
            this.row_q_RegionNum.Name = "row_q_RegionNum";
            this.row_q_RegionNum.Properties.Caption = "№ региона";
            this.row_q_RegionNum.Properties.FieldName = "RegionNum";
            this.row_q_RegionNum.Properties.ReadOnly = true;
            this.row_q_RegionNum.Properties.RowEdit = this.repositoryItemComboBox8;
            this.row_q_RegionNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // cat_q_Info
            // 
            this.cat_q_Info.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_q_Fraction,
            this.row_q_PartyName});
            this.cat_q_Info.Name = "cat_q_Info";
            this.cat_q_Info.Properties.Caption = "Дополнительная информация";
            // 
            // row_q_Fraction
            // 
            this.row_q_Fraction.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_Fraction.Appearance.Options.UseFont = true;
            this.row_q_Fraction.Height = 25;
            this.row_q_Fraction.Name = "row_q_Fraction";
            this.row_q_Fraction.Properties.Caption = "Фракция";
            this.row_q_Fraction.Properties.FieldName = "Fraction";
            this.row_q_Fraction.Properties.ReadOnly = true;
            this.row_q_Fraction.Properties.RowEdit = this.repositoryItemComboBox17;
            this.row_q_Fraction.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_q_PartyName
            // 
            this.row_q_PartyName.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_PartyName.Appearance.Options.UseFont = true;
            this.row_q_PartyName.Height = 27;
            this.row_q_PartyName.Name = "row_q_PartyName";
            this.row_q_PartyName.Properties.Caption = "Партия";
            this.row_q_PartyName.Properties.FieldName = "idParty.Name";
            this.row_q_PartyName.Properties.ReadOnly = true;
            this.row_q_PartyName.Properties.RowEdit = this.repositoryItemComboBox1;
            this.row_q_PartyName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.row_q_PartyName.Properties.Value = "Партия 1";
            // 
            // cat_q_Seat
            // 
            this.cat_q_Seat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_q_MicNum,
            this.row_q_RowNum,
            this.row_q_SeatNum});
            this.cat_q_Seat.Name = "cat_q_Seat";
            // 
            // row_q_MicNum
            // 
            this.row_q_MicNum.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_MicNum.Appearance.Options.UseFont = true;
            this.row_q_MicNum.Height = 30;
            this.row_q_MicNum.Name = "row_q_MicNum";
            this.row_q_MicNum.Properties.Caption = "Мик. №";
            this.row_q_MicNum.Properties.ReadOnly = true;
            this.row_q_MicNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // row_q_RowNum
            // 
            this.row_q_RowNum.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_RowNum.Appearance.Options.UseFont = true;
            this.row_q_RowNum.Name = "row_q_RowNum";
            this.row_q_RowNum.Properties.Caption = "Ряд №";
            this.row_q_RowNum.Properties.ReadOnly = true;
            this.row_q_RowNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // row_q_SeatNum
            // 
            this.row_q_SeatNum.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.row_q_SeatNum.Appearance.Options.UseFont = true;
            this.row_q_SeatNum.Height = 16;
            this.row_q_SeatNum.Name = "row_q_SeatNum";
            this.row_q_SeatNum.Properties.Caption = "Место №";
            this.row_q_SeatNum.Properties.ReadOnly = true;
            this.row_q_SeatNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // pnlQuorumButtons
            // 
            this.pnlQuorumButtons.Controls.Add(this.lblQuorumFinished);
            this.pnlQuorumButtons.Controls.Add(this.btnRegFinish);
            this.pnlQuorumButtons.Controls.Add(this.txtCardQnty);
            this.pnlQuorumButtons.Controls.Add(this.labelControl4);
            this.pnlQuorumButtons.Controls.Add(this.lblRegTime);
            this.pnlQuorumButtons.Controls.Add(this.progressBarRegistration);
            this.pnlQuorumButtons.Controls.Add(this.txtQuorumQnty);
            this.pnlQuorumButtons.Controls.Add(this.labelControl20);
            this.pnlQuorumButtons.Controls.Add(this.txtRegQnty);
            this.pnlQuorumButtons.Controls.Add(this.labelControl19);
            this.pnlQuorumButtons.Controls.Add(this.txtDelegateQnty);
            this.pnlQuorumButtons.Controls.Add(this.lblCardQnty);
            this.pnlQuorumButtons.Controls.Add(this.lblRegistration);
            this.pnlQuorumButtons.Controls.Add(this.picRegTime);
            this.pnlQuorumButtons.Controls.Add(this.btnRegStart);
            this.pnlQuorumButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlQuorumButtons.Location = new System.Drawing.Point(0, 715);
            this.pnlQuorumButtons.Name = "pnlQuorumButtons";
            this.pnlQuorumButtons.Size = new System.Drawing.Size(1609, 116);
            this.pnlQuorumButtons.TabIndex = 30;
            // 
            // lblQuorumFinished
            // 
            this.lblQuorumFinished.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQuorumFinished.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblQuorumFinished.Appearance.Options.UseFont = true;
            this.lblQuorumFinished.Appearance.Options.UseForeColor = true;
            this.lblQuorumFinished.Location = new System.Drawing.Point(1101, 28);
            this.lblQuorumFinished.Name = "lblQuorumFinished";
            this.lblQuorumFinished.Size = new System.Drawing.Size(107, 16);
            this.lblQuorumFinished.TabIndex = 242;
            this.lblQuorumFinished.Text = "Кворум достигнут!";
            this.lblQuorumFinished.Visible = false;
            // 
            // btnRegFinish
            // 
            this.btnRegFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRegFinish.Appearance.Options.UseFont = true;
            this.btnRegFinish.ImageOptions.ImageIndex = 7;
            this.btnRegFinish.ImageOptions.ImageList = this.ButtonImages;
            this.btnRegFinish.Location = new System.Drawing.Point(42, 70);
            this.btnRegFinish.Name = "btnRegFinish";
            this.btnRegFinish.Size = new System.Drawing.Size(194, 34);
            this.btnRegFinish.TabIndex = 258;
            this.btnRegFinish.Text = "Завершить регистрацию";
            this.btnRegFinish.Click += new System.EventHandler(this.btnRegFinish_Click);
            // 
            // txtCardQnty
            // 
            this.txtCardQnty.Location = new System.Drawing.Point(818, 26);
            this.txtCardQnty.Name = "txtCardQnty";
            this.txtCardQnty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtCardQnty.Properties.Appearance.Options.UseFont = true;
            this.txtCardQnty.Size = new System.Drawing.Size(49, 22);
            this.txtCardQnty.TabIndex = 245;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(708, 81);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(89, 16);
            this.labelControl4.TabIndex = 244;
            this.labelControl4.Text = "Общее кол-во:";
            // 
            // lblRegTime
            // 
            this.lblRegTime.Appearance.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRegTime.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblRegTime.Appearance.Options.UseFont = true;
            this.lblRegTime.Appearance.Options.UseForeColor = true;
            this.lblRegTime.Location = new System.Drawing.Point(333, 78);
            this.lblRegTime.Name = "lblRegTime";
            this.lblRegTime.Size = new System.Drawing.Size(42, 15);
            this.lblRegTime.TabIndex = 243;
            this.lblRegTime.Text = "00 : 00";
            // 
            // progressBarRegistration
            // 
            this.progressBarRegistration.Location = new System.Drawing.Point(408, 73);
            this.progressBarRegistration.Name = "progressBarRegistration";
            this.progressBarRegistration.Size = new System.Drawing.Size(216, 24);
            this.progressBarRegistration.StyleController = this.styleController1;
            this.progressBarRegistration.TabIndex = 240;
            // 
            // txtQuorumQnty
            // 
            this.txtQuorumQnty.Location = new System.Drawing.Point(1036, 25);
            this.txtQuorumQnty.Name = "txtQuorumQnty";
            this.txtQuorumQnty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtQuorumQnty.Properties.Appearance.Options.UseFont = true;
            this.txtQuorumQnty.Size = new System.Drawing.Size(49, 22);
            this.txtQuorumQnty.TabIndex = 239;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Location = new System.Drawing.Point(905, 29);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(122, 16);
            this.labelControl20.TabIndex = 238;
            this.labelControl20.Text = "Кол-во для кворума:";
            // 
            // txtRegQnty
            // 
            this.txtRegQnty.Location = new System.Drawing.Point(1036, 78);
            this.txtRegQnty.Name = "txtRegQnty";
            this.txtRegQnty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtRegQnty.Properties.Appearance.Options.UseFont = true;
            this.txtRegQnty.Size = new System.Drawing.Size(49, 22);
            this.txtRegQnty.TabIndex = 237;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Location = new System.Drawing.Point(905, 81);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(126, 16);
            this.labelControl19.TabIndex = 236;
            this.labelControl19.Text = "Зарегистрировалось:";
            // 
            // txtDelegateQnty
            // 
            this.txtDelegateQnty.Location = new System.Drawing.Point(818, 78);
            this.txtDelegateQnty.Name = "txtDelegateQnty";
            this.txtDelegateQnty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtDelegateQnty.Properties.Appearance.Options.UseFont = true;
            this.txtDelegateQnty.Size = new System.Drawing.Size(49, 22);
            this.txtDelegateQnty.TabIndex = 235;
            // 
            // lblCardQnty
            // 
            this.lblCardQnty.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCardQnty.Appearance.Options.UseFont = true;
            this.lblCardQnty.Location = new System.Drawing.Point(708, 29);
            this.lblCardQnty.Name = "lblCardQnty";
            this.lblCardQnty.Size = new System.Drawing.Size(102, 16);
            this.lblCardQnty.TabIndex = 234;
            this.lblCardQnty.Text = "Кол-во карточек:";
            // 
            // lblRegistration
            // 
            this.lblRegistration.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRegistration.Appearance.Options.UseFont = true;
            this.lblRegistration.Location = new System.Drawing.Point(417, 34);
            this.lblRegistration.Name = "lblRegistration";
            this.lblRegistration.Size = new System.Drawing.Size(117, 16);
            this.lblRegistration.TabIndex = 229;
            this.lblRegistration.Text = "Идет регистрация...";
            // 
            // picRegTime
            // 
            this.picRegTime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picRegTime.EditValue = ((object)(resources.GetObject("picRegTime.EditValue")));
            this.picRegTime.Location = new System.Drawing.Point(333, 23);
            this.picRegTime.Name = "picRegTime";
            this.picRegTime.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picRegTime.Properties.Appearance.Options.UseBackColor = true;
            this.picRegTime.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picRegTime.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.picRegTime.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picRegTime.Size = new System.Drawing.Size(35, 47);
            this.picRegTime.TabIndex = 228;
            // 
            // btnRegStart
            // 
            this.btnRegStart.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRegStart.Appearance.Options.UseFont = true;
            this.btnRegStart.ImageOptions.ImageIndex = 9;
            this.btnRegStart.ImageOptions.ImageList = this.ButtonImages;
            this.btnRegStart.Location = new System.Drawing.Point(43, 18);
            this.btnRegStart.Name = "btnRegStart";
            this.btnRegStart.Size = new System.Drawing.Size(193, 36);
            this.btnRegStart.TabIndex = 230;
            this.btnRegStart.Text = "Определение кворума";
            this.btnRegStart.Click += new System.EventHandler(this.btnRegStart_Click);
            // 
            // pgQuestions
            // 
            this.pgQuestions.Controls.Add(this.splitContainerControl5);
            this.pgQuestions.Name = "pgQuestions";
            this.pgQuestions.PageVisible = false;
            this.pgQuestions.Size = new System.Drawing.Size(1609, 831);
            this.pgQuestions.Text = "Вопросы для голосования";
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.panelControl4);
            this.splitContainerControl5.Panel1.Controls.Add(this.panelControl2);
            this.splitContainerControl5.Panel1.Controls.Add(this.btnQuestVoteStart);
            this.splitContainerControl5.Panel1.Controls.Add(this.btnQuestVoteDetail);
            this.splitContainerControl5.Panel1.Controls.Add(this.btnQuestNewRead);
            this.splitContainerControl5.Panel1.Controls.Add(this.navigatorResQuestions);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.grpResQuestProps);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(1609, 831);
            this.splitContainerControl5.SplitterPosition = 962;
            this.splitContainerControl5.TabIndex = 0;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.VoteQuestGrid);
            this.panelControl4.Controls.Add(this.panelControl5);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 263);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(962, 496);
            this.panelControl4.TabIndex = 244;
            // 
            // VoteQuestGrid
            // 
            this.VoteQuestGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VoteQuestGrid.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.VoteQuestGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.VoteQuestGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.VoteQuestGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.VoteQuestGrid.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.VoteQuestGrid.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.VoteQuestGrid.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VoteQuestGrid.Location = new System.Drawing.Point(2, 2);
            this.VoteQuestGrid.MainView = this.GridViewQuestions;
            this.VoteQuestGrid.Name = "VoteQuestGrid";
            this.VoteQuestGrid.Size = new System.Drawing.Size(958, 454);
            this.VoteQuestGrid.TabIndex = 241;
            this.VoteQuestGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridViewQuestions});
            this.VoteQuestGrid.DoubleClick += new System.EventHandler(this.VoteQuestGrid_DoubleClick);
            // 
            // GridViewQuestions
            // 
            this.GridViewQuestions.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_R,
            this.colCaption_R,
            this.colVoteTime_R,
            this.gridColumn8,
            this.gridColumn1,
            this.colTaskState,
            this.colResult,
            this.gridColumn2});
            this.GridViewQuestions.CustomizationFormBounds = new System.Drawing.Rectangle(1004, 568, 216, 178);
            this.GridViewQuestions.GridControl = this.VoteQuestGrid;
            this.GridViewQuestions.GroupCount = 1;
            this.GridViewQuestions.Name = "GridViewQuestions";
            this.GridViewQuestions.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridViewQuestions.OptionsBehavior.Editable = false;
            this.GridViewQuestions.OptionsDetail.AutoZoomDetail = true;
            this.GridViewQuestions.OptionsDetail.EnableMasterViewMode = false;
            this.GridViewQuestions.OptionsView.ShowGroupPanel = false;
            this.GridViewQuestions.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTaskState, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID_R
            // 
            this.colID_R.Caption = "№ п. повестки ";
            this.colID_R.FieldName = "idTask.ItemNum";
            this.colID_R.Name = "colID_R";
            this.colID_R.OptionsColumn.FixedWidth = true;
            this.colID_R.Visible = true;
            this.colID_R.VisibleIndex = 0;
            this.colID_R.Width = 60;
            // 
            // colCaption_R
            // 
            this.colCaption_R.Caption = "Заголовок";
            this.colCaption_R.FieldName = "Name";
            this.colCaption_R.MinWidth = 255;
            this.colCaption_R.Name = "colCaption_R";
            this.colCaption_R.Visible = true;
            this.colCaption_R.VisibleIndex = 1;
            this.colCaption_R.Width = 570;
            // 
            // colVoteTime_R
            // 
            this.colVoteTime_R.Caption = "Дата голос.";
            this.colVoteTime_R.DisplayFormat.FormatString = "H:mm dd/MM";
            this.colVoteTime_R.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colVoteTime_R.FieldName = "idResult.VoteDateTime";
            this.colVoteTime_R.Name = "colVoteTime_R";
            this.colVoteTime_R.Visible = true;
            this.colVoteTime_R.VisibleIndex = 2;
            this.colVoteTime_R.Width = 87;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Результат";
            this.gridColumn8.FieldName = "idResult.idResultValue.Name";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            this.gridColumn8.Width = 90;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Очередь";
            this.gridColumn1.FieldName = "QueueNum";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 5;
            this.gridColumn1.Width = 68;
            // 
            // colTaskState
            // 
            this.colTaskState.Caption = " ";
            this.colTaskState.FieldName = "VoteStatus";
            this.colTaskState.Name = "colTaskState";
            this.colTaskState.OptionsColumn.ShowCaption = false;
            // 
            // colResult
            // 
            this.colResult.Caption = "Result";
            this.colResult.FieldName = "idResult.IsResult";
            this.colResult.Name = "colResult";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Чтение";
            this.gridColumn2.FieldName = "idReadNum.Name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 4;
            this.gridColumn2.Width = 62;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.navControlResQuestions);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl5.Location = new System.Drawing.Point(2, 456);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(958, 38);
            this.panelControl5.TabIndex = 242;
            // 
            // navControlResQuestions
            // 
            this.navControlResQuestions.Buttons.Append.Visible = false;
            this.navControlResQuestions.Buttons.CancelEdit.Visible = false;
            this.navControlResQuestions.Buttons.Edit.Visible = false;
            this.navControlResQuestions.Buttons.EndEdit.Visible = false;
            this.navControlResQuestions.Buttons.ImageList = this.ButtonImages;
            this.navControlResQuestions.Buttons.Remove.Visible = false;
            this.navControlResQuestions.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Добавить вопрос", "Append"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Удалить из списка", "Remove"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Внести изменения", "Edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "вверх по списку", "Queue_Up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 12, true, true, "вниз по списку", "Queue_Down"),
            new DevExpress.XtraEditors.NavigatorCustomButton()});
            this.navControlResQuestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navControlResQuestions.Location = new System.Drawing.Point(2, 2);
            this.navControlResQuestions.Name = "navControlResQuestions";
            this.navControlResQuestions.NavigatableControl = this.VoteQuestGrid;
            this.navControlResQuestions.Size = new System.Drawing.Size(954, 34);
            this.navControlResQuestions.TabIndex = 243;
            this.navControlResQuestions.Text = "controlNavigator1";
            this.navControlResQuestions.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.navControlVoteQuestions_ButtonClick);
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.panelControl2.Controls.Add(this.labelControl18);
            this.panelControl2.Controls.Add(this.edtAmendmentType);
            this.panelControl2.Controls.Add(this.simpleButton3);
            this.panelControl2.Controls.Add(this.edtAmendment);
            this.panelControl2.Controls.Add(this.labelControl149);
            this.panelControl2.Controls.Add(this.edtReadingNum);
            this.panelControl2.Controls.Add(this.btnNewAgendaItem);
            this.panelControl2.Controls.Add(this.edtTaskItem);
            this.panelControl2.Controls.Add(this.txtVoteCaption);
            this.panelControl2.Controls.Add(this.labelControl148);
            this.panelControl2.Controls.Add(this.btnNewVoteFast);
            this.panelControl2.Controls.Add(this.cmbQuestKind);
            this.panelControl2.Controls.Add(this.labelControl147);
            this.panelControl2.Controls.Add(this.memoDescription);
            this.panelControl2.Controls.Add(this.labelControl146);
            this.panelControl2.Controls.Add(this.labelControl40);
            this.panelControl2.Controls.Add(this.labelControl39);
            this.panelControl2.Controls.Add(this.btnNewAdd);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(962, 263);
            this.panelControl2.TabIndex = 243;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(728, 128);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(17, 13);
            this.labelControl18.TabIndex = 263;
            this.labelControl18.Text = "№:";
            // 
            // edtAmendmentType
            // 
            this.edtAmendmentType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtAmendmentType.Location = new System.Drawing.Point(563, 146);
            this.edtAmendmentType.Name = "edtAmendmentType";
            this.edtAmendmentType.Properties.AutoHeight = false;
            this.edtAmendmentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtAmendmentType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Номер чтения", 33, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.edtAmendmentType.Properties.DataSource = this.xpAmendmentType;
            this.edtAmendmentType.Properties.DisplayMember = "Name";
            this.edtAmendmentType.Properties.NullText = "";
            this.edtAmendmentType.Properties.ShowHeader = false;
            this.edtAmendmentType.Properties.ValueMember = "id";
            this.edtAmendmentType.Size = new System.Drawing.Size(154, 32);
            this.edtAmendmentType.TabIndex = 262;
            // 
            // xpAmendmentType
            // 
            this.xpAmendmentType.DeleteObjectOnRemove = true;
            this.xpAmendmentType.ObjectType = typeof(VoteSystem.AmendmentTypeObj);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.ImageOptions.ImageIndex = 13;
            this.simpleButton3.ImageOptions.ImageList = this.ButtonImages;
            this.simpleButton3.Location = new System.Drawing.Point(28, 204);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(272, 39);
            this.simpleButton3.TabIndex = 261;
            this.simpleButton3.Text = "Голосовать сразу!";
            this.simpleButton3.Click += new System.EventHandler(this.btnNewVoteFast_Click);
            // 
            // edtAmendment
            // 
            this.edtAmendment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtAmendment.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.edtAmendment.Location = new System.Drawing.Point(719, 147);
            this.edtAmendment.Name = "edtAmendment";
            this.edtAmendment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edtAmendment.Properties.Appearance.Options.UseFont = true;
            this.edtAmendment.Properties.AutoHeight = false;
            this.edtAmendment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtAmendment.Properties.IsFloatValue = false;
            this.edtAmendment.Properties.Mask.EditMask = "N00";
            this.edtAmendment.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.edtAmendment.Size = new System.Drawing.Size(43, 31);
            this.edtAmendment.TabIndex = 260;
            this.edtAmendment.Click += new System.EventHandler(this.edtAmendment_Click);
            // 
            // labelControl149
            // 
            this.labelControl149.Location = new System.Drawing.Point(592, 128);
            this.labelControl149.Name = "labelControl149";
            this.labelControl149.Size = new System.Drawing.Size(53, 13);
            this.labelControl149.TabIndex = 258;
            this.labelControl149.Text = "Поправка:";
            // 
            // edtReadingNum
            // 
            this.edtReadingNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtReadingNum.Location = new System.Drawing.Point(331, 146);
            this.edtReadingNum.Name = "edtReadingNum";
            this.edtReadingNum.Properties.AutoHeight = false;
            this.edtReadingNum.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtReadingNum.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Номер чтения", 33, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.edtReadingNum.Properties.DataSource = this.xpReadNum;
            this.edtReadingNum.Properties.DisplayMember = "Name";
            this.edtReadingNum.Properties.NullText = "";
            this.edtReadingNum.Properties.ShowHeader = false;
            this.edtReadingNum.Properties.ValueMember = "id";
            this.edtReadingNum.Size = new System.Drawing.Size(197, 32);
            this.edtReadingNum.TabIndex = 257;
            this.edtReadingNum.Click += new System.EventHandler(this.edtReadingNum_Click);
            // 
            // xpReadNum
            // 
            this.xpReadNum.DeleteObjectOnRemove = true;
            this.xpReadNum.ObjectType = typeof(VoteSystem.ReadNumObj);
            // 
            // btnNewAgendaItem
            // 
            this.btnNewAgendaItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewAgendaItem.Location = new System.Drawing.Point(795, 31);
            this.btnNewAgendaItem.Name = "btnNewAgendaItem";
            this.btnNewAgendaItem.Size = new System.Drawing.Size(133, 33);
            this.btnNewAgendaItem.TabIndex = 256;
            this.btnNewAgendaItem.Text = "Очистить";
            this.btnNewAgendaItem.Click += new System.EventHandler(this.btnNewAgendaItem_Click);
            // 
            // edtTaskItem
            // 
            this.edtTaskItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edtTaskItem.Location = new System.Drawing.Point(28, 30);
            this.edtTaskItem.Name = "edtTaskItem";
            this.edtTaskItem.Properties.AutoHeight = false;
            this.edtTaskItem.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtTaskItem.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ItemNumStr", "№", 15, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VoteStatus", "Статус", 25, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Caption", "Название", 250, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SortCriteria", "Sort Criteria", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("QueueNum", "Queue Num", 10, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Описание", 59, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.edtTaskItem.Properties.DataSource = this.xpTasks;
            this.edtTaskItem.Properties.DisplayMember = "CaptionItem";
            this.edtTaskItem.Properties.DropDownRows = 30;
            this.edtTaskItem.Properties.ImmediatePopup = true;
            this.edtTaskItem.Properties.NullText = "";
            this.edtTaskItem.Properties.PopupFormMinSize = new System.Drawing.Size(1100, 0);
            this.edtTaskItem.Properties.SortColumnIndex = 5;
            this.edtTaskItem.Properties.ValueMember = "id";
            this.edtTaskItem.Size = new System.Drawing.Size(734, 33);
            this.edtTaskItem.TabIndex = 255;
            this.edtTaskItem.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.edtTaskItem_QueryPopUp);
            this.edtTaskItem.EditValueChanged += new System.EventHandler(this.edtTaskItem_EditValueChanged);
            this.edtTaskItem.Click += new System.EventHandler(this.edtTaskItem_Click);
            // 
            // txtVoteCaption
            // 
            this.txtVoteCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVoteCaption.Location = new System.Drawing.Point(28, 89);
            this.txtVoteCaption.Name = "txtVoteCaption";
            this.txtVoteCaption.Properties.AutoHeight = false;
            this.txtVoteCaption.Size = new System.Drawing.Size(900, 33);
            this.txtVoteCaption.TabIndex = 254;
            this.txtVoteCaption.Click += new System.EventHandler(this.txtVoteCaption_Click);
            // 
            // labelControl148
            // 
            this.labelControl148.Location = new System.Drawing.Point(42, 11);
            this.labelControl148.Name = "labelControl148";
            this.labelControl148.Size = new System.Drawing.Size(107, 13);
            this.labelControl148.TabIndex = 253;
            this.labelControl148.Text = "Пункт повестки дня:";
            // 
            // btnNewVoteFast
            // 
            this.btnNewVoteFast.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewVoteFast.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNewVoteFast.Appearance.Options.UseFont = true;
            this.btnNewVoteFast.ImageOptions.ImageIndex = 13;
            this.btnNewVoteFast.ImageOptions.ImageList = this.ButtonImages;
            this.btnNewVoteFast.Location = new System.Drawing.Point(346, 204);
            this.btnNewVoteFast.Name = "btnNewVoteFast";
            this.btnNewVoteFast.Size = new System.Drawing.Size(264, 39);
            this.btnNewVoteFast.TabIndex = 251;
            this.btnNewVoteFast.Text = "Голосовать!";
            this.btnNewVoteFast.Click += new System.EventHandler(this.btnNewVote_Click);
            // 
            // cmbQuestKind
            // 
            this.cmbQuestKind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbQuestKind.Location = new System.Drawing.Point(28, 146);
            this.cmbQuestKind.Name = "cmbQuestKind";
            this.cmbQuestKind.Properties.AutoHeight = false;
            this.cmbQuestKind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbQuestKind.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Название", 120, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("idQntyType.Name", "Вид подсчета", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cmbQuestKind.Properties.DataSource = this.xpTaskKinds;
            this.cmbQuestKind.Properties.DisplayMember = "Name";
            this.cmbQuestKind.Properties.DropDownRows = 10;
            this.cmbQuestKind.Properties.NullText = "";
            this.cmbQuestKind.Properties.PopupFormMinSize = new System.Drawing.Size(400, 0);
            this.cmbQuestKind.Properties.ValueMember = "Name";
            this.cmbQuestKind.Size = new System.Drawing.Size(272, 32);
            this.cmbQuestKind.TabIndex = 250;
            this.cmbQuestKind.Click += new System.EventHandler(this.cmbQuestKind_Click);
            // 
            // xpTaskKinds
            // 
            this.xpTaskKinds.CriteriaString = "[id] > 0 And [IsDel] = False";
            this.xpTaskKinds.DeleteObjectOnRemove = true;
            this.xpTaskKinds.DisplayableProperties = "This;id;Name;idVoteKind!;idVoteKind!Key;idVoteKind;idVoteType!;idVoteType!Key;idV" +
    "oteType;idQntyType.Name;idQntyType!Key;idQntyType";
            this.xpTaskKinds.ObjectType = typeof(VoteSystem.TaskKindObj);
            // 
            // labelControl147
            // 
            this.labelControl147.Location = new System.Drawing.Point(804, 129);
            this.labelControl147.Name = "labelControl147";
            this.labelControl147.Size = new System.Drawing.Size(53, 13);
            this.labelControl147.TabIndex = 248;
            this.labelControl147.Text = "Описание:";
            // 
            // memoDescription
            // 
            this.memoDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoDescription.EditValue = "нннннннннннннн пппп  сссссссссс иииииииииииии\r\nффффффф вввввввввв ббббббббб ссссс" +
    "ссссссс\r\nууууу ееееееее ееееееее нннннн гггггг\r\nккае";
            this.memoDescription.Location = new System.Drawing.Point(795, 147);
            this.memoDescription.Name = "memoDescription";
            this.memoDescription.Properties.AutoHeight = false;
            this.memoDescription.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoDescription.Properties.HideSelection = false;
            this.memoDescription.Properties.PopupFormSize = new System.Drawing.Size(350, 150);
            this.memoDescription.Size = new System.Drawing.Size(133, 31);
            this.memoDescription.TabIndex = 247;
            this.memoDescription.Click += new System.EventHandler(this.memoDescription_Click);
            // 
            // labelControl146
            // 
            this.labelControl146.Location = new System.Drawing.Point(345, 129);
            this.labelControl146.Name = "labelControl146";
            this.labelControl146.Size = new System.Drawing.Size(41, 13);
            this.labelControl146.TabIndex = 246;
            this.labelControl146.Text = "Чтение:";
            // 
            // labelControl40
            // 
            this.labelControl40.Location = new System.Drawing.Point(42, 129);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(67, 13);
            this.labelControl40.TabIndex = 245;
            this.labelControl40.Text = "Вид вопроса:";
            // 
            // labelControl39
            // 
            this.labelControl39.Location = new System.Drawing.Point(42, 69);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(101, 13);
            this.labelControl39.TabIndex = 244;
            this.labelControl39.Text = "Заголовок вопроса:";
            // 
            // btnNewAdd
            // 
            this.btnNewAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNewAdd.Appearance.Options.UseFont = true;
            this.btnNewAdd.ImageOptions.ImageIndex = 3;
            this.btnNewAdd.ImageOptions.ImageList = this.ButtonImages;
            this.btnNewAdd.Location = new System.Drawing.Point(664, 204);
            this.btnNewAdd.Name = "btnNewAdd";
            this.btnNewAdd.Size = new System.Drawing.Size(264, 39);
            this.btnNewAdd.TabIndex = 6;
            this.btnNewAdd.Text = "Добавить в список";
            this.btnNewAdd.Click += new System.EventHandler(this.btnNewAdd_Click);
            // 
            // btnQuestVoteStart
            // 
            this.btnQuestVoteStart.ImageOptions.ImageIndex = 13;
            this.btnQuestVoteStart.ImageOptions.ImageList = this.ButtonImages;
            this.btnQuestVoteStart.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnQuestVoteStart.Location = new System.Drawing.Point(281, 769);
            this.btnQuestVoteStart.Name = "btnQuestVoteStart";
            this.btnQuestVoteStart.Size = new System.Drawing.Size(207, 38);
            this.btnQuestVoteStart.TabIndex = 241;
            this.btnQuestVoteStart.Text = "Голосовать из списка";
            this.btnQuestVoteStart.Click += new System.EventHandler(this.btnQuestVoteStart_Click);
            // 
            // btnQuestVoteDetail
            // 
            this.btnQuestVoteDetail.ImageOptions.ImageIndex = 0;
            this.btnQuestVoteDetail.ImageOptions.ImageList = this.ButtonImages;
            this.btnQuestVoteDetail.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnQuestVoteDetail.Location = new System.Drawing.Point(534, 769);
            this.btnQuestVoteDetail.Name = "btnQuestVoteDetail";
            this.btnQuestVoteDetail.Size = new System.Drawing.Size(207, 38);
            this.btnQuestVoteDetail.TabIndex = 239;
            this.btnQuestVoteDetail.Text = "Итоги голосования ...";
            this.btnQuestVoteDetail.Click += new System.EventHandler(this.btnQuestVoteDetail_Click);
            // 
            // btnQuestNewRead
            // 
            this.btnQuestNewRead.ImageOptions.ImageIndex = 3;
            this.btnQuestNewRead.ImageOptions.ImageList = this.ButtonImages;
            this.btnQuestNewRead.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnQuestNewRead.Location = new System.Drawing.Point(28, 769);
            this.btnQuestNewRead.Name = "btnQuestNewRead";
            this.btnQuestNewRead.Size = new System.Drawing.Size(207, 38);
            this.btnQuestNewRead.TabIndex = 237;
            this.btnQuestNewRead.Text = "Новый вопрос";
            this.btnQuestNewRead.Click += new System.EventHandler(this.btnQuestNewRead_Click);
            // 
            // navigatorResQuestions
            // 
            this.navigatorResQuestions.Buttons.Append.Visible = false;
            this.navigatorResQuestions.Buttons.CancelEdit.Visible = false;
            this.navigatorResQuestions.Buttons.EndEdit.Visible = false;
            this.navigatorResQuestions.Buttons.Remove.Visible = false;
            this.navigatorResQuestions.DataSource = this.xpResQuestions;
            this.navigatorResQuestions.Location = new System.Drawing.Point(279, 762);
            this.navigatorResQuestions.Name = "navigatorResQuestions";
            this.navigatorResQuestions.Size = new System.Drawing.Size(151, 33);
            this.navigatorResQuestions.TabIndex = 235;
            this.navigatorResQuestions.Text = "dataNavigator4";
            this.navigatorResQuestions.Visible = false;
            this.navigatorResQuestions.PositionChanged += new System.EventHandler(this.navigatorVoteQuestions_PositionChanged);
            // 
            // xpResQuestions
            // 
            this.xpResQuestions.DeleteObjectOnRemove = true;
            this.xpResQuestions.DisplayableProperties = "This;id;idSession;idTask;Notes;idTask.QueueNum";
            this.xpResQuestions.ObjectType = typeof(VoteSystem.QuestionObj);
            // 
            // grpResQuestProps
            // 
            this.grpResQuestProps.Controls.Add(this.grpResQuestPropsIn);
            this.grpResQuestProps.Controls.Add(this.groupControl6);
            this.grpResQuestProps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpResQuestProps.Location = new System.Drawing.Point(0, 0);
            this.grpResQuestProps.Name = "grpResQuestProps";
            this.grpResQuestProps.Size = new System.Drawing.Size(641, 831);
            this.grpResQuestProps.TabIndex = 235;
            this.grpResQuestProps.Text = "Окно свойств";
            // 
            // grpResQuestPropsIn
            // 
            this.grpResQuestPropsIn.Controls.Add(this.PropertiesControlQuest2);
            this.grpResQuestPropsIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpResQuestPropsIn.Location = new System.Drawing.Point(2, 22);
            this.grpResQuestPropsIn.Name = "grpResQuestPropsIn";
            this.grpResQuestPropsIn.ShowCaption = false;
            this.grpResQuestPropsIn.Size = new System.Drawing.Size(637, 563);
            this.grpResQuestPropsIn.TabIndex = 9;
            this.grpResQuestPropsIn.Text = "groupControl1";
            // 
            // PropertiesControlQuest2
            // 
            this.PropertiesControlQuest2.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest2.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest2.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest2.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlQuest2.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlQuest2.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest2.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.PropertiesControlQuest2.DataSource = this.xpResQuestions;
            this.PropertiesControlQuest2.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControlQuest2.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlQuest2.Location = new System.Drawing.Point(2, 2);
            this.PropertiesControlQuest2.Name = "PropertiesControlQuest2";
            this.PropertiesControlQuest2.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControlQuest2.RecordWidth = 116;
            this.PropertiesControlQuest2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3,
            this.repositoryItemGridLookUpEdit4,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox15,
            this.repositoryItemComboBox16,
            this.repositoryItemComboBox18,
            this.repositoryItemComboBox19,
            this.repositoryItemComboBox20,
            this.repositoryItemMemoEdit1,
            this.repositoryItemMemoEdit4,
            this.repositoryItemTextEdit8});
            this.PropertiesControlQuest2.RowHeaderWidth = 84;
            this.PropertiesControlQuest2.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catCharacteristics_R,
            this.catNotes_R,
            this.catResult_R,
            this.categoryRow2});
            this.PropertiesControlQuest2.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlQuest2.Size = new System.Drawing.Size(633, 536);
            this.PropertiesControlQuest2.TabIndex = 18;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemGridLookUpEdit4
            // 
            this.repositoryItemGridLookUpEdit4.AutoHeight = false;
            this.repositoryItemGridLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit4.Name = "repositoryItemGridLookUpEdit4";
            this.repositoryItemGridLookUpEdit4.PopupView = this.gridView5;
            // 
            // gridView5
            // 
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.ImmediatePopup = true;
            this.repositoryItemComboBox3.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox15
            // 
            this.repositoryItemComboBox15.AutoHeight = false;
            this.repositoryItemComboBox15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox15.ImmediatePopup = true;
            this.repositoryItemComboBox15.Name = "repositoryItemComboBox15";
            this.repositoryItemComboBox15.Sorted = true;
            // 
            // repositoryItemComboBox16
            // 
            this.repositoryItemComboBox16.AutoHeight = false;
            this.repositoryItemComboBox16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox16.Name = "repositoryItemComboBox16";
            // 
            // repositoryItemComboBox18
            // 
            this.repositoryItemComboBox18.AutoHeight = false;
            this.repositoryItemComboBox18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox18.ImmediatePopup = true;
            this.repositoryItemComboBox18.Name = "repositoryItemComboBox18";
            // 
            // repositoryItemComboBox19
            // 
            this.repositoryItemComboBox19.AutoHeight = false;
            this.repositoryItemComboBox19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox19.Name = "repositoryItemComboBox19";
            // 
            // repositoryItemComboBox20
            // 
            this.repositoryItemComboBox20.AutoHeight = false;
            this.repositoryItemComboBox20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox20.Name = "repositoryItemComboBox20";
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // repositoryItemMemoEdit4
            // 
            this.repositoryItemMemoEdit4.Name = "repositoryItemMemoEdit4";
            // 
            // repositoryItemTextEdit8
            // 
            this.repositoryItemTextEdit8.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.repositoryItemTextEdit8.Appearance.Options.UseForeColor = true;
            this.repositoryItemTextEdit8.AutoHeight = false;
            this.repositoryItemTextEdit8.Name = "repositoryItemTextEdit8";
            // 
            // catCharacteristics_R
            // 
            this.catCharacteristics_R.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowCaption_R,
            this.rowTaskItem_R,
            this.rowidKind_Name_R,
            this.rowCrDate_R,
            this.rowReadNum_R,
            this.rowAmendment_R,
            this.rowDescription_R});
            this.catCharacteristics_R.Height = 19;
            this.catCharacteristics_R.Name = "catCharacteristics_R";
            this.catCharacteristics_R.Properties.Caption = "Характеристики";
            // 
            // rowCaption_R
            // 
            this.rowCaption_R.Height = 121;
            this.rowCaption_R.Name = "rowCaption_R";
            this.rowCaption_R.Properties.Caption = "Заголовок";
            this.rowCaption_R.Properties.FieldName = "Name";
            this.rowCaption_R.Properties.ReadOnly = true;
            this.rowCaption_R.Properties.RowEdit = this.repositoryItemMemoEdit4;
            this.rowCaption_R.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowTaskItem_R
            // 
            this.rowTaskItem_R.Height = 55;
            this.rowTaskItem_R.Name = "rowTaskItem_R";
            this.rowTaskItem_R.Properties.Caption = "Пункт повестки";
            this.rowTaskItem_R.Properties.FieldName = "idTask.CaptionItem";
            this.rowTaskItem_R.Properties.ReadOnly = true;
            this.rowTaskItem_R.Properties.RowEdit = this.repositoryItemMemoEdit4;
            // 
            // rowidKind_Name_R
            // 
            this.rowidKind_Name_R.Height = 28;
            this.rowidKind_Name_R.Name = "rowidKind_Name_R";
            this.rowidKind_Name_R.Properties.Caption = "Вид";
            this.rowidKind_Name_R.Properties.FieldName = "idKind.Name";
            this.rowidKind_Name_R.Properties.ReadOnly = true;
            this.rowidKind_Name_R.Properties.RowEdit = this.repositoryItemComboBox20;
            // 
            // rowCrDate_R
            // 
            this.rowCrDate_R.Height = 37;
            this.rowCrDate_R.Name = "rowCrDate_R";
            this.rowCrDate_R.Properties.Caption = "Дата/время";
            this.rowCrDate_R.Properties.FieldName = "CrDate";
            this.rowCrDate_R.Properties.ReadOnly = true;
            // 
            // rowReadNum_R
            // 
            this.rowReadNum_R.Height = 26;
            this.rowReadNum_R.Name = "rowReadNum_R";
            this.rowReadNum_R.Properties.Caption = "Чтение";
            this.rowReadNum_R.Properties.FieldName = "idReadNum.Name";
            this.rowReadNum_R.Properties.ReadOnly = true;
            // 
            // rowAmendment_R
            // 
            this.rowAmendment_R.Height = 28;
            this.rowAmendment_R.Name = "rowAmendment_R";
            this.rowAmendment_R.Properties.Caption = "Поправка";
            this.rowAmendment_R.Properties.FieldName = "AmendmentStr";
            this.rowAmendment_R.Properties.ReadOnly = true;
            // 
            // rowDescription_R
            // 
            this.rowDescription_R.Height = 85;
            this.rowDescription_R.Name = "rowDescription_R";
            this.rowDescription_R.Properties.Caption = "Описание";
            this.rowDescription_R.Properties.FieldName = "idTask.Description";
            this.rowDescription_R.Properties.ReadOnly = true;
            this.rowDescription_R.Properties.RowEdit = this.repositoryItemMemoEdit1;
            // 
            // catNotes_R
            // 
            this.catNotes_R.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowNotes_R});
            this.catNotes_R.Height = 19;
            this.catNotes_R.Name = "catNotes_R";
            this.catNotes_R.Properties.Caption = "Заметки";
            // 
            // rowNotes_R
            // 
            this.rowNotes_R.Height = 86;
            this.rowNotes_R.Name = "rowNotes_R";
            this.rowNotes_R.Properties.FieldName = "Notes";
            this.rowNotes_R.Properties.ReadOnly = true;
            this.rowNotes_R.Properties.RowEdit = this.repositoryItemMemoEdit1;
            // 
            // catResult_R
            // 
            this.catResult_R.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.catResult_R.Appearance.Options.UseFont = true;
            this.catResult_R.Height = 20;
            this.catResult_R.Name = "catResult_R";
            this.catResult_R.Properties.Caption = "Результат";
            this.catResult_R.Visible = false;
            // 
            // categoryRow2
            // 
            this.categoryRow2.Name = "categoryRow2";
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.labelControl16);
            this.groupControl6.Controls.Add(this.lblVoteDateTime);
            this.groupControl6.Controls.Add(this.labelControl22);
            this.groupControl6.Controls.Add(this.lblProcQnty);
            this.groupControl6.Controls.Add(this.grpVoteResults);
            this.groupControl6.Controls.Add(this.lblDecision);
            this.groupControl6.Controls.Add(this.label77);
            this.groupControl6.Controls.Add(this.lblVoteQnty);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl6.Location = new System.Drawing.Point(2, 585);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(637, 244);
            this.groupControl6.TabIndex = 8;
            this.groupControl6.Text = "Итоги голосования";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(193, 43);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(34, 13);
            this.labelControl16.TabIndex = 228;
            this.labelControl16.Text = "Время:";
            // 
            // lblVoteDateTime
            // 
            this.lblVoteDateTime.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteDateTime.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblVoteDateTime.Location = new System.Drawing.Point(233, 27);
            this.lblVoteDateTime.Name = "lblVoteDateTime";
            this.lblVoteDateTime.Size = new System.Drawing.Size(168, 16);
            this.lblVoteDateTime.TabIndex = 227;
            this.lblVoteDateTime.Text = "16:30";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(34, 43);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(48, 13);
            this.labelControl22.TabIndex = 215;
            this.labelControl22.Text = "Решение:";
            // 
            // lblProcQnty
            // 
            this.lblProcQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcQnty.Location = new System.Drawing.Point(253, 215);
            this.lblProcQnty.Name = "lblProcQnty";
            this.lblProcQnty.Size = new System.Drawing.Size(46, 17);
            this.lblProcQnty.TabIndex = 212;
            this.lblProcQnty.Text = "70 %";
            this.lblProcQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // grpVoteResults
            // 
            this.grpVoteResults.Controls.Add(this.label107);
            this.grpVoteResults.Controls.Add(this.label106);
            this.grpVoteResults.Controls.Add(this.label105);
            this.grpVoteResults.Controls.Add(this.label104);
            this.grpVoteResults.Controls.Add(this.label103);
            this.grpVoteResults.Controls.Add(this.lblProcAye);
            this.grpVoteResults.Controls.Add(this.lblVoteAye);
            this.grpVoteResults.Controls.Add(this.label101);
            this.grpVoteResults.Controls.Add(this.lblNonProcQnty);
            this.grpVoteResults.Controls.Add(this.lblNonVoteQnty);
            this.grpVoteResults.Controls.Add(this.label95);
            this.grpVoteResults.Controls.Add(this.lblProcAgainst);
            this.grpVoteResults.Controls.Add(this.label79);
            this.grpVoteResults.Controls.Add(this.label80);
            this.grpVoteResults.Controls.Add(this.lblProcAbstain);
            this.grpVoteResults.Controls.Add(this.lblVoteAgainst);
            this.grpVoteResults.Controls.Add(this.label83);
            this.grpVoteResults.Controls.Add(this.label84);
            this.grpVoteResults.Controls.Add(this.label85);
            this.grpVoteResults.Controls.Add(this.label86);
            this.grpVoteResults.Controls.Add(this.label87);
            this.grpVoteResults.Controls.Add(this.label88);
            this.grpVoteResults.Controls.Add(this.lblVoteAbstain);
            this.grpVoteResults.Controls.Add(this.label90);
            this.grpVoteResults.Controls.Add(this.domainUpDown5);
            this.grpVoteResults.Controls.Add(this.label91);
            this.grpVoteResults.Location = new System.Drawing.Point(30, 41);
            this.grpVoteResults.Name = "grpVoteResults";
            this.grpVoteResults.Size = new System.Drawing.Size(316, 171);
            this.grpVoteResults.TabIndex = 203;
            this.grpVoteResults.TabStop = false;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Enabled = false;
            this.label107.ForeColor = System.Drawing.Color.DimGray;
            this.label107.Location = new System.Drawing.Point(6, 127);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(17, 13);
            this.label107.TabIndex = 184;
            this.label107.Text = "5.";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Enabled = false;
            this.label106.ForeColor = System.Drawing.Color.DimGray;
            this.label106.Location = new System.Drawing.Point(6, 106);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(17, 13);
            this.label106.TabIndex = 183;
            this.label106.Text = "4.";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 84);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(17, 13);
            this.label105.TabIndex = 182;
            this.label105.Text = "3.";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 62);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(17, 13);
            this.label104.TabIndex = 181;
            this.label104.Text = "2.";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 39);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(17, 13);
            this.label103.TabIndex = 180;
            this.label103.Text = "1.";
            // 
            // lblProcAye
            // 
            this.lblProcAye.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAye.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblProcAye.Location = new System.Drawing.Point(223, 39);
            this.lblProcAye.Name = "lblProcAye";
            this.lblProcAye.Size = new System.Drawing.Size(46, 17);
            this.lblProcAye.TabIndex = 179;
            this.lblProcAye.Text = "8";
            this.lblProcAye.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVoteAye
            // 
            this.lblVoteAye.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAye.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblVoteAye.Location = new System.Drawing.Point(147, 39);
            this.lblVoteAye.Name = "lblVoteAye";
            this.lblVoteAye.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAye.TabIndex = 178;
            this.lblVoteAye.Text = "8";
            this.lblVoteAye.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label101.ForeColor = System.Drawing.Color.DarkGreen;
            this.label101.Location = new System.Drawing.Point(44, 40);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(23, 14);
            this.label101.TabIndex = 177;
            this.label101.Text = "ЗА";
            // 
            // lblNonProcQnty
            // 
            this.lblNonProcQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNonProcQnty.Location = new System.Drawing.Point(223, 149);
            this.lblNonProcQnty.Name = "lblNonProcQnty";
            this.lblNonProcQnty.Size = new System.Drawing.Size(46, 17);
            this.lblNonProcQnty.TabIndex = 176;
            this.lblNonProcQnty.Text = "8";
            this.lblNonProcQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblNonVoteQnty
            // 
            this.lblNonVoteQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNonVoteQnty.Location = new System.Drawing.Point(157, 150);
            this.lblNonVoteQnty.Name = "lblNonVoteQnty";
            this.lblNonVoteQnty.Size = new System.Drawing.Size(29, 16);
            this.lblNonVoteQnty.TabIndex = 175;
            this.lblNonVoteQnty.Text = "8";
            this.lblNonVoteQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label95.ForeColor = System.Drawing.Color.Purple;
            this.label95.Location = new System.Drawing.Point(44, 150);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(94, 14);
            this.label95.TabIndex = 174;
            this.label95.Text = "Не голосовали";
            // 
            // lblProcAgainst
            // 
            this.lblProcAgainst.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAgainst.ForeColor = System.Drawing.Color.Maroon;
            this.lblProcAgainst.Location = new System.Drawing.Point(223, 61);
            this.lblProcAgainst.Name = "lblProcAgainst";
            this.lblProcAgainst.Size = new System.Drawing.Size(46, 17);
            this.lblProcAgainst.TabIndex = 173;
            this.lblProcAgainst.Text = "8";
            this.lblProcAgainst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label79.Location = new System.Drawing.Point(223, 127);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(46, 17);
            this.label79.TabIndex = 172;
            this.label79.Text = "9";
            this.label79.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label79.Visible = false;
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label80.Location = new System.Drawing.Point(223, 105);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(46, 17);
            this.label80.TabIndex = 171;
            this.label80.Text = "8";
            this.label80.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label80.Visible = false;
            // 
            // lblProcAbstain
            // 
            this.lblProcAbstain.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAbstain.ForeColor = System.Drawing.Color.Orange;
            this.lblProcAbstain.Location = new System.Drawing.Point(223, 83);
            this.lblProcAbstain.Name = "lblProcAbstain";
            this.lblProcAbstain.Size = new System.Drawing.Size(46, 17);
            this.lblProcAbstain.TabIndex = 170;
            this.lblProcAbstain.Text = "8";
            this.lblProcAbstain.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVoteAgainst
            // 
            this.lblVoteAgainst.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAgainst.ForeColor = System.Drawing.Color.Maroon;
            this.lblVoteAgainst.Location = new System.Drawing.Point(147, 61);
            this.lblVoteAgainst.Name = "lblVoteAgainst";
            this.lblVoteAgainst.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAgainst.TabIndex = 169;
            this.lblVoteAgainst.Text = "8";
            this.lblVoteAgainst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label83.Location = new System.Drawing.Point(147, 127);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(46, 17);
            this.label83.TabIndex = 168;
            this.label83.Text = "9";
            this.label83.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label83.Visible = false;
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label84.Location = new System.Drawing.Point(147, 105);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(46, 17);
            this.label84.TabIndex = 167;
            this.label84.Text = "8";
            this.label84.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label84.Visible = false;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label85.Location = new System.Drawing.Point(223, 15);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(62, 14);
            this.label85.TabIndex = 166;
            this.label85.Text = "Процент:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label86.Location = new System.Drawing.Point(140, 15);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(51, 14);
            this.label86.TabIndex = 165;
            this.label86.Text = "Голоса:";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(44, 127);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(48, 13);
            this.label87.TabIndex = 164;
            this.label87.Text = "Ответ 5";
            this.label87.Visible = false;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(44, 106);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(48, 13);
            this.label88.TabIndex = 163;
            this.label88.Text = "Ответ 4";
            this.label88.Visible = false;
            // 
            // lblVoteAbstain
            // 
            this.lblVoteAbstain.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAbstain.ForeColor = System.Drawing.Color.Orange;
            this.lblVoteAbstain.Location = new System.Drawing.Point(147, 83);
            this.lblVoteAbstain.Name = "lblVoteAbstain";
            this.lblVoteAbstain.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAbstain.TabIndex = 155;
            this.lblVoteAbstain.Text = "8";
            this.lblVoteAbstain.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label90.ForeColor = System.Drawing.Color.Maroon;
            this.label90.Location = new System.Drawing.Point(44, 62);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(57, 14);
            this.label90.TabIndex = 153;
            this.label90.Text = "ПРОТИВ";
            // 
            // domainUpDown5
            // 
            this.domainUpDown5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.domainUpDown5.Location = new System.Drawing.Point(80, 400);
            this.domainUpDown5.Name = "domainUpDown5";
            this.domainUpDown5.Size = new System.Drawing.Size(77, 21);
            this.domainUpDown5.TabIndex = 145;
            this.domainUpDown5.Text = "2";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label91.ForeColor = System.Drawing.Color.Orange;
            this.label91.Location = new System.Drawing.Point(44, 84);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(57, 14);
            this.label91.TabIndex = 139;
            this.label91.Text = "ВОЗДЕР.";
            // 
            // lblDecision
            // 
            this.lblDecision.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDecision.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblDecision.Location = new System.Drawing.Point(90, 26);
            this.lblDecision.Name = "lblDecision";
            this.lblDecision.Size = new System.Drawing.Size(113, 22);
            this.lblDecision.TabIndex = 200;
            this.lblDecision.Text = "Не принято";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(74, 231);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(70, 13);
            this.label77.TabIndex = 209;
            this.label77.Text = "Голосовали:";
            // 
            // lblVoteQnty
            // 
            this.lblVoteQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteQnty.Location = new System.Drawing.Point(149, 215);
            this.lblVoteQnty.Name = "lblVoteQnty";
            this.lblVoteQnty.Size = new System.Drawing.Size(64, 17);
            this.lblVoteQnty.TabIndex = 210;
            this.lblVoteQnty.Text = "8 из 12";
            this.lblVoteQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pgStatements
            // 
            this.pgStatements.Controls.Add(this.splitContainerControl6);
            this.pgStatements.Name = "pgStatements";
            this.pgStatements.PageVisible = false;
            this.pgStatements.Size = new System.Drawing.Size(1609, 831);
            this.pgStatements.Text = "Выступления";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl6.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.btnStateClose);
            this.splitContainerControl6.Panel1.Controls.Add(this.btnStateResult);
            this.splitContainerControl6.Panel1.Controls.Add(this.navControlStatements);
            this.splitContainerControl6.Panel1.Controls.Add(this.btnStatementMode);
            this.splitContainerControl6.Panel1.Controls.Add(this.navigatorStatements);
            this.splitContainerControl6.Panel1.Controls.Add(this.SelStateGrid);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(1609, 831);
            this.splitContainerControl6.SplitterPosition = 940;
            this.splitContainerControl6.TabIndex = 1;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // btnStateClose
            // 
            this.btnStateClose.ImageOptions.ImageList = this.ButtonImages;
            this.btnStateClose.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnStateClose.Location = new System.Drawing.Point(509, 762);
            this.btnStateClose.Name = "btnStateClose";
            this.btnStateClose.Size = new System.Drawing.Size(201, 38);
            this.btnStateClose.TabIndex = 241;
            this.btnStateClose.Text = "Завершить выступления";
            this.btnStateClose.Click += new System.EventHandler(this.btnStateClose_Click);
            // 
            // btnStateResult
            // 
            this.btnStateResult.ImageOptions.ImageIndex = 0;
            this.btnStateResult.ImageOptions.ImageList = this.ButtonImages;
            this.btnStateResult.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnStateResult.Location = new System.Drawing.Point(267, 762);
            this.btnStateResult.Name = "btnStateResult";
            this.btnStateResult.Size = new System.Drawing.Size(201, 38);
            this.btnStateResult.TabIndex = 240;
            this.btnStateResult.Text = "Просмотр итогов";
            this.btnStateResult.Click += new System.EventHandler(this.btnStateResult_Click);
            // 
            // navControlStatements
            // 
            this.navControlStatements.Buttons.Append.Visible = false;
            this.navControlStatements.Buttons.CancelEdit.Visible = false;
            this.navControlStatements.Buttons.Edit.Visible = false;
            this.navControlStatements.Buttons.EndEdit.Visible = false;
            this.navControlStatements.Buttons.ImageList = this.ButtonImages;
            this.navControlStatements.Buttons.Remove.Visible = false;
            this.navControlStatements.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Добавить вопрос", "Append"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Удалить из списка", "Remove"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Внести изменения", "Edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "вверх по списку", "Queue_Up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 12, true, true, "вниз по списку", "Queue_Down")});
            this.navControlStatements.Dock = System.Windows.Forms.DockStyle.Top;
            this.navControlStatements.Location = new System.Drawing.Point(0, 720);
            this.navControlStatements.Name = "navControlStatements";
            this.navControlStatements.NavigatableControl = this.SelStateGrid;
            this.navControlStatements.Size = new System.Drawing.Size(940, 32);
            this.navControlStatements.TabIndex = 239;
            this.navControlStatements.Text = "controlNavigator1";
            this.navControlStatements.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.navControlStatements_ButtonClick);
            // 
            // SelStateGrid
            // 
            this.SelStateGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.SelStateGrid.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.SelStateGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.SelStateGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.SelStateGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.SelStateGrid.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.SelStateGrid.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.SelStateGrid.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SelStateGrid.Location = new System.Drawing.Point(0, 0);
            this.SelStateGrid.MainView = this.gridViewSelState;
            this.SelStateGrid.Name = "SelStateGrid";
            this.SelStateGrid.Size = new System.Drawing.Size(940, 720);
            this.SelStateGrid.TabIndex = 234;
            this.SelStateGrid.UseEmbeddedNavigator = true;
            this.SelStateGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSelState});
            this.SelStateGrid.DoubleClick += new System.EventHandler(this.SelStateGrid_DoubleClick);
            // 
            // gridViewSelState
            // 
            this.gridViewSelState.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridViewSelState.Appearance.Row.Options.UseFont = true;
            this.gridViewSelState.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.colTaskCaption,
            this.colItemStatus,
            this.colQueueNum_,
            this.colSortCriteria,
            this.IsFinished});
            this.gridViewSelState.CustomizationFormBounds = new System.Drawing.Rectangle(938, 491, 216, 183);
            this.gridViewSelState.GridControl = this.SelStateGrid;
            this.gridViewSelState.GroupCount = 1;
            this.gridViewSelState.Name = "gridViewSelState";
            this.gridViewSelState.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridViewSelState.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewSelState.OptionsBehavior.Editable = false;
            this.gridViewSelState.OptionsDetail.AutoZoomDetail = true;
            this.gridViewSelState.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewSelState.OptionsView.ShowGroupPanel = false;
            this.gridViewSelState.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSortCriteria, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Тема выступления";
            this.gridColumn10.FieldName = "Caption";
            this.gridColumn10.MinWidth = 300;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 1;
            this.gridColumn10.Width = 610;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Дата/время созд.";
            this.gridColumn11.DisplayFormat.FormatString = "H:mm (dd/MM/yy)";
            this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn11.FieldName = "CrDate";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 2;
            this.gridColumn11.Width = 128;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = " ";
            this.gridColumn12.FieldName = "Status";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ShowCaption = false;
            // 
            // colTaskCaption
            // 
            this.colTaskCaption.Caption = "П. повестки";
            this.colTaskCaption.FieldName = "idTask.ItemNumStr";
            this.colTaskCaption.Name = "colTaskCaption";
            this.colTaskCaption.Visible = true;
            this.colTaskCaption.VisibleIndex = 0;
            this.colTaskCaption.Width = 86;
            // 
            // colItemStatus
            // 
            this.colItemStatus.Caption = "Статус";
            this.colItemStatus.FieldName = "idTask.VoteStatus";
            this.colItemStatus.Name = "colItemStatus";
            this.colItemStatus.Visible = true;
            this.colItemStatus.VisibleIndex = 3;
            this.colItemStatus.Width = 95;
            // 
            // colQueueNum_
            // 
            this.colQueueNum_.Caption = "Очередь";
            this.colQueueNum_.FieldName = "QueueNum";
            this.colQueueNum_.Name = "colQueueNum_";
            this.colQueueNum_.Width = 71;
            // 
            // colSortCriteria
            // 
            this.colSortCriteria.Caption = "Сортировка";
            this.colSortCriteria.FieldName = "idTask.SortCriteria";
            this.colSortCriteria.Name = "colSortCriteria";
            this.colSortCriteria.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            // 
            // IsFinished
            // 
            this.IsFinished.Caption = "IsFinished";
            this.IsFinished.FieldName = "IsFinished";
            this.IsFinished.Name = "IsFinished";
            // 
            // btnStatementMode
            // 
            this.btnStatementMode.ImageOptions.ImageIndex = 10;
            this.btnStatementMode.ImageOptions.ImageList = this.ButtonImages;
            this.btnStatementMode.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnStatementMode.Location = new System.Drawing.Point(30, 762);
            this.btnStatementMode.Name = "btnStatementMode";
            this.btnStatementMode.Size = new System.Drawing.Size(201, 38);
            this.btnStatementMode.TabIndex = 236;
            this.btnStatementMode.Text = "Режим выступлений";
            this.btnStatementMode.Click += new System.EventHandler(this.btnStatemSpeechMode_Click);
            // 
            // navigatorStatements
            // 
            this.navigatorStatements.Buttons.Append.Visible = false;
            this.navigatorStatements.Buttons.CancelEdit.Visible = false;
            this.navigatorStatements.Buttons.EndEdit.Visible = false;
            this.navigatorStatements.Buttons.ImageList = this.ButtonImages;
            this.navigatorStatements.Buttons.Remove.Visible = false;
            this.navigatorStatements.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Добавить запись", "Append"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Удалить запись", "Remove"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Внести изменения", "Edit")});
            this.navigatorStatements.Location = new System.Drawing.Point(270, 777);
            this.navigatorStatements.Name = "navigatorStatements";
            this.navigatorStatements.Size = new System.Drawing.Size(212, 35);
            this.navigatorStatements.TabIndex = 235;
            this.navigatorStatements.Text = "dataNavigator1";
            this.navigatorStatements.Visible = false;
            this.navigatorStatements.PositionChanged += new System.EventHandler(this.navigatorStatements_PositionChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.PropertiesControlState);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(663, 831);
            this.groupControl1.TabIndex = 238;
            this.groupControl1.Tag = "";
            this.groupControl1.Text = "Панель свойств";
            // 
            // PropertiesControlState
            // 
            this.PropertiesControlState.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlState.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlState.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlState.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlState.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlState.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlState.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControlState.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlState.Location = new System.Drawing.Point(2, 22);
            this.PropertiesControlState.Name = "PropertiesControlState";
            this.PropertiesControlState.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControlState.RecordWidth = 126;
            this.PropertiesControlState.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit5,
            this.repositoryItemGridLookUpEdit5,
            this.repositoryItemComboBox30,
            this.repositoryItemComboBox31,
            this.repositoryItemComboBox32,
            this.repositoryItemComboBox33,
            this.repositoryItemComboBox34,
            this.repositoryItemComboBox35,
            this.repositoryItemDateEdit5,
            this.repositoryItemComboBox36,
            this.repositoryItemComboBox37,
            this.repositoryItemComboBox38,
            this.repositoryItemComboBox39,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemDateEdit7,
            this.repositoryItemMemoEdit5,
            this.repositoryItemComboBox40,
            this.repositoryItemSpinEdit3,
            this.repositoryItemComboBox41,
            this.repositoryItemComboBox42,
            this.repositoryItemMemoEdit6,
            this.repositoryItemMemoEdit7,
            this.repositoryItemCheckEdit4});
            this.PropertiesControlState.RowHeaderWidth = 74;
            this.PropertiesControlState.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catCharacteristics_st,
            this.catTotals_st,
            this.categoryRow6,
            this.categoryRow4});
            this.PropertiesControlState.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlState.Size = new System.Drawing.Size(659, 512);
            this.PropertiesControlState.TabIndex = 12;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // repositoryItemGridLookUpEdit5
            // 
            this.repositoryItemGridLookUpEdit5.AutoHeight = false;
            this.repositoryItemGridLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit5.Name = "repositoryItemGridLookUpEdit5";
            this.repositoryItemGridLookUpEdit5.PopupView = this.gridView11;
            // 
            // gridView11
            // 
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox30
            // 
            this.repositoryItemComboBox30.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox30.ImmediatePopup = true;
            this.repositoryItemComboBox30.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox30.Name = "repositoryItemComboBox30";
            // 
            // repositoryItemComboBox31
            // 
            this.repositoryItemComboBox31.AutoHeight = false;
            this.repositoryItemComboBox31.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox31.ImmediatePopup = true;
            this.repositoryItemComboBox31.Name = "repositoryItemComboBox31";
            this.repositoryItemComboBox31.Sorted = true;
            // 
            // repositoryItemComboBox32
            // 
            this.repositoryItemComboBox32.AutoHeight = false;
            this.repositoryItemComboBox32.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox32.Name = "repositoryItemComboBox32";
            // 
            // repositoryItemComboBox33
            // 
            this.repositoryItemComboBox33.AutoHeight = false;
            this.repositoryItemComboBox33.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox33.ImmediatePopup = true;
            this.repositoryItemComboBox33.Name = "repositoryItemComboBox33";
            // 
            // repositoryItemComboBox34
            // 
            this.repositoryItemComboBox34.AutoHeight = false;
            this.repositoryItemComboBox34.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox34.Name = "repositoryItemComboBox34";
            // 
            // repositoryItemComboBox35
            // 
            this.repositoryItemComboBox35.AutoHeight = false;
            this.repositoryItemComboBox35.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox35.Name = "repositoryItemComboBox35";
            // 
            // repositoryItemDateEdit5
            // 
            this.repositoryItemDateEdit5.AutoHeight = false;
            this.repositoryItemDateEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit5.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit5.Name = "repositoryItemDateEdit5";
            // 
            // repositoryItemComboBox36
            // 
            this.repositoryItemComboBox36.AutoHeight = false;
            this.repositoryItemComboBox36.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox36.Name = "repositoryItemComboBox36";
            // 
            // repositoryItemComboBox37
            // 
            this.repositoryItemComboBox37.AutoHeight = false;
            this.repositoryItemComboBox37.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox37.Name = "repositoryItemComboBox37";
            // 
            // repositoryItemComboBox38
            // 
            this.repositoryItemComboBox38.AutoHeight = false;
            this.repositoryItemComboBox38.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox38.Name = "repositoryItemComboBox38";
            // 
            // repositoryItemComboBox39
            // 
            this.repositoryItemComboBox39.AutoHeight = false;
            this.repositoryItemComboBox39.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox39.Name = "repositoryItemComboBox39";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            // 
            // repositoryItemDateEdit7
            // 
            this.repositoryItemDateEdit7.AutoHeight = false;
            this.repositoryItemDateEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit7.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit7.Name = "repositoryItemDateEdit7";
            // 
            // repositoryItemMemoEdit5
            // 
            this.repositoryItemMemoEdit5.Name = "repositoryItemMemoEdit5";
            // 
            // repositoryItemComboBox40
            // 
            this.repositoryItemComboBox40.AutoHeight = false;
            this.repositoryItemComboBox40.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox40.Name = "repositoryItemComboBox40";
            // 
            // repositoryItemComboBox41
            // 
            this.repositoryItemComboBox41.AutoHeight = false;
            this.repositoryItemComboBox41.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox41.Name = "repositoryItemComboBox41";
            // 
            // repositoryItemComboBox42
            // 
            this.repositoryItemComboBox42.AutoHeight = false;
            this.repositoryItemComboBox42.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox42.Name = "repositoryItemComboBox42";
            // 
            // repositoryItemMemoEdit6
            // 
            this.repositoryItemMemoEdit6.Name = "repositoryItemMemoEdit6";
            // 
            // repositoryItemMemoEdit7
            // 
            this.repositoryItemMemoEdit7.Name = "repositoryItemMemoEdit7";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.AutoWidth = true;
            this.repositoryItemCheckEdit4.Caption = " Отключать микрофон по истечению времени";
            this.repositoryItemCheckEdit4.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // catCharacteristics_st
            // 
            this.catCharacteristics_st.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowID_st,
            this.rowName_st,
            this.rowStartDate_st,
            this.rowDescription_st,
            this.rowStatus_st});
            this.catCharacteristics_st.Height = 19;
            this.catCharacteristics_st.Name = "catCharacteristics_st";
            this.catCharacteristics_st.Properties.Caption = "Характеристики";
            // 
            // rowID_st
            // 
            this.rowID_st.Height = 42;
            this.rowID_st.Name = "rowID_st";
            this.rowID_st.Properties.Caption = "№ (пункта)";
            this.rowID_st.Properties.FieldName = "idTask.CaptionItem";
            this.rowID_st.Properties.RowEdit = this.repositoryItemMemoEdit5;
            // 
            // rowName_st
            // 
            this.rowName_st.Height = 99;
            this.rowName_st.Name = "rowName_st";
            this.rowName_st.Properties.Caption = "Название темы";
            this.rowName_st.Properties.FieldName = "Caption";
            this.rowName_st.Properties.RowEdit = this.repositoryItemMemoEdit5;
            // 
            // rowStartDate_st
            // 
            this.rowStartDate_st.Height = 32;
            this.rowStartDate_st.Name = "rowStartDate_st";
            this.rowStartDate_st.Properties.Caption = "Начало обсуждений";
            this.rowStartDate_st.Properties.FieldName = "DateTimeStart";
            this.rowStartDate_st.Properties.Format.FormatString = "H:mm (dd/MM/yy)";
            this.rowStartDate_st.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // rowDescription_st
            // 
            this.rowDescription_st.Height = 71;
            this.rowDescription_st.Name = "rowDescription_st";
            this.rowDescription_st.Properties.Caption = "Описание";
            this.rowDescription_st.Properties.FieldName = "idTask.Description";
            this.rowDescription_st.Properties.RowEdit = this.repositoryItemMemoEdit5;
            // 
            // rowStatus_st
            // 
            this.rowStatus_st.Height = 25;
            this.rowStatus_st.Name = "rowStatus_st";
            this.rowStatus_st.Properties.Caption = "Состояние";
            this.rowStatus_st.Properties.FieldName = "Status";
            this.rowStatus_st.Properties.ReadOnly = true;
            // 
            // catTotals_st
            // 
            this.catTotals_st.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowDelegQnty_st,
            this.rowSpeechTime_st,
            this.rowSpeechTimeAvrg_st});
            this.catTotals_st.Name = "catTotals_st";
            this.catTotals_st.Properties.Caption = "Итоги";
            // 
            // rowDelegQnty_st
            // 
            this.rowDelegQnty_st.Height = 22;
            this.rowDelegQnty_st.Name = "rowDelegQnty_st";
            this.rowDelegQnty_st.Properties.Caption = "Выступили";
            this.rowDelegQnty_st.Properties.FieldName = "DelegateQnty_F";
            this.rowDelegQnty_st.Properties.Value = "7";
            // 
            // rowSpeechTime_st
            // 
            this.rowSpeechTime_st.Height = 21;
            this.rowSpeechTime_st.Name = "rowSpeechTime_st";
            this.rowSpeechTime_st.Properties.Caption = "Общее время";
            this.rowSpeechTime_st.Properties.FieldName = "SpeechTimeTotal_F";
            this.rowSpeechTime_st.Properties.Value = "12";
            // 
            // rowSpeechTimeAvrg_st
            // 
            this.rowSpeechTimeAvrg_st.Height = 21;
            this.rowSpeechTimeAvrg_st.Name = "rowSpeechTimeAvrg_st";
            this.rowSpeechTimeAvrg_st.Properties.Caption = "Среднее время";
            this.rowSpeechTimeAvrg_st.Properties.FieldName = "AvrgTime_F";
            this.rowSpeechTimeAvrg_st.Properties.Value = "1.75";
            // 
            // categoryRow6
            // 
            this.categoryRow6.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow25});
            this.categoryRow6.Name = "categoryRow6";
            this.categoryRow6.Properties.Caption = "Заметки";
            // 
            // editorRow25
            // 
            this.editorRow25.Height = 84;
            this.editorRow25.Name = "editorRow25";
            this.editorRow25.Properties.FieldName = "Notes";
            this.editorRow25.Properties.RowEdit = this.repositoryItemMemoEdit6;
            this.editorRow25.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow25.Properties.Value = "В процессе обсуждения был вынесен вопрос на голосвание под №143 О приватизации кв" +
    "артир в дородовском районе";
            // 
            // categoryRow4
            // 
            this.categoryRow4.Name = "categoryRow4";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1,
            this.dataTable2,
            this.dataTable3});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn2,
            this.dataColumn12});
            this.dataTable1.TableName = "Delegates";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "ФИО";
            this.dataColumn1.ColumnName = "FIO";
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "№ Мик.";
            this.dataColumn3.ColumnName = "Seat";
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Округ №";
            this.dataColumn4.ColumnName = "OkrugNum";
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "Название окргуа";
            this.dataColumn5.ColumnName = "OkrugName";
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Фракция";
            this.dataColumn2.ColumnName = "Party";
            // 
            // dataColumn12
            // 
            this.dataColumn12.Caption = "Присутствие";
            this.dataColumn12.ColumnName = "Registration";
            this.dataColumn12.MaxLength = 3;
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn6,
            this.dataColumn8,
            this.dataColumn7});
            this.dataTable2.TableName = "Questions";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "№ воп.";
            this.dataColumn6.ColumnName = "QuestNum";
            // 
            // dataColumn8
            // 
            this.dataColumn8.Caption = "Дата/время";
            this.dataColumn8.ColumnName = "DateTime";
            this.dataColumn8.DataType = typeof(System.DateTime);
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "Заголовок";
            this.dataColumn7.ColumnName = "Caption";
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.QuestNum,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn16});
            this.dataTable3.TableName = "Voting";
            // 
            // QuestNum
            // 
            this.QuestNum.Caption = "№ воп.";
            this.QuestNum.ColumnName = "Number";
            this.QuestNum.DataType = typeof(int);
            // 
            // dataColumn9
            // 
            this.dataColumn9.Caption = "Заголовок";
            this.dataColumn9.ColumnName = "Caption";
            // 
            // dataColumn10
            // 
            this.dataColumn10.Caption = "Дата/время гол.";
            this.dataColumn10.ColumnName = "DateTime";
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "Решение";
            this.dataColumn11.ColumnName = "Decision";
            // 
            // dataColumn16
            // 
            this.dataColumn16.Caption = "Кворум";
            this.dataColumn16.ColumnName = "Quorum";
            this.dataColumn16.DefaultValue = "Есть";
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(7, "geen ball gray.png");
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(48, 48);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // gridView3
            // 
            this.gridView3.Name = "gridView3";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(538, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 28);
            this.button2.TabIndex = 198;
            this.button2.Text = "Подробнее...";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.DarkGreen;
            this.label4.Location = new System.Drawing.Point(407, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 17);
            this.label4.TabIndex = 189;
            this.label4.Text = "Принято";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(627, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 17);
            this.label5.TabIndex = 197;
            this.label5.Text = "8";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(544, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 196;
            this.label6.Text = "Проголосовали:";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(487, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 17);
            this.label7.TabIndex = 195;
            this.label7.Text = "8";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(407, 178);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 194;
            this.label8.Text = "Присутствуют:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.label56);
            this.groupBox7.Controls.Add(this.domainUpDown3);
            this.groupBox7.Controls.Add(this.label57);
            this.groupBox7.Location = new System.Drawing.Point(401, 39);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(261, 136);
            this.groupBox7.TabIndex = 192;
            this.groupBox7.TabStop = false;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(185, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 17);
            this.label9.TabIndex = 173;
            this.label9.Text = "8";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(185, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 17);
            this.label10.TabIndex = 172;
            this.label10.Text = "8";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(185, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 17);
            this.label11.TabIndex = 171;
            this.label11.Text = "8";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(185, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 17);
            this.label13.TabIndex = 170;
            this.label13.Text = "8";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(86, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 17);
            this.label16.TabIndex = 169;
            this.label16.Text = "8";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(86, 113);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 17);
            this.label17.TabIndex = 168;
            this.label17.Text = "8";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(86, 86);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 17);
            this.label18.TabIndex = 167;
            this.label18.Text = "8";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(185, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 166;
            this.label19.Text = "Процент:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(86, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 13);
            this.label20.TabIndex = 165;
            this.label20.Text = "Голоса:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 111);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 13);
            this.label21.TabIndex = 164;
            this.label21.Text = "Не голос. :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 86);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 13);
            this.label22.TabIndex = 163;
            this.label22.Text = "ВОЗДЕР. :";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(86, 58);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 17);
            this.label23.TabIndex = 155;
            this.label23.Text = "8";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 38);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(24, 13);
            this.label56.TabIndex = 153;
            this.label56.Text = "ЗА:";
            // 
            // domainUpDown3
            // 
            this.domainUpDown3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.domainUpDown3.Location = new System.Drawing.Point(80, 400);
            this.domainUpDown3.Name = "domainUpDown3";
            this.domainUpDown3.Size = new System.Drawing.Size(77, 20);
            this.domainUpDown3.TabIndex = 145;
            this.domainUpDown3.Text = "2";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(6, 62);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(58, 13);
            this.label57.TabIndex = 139;
            this.label57.Text = "ПРОТИВ :";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(407, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(34, 13);
            this.label58.TabIndex = 193;
            this.label58.Text = "Итог:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label59);
            this.groupBox8.Controls.Add(this.label60);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this.label62);
            this.groupBox8.Controls.Add(this.label63);
            this.groupBox8.Controls.Add(this.label64);
            this.groupBox8.Controls.Add(this.label65);
            this.groupBox8.Controls.Add(this.label66);
            this.groupBox8.Controls.Add(this.label67);
            this.groupBox8.Controls.Add(this.label68);
            this.groupBox8.Controls.Add(this.label69);
            this.groupBox8.Controls.Add(this.label70);
            this.groupBox8.Controls.Add(this.domainUpDown4);
            this.groupBox8.Controls.Add(this.label71);
            this.groupBox8.Location = new System.Drawing.Point(5, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(326, 181);
            this.groupBox8.TabIndex = 190;
            this.groupBox8.TabStop = false;
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label59.Location = new System.Drawing.Point(86, 67);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(169, 17);
            this.label59.TabIndex = 164;
            this.label59.Text = "Открытое";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 68);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(64, 13);
            this.label60.TabIndex = 163;
            this.label60.Text = "Тип голос.:";
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label61.Location = new System.Drawing.Point(85, 159);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(170, 16);
            this.label61.TabIndex = 162;
            this.label61.Text = "До 2 мин";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 159);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(43, 13);
            this.label62.TabIndex = 161;
            this.label62.Text = "Время:";
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label63.Location = new System.Drawing.Point(85, 129);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(235, 30);
            this.label63.TabIndex = 160;
            this.label63.Text = "Простое большинство, голосов \"За\" более 50%";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 129);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(58, 13);
            this.label64.TabIndex = 159;
            this.label64.Text = "Критерий:";
            // 
            // label65
            // 
            this.label65.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label65.Location = new System.Drawing.Point(85, 107);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(170, 26);
            this.label65.TabIndex = 158;
            this.label65.Text = "От присутствующих > 75";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 107);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(43, 13);
            this.label66.TabIndex = 157;
            this.label66.Text = "Квота :";
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label67.Location = new System.Drawing.Point(85, 86);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(170, 17);
            this.label67.TabIndex = 156;
            this.label67.Text = "Для голосования";
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label68.Location = new System.Drawing.Point(86, 49);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(169, 17);
            this.label68.TabIndex = 155;
            this.label68.Text = "Парламентское голос.";
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label69.Location = new System.Drawing.Point(6, 8);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(314, 36);
            this.label69.TabIndex = 154;
            this.label69.Text = "Утверждение расходной части Утверждение расходной части Утверждение расходной час" +
    "ти";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 86);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(80, 13);
            this.label70.TabIndex = 151;
            this.label70.Text = "Тип вопроса:  ";
            // 
            // domainUpDown4
            // 
            this.domainUpDown4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.domainUpDown4.Location = new System.Drawing.Point(80, 400);
            this.domainUpDown4.Name = "domainUpDown4";
            this.domainUpDown4.Size = new System.Drawing.Size(77, 20);
            this.domainUpDown4.TabIndex = 145;
            this.domainUpDown4.Text = "2";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(6, 50);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(64, 13);
            this.label71.TabIndex = 139;
            this.label71.Text = "Вид голос.:";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(20, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(138, 13);
            this.label72.TabIndex = 191;
            this.label72.Text = "Характеристика вопроса:";
            // 
            // gridControl5
            // 
            this.gridControl5.DataMember = "Voting";
            this.gridControl5.DataSource = this.dataSet1;
            this.gridControl5.Location = new System.Drawing.Point(3, 199);
            this.gridControl5.MainView = this.gridView6;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(705, 320);
            this.gridControl5.TabIndex = 5;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView6.GridControl = this.gridControl5;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "№ воп.";
            this.gridColumn3.FieldName = "Number";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Заголовок";
            this.gridColumn4.FieldName = "Caption";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Дата/время гол.";
            this.gridColumn5.FieldName = "DateTime";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Результат";
            this.gridColumn6.FieldName = "Result";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barbtnExit);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Сервис";
            // 
            // PropertyDataSet
            // 
            this.PropertyDataSet.DataSetName = "NewDataSet";
            this.PropertyDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable4});
            // 
            // dataTable4
            // 
            this.dataTable4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15});
            this.dataTable4.TableName = "Delegates";
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "Фамилия";
            this.dataColumn13.ColumnName = "Family";
            // 
            // dataColumn14
            // 
            this.dataColumn14.Caption = "Имя";
            this.dataColumn14.ColumnName = "FirstName";
            // 
            // dataColumn15
            // 
            this.dataColumn15.Caption = "Отчество";
            this.dataColumn15.ColumnName = "SecondName";
            // 
            // repositoryItemPictureEdit4
            // 
            this.repositoryItemPictureEdit4.Name = "repositoryItemPictureEdit4";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // repositoryItemDateEdit4
            // 
            this.repositoryItemDateEdit4.AutoHeight = false;
            this.repositoryItemDateEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit4.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit4.Name = "repositoryItemDateEdit4";
            // 
            // editorRow35
            // 
            this.editorRow35.Height = 28;
            this.editorRow35.Name = "editorRow35";
            this.editorRow35.Properties.Caption = "Фамилия";
            this.editorRow35.Properties.FieldName = "Family";
            this.editorRow35.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow36
            // 
            this.editorRow36.Height = 28;
            this.editorRow36.Name = "editorRow36";
            this.editorRow36.Properties.Caption = "Имя";
            this.editorRow36.Properties.FieldName = "Name";
            this.editorRow36.Properties.ReadOnly = true;
            this.editorRow36.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow37
            // 
            this.editorRow37.Height = 26;
            this.editorRow37.Name = "editorRow37";
            this.editorRow37.Properties.Caption = "Отчество";
            this.editorRow37.Properties.FieldName = "SecondName";
            this.editorRow37.Properties.ReadOnly = true;
            // 
            // editorRow38
            // 
            this.editorRow38.Height = 80;
            this.editorRow38.Name = "editorRow38";
            this.editorRow38.Properties.Caption = "Фото";
            this.editorRow38.Properties.FieldName = "Photo";
            this.editorRow38.Properties.ReadOnly = true;
            this.editorRow38.Properties.RowEdit = this.repositoryItemPictureEdit4;
            // 
            // editorRow39
            // 
            this.editorRow39.Name = "editorRow39";
            this.editorRow39.Properties.Caption = "Фракция";
            this.editorRow39.Properties.FieldName = "Party";
            this.editorRow39.Properties.ReadOnly = true;
            // 
            // editorRow40
            // 
            this.editorRow40.Name = "editorRow40";
            this.editorRow40.Properties.Caption = "Округ";
            this.editorRow40.Properties.FieldName = "Okrug";
            this.editorRow40.Properties.ReadOnly = true;
            this.editorRow40.Properties.RowEdit = this.repositoryItemTextEdit4;
            // 
            // editorRow41
            // 
            this.editorRow41.Name = "editorRow41";
            this.editorRow41.Properties.Caption = "Округ №";
            // 
            // editorRow42
            // 
            this.editorRow42.Name = "editorRow42";
            this.editorRow42.Properties.Caption = "Регистрация";
            // 
            // editorRow43
            // 
            this.editorRow43.Name = "editorRow43";
            this.editorRow43.Properties.Caption = "Партия";
            // 
            // editorRow44
            // 
            this.editorRow44.Name = "editorRow44";
            this.editorRow44.Properties.Caption = "Должность";
            // 
            // editorRow45
            // 
            this.editorRow45.Name = "editorRow45";
            this.editorRow45.Properties.Caption = "Дата рождения";
            this.editorRow45.Properties.Format.FormatString = "d";
            this.editorRow45.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.editorRow45.Properties.RowEdit = this.repositoryItemDateEdit4;
            this.editorRow45.Properties.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            // 
            // editorRow46
            // 
            this.editorRow46.Name = "editorRow46";
            this.editorRow46.Properties.Caption = "Образование";
            // 
            // editorRow47
            // 
            this.editorRow47.Name = "editorRow47";
            this.editorRow47.Properties.Caption = "Статус";
            // 
            // propertyGridControl4
            // 
            this.propertyGridControl4.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.propertyGridControl4.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.propertyGridControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.propertyGridControl4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSet1, "Delegates.Registration", true));
            this.propertyGridControl4.DefaultEditors.AddRange(new DevExpress.XtraVerticalGrid.Rows.DefaultEditor[] {
            new DevExpress.XtraVerticalGrid.Rows.DefaultEditor(null, null)});
            this.propertyGridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridControl4.Location = new System.Drawing.Point(2, 20);
            this.propertyGridControl4.Name = "propertyGridControl4";
            this.propertyGridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit4,
            this.repositoryItemTextEdit4,
            this.repositoryItemDateEdit4});
            this.propertyGridControl4.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow35,
            this.editorRow36,
            this.editorRow37,
            this.editorRow38,
            this.editorRow39,
            this.editorRow40,
            this.editorRow41,
            this.editorRow42,
            this.editorRow43,
            this.editorRow44,
            this.editorRow45,
            this.editorRow46,
            this.editorRow47});
            this.propertyGridControl4.Size = new System.Drawing.Size(276, 659);
            this.propertyGridControl4.TabIndex = 6;
            // 
            // tmrRegStart
            // 
            this.tmrRegStart.Interval = 1000;
            this.tmrRegStart.Tick += new System.EventHandler(this.tmrRegStart_Tick);
            // 
            // TimerImages
            // 
            this.TimerImages.ImageSize = new System.Drawing.Size(96, 96);
            this.TimerImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("TimerImages.ImageStream")));
            // 
            // tmrRotation
            // 
            this.tmrRotation.Interval = 500;
            this.tmrRotation.Tick += new System.EventHandler(this.tmrRotation_Tick);
            // 
            // xpSessions
            // 
            this.xpSessions.DeleteObjectOnRemove = true;
            this.xpSessions.ObjectType = typeof(VoteSystem.SessionObj);
            this.xpSessions.Sorting.AddRange(new DevExpress.Xpo.SortProperty[] {
            new DevExpress.Xpo.SortProperty("[StartDate]", DevExpress.Xpo.DB.SortingDirection.Descending)});
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Печать";
            this.barButtonItem5.Id = 57;
            this.barButtonItem5.ImageOptions.LargeImageIndex = 15;
            this.barButtonItem5.LargeWidth = 90;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // tmrRegCardStart
            // 
            this.tmrRegCardStart.Interval = 1000;
            this.tmrRegCardStart.Tick += new System.EventHandler(this.tmrRegCardStart_Tick);
            // 
            // tmrGeneral
            // 
            this.tmrGeneral.Interval = 10000;
            this.tmrGeneral.Tick += new System.EventHandler(this.tmrGeneral_Tick);
            // 
            // tmrCardBackMode
            // 
            this.tmrCardBackMode.Interval = 1000;
            this.tmrCardBackMode.Tick += new System.EventHandler(this.tmrCardBackMode_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1884, 1001);
            this.Controls.Add(this.clientPanel);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Система голосования";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuMicSeat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientPanel)).EndInit();
            this.clientPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainTabControl)).EndInit();
            this.MainTabControl.ResumeLayout(false);
            this.pgSession.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpSesCharacteristics)).EndInit();
            this.grpSesCharacteristics.ResumeLayout(false);
            this.grpSesCharacteristics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpeechTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoReportTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFinishDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFinishDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbHistory)).EndInit();
            this.pgDelegates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertiesDelegate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).EndInit();
            this.pgSelQuestions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SelQuestGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSelQuest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSelQuestPropsIn)).EndInit();
            this.grpSelQuestPropsIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.pgQuorumGraphic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlHallSettings)).EndInit();
            this.pnlHallSettings.ResumeLayout(false);
            this.pnlHallSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinBackCardMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBackCardMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoCardMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDelegateSeat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHall)).EndInit();
            this.pnlHall.ResumeLayout(false);
            this.pnlHall.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl_QPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlQuorumButtons)).EndInit();
            this.pnlQuorumButtons.ResumeLayout(false);
            this.pnlQuorumButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarRegistration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuorumQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRegTime.Properties)).EndInit();
            this.pgQuestions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VoteQuestGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtAmendmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpAmendmentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtAmendment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtReadingNum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpReadNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtTaskItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteCaption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuestKind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpTaskKinds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpResQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpResQuestProps)).EndInit();
            this.grpResQuestProps.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpResQuestPropsIn)).EndInit();
            this.grpResQuestPropsIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            this.grpVoteResults.ResumeLayout(false);
            this.grpVoteResults.PerformLayout();
            this.pgStatements.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SelStateGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSelState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimerImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpSessions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraEditors.PanelControl clientPanel;
        private DevExpress.XtraBars.BarButtonItem barbtnPredsedSendMsg;
        private DevExpress.XtraBars.BarButtonItem barbtnMonitorMsg;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barbtnDelegates;
        private DevExpress.XtraBars.BarButtonItem barbtnQuestions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barbtnExit;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.Utils.ImageCollection RibbonImages;
        private DevExpress.Utils.ImageCollection NavBarImages;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataTable dataTable2;
        private System.Data.DataColumn dataColumn6;
        private DevExpress.XtraTab.XtraTabControl MainTabControl;
        private DevExpress.XtraTab.XtraTabPage pgSession;
        private DevExpress.XtraTab.XtraTabPage pgSelQuestions;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataTable dataTable3;
        private System.Data.DataColumn QuestNum;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private DevExpress.XtraTab.XtraTabPage pgQuestions;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.DomainUpDown domainUpDown3;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.DomainUpDown domainUpDown4;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraTab.XtraTabPage pgStatements;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraTab.XtraTabPage pgQuorumGraphic;
        private DevExpress.XtraEditors.PanelControl pnlQuorumButtons;
        private DevExpress.XtraEditors.TextEdit txtRegQnty;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtDelegateQnty;
        private DevExpress.XtraEditors.LabelControl lblCardQnty;
        private DevExpress.XtraEditors.LabelControl lblRegistration;
        private DevExpress.XtraEditors.PictureEdit picRegTime;
        private DevExpress.XtraEditors.SimpleButton btnRegStart;
        private DevExpress.XtraEditors.TextEdit txtQuorumQnty;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private System.Data.DataColumn dataColumn2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataSet PropertyDataSet;
        private System.Data.DataTable dataTable4;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn16;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.ListBoxControl lbHistory;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.Xpo.XPCollection xpDelegates;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow35;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow36;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow37;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow38;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow39;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow40;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow41;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow42;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow43;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow44;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow45;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow46;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow47;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridControl4;
        private DevExpress.Xpo.XPCollection xpTasks;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl pnlHall;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label92;
        private DevExpress.XtraEditors.ProgressBarControl progressBarRegistration;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControl_QPage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox7;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox17;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_LastName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_FirstName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_SecondName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_q_Region;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_RegionName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_RegionNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_q_Info;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_Fraction;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_PartyName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_q_Seat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_MicNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_RowNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_SeatNum;
        private DevExpress.XtraBars.PopupMenu menuMicSeat;
        private DevExpress.XtraBars.BarButtonItem cntSeatFree;
        private DevExpress.XtraBars.BarButtonItem cntSeatSelect;
        private DevExpress.XtraBars.BarEditItem cntSwitherPhysical;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.BarEditItem cntSwitherLogical;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraEditors.LabelControl lblQuorumFinished;
        private DevExpress.XtraEditors.LabelControl lblRegTime;
        private System.Windows.Forms.Timer tmrRegStart;
        private DevExpress.Utils.ImageCollection TimerImages;
        private System.Windows.Forms.Timer tmrRotation;
        private DevExpress.Xpo.XPCollection xpResQuestions;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.BarButtonItem barbtnInfoToMonitor;
        private DevExpress.XtraTab.XtraTabPage pgDelegates;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gridDelegates;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDelegates;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colFraction;
        private DevExpress.XtraGrid.Columns.GridColumn colPartyName;
        private DevExpress.XtraGrid.Columns.GridColumn colIsRegistered;
        private DevExpress.XtraEditors.DataNavigator navigatorDelegates;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraEditors.DataNavigator navigatorQuestions;
        private DevExpress.XtraGrid.GridControl SelQuestGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSelQuest;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestCaption;
        private DevExpress.XtraEditors.GroupControl grpSelQuestPropsIn;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraEditors.SimpleButton btnQuestVoteDetail;
        private DevExpress.XtraEditors.SimpleButton btnQuestNewRead;
        private DevExpress.XtraEditors.DataNavigator navigatorResQuestions;
        private DevExpress.XtraEditors.GroupControl grpResQuestProps;
        private DevExpress.XtraEditors.GroupControl grpResQuestPropsIn;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private System.Windows.Forms.Label lblVoteDateTime;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private System.Windows.Forms.Label lblProcQnty;
        private System.Windows.Forms.GroupBox grpVoteResults;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label lblProcAye;
        private System.Windows.Forms.Label lblVoteAye;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label lblNonProcQnty;
        private System.Windows.Forms.Label lblNonVoteQnty;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label lblProcAgainst;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label lblProcAbstain;
        private System.Windows.Forms.Label lblVoteAgainst;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label lblVoteAbstain;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.DomainUpDown domainUpDown5;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label lblDecision;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label lblVoteQnty;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarSubItem barSubItemSession;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem17;
        private DevExpress.XtraBars.BarButtonItem barButtonItem18;
        private DevExpress.XtraBars.BarButtonItem barButtonItem19;
        private DevExpress.XtraBars.BarSubItem barSubItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem20;
        private DevExpress.XtraBars.BarButtonItem barButtonItem21;
        private DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraBars.BarButtonItem barButtonItem23;
        private DevExpress.XtraBars.BarButtonItem barButtonItem24;
        private DevExpress.XtraBars.BarButtonItem barButtonItem25;
        private DevExpress.XtraBars.BarButtonItem barButtonItem26;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarButtonItem barbtnReport;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private DevExpress.XtraEditors.SimpleButton btnStatementMode;
        private DevExpress.XtraEditors.DataNavigator navigatorStatements;
        private DevExpress.XtraGrid.GridControl SelStateGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSelState;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlState;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox30;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox31;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox32;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox33;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox34;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox35;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox36;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox37;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox38;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox39;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox40;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox41;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox42;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit7;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catCharacteristics_st;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow6;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catTotals_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDelegQnty_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSpeechTime_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSpeechTimeAvrg_st;
        private DevExpress.Xpo.XPCollection xpSessions;
        private DevExpress.XtraGrid.Columns.GridColumn colCardRegistered;
        private DevExpress.XtraVerticalGrid.VGridControl propertiesDelegate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit9;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catFIO;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLastName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFirstName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSecondName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catRegion;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catInfo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowidParty_Name;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catSeat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMicNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRowNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSeatNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catNotes;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNotes;
        private DevExpress.XtraEditors.TextEdit txtCardQnty;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn colQueueNum;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlQuest;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox9;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox10;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox11;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox12;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox13;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catCharacteristics;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCaption;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowidKind_Name;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCrDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowReadNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDescription;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow8;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow26;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catResult;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowResult;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowResDate;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlQuest2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox15;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox16;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox18;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox19;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox20;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit8;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catCharacteristics_R;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCaption_R;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowidKind_Name_R;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCrDate_R;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowReadNum_R;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDescription_R;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catNotes_R;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNotes_R;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catResult_R;
        private DevExpress.XtraGrid.Columns.GridColumn colState;
        private DevExpress.XtraEditors.ControlNavigator navControlQuestions;
        private DevExpress.XtraEditors.SimpleButton btnRegFinish;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraBars.BarButtonItem barbtnSplashToMonitor;
        private DevExpress.XtraBars.BarCheckItem chkPanels;
        private DevExpress.XtraBars.BarButtonItem btnSettings;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraEditors.GroupControl grpSesCharacteristics;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.DateEdit txtFinishDate;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtVoteTime;
        private DevExpress.XtraEditors.LabelControl labelControl145;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtRegTime;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit txtStartDate;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit txtCaption;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowID_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowName_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStartDate_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDescription_st;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colTaskCaption;
        private DevExpress.XtraGrid.Columns.GridColumn colItemStatus;
        private DevExpress.XtraEditors.ControlNavigator navControlStatements;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStatus_st;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraBars.BarButtonItem cntRegDelegate;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.RichTextBox richContent;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit txtSpeechTime;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit txtCoReportTime;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtReportTime;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.SimpleButton btnQuestVoteStart;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnNewAdd;
        private DevExpress.XtraEditors.LabelControl labelControl146;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.GridControl VoteQuestGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView GridViewQuestions;
        private DevExpress.XtraGrid.Columns.GridColumn colID_R;
        private DevExpress.XtraGrid.Columns.GridColumn colCaption_R;
        private DevExpress.XtraGrid.Columns.GridColumn colVoteTime_R;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colTaskState;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.ControlNavigator navControlResQuestions;
        private DevExpress.XtraEditors.MemoExEdit memoDescription;
        private DevExpress.XtraEditors.LabelControl labelControl147;
        private DevExpress.Xpo.XPCollection xpTaskKinds;
        private DevExpress.XtraEditors.LookUpEdit cmbQuestKind;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit10;
        private DevExpress.XtraEditors.SimpleButton btnCardStart;
        private DevExpress.XtraEditors.SimpleButton btnStateResult;
        private DevExpress.XtraGrid.Columns.GridColumn colMicNum;
        private DevExpress.XtraBars.BarButtonItem cntRegCard;
        private System.Windows.Forms.Timer tmrRegCardStart;
        private DevExpress.XtraEditors.SimpleButton btnTextOut;
        private DevExpress.XtraEditors.SimpleButton btnActAllCards;
        private System.Windows.Forms.Timer tmrGeneral;
        private DevExpress.XtraBars.BarButtonItem btndrvStart;
        private DevExpress.XtraBars.BarButtonItem btndrvStop;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarButtonItem btndrvHitMics;
        private DevExpress.XtraEditors.SimpleButton btnNewVoteFast;
        private DevExpress.XtraEditors.PanelControl pnlHallSettings;
        private DevExpress.XtraEditors.SimpleButton btnClearSeats;
        private DevExpress.XtraEditors.CheckEdit chkAutoCardMode;
        private DevExpress.XtraEditors.CheckEdit chkDelegateSeat;
        private DevExpress.XtraBars.BarButtonItem barbtnMainMenu;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraEditors.LabelControl labelControl148;
        private DevExpress.XtraEditors.TextEdit txtVoteCaption;
        private DevExpress.XtraEditors.LookUpEdit edtTaskItem;
        private DevExpress.XtraEditors.SimpleButton btnNewAgendaItem;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTaskItem_R;
        private DevExpress.XtraEditors.LookUpEdit edtReadingNum;
        private DevExpress.Xpo.XPCollection xpReadNum;
        private DevExpress.XtraEditors.LabelControl labelControl149;
        private DevExpress.XtraEditors.SpinEdit edtAmendment;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmendment_R;
        private DevExpress.XtraGrid.Columns.GridColumn colItemNum;
        private DevExpress.XtraGrid.Columns.GridColumn colIsHot;
        private DevExpress.XtraGrid.Columns.GridColumn colResult;
        private DevExpress.XtraGrid.Columns.GridColumn colQueueNum_;
        private DevExpress.XtraBars.BarStaticItem barStaticText;
        private DevExpress.XtraEditors.LabelControl info_1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow3;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow4;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow5;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarItem itmAgenda;
        private DevExpress.XtraNavBar.NavBarItem itmDelegates;
        private DevExpress.XtraNavBar.NavBarItem itmAgendaItems;
        private DevExpress.XtraNavBar.NavBarItem itmRegistration;
        private DevExpress.XtraNavBar.NavBarItem itmQuestions;
        private DevExpress.XtraNavBar.NavBarItem itmReport;
        private DevExpress.XtraNavBar.NavBarItem itmStatements;
        private DevExpress.XtraNavBar.NavBarItem itmProtocol;
        private DevExpress.XtraNavBar.NavBarItem navBarItem10;
        private DevExpress.XtraNavBar.NavBarItem navBarItem11;
        private DevExpress.XtraNavBar.NavBarItem navBarItem12;
        private DevExpress.XtraEditors.StyleController styleController1;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarButtonItem cntSeatFix;
        private DevExpress.XtraEditors.CheckEdit chkBackCardMode;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SpinEdit spinBackCardMode;
        private DevExpress.XtraBars.BarStaticItem barProcesText;
        private DevExpress.XtraBars.BarListItem barHistoryList;
        private System.Windows.Forms.Timer tmrCardBackMode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDelegateType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowVoteType;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LookUpEdit edtAmendmentType;
        private DevExpress.Xpo.XPCollection xpAmendmentType;
        private DevExpress.XtraGrid.Columns.GridColumn colSortCriteria;
        private DevExpress.XtraGrid.Columns.GridColumn IsFinished;
        private DevExpress.XtraEditors.SimpleButton btnStateClose;
        private SeatComponent seatComponent1;
        private SeatComponent seatComponent2;
        private SeatComponent seatComponent4;
        private SeatComponent seatComponent3;
        private SeatComponent seatComponent18;
        private SeatComponent seatComponent19;
        private SeatComponent seatComponent14;
        private SeatComponent seatComponent15;
        private SeatComponent seatComponent16;
        private SeatComponent seatComponent17;
        private SeatComponent seatComponent13;
        private SeatComponent seatComponent12;
        private SeatComponent seatComponent11;
        private SeatComponent seatComponent10;
        private SeatComponent seatComponent9;
        private SeatComponent seatComponent8;
        private SeatComponent seatComponent7;
        private SeatComponent seatComponent6;
        private SeatComponent seatComponent5;
        private SeatComponent seatComponent31;
        private SeatComponent seatComponent27;
        private SeatComponent seatComponent20;
        private SeatComponent seatComponent30;
        private SeatComponent seatComponent26;
        private SeatComponent seatComponent21;
        private SeatComponent seatComponent29;
        private SeatComponent seatComponent25;
        private SeatComponent seatComponent22;
        private SeatComponent seatComponent28;
        private SeatComponent seatComponent24;
        private SeatComponent seatComponent23;
        private SeatComponent seatComponent33;
        private SeatComponent seatComponent32;
        private SeatComponent seatComponent139;
        private SeatComponent seatComponent121;
        private SeatComponent seatComponent86;
        private SeatComponent seatComponent138;
        private SeatComponent seatComponent137;
        private SeatComponent seatComponent120;
        private SeatComponent seatComponent119;
        private SeatComponent seatComponent136;
        private SeatComponent seatComponent87;
        private SeatComponent seatComponent118;
        private SeatComponent seatComponent135;
        private SeatComponent seatComponent88;
        private SeatComponent seatComponent117;
        private SeatComponent seatComponent134;
        private SeatComponent seatComponent89;
        private SeatComponent seatComponent116;
        private SeatComponent seatComponent133;
        private SeatComponent seatComponent90;
        private SeatComponent seatComponent115;
        private SeatComponent seatComponent132;
        private SeatComponent seatComponent91;
        private SeatComponent seatComponent114;
        private SeatComponent seatComponent131;
        private SeatComponent seatComponent92;
        private SeatComponent seatComponent113;
        private SeatComponent seatComponent130;
        private SeatComponent seatComponent93;
        private SeatComponent seatComponent112;
        private SeatComponent seatComponent129;
        private SeatComponent seatComponent94;
        private SeatComponent seatComponent111;
        private SeatComponent seatComponent128;
        private SeatComponent seatComponent95;
        private SeatComponent seatComponent110;
        private SeatComponent seatComponent127;
        private SeatComponent seatComponent96;
        private SeatComponent seatComponent109;
        private SeatComponent seatComponent126;
        private SeatComponent seatComponent97;
        private SeatComponent seatComponent108;
        private SeatComponent seatComponent125;
        private SeatComponent seatComponent98;
        private SeatComponent seatComponent107;
        private SeatComponent seatComponent124;
        private SeatComponent seatComponent99;
        private SeatComponent seatComponent106;
        private SeatComponent seatComponent123;
        private SeatComponent seatComponent100;
        private SeatComponent seatComponent105;
        private SeatComponent seatComponent122;
        private SeatComponent seatComponent101;
        private SeatComponent seatComponent104;
        private SeatComponent seatComponent102;
        private SeatComponent seatComponent103;
        private SeatComponent seatComponent68;
        private SeatComponent seatComponent69;
        private SeatComponent seatComponent70;
        private SeatComponent seatComponent71;
        private SeatComponent seatComponent72;
        private SeatComponent seatComponent73;
        private SeatComponent seatComponent74;
        private SeatComponent seatComponent75;
        private SeatComponent seatComponent76;
        private SeatComponent seatComponent77;
        private SeatComponent seatComponent78;
        private SeatComponent seatComponent79;
        private SeatComponent seatComponent80;
        private SeatComponent seatComponent81;
        private SeatComponent seatComponent82;
        private SeatComponent seatComponent83;
        private SeatComponent seatComponent84;
        private SeatComponent seatComponent85;
        private SeatComponent seatComponent67;
        private SeatComponent seatComponent66;
        private SeatComponent seatComponent65;
        private SeatComponent seatComponent49;
        private SeatComponent seatComponent64;
        private SeatComponent seatComponent48;
        private SeatComponent seatComponent63;
        private SeatComponent seatComponent47;
        private SeatComponent seatComponent62;
        private SeatComponent seatComponent46;
        private SeatComponent seatComponent61;
        private SeatComponent seatComponent45;
        private SeatComponent seatComponent60;
        private SeatComponent seatComponent44;
        private SeatComponent seatComponent59;
        private SeatComponent seatComponent43;
        private SeatComponent seatComponent58;
        private SeatComponent seatComponent42;
        private SeatComponent seatComponent57;
        private SeatComponent seatComponent41;
        private SeatComponent seatComponent56;
        private SeatComponent seatComponent40;
        private SeatComponent seatComponent55;
        private SeatComponent seatComponent39;
        private SeatComponent seatComponent54;
        private SeatComponent seatComponent38;
        private SeatComponent seatComponent53;
        private SeatComponent seatComponent37;
        private SeatComponent seatComponent52;
        private SeatComponent seatComponent36;
        private SeatComponent seatComponent51;
        private SeatComponent seatComponent50;
        private SeatComponent seatComponent35;
        private SeatComponent seatComponent34;
    }
}