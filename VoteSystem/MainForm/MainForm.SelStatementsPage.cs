﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using System.Drawing;
using DevExpress.Utils;

namespace VoteSystem
{
    public interface ISelStatementsPage
    {
        IMsgMethods GetShowInterface();
//      IQuestProperties GetQuestPropertiesInterface();
//      void ShowView();

        void Init();
        void InitControls(ref XPCollection<StatementObj> SesStatements);
        int NavigatorPosition { get; set; }
        void EnableButtons(bool IsFinished);

        void SelectPos (int id);

        XtraForm GetView();
    }

    partial class MainForm : ISelStatementsPage
    {
        SelStatementCntrler _Controller_SS;

#region реализация интерфейса ISelStatementsPage
        IMsgMethods ISelStatementsPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }
/*
        IQuestProperties ISelStatementsPage.GetQuestPropertiesInterface()
        {
            return (IQuestProperties)this;
        }
*/

        void ISelStatementsPage.Init()
        {
            _Controller_SS = (SelStatementCntrler)_Controller.GetSubController("Select Statements Page Controller"); ;
            ConditionsAdjustment2();
        }

        void ISelStatementsPage.InitControls(ref XPCollection<StatementObj> SesStatements)
        {

            PropertiesControlState.DataSource = SesStatements;
            SelStateGrid.DataSource = SesStatements;
            navigatorStatements.DataSource = SesStatements;

            gridViewSelState.ExpandAllGroups();
        }

        int ISelStatementsPage.NavigatorPosition
        {
            get
            {
                return navigatorStatements.Position;
            }
            set
            {
                navigatorStatements.Position = value;
            }
        }

        void ISelStatementsPage.SelectPos (int id)
        {
            int handle = gridViewSelState.LocateByValue(0, gridViewSelState.Columns["id"], id);
            gridViewSelState.FocusedRowHandle = handle;
        }


        DevExpress.XtraEditors.XtraForm ISelStatementsPage.GetView()
        {
            return this;
        }

        void ISelStatementsPage.EnableButtons(bool IsFinished)
        {

            navControlStatements.Buttons.CustomButtons[1].Enabled = !IsFinished;
            navControlStatements.Buttons.CustomButtons[2].Enabled = !IsFinished;
            navControlStatements.Buttons.CustomButtons[3].Enabled = !IsFinished;
            navControlStatements.Buttons.CustomButtons[4].Enabled = !IsFinished;
//          btnStatementMode.Enabled = !IsFinished;

            btnStateResult.Enabled = IsFinished;
 
        }

#endregion

        private void navigatorStatements_PositionChanged(object sender, EventArgs e)
        {
            if (_Controller_SS != null)
                _Controller_SS.UpdatePropData();
        }

        private void ConditionsAdjustment2()
        {
            StyleFormatCondition cn;

            cn = new StyleFormatCondition(FormatConditionEnum.Equal, gridViewSelState.Columns["IsFinished"], null, true);
            cn.ApplyToRow = true;
            cn.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Regular);
            cn.Appearance.ForeColor = SystemColors.ControlDark;
            gridViewSelState.FormatConditions.Add(cn);

            cn = new StyleFormatCondition(FormatConditionEnum.Equal, gridViewSelState.Columns["IsFinished"], null, false);
            cn.ApplyToRow = true;
            cn.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Regular);
            gridViewSelQuest.FormatConditions.Add(cn);
        }

        private void btnStatemSpeechMode_Click(object sender, EventArgs e)
        {
            _Controller_SS.SpeechModeRun(true);
        }


        private void SelStateGrid_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                _Controller_SS.EditCurrRecord();
            }
        }


        private void navControlStatements_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Append")
                {
                    _Controller_SS.AppendItem();
/*
                    if (i >= 0)
                        navigatorStatements.Position = i;
*/

                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    DialogResult dr = _messages.ShowQuestion("Вы уверены, что хотите удалить тему из повестки дня?");
                    if (dr == DialogResult.Yes)
                    {
                        _Controller_SS.RemoveCurrentItem();
                    }

                }
                else if (e.Button.Tag.ToString() == "Edit")
                {
                    _Controller_SS.EditCurrRecord();
                }
                else if (e.Button.Tag.ToString() == "Queue_Up" || e.Button.Tag.ToString() == "Queue_Down")
                {
                    _Controller_SS.ChangeQueuePos(e.Button.Tag.ToString());
                }
            }
        }

        private void SelStateGrid_DoubleClick(object sender, EventArgs e)
        {
            // Checks whether an end-used has double clicked the row indicator.
            GridHitInfo hi = gridViewSelState.CalcHitInfo(SelStateGrid.PointToClient(MousePosition));
            if (!hi.InRow) return;

            _Controller_SS.SpeechModeRun(false);
        }

        private void btnStateResult_Click(object sender, EventArgs e)
        {
            _Controller_SS.ViewModeRun();
        }

        private void btnStateClose_Click(object sender, EventArgs e)
        {
            _Controller.CloseOpenStatements();

        }

   }
}
