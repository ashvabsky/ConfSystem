﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using System.Drawing;
using DevExpress.Utils;
using System.ComponentModel;

namespace VoteSystem
{
    public interface IVoteQuestsPage
    {
        IMsgMethods GetShowInterface();
//      IQuestProperties GetQuestPropertiesInterface();
//      void ShowView();

        void Init();
        void InitControls(ref XPCollection<QuestionObj> Questions, XPCollection<TaskKindObj> QuestKinds, SesTaskObj currTask, ref XPCollection<SesTaskObj> SesTasks, ReadNumObj readnum, AmendmentTypeObj amendtype, bool IsAutoCardMode);
        void UpdateControls();
        void InitQFields(SesTaskObj currTask, ReadNumObj readnum, AmendmentTypeObj amendtype);
        void SelectTask(SesTaskObj currTask);
        void SetReadNum(ReadNumObj readnum);
        int NavigatorPosition { get; set; }

        void ShowVoteResults(VoteResultObj vro);
        void SelectPos(int id);
        void EnableButtons(bool IsVoted);

        XtraForm GetView();

    }

    partial class MainForm : IVoteQuestsPage
    {
        VoteQuestionsCntrler _Controller_VQ;

        bool _InitVoteQuestesPage = false;

        #region реализация интерфейса IResultsPage
        IMsgMethods IVoteQuestsPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }
/*
        IQuestProperties IResultsPage.GetQuestPropertiesInterface()
        {
            return (IQuestProperties)this;
        }
*/
        void IVoteQuestsPage.Init()
        {
            _Controller_VQ = (VoteQuestionsCntrler)_Controller.GetSubController("VoteQuestions Page Controller");
            txtVoteCaption.Text = "";
            _InitVoteQuestesPage = false;
            ConditionsAdjustment_VoteQuest();

            edtTaskItem.Properties.Columns["QueueNum"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
        }

        void IVoteQuestsPage.InitControls(ref XPCollection<QuestionObj> Questions, XPCollection<TaskKindObj> QuestKinds, SesTaskObj currTask, ref XPCollection<SesTaskObj> SesTasks, ReadNumObj readnum, AmendmentTypeObj amendtype, bool IsAutoCardMode)
        {

            PropertiesControlQuest2.DataSource = Questions;
            VoteQuestGrid.DataSource = Questions;
            navigatorResQuestions.DataSource = Questions;

            if (_InitVoteQuestesPage == false)
            {
                cmbQuestKind.ItemIndex = xpTaskKinds.IndexOf(currTask.idKind);
                cmbQuestKind.EditValue = currTask.idKind;
                txtVoteCaption.Text = currTask.Caption;
                memoDescription.Text = currTask.Description;
                edtReadingNum.EditValue = readnum.id;
                edtAmendment.Value = 0;
                edtAmendmentType.EditValue = amendtype.id;
                _InitVoteQuestesPage = true;
            }

            SesTasks.Load();
            SesTasks.Reload();

            edtTaskItem.Properties.DataSource = SesTasks;

//          edtTaskItem.Properties.Columns["QueueNum"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            edtTaskItem.Properties.Columns["QueueNum"].Visible = false;
            edtTaskItem.Properties.Columns["SortCriteria"].Visible = true;
            edtTaskItem.Properties.Columns["SortCriteria"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            edtTaskItem.Properties.SortColumnIndex = 3;
            edtTaskItem.Refresh();
            edtTaskItem.Properties.Columns["SortCriteria"].Visible = false;
            edtTaskItem.Refresh();

        }

        private void edtTaskItem_QueryPopUp(object sender, CancelEventArgs e)
        {
            edtTaskItem.Properties.Columns["SortCriteria"].Visible = true;
            edtTaskItem.Properties.Columns["SortCriteria"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            edtTaskItem.Properties.SortColumnIndex = 3;
            edtTaskItem.Refresh();
            edtTaskItem.Properties.Columns["SortCriteria"].Visible = false;
            edtTaskItem.Refresh();
 
        }

        void IVoteQuestsPage.UpdateControls()
        {
            VoteQuestGrid.RefreshDataSource();
        }

        int IVoteQuestsPage.NavigatorPosition
        {
            get
            {
                return navigatorResQuestions.Position;
            }
            set
            {
                navigatorResQuestions.Position = value;
            }
        }


        void IVoteQuestsPage.ShowVoteResults(VoteResultObj vro)
        {
            if (vro != null && vro.id != 0)
            {
                grpVoteResults.Visible = true;
                lblVoteDateTime.Visible = true;

                lblDecision.Text = vro.idResultValue.Name;
                lblVoteDateTime.Text = vro.VoteDateTime.ToString("t");

                lblVoteAye.Text = vro.AnsQnty1.ToString();
                lblVoteAgainst.Text = vro.AnsQnty3.ToString();
                lblVoteAbstain.Text = vro.AnsQnty2.ToString();

                int votes = vro.AnsQnty1 + vro.AnsQnty2 + vro.AnsQnty3;

                if (vro.AvailableQnty != 0)
                {
                    int votes_proc = (votes * 100) / vro.AvailableQnty;

                    int procAye = (vro.AnsQnty1 * 100) / vro.AvailableQnty;
                    int procAgainst = (vro.AnsQnty3 * 100) / vro.AvailableQnty;
                    int procAbstain = (vro.AnsQnty2 * 100) / vro.AvailableQnty;

                    lblProcAye.Text = procAye.ToString();
                    lblProcAgainst.Text = procAgainst.ToString();
                    lblProcAbstain.Text = procAbstain.ToString();

                    int notvoted = vro.AvailableQnty - votes;
                    int notvoted_proc = (notvoted * 100) / vro.AvailableQnty;

                    lblNonVoteQnty.Text = notvoted.ToString();
                    lblNonProcQnty.Text = notvoted_proc.ToString();

                    lblVoteQnty.Text = string.Format("{0} из {1}", votes, vro.AvailableQnty);
                    lblProcQnty.Text = string.Format("{0}", votes_proc);
                }
                else
                {
                    lblProcAye.Text = "";
                    lblProcAgainst.Text = "";
                    lblProcAbstain.Text = "";
                    lblNonVoteQnty.Text = "";
                    lblNonProcQnty.Text = "";
                    lblVoteQnty.Text = "";
                    lblProcQnty.Text = "";

                }
            }
            else
            {
                lblDecision.Text = "Нет решения";
                grpVoteResults.Visible = false;
                lblVoteDateTime.Visible = false;

                lblVoteQnty.Text = "";
                lblProcQnty.Text = "";
            }

        }

        void IVoteQuestsPage.SelectPos(int id)
        {
            int handle = GridViewQuestions.LocateByValue(0, GridViewQuestions.Columns["id"], id);
            GridViewQuestions.FocusedRowHandle = handle;
            _Controller_VQ.OnSelectNewPosition();
//            navigatorResQuestions.Position = 
        }

        void IVoteQuestsPage.EnableButtons(bool IsVoted)
        {
            navControlResQuestions.Buttons.CustomButtons[1].Enabled = !IsVoted;
//            navControlResQuestions.Buttons.CustomButtons[2].Enabled = !IsVoted;
            navControlResQuestions.Buttons.CustomButtons[3].Enabled = !IsVoted;
            navControlResQuestions.Buttons.CustomButtons[4].Enabled = !IsVoted;
            btnQuestVoteDetail.Enabled = IsVoted;
            btnQuestVoteStart.Enabled = !IsVoted;
        }


        DevExpress.XtraEditors.XtraForm IVoteQuestsPage.GetView()
        {
            return this;
        }

        void IVoteQuestsPage.InitQFields(SesTaskObj currTask, ReadNumObj readnum, AmendmentTypeObj amendtype)
        {
            if (currTask == null || currTask.id == 0)
                edtTaskItem.EditValue = null;

            if (currTask != null)
            {
                cmbQuestKind.ItemIndex = xpTaskKinds.IndexOf(currTask.idKind);
                cmbQuestKind.EditValue = currTask.idKind;
                txtVoteCaption.Text = "\"" + currTask.Caption + "\"";
                memoDescription.Text = currTask.Description;
                edtReadingNum.EditValue = readnum.id;
                edtAmendment.Value = 0;
                edtAmendmentType.EditValue = amendtype.id;
            }
        }

        void IVoteQuestsPage.SetReadNum(ReadNumObj readnum)
        {
            edtReadingNum.EditValue = readnum.id;
            edtAmendment.Value = 0;
        }
        void IVoteQuestsPage.SelectTask(SesTaskObj currTask)
        {
            if (currTask.id == 0)
                edtTaskItem.EditValue = null;
            else
            {
                edtTaskItem.EditValue = currTask.id;
            }
        }

        #endregion

        private void edtTaskItem_EditValueChanged(object sender, EventArgs e)
        {
//          txtCaption.Text = "\"" + edtTaskItem.Text + "\"";
            int idTask = 0;
            if (edtTaskItem.EditValue != null)
            {
                idTask = (int)edtTaskItem.EditValue;
                _Controller_VQ.OnSelectTask(idTask);
            }

//          cmbQuestKind.ItemIndex = xpTaskKinds.IndexOf(currTask.idKind);
        }



        private void edtTaskItem_Click(object sender, EventArgs e)
        {

        }

        private void txtVoteCaption_Click(object sender, EventArgs e)
        {

        }

        private void cmbQuestKind_Click(object sender, EventArgs e)
        {

        }

        private void edtReadingNum_Click(object sender, EventArgs e)
        {

        }

        private void edtAmendment_Click(object sender, EventArgs e)
        {

        }

        private void memoDescription_Click(object sender, EventArgs e)
        {

        }

        private void navControlVoteQuestions_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Edit")
                {
                    _Controller_VQ.EditCurrRecord();
                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    DialogResult dr = _messages.ShowQuestion("Вы уверены, что хотите удалить вопрос из списка?");
                    if (dr == DialogResult.Yes)
                    {
                        _Controller_VQ.RemoveCurrentItem();
                    }

                }
                else if (e.Button.Tag.ToString() == "Append")
                {
//                    int i = _Controller_VQ.AppendItem();
                    edtTaskItem.ShowPopup();
                }

                else if (e.Button.Tag.ToString() == "Queue_Up" || e.Button.Tag.ToString() == "Queue_Down")
                {
                    _Controller_VQ.ChangeQueuePos(e.Button.Tag.ToString());
                }

            }
        }

        private void navigatorVoteQuestions_PositionChanged(object sender, EventArgs e)
        {
            if (_Controller_VQ != null)
                _Controller_VQ.OnSelectNewPosition();
        }



        private void btnQuestNewRead_Click(object sender, EventArgs e)
        {
            _Controller_VQ.CreateNewReading();
        }

        private void btnQuestVoteDetail_Click(object sender, EventArgs e)
        {
            _Controller_VQ.VoteDetailsRun();
        }

        private void btnQuestVoteStart_Click(object sender, EventArgs e)
        {
            _Controller_VQ.VoteModeRunFast();
        }


        private void VoteQuestGrid_DoubleClick(object sender, EventArgs e)
        {
            // Checks whether an end-used has double clicked the row indicator.
            GridHitInfo hi = GridViewQuestions.CalcHitInfo(VoteQuestGrid.PointToClient(MousePosition));
            if (!hi.InRow) return;

            _Controller_VQ.VoteModeRun();
        }


        private void btnNewVoteFast_Click(object sender, EventArgs e)
        {
            TaskKindObj taskkind = (TaskKindObj)xpTaskKinds[cmbQuestKind.ItemIndex];
            ReadNumObj readnum = (ReadNumObj)xpReadNum.Lookup(edtReadingNum.EditValue);
            AmendmentTypeObj amendtype = (AmendmentTypeObj)xpAmendmentType.Lookup(edtAmendmentType.EditValue);
            int amend = System.Convert.ToInt32(edtAmendment.Value);

            _Controller_VQ.CreateQuestionFromCurrentTask(txtVoteCaption.Text, taskkind, memoDescription.Text, readnum, amendtype, amend);
            _Controller_VQ.VoteModeRunFast();
        }

        private void btnNewVote_Click(object sender, EventArgs e)
        {
            TaskKindObj taskkind = (TaskKindObj)xpTaskKinds[cmbQuestKind.ItemIndex];
            ReadNumObj readnum = (ReadNumObj)xpReadNum.Lookup(edtReadingNum.EditValue);
            AmendmentTypeObj amendtype = (AmendmentTypeObj)xpAmendmentType.Lookup(edtAmendmentType.EditValue);
            int amend = System.Convert.ToInt32(edtAmendment.Value);

            _Controller_VQ.CreateQuestionFromCurrentTask(txtVoteCaption.Text, taskkind, memoDescription.Text, readnum, amendtype, amend);
            _Controller_VQ.VoteModeRun();
        }

        private void btnNewAdd_Click(object sender, EventArgs e)
        {
            TaskKindObj taskkind = (TaskKindObj)xpTaskKinds[cmbQuestKind.ItemIndex];
            ReadNumObj readnum = (ReadNumObj)xpReadNum.Lookup(edtReadingNum.EditValue);
            AmendmentTypeObj amendtype = (AmendmentTypeObj)xpAmendmentType.Lookup(edtAmendmentType.EditValue); 
            int amend = System.Convert.ToInt32(edtAmendment.Value);

            _Controller_VQ.CreateQuestionFromCurrentTask(txtVoteCaption.Text, taskkind, memoDescription.Text, readnum, amendtype, amend);
        }

        private void btnNewAgendaItem_Click(object sender, EventArgs e)
        {
            edtTaskItem.EditValue = null;
            _Controller_VQ.OnSelectTask(0);
        }

        private void ConditionsAdjustment_VoteQuest()
        {
            StyleFormatCondition cn;
            cn = new StyleFormatCondition(FormatConditionEnum.Equal, GridViewQuestions.Columns["idResult.IsResult"], null, 0);
            cn.ApplyToRow = true;
            cn.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Regular);
            cn.Appearance.ForeColor = Color.DarkGreen;
            GridViewQuestions.FormatConditions.Add(cn);

            cn = new StyleFormatCondition(FormatConditionEnum.Equal, GridViewQuestions.Columns["idResult.IsResult"], null, 1);
            cn.ApplyToRow = true;
            cn.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Regular);
            GridViewQuestions.FormatConditions.Add(cn);
        }

        private void edtAmendmentType_EditValueChanged(object sender, EventArgs e)
        {
            AmendmentTypeObj amendtype = (AmendmentTypeObj)xpAmendmentType.Lookup(edtAmendmentType.EditValue);
            if (amendtype.CodeName == "AmendmentTable")
            {
                edtAmendment.Value = 0;
            }
        }
   }
}