using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class MonitorPreviewForm2 : DevExpress.XtraEditors.XtraForm
    {
        public MonitorPreviewForm2()
        {
            InitializeComponent();
        }

        private void MonitorPreviewForm2_Load(object sender, EventArgs e)
        {
            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
        }

        private void memoEdit23_EditValueChanged(object sender, EventArgs e)
        {

        }

        public void ShowRegistration(Object regInfo)
        {
            ShowPage("xtraTabPage3");
            ShowDialog();
        }

        public void ShowRegPrepare()
        {
            ShowPage("xtraTabPage1");
            ShowDialog();
        }

        public void ShowRegStart()
        {
            ShowPage("xtraTabPage2");
            ShowDialog();
        }

        public void ShowStateStart()
        {
            ShowPage("xtraTabPage4");
            ShowDialog();
        }

        public void ShowDelegateAcceptStart()
        {
            ShowPage("xtraTabPage5");
            ShowDialog();
        }


        public void SetDelay(int delay)
        {
            tmrForm.Interval = delay;
        }

        public void ShowPage(string PageName)
        {
            xtraTabPage1.PageVisible = false;
            xtraTabPage2.PageVisible = false;
            xtraTabPage3.PageVisible = false;
            xtraTabPage4.PageVisible = false;
            xtraTabPage5.PageVisible = false;

            if (PageName == "xtraTabPage1")
            {
                xtraTabPage1.PageVisible = true;
            }
            else if (PageName == "xtraTabPage2")
            {
                xtraTabPage2.PageVisible = true;
            }
            else if (PageName == "xtraTabPage3")
            {
                xtraTabPage3.PageVisible = true;
            }
            else if (PageName == "xtraTabPage4")
            {
                xtraTabPage4.PageVisible = true;
            }
            else if (PageName == "xtraTabPage5")
            {
                xtraTabPage5.PageVisible = true;
            }
        }

        private void tmrForm_Tick(object sender, EventArgs e)
        {
            tmrForm.Stop();
            Hide();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void MonitorPreviewForm2_FormClosing(object sender, FormClosingEventArgs e)
        {
            tmrForm.Stop();
            e.Cancel = true;
            Hide();
        }

        private void MonitorPreviewForm2_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                tmrForm.Start();
            }
            else
            {
                tmrForm.Stop();
            }
        }
    }
}