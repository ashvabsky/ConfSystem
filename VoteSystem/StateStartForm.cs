using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class StateStartForm : DevExpress.XtraEditors.XtraForm
    {
        VoteModeCntrler _Controller;

        public StateStartForm()
        {
            InitializeComponent();
        }

        public StateStartForm(VoteModeCntrler controller)
        {
            InitializeComponent();
            _Controller = controller;
        }

        public int VoteTime
        {
            set { txtVoteTime.Text = value.ToString(); }
            get { return Int32.Parse(txtVoteTime.Text); }
        }

        private void btnMonitorStatePrepare_CheckedChanged(object sender, EventArgs e)
        {
            if (btnMonitorStatePrepare.Checked == true)
            {
                MonitorPreviewForm2 mpf2 = new MonitorPreviewForm2();
                mpf2.ShowStateStart();

            }

        }

    }
}