﻿namespace VoteSystem
{
    partial class QuestionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuestionsForm));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnSelect = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.xpQuestions = new DevExpress.Xpo.XPCollection();
            this.btnChange = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCaption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VoteType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VoteKind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnPropCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnPropApply = new DevExpress.XtraEditors.SimpleButton();
            this.propertyDescriptionControl1 = new DevExpress.XtraVerticalGrid.PropertyDescriptionControl();
            this.PropertiesControl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.rowid = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowCaption = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDescription = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowidType_Name = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowidKind_Name = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.session1 = new DevExpress.Xpo.Session();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem2 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem3 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barLargeButtonItem4 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.xpVoteTypes = new DevExpress.Xpo.XPCollection();
            this.xpVoteKinds = new DevExpress.Xpo.XPCollection();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpVoteTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpVoteKinds)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 24);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlTop);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlBottom);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlLeft);
            this.splitContainerControl1.Panel1.Controls.Add(this.barDockControlRight);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Top;
            this.splitContainerControl1.Panel2.Controls.Add(this.btnPropCancel);
            this.splitContainerControl1.Panel2.Controls.Add(this.btnPropApply);
            this.splitContainerControl1.Panel2.Controls.Add(this.propertyDescriptionControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.PropertiesControl);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Информация";
            this.splitContainerControl1.Size = new System.Drawing.Size(1242, 627);
            this.splitContainerControl1.SplitterPosition = 888;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnSelect);
            this.panelControl1.Controls.Add(this.dataNavigator1);
            this.panelControl1.Controls.Add(this.btnChange);
            this.panelControl1.Controls.Add(this.btnRemove);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 568);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(884, 55);
            this.panelControl1.TabIndex = 10;
            // 
            // btnSelect
            // 
            this.btnSelect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSelect.ImageIndex = 12;
            this.btnSelect.ImageList = this.ButtonImages;
            this.btnSelect.Location = new System.Drawing.Point(618, 9);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(107, 34);
            this.btnSelect.TabIndex = 13;
            this.btnSelect.Text = "Выбрать";
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.DataSource = this.xpQuestions;
            this.dataNavigator1.Location = new System.Drawing.Point(406, 19);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(192, 24);
            this.dataNavigator1.TabIndex = 11;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.Visible = false;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            // 
            // xpQuestions
            // 
            this.xpQuestions.CriteriaString = "[id] > 0";
            this.xpQuestions.DeleteObjectOnRemove = true;
            this.xpQuestions.ObjectType = typeof(VoteSystem.QuestObj);
            // 
            // btnChange
            // 
            this.btnChange.ImageIndex = 0;
            this.btnChange.ImageList = this.ButtonImages;
            this.btnChange.Location = new System.Drawing.Point(304, 6);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(96, 34);
            this.btnChange.TabIndex = 11;
            this.btnChange.Text = "Изменить";
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.ImageIndex = 2;
            this.btnRemove.ImageList = this.ButtonImages;
            this.btnRemove.Location = new System.Drawing.Point(164, 6);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(96, 34);
            this.btnRemove.TabIndex = 10;
            this.btnRemove.Text = "Удалить";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ImageIndex = 3;
            this.btnAdd.ImageList = this.ButtonImages;
            this.btnAdd.Location = new System.Drawing.Point(24, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(96, 34);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton1.ImageIndex = 8;
            this.simpleButton1.ImageList = this.ButtonImages;
            this.simpleButton1.Location = new System.Drawing.Point(757, 9);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(107, 34);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Text = "Закрыть";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.xpQuestions;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridControl1.EmbeddedNavigator.Name = "";
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(884, 568);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick_1);
            this.gridControl1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gridControl1_KeyPress);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colCaption,
            this.VoteType,
            this.VoteKind,
            this.colCrDate});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsDetail.AutoZoomDetail = true;
            this.gridView1.OptionsDetail.EnableMasterViewMode = false;
            // 
            // colid
            // 
            this.colid.Caption = "№";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // colCaption
            // 
            this.colCaption.Caption = "Заголовок";
            this.colCaption.FieldName = "Caption";
            this.colCaption.MinWidth = 255;
            this.colCaption.Name = "colCaption";
            this.colCaption.Visible = true;
            this.colCaption.VisibleIndex = 1;
            this.colCaption.Width = 255;
            // 
            // VoteType
            // 
            this.VoteType.Caption = "Тип";
            this.VoteType.FieldName = "idType.Name";
            this.VoteType.Name = "VoteType";
            this.VoteType.Visible = true;
            this.VoteType.VisibleIndex = 2;
            // 
            // VoteKind
            // 
            this.VoteKind.Caption = "Вид";
            this.VoteKind.FieldName = "idKind.Name";
            this.VoteKind.Name = "VoteKind";
            this.VoteKind.Visible = true;
            this.VoteKind.VisibleIndex = 3;
            // 
            // colCrDate
            // 
            this.colCrDate.Caption = "Дата создания";
            this.colCrDate.FieldName = "CrDate";
            this.colCrDate.Name = "colCrDate";
            this.colCrDate.Visible = true;
            this.colCrDate.VisibleIndex = 4;
            // 
            // btnPropCancel
            // 
            this.btnPropCancel.ImageIndex = 1;
            this.btnPropCancel.Location = new System.Drawing.Point(230, 308);
            this.btnPropCancel.Name = "btnPropCancel";
            this.btnPropCancel.Size = new System.Drawing.Size(104, 30);
            this.btnPropCancel.TabIndex = 3;
            this.btnPropCancel.Text = "Отменить ";
            this.btnPropCancel.Click += new System.EventHandler(this.btnPropCancel_Click);
            // 
            // btnPropApply
            // 
            this.btnPropApply.ImageIndex = 0;
            this.btnPropApply.Location = new System.Drawing.Point(5, 308);
            this.btnPropApply.Name = "btnPropApply";
            this.btnPropApply.Size = new System.Drawing.Size(164, 30);
            this.btnPropApply.TabIndex = 2;
            this.btnPropApply.Text = "Сохранить изменения";
            this.btnPropApply.Click += new System.EventHandler(this.btnPropApply_Click);
            // 
            // propertyDescriptionControl1
            // 
            this.propertyDescriptionControl1.Location = new System.Drawing.Point(5, 344);
            this.propertyDescriptionControl1.Name = "propertyDescriptionControl1";
            this.propertyDescriptionControl1.Size = new System.Drawing.Size(329, 120);
            this.propertyDescriptionControl1.TabIndex = 1;
            this.propertyDescriptionControl1.TabStop = false;
            // 
            // PropertiesControl
            // 
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControl.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControl.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControl.Location = new System.Drawing.Point(5, 5);
            this.PropertiesControl.Name = "PropertiesControl";
            this.PropertiesControl.RecordWidth = 137;
            this.PropertiesControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4,
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6,
            this.repositoryItemMemoEdit1,
            this.repositoryItemMemoEdit2});
            this.PropertiesControl.RowHeaderWidth = 63;
            this.PropertiesControl.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowid,
            this.rowCaption,
            this.rowDescription,
            this.rowidType_Name,
            this.rowidKind_Name});
            this.PropertiesControl.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControl.Size = new System.Drawing.Size(329, 296);
            this.PropertiesControl.TabIndex = 0;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ImmediatePopup = true;
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox2.ImmediatePopup = true;
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.Sorted = true;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.ImmediatePopup = true;
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // rowid
            // 
            this.rowid.Height = 25;
            this.rowid.Name = "rowid";
            this.rowid.Properties.Caption = "№";
            this.rowid.Properties.FieldName = "id";
            this.rowid.Properties.ReadOnly = true;
            this.rowid.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // rowCaption
            // 
            this.rowCaption.Height = 61;
            this.rowCaption.Name = "rowCaption";
            this.rowCaption.Properties.Caption = "Заголовок";
            this.rowCaption.Properties.FieldName = "Caption";
            this.rowCaption.Properties.ReadOnly = true;
            this.rowCaption.Properties.RowEdit = this.repositoryItemMemoEdit2;
            this.rowCaption.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowDescription
            // 
            this.rowDescription.Height = 117;
            this.rowDescription.Name = "rowDescription";
            this.rowDescription.Properties.Caption = "Описание";
            this.rowDescription.Properties.RowEdit = this.repositoryItemMemoEdit1;
            // 
            // rowidType_Name
            // 
            this.rowidType_Name.Height = 25;
            this.rowidType_Name.Name = "rowidType_Name";
            this.rowidType_Name.Properties.Caption = "Тип";
            this.rowidType_Name.Properties.FieldName = "idType.Name";
            this.rowidType_Name.Properties.ReadOnly = true;
            this.rowidType_Name.Properties.RowEdit = this.repositoryItemComboBox5;
            this.rowidType_Name.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowidKind_Name
            // 
            this.rowidKind_Name.Height = 25;
            this.rowidKind_Name.Name = "rowidKind_Name";
            this.rowidKind_Name.Properties.Caption = "Вид";
            this.rowidKind_Name.Properties.FieldName = "idKind.Name";
            this.rowidKind_Name.Properties.ReadOnly = true;
            this.rowidKind_Name.Properties.RowEdit = this.repositoryItemComboBox6;
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "  Фракции  ";
            this.barLargeButtonItem1.Id = 0;
            this.barLargeButtonItem1.LargeImageIndex = 9;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.OwnFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barLargeButtonItem1.UseOwnFont = true;
            // 
            // barLargeButtonItem2
            // 
            this.barLargeButtonItem2.Caption = "  Партии  ";
            this.barLargeButtonItem2.Id = 2;
            this.barLargeButtonItem2.LargeImageIndex = 10;
            this.barLargeButtonItem2.Name = "barLargeButtonItem2";
            this.barLargeButtonItem2.OwnFont = new System.Drawing.Font("Tahoma", 9.75F);
            this.barLargeButtonItem2.UseOwnFont = true;
            // 
            // barLargeButtonItem3
            // 
            this.barLargeButtonItem3.Caption = "  Регионы  ";
            this.barLargeButtonItem3.Id = 3;
            this.barLargeButtonItem3.LargeImageIndex = 11;
            this.barLargeButtonItem3.Name = "barLargeButtonItem3";
            this.barLargeButtonItem3.OwnFont = new System.Drawing.Font("Tahoma", 9.75F);
            this.barLargeButtonItem3.UseOwnFont = true;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barLargeButtonItem4,
            this.barButtonItem1});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barLargeButtonItem4
            // 
            this.barLargeButtonItem4.Caption = "Типы голосований";
            this.barLargeButtonItem4.Id = 1;
            this.barLargeButtonItem4.Name = "barLargeButtonItem4";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Виды голосований";
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // xpVoteTypes
            // 
            this.xpVoteTypes.CriteriaString = "[id] > 0";
            this.xpVoteTypes.DeleteObjectOnRemove = true;
            this.xpVoteTypes.ObjectType = typeof(VoteSystem.VoteTypeObj);
            // 
            // xpVoteKinds
            // 
            this.xpVoteKinds.CriteriaString = "[id] > 0";
            this.xpVoteKinds.DeleteObjectOnRemove = true;
            this.xpVoteKinds.ObjectType = typeof(VoteSystem.VoteKindObj);
            // 
            // QuestionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 673);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QuestionsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "База данных вопросов для голосования";
            this.Load += new System.EventHandler(this.QuestionsForm_Load);
            this.Shown += new System.EventHandler(this.QuestionsForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpVoteTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpVoteKinds)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.SimpleButton btnChange;
        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnPropCancel;
        private DevExpress.XtraEditors.SimpleButton btnPropApply;
        private DevExpress.XtraVerticalGrid.PropertyDescriptionControl propertyDescriptionControl1;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControl;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.Xpo.Session session1;
        private DevExpress.Xpo.XPCollection xpQuestions;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colCaption;
        private DevExpress.XtraGrid.Columns.GridColumn VoteType;
        private DevExpress.XtraGrid.Columns.GridColumn VoteKind;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem2;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowid;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCaption;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowidType_Name;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowidKind_Name;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.Xpo.XPCollection xpVoteTypes;
        private DevExpress.Xpo.XPCollection xpVoteKinds;
        private DevExpress.XtraGrid.Columns.GridColumn colCrDate;
        private DevExpress.XtraEditors.SimpleButton btnSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
    }
}