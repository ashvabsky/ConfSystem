﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using MySql.Data;
using MySql.Data.MySqlClient;
using DevExpress.LookAndFeel;

namespace VoteSystem
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (MainForm mainform = new MainForm())
            {
                DevExpress.UserSkins.BonusSkins.Register();
                DefaultLookAndFeel defaultLF = new DefaultLookAndFeel();
                defaultLF.LookAndFeel.UseDefaultLookAndFeel = true;

                DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "Caramel";
                DevExpress.LookAndFeel.UserLookAndFeel.Default.UseWindowsXPTheme = false;

                Application.Run(mainform);
            }

/*
            try
            {
                Application.Run(new MainForm());
            }
            catch (MySqlException ex)
            {
                ConfHelper.Finish(); // это чтобы остановить сокет-поток

                string err = "Отсутствует соединение с базой данных!\nПроверьте сервер базы данных, наличие сетевого соединения и перезапустите приложение.";
//              err = "errorcode = " + ex.ErrorCode.ToString() + "   Number = " + ex.Number.ToString();
                System.Windows.Forms.MessageBox.Show(err);

                //              throw (ex);
            }
*/
        }
    }
}
