using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class RegStartForm : DevExpress.XtraEditors.XtraForm
    {
        S_DelegatesCntrler _Controller;
        public RegStartForm(S_DelegatesCntrler controller)
        {
            InitializeComponent();
            _Controller = controller;
        }

        public int RegTime
        {
            set { txtRegTime.Text = value.ToString(); }
            get { return Int32.Parse(txtRegTime.Text); }
        }

        public int DelegatesQnty
        {
            set { txtDelegateQnty.Text = value.ToString(); }
        }

        public int QuorumQnty
        {
            set { txtQuorumQnty.Text = value.ToString(); }
            get { return Int32.Parse(txtQuorumQnty.Text); }
        }

        public bool IsRegProccess
        {
            set { chkRegProccess.Checked = value; }
            get { return chkRegProccess.Checked; }
        }

        public bool IsTotalsToScreen
        {
            set { chkTotalsToScreen.Checked = value; }
            get { return chkTotalsToScreen.Checked; }
        }

        private void RegStartForm_Load(object sender, EventArgs e)
        {
            _Controller.OnRegStartFormLoad();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {

        }

        private void btnMonitorRegPrepare_CheckedChanged(object sender, EventArgs e)
        {
            if (btnMonitorRegPrepare.Checked == true)
            {
                _Controller.ShowMonitorPreview("RegPrepare");
            }

        }

        private void chkRegProccess_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRegProccess.Checked == true)
            {
                _Controller.ShowMonitorPreview("RegStart");
            }

        }

        private void chkTotalsToScreen_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTotalsToScreen.Checked == true)
            {
                _Controller.ShowMonitorPreview("RegResults");
            }

        }

    }
}