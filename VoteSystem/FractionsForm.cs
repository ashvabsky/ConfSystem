﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class FractionsForm : DevExpress.XtraEditors.XtraForm
    {
        public FractionsForm()
        {
            InitializeComponent();
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            gridControl.MainView.ShowEditor();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int i = AddNewFraction();
            if (i >= 0)
                dataNavigator1.Position = i;

            gridControl.MainView.ShowEditor();
        }

        public int AddNewFraction()
        {
            FractionObj newFraction = new FractionObj();
            newFraction.Name = "Новый";

            FractionObj last = (FractionObj)xpFractions[xpFractions.Count - 1];
            newFraction.id = last.id + 1;
            int i = xpFractions.Add(newFraction);
            newFraction.Save();
            return i;
        }

        public DialogResult ShowQuestion(string Quest)
        {
            return MessageBox.Show(this, Quest, "Вопрос", MessageBoxButtons.YesNoCancel);
        }

        public void ShowWarningMsg(string Text)
        {
            MessageBox.Show(this, Text, "Внимание!", MessageBoxButtons.OK);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            DialogResult dr = ShowQuestion("Вы уверены, что хотите удалить выделенную запись?");
            if (dr == DialogResult.Yes)
            {
                int i = dataNavigator1.Position;
                if (i >= 0)
                {
                    FractionObj o = (FractionObj)xpFractions[i];
                    if (o.Delegates.Count == 0)
                        xpFractions.Remove(o);
                    else
                    {
                        ShowWarningMsg("Невозможно удалить данную запись, так как на нее ссылаются другие записи.");
                    }
                }
            }
        }
    }
}