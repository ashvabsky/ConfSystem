using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace VoteSystem
{
    public interface IDelegatesForm
    {
        IMsgMethods GetShowInterface();

        DialogResult ShowView();

        void Properties_StartEdit();
        void Properties_FinishEdit();
        void SetProperties(Dictionary<string, string> properties);

    }

    public partial class DelegatesForm : DevExpress.XtraEditors.XtraForm, IDelegatesForm
    {
        DelegatesCntrler _Controller;
        MsgForm _msgForm;
        IMsgMethods _messages;

        bool _init = false;

        public DelegatesForm()
        {
            InitializeComponent();
        }

        public DelegatesForm(DelegatesCntrler cntrler, MainCntrler mcontr, MainForm mform)
        {
            InitializeComponent();
            _Controller = cntrler;
            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;

            _Controller.Init(ref xpDelegates, ref xpParties, ref xpFractions, ref xpRegions, ref xpSeats);
        }

#region ���������� ���������� IDelegatesForm

        IMsgMethods IDelegatesForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        DialogResult IDelegatesForm.ShowView()
        {
            return ShowDialog();
        }

        void IDelegatesForm.Properties_StartEdit()
        {

            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControl.Rows)
            {
                row.Properties.ReadOnly = false;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = false;
                }

            }

            // ����������� ����, ������� ������� �������� ��� readonly:
            PropertiesControl.Rows["rowRegionNum"].Properties.ReadOnly = true;

            btnPropApply.Enabled = true;
            btnPropCancel.Enabled = true;

            splitContainerControl1.Panel1.Enabled = false;

            PropertiesControl.FocusedRow = PropertiesControl.Rows["rowLastName"];
            PropertiesControl.ShowEditor();

        }

        void IDelegatesForm.Properties_FinishEdit()
        {
            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControl.Rows)
            {
                row.Properties.ReadOnly = true;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = true;
                }
            }

            btnPropApply.Enabled = false;
            btnPropCancel.Enabled = false;

            splitContainerControl1.Panel1.Enabled = true;

            _Controller.DataPositionChanged(dataNavigator1.Position);

        }

        void IDelegatesForm.SetProperties(Dictionary<string, string> properties)
        {
            PropertiesControl.Rows["rowFirstName"].Properties.Value = properties["FirstName"];
            PropertiesControl.Rows["rowSecondName"].Properties.Value = properties["SecondName"];
            PropertiesControl.Rows["rowLastName"].Properties.Value = properties["LastName"];

            PropertiesControl.Rows["rowidParty_Name"].Properties.Value = properties["PartyName"];
            PropertiesControl.Rows["rowFraction"].Properties.Value = properties["FractionName"];
            PropertiesControl.Rows["rowRegionName"].Properties.Value = properties["RegionName"];
            PropertiesControl.Rows["rowRegionNum"].Properties.Value = properties["RegionNum"];

            PropertiesControl.Rows["rowMicNum"].Properties.Value = properties["MicNum"];
            PropertiesControl.Rows["rowSeatNum"].Properties.Value = properties["SeatNum"];
            PropertiesControl.Rows["rowRowNum"].Properties.Value = properties["RowNum"];
        }

#endregion

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {

        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            DataNavigator dn = (DataNavigator)sender;
            if (dn == null)
                return;

            _Controller.DataPositionChanged(dn.Position);
        }

        private void StartEdit()
        {

            int Pos = dataNavigator1.Position;
            _Controller.EditRecord(Pos);
        }


        private void DelegatesForm_Load(object sender, EventArgs e)
        {
//            _Controller.Init(ref xpDelegates, ref xpParties, ref xpFractions, ref xpRegions);

            btnPropApply.Enabled = false;
            btnPropCancel.Enabled = false;

            FillDropDowns();

            _init = true;
            dataNavigator1_PositionChanged(dataNavigator1, null); // �������� ��������� ��������
        }

        private void FillDropDowns()
        {
            DevExpress.XtraEditors.Repository.RepositoryItemComboBox r1 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControl.Rows["rowidParty_Name"].Properties.RowEdit;
            r1.Items.Clear();

            foreach (PartyObj po in xpParties)
            {
                if (po.Name != null)
                    r1.Items.Add(po.Name);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox r2 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControl.Rows["rowFraction"].Properties.RowEdit;
            r2.Items.Clear();

            foreach (FractionObj fo in xpFractions)
            {
                if (fo.Name != null)
                    r2.Items.Add(fo.Name);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox r3 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControl.Rows["rowRegionName"].Properties.RowEdit;
            r3.Items.Clear();

            foreach (RegionObj ro in xpRegions)
            {
                if (ro.Name != null)
                    r3.Items.Add(ro.Name);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox r4 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControl.Rows["rowRegionNum"].Properties.RowEdit;
            r4.Items.Clear();

            foreach (RegionObj ro in xpRegions)
            {
                if (ro.Number != null)
                    r4.Items.Add(ro.Number);
            }
        }

        private void btnPropApply_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties["LastName"] = PropertiesControl.Rows["rowLastName"].Properties.Value.ToString();
            properties["SecondName"] = PropertiesControl.Rows["rowSecondName"].Properties.Value.ToString();
            properties["FirstName"] = PropertiesControl.Rows["rowFirstName"].Properties.Value.ToString();
            properties["PartyName"] = PropertiesControl.Rows["rowidParty_Name"].Properties.Value.ToString();
            properties["FractionName"] = PropertiesControl.Rows["rowFraction"].Properties.Value.ToString();
            properties["RegionName"] = PropertiesControl.Rows["rowRegionName"].Properties.Value.ToString();
            properties["RegionNum"] = PropertiesControl.Rows["rowRegionNum"].Properties.Value.ToString();

            properties["MicNum"] = PropertiesControl.Rows["rowMicNum"].Properties.Value.ToString();
            properties["RowNum"] = PropertiesControl.Rows["rowRowNum"].Properties.Value.ToString();
            properties["SeatNum"] = PropertiesControl.Rows["rowSeatNum"].Properties.Value.ToString();

            _Controller.ApplyChanges(properties);
        }

        private void gridControl1_DoubleClick_1(object sender, EventArgs e)
        {
            // Checks whether an end-used has double clicked the row indicator.
            GridHitInfo hi = gridView1.CalcHitInfo(gridControl1.PointToClient(MousePosition));
            if (!hi.InRow) return;

            StartEdit();
        }

        private void gridControl1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                StartEdit();
            }
        }

        private void btnPropCancel_Click(object sender, EventArgs e)
        {
            _Controller.CancelChanges();
        }

        private void xpView1_ListChanged(object sender, ListChangedEventArgs e)
        {
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int i = _Controller.AddNewDelegate();
            if (i >= 0)
                dataNavigator1.Position = i;

            StartEdit();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            DialogResult dr = _messages.ShowQuestion("�� �������, ��� ������ ������� ���������� ������?");
            if (dr == DialogResult.Yes)
            {
                _Controller.RemoveDelegate();
            }
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            StartEdit();
        }

        private void xpCollection1_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (_init) // ��������: �������������� �������� ������ ��� ������������ 
                _Controller.DataPositionChanged(dataNavigator1.Position); // �������� ��������� ��������
        }

        private void PropertiesControl_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            if (e.Row.Name == "rowRegionName")
            {
                string RegionName = e.Value.ToString();
                PropertiesControl.Rows["rowRegionNum"].Properties.Value = _Controller.GetRegionNumByValue(RegionName);
            }
        }

        private void PropertiesControl_CellValueChanging(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {

        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FractionsForm fr = new FractionsForm();
            fr.ShowDialog();
            xpFractions.Reload();
            FillDropDowns();
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PartiesForm pr = new PartiesForm();
            pr.ShowDialog();
            xpParties.Reload();
            FillDropDowns();
        }

        private void barLargeButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegionsForm rg = new RegionsForm();
            rg.ShowDialog();
            xpRegions.Reload();
            FillDropDowns();
        }

        private void DelegatesForm_Shown(object sender, EventArgs e)
        {
            if (_Controller.Mode == DelegatesCntrler.DelegatesFormMode.Manage)
            {
                btnSelect.Visible = false;
            }
            else if (_Controller.Mode == DelegatesCntrler.DelegatesFormMode.Select)
            {
                btnSelect.Visible = true;
            }
            else if (_Controller.Mode == DelegatesCntrler.DelegatesFormMode.SeatSelect)
            {
                btnSelect.Visible = true;
                bar1.Visible = false;

                btnAdd.Visible = false;
                btnRemove.Visible = false;
                btnChange.Visible = false;
                btnPropApply.Visible = false;
                btnPropCancel.Visible = false;
                catSeat.Visible = false;
            }
        }
    }
}