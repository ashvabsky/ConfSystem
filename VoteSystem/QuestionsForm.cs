﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace VoteSystem
{
    public interface IQuestionsForm
    {
        IMsgMethods GetShowInterface();

        DialogResult ShowView();

        void Properties_StartEdit();
        void Properties_FinishEdit();
        void SetProperties(Dictionary<string, string> properties);
    }

    public partial class QuestionsForm : DevExpress.XtraEditors.XtraForm, IQuestionsForm
    {
        QuestionsCntrler _Controller;
        IMsgMethods _msgForm;

        bool _init = false;

        public QuestionsForm()
        {
            InitializeComponent();
            _Controller.Init(ref xpQuestions, ref xpVoteTypes, ref xpVoteKinds);
        }

        public QuestionsForm(QuestionsCntrler cntrler, MainCntrler mcontr, MainForm mform)
        {
            InitializeComponent();
            _Controller = cntrler;

            _msgForm = (IMsgMethods) new MsgForm(this);

            _Controller.Init(ref xpQuestions, ref xpVoteTypes, ref xpVoteKinds);

        }

        #region реализация интерфейса IQuestionsForm

        IMsgMethods IQuestionsForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        DialogResult IQuestionsForm.ShowView()
        {
            return ShowDialog();
        }

        void IQuestionsForm.Properties_StartEdit()
        {

            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControl.Rows)
            {
                row.Properties.ReadOnly = false;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = false;
                }

            }

            // перечислить поля, которые следует оставить как readonly:
            PropertiesControl.Rows["rowid"].Properties.ReadOnly = true;

            btnPropApply.Enabled = true;
            btnPropCancel.Enabled = true;

            splitContainerControl1.Panel1.Enabled = false;

            PropertiesControl.FocusedRow = PropertiesControl.Rows["rowCaption"];
            PropertiesControl.ShowEditor();

        }

        void IQuestionsForm.Properties_FinishEdit()
        {
            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControl.Rows)
            {
                row.Properties.ReadOnly = true;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = true;
                }
            }

            btnPropApply.Enabled = false;
            btnPropCancel.Enabled = false;

            splitContainerControl1.Panel1.Enabled = true;

            _Controller.DataPositionChanged(dataNavigator1.Position);


        }

        void IQuestionsForm.SetProperties(Dictionary<string, string> properties)
        {
            PropertiesControl.Rows["rowid"].Properties.Value = properties["id"];
            PropertiesControl.Rows["rowCaption"].Properties.Value = properties["Caption"];
            PropertiesControl.Rows["rowDescription"].Properties.Value = properties["Description"];
            PropertiesControl.Rows["rowidKind_Name"].Properties.Value = properties["VoteKind"];
            PropertiesControl.Rows["rowidType_Name"].Properties.Value = properties["VoteType"];
        }

#endregion

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            DataNavigator dn = (DataNavigator)sender;
            if (dn == null)
                return;

            _Controller.DataPositionChanged(dn.Position);
        }

        private void StartEdit()
        {

            int Pos = dataNavigator1.Position;
            _Controller.EditRecord(Pos);
        }


        private void QuestionsForm_Load(object sender, EventArgs e)
        {

            btnPropApply.Enabled = false;
            btnPropCancel.Enabled = false;

            FillDropDowns();

            _init = true;
            dataNavigator1_PositionChanged(dataNavigator1, null); // обновить выводимые свойства
        }

        private void FillDropDowns()
        {
            DevExpress.XtraEditors.Repository.RepositoryItemComboBox r1 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControl.Rows["rowidKind_Name"].Properties.RowEdit;
            r1.Items.Clear();

            foreach (VoteKindObj k in xpVoteKinds)
            {
                if (k.Name != null)
                    r1.Items.Add(k.Name);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox r2 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControl.Rows["rowidType_Name"].Properties.RowEdit;
            r2.Items.Clear();

            foreach (VoteTypeObj t in xpVoteTypes)
            {
                if (t.Name != null)
                    r2.Items.Add(t.Name);
            }
        }

        private void btnPropApply_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties["Caption"] = PropertiesControl.Rows["rowCaption"].Properties.Value.ToString(); ;
            properties["Description"] = PropertiesControl.Rows["rowDescription"].Properties.Value.ToString();
            properties["VoteKind"] = PropertiesControl.Rows["rowidKind_Name"].Properties.Value.ToString();
            properties["VoteType"] = PropertiesControl.Rows["rowidType_Name"].Properties.Value.ToString();

            _Controller.ApplyChanges(properties);
        }

        private void gridControl1_DoubleClick_1(object sender, EventArgs e)
        {
            // Checks whether an end-used has double clicked the row indicator.
            GridHitInfo hi = gridView1.CalcHitInfo(gridControl1.PointToClient(MousePosition));
            if (!hi.InRow) return;

            StartEdit();
        }

        private void gridControl1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                StartEdit();
            }
        }

        private void btnPropCancel_Click(object sender, EventArgs e)
        {
            _Controller.CancelChanges();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int i = _Controller.AddNewQuestion();
            if (i >= 0)
                dataNavigator1.Position = i;

            StartEdit();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            DialogResult dr = _msgForm.ShowQuestion("Вы уверены, что хотите удалить выделенную запись?");
            if (dr == DialogResult.Yes)
            {
                _Controller.RemoveQuestion();
            }
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            StartEdit();
        }

        private void xpCollection1_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (_init) // проверка: первоначальная загрузка списка уже осуществлена 
                _Controller.DataPositionChanged(dataNavigator1.Position); // обновить выводимые свойства
        }


        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
/*
            FractionsForm fr = new FractionsForm();
            fr.ShowDialog();
            xpFractions.Reload();
            FillDropDowns();
 */ 
        }

        private void QuestionsForm_Shown(object sender, EventArgs e)
        {
            if (_Controller.Mode == QuestionsCntrler.QuestsFormMode.Manage)
            {
//                btnSelect.Visible = false;
                panelbtnSelect.Visible = false;
            }
            else if (_Controller.Mode == QuestionsCntrler.QuestsFormMode.Select)
            {
//                btnSelect.Visible = true;
                panelbtnSelect.Visible = true;
            }

        }
    }
}