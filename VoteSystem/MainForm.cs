﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        MainCntrler _Controller;
        SelQuestionsCntrler _selquestController;
        ResQuestionsCntrler _resquestController;
        S_DelegatesCntrler _S_DelegatesCntrler;

        string _selectedVoteType = "";
        string _selectedVoteKind = "";
        string _selectedVoteQnty = "";
        string _selectedDecisionProcType = "";

        public MainForm()
        {
            InitializeComponent();
            _Controller = new MainCntrler(this);

            _selquestController = new SelQuestionsCntrler(this, _Controller);
            _resquestController = _Controller.ResQuestCntrler;
            _S_DelegatesCntrler = new S_DelegatesCntrler(this, _Controller);

            _Controller.AddController(_selquestController);

        }

        public DialogResult ShowWarningQuiestion(string Text)
        {
            DialogResult dr = MessageBox.Show(this, Text, "Предупреждение", MessageBoxButtons.YesNoCancel);
            return dr;
        }

        public void ShowWarningMsg(string Text)
        {
            MessageBox.Show(this, Text, "Внимание!", MessageBoxButtons.OK);
        }

        public DialogResult ShowQuestion(string Quest)
        {
            return MessageBox.Show(this, Quest, "Вопрос", MessageBoxButtons.YesNoCancel);
        }

        public void ShowError(string ErrText)
        {
            MessageBox.Show(this, ErrText, "Ошибка!", MessageBoxButtons.OK);
        }

        public bool ShowRetryError(string ErrText)
        {
            DialogResult dr = MessageBox.Show(this, ErrText, "Ошибка!", MessageBoxButtons.RetryCancel);
            if (dr == DialogResult.Cancel)
                return false;
            else
                return true;

        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmAgenda"]);
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmDelegates"]);
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmRegistration"]);
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmQuestions"]);
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmResults"]);

//            navBarGroup3.ItemLinks.Add(navBarControl1.Items["itmReport"]);

            FillDelegates();
            FillQuestions();
            FillVotingResults();

            _Controller.InitSessionPage();

        }

        int _CurrentImageNum = 0;
        public void InitControls_QuorumPage(/*int RegTime, int DelegatesQnty, int RegQnty, int QuorumQnty*/)
        {
            _CurrentImageNum = 0;
            picRegTime.Image = TimerImages.Images[_CurrentImageNum];
            picRegTime.Enabled = false;
            lblRegistration.Enabled = false;
            progressBarRegistration.Enabled = false;

            txtDelegateQnty.Properties.ReadOnly = true;
            
            txtRegQnty.Properties.ReadOnly = true;

            txtQuorumQnty.Properties.ReadOnly = true;

            lblQuorumFinished.Visible = false;

            btnRegCancel.Enabled = true;


//            picRegTime.Visible = true;


//          InitRegParams(RegTime, DelegatesQnty, RegQnty, QuorumQnty);
        }

        public void InitRegParams(int RegTime, int DelegatesQnty, int RegQnty, int QuorumQnty)
        {

            string stime = RegTime.ToString();
            if (stime.Length <= 1)
                stime = "0" + stime;
            lblRegTime.Text = string.Format("{0}:00", stime);

            txtDelegateQnty.Text = DelegatesQnty.ToString();
            txtRegQnty.Text = RegQnty.ToString();
            txtQuorumQnty.Text = QuorumQnty.ToString();
        }

        public void FillQuorumGraphicTabPage(Dictionary<int, S_DelegateObj> SeatDelegatesList, Dictionary<int, SeatObj> SeatsList)
        {
            foreach (  Control c in groupControl10.Controls)
            {
                if (c.Tag == null)
                    continue;

                if (c.Tag.ToString().StartsWith("Mic"))
                {
                    string s = c.Tag.ToString();
                    int MicNum = Convert.ToInt32(s.Substring(3));
                    DevExpress.XtraEditors.PictureEdit pic = (DevExpress.XtraEditors.PictureEdit)c;
                    if (SeatDelegatesList.ContainsKey(MicNum))
                    {
                        S_DelegateObj seatDelegate = SeatDelegatesList[MicNum];
                        if (seatDelegate.IsRegistered == true)
                        {
                            if (seatDelegate.idSeat.OnPhysicalState == false)
                            {
                                pic.Image = imageCollection2.Images[5];
                            }
                            else if (seatDelegate.idSeat.OnLogicalState == false)
                            {
                                pic.Image = imageCollection2.Images[4];
                            }
                            else
                                pic.Image = imageCollection2.Images[1];

                        }
                        else if (seatDelegate.IsRegistered == false)
                        {
                            if (seatDelegate.idSeat.OnPhysicalState == false)
                            {
                                pic.Image = imageCollection2.Images[8];
                            }
                            else if (seatDelegate.idSeat.OnLogicalState == false)
                            {
                                pic.Image = imageCollection2.Images[7];
                            }
                            else
                                pic.Image = imageCollection2.Images[0];
                            //                          c.Enabled = false;
                        }
                    }
                    else
                    {
                        if (SeatsList.ContainsKey(MicNum) && SeatsList[MicNum].OnPhysicalState == false)
                        {
                            pic.Image = imageCollection2.Images[6];
                        }
                        else
                        {
                            pic.Image = imageCollection2.Images[3];
                        }
//                        c.Enabled = false;
                    }
                }
            }

        }

        private void FillDelegates()
        {
        }

        private void FillQuestions()
        {
        }

        private void FillVotingResults()
        {
            DataTable tbl2 = dataSet1.Tables[2];

            DataRow newRow = tbl2.NewRow();

            newRow["Number"] = "1";
            newRow["Caption"] = "Увеличение ставки налога на 5%";
            newRow["DateTime"] = DateTime.Parse("13.02.2009 10:05");
            newRow["Decision"] = "принято";

            tbl2.Rows.Add(newRow);

            DataRow newRow2 = tbl2.NewRow();

            newRow2["Number"] = "2";
            newRow2["Caption"] = "Утверждение расходной части";
            newRow2["DateTime"] = DateTime.Parse("13.02.2009");
            newRow2["Decision"] = "принято";

            tbl2.Rows.Add(newRow2);

            DataRow newRow3 = tbl2.NewRow();

            newRow3["Number"] = "3";
            newRow3["Caption"] = "Принятие решения по правкам в бюджете";
            newRow3["DateTime"] = DateTime.Parse("13.02.2009");
            newRow3["Decision"] = "принято";

            tbl2.Rows.Add(newRow3);
        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            DelegatesForm f = new DelegatesForm();
            f.ShowDialog();
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            QuestionsForm q = new QuestionsForm();
            q.ShowDialog();
        }

        private void navBarControl1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            pgSession.PageVisible = false;
            pgDelegates.PageVisible = false;
            pgSelQuestions.PageVisible = false;
            pgQuorumGraphic.PageVisible = false;
            xtraTabPage5.PageVisible = false;
            pgResQuestions.PageVisible = false;
            xtraTabPage7.PageVisible = false;

            ShowPage(e.Link.ItemName);
        }

        public void ShowPage(string PageName)
        {
            pgSession.PageVisible = false;
            pgDelegates.PageVisible = false;
            pgSelQuestions.PageVisible = false;
            pgQuorumGraphic.PageVisible = false;
            xtraTabPage5.PageVisible = false;
            pgResQuestions.PageVisible = false;
            xtraTabPage7.PageVisible = false;

            if (PageName == "itmAgenda")
            {
                pgSession.PageVisible = true;
            }
            else if (PageName == "itmDelegates")
            {
                pgDelegates.PageVisible = true;
            }
            else if (PageName == "itmQuestions")
            {
                pgSelQuestions.PageVisible = true;
            }
            else if (PageName == "itmRegistration")
            {
                pgQuorumGraphic.PageVisible = true;
            }
            else if (PageName == "itmVoting")
            {
                xtraTabPage5.PageVisible = true;
            }
            else if (PageName == "itmResults")
            {
                pgResQuestions.PageVisible = true;
            }
        }

        private void memoEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            EnableMonitor(true);
            ClearMonitor();

            if (e.Page == null)
                return;

            if (e.Page.Name == "pgSession")
            {
                _Controller.InitSessionPage();
            }
            else if (e.Page.Name == "pgDelegates")
            {
                _Controller.InitDelegatePage();
            }
            else if (e.Page.Name == "pgSelQuestions")
            {
                _selquestController.InitSelQuestionsPage();
            }
            else if (e.Page.Name == "pgQuorumGraphic")
            {
                _S_DelegatesCntrler.InitQuorumGraphicPage();
            }
            else if (e.Page.Name == "pgResQuestions")
            {
                _resquestController.InitResQuestionsPage();
            }
            else
                _Controller.Save();
        }

        public void BindingSessionObj(SessionObj sessionObj)
        {
            if (sessionObj == null)
                return;

            if (txtCode.DataBindings.Count == 0)
                txtCode.DataBindings.Add(new Binding("Text", sessionObj, "Code"));

            if (txtCaption.DataBindings.Count == 0)
                txtCaption.DataBindings.Add(new Binding("Text", sessionObj, "Caption"));

            if (txtContent.DataBindings.Count == 0)
                txtContent.DataBindings.Add(new Binding("Text", sessionObj, "Content"));

            if (txtStartDate.DataBindings.Count == 0)
                txtStartDate.DataBindings.Add(new Binding("DateTime", sessionObj, "StartDate"));

            if (txtQourum.DataBindings.Count == 0)
                txtQourum.DataBindings.Add(new Binding("Text", sessionObj, "Quorum"));
            
            if (txtHall.DataBindings.Count == 0)
                txtHall.DataBindings.Add(new Binding("Text", sessionObj, "HallName"));

            if (chbRegAfterTime.DataBindings.Count == 0)
                chbRegAfterTime.DataBindings.Add(new Binding("Checked", sessionObj, "RegAfterTime"));

            chbRegAfterTime.Checked = false;
            chbRegAfterTime.Checked = sessionObj.RegAfterTime;

            if (txtRegTime.DataBindings.Count == 0)
                txtRegTime.DataBindings.Add(new Binding("Text", sessionObj, "RegTime"));

            if (txtVoteTime.DataBindings.Count == 0)
                txtVoteTime.DataBindings.Add(new Binding("Text", sessionObj, "VoteTime"));

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (xpSessions.Count == 0)
                return;

            _Controller.Init(ref xpDelegates);
            _selquestController.Init(DataModel.TheSession, ref xpQuestions, ref xpVoteTypes, ref xpVoteKinds, ref xpVoteQntyTypes, ref xpDecisionProcTypes, ref xpQuotaProcTypes);
            _resquestController.Init(DataModel.TheSession, ref xpResQuestions, ref xpVoteTypes, ref xpVoteKinds, ref xpVoteQntyTypes, ref xpDecisionProcTypes, ref xpQuotaProcTypes, ref xpVoteResults);
            _S_DelegatesCntrler.Init(DataModel.TheSession, ref xpDelegates, ref xpSeats);
            _resquestController.Init(DataModel.TheSession, ref xpResQuestions, ref xpVoteTypes, ref xpVoteKinds, ref xpVoteQntyTypes, ref xpDecisionProcTypes, ref xpQuotaProcTypes, ref xpVoteResults);
        }

        public void Init1()
        {
            _Controller.UpdatePropData(navigatorDelegates.Position);
        }

        public void InitSelQuestPage()
        {
            grpSelQuestPropsIn.Controls.Add(PropertiesControlQuest);
//          cat_sq_Criteries.Expanded = true;
            cat_sq_Notes.Expanded = true;

            PropertiesControlQuest.Dock = DockStyle.Fill;

            SetViewMode();
            _selquestController.UpdatePropData(navigatorQuestions.Position);
        }

        public void InitResQuestPage()
        {
            grpResQuestPropsIn.Controls.Remove(PropertiesControlQuest2);
            grpResQuestPropsIn.Controls.Add(PropertiesControlQuest);
//            cat_sq_Criteries.Expanded = false;

            SetViewMode();
            _resquestController.UpdatePropData(navigatorResQuestions.Position);
        }

        private void SetViewMode()
        {
            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControlQuest.Rows)
            {
                row.Properties.ReadOnly = true;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = true;
                    foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowinin in rowin.ChildRows)
                    {
                        rowinin.Properties.ReadOnly = true;
                    }
                }
            }

            btnPropQuestApply.Enabled = false;
            btnPropQuestCancel.Enabled = false;
        }

        private void navigatorDelegates_PositionChanged(object sender, EventArgs e)
        {
            DataNavigator dn = (DataNavigator)sender;
            if (dn == null)
                return;

            _Controller.UpdatePropData(dn.Position);

        }


        public void SetProperties(Dictionary<string, string> properties)
        {
            PropertiesControl.Rows["rowFirstName"].Properties.Value = properties["FirstName"];
            PropertiesControl.Rows["rowSecondName"].Properties.Value = properties["SecondName"];
            PropertiesControl.Rows["rowLastName"].Properties.Value = properties["LastName"];

            PropertiesControl.Rows["rowPartyName"].Properties.Value = properties["PartyName"];
            PropertiesControl.Rows["rowFraction"].Properties.Value = properties["FractionName"];
            PropertiesControl.Rows["rowRegionName"].Properties.Value = properties["RegionName"];
            PropertiesControl.Rows["rowRegionNum"].Properties.Value = properties["RegionNum"];

            PropertiesControl.Rows["rowMicNum"].Properties.Value = properties["MicNum"];
            PropertiesControl.Rows["rowRowNum"].Properties.Value = properties["RowNum"];
            PropertiesControl.Rows["rowSeatNum"].Properties.Value = properties["SeatNum"];
        }


        public void SetProperties_QuorumPage(Dictionary<string, string> properties)
        {
            PropertiesControl_QPage.Rows["row_q_FirstName"].Properties.Value = properties["FirstName"];
            PropertiesControl_QPage.Rows["row_q_SecondName"].Properties.Value = properties["SecondName"];
            PropertiesControl_QPage.Rows["row_q_LastName"].Properties.Value = properties["LastName"];

            PropertiesControl_QPage.Rows["row_q_PartyName"].Properties.Value = properties["PartyName"];
            PropertiesControl_QPage.Rows["row_q_Fraction"].Properties.Value = properties["FractionName"];
            PropertiesControl_QPage.Rows["row_q_RegionName"].Properties.Value = properties["RegionName"];
            PropertiesControl_QPage.Rows["row_q_RegionNum"].Properties.Value = properties["RegionNum"];

            PropertiesControl_QPage.Rows["row_q_MicNum"].Properties.Value = properties["MicNum"];
            PropertiesControl_QPage.Rows["row_q_RowNum"].Properties.Value = properties["RowNum"];
            PropertiesControl_QPage.Rows["row_q_SeatNum"].Properties.Value = properties["SeatNum"];
        }

        public void SetPropertiesQuestions(Dictionary<string, object> properties)
        {
            PropertiesControlQuest.Rows["row_sq_Number"].Properties.Value = (string)properties["Number"].ToString();
            PropertiesControlQuest.Rows["row_sq_Caption"].Properties.Value = (string)properties["Caption"];
            PropertiesControlQuest.Rows["row_sq_CrDate"].Properties.Value = properties["CrDate"];

            PropertiesControlQuest.Rows["row_sq_Notes"].Properties.Value = properties["Description"];
            PropertiesControlQuest.Rows["row_sq_VoteType"].Properties.Value = properties["VoteType_Value"];
            _selectedVoteType = properties["VoteType_Name"].ToString();

            PropertiesControlQuest.Rows["row_sq_VoteKind"].Properties.Value = properties["VoteKind_Value"];
            _selectedVoteKind = properties["VoteKind_Name"].ToString();

            PropertiesControlQuest.Rows["row_sq_ReadNum"].Properties.Value = properties["ReadNum"];
            
            PropertiesControlQuest.Rows["row_sq_VoteQnty"].Properties.Value = properties["VoteQntyType_Value"];
            _selectedVoteQnty = properties["VoteQntyType_Name"].ToString();

            PropertiesControlQuest.Rows["row_sq_DecisionProcValue"].Properties.Value = properties["DecisionProc_Value"];

            PropertiesControlQuest.Rows["row_sq_DecisionProcType"].Properties.Value = properties["DecisionProcType_Value"];
            _selectedDecisionProcType = properties["DecisionProcType_Name"].ToString();

            PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = false;
            PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = false;

            if ((string)properties["QuotaProcType_Name"] == "Total_Qnty")
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = true;
            else if ((string)properties["QuotaProcType_Name"] == "Present_Qnty")
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = true;

            if (properties["Notes"] == "" && xtraTabControl1.SelectedTabPage.Name == "pgResQuestions")
                cat_sq_Notes.Expanded = false;
            else
                cat_sq_Notes.Expanded = true;


        }

        private void btnPropQuestApply_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties["Notes"] = PropertiesControlQuest.Rows["row_sq_Notes"].Properties.Value;
            properties["VoteType_Name"] = _selectedVoteType;

            properties["VoteKind_Name"] = _selectedVoteKind;

            properties["VoteQntyType_Name"] = _selectedVoteQnty;

            properties["DecisionProc_Value"] = PropertiesControlQuest.Rows["row_sq_DecisionProcValue"].Properties.Value;

            properties["DecisionProcType_Name"] = _selectedDecisionProcType;

            properties["QuotaProcType_Name"] = "";

            if ((bool)PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value == true)
                properties["QuotaProcType_Name"] = "Total_Qnty";
            else if((bool)PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value == true)
                properties["QuotaProcType_Name"] = "Present_Qnty";

            _selquestController.ApplyChanges(properties);
        }

        private void btnPropQuestCancel_Click(object sender, EventArgs e)
        {
            _selquestController.CancelChanges();

            // обновить поля
            _selquestController.UpdatePropData(navigatorQuestions.Position);

        }

        private void StartEditQuestion()
        {

            int Pos = navigatorQuestions.Position;
            _selquestController.EditRecord(Pos);
        }

        public void PropertiesQuestions_StartEdit()
        {

            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControlQuest.Rows)
            {
                row.Properties.ReadOnly = false;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = false;
                    foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowinin in rowin.ChildRows)
                    {
                        rowinin.Properties.ReadOnly = false;
                    }
                }

            }

            // перечислить поля, которые следует оставить как readonly:
            PropertiesControlQuest.Rows["row_sq_Number"].Properties.ReadOnly = true;
            PropertiesControlQuest.Rows["row_sq_Caption"].Properties.ReadOnly = true;
            PropertiesControlQuest.Rows["row_sq_CrDate"].Properties.ReadOnly = true;
            PropertiesControlQuest.Rows["row_sq_ReadNum"].Properties.ReadOnly = true;

            btnPropQuestApply.Enabled = true;
            btnPropQuestCancel.Enabled = true;

            SelQuestGrid.Enabled = false;

            PropertiesControlQuest.FocusedRow = PropertiesControlQuest.Rows["row_sq_Notes"];
            PropertiesControlQuest.ShowEditor();

        }

        public void Properties_FinishEdit()
        {
            SetViewMode();

            SelQuestGrid.Enabled = true;

        }


        private void navigatorDelegates_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Append")
                {
                    int i = _Controller.AppendDelegate();
                    if (i >= 0)
                        navigatorDelegates.Position = i;

                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    DialogResult dr = ShowQuestion("Вы уверены, что хотите удалить выделенную запись?");
                    if (dr == DialogResult.Yes)
                    {
                        _Controller.RemoveDelegate();
                    }

                }
            }
        }

        private void navigatorQuestions_PositionChanged(object sender, EventArgs e)
        {
            ClearMonitor();

            DataNavigator dn = (DataNavigator)sender;
            if (dn == null)
                return;

            _selquestController.UpdatePropData(dn.Position);
        }

        private void navigatorResQuestions_PositionChanged(object sender, EventArgs e)
        {
            ClearMonitor();

            navigatorResQuestions_PositionChanged();
        }

        private void navigatorResQuestions_PositionChanged()
        {
            ClearMonitor();
            _resquestController.UpdatePropData(navigatorResQuestions.Position);
        }

        public void FillDropDowns()
        {
            DevExpress.XtraEditors.Repository.RepositoryItemComboBox list = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControlQuest.Rows["row_sq_VoteType"].Properties.RowEdit;
            list.Items.Clear();

            foreach (VoteTypeObj vt in xpVoteTypes)
            {
                if (vt.Name != null)
                    list.Items.Add(vt.Name);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox list2 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControlQuest.Rows["row_sq_VoteKind"].Properties.RowEdit;
            list2.Items.Clear();

            foreach (VoteKindObj vk in xpVoteKinds)
            {
                if (vk.Name != null)
                    list2.Items.Add(vk.Name);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox list3 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControlQuest.Rows["row_sq_VoteQnty"].Properties.RowEdit;
            list3.Items.Clear();

            foreach (VoteQntyObj vq in xpVoteQntyTypes)
            {
                if (vq.Value != null)
                    list3.Items.Add(vq.Value);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox list4 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControlQuest.Rows["row_sq_DecisionProcType"].Properties.RowEdit;
            list4.Items.Clear();

            foreach (DecisionProcTypeObj dpt in xpDecisionProcTypes)
            {
                if (dpt.Value != null)
                    list4.Items.Add(dpt.Value);
            }

        }

        private void navigatorQuestions_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Append")
                {
                    int i = _selquestController.AppendItem();
                    if (i >= 0)
                        navigatorQuestions.Position = i;

                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    DialogResult dr = ShowQuestion("Вы уверены, что хотите удалить выделенную запись?");
                    if (dr == DialogResult.Yes)
                    {
                        _selquestController.RemoveItem();
                        _selquestController.UpdatePropData(navigatorQuestions.Position);
                    }

                }
                else if (e.Button.Tag.ToString() == "Edit")
                {
                        StartEditQuestion();
                }
            }
        }

        private void repository_cmb_votetypes_SelectedIndexChanged(object sender, EventArgs e)
        {

            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            VoteTypeObj vto = (VoteTypeObj)xpVoteTypes[cmb.SelectedIndex];

            _selectedVoteType = vto.Name;
         }

        private void repository_cmb_VoteKinds_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            VoteKindObj vko = (VoteKindObj)xpVoteKinds[cmb.SelectedIndex];
            _selectedVoteKind = vko.Name;
        }

        private void repository_cmb_VoteQntyTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            VoteQntyObj vqo = (VoteQntyObj)xpVoteQntyTypes[cmb.SelectedIndex];
            _selectedVoteQnty = vqo.Name;

        }

        private void repository_cmb_DecisionProcTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            DecisionProcTypeObj dpt = (DecisionProcTypeObj)xpDecisionProcTypes[cmb.SelectedIndex];
            _selectedDecisionProcType = dpt.Name;

        }

        private void repository_cedt_QuotaProcType_Present_EditValueChanged(object sender, EventArgs e)
        {
            CheckEdit chkedit = (CheckEdit)sender;
            if ((bool)chkedit.EditValue == true)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = false;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = true;
            }
            else if ((bool)chkedit.EditValue == false)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = true;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = false;
            }
        }

        private void repository_cedt_QuotaProcType_Total_EditValueChanged(object sender, EventArgs e)
        {
            CheckEdit chkedit = (CheckEdit)sender;
            if ((bool)chkedit.EditValue == true)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = true;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = false;
            }
            else if ((bool)chkedit.EditValue == false)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = false;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = true;
            }
        }

        private void Mic_MouseEnter(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            if (c.Tag == null)
            {
                _S_DelegatesCntrler.ShowDelegateProp(-1); // показать пустые поля
                return;
            }

            string s = c.Tag.ToString();
            int MicNum = Convert.ToInt32(s.Substring(3));

            _S_DelegatesCntrler.ShowDelegateProp(MicNum);

        }

        private void Mic_DragDrop(object sender, DragEventArgs e)
        {
            PictureEdit mic = (PictureEdit)sender;
            mic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;

            int DestSeatNum = 0;
            string s = mic.Tag.ToString();
            if (s != "")
                DestSeatNum = Convert.ToInt32(s.Substring(3));

            string[] formats = e.Data.GetFormats();
            bool b = e.Data.GetDataPresent("System.String");
            if (b == true)
            {
                string source = (string)e.Data.GetData("System.String");
                if (source != "")
                {
                    int SourceSeatNum = Convert.ToInt32(source.Substring(3));
                    _S_DelegatesCntrler.MoveDelegate(SourceSeatNum, DestSeatNum);
                }
            }
        }

        private void Mic_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PictureEdit mic = (PictureEdit)sender;

                string s = mic.Tag.ToString();
                int MicNum = Convert.ToInt32(s.Substring(3));

                mic.DoDragDrop(s, DragDropEffects.Move);
            }


        }

        private void Mic_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void Mic_DragOver2(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void Mic_DragEnter(object sender, DragEventArgs e)
        {

            PictureEdit pic = (PictureEdit)sender;
            pic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;

        }

        private void Mic_DragLeave(object sender, EventArgs e)
        {
            PictureEdit pic = (PictureEdit)sender;
            pic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
        }


        private void cntSwitherPhysical_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem item = (BarEditItem)sender;
            bool value = (bool)item.EditValue;
            _S_DelegatesCntrler.SetSwitchersValuePhysic(value);

        }

        private void cntSwitherLogical_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem item = (BarEditItem)sender;
            bool value = (bool)item.EditValue;
            _S_DelegatesCntrler.SetSwitchersValueLogic(value);
        }

        private void cntSeatFree_ItemPress(object sender, ItemClickEventArgs e)
        {
            _S_DelegatesCntrler.FreeCurrentSeat();
        }

        private void menuMicSeat_Popup(object sender, EventArgs e)
        {
            bool SwitcherPhysicalState = false;
            bool SwitcherLogicalState = false;
            bool SeatFree_Enable = true;
            bool SeatSelect_Enable = true;
            bool AllDisable = false;

            _S_DelegatesCntrler.GetPopupMenuStates(ref SwitcherPhysicalState, ref SwitcherLogicalState, ref SeatFree_Enable, ref SeatSelect_Enable, ref AllDisable);
            if (AllDisable)
            {
                cntSwitherPhysical.Enabled = false;
                cntSwitherLogical.Enabled = false;
                cntSeatFree.Enabled = false;
                cntSeatSelect.Enabled = false;
            }

            cntSwitherPhysical.EditValue = SwitcherPhysicalState;
            cntSwitherLogical.EditValue = SwitcherLogicalState;
            cntSeatFree.Enabled = SeatFree_Enable;
            cntSeatSelect.Enabled = SeatSelect_Enable;
            
        }

        private void cntSeatSelect_ItemClick(object sender, ItemClickEventArgs e)
        {
            _S_DelegatesCntrler.Delegate_SetToSeat();
        }

        private void pgSession_Leave(object sender, EventArgs e)
        {
            _Controller.SaveSession();
        }

        private void btnRegStart_Click(object sender, EventArgs e)
        {
            if (_S_DelegatesCntrler.RegStartMode == false)
                _S_DelegatesCntrler.Registration_PrepareToStart();
            else
                _S_DelegatesCntrler.Registration_Stop_Ask();
            
        }

        DateTime _startregtime;
        int _RegTime; 
        int _RegQnty = 0;

        public void StartRegistration(int RegTime)
        {
            btnRegCancel.Enabled = false;
            picRegTime.Enabled = true;
            lblRegistration.Visible = true;
            lblRegistration.Enabled = true;
            progressBarRegistration.Enabled = true;
            progressBarRegistration.Visible = true;
            btnRegCancel.Enabled = false;

            btnRegStart.Text = "Завершить регистрацию";
            btnRegStart.ImageIndex = 9;

            _RegTime = RegTime;
            _startregtime = DateTime.Now;

            tmrRegStart.Start();
            tmrRotation.Start();
        }

        private void tmrRegStart_Tick(object sender, EventArgs e)
        {

            System.TimeSpan time = DateTime.Now - _startregtime;
//          DateTime t = new DateTime(time.Ticks);
            DateTime quorumTime = new DateTime(1900, 12, 13, 0, _RegTime, 0);

            DateTime t = quorumTime.AddTicks(-time.Ticks); 
            lblRegTime.Text = t.ToString("T");

            int QuorumSecs = quorumTime.Minute * 60 + quorumTime.Second;
            DateTime t2 = new DateTime(time.Ticks);
            int Secs = t2.Minute * 60 + t2.Second;

            int pos = (Secs * 100) / QuorumSecs;

            _S_DelegatesCntrler.OnRegProcess();

            progressBarRegistration.Position = pos;
            if (t.Minute == 0 && t.Second == 0)
            {
                _S_DelegatesCntrler.Registration_Stop();
            }

        }


        public void StopRegistration()
        {
            if (tmrRegStart.Enabled == true)
            {
                tmrRegStart.Stop();
                tmrRotation.Stop();
            }

            progressBarRegistration.Position = 0;

            _CurrentImageNum = 0;
            picRegTime.Image = TimerImages.Images[_CurrentImageNum];

            picRegTime.Enabled = false;
            lblRegistration.Enabled = false;
            progressBarRegistration.Enabled = false;

            btnRegStart.Text = "Начать регистрацию";
            btnRegStart.ImageIndex = 0;

            btnRegCancel.Enabled = true;

            lblQuorumFinished.Visible = false;
            IsQuorum = false;
        }

        private void tmrRotation_Tick(object sender, EventArgs e)
        {
            _CurrentImageNum ++;
            if (_CurrentImageNum > TimerImages.Images.Count - 1)
                _CurrentImageNum = 0;

            picRegTime.Image = TimerImages.Images[_CurrentImageNum];
        }

        public int ReqQnty
        {
            set { txtRegQnty.Text = value.ToString(); }
        }

        public bool IsQuorum
        {
            set
            {
                if (value == true)
                {
                    lblQuorumFinished.Visible = true;
//                  lblQuorumFinished.Text = "Кворум достигнут!";
                }
                else
                {
                    lblQuorumFinished.Visible = false;
                }
            }
                
        }

        private void btnRegCancel_Click(object sender, EventArgs e)
        {
            _S_DelegatesCntrler.Registration_Clear_Ask();
        }


        public void ShowVoteResults(VoteResultObj vro)
        {
            if (vro.id != 0)
            {
                grpVoteResults.Visible = true;
                lblVoteDateTime.Visible = true;

                lblDecision.Text = vro.idResultValue.Value;

                lblVoteDateTime.Text = vro.VoteDateTime.ToString("t");

                lblVoteAye.Text = vro.AnsQnty1.ToString();
                lblVoteAgainst.Text = vro.AnsQnty2.ToString();
                lblVoteAbstain.Text = vro.AnsQnty3.ToString();

                int votes = vro.AnsQnty1 + vro.AnsQnty2+ vro.AnsQnty3;
                int votes_proc = (votes * 100)/ vro.AvailableQnty;

                int procAye = (vro.AnsQnty1 * 100) / vro.AvailableQnty;
                int procAgainst = (vro.AnsQnty2 * 100) / vro.AvailableQnty;
                int procAbstain = (vro.AnsQnty3 * 100) / vro.AvailableQnty;

                lblProcAye.Text = procAye.ToString();
                lblProcAgainst.Text = procAgainst.ToString();
                lblProcAbstain.Text = procAbstain.ToString();

                int notvoted = vro.AvailableQnty - votes;
                int notvoted_proc = (notvoted * 100) / vro.AvailableQnty;

                lblNonVoteQnty.Text = notvoted.ToString();
                lblNonProcQnty.Text = notvoted_proc.ToString();

                lblVoteQnty.Text = string.Format("{0} из {1}", votes, vro.AvailableQnty);
                lblProcQnty.Text = string.Format("{0}", votes_proc);

            }
            else
            {
                lblDecision.Text = "Нет решения";
                grpVoteResults.Visible = false;
                lblVoteDateTime.Visible = false;

                lblVoteQnty.Text = "";
                lblProcQnty.Text = "";
            }

        }

        private void btnQuestVoteCancel_Click(object sender, EventArgs e)
        {
            _resquestController.ResultCancel();
            navigatorResQuestions_PositionChanged();
        }

        private void btnQuestNewRead_Click(object sender, EventArgs e)
        {
            _resquestController.CreateNewReading();
        }

        public void ShowSelQuestsPage(int selIndex)
        {
            ShowPage("itmQuestions");
            _selquestController.SetCurrentPos(selIndex);
        }

        public void SetNavigatorPos_QuestPage(int iPos)
        {
            navigatorQuestions.Position = iPos;
            _selquestController.UpdatePropData(navigatorQuestions.Position);
        }

        public void SetNavigatorPos_ResQuestPage(int iPos)
        {
            ClearMonitor();
            navigatorResQuestions.Position = iPos;
            _resquestController.UpdatePropData(navigatorQuestions.Position);
        }

        private void btnQuestVoteMode_Click(object sender, EventArgs e)
        {
            EnableMonitor(false);

            _Controller.VoteModeRun();
        }

        private void btnQuestVoteDetail_Click(object sender, EventArgs e)
        {
            _Controller.VoteDetailsRun();
        }

        private void barbtnInfoToMonitor_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (barbtnClearMonitor.Checked == true)
            {
                if (barbtnSplashToMonitor.Checked == true)
                    barbtnSplashToMonitor.Checked = false;
            }
        }

        private void barbtnSplashToMonitor_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (barbtnSplashToMonitor.Checked == true)
            {
                if (barbtnClearMonitor.Checked == true)
                    barbtnClearMonitor.Checked = false;
            }
        }

        public void ClearMonitor()
        {
//          barbtnClearMonitor.Checked = true;
        }
            
        public void EnableMonitor(bool IsEnable)
        {
  //        ClearMonitor();
            barbtnInfoToMonitor.Enabled = IsEnable;
        }

        private void barbtnInfoToMonitor_ItemClick(object sender, ItemClickEventArgs e)
        {
            MonitorPreviewForm mpf = new MonitorPreviewForm();
            mpf.ShowDialog();
        }

        private void barbtnExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            Application.Exit();
        }

        private void barbtnPredsedSendMsg_ItemClick(object sender, ItemClickEventArgs e)
        {
            SendTextForm stf = new SendTextForm();
            stf.Text = "Текстовое сообщение для председателя";
            stf.ShowDialog();
        }

        private void barbtnMonitorMsg_ItemClick(object sender, ItemClickEventArgs e)
        {
            SendTextForm stf = new SendTextForm();
            stf.Text = "Текстовое сообщение на мониторы в зале";
            stf.ShowDialog();
        }

/*
        private void Mic_1_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            PictureEdit pic = (PictureEdit)sender;

            if (pic != null)
            {

//              Form f = pic.FindForm();

                // Cancel the drag if the mouse moves off the form. The screenOffset
                // takes into account any desktop bands that may be at the top or left
                // side of the screen.
                Point screenOffset = SystemInformation.WorkingArea.Location;

                if (((Control.MousePosition.X - screenOffset.X) < this.DesktopBounds.Left) ||
                    ((Control.MousePosition.X - screenOffset.X) > this.DesktopBounds.Right) ||
                    ((Control.MousePosition.Y - screenOffset.Y) < this.DesktopBounds.Top) ||
                    ((Control.MousePosition.Y - screenOffset.Y) > this.DesktopBounds.Bottom))
                {

                    e.Action = DragAction.Cancel;
                }
            }
        }
*/
    }
}