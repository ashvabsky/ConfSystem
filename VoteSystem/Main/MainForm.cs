﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm 
    {
        MsgForm _msgForm;
        IMsgMethods _messages;

        MainCntrler _Controller;

        string _selectedVoteType = "";
        string _selectedVoteKind = "";
        string _selectedVoteQnty = "";
        string _selectedDecisionProcType = "";

        public MainForm()
        {
            InitializeComponent();

            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;

            _Controller = new MainCntrler(this);


        }


        private void MainForm_Shown(object sender, EventArgs e)
        {
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmAgenda"]);
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmDelegates"]);
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmRegistration"]);
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmQuestions"]);
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmResults"]);
            navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmStatements"]);

//            navBarGroup3.ItemLinks.Add(navBarControl1.Items["itmReport"]);


            _Controller.InitSessionPage();

        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            DelegatesCntrler d = (DelegatesCntrler)_Controller.GetSubController("Delegate List Controller");
            d.ShowView();
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            QuestionsCntrler q = (QuestionsCntrler)_Controller.GetSubController("Questions List Controller");
            q.ShowView();

        }

        private void navBarControl1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            pgSession.PageVisible = false;
            pgDelegates.PageVisible = false;
            pgSelQuestions.PageVisible = false;
            pgQuorumGraphic.PageVisible = false;
            pgResQuestions.PageVisible = false;
            pgStatements.PageVisible = false;

            ShowPage(e.Link.ItemName);
        }

        public void ShowPage(string PageName)
        {
            pgSession.PageVisible = false;
            pgDelegates.PageVisible = false;
            pgSelQuestions.PageVisible = false;
            pgQuorumGraphic.PageVisible = false;
            pgResQuestions.PageVisible = false;
            pgStatements.PageVisible = false;

            if (PageName == "itmAgenda")
            {
                pgSession.PageVisible = true;
            }
            else if (PageName == "itmDelegates")
            {
                pgDelegates.PageVisible = true;
            }
            else if (PageName == "itmQuestions")
            {
                pgSelQuestions.PageVisible = true;
            }
            else if (PageName == "itmRegistration")
            {
                pgQuorumGraphic.PageVisible = true;
            }
            else if (PageName == "itmResults")
            {
                pgResQuestions.PageVisible = true;
            }
            else if (PageName == "itmStatements")
            {
                pgStatements.PageVisible = true;
            }
        }


        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            EnableMonitor(true);
            ClearMonitor();

            if (e.Page == null)
                return;

            _Controller.InitPage(e.Page.Name);
        }

        public void BindingSessionObj(SessionObj sessionObj)
        {
            if (sessionObj == null)
                return;

            if (txtCode.DataBindings.Count == 0)
                txtCode.DataBindings.Add(new Binding("Text", sessionObj, "Code"));

            if (txtCaption.DataBindings.Count == 0)
                txtCaption.DataBindings.Add(new Binding("Text", sessionObj, "Caption"));

            if (txtContent.DataBindings.Count == 0)
                txtContent.DataBindings.Add(new Binding("Text", sessionObj, "Content"));

            if (txtStartDate.DataBindings.Count == 0)
                txtStartDate.DataBindings.Add(new Binding("DateTime", sessionObj, "StartDate"));

            if (txtQourum.DataBindings.Count == 0)
                txtQourum.DataBindings.Add(new Binding("Text", sessionObj, "Quorum"));
            
            if (txtHall.DataBindings.Count == 0)
                txtHall.DataBindings.Add(new Binding("Text", sessionObj, "HallName"));

            if (chbRegAfterTime.DataBindings.Count == 0)
                chbRegAfterTime.DataBindings.Add(new Binding("Checked", sessionObj, "RegAfterTime"));

            chbRegAfterTime.Checked = false;
            chbRegAfterTime.Checked = sessionObj.RegAfterTime;

            if (txtRegTime.DataBindings.Count == 0)
                txtRegTime.DataBindings.Add(new Binding("Text", sessionObj, "RegTime"));

            if (txtVoteTime.DataBindings.Count == 0)
                txtVoteTime.DataBindings.Add(new Binding("Text", sessionObj, "VoteTime"));

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (xpSessions.Count == 0)
                return;

            _Controller.Init(ref xpDelegates, ref xpQuestions);

            _selquestController = (SelQuestionsCntrler)_Controller.GetSubController("VoteQuestPage Controller"); ;
            _resquestController = (ResQuestionsCntrler)_Controller.GetSubController("ResultsPage Controller"); ;

            _S_DelegatesCntrler = (S_DelegatesCntrler)_Controller.GetSubController("QuorumPage Controller");

            _selquestController.Init( ref xpQuestions);
            _S_DelegatesCntrler.Init();
            _resquestController.Init(ref xpResQuestions);
        }

        public void Init1()
        {
            _Controller.UpdatePropData(navigatorDelegates.Position);
        }


        private void navigatorDelegates_PositionChanged(object sender, EventArgs e)
        {
            DataNavigator dn = (DataNavigator)sender;
            if (dn == null)
                return;

            _Controller.UpdatePropData(dn.Position);

        }

        public void SetProperties_Delegate(Dictionary<string, string> properties)
        {
            PropertiesControl.Rows["rowFirstName"].Properties.Value = properties["FirstName"];
            PropertiesControl.Rows["rowSecondName"].Properties.Value = properties["SecondName"];
            PropertiesControl.Rows["rowLastName"].Properties.Value = properties["LastName"];

            PropertiesControl.Rows["rowPartyName"].Properties.Value = properties["PartyName"];
            PropertiesControl.Rows["rowFraction"].Properties.Value = properties["FractionName"];
            PropertiesControl.Rows["rowRegionName"].Properties.Value = properties["RegionName"];
            PropertiesControl.Rows["rowRegionNum"].Properties.Value = properties["RegionNum"];

            PropertiesControl.Rows["rowMicNum"].Properties.Value = properties["MicNum"];
            PropertiesControl.Rows["rowRowNum"].Properties.Value = properties["RowNum"];
            PropertiesControl.Rows["rowSeatNum"].Properties.Value = properties["SeatNum"];
        }

        private void navigatorDelegates_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Append")
                {
                    int i = _Controller.AppendDelegate();
                    if (i >= 0)
                        navigatorDelegates.Position = i;

                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    DialogResult dr = _messages.ShowQuestion("Вы уверены, что хотите удалить выделенную запись?");
                    if (dr == DialogResult.Yes)
                    {
                        _Controller.RemoveDelegate();
                    }

                }
            }
        }

        private void pgSession_Leave(object sender, EventArgs e)
        {
            _Controller.SaveSession();
        }

        public void ShowSelQuestsPage()
        {
            ShowPage("itmQuestions");
        }

        private void barbtnInfoToMonitor_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (barbtnClearMonitor.Checked == true)
            {
                if (barbtnSplashToMonitor.Checked == true)
                    barbtnSplashToMonitor.Checked = false;
            }
        }

        private void barbtnSplashToMonitor_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (barbtnSplashToMonitor.Checked == true)
            {
                if (barbtnClearMonitor.Checked == true)
                    barbtnClearMonitor.Checked = false;
            }
        }

        public void ClearMonitor()
        {
//          barbtnClearMonitor.Checked = true;
        }
            
        public void EnableMonitor(bool IsEnable)
        {
  //        ClearMonitor();
            barbtnInfoToMonitor.Enabled = IsEnable;
        }

        private void barbtnInfoToMonitor_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.ShowMonitorPreview();
        }

        private void barbtnExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            Application.Exit();
        }

        private void barbtnPredsedSendMsg_ItemClick(object sender, ItemClickEventArgs e)
        {
            SendTextForm stf = new SendTextForm();
            stf.Text = "Текстовое сообщение для председателя";
            stf.ShowDialog();
        }

        private void barbtnMonitorMsg_ItemClick(object sender, ItemClickEventArgs e)
        {
            SendTextForm stf = new SendTextForm();
            stf.Text = "Текстовое сообщение на мониторы в зале";
            stf.ShowDialog();
        }


        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.ShowMonitorPreview();

        }

        private void btnStatementMode_Click(object sender, EventArgs e)
        {
            if (navigatorStatements.Position < 0)
                return;

            StatementObj st = xpStatements[navigatorStatements.Position] as StatementObj;

            StatementForm f = new StatementForm(st);
            f.ShowDialog();
        }

    }
}
