﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public interface IQuorumPage
    {
        IMsgMethods GetShowInterface();
//      void ShowView();

        void InitControls();
        void FillQuorumGraphicTabPage(Dictionary<int, S_DelegateObj> SeatDelegatesList, Dictionary<int, SeatObj> SeatsList);
        void SetProperties(Dictionary<string, string> properties);
        void StartRegistration(int RegTime);
        void InitRegParams(int RegTime, int DelegatesQnty, int RegQnty, int QuorumQnty);
        void StopRegistration();
        int ReqQnty{ set; }
        bool IsQuorum { set; }
    }

    partial class MainForm : IQuorumPage
    {
        S_DelegatesCntrler _S_DelegatesCntrler;

        int _CurrentImageNum = 0;

        DateTime _startregtime;
        int _RegTime;
        int _RegQnty = 0;


        #region реализация интерфейса IQuorumPage
        IMsgMethods IQuorumPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        void IQuorumPage.InitControls()
        {
            _CurrentImageNum = 0;
            picRegTime.Image = TimerImages.Images[_CurrentImageNum];
            picRegTime.Enabled = false;
            lblRegistration.Enabled = false;
            progressBarRegistration.Enabled = false;

            txtDelegateQnty.Properties.ReadOnly = true;

            txtRegQnty.Properties.ReadOnly = true;

            txtQuorumQnty.Properties.ReadOnly = true;

            lblQuorumFinished.Visible = false;

            btnRegCancel.Enabled = true;
        }

        void IQuorumPage.FillQuorumGraphicTabPage(Dictionary<int, S_DelegateObj> SeatDelegatesList, Dictionary<int, SeatObj> SeatsList)
        {
            foreach (Control c in groupControl10.Controls)
            {
                if (c.Tag == null)
                    continue;

                if (c.Tag.ToString().StartsWith("Mic"))
                {
                    string s = c.Tag.ToString();
                    int MicNum = Convert.ToInt32(s.Substring(3));
                    DevExpress.XtraEditors.PictureEdit pic = (DevExpress.XtraEditors.PictureEdit)c;
                    if (SeatDelegatesList.ContainsKey(MicNum))
                    {
                        S_DelegateObj seatDelegate = SeatDelegatesList[MicNum];
                        if (seatDelegate.IsRegistered == true)
                        {
                            if (seatDelegate.idSeat.OnPhysicalState == false)
                            {
                                pic.Image = imageCollection2.Images[5];
                            }
                            else if (seatDelegate.idSeat.OnLogicalState == false)
                            {
                                pic.Image = imageCollection2.Images[4];
                            }
                            else
                                pic.Image = imageCollection2.Images[1];

                        }
                        else if (seatDelegate.IsRegistered == false)
                        {
                            if (seatDelegate.idSeat.OnPhysicalState == false)
                            {
                                pic.Image = imageCollection2.Images[8];
                            }
                            else if (seatDelegate.idSeat.OnLogicalState == false)
                            {
                                pic.Image = imageCollection2.Images[7];
                            }
                            else
                                pic.Image = imageCollection2.Images[0];
                        }
                    }
                    else
                    {
                        if (SeatsList.ContainsKey(MicNum) && SeatsList[MicNum].OnPhysicalState == false)
                        {
                            pic.Image = imageCollection2.Images[6];
                        }
                        else
                        {
                            pic.Image = imageCollection2.Images[3];
                        }
                    }
                }
            }

        }

        void IQuorumPage.SetProperties(Dictionary<string, string> properties)
        {
            PropertiesControl_QPage.Rows["row_q_FirstName"].Properties.Value = properties["FirstName"];
            PropertiesControl_QPage.Rows["row_q_SecondName"].Properties.Value = properties["SecondName"];
            PropertiesControl_QPage.Rows["row_q_LastName"].Properties.Value = properties["LastName"];

            PropertiesControl_QPage.Rows["row_q_PartyName"].Properties.Value = properties["PartyName"];
            PropertiesControl_QPage.Rows["row_q_Fraction"].Properties.Value = properties["FractionName"];
            PropertiesControl_QPage.Rows["row_q_RegionName"].Properties.Value = properties["RegionName"];
            PropertiesControl_QPage.Rows["row_q_RegionNum"].Properties.Value = properties["RegionNum"];

            PropertiesControl_QPage.Rows["row_q_MicNum"].Properties.Value = properties["MicNum"];
            PropertiesControl_QPage.Rows["row_q_RowNum"].Properties.Value = properties["RowNum"];
            PropertiesControl_QPage.Rows["row_q_SeatNum"].Properties.Value = properties["SeatNum"];
        }

        void IQuorumPage.StartRegistration(int RegTime)
        {
            btnRegCancel.Enabled = false;
            picRegTime.Enabled = true;
            lblRegistration.Visible = true;
            lblRegistration.Enabled = true;
            progressBarRegistration.Enabled = true;
            progressBarRegistration.Visible = true;
            btnRegCancel.Enabled = false;

            btnRegStart.Text = "Завершить регистрацию";
            btnRegStart.ImageIndex = 9;

            _RegTime = RegTime;
            _startregtime = DateTime.Now;

            tmrRegStart.Start();
            tmrRotation.Start();
        }

        void IQuorumPage.InitRegParams(int RegTime, int DelegatesQnty, int RegQnty, int QuorumQnty)
        {
            string stime = RegTime.ToString();
            if (stime.Length <= 1)
                stime = "0" + stime;
            lblRegTime.Text = string.Format("{0}:00", stime);

            txtDelegateQnty.Text = DelegatesQnty.ToString();
            txtRegQnty.Text = RegQnty.ToString();
            txtQuorumQnty.Text = QuorumQnty.ToString();

            if (RegQnty >= QuorumQnty)
                IsQuorum = true;
            else
                IsQuorum = false;

        }

        void IQuorumPage.StopRegistration()
        {
            if (tmrRegStart.Enabled == true)
            {
                tmrRegStart.Stop();
                tmrRotation.Stop();
            }

            progressBarRegistration.Position = 0;

            _CurrentImageNum = 0;
            picRegTime.Image = TimerImages.Images[_CurrentImageNum];

            picRegTime.Enabled = false;
            lblRegistration.Enabled = false;
            progressBarRegistration.Enabled = false;

            btnRegStart.Text = "Начать регистрацию";
            btnRegStart.ImageIndex = 0;

            btnRegCancel.Enabled = true;

        }

        int IQuorumPage.ReqQnty
        {
            set { txtRegQnty.Text = value.ToString(); }
        }

        bool IQuorumPage.IsQuorum {set{IsQuorum = value;}}
        bool IsQuorum
        {
            set
            {
                if (value == true)
                {
                    lblQuorumFinished.Visible = true;
                    //                  lblQuorumFinished.Text = "Кворум достигнут!";
                }
                else
                {
                    lblQuorumFinished.Visible = false;
                }
            }

        }

        #endregion



        private void Mic_MouseEnter(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            if (c.Tag == null)
            {
                _S_DelegatesCntrler.ShowDelegateProp(-1); // показать пустые поля
                return;
            }

            string s = c.Tag.ToString();
            int MicNum = Convert.ToInt32(s.Substring(3));

            _S_DelegatesCntrler.ShowDelegateProp(MicNum);

        }

        private void Mic_DragDrop(object sender, DragEventArgs e)
        {
            PictureEdit mic = (PictureEdit)sender;
            mic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;

            int DestSeatNum = 0;
            string s = mic.Tag.ToString();
            if (s != "")
                DestSeatNum = Convert.ToInt32(s.Substring(3));

            string[] formats = e.Data.GetFormats();
            bool b = e.Data.GetDataPresent("System.String");
            if (b == true)
            {
                string source = (string)e.Data.GetData("System.String");
                if (source != "")
                {
                    int SourceSeatNum = Convert.ToInt32(source.Substring(3));
                    _S_DelegatesCntrler.MoveDelegate(SourceSeatNum, DestSeatNum);
                }
            }
        }

        private void Mic_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PictureEdit mic = (PictureEdit)sender;

                string s = mic.Tag.ToString();
                int MicNum = Convert.ToInt32(s.Substring(3));

                mic.DoDragDrop(s, DragDropEffects.Move);
            }


        }

        private void Mic_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void Mic_DragOver2(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void Mic_DragEnter(object sender, DragEventArgs e)
        {

            PictureEdit pic = (PictureEdit)sender;
            pic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;

        }

        private void Mic_DragLeave(object sender, EventArgs e)
        {
            PictureEdit pic = (PictureEdit)sender;
            pic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
        }


        private void cntSwitherPhysical_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem item = (BarEditItem)sender;
            bool value = (bool)item.EditValue;
            _S_DelegatesCntrler.SetSwitchersValuePhysic(value);

        }

        private void cntSwitherLogical_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem item = (BarEditItem)sender;
            bool value = (bool)item.EditValue;
            _S_DelegatesCntrler.SetSwitchersValueLogic(value);
        }

        private void cntSeatFree_ItemPress(object sender, ItemClickEventArgs e)
        {
            _S_DelegatesCntrler.FreeCurrentSeat();
        }

        private void menuMicSeat_Popup(object sender, EventArgs e)
        {
            bool SwitcherPhysicalState = false;
            bool SwitcherLogicalState = false;
            bool SeatFree_Enable = true;
            bool SeatSelect_Enable = true;
            bool AllDisable = false;

            _S_DelegatesCntrler.GetPopupMenuStates(ref SwitcherPhysicalState, ref SwitcherLogicalState, ref SeatFree_Enable, ref SeatSelect_Enable, ref AllDisable);
            if (AllDisable)
            {
                cntSwitherPhysical.Enabled = false;
                cntSwitherLogical.Enabled = false;
                cntSeatFree.Enabled = false;
                cntSeatSelect.Enabled = false;
            }

            cntSwitherPhysical.EditValue = SwitcherPhysicalState;
            cntSwitherLogical.EditValue = SwitcherLogicalState;
            cntSeatFree.Enabled = SeatFree_Enable;
            cntSeatSelect.Enabled = SeatSelect_Enable;

        }

        private void cntSeatSelect_ItemClick(object sender, ItemClickEventArgs e)
        {
            _S_DelegatesCntrler.Delegate_SetToSeat();
        }

        private void btnRegStart_Click(object sender, EventArgs e)
        {
            if (_S_DelegatesCntrler.RegStartMode == false)
                _S_DelegatesCntrler.Registration_PrepareToStart();
            else
                _S_DelegatesCntrler.Registration_Stop_Ask();

        }

        private void tmrRegStart_Tick(object sender, EventArgs e)
        {

            System.TimeSpan time = DateTime.Now - _startregtime;
            //          DateTime t = new DateTime(time.Ticks);
            DateTime quorumTime = new DateTime(1900, 12, 13, 0, _RegTime, 0);

            DateTime t = quorumTime.AddTicks(-time.Ticks);
            lblRegTime.Text = t.ToString("T");

            int QuorumSecs = quorumTime.Minute * 60 + quorumTime.Second;
            DateTime t2 = new DateTime(time.Ticks);
            int Secs = t2.Minute * 60 + t2.Second;

            int pos = (Secs * 100) / QuorumSecs;

            _S_DelegatesCntrler.OnRegProcess();

            progressBarRegistration.Position = pos;
            if (t.Minute == 0 && t.Second == 0)
            {
                _S_DelegatesCntrler.Registration_Stop();
            }

        }

        private void tmrRotation_Tick(object sender, EventArgs e)
        {
            _CurrentImageNum++;
            if (_CurrentImageNum > TimerImages.Images.Count - 1)
                _CurrentImageNum = 0;

            picRegTime.Image = TimerImages.Images[_CurrentImageNum];
        }


        private void btnRegCancel_Click(object sender, EventArgs e)
        {
            _S_DelegatesCntrler.Registration_Clear_Ask();
        }

        /*
                private void Mic_1_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
                {
                    PictureEdit pic = (PictureEdit)sender;

                    if (pic != null)
                    {

        //              Form f = pic.FindForm();

                        // Cancel the drag if the mouse moves off the form. The screenOffset
                        // takes into account any desktop bands that may be at the top or left
                        // side of the screen.
                        Point screenOffset = SystemInformation.WorkingArea.Location;

                        if (((Control.MousePosition.X - screenOffset.X) < this.DesktopBounds.Left) ||
                            ((Control.MousePosition.X - screenOffset.X) > this.DesktopBounds.Right) ||
                            ((Control.MousePosition.Y - screenOffset.Y) < this.DesktopBounds.Top) ||
                            ((Control.MousePosition.Y - screenOffset.Y) > this.DesktopBounds.Bottom))
                        {

                            e.Action = DragAction.Cancel;
                        }
                    }
                }
        */
    }
}