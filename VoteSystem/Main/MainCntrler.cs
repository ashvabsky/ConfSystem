﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public class MainCntrler
    {
        SessionObj _TheSession;
        MainForm _View;

        S_DelegateObj _theDelegate;

        NestedUnitOfWork _nuow;

        XPCollection _Delegates;

        // пул контроллеров
        SelQuestionsCntrler _selquestionsCntrler;
        ResQuestionsCntrler _resquestController;
        S_DelegatesCntrler _S_DelegatesCntrler;
        DelegatesCntrler _delegatesCntrler;
        QuestionsCntrler _questCntrler;
        VoteModeCntrler _votemodeCntrler;

        // пул view

        VoteModeForm _votemodeForm;
        QuestionsForm _questForm;
        DelegatesForm _delegatesForm;

        public enum Activities {None, Session, SelectDelegate, SelectQuestion, RegistrationInfo, QuestionResults, VotePrepare, VoteStart, RegPrepare, RegStart }

        Activities _CurrentActivity = new Activities();

        public MainCntrler(MainForm view)
        {
            _View = view;

            _votemodeCntrler        = new VoteModeCntrler(this);
            _resquestController     = new ResQuestionsCntrler(this);
            _selquestionsCntrler    = new SelQuestionsCntrler(this);
            _S_DelegatesCntrler     = new S_DelegatesCntrler(this);
            _delegatesCntrler       = new DelegatesCntrler(this);
            _questCntrler           = new QuestionsCntrler(this);


            _votemodeForm   = new VoteModeForm(_votemodeCntrler, this, view);
            _questForm      = new QuestionsForm(_questCntrler, this, view);
            _delegatesForm  = new DelegatesForm(_delegatesCntrler, this, view);

            _votemodeCntrler.AssignView((IVoteModeForm)_votemodeForm);
            _resquestController.AssignView(_View);
            _selquestionsCntrler.AssignView(_View);
            _S_DelegatesCntrler.AssignView(_View);
            _delegatesCntrler.AssignView((IDelegatesForm)_delegatesForm);
            _questCntrler.AssignView((IQuestionsForm)_questForm);

            _CurrentActivity = Activities.None;
        }

        public object GetSubController(string ControllerName)
        {
            switch (ControllerName)
            {
                case "QuorumPage Controller":
                    return _S_DelegatesCntrler;
                case "Delegate List Controller":
                    return _delegatesCntrler;
                case "Questions List Controller":
                    return _questCntrler;
                case "ResultsPage Controller":
                    return _resquestController;
                case "VoteQuestPage Controller":
                    return _selquestionsCntrler;
            }

            return null;
        }

        public Activities CurrentActivity
        {
            get { return _CurrentActivity; }
            set {_CurrentActivity = value;}
        }

        public void Init(ref XPCollection Delegates, ref XPCollection SessionQuestions)
        {
            DataModel.Init(SessionQuestions);

            _TheSession = DataModel.TheSession;
            _Delegates = Delegates;

            _View.Init1();
        }

        public void InitSessionPage()
        {
            _View.BindingSessionObj(_TheSession);
            _CurrentActivity = Activities.Session;

        }

        public void SaveSession()
        {
            _TheSession.Save();
        }

        public void InitDelegatePage()
        {
            _Delegates.Reload();

        }

        public void Save()
        {
            if (_TheSession == null)
                return;
            _TheSession.Save();
        }

        public void UpdatePropData(int pos)
        {
            _CurrentActivity = Activities.None;

            _theDelegate = null;

            if (pos < 0)
                return;


            if (pos >= _Delegates.Count)
                return;

            _theDelegate = _Delegates[pos] as S_DelegateObj;

            ShowRecProperties(_theDelegate);

            _CurrentActivity = Activities.SelectDelegate;

        }

        public void ShowRecProperties(S_DelegateObj theDelegate)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties["FirstName"] = theDelegate.idDelegate.FirstName;
            properties["SecondName"] = theDelegate.idDelegate.SecondName;
            properties["LastName"] = theDelegate.idDelegate.LastName;
            properties["PartyName"] = theDelegate.idDelegate.idParty.Name;
            properties["FractionName"] = theDelegate.idDelegate.idFraction.Name;
            properties["RegionName"] = theDelegate.idDelegate.idRegion.Name;
            properties["RegionNum"] = theDelegate.idDelegate.idRegion.Number;

            properties["MicNum"] = theDelegate.idSeat.MicNum.ToString();
            properties["RowNum"] = theDelegate.idSeat.RowNum.ToString();
            properties["SeatNum"] = theDelegate.idSeat.SeatNum.ToString();

            _View.SetProperties_Delegate(properties);
        }

        public void RemoveDelegate()
        {
            if (_theDelegate == null)
                return;

            _Delegates.Remove(_theDelegate);
            DataModel.UpdateSesDelegates();
        }

        public int AppendDelegate()
        {
            List<int> arr = new List<int>();
            List<int> arr_Max = new List<int>();
            foreach (S_DelegateObj o in DataModel.SesDelegates)
            {
                arr.Add(o.idDelegate.id);
                arr_Max.Add(o.id);
            }

            int idMax = arr_Max.Max();

            DelegatesCntrler d = (DelegatesCntrler)GetSubController("Delegate List Controller");
            DelegateObj Delegate = d.GetSelectedItem(arr);

            int new_index = 0;
            if (Delegate != null)
            {
                S_DelegateObj S_Delegate = new S_DelegateObj();

                S_Delegate.idDelegate = Delegate;
                S_Delegate.idSession = _TheSession;
                S_Delegate.idSeat = Delegate.idSeat;
                S_Delegate.IsRegistered = false;

                S_Delegate.id = idMax + 1;
                new_index = _Delegates.Add(S_Delegate);
                S_Delegate.Save();
                DataModel.UpdateSesDelegates();
            }

            return new_index;

        }

        public void AddController(SelQuestionsCntrler selquestionsCntrler)
        {
            _selquestionsCntrler = selquestionsCntrler;
        }

        public bool AddNewVoteQuestion(S_QuestionObj q)
        {
            int newindex = _selquestionsCntrler.AddNewVoteQuestion(q);
            if (newindex == 0)
                return false;
            _View.ShowSelQuestsPage();
            _selquestionsCntrler.SetCurrentPos(newindex);

            return true;
        }

        public void VoteModeRun()
        {
            if (_selquestionsCntrler.CurrentQuestion != null)
            {
                _votemodeCntrler.Init(_selquestionsCntrler.CurrentQuestion);
                _votemodeCntrler.InitForm();

                if (_selquestionsCntrler.CurrentQuestion.IsResult)
                    _resquestController.SetPosition(_selquestionsCntrler.CurrentQuestion);
                
                _View.ShowPage("itmQuestions");
            }
        }

        public void VoteDetailsRun()
        {
            S_QuestionObj q = _resquestController.CurrentQuestion;
            if (q != null)
            {
                _votemodeCntrler.Init(q);
                _votemodeCntrler.InitForm(); // ShowVoteModeForm();

                if (!q.IsResult)
                    _selquestionsCntrler.SetPosition(q);

                _View.ShowPage("itmResults");

            }
        }

        public void ShowMonitorPreview()
        {
            MonitorPreviewForm mpf = new MonitorPreviewForm();
            MonitorPreviewForm2 mpf2 = new MonitorPreviewForm2();

            if (_CurrentActivity == Activities.Session)
            {
                mpf.ShowSession(_TheSession);
            }
            else if (_CurrentActivity == Activities.SelectDelegate)
            {
                mpf.ShowDelegate(_theDelegate);
            }
            else if (_CurrentActivity == Activities.RegistrationInfo)
            {
                mpf2.ShowRegistration(null/*_RegistrationInfo*/);
            }
            else if (_CurrentActivity == Activities.SelectQuestion)
            {
                mpf.ShowQuestion(_selquestionsCntrler.CurrentQuestion);
            }
            else if (_CurrentActivity == Activities.QuestionResults)
            {
                VoteResultObj vro = new VoteResultObj();
                mpf.ShowQuestionResults(vro);
            }
            else if (_CurrentActivity == Activities.VotePrepare)
            {
                mpf.ShowPrepareToVote(_votemodeCntrler.CurrentQuestion);
            }
            else if (_CurrentActivity == Activities.VoteStart)
            {
                mpf.ShowVoteStart(_votemodeCntrler.CurrentQuestion);
            }
            else if (_CurrentActivity == Activities.RegPrepare)
            {
                mpf2.ShowRegPrepare();
            }
            else if (_CurrentActivity == Activities.RegStart)
            {
                mpf2.ShowRegStart();
            }

        }

        public void InitPage(string pageName)
        {
            if (pageName == "pgSession")
            {
                InitSessionPage();
            }
            else if (pageName == "pgDelegates")
            {
                InitDelegatePage();
            }
            else if (pageName == "pgSelQuestions")
            {
                _selquestionsCntrler.InitSelQuestionsPage();
            }
            else if (pageName == "pgQuorumGraphic")
            {
                _S_DelegatesCntrler.InitQuorumGraphicPage();
            }
            else if (pageName == "pgResQuestions")
            {
                _resquestController.InitResQuestionsPage();
            }
        }
    }
}
