﻿namespace VoteSystem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties multiEditorRowProperties5 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties();
            DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties multiEditorRowProperties6 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties();
            DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties multiEditorRowProperties7 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties();
            DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties multiEditorRowProperties8 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.barSubItemSession = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem17 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem18 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem19 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem6 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem24 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem25 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem26 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.RibbonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.barbtnPredsedSendMsg = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnMonitorMsg = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnExit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.cntSeatFree = new DevExpress.XtraBars.BarButtonItem();
            this.cntSeatSelect = new DevExpress.XtraBars.BarButtonItem();
            this.cntSwitherPhysical = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.cntSwitherLogical = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barbtnClearMonitor = new DevExpress.XtraBars.BarCheckItem();
            this.barbtnSplashToMonitor = new DevExpress.XtraBars.BarCheckItem();
            this.barbtnInfoToMonitor = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.Mic_1 = new DevExpress.XtraEditors.PictureEdit();
            this.menuMicSeat = new DevExpress.XtraBars.PopupMenu(this.components);
            this.pictureEdit59 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit60 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit61 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit62 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit63 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit64 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit65 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit66 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit67 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit68 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit69 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit70 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit71 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit72 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit73 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit74 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit43 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit44 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit45 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit46 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit47 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit48 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit49 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit50 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit51 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit52 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit53 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit54 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit55 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit56 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit57 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit58 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit27 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit19 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit28 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit20 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit29 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit21 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit30 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit22 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit31 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit23 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit32 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit24 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit33 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit25 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit34 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit26 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit35 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit36 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit37 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit38 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit39 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit40 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit41 = new DevExpress.XtraEditors.PictureEdit();
            this.Mic_2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit42 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit75 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit76 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit77 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit78 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit79 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit80 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit81 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit82 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit83 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit84 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit85 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit86 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit87 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit88 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit89 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit90 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit91 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit92 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit93 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit94 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit95 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit96 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit97 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit98 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit99 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit100 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit101 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit102 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit103 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit104 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit105 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit106 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit107 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit108 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit109 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit110 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit111 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit112 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit113 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit114 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit115 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit116 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit117 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit118 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit119 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit120 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit121 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit122 = new DevExpress.XtraEditors.PictureEdit();
            this.MicSeatMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.clientPanel = new DevExpress.XtraEditors.PanelControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.itmAgenda = new DevExpress.XtraNavBar.NavBarItem();
            this.itmDelegates = new DevExpress.XtraNavBar.NavBarItem();
            this.itmQuestions = new DevExpress.XtraNavBar.NavBarItem();
            this.itmRegistration = new DevExpress.XtraNavBar.NavBarItem();
            this.itmResults = new DevExpress.XtraNavBar.NavBarItem();
            this.itmReport = new DevExpress.XtraNavBar.NavBarItem();
            this.itmStatements = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem9 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem10 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem11 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem12 = new DevExpress.XtraNavBar.NavBarItem();
            this.NavBarImages = new DevExpress.Utils.ImageCollection(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.MainTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.pgSession = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtVoteTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl145 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.lbHistory = new DevExpress.XtraEditors.ListBoxControl();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtContent = new DevExpress.XtraEditors.MemoEdit();
            this.txtHall = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.chbRegAfterTime = new System.Windows.Forms.CheckBox();
            this.txtRegTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtQourum = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtStartDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtCaption = new DevExpress.XtraEditors.MemoEdit();
            this.pgDelegates = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridDelegates = new DevExpress.XtraGrid.GridControl();
            this.xpDelegates = new DevExpress.Xpo.XPCollection();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFraction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsRegistered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.navigatorDelegates = new DevExpress.XtraEditors.DataNavigator();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.listBoxControl1 = new DevExpress.XtraEditors.ListBoxControl();
            this.PropertiesControl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.catFIO = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowLastName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFirstName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSecondName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catRegion = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowRegionName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRegionNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catInfo = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowFraction = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPartyName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catSeat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMicNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRowNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSeatNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.pgSelQuestions = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btnQuestVoteMode = new DevExpress.XtraEditors.SimpleButton();
            this.navigatorQuestions = new DevExpress.XtraEditors.DataNavigator();
            this.xpQuestions = new DevExpress.Xpo.XPCollection();
            this.SelQuestGrid = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestCaption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grpSelQuestPropsIn = new DevExpress.XtraEditors.GroupControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnPropQuestApply = new DevExpress.XtraEditors.SimpleButton();
            this.btnPropQuestCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.PropertiesControlQuest = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox9 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox10 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox11 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox12 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox13 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox14 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox15 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox16 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repository_cmb_DecisionProcTypes = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repository_cmb_VoteQntyTypes = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repository_cedt_QuotaProcType_Present = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repository_cedt_QuotaProcType_Total = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repository_cmb_VoteKinds = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repository_cmb_votetypes = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repository_cmb_ReadNum = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.cat_sq_Characteristics = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_sq_Number = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_Caption = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_Description = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_CrDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_VoteType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_VoteKind = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_ReadNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_sq_Criteries = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_sq_DecisionProcValue = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRow();
            this.row_sq_DecisionProcType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_VoteQnty = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_QuotaProcentValue = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_QuotaProcentType_Present = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_QuotaProcentType_Total = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_sq_Notes = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_sq_Notes = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.pgQuorumGraphic = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.label92 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.labelControl97 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl98 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl99 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl100 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl101 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl102 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl103 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl104 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl105 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl106 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl107 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl108 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl109 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl110 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl111 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl113 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl114 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl117 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl118 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl119 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl120 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl121 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl122 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl123 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl124 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl125 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl126 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl127 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl128 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl129 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl130 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl131 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl132 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl133 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl134 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl135 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl136 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl137 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl138 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl139 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl140 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl141 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl142 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl143 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl144 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl81 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl82 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl83 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl87 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl88 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl89 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl90 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl91 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl92 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl93 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl94 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl95 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl96 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.PropertiesControl_QPage = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox7 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox8 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox17 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_q_LastName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_FirstName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_SecondName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_q_Region = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_q_RegionName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_RegionNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_q_Info = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_q_Fraction = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_PartyName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_q_Seat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_q_MicNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_RowNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_q_SeatNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.listBoxControl3 = new DevExpress.XtraEditors.ListBoxControl();
            this.label1 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.lblRegTime = new DevExpress.XtraEditors.LabelControl();
            this.lblQuorumFinished = new DevExpress.XtraEditors.LabelControl();
            this.btnRegCancel = new DevExpress.XtraEditors.SimpleButton();
            this.progressBarRegistration = new DevExpress.XtraEditors.ProgressBarControl();
            this.txtQuorumQnty = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtRegQnty = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtDelegateQnty = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.lblRegistration = new DevExpress.XtraEditors.LabelControl();
            this.picRegTime = new DevExpress.XtraEditors.PictureEdit();
            this.btnRegStart = new DevExpress.XtraEditors.SimpleButton();
            this.pgResQuestions = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.navigatorResQuestions = new DevExpress.XtraEditors.DataNavigator();
            this.xpResQuestions = new DevExpress.Xpo.XPCollection();
            this.btnQuestVoteDetail = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuestVoteCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuestNewRead = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grpResQuestProps = new DevExpress.XtraEditors.GroupControl();
            this.grpResQuestPropsIn = new DevExpress.XtraEditors.GroupControl();
            this.PropertiesControlQuest2 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox18 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox19 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox20 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox21 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox22 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox23 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox24 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox25 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox26 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox27 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox28 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox29 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.categoryRow2 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow4 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow5 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow6 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow7 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow3 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.multiEditorRow1 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRow();
            this.editorRow8 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow9 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow10 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow11 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow12 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.lblVoteDateTime = new System.Windows.Forms.Label();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.lblProcQnty = new System.Windows.Forms.Label();
            this.grpVoteResults = new System.Windows.Forms.GroupBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.lblProcAye = new System.Windows.Forms.Label();
            this.lblVoteAye = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.lblNonProcQnty = new System.Windows.Forms.Label();
            this.lblNonVoteQnty = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.lblProcAgainst = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.lblProcAbstain = new System.Windows.Forms.Label();
            this.lblVoteAgainst = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.lblVoteAbstain = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.domainUpDown5 = new System.Windows.Forms.DomainUpDown();
            this.label91 = new System.Windows.Forms.Label();
            this.lblDecision = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.lblVoteQnty = new System.Windows.Forms.Label();
            this.pgStatements = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.btnStatementMode = new DevExpress.XtraEditors.SimpleButton();
            this.navigatorStatements = new DevExpress.XtraEditors.DataNavigator();
            this.xpStatements = new DevExpress.Xpo.XPCollection();
            this.StatementGrid = new DevExpress.XtraGrid.GridControl();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.vGridControl1 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox30 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox31 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox32 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox33 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox34 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox35 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemDateEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox36 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox37 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox38 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox39 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox40 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox41 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox42 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.categoryRow4 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow13 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow14 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow15 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow16 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow17 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow19 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow5 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow20 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow18 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow21 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow7 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow22 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow23 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow24 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow6 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow25 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.dataSet1 = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataTable3 = new System.Data.DataTable();
            this.QuestNum = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.domainUpDown3 = new System.Windows.Forms.DomainUpDown();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.domainUpDown4 = new System.Windows.Forms.DomainUpDown();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.PropertyDataSet = new System.Data.DataSet();
            this.dataTable4 = new System.Data.DataTable();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.repositoryItemPictureEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.editorRow35 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow36 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow37 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow38 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow39 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow40 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow41 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow42 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow43 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow44 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow45 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow46 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow47 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.propertyGridControl4 = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.tmrRegStart = new System.Windows.Forms.Timer(this.components);
            this.TimerImages = new DevExpress.Utils.ImageCollection(this.components);
            this.tmrRotation = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuMicSeat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit59.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit60.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit61.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit62.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit63.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit64.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit65.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit66.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit67.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit68.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit69.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit70.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit71.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit72.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit73.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit74.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit49.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit50.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit55.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit56.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit57.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit58.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit75.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit76.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit77.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit78.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit79.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit80.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit81.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit82.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit83.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit84.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit85.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit86.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit87.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit88.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit89.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit90.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit91.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit92.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit93.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit94.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit95.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit96.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit97.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit98.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit99.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit100.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit101.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit102.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit103.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit104.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit105.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit106.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit107.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit108.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit109.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit110.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit111.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit112.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit113.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit114.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit115.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit116.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit117.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit118.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit119.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit120.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit121.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit122.Properties)).BeginInit();
            this.MicSeatMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientPanel)).BeginInit();
            this.clientPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainTabControl)).BeginInit();
            this.MainTabControl.SuspendLayout();
            this.pgSession.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHall.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQourum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption.Properties)).BeginInit();
            this.pgDelegates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            this.pgSelQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xpQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelQuestGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSelQuestPropsIn)).BeginInit();
            this.grpSelQuestPropsIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_DecisionProcTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_VoteQntyTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cedt_QuotaProcType_Present)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cedt_QuotaProcType_Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_VoteKinds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_votetypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_ReadNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).BeginInit();
            this.pgQuorumGraphic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl_QPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarRegistration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuorumQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRegTime.Properties)).BeginInit();
            this.pgResQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xpResQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpResQuestProps)).BeginInit();
            this.grpResQuestProps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpResQuestPropsIn)).BeginInit();
            this.grpResQuestPropsIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            this.grpVoteResults.SuspendLayout();
            this.pgStatements.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xpStatements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatementGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimerImages)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit3.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbon.ApplicationButtonKeyTip = "";
            this.ribbon.ApplicationIcon = ((System.Drawing.Bitmap)(resources.GetObject("ribbon.ApplicationIcon")));
            this.ribbon.Images = this.RibbonImages;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barbtnPredsedSendMsg,
            this.barbtnMonitorMsg,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barbtnExit,
            this.barButtonItem11,
            this.barButtonItem12,
            this.barButtonItem13,
            this.barButtonItem14,
            this.cntSeatFree,
            this.cntSeatSelect,
            this.cntSwitherPhysical,
            this.cntSwitherLogical,
            this.barStaticItem1,
            this.barbtnClearMonitor,
            this.barbtnSplashToMonitor,
            this.barbtnInfoToMonitor,
            this.barSubItem1,
            this.barButtonItem1,
            this.barSubItemSession,
            this.barButtonItem2,
            this.barSubItem2,
            this.barSubItem3,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem9,
            this.barButtonItem15,
            this.barButtonItem16,
            this.barSubItem4,
            this.barSubItem5,
            this.barSubItem6,
            this.barButtonItem17,
            this.barButtonItem18,
            this.barButtonItem19,
            this.barButtonItem20,
            this.barButtonItem21,
            this.barButtonItem22,
            this.barButtonItem23,
            this.barButtonItem24,
            this.barButtonItem25,
            this.barButtonItem26,
            this.barButtonItem7,
            this.barButtonItem10,
            this.barButtonItem8});
            this.ribbon.LargeImages = this.RibbonImages;
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 60;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2});
            this.ribbon.SelectedPage = this.ribbonPage1;
            this.ribbon.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1242, 95);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ItemLinks.Add(this.barSubItem1);
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            this.ribbon.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.BottomPaneControlContainer = null;
            this.applicationMenu1.ItemLinks.Add(this.barSubItemSession);
            this.applicationMenu1.ItemLinks.Add(this.barSubItem2);
            this.applicationMenu1.ItemLinks.Add(this.barSubItem3);
            this.applicationMenu1.ItemLinks.Add(this.barSubItem4);
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbon;
            this.applicationMenu1.RightPaneControlContainer = null;
            this.applicationMenu1.RightPaneWidth = 240;
            // 
            // barSubItemSession
            // 
            this.barSubItemSession.Caption = "Сессия";
            this.barSubItemSession.Id = 35;
            this.barSubItemSession.ImageIndex = 5;
            this.barSubItemSession.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem6)});
            this.barSubItemSession.Name = "barSubItemSession";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Новая...";
            this.barButtonItem2.Id = 36;
            this.barButtonItem2.ImageIndex = 5;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Загрузить...";
            this.barButtonItem3.Id = 39;
            this.barButtonItem3.ImageIndex = 5;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "Эксопрт в файл";
            this.barSubItem5.Id = 45;
            this.barSubItem5.ImageIndex = 8;
            this.barSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem17),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem18),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem19)});
            this.barSubItem5.Name = "barSubItem5";
            // 
            // barButtonItem17
            // 
            this.barButtonItem17.Caption = "Список делегатов";
            this.barButtonItem17.Id = 47;
            this.barButtonItem17.Name = "barButtonItem17";
            // 
            // barButtonItem18
            // 
            this.barButtonItem18.Caption = "Список вопросов";
            this.barButtonItem18.Id = 48;
            this.barButtonItem18.Name = "barButtonItem18";
            // 
            // barButtonItem19
            // 
            this.barButtonItem19.Caption = "Итоги голосований";
            this.barButtonItem19.Id = 49;
            this.barButtonItem19.Name = "barButtonItem19";
            // 
            // barSubItem6
            // 
            this.barSubItem6.Caption = "Импорт из файла";
            this.barSubItem6.Id = 46;
            this.barSubItem6.ImageIndex = 8;
            this.barSubItem6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem20),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem22)});
            this.barSubItem6.Name = "barSubItem6";
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Caption = "Список делегатов";
            this.barButtonItem20.Id = 50;
            this.barButtonItem20.Name = "barButtonItem20";
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Caption = "Список вопросов";
            this.barButtonItem21.Id = 51;
            this.barButtonItem21.Name = "barButtonItem21";
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Caption = "Итоги голосований";
            this.barButtonItem22.Id = 52;
            this.barButtonItem22.Name = "barButtonItem22";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "База данных";
            this.barSubItem2.Id = 37;
            this.barSubItem2.ImageIndex = 12;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem15)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Создать резервную копию";
            this.barButtonItem4.Id = 40;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Загрузить резервную копию";
            this.barButtonItem9.Id = 41;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "Очистить";
            this.barButtonItem15.Id = 42;
            this.barButtonItem15.Name = "barButtonItem15";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Настройки";
            this.barSubItem3.Id = 38;
            this.barSubItem3.ImageIndex = 6;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem23),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem24),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem25),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem26)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Caption = "Сетевые настройки";
            this.barButtonItem23.Id = 53;
            this.barButtonItem23.Name = "barButtonItem23";
            // 
            // barButtonItem24
            // 
            this.barButtonItem24.Caption = "Система голосования";
            this.barButtonItem24.Id = 54;
            this.barButtonItem24.Name = "barButtonItem24";
            // 
            // barButtonItem25
            // 
            this.barButtonItem25.Caption = "Общие";
            this.barButtonItem25.Id = 55;
            this.barButtonItem25.Name = "barButtonItem25";
            // 
            // barButtonItem26
            // 
            this.barButtonItem26.Caption = "Тест системы";
            this.barButtonItem26.Id = 56;
            this.barButtonItem26.Name = "barButtonItem26";
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "Отчеты";
            this.barSubItem4.Id = 44;
            this.barSubItem4.ImageIndex = 8;
            this.barSubItem4.Name = "barSubItem4";
            // 
            // RibbonImages
            // 
            this.RibbonImages.ImageSize = new System.Drawing.Size(48, 48);
            this.RibbonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("RibbonImages.ImageStream")));
            // 
            // barbtnPredsedSendMsg
            // 
            this.barbtnPredsedSendMsg.Caption = "Сообщение";
            this.barbtnPredsedSendMsg.Id = 0;
            this.barbtnPredsedSendMsg.LargeImageIndex = 0;
            this.barbtnPredsedSendMsg.LargeWidth = 90;
            this.barbtnPredsedSendMsg.Name = "barbtnPredsedSendMsg";
            this.barbtnPredsedSendMsg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnPredsedSendMsg_ItemClick);
            // 
            // barbtnMonitorMsg
            // 
            this.barbtnMonitorMsg.Caption = "Сообщение на мониторы";
            this.barbtnMonitorMsg.Id = 1;
            this.barbtnMonitorMsg.LargeImageIndex = 1;
            this.barbtnMonitorMsg.LargeWidth = 90;
            this.barbtnMonitorMsg.Name = "barbtnMonitorMsg";
            this.barbtnMonitorMsg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnMonitorMsg_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Депутаты";
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.LargeImageIndex = 3;
            this.barButtonItem5.LargeWidth = 100;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Вопросы";
            this.barButtonItem6.Id = 6;
            this.barButtonItem6.LargeImageIndex = 4;
            this.barButtonItem6.LargeWidth = 100;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barbtnExit
            // 
            this.barbtnExit.Caption = "Выход";
            this.barbtnExit.Id = 9;
            this.barbtnExit.LargeImageIndex = 7;
            this.barbtnExit.LargeWidth = 98;
            this.barbtnExit.Name = "barbtnExit";
            this.barbtnExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnExit_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "По депутатам";
            this.barButtonItem11.Id = 12;
            this.barButtonItem11.LargeWidth = 90;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "Статистика";
            this.barButtonItem12.Id = 13;
            this.barButtonItem12.LargeWidth = 90;
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "по депутатам";
            this.barButtonItem13.Id = 18;
            this.barButtonItem13.LargeImageIndex = 13;
            this.barButtonItem13.LargeWidth = 90;
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "статистика";
            this.barButtonItem14.Id = 19;
            this.barButtonItem14.LargeImageIndex = 14;
            this.barButtonItem14.LargeWidth = 90;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // cntSeatFree
            // 
            this.cntSeatFree.Caption = "Освободить место";
            this.cntSeatFree.Id = 24;
            this.cntSeatFree.Name = "cntSeatFree";
            this.cntSeatFree.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.cntSeatFree_ItemPress);
            // 
            // cntSeatSelect
            // 
            this.cntSeatSelect.Caption = "Выбрать депутата на место...";
            this.cntSeatSelect.Id = 25;
            this.cntSeatSelect.Name = "cntSeatSelect";
            this.cntSeatSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cntSeatSelect_ItemClick);
            // 
            // cntSwitherPhysical
            // 
            this.cntSwitherPhysical.Caption = "Вкл\\Выкл физически";
            this.cntSwitherPhysical.Edit = this.repositoryItemCheckEdit1;
            this.cntSwitherPhysical.EditValue = true;
            this.cntSwitherPhysical.Id = 26;
            this.cntSwitherPhysical.Name = "cntSwitherPhysical";
            this.cntSwitherPhysical.EditValueChanged += new System.EventHandler(this.cntSwitherPhysical_EditValueChanged);
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style16;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // cntSwitherLogical
            // 
            this.cntSwitherLogical.Caption = "Вкл\\Выкл логически";
            this.cntSwitherLogical.Edit = this.repositoryItemCheckEdit2;
            this.cntSwitherLogical.EditValue = true;
            this.cntSwitherLogical.Id = 28;
            this.cntSwitherLogical.Name = "cntSwitherLogical";
            this.cntSwitherLogical.EditValueChanged += new System.EventHandler(this.cntSwitherLogical_EditValueChanged);
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style15;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "___________________________";
            this.barStaticItem1.Id = 29;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barbtnClearMonitor
            // 
            this.barbtnClearMonitor.Caption = "Очистить экран";
            this.barbtnClearMonitor.Id = 30;
            this.barbtnClearMonitor.LargeImageIndex = 11;
            this.barbtnClearMonitor.LargeWidth = 80;
            this.barbtnClearMonitor.Name = "barbtnClearMonitor";
            this.barbtnClearMonitor.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnInfoToMonitor_CheckedChanged);
            // 
            // barbtnSplashToMonitor
            // 
            this.barbtnSplashToMonitor.Caption = "Вывод заставки";
            this.barbtnSplashToMonitor.Id = 31;
            this.barbtnSplashToMonitor.LargeImageIndex = 9;
            this.barbtnSplashToMonitor.LargeWidth = 80;
            this.barbtnSplashToMonitor.Name = "barbtnSplashToMonitor";
            this.barbtnSplashToMonitor.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnSplashToMonitor_CheckedChanged);
            // 
            // barbtnInfoToMonitor
            // 
            this.barbtnInfoToMonitor.Caption = "Вывод информации";
            this.barbtnInfoToMonitor.Id = 32;
            this.barbtnInfoToMonitor.LargeImageIndex = 10;
            this.barbtnInfoToMonitor.LargeWidth = 100;
            this.barbtnInfoToMonitor.Name = "barbtnInfoToMonitor";
            this.barbtnInfoToMonitor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnInfoToMonitor_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "barSubItem1";
            this.barSubItem1.Id = 33;
            this.barSubItem1.MenuCaption = "hjh";
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.ShowMenuCaption = true;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 34;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "barButtonItem16";
            this.barButtonItem16.Id = 43;
            this.barButtonItem16.Name = "barButtonItem16";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Печать";
            this.barButtonItem7.Id = 57;
            this.barButtonItem7.LargeImageIndex = 15;
            this.barButtonItem7.LargeWidth = 100;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Протокол";
            this.barButtonItem10.Id = 58;
            this.barButtonItem10.LargeImageIndex = 8;
            this.barButtonItem10.LargeWidth = 100;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Просмотр мониторов";
            this.barButtonItem8.Id = 59;
            this.barButtonItem8.LargeImageIndex = 10;
            this.barButtonItem8.LargeWidth = 80;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup7,
            this.ribbonPageGroup6});
            this.ribbonPage1.KeyTip = "";
            this.ribbonPage1.Name = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barbtnPredsedSendMsg);
            this.ribbonPageGroup1.KeyTip = "";
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Председатель";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barbtnInfoToMonitor);
            this.ribbonPageGroup2.ItemLinks.Add(this.barbtnClearMonitor);
            this.ribbonPageGroup2.ItemLinks.Add(this.barbtnSplashToMonitor);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem8);
            this.ribbonPageGroup2.ItemLinks.Add(this.barbtnMonitorMsg);
            this.ribbonPageGroup2.KeyTip = "";
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Мониторы";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem5);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup3.KeyTip = "";
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Базы данных";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.barButtonItem13);
            this.ribbonPageGroup7.ItemLinks.Add(this.barButtonItem14);
            this.ribbonPageGroup7.KeyTip = "";
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Отчеты";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem10);
            this.ribbonPageGroup6.ItemLinks.Add(this.barbtnExit);
            this.ribbonPageGroup6.KeyTip = "";
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Сервис";
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 875);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1242, 24);
            // 
            // Mic_1
            // 
            this.Mic_1.AllowDrop = true;
            this.Mic_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mic_1.EditValue = global::VoteSystem.Properties.Resources.User;
            this.Mic_1.Location = new System.Drawing.Point(109, 35);
            this.Mic_1.Name = "Mic_1";
            this.ribbon.SetPopupContextMenu(this.Mic_1, this.menuMicSeat);
            this.Mic_1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Mic_1.Properties.Appearance.Options.UseBackColor = true;
            this.Mic_1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Mic_1.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.Mic_1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.Mic_1.Properties.UseParentBackground = true;
            this.Mic_1.Size = new System.Drawing.Size(28, 32);
            this.Mic_1.TabIndex = 47;
            this.Mic_1.Tag = "Mic001";
            this.Mic_1.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.Mic_1.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.Mic_1.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.Mic_1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.Mic_1.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.Mic_1.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // menuMicSeat
            // 
            this.menuMicSeat.ItemLinks.Add(this.cntSwitherPhysical);
            this.menuMicSeat.ItemLinks.Add(this.cntSwitherLogical);
            this.menuMicSeat.ItemLinks.Add(this.barStaticItem1);
            this.menuMicSeat.ItemLinks.Add(this.cntSeatFree);
            this.menuMicSeat.ItemLinks.Add(this.cntSeatSelect);
            this.menuMicSeat.Name = "menuMicSeat";
            this.menuMicSeat.Ribbon = this.ribbon;
            this.menuMicSeat.Popup += new System.EventHandler(this.menuMicSeat_Popup);
            // 
            // pictureEdit59
            // 
            this.pictureEdit59.AllowDrop = true;
            this.pictureEdit59.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit59.Location = new System.Drawing.Point(714, 250);
            this.pictureEdit59.Name = "pictureEdit59";
            this.ribbon.SetPopupContextMenu(this.pictureEdit59, this.menuMicSeat);
            this.pictureEdit59.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit59.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit59.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit59.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit59.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit59.Properties.UseParentBackground = true;
            this.pictureEdit59.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit59.TabIndex = 173;
            this.pictureEdit59.Tag = "Mic064";
            this.pictureEdit59.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit59.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit59.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit59.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit59.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit59.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit60
            // 
            this.pictureEdit60.AllowDrop = true;
            this.pictureEdit60.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit60.Location = new System.Drawing.Point(675, 250);
            this.pictureEdit60.Name = "pictureEdit60";
            this.ribbon.SetPopupContextMenu(this.pictureEdit60, this.menuMicSeat);
            this.pictureEdit60.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit60.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit60.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit60.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit60.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit60.Properties.UseParentBackground = true;
            this.pictureEdit60.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit60.TabIndex = 171;
            this.pictureEdit60.Tag = "Mic063";
            this.pictureEdit60.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit60.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit60.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit60.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit60.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit60.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit61
            // 
            this.pictureEdit61.AllowDrop = true;
            this.pictureEdit61.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit61.Location = new System.Drawing.Point(636, 250);
            this.pictureEdit61.Name = "pictureEdit61";
            this.ribbon.SetPopupContextMenu(this.pictureEdit61, this.menuMicSeat);
            this.pictureEdit61.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit61.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit61.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit61.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit61.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit61.Properties.UseParentBackground = true;
            this.pictureEdit61.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit61.TabIndex = 169;
            this.pictureEdit61.Tag = "Mic062";
            this.pictureEdit61.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit61.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit61.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit61.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit61.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit61.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit62
            // 
            this.pictureEdit62.AllowDrop = true;
            this.pictureEdit62.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit62.Location = new System.Drawing.Point(597, 250);
            this.pictureEdit62.Name = "pictureEdit62";
            this.ribbon.SetPopupContextMenu(this.pictureEdit62, this.menuMicSeat);
            this.pictureEdit62.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit62.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit62.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit62.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit62.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit62.Properties.UseParentBackground = true;
            this.pictureEdit62.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit62.TabIndex = 167;
            this.pictureEdit62.Tag = "Mic061";
            this.pictureEdit62.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit62.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit62.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit62.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit62.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit62.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit63
            // 
            this.pictureEdit63.AllowDrop = true;
            this.pictureEdit63.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit63.Location = new System.Drawing.Point(558, 250);
            this.pictureEdit63.Name = "pictureEdit63";
            this.ribbon.SetPopupContextMenu(this.pictureEdit63, this.menuMicSeat);
            this.pictureEdit63.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit63.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit63.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit63.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit63.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit63.Properties.UseParentBackground = true;
            this.pictureEdit63.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit63.TabIndex = 165;
            this.pictureEdit63.Tag = "Mic060";
            this.pictureEdit63.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit63.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit63.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit63.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit63.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit63.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit64
            // 
            this.pictureEdit64.AllowDrop = true;
            this.pictureEdit64.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit64.Location = new System.Drawing.Point(519, 250);
            this.pictureEdit64.Name = "pictureEdit64";
            this.ribbon.SetPopupContextMenu(this.pictureEdit64, this.menuMicSeat);
            this.pictureEdit64.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit64.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit64.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit64.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit64.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit64.Properties.UseParentBackground = true;
            this.pictureEdit64.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit64.TabIndex = 163;
            this.pictureEdit64.Tag = "Mic059";
            this.pictureEdit64.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit64.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit64.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit64.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit64.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit64.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit65
            // 
            this.pictureEdit65.AllowDrop = true;
            this.pictureEdit65.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit65.Location = new System.Drawing.Point(480, 250);
            this.pictureEdit65.Name = "pictureEdit65";
            this.ribbon.SetPopupContextMenu(this.pictureEdit65, this.menuMicSeat);
            this.pictureEdit65.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit65.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit65.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit65.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit65.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit65.Properties.UseParentBackground = true;
            this.pictureEdit65.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit65.TabIndex = 161;
            this.pictureEdit65.Tag = "Mic058";
            this.pictureEdit65.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit65.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit65.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit65.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit65.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit65.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit66
            // 
            this.pictureEdit66.AllowDrop = true;
            this.pictureEdit66.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit66.Location = new System.Drawing.Point(441, 250);
            this.pictureEdit66.Name = "pictureEdit66";
            this.ribbon.SetPopupContextMenu(this.pictureEdit66, this.menuMicSeat);
            this.pictureEdit66.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit66.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit66.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit66.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit66.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit66.Properties.UseParentBackground = true;
            this.pictureEdit66.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit66.TabIndex = 159;
            this.pictureEdit66.Tag = "Mic057";
            this.pictureEdit66.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit66.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit66.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit66.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit66.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit66.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit67
            // 
            this.pictureEdit67.AllowDrop = true;
            this.pictureEdit67.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit67.Location = new System.Drawing.Point(382, 250);
            this.pictureEdit67.Name = "pictureEdit67";
            this.ribbon.SetPopupContextMenu(this.pictureEdit67, this.menuMicSeat);
            this.pictureEdit67.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit67.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit67.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit67.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit67.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit67.Properties.UseParentBackground = true;
            this.pictureEdit67.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit67.TabIndex = 157;
            this.pictureEdit67.Tag = "Mic056";
            this.pictureEdit67.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit67.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit67.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit67.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit67.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit67.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit68
            // 
            this.pictureEdit68.AllowDrop = true;
            this.pictureEdit68.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit68.Location = new System.Drawing.Point(343, 250);
            this.pictureEdit68.Name = "pictureEdit68";
            this.ribbon.SetPopupContextMenu(this.pictureEdit68, this.menuMicSeat);
            this.pictureEdit68.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit68.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit68.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit68.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit68.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit68.Properties.UseParentBackground = true;
            this.pictureEdit68.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit68.TabIndex = 155;
            this.pictureEdit68.Tag = "Mic055";
            this.pictureEdit68.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit68.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit68.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit68.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit68.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit68.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit69
            // 
            this.pictureEdit69.AllowDrop = true;
            this.pictureEdit69.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit69.Location = new System.Drawing.Point(304, 250);
            this.pictureEdit69.Name = "pictureEdit69";
            this.ribbon.SetPopupContextMenu(this.pictureEdit69, this.menuMicSeat);
            this.pictureEdit69.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit69.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit69.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit69.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit69.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit69.Properties.UseParentBackground = true;
            this.pictureEdit69.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit69.TabIndex = 153;
            this.pictureEdit69.Tag = "Mic054";
            this.pictureEdit69.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit69.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit69.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit69.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit69.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit69.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit70
            // 
            this.pictureEdit70.AllowDrop = true;
            this.pictureEdit70.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit70.Location = new System.Drawing.Point(265, 250);
            this.pictureEdit70.Name = "pictureEdit70";
            this.ribbon.SetPopupContextMenu(this.pictureEdit70, this.menuMicSeat);
            this.pictureEdit70.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit70.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit70.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit70.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit70.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit70.Properties.UseParentBackground = true;
            this.pictureEdit70.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit70.TabIndex = 151;
            this.pictureEdit70.Tag = "Mic053";
            this.pictureEdit70.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit70.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit70.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit70.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit70.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit70.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit71
            // 
            this.pictureEdit71.AllowDrop = true;
            this.pictureEdit71.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit71.Location = new System.Drawing.Point(226, 250);
            this.pictureEdit71.Name = "pictureEdit71";
            this.ribbon.SetPopupContextMenu(this.pictureEdit71, this.menuMicSeat);
            this.pictureEdit71.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit71.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit71.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit71.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit71.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit71.Properties.UseParentBackground = true;
            this.pictureEdit71.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit71.TabIndex = 149;
            this.pictureEdit71.Tag = "Mic052";
            this.pictureEdit71.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit71.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit71.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit71.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit71.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit71.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit72
            // 
            this.pictureEdit72.AllowDrop = true;
            this.pictureEdit72.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit72.Location = new System.Drawing.Point(187, 250);
            this.pictureEdit72.Name = "pictureEdit72";
            this.ribbon.SetPopupContextMenu(this.pictureEdit72, this.menuMicSeat);
            this.pictureEdit72.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit72.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit72.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit72.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit72.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit72.Properties.UseParentBackground = true;
            this.pictureEdit72.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit72.TabIndex = 147;
            this.pictureEdit72.Tag = "Mic051";
            this.pictureEdit72.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit72.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit72.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit72.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit72.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit72.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit73
            // 
            this.pictureEdit73.AllowDrop = true;
            this.pictureEdit73.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit73.Location = new System.Drawing.Point(148, 250);
            this.pictureEdit73.Name = "pictureEdit73";
            this.ribbon.SetPopupContextMenu(this.pictureEdit73, this.menuMicSeat);
            this.pictureEdit73.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit73.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit73.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit73.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit73.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit73.Properties.UseParentBackground = true;
            this.pictureEdit73.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit73.TabIndex = 145;
            this.pictureEdit73.Tag = "Mic050";
            this.pictureEdit73.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit73.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit73.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit73.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit73.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit73.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit74
            // 
            this.pictureEdit74.AllowDrop = true;
            this.pictureEdit74.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit74.Location = new System.Drawing.Point(109, 250);
            this.pictureEdit74.Name = "pictureEdit74";
            this.ribbon.SetPopupContextMenu(this.pictureEdit74, this.menuMicSeat);
            this.pictureEdit74.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit74.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit74.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit74.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit74.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit74.Properties.UseParentBackground = true;
            this.pictureEdit74.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit74.TabIndex = 143;
            this.pictureEdit74.Tag = "Mic049";
            this.pictureEdit74.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit74.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit74.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit74.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit74.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit74.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit43
            // 
            this.pictureEdit43.AllowDrop = true;
            this.pictureEdit43.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit43.Location = new System.Drawing.Point(714, 186);
            this.pictureEdit43.Name = "pictureEdit43";
            this.ribbon.SetPopupContextMenu(this.pictureEdit43, this.menuMicSeat);
            this.pictureEdit43.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit43.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit43.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit43.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit43.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit43.Properties.UseParentBackground = true;
            this.pictureEdit43.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit43.TabIndex = 141;
            this.pictureEdit43.Tag = "Mic048";
            this.pictureEdit43.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit43.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit43.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit43.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit43.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit43.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit44
            // 
            this.pictureEdit44.AllowDrop = true;
            this.pictureEdit44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit44.Location = new System.Drawing.Point(675, 186);
            this.pictureEdit44.Name = "pictureEdit44";
            this.ribbon.SetPopupContextMenu(this.pictureEdit44, this.menuMicSeat);
            this.pictureEdit44.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit44.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit44.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit44.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit44.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit44.Properties.UseParentBackground = true;
            this.pictureEdit44.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit44.TabIndex = 139;
            this.pictureEdit44.Tag = "Mic047";
            this.pictureEdit44.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit44.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit44.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit44.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit44.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit44.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit45
            // 
            this.pictureEdit45.AllowDrop = true;
            this.pictureEdit45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit45.Location = new System.Drawing.Point(636, 186);
            this.pictureEdit45.Name = "pictureEdit45";
            this.ribbon.SetPopupContextMenu(this.pictureEdit45, this.menuMicSeat);
            this.pictureEdit45.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit45.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit45.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit45.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit45.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit45.Properties.UseParentBackground = true;
            this.pictureEdit45.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit45.TabIndex = 137;
            this.pictureEdit45.Tag = "Mic046";
            this.pictureEdit45.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit45.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit45.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit45.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit45.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit45.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit46
            // 
            this.pictureEdit46.AllowDrop = true;
            this.pictureEdit46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit46.Location = new System.Drawing.Point(597, 186);
            this.pictureEdit46.Name = "pictureEdit46";
            this.ribbon.SetPopupContextMenu(this.pictureEdit46, this.menuMicSeat);
            this.pictureEdit46.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit46.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit46.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit46.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit46.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit46.Properties.UseParentBackground = true;
            this.pictureEdit46.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit46.TabIndex = 135;
            this.pictureEdit46.Tag = "Mic045";
            this.pictureEdit46.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit46.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit46.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit46.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit46.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit46.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit47
            // 
            this.pictureEdit47.AllowDrop = true;
            this.pictureEdit47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit47.Location = new System.Drawing.Point(558, 186);
            this.pictureEdit47.Name = "pictureEdit47";
            this.ribbon.SetPopupContextMenu(this.pictureEdit47, this.menuMicSeat);
            this.pictureEdit47.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit47.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit47.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit47.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit47.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit47.Properties.UseParentBackground = true;
            this.pictureEdit47.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit47.TabIndex = 133;
            this.pictureEdit47.Tag = "Mic044";
            this.pictureEdit47.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit47.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit47.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit47.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit47.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit47.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit48
            // 
            this.pictureEdit48.AllowDrop = true;
            this.pictureEdit48.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit48.Location = new System.Drawing.Point(519, 186);
            this.pictureEdit48.Name = "pictureEdit48";
            this.ribbon.SetPopupContextMenu(this.pictureEdit48, this.menuMicSeat);
            this.pictureEdit48.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit48.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit48.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit48.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit48.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit48.Properties.UseParentBackground = true;
            this.pictureEdit48.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit48.TabIndex = 131;
            this.pictureEdit48.Tag = "Mic043";
            this.pictureEdit48.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit48.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit48.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit48.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit48.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit48.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit49
            // 
            this.pictureEdit49.AllowDrop = true;
            this.pictureEdit49.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit49.Location = new System.Drawing.Point(480, 186);
            this.pictureEdit49.Name = "pictureEdit49";
            this.ribbon.SetPopupContextMenu(this.pictureEdit49, this.menuMicSeat);
            this.pictureEdit49.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit49.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit49.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit49.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit49.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit49.Properties.UseParentBackground = true;
            this.pictureEdit49.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit49.TabIndex = 129;
            this.pictureEdit49.Tag = "Mic042";
            this.pictureEdit49.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit49.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit49.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit49.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit49.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit49.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit50
            // 
            this.pictureEdit50.AllowDrop = true;
            this.pictureEdit50.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit50.Location = new System.Drawing.Point(441, 186);
            this.pictureEdit50.Name = "pictureEdit50";
            this.ribbon.SetPopupContextMenu(this.pictureEdit50, this.menuMicSeat);
            this.pictureEdit50.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit50.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit50.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit50.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit50.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit50.Properties.UseParentBackground = true;
            this.pictureEdit50.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit50.TabIndex = 127;
            this.pictureEdit50.Tag = "Mic041";
            this.pictureEdit50.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit50.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit50.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit50.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit50.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit50.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit51
            // 
            this.pictureEdit51.AllowDrop = true;
            this.pictureEdit51.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit51.Location = new System.Drawing.Point(382, 186);
            this.pictureEdit51.Name = "pictureEdit51";
            this.ribbon.SetPopupContextMenu(this.pictureEdit51, this.menuMicSeat);
            this.pictureEdit51.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit51.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit51.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit51.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit51.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit51.Properties.UseParentBackground = true;
            this.pictureEdit51.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit51.TabIndex = 125;
            this.pictureEdit51.Tag = "Mic040";
            this.pictureEdit51.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit51.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit51.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit51.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit51.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit51.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit52
            // 
            this.pictureEdit52.AllowDrop = true;
            this.pictureEdit52.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit52.Location = new System.Drawing.Point(343, 186);
            this.pictureEdit52.Name = "pictureEdit52";
            this.ribbon.SetPopupContextMenu(this.pictureEdit52, this.menuMicSeat);
            this.pictureEdit52.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit52.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit52.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit52.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit52.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit52.Properties.UseParentBackground = true;
            this.pictureEdit52.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit52.TabIndex = 123;
            this.pictureEdit52.Tag = "Mic039";
            this.pictureEdit52.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit52.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit52.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit52.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit52.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit52.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit53
            // 
            this.pictureEdit53.AllowDrop = true;
            this.pictureEdit53.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit53.Location = new System.Drawing.Point(304, 186);
            this.pictureEdit53.Name = "pictureEdit53";
            this.ribbon.SetPopupContextMenu(this.pictureEdit53, this.menuMicSeat);
            this.pictureEdit53.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit53.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit53.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit53.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit53.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit53.Properties.UseParentBackground = true;
            this.pictureEdit53.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit53.TabIndex = 121;
            this.pictureEdit53.Tag = "Mic038";
            this.pictureEdit53.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit53.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit53.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit53.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit53.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit53.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit54
            // 
            this.pictureEdit54.AllowDrop = true;
            this.pictureEdit54.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit54.Location = new System.Drawing.Point(265, 186);
            this.pictureEdit54.Name = "pictureEdit54";
            this.ribbon.SetPopupContextMenu(this.pictureEdit54, this.menuMicSeat);
            this.pictureEdit54.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit54.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit54.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit54.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit54.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit54.Properties.UseParentBackground = true;
            this.pictureEdit54.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit54.TabIndex = 119;
            this.pictureEdit54.Tag = "Mic037";
            this.pictureEdit54.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit54.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit54.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit54.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit54.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit54.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit55
            // 
            this.pictureEdit55.AllowDrop = true;
            this.pictureEdit55.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit55.Location = new System.Drawing.Point(226, 186);
            this.pictureEdit55.Name = "pictureEdit55";
            this.ribbon.SetPopupContextMenu(this.pictureEdit55, this.menuMicSeat);
            this.pictureEdit55.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit55.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit55.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit55.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit55.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit55.Properties.UseParentBackground = true;
            this.pictureEdit55.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit55.TabIndex = 117;
            this.pictureEdit55.Tag = "Mic036";
            this.pictureEdit55.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit55.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit55.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit55.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit55.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit55.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit56
            // 
            this.pictureEdit56.AllowDrop = true;
            this.pictureEdit56.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit56.Location = new System.Drawing.Point(187, 186);
            this.pictureEdit56.Name = "pictureEdit56";
            this.ribbon.SetPopupContextMenu(this.pictureEdit56, this.menuMicSeat);
            this.pictureEdit56.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit56.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit56.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit56.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit56.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit56.Properties.UseParentBackground = true;
            this.pictureEdit56.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit56.TabIndex = 115;
            this.pictureEdit56.Tag = "Mic035";
            this.pictureEdit56.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit56.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit56.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit56.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit56.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit56.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit57
            // 
            this.pictureEdit57.AllowDrop = true;
            this.pictureEdit57.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit57.Location = new System.Drawing.Point(148, 186);
            this.pictureEdit57.Name = "pictureEdit57";
            this.ribbon.SetPopupContextMenu(this.pictureEdit57, this.menuMicSeat);
            this.pictureEdit57.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit57.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit57.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit57.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit57.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit57.Properties.UseParentBackground = true;
            this.pictureEdit57.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit57.TabIndex = 113;
            this.pictureEdit57.Tag = "Mic034";
            this.pictureEdit57.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit57.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit57.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit57.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit57.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit57.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit58
            // 
            this.pictureEdit58.AllowDrop = true;
            this.pictureEdit58.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit58.Location = new System.Drawing.Point(109, 186);
            this.pictureEdit58.Name = "pictureEdit58";
            this.ribbon.SetPopupContextMenu(this.pictureEdit58, this.menuMicSeat);
            this.pictureEdit58.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit58.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit58.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit58.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit58.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit58.Properties.UseParentBackground = true;
            this.pictureEdit58.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit58.TabIndex = 111;
            this.pictureEdit58.Tag = "Mic033";
            this.pictureEdit58.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit58.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit58.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit58.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit58.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit58.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit27
            // 
            this.pictureEdit27.AllowDrop = true;
            this.pictureEdit27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit27.Location = new System.Drawing.Point(714, 127);
            this.pictureEdit27.Name = "pictureEdit27";
            this.ribbon.SetPopupContextMenu(this.pictureEdit27, this.menuMicSeat);
            this.pictureEdit27.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit27.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit27.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit27.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit27.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit27.Properties.UseParentBackground = true;
            this.pictureEdit27.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit27.TabIndex = 109;
            this.pictureEdit27.Tag = "Mic032";
            this.pictureEdit27.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit27.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit27.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit27.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit27.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit27.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit19
            // 
            this.pictureEdit19.AllowDrop = true;
            this.pictureEdit19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit19.Location = new System.Drawing.Point(714, 35);
            this.pictureEdit19.Name = "pictureEdit19";
            this.ribbon.SetPopupContextMenu(this.pictureEdit19, this.menuMicSeat);
            this.pictureEdit19.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit19.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit19.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit19.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit19.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit19.Properties.UseParentBackground = true;
            this.pictureEdit19.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit19.TabIndex = 77;
            this.pictureEdit19.Tag = "Mic016";
            this.pictureEdit19.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit19.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit19.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit19.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit19.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit19.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit28
            // 
            this.pictureEdit28.AllowDrop = true;
            this.pictureEdit28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit28.Location = new System.Drawing.Point(675, 127);
            this.pictureEdit28.Name = "pictureEdit28";
            this.ribbon.SetPopupContextMenu(this.pictureEdit28, this.menuMicSeat);
            this.pictureEdit28.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit28.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit28.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit28.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit28.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit28.Properties.UseParentBackground = true;
            this.pictureEdit28.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit28.TabIndex = 107;
            this.pictureEdit28.Tag = "Mic031";
            this.pictureEdit28.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit28.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit28.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit28.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit28.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit28.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit20
            // 
            this.pictureEdit20.AllowDrop = true;
            this.pictureEdit20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit20.Location = new System.Drawing.Point(675, 35);
            this.pictureEdit20.Name = "pictureEdit20";
            this.ribbon.SetPopupContextMenu(this.pictureEdit20, this.menuMicSeat);
            this.pictureEdit20.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit20.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit20.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit20.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit20.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit20.Properties.UseParentBackground = true;
            this.pictureEdit20.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit20.TabIndex = 75;
            this.pictureEdit20.Tag = "Mic015";
            this.pictureEdit20.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit20.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit20.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit20.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit20.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit20.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit29
            // 
            this.pictureEdit29.AllowDrop = true;
            this.pictureEdit29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit29.Location = new System.Drawing.Point(636, 127);
            this.pictureEdit29.Name = "pictureEdit29";
            this.ribbon.SetPopupContextMenu(this.pictureEdit29, this.menuMicSeat);
            this.pictureEdit29.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit29.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit29.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit29.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit29.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit29.Properties.UseParentBackground = true;
            this.pictureEdit29.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit29.TabIndex = 105;
            this.pictureEdit29.Tag = "Mic030";
            this.pictureEdit29.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit29.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit29.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit29.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit29.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit29.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit21
            // 
            this.pictureEdit21.AllowDrop = true;
            this.pictureEdit21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit21.Location = new System.Drawing.Point(636, 35);
            this.pictureEdit21.Name = "pictureEdit21";
            this.ribbon.SetPopupContextMenu(this.pictureEdit21, this.menuMicSeat);
            this.pictureEdit21.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit21.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit21.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit21.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit21.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit21.Properties.UseParentBackground = true;
            this.pictureEdit21.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit21.TabIndex = 73;
            this.pictureEdit21.Tag = "Mic014";
            this.pictureEdit21.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit21.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit21.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit21.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit21.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit21.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit30
            // 
            this.pictureEdit30.AllowDrop = true;
            this.pictureEdit30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit30.Location = new System.Drawing.Point(597, 127);
            this.pictureEdit30.Name = "pictureEdit30";
            this.ribbon.SetPopupContextMenu(this.pictureEdit30, this.menuMicSeat);
            this.pictureEdit30.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit30.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit30.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit30.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit30.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit30.Properties.UseParentBackground = true;
            this.pictureEdit30.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit30.TabIndex = 103;
            this.pictureEdit30.Tag = "Mic029";
            this.pictureEdit30.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit30.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit30.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit30.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit30.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit30.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit22
            // 
            this.pictureEdit22.AllowDrop = true;
            this.pictureEdit22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit22.Location = new System.Drawing.Point(597, 35);
            this.pictureEdit22.Name = "pictureEdit22";
            this.ribbon.SetPopupContextMenu(this.pictureEdit22, this.menuMicSeat);
            this.pictureEdit22.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit22.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit22.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit22.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit22.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit22.Properties.UseParentBackground = true;
            this.pictureEdit22.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit22.TabIndex = 71;
            this.pictureEdit22.Tag = "Mic013";
            this.pictureEdit22.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit22.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit22.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit22.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit22.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit22.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit31
            // 
            this.pictureEdit31.AllowDrop = true;
            this.pictureEdit31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit31.Location = new System.Drawing.Point(558, 127);
            this.pictureEdit31.Name = "pictureEdit31";
            this.ribbon.SetPopupContextMenu(this.pictureEdit31, this.menuMicSeat);
            this.pictureEdit31.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit31.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit31.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit31.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit31.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit31.Properties.UseParentBackground = true;
            this.pictureEdit31.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit31.TabIndex = 101;
            this.pictureEdit31.Tag = "Mic028";
            this.pictureEdit31.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit31.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit31.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit31.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit31.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit31.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit23
            // 
            this.pictureEdit23.AllowDrop = true;
            this.pictureEdit23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit23.Location = new System.Drawing.Point(558, 35);
            this.pictureEdit23.Name = "pictureEdit23";
            this.ribbon.SetPopupContextMenu(this.pictureEdit23, this.menuMicSeat);
            this.pictureEdit23.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit23.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit23.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit23.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit23.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit23.Properties.UseParentBackground = true;
            this.pictureEdit23.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit23.TabIndex = 69;
            this.pictureEdit23.Tag = "Mic012";
            this.pictureEdit23.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit23.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit23.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit23.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit23.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit23.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit32
            // 
            this.pictureEdit32.AllowDrop = true;
            this.pictureEdit32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit32.Location = new System.Drawing.Point(519, 127);
            this.pictureEdit32.Name = "pictureEdit32";
            this.ribbon.SetPopupContextMenu(this.pictureEdit32, this.menuMicSeat);
            this.pictureEdit32.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit32.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit32.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit32.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit32.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit32.Properties.UseParentBackground = true;
            this.pictureEdit32.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit32.TabIndex = 99;
            this.pictureEdit32.Tag = "Mic027";
            this.pictureEdit32.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit32.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit32.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit32.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit32.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit32.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit24
            // 
            this.pictureEdit24.AllowDrop = true;
            this.pictureEdit24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit24.Location = new System.Drawing.Point(519, 35);
            this.pictureEdit24.Name = "pictureEdit24";
            this.ribbon.SetPopupContextMenu(this.pictureEdit24, this.menuMicSeat);
            this.pictureEdit24.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit24.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit24.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit24.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit24.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit24.Properties.UseParentBackground = true;
            this.pictureEdit24.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit24.TabIndex = 67;
            this.pictureEdit24.Tag = "Mic011";
            this.pictureEdit24.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit24.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit24.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit24.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit24.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit24.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit33
            // 
            this.pictureEdit33.AllowDrop = true;
            this.pictureEdit33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit33.Location = new System.Drawing.Point(480, 127);
            this.pictureEdit33.Name = "pictureEdit33";
            this.ribbon.SetPopupContextMenu(this.pictureEdit33, this.menuMicSeat);
            this.pictureEdit33.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit33.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit33.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit33.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit33.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit33.Properties.UseParentBackground = true;
            this.pictureEdit33.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit33.TabIndex = 97;
            this.pictureEdit33.Tag = "Mic026";
            this.pictureEdit33.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit33.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit33.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit33.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit33.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit33.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit25
            // 
            this.pictureEdit25.AllowDrop = true;
            this.pictureEdit25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit25.Location = new System.Drawing.Point(480, 35);
            this.pictureEdit25.Name = "pictureEdit25";
            this.ribbon.SetPopupContextMenu(this.pictureEdit25, this.menuMicSeat);
            this.pictureEdit25.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit25.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit25.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit25.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit25.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit25.Properties.UseParentBackground = true;
            this.pictureEdit25.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit25.TabIndex = 65;
            this.pictureEdit25.Tag = "Mic010";
            this.pictureEdit25.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit25.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit25.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit25.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit25.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit25.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit34
            // 
            this.pictureEdit34.AllowDrop = true;
            this.pictureEdit34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit34.Location = new System.Drawing.Point(441, 127);
            this.pictureEdit34.Name = "pictureEdit34";
            this.ribbon.SetPopupContextMenu(this.pictureEdit34, this.menuMicSeat);
            this.pictureEdit34.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit34.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit34.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit34.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit34.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit34.Properties.UseParentBackground = true;
            this.pictureEdit34.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit34.TabIndex = 95;
            this.pictureEdit34.Tag = "Mic025";
            this.pictureEdit34.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit34.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit34.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit34.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit34.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit34.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit26
            // 
            this.pictureEdit26.AllowDrop = true;
            this.pictureEdit26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit26.Location = new System.Drawing.Point(441, 35);
            this.pictureEdit26.Name = "pictureEdit26";
            this.ribbon.SetPopupContextMenu(this.pictureEdit26, this.menuMicSeat);
            this.pictureEdit26.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit26.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit26.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit26.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit26.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit26.Properties.UseParentBackground = true;
            this.pictureEdit26.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit26.TabIndex = 63;
            this.pictureEdit26.Tag = "Mic009";
            this.pictureEdit26.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit26.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit26.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit26.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit26.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit26.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit35
            // 
            this.pictureEdit35.AllowDrop = true;
            this.pictureEdit35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit35.Location = new System.Drawing.Point(382, 127);
            this.pictureEdit35.Name = "pictureEdit35";
            this.ribbon.SetPopupContextMenu(this.pictureEdit35, this.menuMicSeat);
            this.pictureEdit35.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit35.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit35.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit35.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit35.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit35.Properties.UseParentBackground = true;
            this.pictureEdit35.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit35.TabIndex = 93;
            this.pictureEdit35.Tag = "Mic024";
            this.pictureEdit35.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit35.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit35.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit35.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit35.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit35.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.AllowDrop = true;
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit1.Location = new System.Drawing.Point(382, 35);
            this.pictureEdit1.Name = "pictureEdit1";
            this.ribbon.SetPopupContextMenu(this.pictureEdit1, this.menuMicSeat);
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Properties.UseParentBackground = true;
            this.pictureEdit1.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit1.TabIndex = 61;
            this.pictureEdit1.Tag = "Mic008";
            this.pictureEdit1.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit1.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit1.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit1.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit1.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit36
            // 
            this.pictureEdit36.AllowDrop = true;
            this.pictureEdit36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit36.Location = new System.Drawing.Point(343, 127);
            this.pictureEdit36.Name = "pictureEdit36";
            this.ribbon.SetPopupContextMenu(this.pictureEdit36, this.menuMicSeat);
            this.pictureEdit36.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit36.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit36.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit36.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit36.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit36.Properties.UseParentBackground = true;
            this.pictureEdit36.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit36.TabIndex = 91;
            this.pictureEdit36.Tag = "Mic023";
            this.pictureEdit36.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit36.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit36.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit36.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit36.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit36.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.AllowDrop = true;
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit2.Location = new System.Drawing.Point(343, 35);
            this.pictureEdit2.Name = "pictureEdit2";
            this.ribbon.SetPopupContextMenu(this.pictureEdit2, this.menuMicSeat);
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Properties.UseParentBackground = true;
            this.pictureEdit2.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit2.TabIndex = 59;
            this.pictureEdit2.Tag = "Mic007";
            this.pictureEdit2.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit2.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit2.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit2.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit2.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit37
            // 
            this.pictureEdit37.AllowDrop = true;
            this.pictureEdit37.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit37.Location = new System.Drawing.Point(304, 127);
            this.pictureEdit37.Name = "pictureEdit37";
            this.ribbon.SetPopupContextMenu(this.pictureEdit37, this.menuMicSeat);
            this.pictureEdit37.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit37.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit37.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit37.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit37.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit37.Properties.UseParentBackground = true;
            this.pictureEdit37.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit37.TabIndex = 89;
            this.pictureEdit37.Tag = "Mic022";
            this.pictureEdit37.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit37.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit37.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit37.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit37.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit37.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.AllowDrop = true;
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit3.Location = new System.Drawing.Point(304, 35);
            this.pictureEdit3.Name = "pictureEdit3";
            this.ribbon.SetPopupContextMenu(this.pictureEdit3, this.menuMicSeat);
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit3.Properties.UseParentBackground = true;
            this.pictureEdit3.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit3.TabIndex = 57;
            this.pictureEdit3.Tag = "Mic006";
            this.pictureEdit3.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit3.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit3.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit3.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit3.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit38
            // 
            this.pictureEdit38.AllowDrop = true;
            this.pictureEdit38.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit38.Location = new System.Drawing.Point(265, 127);
            this.pictureEdit38.Name = "pictureEdit38";
            this.ribbon.SetPopupContextMenu(this.pictureEdit38, this.menuMicSeat);
            this.pictureEdit38.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit38.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit38.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit38.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit38.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit38.Properties.UseParentBackground = true;
            this.pictureEdit38.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit38.TabIndex = 87;
            this.pictureEdit38.Tag = "Mic021";
            this.pictureEdit38.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit38.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit38.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit38.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit38.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit38.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.AllowDrop = true;
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit4.Location = new System.Drawing.Point(265, 35);
            this.pictureEdit4.Name = "pictureEdit4";
            this.ribbon.SetPopupContextMenu(this.pictureEdit4, this.menuMicSeat);
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit4.Properties.UseParentBackground = true;
            this.pictureEdit4.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit4.TabIndex = 55;
            this.pictureEdit4.Tag = "Mic005";
            this.pictureEdit4.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit4.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit4.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit4.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit4.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit39
            // 
            this.pictureEdit39.AllowDrop = true;
            this.pictureEdit39.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit39.Location = new System.Drawing.Point(226, 127);
            this.pictureEdit39.Name = "pictureEdit39";
            this.ribbon.SetPopupContextMenu(this.pictureEdit39, this.menuMicSeat);
            this.pictureEdit39.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit39.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit39.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit39.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit39.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit39.Properties.UseParentBackground = true;
            this.pictureEdit39.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit39.TabIndex = 85;
            this.pictureEdit39.Tag = "Mic020";
            this.pictureEdit39.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit39.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit39.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit39.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit39.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit39.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.AllowDrop = true;
            this.pictureEdit5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit5.Location = new System.Drawing.Point(226, 35);
            this.pictureEdit5.Name = "pictureEdit5";
            this.ribbon.SetPopupContextMenu(this.pictureEdit5, this.menuMicSeat);
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit5.Properties.UseParentBackground = true;
            this.pictureEdit5.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit5.TabIndex = 53;
            this.pictureEdit5.Tag = "Mic004";
            this.pictureEdit5.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit5.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit5.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit5.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit5.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit40
            // 
            this.pictureEdit40.AllowDrop = true;
            this.pictureEdit40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit40.Location = new System.Drawing.Point(187, 127);
            this.pictureEdit40.Name = "pictureEdit40";
            this.ribbon.SetPopupContextMenu(this.pictureEdit40, this.menuMicSeat);
            this.pictureEdit40.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit40.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit40.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit40.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit40.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit40.Properties.UseParentBackground = true;
            this.pictureEdit40.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit40.TabIndex = 83;
            this.pictureEdit40.Tag = "Mic019";
            this.pictureEdit40.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit40.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit40.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit40.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit40.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit40.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.AllowDrop = true;
            this.pictureEdit6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit6.Location = new System.Drawing.Point(187, 35);
            this.pictureEdit6.Name = "pictureEdit6";
            this.ribbon.SetPopupContextMenu(this.pictureEdit6, this.menuMicSeat);
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit6.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit6.Properties.UseParentBackground = true;
            this.pictureEdit6.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit6.TabIndex = 51;
            this.pictureEdit6.Tag = "Mic003";
            this.pictureEdit6.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit6.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit6.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit6.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit6.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit41
            // 
            this.pictureEdit41.AllowDrop = true;
            this.pictureEdit41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit41.Location = new System.Drawing.Point(148, 127);
            this.pictureEdit41.Name = "pictureEdit41";
            this.ribbon.SetPopupContextMenu(this.pictureEdit41, this.menuMicSeat);
            this.pictureEdit41.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit41.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit41.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit41.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit41.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit41.Properties.UseParentBackground = true;
            this.pictureEdit41.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit41.TabIndex = 81;
            this.pictureEdit41.Tag = "Mic018";
            this.pictureEdit41.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit41.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit41.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit41.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit41.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit41.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // Mic_2
            // 
            this.Mic_2.AllowDrop = true;
            this.Mic_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mic_2.Location = new System.Drawing.Point(148, 35);
            this.Mic_2.Name = "Mic_2";
            this.ribbon.SetPopupContextMenu(this.Mic_2, this.menuMicSeat);
            this.Mic_2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Mic_2.Properties.Appearance.Options.UseBackColor = true;
            this.Mic_2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Mic_2.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.Mic_2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.Mic_2.Properties.UseParentBackground = true;
            this.Mic_2.Size = new System.Drawing.Size(28, 32);
            this.Mic_2.TabIndex = 49;
            this.Mic_2.Tag = "Mic002";
            this.Mic_2.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.Mic_2.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.Mic_2.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.Mic_2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.Mic_2.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.Mic_2.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit42
            // 
            this.pictureEdit42.AllowDrop = true;
            this.pictureEdit42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit42.Location = new System.Drawing.Point(109, 127);
            this.pictureEdit42.Name = "pictureEdit42";
            this.ribbon.SetPopupContextMenu(this.pictureEdit42, this.menuMicSeat);
            this.pictureEdit42.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit42.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit42.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit42.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit42.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit42.Properties.UseParentBackground = true;
            this.pictureEdit42.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit42.TabIndex = 79;
            this.pictureEdit42.Tag = "Mic017";
            this.pictureEdit42.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit42.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit42.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit42.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit42.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit42.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit75
            // 
            this.pictureEdit75.AllowDrop = true;
            this.pictureEdit75.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit75.Location = new System.Drawing.Point(714, 431);
            this.pictureEdit75.Name = "pictureEdit75";
            this.ribbon.SetPopupContextMenu(this.pictureEdit75, this.menuMicSeat);
            this.pictureEdit75.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit75.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit75.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit75.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit75.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit75.Properties.UseParentBackground = true;
            this.pictureEdit75.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit75.TabIndex = 269;
            this.pictureEdit75.Tag = "Mic112";
            this.pictureEdit75.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit75.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit75.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit75.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit75.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit75.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit76
            // 
            this.pictureEdit76.AllowDrop = true;
            this.pictureEdit76.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit76.Location = new System.Drawing.Point(675, 431);
            this.pictureEdit76.Name = "pictureEdit76";
            this.ribbon.SetPopupContextMenu(this.pictureEdit76, this.menuMicSeat);
            this.pictureEdit76.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit76.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit76.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit76.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit76.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit76.Properties.UseParentBackground = true;
            this.pictureEdit76.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit76.TabIndex = 267;
            this.pictureEdit76.Tag = "Mic111";
            this.pictureEdit76.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit76.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit76.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit76.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit76.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit76.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit77
            // 
            this.pictureEdit77.AllowDrop = true;
            this.pictureEdit77.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit77.Location = new System.Drawing.Point(636, 431);
            this.pictureEdit77.Name = "pictureEdit77";
            this.ribbon.SetPopupContextMenu(this.pictureEdit77, this.menuMicSeat);
            this.pictureEdit77.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit77.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit77.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit77.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit77.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit77.Properties.UseParentBackground = true;
            this.pictureEdit77.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit77.TabIndex = 265;
            this.pictureEdit77.Tag = "Mic110";
            this.pictureEdit77.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit77.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit77.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit77.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit77.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit77.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit78
            // 
            this.pictureEdit78.AllowDrop = true;
            this.pictureEdit78.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit78.Location = new System.Drawing.Point(597, 431);
            this.pictureEdit78.Name = "pictureEdit78";
            this.ribbon.SetPopupContextMenu(this.pictureEdit78, this.menuMicSeat);
            this.pictureEdit78.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit78.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit78.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit78.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit78.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit78.Properties.UseParentBackground = true;
            this.pictureEdit78.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit78.TabIndex = 263;
            this.pictureEdit78.Tag = "Mic109";
            this.pictureEdit78.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit78.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit78.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit78.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit78.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit78.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit79
            // 
            this.pictureEdit79.AllowDrop = true;
            this.pictureEdit79.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit79.Location = new System.Drawing.Point(558, 431);
            this.pictureEdit79.Name = "pictureEdit79";
            this.ribbon.SetPopupContextMenu(this.pictureEdit79, this.menuMicSeat);
            this.pictureEdit79.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit79.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit79.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit79.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit79.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit79.Properties.UseParentBackground = true;
            this.pictureEdit79.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit79.TabIndex = 261;
            this.pictureEdit79.Tag = "Mic108";
            this.pictureEdit79.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit79.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit79.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit79.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit79.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit79.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit80
            // 
            this.pictureEdit80.AllowDrop = true;
            this.pictureEdit80.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit80.Location = new System.Drawing.Point(519, 431);
            this.pictureEdit80.Name = "pictureEdit80";
            this.ribbon.SetPopupContextMenu(this.pictureEdit80, this.menuMicSeat);
            this.pictureEdit80.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit80.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit80.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit80.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit80.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit80.Properties.UseParentBackground = true;
            this.pictureEdit80.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit80.TabIndex = 259;
            this.pictureEdit80.Tag = "Mic107";
            this.pictureEdit80.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit80.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit80.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit80.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit80.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit80.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit81
            // 
            this.pictureEdit81.AllowDrop = true;
            this.pictureEdit81.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit81.Location = new System.Drawing.Point(480, 431);
            this.pictureEdit81.Name = "pictureEdit81";
            this.ribbon.SetPopupContextMenu(this.pictureEdit81, this.menuMicSeat);
            this.pictureEdit81.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit81.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit81.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit81.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit81.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit81.Properties.UseParentBackground = true;
            this.pictureEdit81.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit81.TabIndex = 257;
            this.pictureEdit81.Tag = "Mic106";
            this.pictureEdit81.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit81.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit81.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit81.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit81.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit81.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit82
            // 
            this.pictureEdit82.AllowDrop = true;
            this.pictureEdit82.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit82.Location = new System.Drawing.Point(441, 431);
            this.pictureEdit82.Name = "pictureEdit82";
            this.ribbon.SetPopupContextMenu(this.pictureEdit82, this.menuMicSeat);
            this.pictureEdit82.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit82.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit82.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit82.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit82.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit82.Properties.UseParentBackground = true;
            this.pictureEdit82.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit82.TabIndex = 255;
            this.pictureEdit82.Tag = "Mic105";
            this.pictureEdit82.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit82.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit82.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit82.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit82.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit82.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit83
            // 
            this.pictureEdit83.AllowDrop = true;
            this.pictureEdit83.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit83.Location = new System.Drawing.Point(382, 431);
            this.pictureEdit83.Name = "pictureEdit83";
            this.ribbon.SetPopupContextMenu(this.pictureEdit83, this.menuMicSeat);
            this.pictureEdit83.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit83.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit83.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit83.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit83.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit83.Properties.UseParentBackground = true;
            this.pictureEdit83.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit83.TabIndex = 253;
            this.pictureEdit83.Tag = "Mic104";
            this.pictureEdit83.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit83.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit83.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit83.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit83.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit83.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit84
            // 
            this.pictureEdit84.AllowDrop = true;
            this.pictureEdit84.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit84.Location = new System.Drawing.Point(343, 431);
            this.pictureEdit84.Name = "pictureEdit84";
            this.ribbon.SetPopupContextMenu(this.pictureEdit84, this.menuMicSeat);
            this.pictureEdit84.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit84.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit84.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit84.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit84.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit84.Properties.UseParentBackground = true;
            this.pictureEdit84.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit84.TabIndex = 251;
            this.pictureEdit84.Tag = "Mic103";
            this.pictureEdit84.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit84.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit84.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit84.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit84.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit84.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit85
            // 
            this.pictureEdit85.AllowDrop = true;
            this.pictureEdit85.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit85.Location = new System.Drawing.Point(304, 431);
            this.pictureEdit85.Name = "pictureEdit85";
            this.ribbon.SetPopupContextMenu(this.pictureEdit85, this.menuMicSeat);
            this.pictureEdit85.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit85.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit85.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit85.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit85.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit85.Properties.UseParentBackground = true;
            this.pictureEdit85.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit85.TabIndex = 249;
            this.pictureEdit85.Tag = "Mic102";
            this.pictureEdit85.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit85.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit85.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit85.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit85.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit85.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit86
            // 
            this.pictureEdit86.AllowDrop = true;
            this.pictureEdit86.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit86.Location = new System.Drawing.Point(265, 431);
            this.pictureEdit86.Name = "pictureEdit86";
            this.ribbon.SetPopupContextMenu(this.pictureEdit86, this.menuMicSeat);
            this.pictureEdit86.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit86.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit86.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit86.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit86.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit86.Properties.UseParentBackground = true;
            this.pictureEdit86.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit86.TabIndex = 247;
            this.pictureEdit86.Tag = "Mic101";
            this.pictureEdit86.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit86.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit86.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit86.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit86.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit86.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit87
            // 
            this.pictureEdit87.AllowDrop = true;
            this.pictureEdit87.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit87.Location = new System.Drawing.Point(226, 431);
            this.pictureEdit87.Name = "pictureEdit87";
            this.ribbon.SetPopupContextMenu(this.pictureEdit87, this.menuMicSeat);
            this.pictureEdit87.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit87.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit87.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit87.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit87.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit87.Properties.UseParentBackground = true;
            this.pictureEdit87.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit87.TabIndex = 245;
            this.pictureEdit87.Tag = "Mic100";
            this.pictureEdit87.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit87.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit87.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit87.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit87.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit87.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit88
            // 
            this.pictureEdit88.AllowDrop = true;
            this.pictureEdit88.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit88.Location = new System.Drawing.Point(187, 431);
            this.pictureEdit88.Name = "pictureEdit88";
            this.ribbon.SetPopupContextMenu(this.pictureEdit88, this.menuMicSeat);
            this.pictureEdit88.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit88.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit88.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit88.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit88.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit88.Properties.UseParentBackground = true;
            this.pictureEdit88.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit88.TabIndex = 243;
            this.pictureEdit88.Tag = "Mic099";
            this.pictureEdit88.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit88.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit88.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit88.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit88.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit88.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit89
            // 
            this.pictureEdit89.AllowDrop = true;
            this.pictureEdit89.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit89.Location = new System.Drawing.Point(148, 431);
            this.pictureEdit89.Name = "pictureEdit89";
            this.ribbon.SetPopupContextMenu(this.pictureEdit89, this.menuMicSeat);
            this.pictureEdit89.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit89.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit89.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit89.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit89.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit89.Properties.UseParentBackground = true;
            this.pictureEdit89.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit89.TabIndex = 241;
            this.pictureEdit89.Tag = "Mic098";
            this.pictureEdit89.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit89.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit89.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit89.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit89.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit89.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit90
            // 
            this.pictureEdit90.AllowDrop = true;
            this.pictureEdit90.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit90.Location = new System.Drawing.Point(109, 431);
            this.pictureEdit90.Name = "pictureEdit90";
            this.ribbon.SetPopupContextMenu(this.pictureEdit90, this.menuMicSeat);
            this.pictureEdit90.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit90.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit90.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit90.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit90.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit90.Properties.UseParentBackground = true;
            this.pictureEdit90.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit90.TabIndex = 239;
            this.pictureEdit90.Tag = "Mic097";
            this.pictureEdit90.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit90.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit90.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit90.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit90.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit90.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit91
            // 
            this.pictureEdit91.AllowDrop = true;
            this.pictureEdit91.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit91.Location = new System.Drawing.Point(714, 367);
            this.pictureEdit91.Name = "pictureEdit91";
            this.ribbon.SetPopupContextMenu(this.pictureEdit91, this.menuMicSeat);
            this.pictureEdit91.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit91.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit91.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit91.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit91.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit91.Properties.UseParentBackground = true;
            this.pictureEdit91.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit91.TabIndex = 237;
            this.pictureEdit91.Tag = "Mic096";
            this.pictureEdit91.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit91.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit91.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit91.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit91.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit91.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit92
            // 
            this.pictureEdit92.AllowDrop = true;
            this.pictureEdit92.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit92.Location = new System.Drawing.Point(675, 367);
            this.pictureEdit92.Name = "pictureEdit92";
            this.ribbon.SetPopupContextMenu(this.pictureEdit92, this.menuMicSeat);
            this.pictureEdit92.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit92.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit92.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit92.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit92.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit92.Properties.UseParentBackground = true;
            this.pictureEdit92.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit92.TabIndex = 235;
            this.pictureEdit92.Tag = "Mic095";
            this.pictureEdit92.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit92.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit92.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit92.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit92.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit92.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit93
            // 
            this.pictureEdit93.AllowDrop = true;
            this.pictureEdit93.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit93.Location = new System.Drawing.Point(636, 367);
            this.pictureEdit93.Name = "pictureEdit93";
            this.ribbon.SetPopupContextMenu(this.pictureEdit93, this.menuMicSeat);
            this.pictureEdit93.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit93.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit93.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit93.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit93.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit93.Properties.UseParentBackground = true;
            this.pictureEdit93.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit93.TabIndex = 233;
            this.pictureEdit93.Tag = "Mic094";
            this.pictureEdit93.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit93.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit93.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit93.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit93.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit93.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit94
            // 
            this.pictureEdit94.AllowDrop = true;
            this.pictureEdit94.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit94.Location = new System.Drawing.Point(597, 367);
            this.pictureEdit94.Name = "pictureEdit94";
            this.ribbon.SetPopupContextMenu(this.pictureEdit94, this.menuMicSeat);
            this.pictureEdit94.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit94.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit94.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit94.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit94.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit94.Properties.UseParentBackground = true;
            this.pictureEdit94.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit94.TabIndex = 231;
            this.pictureEdit94.Tag = "Mic093";
            this.pictureEdit94.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit94.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit94.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit94.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit94.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit94.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit95
            // 
            this.pictureEdit95.AllowDrop = true;
            this.pictureEdit95.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit95.Location = new System.Drawing.Point(558, 367);
            this.pictureEdit95.Name = "pictureEdit95";
            this.ribbon.SetPopupContextMenu(this.pictureEdit95, this.menuMicSeat);
            this.pictureEdit95.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit95.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit95.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit95.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit95.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit95.Properties.UseParentBackground = true;
            this.pictureEdit95.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit95.TabIndex = 229;
            this.pictureEdit95.Tag = "Mic092";
            this.pictureEdit95.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit95.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit95.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit95.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit95.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit95.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit96
            // 
            this.pictureEdit96.AllowDrop = true;
            this.pictureEdit96.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit96.Location = new System.Drawing.Point(519, 367);
            this.pictureEdit96.Name = "pictureEdit96";
            this.ribbon.SetPopupContextMenu(this.pictureEdit96, this.menuMicSeat);
            this.pictureEdit96.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit96.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit96.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit96.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit96.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit96.Properties.UseParentBackground = true;
            this.pictureEdit96.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit96.TabIndex = 227;
            this.pictureEdit96.Tag = "Mic091";
            this.pictureEdit96.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit96.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit96.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit96.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit96.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit96.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit97
            // 
            this.pictureEdit97.AllowDrop = true;
            this.pictureEdit97.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit97.Location = new System.Drawing.Point(480, 367);
            this.pictureEdit97.Name = "pictureEdit97";
            this.ribbon.SetPopupContextMenu(this.pictureEdit97, this.menuMicSeat);
            this.pictureEdit97.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit97.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit97.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit97.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit97.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit97.Properties.UseParentBackground = true;
            this.pictureEdit97.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit97.TabIndex = 225;
            this.pictureEdit97.Tag = "Mic090";
            this.pictureEdit97.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit97.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit97.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit97.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit97.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit97.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit98
            // 
            this.pictureEdit98.AllowDrop = true;
            this.pictureEdit98.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit98.Location = new System.Drawing.Point(441, 367);
            this.pictureEdit98.Name = "pictureEdit98";
            this.ribbon.SetPopupContextMenu(this.pictureEdit98, this.menuMicSeat);
            this.pictureEdit98.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit98.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit98.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit98.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit98.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit98.Properties.UseParentBackground = true;
            this.pictureEdit98.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit98.TabIndex = 223;
            this.pictureEdit98.Tag = "Mic089";
            this.pictureEdit98.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit98.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit98.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit98.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit98.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit98.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit99
            // 
            this.pictureEdit99.AllowDrop = true;
            this.pictureEdit99.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit99.Location = new System.Drawing.Point(382, 367);
            this.pictureEdit99.Name = "pictureEdit99";
            this.ribbon.SetPopupContextMenu(this.pictureEdit99, this.menuMicSeat);
            this.pictureEdit99.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit99.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit99.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit99.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit99.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit99.Properties.UseParentBackground = true;
            this.pictureEdit99.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit99.TabIndex = 221;
            this.pictureEdit99.Tag = "Mic088";
            this.pictureEdit99.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit99.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit99.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit99.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit99.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit99.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit100
            // 
            this.pictureEdit100.AllowDrop = true;
            this.pictureEdit100.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit100.Location = new System.Drawing.Point(343, 367);
            this.pictureEdit100.Name = "pictureEdit100";
            this.ribbon.SetPopupContextMenu(this.pictureEdit100, this.menuMicSeat);
            this.pictureEdit100.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit100.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit100.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit100.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit100.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit100.Properties.UseParentBackground = true;
            this.pictureEdit100.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit100.TabIndex = 219;
            this.pictureEdit100.Tag = "Mic087";
            this.pictureEdit100.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit100.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit100.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit100.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit100.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit100.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit101
            // 
            this.pictureEdit101.AllowDrop = true;
            this.pictureEdit101.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit101.Location = new System.Drawing.Point(304, 367);
            this.pictureEdit101.Name = "pictureEdit101";
            this.ribbon.SetPopupContextMenu(this.pictureEdit101, this.menuMicSeat);
            this.pictureEdit101.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit101.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit101.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit101.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit101.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit101.Properties.UseParentBackground = true;
            this.pictureEdit101.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit101.TabIndex = 217;
            this.pictureEdit101.Tag = "Mic086";
            this.pictureEdit101.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit101.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit101.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit101.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit101.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit101.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit102
            // 
            this.pictureEdit102.AllowDrop = true;
            this.pictureEdit102.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit102.Location = new System.Drawing.Point(265, 367);
            this.pictureEdit102.Name = "pictureEdit102";
            this.ribbon.SetPopupContextMenu(this.pictureEdit102, this.menuMicSeat);
            this.pictureEdit102.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit102.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit102.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit102.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit102.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit102.Properties.UseParentBackground = true;
            this.pictureEdit102.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit102.TabIndex = 215;
            this.pictureEdit102.Tag = "Mic085";
            this.pictureEdit102.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit102.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit102.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit102.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit102.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit102.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit103
            // 
            this.pictureEdit103.AllowDrop = true;
            this.pictureEdit103.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit103.Location = new System.Drawing.Point(226, 367);
            this.pictureEdit103.Name = "pictureEdit103";
            this.ribbon.SetPopupContextMenu(this.pictureEdit103, this.menuMicSeat);
            this.pictureEdit103.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit103.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit103.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit103.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit103.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit103.Properties.UseParentBackground = true;
            this.pictureEdit103.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit103.TabIndex = 213;
            this.pictureEdit103.Tag = "Mic084";
            this.pictureEdit103.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit103.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit103.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit103.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit103.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit103.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit104
            // 
            this.pictureEdit104.AllowDrop = true;
            this.pictureEdit104.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit104.Location = new System.Drawing.Point(187, 367);
            this.pictureEdit104.Name = "pictureEdit104";
            this.ribbon.SetPopupContextMenu(this.pictureEdit104, this.menuMicSeat);
            this.pictureEdit104.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit104.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit104.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit104.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit104.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit104.Properties.UseParentBackground = true;
            this.pictureEdit104.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit104.TabIndex = 211;
            this.pictureEdit104.Tag = "Mic083";
            this.pictureEdit104.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit104.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit104.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit104.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit104.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit104.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit105
            // 
            this.pictureEdit105.AllowDrop = true;
            this.pictureEdit105.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit105.Location = new System.Drawing.Point(148, 367);
            this.pictureEdit105.Name = "pictureEdit105";
            this.ribbon.SetPopupContextMenu(this.pictureEdit105, this.menuMicSeat);
            this.pictureEdit105.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit105.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit105.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit105.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit105.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit105.Properties.UseParentBackground = true;
            this.pictureEdit105.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit105.TabIndex = 209;
            this.pictureEdit105.Tag = "Mic082";
            this.pictureEdit105.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit105.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit105.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit105.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit105.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit105.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit106
            // 
            this.pictureEdit106.AllowDrop = true;
            this.pictureEdit106.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit106.Location = new System.Drawing.Point(109, 367);
            this.pictureEdit106.Name = "pictureEdit106";
            this.ribbon.SetPopupContextMenu(this.pictureEdit106, this.menuMicSeat);
            this.pictureEdit106.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit106.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit106.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit106.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit106.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit106.Properties.UseParentBackground = true;
            this.pictureEdit106.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit106.TabIndex = 207;
            this.pictureEdit106.Tag = "Mic081";
            this.pictureEdit106.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit106.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit106.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit106.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit106.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit106.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit107
            // 
            this.pictureEdit107.AllowDrop = true;
            this.pictureEdit107.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit107.Location = new System.Drawing.Point(714, 308);
            this.pictureEdit107.Name = "pictureEdit107";
            this.ribbon.SetPopupContextMenu(this.pictureEdit107, this.menuMicSeat);
            this.pictureEdit107.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit107.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit107.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit107.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit107.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit107.Properties.UseParentBackground = true;
            this.pictureEdit107.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit107.TabIndex = 205;
            this.pictureEdit107.Tag = "Mic080";
            this.pictureEdit107.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit107.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit107.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit107.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit107.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit107.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit108
            // 
            this.pictureEdit108.AllowDrop = true;
            this.pictureEdit108.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit108.Location = new System.Drawing.Point(675, 308);
            this.pictureEdit108.Name = "pictureEdit108";
            this.ribbon.SetPopupContextMenu(this.pictureEdit108, this.menuMicSeat);
            this.pictureEdit108.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit108.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit108.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit108.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit108.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit108.Properties.UseParentBackground = true;
            this.pictureEdit108.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit108.TabIndex = 203;
            this.pictureEdit108.Tag = "Mic079";
            this.pictureEdit108.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit108.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit108.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit108.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit108.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit108.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit109
            // 
            this.pictureEdit109.AllowDrop = true;
            this.pictureEdit109.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit109.Location = new System.Drawing.Point(636, 308);
            this.pictureEdit109.Name = "pictureEdit109";
            this.ribbon.SetPopupContextMenu(this.pictureEdit109, this.menuMicSeat);
            this.pictureEdit109.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit109.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit109.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit109.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit109.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit109.Properties.UseParentBackground = true;
            this.pictureEdit109.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit109.TabIndex = 201;
            this.pictureEdit109.Tag = "Mic078";
            this.pictureEdit109.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit109.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit109.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit109.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit109.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit109.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit110
            // 
            this.pictureEdit110.AllowDrop = true;
            this.pictureEdit110.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit110.Location = new System.Drawing.Point(597, 308);
            this.pictureEdit110.Name = "pictureEdit110";
            this.ribbon.SetPopupContextMenu(this.pictureEdit110, this.menuMicSeat);
            this.pictureEdit110.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit110.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit110.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit110.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit110.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit110.Properties.UseParentBackground = true;
            this.pictureEdit110.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit110.TabIndex = 199;
            this.pictureEdit110.Tag = "Mic077";
            this.pictureEdit110.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit110.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit110.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit110.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit110.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit110.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit111
            // 
            this.pictureEdit111.AllowDrop = true;
            this.pictureEdit111.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit111.Location = new System.Drawing.Point(558, 308);
            this.pictureEdit111.Name = "pictureEdit111";
            this.ribbon.SetPopupContextMenu(this.pictureEdit111, this.menuMicSeat);
            this.pictureEdit111.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit111.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit111.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit111.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit111.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit111.Properties.UseParentBackground = true;
            this.pictureEdit111.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit111.TabIndex = 197;
            this.pictureEdit111.Tag = "Mic076";
            this.pictureEdit111.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit111.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit111.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit111.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit111.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit111.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit112
            // 
            this.pictureEdit112.AllowDrop = true;
            this.pictureEdit112.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit112.Location = new System.Drawing.Point(519, 308);
            this.pictureEdit112.Name = "pictureEdit112";
            this.ribbon.SetPopupContextMenu(this.pictureEdit112, this.menuMicSeat);
            this.pictureEdit112.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit112.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit112.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit112.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit112.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit112.Properties.UseParentBackground = true;
            this.pictureEdit112.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit112.TabIndex = 195;
            this.pictureEdit112.Tag = "Mic075";
            this.pictureEdit112.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit112.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit112.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit112.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit112.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit112.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit113
            // 
            this.pictureEdit113.AllowDrop = true;
            this.pictureEdit113.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit113.Location = new System.Drawing.Point(480, 308);
            this.pictureEdit113.Name = "pictureEdit113";
            this.ribbon.SetPopupContextMenu(this.pictureEdit113, this.menuMicSeat);
            this.pictureEdit113.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit113.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit113.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit113.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit113.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit113.Properties.UseParentBackground = true;
            this.pictureEdit113.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit113.TabIndex = 193;
            this.pictureEdit113.Tag = "Mic074";
            this.pictureEdit113.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit113.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit113.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit113.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit113.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit113.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit114
            // 
            this.pictureEdit114.AllowDrop = true;
            this.pictureEdit114.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit114.Location = new System.Drawing.Point(441, 308);
            this.pictureEdit114.Name = "pictureEdit114";
            this.ribbon.SetPopupContextMenu(this.pictureEdit114, this.menuMicSeat);
            this.pictureEdit114.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit114.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit114.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit114.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit114.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit114.Properties.UseParentBackground = true;
            this.pictureEdit114.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit114.TabIndex = 191;
            this.pictureEdit114.Tag = "Mic073";
            this.pictureEdit114.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit114.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit114.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit114.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit114.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit114.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit115
            // 
            this.pictureEdit115.AllowDrop = true;
            this.pictureEdit115.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit115.Location = new System.Drawing.Point(382, 308);
            this.pictureEdit115.Name = "pictureEdit115";
            this.ribbon.SetPopupContextMenu(this.pictureEdit115, this.menuMicSeat);
            this.pictureEdit115.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit115.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit115.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit115.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit115.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit115.Properties.UseParentBackground = true;
            this.pictureEdit115.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit115.TabIndex = 189;
            this.pictureEdit115.Tag = "Mic072";
            this.pictureEdit115.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit115.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit115.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit115.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit115.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit115.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit116
            // 
            this.pictureEdit116.AllowDrop = true;
            this.pictureEdit116.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit116.Location = new System.Drawing.Point(343, 308);
            this.pictureEdit116.Name = "pictureEdit116";
            this.ribbon.SetPopupContextMenu(this.pictureEdit116, this.menuMicSeat);
            this.pictureEdit116.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit116.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit116.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit116.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit116.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit116.Properties.UseParentBackground = true;
            this.pictureEdit116.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit116.TabIndex = 187;
            this.pictureEdit116.Tag = "Mic071";
            this.pictureEdit116.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit116.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit116.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit116.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit116.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit116.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit117
            // 
            this.pictureEdit117.AllowDrop = true;
            this.pictureEdit117.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit117.Location = new System.Drawing.Point(304, 308);
            this.pictureEdit117.Name = "pictureEdit117";
            this.ribbon.SetPopupContextMenu(this.pictureEdit117, this.menuMicSeat);
            this.pictureEdit117.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit117.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit117.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit117.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit117.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit117.Properties.UseParentBackground = true;
            this.pictureEdit117.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit117.TabIndex = 185;
            this.pictureEdit117.Tag = "Mic070";
            this.pictureEdit117.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit117.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit117.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit117.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit117.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit117.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit118
            // 
            this.pictureEdit118.AllowDrop = true;
            this.pictureEdit118.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit118.Location = new System.Drawing.Point(265, 308);
            this.pictureEdit118.Name = "pictureEdit118";
            this.ribbon.SetPopupContextMenu(this.pictureEdit118, this.menuMicSeat);
            this.pictureEdit118.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit118.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit118.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit118.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit118.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit118.Properties.UseParentBackground = true;
            this.pictureEdit118.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit118.TabIndex = 183;
            this.pictureEdit118.Tag = "Mic069";
            this.pictureEdit118.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit118.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit118.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit118.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit118.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit118.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit119
            // 
            this.pictureEdit119.AllowDrop = true;
            this.pictureEdit119.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit119.Location = new System.Drawing.Point(226, 308);
            this.pictureEdit119.Name = "pictureEdit119";
            this.ribbon.SetPopupContextMenu(this.pictureEdit119, this.menuMicSeat);
            this.pictureEdit119.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit119.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit119.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit119.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit119.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit119.Properties.UseParentBackground = true;
            this.pictureEdit119.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit119.TabIndex = 181;
            this.pictureEdit119.Tag = "Mic068";
            this.pictureEdit119.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit119.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit119.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit119.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit119.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit119.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit120
            // 
            this.pictureEdit120.AllowDrop = true;
            this.pictureEdit120.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit120.Location = new System.Drawing.Point(187, 308);
            this.pictureEdit120.Name = "pictureEdit120";
            this.ribbon.SetPopupContextMenu(this.pictureEdit120, this.menuMicSeat);
            this.pictureEdit120.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit120.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit120.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit120.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit120.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit120.Properties.UseParentBackground = true;
            this.pictureEdit120.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit120.TabIndex = 179;
            this.pictureEdit120.Tag = "Mic067";
            this.pictureEdit120.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit120.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit120.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit120.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit120.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit120.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit121
            // 
            this.pictureEdit121.AllowDrop = true;
            this.pictureEdit121.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit121.Location = new System.Drawing.Point(148, 308);
            this.pictureEdit121.Name = "pictureEdit121";
            this.ribbon.SetPopupContextMenu(this.pictureEdit121, this.menuMicSeat);
            this.pictureEdit121.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit121.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit121.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit121.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit121.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit121.Properties.UseParentBackground = true;
            this.pictureEdit121.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit121.TabIndex = 177;
            this.pictureEdit121.Tag = "Mic066";
            this.pictureEdit121.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit121.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit121.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit121.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit121.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit121.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // pictureEdit122
            // 
            this.pictureEdit122.AllowDrop = true;
            this.pictureEdit122.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit122.Location = new System.Drawing.Point(109, 308);
            this.pictureEdit122.Name = "pictureEdit122";
            this.ribbon.SetPopupContextMenu(this.pictureEdit122, this.menuMicSeat);
            this.pictureEdit122.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit122.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit122.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit122.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit122.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit122.Properties.UseParentBackground = true;
            this.pictureEdit122.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit122.TabIndex = 175;
            this.pictureEdit122.Tag = "Mic065";
            this.pictureEdit122.DragLeave += new System.EventHandler(this.Mic_DragLeave);
            this.pictureEdit122.DragDrop += new System.Windows.Forms.DragEventHandler(this.Mic_DragDrop);
            this.pictureEdit122.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.pictureEdit122.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDown);
            this.pictureEdit122.DragEnter += new System.Windows.Forms.DragEventHandler(this.Mic_DragEnter);
            this.pictureEdit122.DragOver += new System.Windows.Forms.DragEventHandler(this.Mic_DragOver);
            // 
            // MicSeatMenu
            // 
            this.MicSeatMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripSeparator1,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.MicSeatMenu.Name = "MicSeatMenu";
            this.MicSeatMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.MicSeatMenu.ShowCheckMargin = true;
            this.MicSeatMenu.Size = new System.Drawing.Size(283, 98);
            this.MicSeatMenu.Text = "Включение";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Checked = true;
            this.toolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(282, 22);
            this.toolStripMenuItem1.Text = "Физическое включение";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Checked = true;
            this.toolStripMenuItem2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(282, 22);
            this.toolStripMenuItem2.Text = "Логическое включение";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(279, 6);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(282, 22);
            this.toolStripMenuItem3.Text = "Выбрать депутата на это место...";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(282, 22);
            this.toolStripMenuItem4.Text = "Освободить место";
            // 
            // clientPanel
            // 
            this.clientPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.clientPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.clientPanel.Controls.Add(this.splitContainerControl1);
            this.clientPanel.Controls.Add(this.panelControl1);
            this.clientPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientPanel.Location = new System.Drawing.Point(0, 95);
            this.clientPanel.Name = "clientPanel";
            this.clientPanel.Size = new System.Drawing.Size(1242, 780);
            this.clientPanel.TabIndex = 2;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(3, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.navBarControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.AppearanceCaption.Image = ((System.Drawing.Image)(resources.GetObject("splitContainerControl1.Panel2.AppearanceCaption.Image")));
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseImage = true;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Top;
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Окно сообщений";
            this.splitContainerControl1.Size = new System.Drawing.Size(204, 722);
            this.splitContainerControl1.SplitterPosition = 441;
            this.splitContainerControl1.TabIndex = 2;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.navBarGroup1;
            this.navBarControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.navBarControl1.ContentButtonHint = null;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.itmAgenda,
            this.itmDelegates,
            this.itmQuestions,
            this.itmRegistration,
            this.itmResults,
            this.itmReport,
            this.itmStatements,
            this.navBarItem9,
            this.navBarItem10,
            this.navBarItem11,
            this.navBarItem12});
            this.navBarControl1.LargeImages = this.NavBarImages;
            this.navBarControl1.Location = new System.Drawing.Point(0, 0);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 203;
            this.navBarControl1.Size = new System.Drawing.Size(196, 437);
            this.navBarControl1.SmallImages = this.NavBarImages;
            this.navBarControl1.TabIndex = 0;
            this.navBarControl1.Text = "navBarControl1";
            this.navBarControl1.View = new DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("Caramel");
            this.navBarControl1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarControl1_LinkClicked);
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "Сессия голосования";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.GroupClientHeight = 400;
            this.navBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // itmAgenda
            // 
            this.itmAgenda.Caption = "Настройка сессии";
            this.itmAgenda.LargeImageIndex = 0;
            this.itmAgenda.Name = "itmAgenda";
            this.itmAgenda.SmallImageIndex = 0;
            // 
            // itmDelegates
            // 
            this.itmDelegates.Caption = "Депутаты";
            this.itmDelegates.LargeImageIndex = 1;
            this.itmDelegates.Name = "itmDelegates";
            this.itmDelegates.SmallImageIndex = 1;
            // 
            // itmQuestions
            // 
            this.itmQuestions.Caption = "Вопросы на голосование";
            this.itmQuestions.LargeImageIndex = 4;
            this.itmQuestions.Name = "itmQuestions";
            this.itmQuestions.SmallImageIndex = 4;
            // 
            // itmRegistration
            // 
            this.itmRegistration.Caption = "Управление залом";
            this.itmRegistration.LargeImageIndex = 3;
            this.itmRegistration.Name = "itmRegistration";
            this.itmRegistration.SmallImageIndex = 3;
            // 
            // itmResults
            // 
            this.itmResults.Caption = "Просмотр итогов";
            this.itmResults.LargeImageIndex = 5;
            this.itmResults.Name = "itmResults";
            this.itmResults.SmallImageIndex = 5;
            // 
            // itmReport
            // 
            this.itmReport.Caption = "Отчет по голосованию";
            this.itmReport.LargeImageIndex = 5;
            this.itmReport.Name = "itmReport";
            this.itmReport.SmallImageIndex = 5;
            // 
            // itmStatements
            // 
            this.itmStatements.Caption = "Выступления";
            this.itmStatements.LargeImageIndex = 6;
            this.itmStatements.Name = "itmStatements";
            // 
            // navBarItem9
            // 
            this.navBarItem9.Caption = "Статистика по депутатам";
            this.navBarItem9.Name = "navBarItem9";
            // 
            // navBarItem10
            // 
            this.navBarItem10.Caption = "navBarItem10";
            this.navBarItem10.Name = "navBarItem10";
            // 
            // navBarItem11
            // 
            this.navBarItem11.Caption = "navBarItem11";
            this.navBarItem11.Name = "navBarItem11";
            // 
            // navBarItem12
            // 
            this.navBarItem12.Caption = "navBarItem12";
            this.navBarItem12.Name = "navBarItem12";
            // 
            // NavBarImages
            // 
            this.NavBarImages.ImageSize = new System.Drawing.Size(36, 36);
            this.NavBarImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("NavBarImages.ImageStream")));
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl1.Controls.Add(this.MainTabControl);
            this.panelControl1.Location = new System.Drawing.Point(206, 6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1032, 716);
            this.panelControl1.TabIndex = 1;
            // 
            // MainTabControl
            // 
            this.MainTabControl.Location = new System.Drawing.Point(2, 2);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedTabPage = this.pgSession;
            this.MainTabControl.Size = new System.Drawing.Size(1032, 712);
            this.MainTabControl.TabIndex = 1;
            this.MainTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pgSession,
            this.pgDelegates,
            this.pgSelQuestions,
            this.pgQuorumGraphic,
            this.pgResQuestions,
            this.pgStatements});
            this.MainTabControl.Text = "Список";
            this.MainTabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // pgSession
            // 
            this.pgSession.Controls.Add(this.labelControl21);
            this.pgSession.Controls.Add(this.txtVoteTime);
            this.pgSession.Controls.Add(this.labelControl145);
            this.pgSession.Controls.Add(this.labelControl3);
            this.pgSession.Controls.Add(this.groupControl9);
            this.pgSession.Controls.Add(this.txtCode);
            this.pgSession.Controls.Add(this.labelControl17);
            this.pgSession.Controls.Add(this.labelControl7);
            this.pgSession.Controls.Add(this.txtContent);
            this.pgSession.Controls.Add(this.txtHall);
            this.pgSession.Controls.Add(this.labelControl6);
            this.pgSession.Controls.Add(this.chbRegAfterTime);
            this.pgSession.Controls.Add(this.txtRegTime);
            this.pgSession.Controls.Add(this.labelControl5);
            this.pgSession.Controls.Add(this.txtQourum);
            this.pgSession.Controls.Add(this.labelControl4);
            this.pgSession.Controls.Add(this.labelControl2);
            this.pgSession.Controls.Add(this.txtStartDate);
            this.pgSession.Controls.Add(this.labelControl1);
            this.pgSession.Controls.Add(this.txtCaption);
            this.pgSession.Name = "pgSession";
            this.pgSession.Size = new System.Drawing.Size(1023, 681);
            this.pgSession.Text = "Сессия";
            this.pgSession.Leave += new System.EventHandler(this.pgSession_Leave);
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(514, 263);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(22, 13);
            this.labelControl21.TabIndex = 21;
            this.labelControl21.Text = "мин.";
            // 
            // txtVoteTime
            // 
            this.txtVoteTime.Location = new System.Drawing.Point(408, 260);
            this.txtVoteTime.Name = "txtVoteTime";
            this.txtVoteTime.Size = new System.Drawing.Size(100, 20);
            this.txtVoteTime.TabIndex = 20;
            // 
            // labelControl145
            // 
            this.labelControl145.Location = new System.Drawing.Point(289, 264);
            this.labelControl145.Name = "labelControl145";
            this.labelControl145.Size = new System.Drawing.Size(116, 13);
            this.labelControl145.TabIndex = 19;
            this.labelControl145.Text = "Время на голосование:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(229, 263);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(22, 13);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "мин.";
            // 
            // groupControl9
            // 
            this.groupControl9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.groupControl9.Controls.Add(this.lbHistory);
            this.groupControl9.Location = new System.Drawing.Point(35, 329);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(933, 322);
            this.groupControl9.TabIndex = 17;
            this.groupControl9.Text = "История событий";
            // 
            // lbHistory
            // 
            this.lbHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbHistory.Items.AddRange(new object[] {
            "10.03.2010",
            "   10:00 старт системы",
            "   10:15 тестирование системы",
            "   11:00 регистрация депутатов "});
            this.lbHistory.Location = new System.Drawing.Point(2, 20);
            this.lbHistory.Name = "lbHistory";
            this.lbHistory.Size = new System.Drawing.Size(929, 300);
            this.lbHistory.TabIndex = 0;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(123, 30);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(174, 20);
            this.txtCode.TabIndex = 16;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(35, 30);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(60, 13);
            this.labelControl17.TabIndex = 15;
            this.labelControl17.Text = "Код сессии:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(557, 37);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(68, 13);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Содержание:";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(576, 64);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(380, 79);
            this.txtContent.TabIndex = 13;
            // 
            // txtHall
            // 
            this.txtHall.Location = new System.Drawing.Point(576, 215);
            this.txtHall.Name = "txtHall";
            this.txtHall.Size = new System.Drawing.Size(372, 20);
            this.txtHall.TabIndex = 12;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(557, 196);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(22, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Зал:";
            // 
            // chbRegAfterTime
            // 
            this.chbRegAfterTime.AutoSize = true;
            this.chbRegAfterTime.Checked = true;
            this.chbRegAfterTime.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.chbRegAfterTime.Location = new System.Drawing.Point(576, 262);
            this.chbRegAfterTime.Name = "chbRegAfterTime";
            this.chbRegAfterTime.Size = new System.Drawing.Size(269, 17);
            this.chbRegAfterTime.TabIndex = 10;
            this.chbRegAfterTime.Text = "Регистрировать пришедших после регистрации";
            this.chbRegAfterTime.UseVisualStyleBackColor = true;
            // 
            // txtRegTime
            // 
            this.txtRegTime.Location = new System.Drawing.Point(123, 260);
            this.txtRegTime.Name = "txtRegTime";
            this.txtRegTime.Size = new System.Drawing.Size(100, 20);
            this.txtRegTime.TabIndex = 9;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(35, 264);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(69, 13);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Время на рег:";
            // 
            // txtQourum
            // 
            this.txtQourum.Location = new System.Drawing.Point(123, 215);
            this.txtQourum.Name = "txtQourum";
            this.txtQourum.Size = new System.Drawing.Size(100, 20);
            this.txtQourum.TabIndex = 7;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(35, 215);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(41, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Кворум:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(35, 165);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(69, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Дата начала:";
            // 
            // txtStartDate
            // 
            this.txtStartDate.EditValue = null;
            this.txtStartDate.Location = new System.Drawing.Point(123, 162);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtStartDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtStartDate.Size = new System.Drawing.Size(174, 20);
            this.txtStartDate.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(35, 79);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Тема:";
            // 
            // txtCaption
            // 
            this.txtCaption.Location = new System.Drawing.Point(123, 77);
            this.txtCaption.Name = "txtCaption";
            this.txtCaption.Size = new System.Drawing.Size(338, 53);
            this.txtCaption.TabIndex = 0;
            // 
            // pgDelegates
            // 
            this.pgDelegates.Controls.Add(this.splitContainerControl3);
            this.pgDelegates.Name = "pgDelegates";
            this.pgDelegates.Size = new System.Drawing.Size(1023, 681);
            this.pgDelegates.Text = "Делегаты";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.gridDelegates);
            this.splitContainerControl3.Panel1.Controls.Add(this.navigatorDelegates);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.listBoxControl1);
            this.splitContainerControl3.Panel2.Controls.Add(this.PropertiesControl);
            this.splitContainerControl3.Panel2.ShowCaption = true;
            this.splitContainerControl3.Panel2.Text = "Информация";
            this.splitContainerControl3.Size = new System.Drawing.Size(1023, 681);
            this.splitContainerControl3.SplitterPosition = 780;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridDelegates
            // 
            this.gridDelegates.DataSource = this.xpDelegates;
            this.gridDelegates.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridDelegates.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridDelegates.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridDelegates.EmbeddedNavigator.Name = "";
            this.gridDelegates.Location = new System.Drawing.Point(0, 0);
            this.gridDelegates.MainView = this.gridView1;
            this.gridDelegates.Name = "gridDelegates";
            this.gridDelegates.Size = new System.Drawing.Size(776, 611);
            this.gridDelegates.TabIndex = 7;
            this.gridDelegates.UseEmbeddedNavigator = true;
            this.gridDelegates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // xpDelegates
            // 
            this.xpDelegates.DeleteObjectOnRemove = true;
            this.xpDelegates.ObjectType = typeof(VoteSystem.S_DelegateObj);
            this.xpDelegates.DisplayableProperties = "This;idSession.id;idDelegate.FullName;idDelegate.idFraction.Name;idDelegate.idPar" +
                "ty.Name;idDelegate.idRegion.Name;idDelegate.idRegion.Number;IsRegistered";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFullName,
            this.colRegionName,
            this.colRegionNum,
            this.colFraction,
            this.colPartyName,
            this.colIsRegistered});
            this.gridView1.GridControl = this.gridDelegates;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView1.OptionsBehavior.Editable = false;
            // 
            // colFullName
            // 
            this.colFullName.Caption = "ФИО";
            this.colFullName.FieldName = "idDelegate.FullName";
            this.colFullName.MinWidth = 100;
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.FixedWidth = true;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 0;
            this.colFullName.Width = 174;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Название региона";
            this.colRegionName.FieldName = "idDelegate.idRegion.Name";
            this.colRegionName.MinWidth = 100;
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 1;
            this.colRegionName.Width = 129;
            // 
            // colRegionNum
            // 
            this.colRegionNum.Caption = "Номер региона";
            this.colRegionNum.FieldName = "idDelegate.idRegion.Number";
            this.colRegionNum.MinWidth = 50;
            this.colRegionNum.Name = "colRegionNum";
            this.colRegionNum.OptionsColumn.ReadOnly = true;
            this.colRegionNum.Visible = true;
            this.colRegionNum.VisibleIndex = 2;
            this.colRegionNum.Width = 126;
            // 
            // colFraction
            // 
            this.colFraction.Caption = "Фракция";
            this.colFraction.FieldName = "idDelegate.idFraction.Name";
            this.colFraction.MinWidth = 100;
            this.colFraction.Name = "colFraction";
            this.colFraction.OptionsColumn.ReadOnly = true;
            this.colFraction.Visible = true;
            this.colFraction.VisibleIndex = 3;
            this.colFraction.Width = 100;
            // 
            // colPartyName
            // 
            this.colPartyName.Caption = "Партия";
            this.colPartyName.FieldName = "idDelegate.idParty.Name";
            this.colPartyName.MinWidth = 130;
            this.colPartyName.Name = "colPartyName";
            this.colPartyName.OptionsColumn.ReadOnly = true;
            this.colPartyName.Visible = true;
            this.colPartyName.VisibleIndex = 4;
            this.colPartyName.Width = 142;
            // 
            // colIsRegistered
            // 
            this.colIsRegistered.Caption = "Регистрация";
            this.colIsRegistered.FieldName = "IsRegistered";
            this.colIsRegistered.MinWidth = 100;
            this.colIsRegistered.Name = "colIsRegistered";
            this.colIsRegistered.Visible = true;
            this.colIsRegistered.VisibleIndex = 5;
            this.colIsRegistered.Width = 100;
            // 
            // navigatorDelegates
            // 
            this.navigatorDelegates.Buttons.Append.Visible = false;
            this.navigatorDelegates.Buttons.CancelEdit.Visible = false;
            this.navigatorDelegates.Buttons.EndEdit.Visible = false;
            this.navigatorDelegates.Buttons.ImageList = this.ButtonImages;
            this.navigatorDelegates.Buttons.Remove.Visible = false;
            this.navigatorDelegates.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "", "Append"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "", "Remove")});
            this.navigatorDelegates.DataSource = this.xpDelegates;
            this.navigatorDelegates.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.navigatorDelegates.Location = new System.Drawing.Point(0, 639);
            this.navigatorDelegates.Name = "navigatorDelegates";
            this.navigatorDelegates.Size = new System.Drawing.Size(776, 38);
            this.navigatorDelegates.TabIndex = 6;
            this.navigatorDelegates.Text = "dataNavigator1";
            this.navigatorDelegates.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.navigatorDelegates_ButtonClick);
            this.navigatorDelegates.PositionChanged += new System.EventHandler(this.navigatorDelegates_PositionChanged);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // listBoxControl1
            // 
            this.listBoxControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBoxControl1.Items.AddRange(new object[] {
            "12:30 Регистрация",
            "16:44 Голосование по вопросу №34"});
            this.listBoxControl1.Location = new System.Drawing.Point(0, 330);
            this.listBoxControl1.Name = "listBoxControl1";
            this.listBoxControl1.Size = new System.Drawing.Size(233, 329);
            this.listBoxControl1.TabIndex = 10;
            // 
            // PropertiesControl
            // 
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControl.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControl.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControl.Location = new System.Drawing.Point(0, 0);
            this.PropertiesControl.Name = "PropertiesControl";
            this.PropertiesControl.OptionsView.AutoScaleBands = true;
            this.PropertiesControl.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControl.OptionsView.ShowEmptyRowImage = true;
            this.PropertiesControl.RecordWidth = 114;
            this.PropertiesControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit9,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox4,
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6});
            this.PropertiesControl.RowHeaderWidth = 86;
            this.PropertiesControl.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catFIO,
            this.catRegion,
            this.catInfo,
            this.catSeat});
            this.PropertiesControl.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControl.Size = new System.Drawing.Size(233, 319);
            this.PropertiesControl.TabIndex = 9;
            // 
            // repositoryItemTextEdit9
            // 
            this.repositoryItemTextEdit9.AutoHeight = false;
            this.repositoryItemTextEdit9.Name = "repositoryItemTextEdit9";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.gridView4;
            // 
            // gridView4
            // 
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.ImmediatePopup = true;
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox4.ImmediatePopup = true;
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            this.repositoryItemComboBox4.Sorted = true;
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.ImmediatePopup = true;
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // catFIO
            // 
            this.catFIO.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowLastName,
            this.rowFirstName,
            this.rowSecondName});
            this.catFIO.Name = "catFIO";
            this.catFIO.Properties.Caption = "ФИО";
            // 
            // rowLastName
            // 
            this.rowLastName.Height = 27;
            this.rowLastName.Name = "rowLastName";
            this.rowLastName.Properties.Caption = "Фамилия";
            this.rowLastName.Properties.FieldName = "LastName";
            this.rowLastName.Properties.ReadOnly = true;
            this.rowLastName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowLastName.Properties.Value = "1111";
            // 
            // rowFirstName
            // 
            this.rowFirstName.Height = 20;
            this.rowFirstName.Name = "rowFirstName";
            this.rowFirstName.Properties.Caption = "Имя";
            this.rowFirstName.Properties.FieldName = "FirstName";
            this.rowFirstName.Properties.ReadOnly = true;
            this.rowFirstName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowSecondName
            // 
            this.rowSecondName.Height = 20;
            this.rowSecondName.Name = "rowSecondName";
            this.rowSecondName.Properties.Caption = "Отчество";
            this.rowSecondName.Properties.FieldName = "SecondName";
            this.rowSecondName.Properties.ReadOnly = true;
            // 
            // catRegion
            // 
            this.catRegion.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowRegionName,
            this.rowRegionNum});
            this.catRegion.Name = "catRegion";
            this.catRegion.Properties.Caption = "Регион";
            // 
            // rowRegionName
            // 
            this.rowRegionName.Height = 20;
            this.rowRegionName.Name = "rowRegionName";
            this.rowRegionName.Properties.Caption = "Регион";
            this.rowRegionName.Properties.FieldName = "RegionName";
            this.rowRegionName.Properties.ReadOnly = true;
            this.rowRegionName.Properties.RowEdit = this.repositoryItemComboBox4;
            this.rowRegionName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowRegionNum
            // 
            this.rowRegionNum.Height = 20;
            this.rowRegionNum.Name = "rowRegionNum";
            this.rowRegionNum.Properties.Caption = "№ региона";
            this.rowRegionNum.Properties.FieldName = "RegionNum";
            this.rowRegionNum.Properties.ReadOnly = true;
            this.rowRegionNum.Properties.RowEdit = this.repositoryItemComboBox5;
            this.rowRegionNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // catInfo
            // 
            this.catInfo.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowFraction,
            this.rowPartyName});
            this.catInfo.Name = "catInfo";
            this.catInfo.Properties.Caption = "Дополнительная информация";
            // 
            // rowFraction
            // 
            this.rowFraction.Height = 25;
            this.rowFraction.Name = "rowFraction";
            this.rowFraction.Properties.Caption = "Фракция";
            this.rowFraction.Properties.FieldName = "Fraction";
            this.rowFraction.Properties.ReadOnly = true;
            this.rowFraction.Properties.RowEdit = this.repositoryItemComboBox6;
            this.rowFraction.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowPartyName
            // 
            this.rowPartyName.Height = 23;
            this.rowPartyName.Name = "rowPartyName";
            this.rowPartyName.Properties.Caption = "Партия";
            this.rowPartyName.Properties.FieldName = "idParty.Name";
            this.rowPartyName.Properties.ReadOnly = true;
            this.rowPartyName.Properties.RowEdit = this.repositoryItemComboBox2;
            this.rowPartyName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowPartyName.Properties.Value = "Партия 1";
            // 
            // catSeat
            // 
            this.catSeat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMicNum,
            this.rowRowNum,
            this.rowSeatNum});
            this.catSeat.Name = "catSeat";
            // 
            // rowMicNum
            // 
            this.rowMicNum.Height = 16;
            this.rowMicNum.Name = "rowMicNum";
            this.rowMicNum.Properties.Caption = "Микрофон №";
            this.rowMicNum.Properties.ReadOnly = true;
            this.rowMicNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // rowRowNum
            // 
            this.rowRowNum.Name = "rowRowNum";
            this.rowRowNum.Properties.Caption = "Ряд №";
            this.rowRowNum.Properties.ReadOnly = true;
            this.rowRowNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // rowSeatNum
            // 
            this.rowSeatNum.Name = "rowSeatNum";
            this.rowSeatNum.Properties.Caption = "Место №";
            this.rowSeatNum.Properties.ReadOnly = true;
            this.rowSeatNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // pgSelQuestions
            // 
            this.pgSelQuestions.Controls.Add(this.splitContainerControl4);
            this.pgSelQuestions.Name = "pgSelQuestions";
            this.pgSelQuestions.PageVisible = false;
            this.pgSelQuestions.Size = new System.Drawing.Size(1023, 681);
            this.pgSelQuestions.Text = "Вопросы на голосование";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.btnQuestVoteMode);
            this.splitContainerControl4.Panel1.Controls.Add(this.navigatorQuestions);
            this.splitContainerControl4.Panel1.Controls.Add(this.SelQuestGrid);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.grpSelQuestPropsIn);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(1023, 681);
            this.splitContainerControl4.SplitterPosition = 623;
            this.splitContainerControl4.TabIndex = 0;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // btnQuestVoteMode
            // 
            this.btnQuestVoteMode.ImageIndex = 10;
            this.btnQuestVoteMode.ImageList = this.ButtonImages;
            this.btnQuestVoteMode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnQuestVoteMode.Location = new System.Drawing.Point(52, 630);
            this.btnQuestVoteMode.Name = "btnQuestVoteMode";
            this.btnQuestVoteMode.Size = new System.Drawing.Size(201, 38);
            this.btnQuestVoteMode.TabIndex = 236;
            this.btnQuestVoteMode.Text = "Режим голосования...";
            this.btnQuestVoteMode.Click += new System.EventHandler(this.btnQuestVoteMode_Click);
            // 
            // navigatorQuestions
            // 
            this.navigatorQuestions.Buttons.Append.Visible = false;
            this.navigatorQuestions.Buttons.CancelEdit.Visible = false;
            this.navigatorQuestions.Buttons.EndEdit.Visible = false;
            this.navigatorQuestions.Buttons.ImageList = this.ButtonImages;
            this.navigatorQuestions.Buttons.Remove.Visible = false;
            this.navigatorQuestions.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Добавить запись", "Append"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Удалить запись", "Remove"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Внести изменения", "Edit")});
            this.navigatorQuestions.DataSource = this.xpQuestions;
            this.navigatorQuestions.Dock = System.Windows.Forms.DockStyle.Top;
            this.navigatorQuestions.Location = new System.Drawing.Point(0, 583);
            this.navigatorQuestions.Name = "navigatorQuestions";
            this.navigatorQuestions.Size = new System.Drawing.Size(619, 31);
            this.navigatorQuestions.TabIndex = 235;
            this.navigatorQuestions.Text = "dataNavigator1";
            this.navigatorQuestions.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.navigatorQuestions_ButtonClick);
            this.navigatorQuestions.PositionChanged += new System.EventHandler(this.navigatorQuestions_PositionChanged);
            // 
            // xpQuestions
            // 
            this.xpQuestions.CriteriaString = "[IsResult] = False And [id] > 0";
            this.xpQuestions.DeleteObjectOnRemove = true;
            this.xpQuestions.ObjectType = typeof(VoteSystem.S_QuestionObj);
            this.xpQuestions.DisplayableProperties = @"This;id;idSession!;idSession!Key;idSession;idQuestion!;idQuestion.Caption;idQuestion;idVoteType!;idVoteType;idVoteKind!;idVoteKind;idVoteQnty!;idVoteQnty;idProcentDecisionType!;idProcentDecisionType;idQuotaProcentType!;idQuotaProcentType;idQuestion.CrDate;IsResult";
            // 
            // SelQuestGrid
            // 
            this.SelQuestGrid.DataSource = this.xpQuestions;
            this.SelQuestGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.SelQuestGrid.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.SelQuestGrid.EmbeddedNavigator.Name = "";
            this.SelQuestGrid.Location = new System.Drawing.Point(0, 0);
            this.SelQuestGrid.MainView = this.gridView5;
            this.SelQuestGrid.Name = "SelQuestGrid";
            this.SelQuestGrid.Size = new System.Drawing.Size(619, 583);
            this.SelQuestGrid.TabIndex = 234;
            this.SelQuestGrid.UseEmbeddedNavigator = true;
            this.SelQuestGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colQuestCaption,
            this.colCrDate});
            this.gridView5.GridControl = this.SelQuestGrid;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView5.OptionsBehavior.Editable = false;
            this.gridView5.OptionsDetail.AutoZoomDetail = true;
            this.gridView5.OptionsDetail.EnableMasterViewMode = false;
            // 
            // colid
            // 
            this.colid.Caption = "№";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.OptionsColumn.FixedWidth = true;
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            this.colid.Width = 42;
            // 
            // colQuestCaption
            // 
            this.colQuestCaption.Caption = "Заголовок";
            this.colQuestCaption.FieldName = "idQuestion.Caption";
            this.colQuestCaption.MinWidth = 255;
            this.colQuestCaption.Name = "colQuestCaption";
            this.colQuestCaption.Visible = true;
            this.colQuestCaption.VisibleIndex = 1;
            this.colQuestCaption.Width = 426;
            // 
            // colCrDate
            // 
            this.colCrDate.Caption = "Дата создания";
            this.colCrDate.FieldName = "idQuestion.CrDate";
            this.colCrDate.Name = "colCrDate";
            this.colCrDate.Visible = true;
            this.colCrDate.VisibleIndex = 2;
            this.colCrDate.Width = 128;
            // 
            // grpSelQuestPropsIn
            // 
            this.grpSelQuestPropsIn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.grpSelQuestPropsIn.Controls.Add(this.panelControl4);
            this.grpSelQuestPropsIn.Controls.Add(this.panelControl3);
            this.grpSelQuestPropsIn.Controls.Add(this.PropertiesControlQuest);
            this.grpSelQuestPropsIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSelQuestPropsIn.Location = new System.Drawing.Point(0, 0);
            this.grpSelQuestPropsIn.Name = "grpSelQuestPropsIn";
            this.grpSelQuestPropsIn.Size = new System.Drawing.Size(390, 677);
            this.grpSelQuestPropsIn.TabIndex = 238;
            this.grpSelQuestPropsIn.Tag = "";
            this.grpSelQuestPropsIn.Text = "Панель свойств";
            // 
            // panelControl4
            // 
            this.panelControl4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.btnPropQuestApply);
            this.panelControl4.Controls.Add(this.btnPropQuestCancel);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl4.Location = new System.Drawing.Point(2, 533);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(386, 40);
            this.panelControl4.TabIndex = 16;
            // 
            // btnPropQuestApply
            // 
            this.btnPropQuestApply.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPropQuestApply.Enabled = false;
            this.btnPropQuestApply.ImageIndex = 0;
            this.btnPropQuestApply.Location = new System.Drawing.Point(0, 0);
            this.btnPropQuestApply.Name = "btnPropQuestApply";
            this.btnPropQuestApply.Size = new System.Drawing.Size(164, 40);
            this.btnPropQuestApply.TabIndex = 12;
            this.btnPropQuestApply.Text = "Сохранить изменения";
            this.btnPropQuestApply.Click += new System.EventHandler(this.btnPropQuestApply_Click);
            // 
            // btnPropQuestCancel
            // 
            this.btnPropQuestCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPropQuestCancel.Enabled = false;
            this.btnPropQuestCancel.ImageIndex = 1;
            this.btnPropQuestCancel.Location = new System.Drawing.Point(282, 0);
            this.btnPropQuestCancel.Name = "btnPropQuestCancel";
            this.btnPropQuestCancel.Size = new System.Drawing.Size(104, 40);
            this.btnPropQuestCancel.TabIndex = 13;
            this.btnPropQuestCancel.Text = "Отменить ";
            this.btnPropQuestCancel.Click += new System.EventHandler(this.btnPropQuestCancel_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 573);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(386, 102);
            this.panelControl3.TabIndex = 15;
            // 
            // PropertiesControlQuest
            // 
            this.PropertiesControlQuest.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlQuest.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlQuest.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControlQuest.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlQuest.Location = new System.Drawing.Point(2, 20);
            this.PropertiesControlQuest.Name = "PropertiesControlQuest";
            this.PropertiesControlQuest.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControlQuest.RecordWidth = 117;
            this.PropertiesControlQuest.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpEdit2,
            this.repositoryItemComboBox9,
            this.repositoryItemComboBox10,
            this.repositoryItemComboBox11,
            this.repositoryItemComboBox12,
            this.repositoryItemComboBox13,
            this.repositoryItemComboBox14,
            this.repositoryItemDateEdit1,
            this.repositoryItemComboBox15,
            this.repositoryItemComboBox16,
            this.repository_cmb_DecisionProcTypes,
            this.repository_cmb_VoteQntyTypes,
            this.repository_cedt_QuotaProcType_Present,
            this.repository_cedt_QuotaProcType_Total,
            this.repositoryItemDateEdit6,
            this.repositoryItemMemoEdit2,
            this.repository_cmb_VoteKinds,
            this.repositoryItemSpinEdit1,
            this.repository_cmb_votetypes,
            this.repository_cmb_ReadNum,
            this.repositoryItemMemoEdit3,
            this.repositoryItemMemoEdit4});
            this.PropertiesControlQuest.RowHeaderWidth = 83;
            this.PropertiesControlQuest.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.cat_sq_Characteristics,
            this.cat_sq_Criteries,
            this.cat_sq_Notes});
            this.PropertiesControlQuest.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlQuest.Size = new System.Drawing.Size(386, 510);
            this.PropertiesControlQuest.TabIndex = 12;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox9
            // 
            this.repositoryItemComboBox9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox9.ImmediatePopup = true;
            this.repositoryItemComboBox9.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox9.Name = "repositoryItemComboBox9";
            // 
            // repositoryItemComboBox10
            // 
            this.repositoryItemComboBox10.AutoHeight = false;
            this.repositoryItemComboBox10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox10.ImmediatePopup = true;
            this.repositoryItemComboBox10.Name = "repositoryItemComboBox10";
            this.repositoryItemComboBox10.Sorted = true;
            // 
            // repositoryItemComboBox11
            // 
            this.repositoryItemComboBox11.AutoHeight = false;
            this.repositoryItemComboBox11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox11.Name = "repositoryItemComboBox11";
            // 
            // repositoryItemComboBox12
            // 
            this.repositoryItemComboBox12.AutoHeight = false;
            this.repositoryItemComboBox12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox12.ImmediatePopup = true;
            this.repositoryItemComboBox12.Name = "repositoryItemComboBox12";
            // 
            // repositoryItemComboBox13
            // 
            this.repositoryItemComboBox13.AutoHeight = false;
            this.repositoryItemComboBox13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox13.Name = "repositoryItemComboBox13";
            // 
            // repositoryItemComboBox14
            // 
            this.repositoryItemComboBox14.AutoHeight = false;
            this.repositoryItemComboBox14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox14.Name = "repositoryItemComboBox14";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemComboBox15
            // 
            this.repositoryItemComboBox15.AutoHeight = false;
            this.repositoryItemComboBox15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox15.Name = "repositoryItemComboBox15";
            // 
            // repositoryItemComboBox16
            // 
            this.repositoryItemComboBox16.AutoHeight = false;
            this.repositoryItemComboBox16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox16.Name = "repositoryItemComboBox16";
            // 
            // repository_cmb_DecisionProcTypes
            // 
            this.repository_cmb_DecisionProcTypes.AutoHeight = false;
            this.repository_cmb_DecisionProcTypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repository_cmb_DecisionProcTypes.Name = "repository_cmb_DecisionProcTypes";
            // 
            // repository_cmb_VoteQntyTypes
            // 
            this.repository_cmb_VoteQntyTypes.AutoHeight = false;
            this.repository_cmb_VoteQntyTypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repository_cmb_VoteQntyTypes.Name = "repository_cmb_VoteQntyTypes";
            // 
            // repository_cedt_QuotaProcType_Present
            // 
            this.repository_cedt_QuotaProcType_Present.AutoHeight = false;
            this.repository_cedt_QuotaProcType_Present.Name = "repository_cedt_QuotaProcType_Present";
            // 
            // repository_cedt_QuotaProcType_Total
            // 
            this.repository_cedt_QuotaProcType_Total.AutoHeight = false;
            this.repository_cedt_QuotaProcType_Total.Name = "repository_cedt_QuotaProcType_Total";
            // 
            // repositoryItemDateEdit6
            // 
            this.repositoryItemDateEdit6.AutoHeight = false;
            this.repositoryItemDateEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit6.Name = "repositoryItemDateEdit6";
            this.repositoryItemDateEdit6.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // repository_cmb_VoteKinds
            // 
            this.repository_cmb_VoteKinds.AutoHeight = false;
            this.repository_cmb_VoteKinds.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repository_cmb_VoteKinds.Name = "repository_cmb_VoteKinds";
            // 
            // repository_cmb_votetypes
            // 
            this.repository_cmb_votetypes.AutoHeight = false;
            this.repository_cmb_votetypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repository_cmb_votetypes.Name = "repository_cmb_votetypes";
            // 
            // repository_cmb_ReadNum
            // 
            this.repository_cmb_ReadNum.AutoHeight = false;
            this.repository_cmb_ReadNum.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repository_cmb_ReadNum.Name = "repository_cmb_ReadNum";
            // 
            // repositoryItemMemoEdit3
            // 
            this.repositoryItemMemoEdit3.Name = "repositoryItemMemoEdit3";
            // 
            // repositoryItemMemoEdit4
            // 
            this.repositoryItemMemoEdit4.Name = "repositoryItemMemoEdit4";
            // 
            // cat_sq_Characteristics
            // 
            this.cat_sq_Characteristics.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_sq_Number,
            this.row_sq_Caption,
            this.row_sq_Description,
            this.row_sq_CrDate,
            this.row_sq_VoteType,
            this.row_sq_VoteKind,
            this.row_sq_ReadNum});
            this.cat_sq_Characteristics.Height = 19;
            this.cat_sq_Characteristics.Name = "cat_sq_Characteristics";
            this.cat_sq_Characteristics.Properties.Caption = "Характеристики";
            // 
            // row_sq_Number
            // 
            this.row_sq_Number.Height = 28;
            this.row_sq_Number.Name = "row_sq_Number";
            this.row_sq_Number.Properties.Caption = "№ вопроса";
            this.row_sq_Number.Properties.ReadOnly = true;
            this.row_sq_Number.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_Caption
            // 
            this.row_sq_Caption.Height = 47;
            this.row_sq_Caption.Name = "row_sq_Caption";
            this.row_sq_Caption.Properties.Caption = "Заголовок";
            this.row_sq_Caption.Properties.ReadOnly = true;
            this.row_sq_Caption.Properties.RowEdit = this.repositoryItemMemoEdit4;
            this.row_sq_Caption.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_Description
            // 
            this.row_sq_Description.Height = 80;
            this.row_sq_Description.Name = "row_sq_Description";
            this.row_sq_Description.Properties.Caption = "Описание";
            this.row_sq_Description.Properties.ReadOnly = true;
            this.row_sq_Description.Properties.RowEdit = this.repositoryItemMemoEdit2;
            this.row_sq_Description.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_CrDate
            // 
            this.row_sq_CrDate.Height = 28;
            this.row_sq_CrDate.Name = "row_sq_CrDate";
            this.row_sq_CrDate.Properties.Caption = "Дата/время создания";
            this.row_sq_CrDate.Properties.ReadOnly = true;
            this.row_sq_CrDate.Properties.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            // 
            // row_sq_VoteType
            // 
            this.row_sq_VoteType.Name = "row_sq_VoteType";
            this.row_sq_VoteType.Properties.Caption = "Тип голосования";
            this.row_sq_VoteType.Properties.ReadOnly = true;
            this.row_sq_VoteType.Properties.RowEdit = this.repository_cmb_votetypes;
            this.row_sq_VoteType.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_VoteKind
            // 
            this.row_sq_VoteKind.Name = "row_sq_VoteKind";
            this.row_sq_VoteKind.Properties.Caption = "Вид голосования";
            this.row_sq_VoteKind.Properties.ReadOnly = true;
            this.row_sq_VoteKind.Properties.RowEdit = this.repository_cmb_VoteKinds;
            this.row_sq_VoteKind.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_ReadNum
            // 
            this.row_sq_ReadNum.Height = 25;
            this.row_sq_ReadNum.Name = "row_sq_ReadNum";
            this.row_sq_ReadNum.Properties.Caption = "Чтение";
            this.row_sq_ReadNum.Properties.ReadOnly = true;
            this.row_sq_ReadNum.Properties.RowEdit = this.repository_cmb_ReadNum;
            // 
            // cat_sq_Criteries
            // 
            this.cat_sq_Criteries.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_sq_DecisionProcValue,
            this.row_sq_QuotaProcentValue});
            this.cat_sq_Criteries.Name = "cat_sq_Criteries";
            this.cat_sq_Criteries.Properties.Caption = "Критерий принятия решения";
            // 
            // row_sq_DecisionProcValue
            // 
            this.row_sq_DecisionProcValue.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_sq_DecisionProcType,
            this.row_sq_VoteQnty});
            this.row_sq_DecisionProcValue.Height = 16;
            this.row_sq_DecisionProcValue.Name = "row_sq_DecisionProcValue";
            multiEditorRowProperties5.Caption = "Порог прин. решения";
            multiEditorRowProperties5.CellWidth = 15;
            multiEditorRowProperties5.RowEdit = this.repositoryItemSpinEdit1;
            multiEditorRowProperties5.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            multiEditorRowProperties5.Width = 139;
            multiEditorRowProperties6.ReadOnly = true;
            multiEditorRowProperties6.UnboundType = DevExpress.Data.UnboundColumnType.String;
            multiEditorRowProperties6.Value = "%";
            multiEditorRowProperties6.Width = 15;
            this.row_sq_DecisionProcValue.PropertiesCollection.AddRange(new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties[] {
            multiEditorRowProperties5,
            multiEditorRowProperties6});
            // 
            // row_sq_DecisionProcType
            // 
            this.row_sq_DecisionProcType.Name = "row_sq_DecisionProcType";
            this.row_sq_DecisionProcType.Properties.Caption = "От числа депутатов";
            this.row_sq_DecisionProcType.Properties.ReadOnly = true;
            this.row_sq_DecisionProcType.Properties.RowEdit = this.repository_cmb_DecisionProcTypes;
            this.row_sq_DecisionProcType.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_VoteQnty
            // 
            this.row_sq_VoteQnty.Name = "row_sq_VoteQnty";
            this.row_sq_VoteQnty.Properties.Caption = "Число голосов";
            this.row_sq_VoteQnty.Properties.ReadOnly = true;
            this.row_sq_VoteQnty.Properties.RowEdit = this.repository_cmb_VoteQntyTypes;
            this.row_sq_VoteQnty.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_QuotaProcentValue
            // 
            this.row_sq_QuotaProcentValue.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_sq_QuotaProcentType_Present,
            this.row_sq_QuotaProcentType_Total});
            this.row_sq_QuotaProcentValue.Name = "row_sq_QuotaProcentValue";
            this.row_sq_QuotaProcentValue.Properties.Caption = "Квота на кворум";
            this.row_sq_QuotaProcentValue.Properties.ReadOnly = true;
            this.row_sq_QuotaProcentValue.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // row_sq_QuotaProcentType_Present
            // 
            this.row_sq_QuotaProcentType_Present.Height = 17;
            this.row_sq_QuotaProcentType_Present.Name = "row_sq_QuotaProcentType_Present";
            this.row_sq_QuotaProcentType_Present.Properties.Caption = "От присутствующих";
            this.row_sq_QuotaProcentType_Present.Properties.RowEdit = this.repository_cedt_QuotaProcType_Present;
            this.row_sq_QuotaProcentType_Present.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            // 
            // row_sq_QuotaProcentType_Total
            // 
            this.row_sq_QuotaProcentType_Total.Name = "row_sq_QuotaProcentType_Total";
            this.row_sq_QuotaProcentType_Total.Properties.Caption = "От общего списка";
            this.row_sq_QuotaProcentType_Total.Properties.RowEdit = this.repository_cedt_QuotaProcType_Total;
            // 
            // cat_sq_Notes
            // 
            this.cat_sq_Notes.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_sq_Notes});
            this.cat_sq_Notes.Name = "cat_sq_Notes";
            this.cat_sq_Notes.Properties.Caption = "Заметки";
            // 
            // row_sq_Notes
            // 
            this.row_sq_Notes.Height = 80;
            this.row_sq_Notes.Name = "row_sq_Notes";
            this.row_sq_Notes.Properties.RowEdit = this.repositoryItemMemoEdit3;
            this.row_sq_Notes.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // pgQuorumGraphic
            // 
            this.pgQuorumGraphic.Controls.Add(this.splitContainerControl2);
            this.pgQuorumGraphic.Controls.Add(this.panelControl2);
            this.pgQuorumGraphic.Name = "pgQuorumGraphic";
            this.pgQuorumGraphic.PageVisible = false;
            this.pgQuorumGraphic.Size = new System.Drawing.Size(1023, 681);
            this.pgQuorumGraphic.Text = "Управление залом";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.groupControl10);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.groupControl3);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1023, 589);
            this.splitContainerControl2.SplitterPosition = 778;
            this.splitContainerControl2.TabIndex = 32;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // groupControl10
            // 
            this.groupControl10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.groupControl10.ContentImage = global::VoteSystem.Properties.Resources.Background2;
            this.groupControl10.ContentImageAlignement = System.Drawing.ContentAlignment.MiddleLeft;
            this.groupControl10.Controls.Add(this.label92);
            this.groupControl10.Controls.Add(this.label15);
            this.groupControl10.Controls.Add(this.label24);
            this.groupControl10.Controls.Add(this.label27);
            this.groupControl10.Controls.Add(this.label36);
            this.groupControl10.Controls.Add(this.label37);
            this.groupControl10.Controls.Add(this.label38);
            this.groupControl10.Controls.Add(this.labelControl97);
            this.groupControl10.Controls.Add(this.pictureEdit75);
            this.groupControl10.Controls.Add(this.labelControl98);
            this.groupControl10.Controls.Add(this.pictureEdit76);
            this.groupControl10.Controls.Add(this.labelControl99);
            this.groupControl10.Controls.Add(this.pictureEdit77);
            this.groupControl10.Controls.Add(this.labelControl100);
            this.groupControl10.Controls.Add(this.pictureEdit78);
            this.groupControl10.Controls.Add(this.labelControl101);
            this.groupControl10.Controls.Add(this.pictureEdit79);
            this.groupControl10.Controls.Add(this.labelControl102);
            this.groupControl10.Controls.Add(this.pictureEdit80);
            this.groupControl10.Controls.Add(this.labelControl103);
            this.groupControl10.Controls.Add(this.pictureEdit81);
            this.groupControl10.Controls.Add(this.labelControl104);
            this.groupControl10.Controls.Add(this.pictureEdit82);
            this.groupControl10.Controls.Add(this.labelControl105);
            this.groupControl10.Controls.Add(this.pictureEdit83);
            this.groupControl10.Controls.Add(this.labelControl106);
            this.groupControl10.Controls.Add(this.pictureEdit84);
            this.groupControl10.Controls.Add(this.labelControl107);
            this.groupControl10.Controls.Add(this.pictureEdit85);
            this.groupControl10.Controls.Add(this.labelControl108);
            this.groupControl10.Controls.Add(this.pictureEdit86);
            this.groupControl10.Controls.Add(this.labelControl109);
            this.groupControl10.Controls.Add(this.pictureEdit87);
            this.groupControl10.Controls.Add(this.labelControl110);
            this.groupControl10.Controls.Add(this.pictureEdit88);
            this.groupControl10.Controls.Add(this.labelControl111);
            this.groupControl10.Controls.Add(this.pictureEdit89);
            this.groupControl10.Controls.Add(this.labelControl112);
            this.groupControl10.Controls.Add(this.pictureEdit90);
            this.groupControl10.Controls.Add(this.labelControl113);
            this.groupControl10.Controls.Add(this.pictureEdit91);
            this.groupControl10.Controls.Add(this.labelControl114);
            this.groupControl10.Controls.Add(this.pictureEdit92);
            this.groupControl10.Controls.Add(this.labelControl115);
            this.groupControl10.Controls.Add(this.pictureEdit93);
            this.groupControl10.Controls.Add(this.labelControl116);
            this.groupControl10.Controls.Add(this.pictureEdit94);
            this.groupControl10.Controls.Add(this.labelControl117);
            this.groupControl10.Controls.Add(this.pictureEdit95);
            this.groupControl10.Controls.Add(this.labelControl118);
            this.groupControl10.Controls.Add(this.pictureEdit96);
            this.groupControl10.Controls.Add(this.labelControl119);
            this.groupControl10.Controls.Add(this.pictureEdit97);
            this.groupControl10.Controls.Add(this.labelControl120);
            this.groupControl10.Controls.Add(this.pictureEdit98);
            this.groupControl10.Controls.Add(this.labelControl121);
            this.groupControl10.Controls.Add(this.pictureEdit99);
            this.groupControl10.Controls.Add(this.labelControl122);
            this.groupControl10.Controls.Add(this.pictureEdit100);
            this.groupControl10.Controls.Add(this.labelControl123);
            this.groupControl10.Controls.Add(this.pictureEdit101);
            this.groupControl10.Controls.Add(this.labelControl124);
            this.groupControl10.Controls.Add(this.pictureEdit102);
            this.groupControl10.Controls.Add(this.labelControl125);
            this.groupControl10.Controls.Add(this.pictureEdit103);
            this.groupControl10.Controls.Add(this.labelControl126);
            this.groupControl10.Controls.Add(this.pictureEdit104);
            this.groupControl10.Controls.Add(this.labelControl127);
            this.groupControl10.Controls.Add(this.pictureEdit105);
            this.groupControl10.Controls.Add(this.labelControl128);
            this.groupControl10.Controls.Add(this.pictureEdit106);
            this.groupControl10.Controls.Add(this.labelControl129);
            this.groupControl10.Controls.Add(this.pictureEdit107);
            this.groupControl10.Controls.Add(this.labelControl130);
            this.groupControl10.Controls.Add(this.pictureEdit108);
            this.groupControl10.Controls.Add(this.labelControl131);
            this.groupControl10.Controls.Add(this.pictureEdit109);
            this.groupControl10.Controls.Add(this.labelControl132);
            this.groupControl10.Controls.Add(this.pictureEdit110);
            this.groupControl10.Controls.Add(this.labelControl133);
            this.groupControl10.Controls.Add(this.pictureEdit111);
            this.groupControl10.Controls.Add(this.labelControl134);
            this.groupControl10.Controls.Add(this.pictureEdit112);
            this.groupControl10.Controls.Add(this.labelControl135);
            this.groupControl10.Controls.Add(this.pictureEdit113);
            this.groupControl10.Controls.Add(this.labelControl136);
            this.groupControl10.Controls.Add(this.pictureEdit114);
            this.groupControl10.Controls.Add(this.labelControl137);
            this.groupControl10.Controls.Add(this.pictureEdit115);
            this.groupControl10.Controls.Add(this.labelControl138);
            this.groupControl10.Controls.Add(this.pictureEdit116);
            this.groupControl10.Controls.Add(this.labelControl139);
            this.groupControl10.Controls.Add(this.pictureEdit117);
            this.groupControl10.Controls.Add(this.labelControl140);
            this.groupControl10.Controls.Add(this.pictureEdit118);
            this.groupControl10.Controls.Add(this.labelControl141);
            this.groupControl10.Controls.Add(this.pictureEdit119);
            this.groupControl10.Controls.Add(this.labelControl142);
            this.groupControl10.Controls.Add(this.pictureEdit120);
            this.groupControl10.Controls.Add(this.labelControl143);
            this.groupControl10.Controls.Add(this.pictureEdit121);
            this.groupControl10.Controls.Add(this.labelControl144);
            this.groupControl10.Controls.Add(this.pictureEdit122);
            this.groupControl10.Controls.Add(this.labelControl81);
            this.groupControl10.Controls.Add(this.pictureEdit59);
            this.groupControl10.Controls.Add(this.labelControl82);
            this.groupControl10.Controls.Add(this.pictureEdit60);
            this.groupControl10.Controls.Add(this.labelControl83);
            this.groupControl10.Controls.Add(this.pictureEdit61);
            this.groupControl10.Controls.Add(this.labelControl84);
            this.groupControl10.Controls.Add(this.pictureEdit62);
            this.groupControl10.Controls.Add(this.labelControl85);
            this.groupControl10.Controls.Add(this.pictureEdit63);
            this.groupControl10.Controls.Add(this.labelControl86);
            this.groupControl10.Controls.Add(this.pictureEdit64);
            this.groupControl10.Controls.Add(this.labelControl87);
            this.groupControl10.Controls.Add(this.pictureEdit65);
            this.groupControl10.Controls.Add(this.labelControl88);
            this.groupControl10.Controls.Add(this.pictureEdit66);
            this.groupControl10.Controls.Add(this.labelControl89);
            this.groupControl10.Controls.Add(this.pictureEdit67);
            this.groupControl10.Controls.Add(this.labelControl90);
            this.groupControl10.Controls.Add(this.pictureEdit68);
            this.groupControl10.Controls.Add(this.labelControl91);
            this.groupControl10.Controls.Add(this.pictureEdit69);
            this.groupControl10.Controls.Add(this.labelControl92);
            this.groupControl10.Controls.Add(this.pictureEdit70);
            this.groupControl10.Controls.Add(this.labelControl93);
            this.groupControl10.Controls.Add(this.pictureEdit71);
            this.groupControl10.Controls.Add(this.labelControl94);
            this.groupControl10.Controls.Add(this.pictureEdit72);
            this.groupControl10.Controls.Add(this.labelControl95);
            this.groupControl10.Controls.Add(this.pictureEdit73);
            this.groupControl10.Controls.Add(this.labelControl96);
            this.groupControl10.Controls.Add(this.pictureEdit74);
            this.groupControl10.Controls.Add(this.labelControl65);
            this.groupControl10.Controls.Add(this.pictureEdit43);
            this.groupControl10.Controls.Add(this.labelControl66);
            this.groupControl10.Controls.Add(this.pictureEdit44);
            this.groupControl10.Controls.Add(this.labelControl67);
            this.groupControl10.Controls.Add(this.pictureEdit45);
            this.groupControl10.Controls.Add(this.labelControl68);
            this.groupControl10.Controls.Add(this.pictureEdit46);
            this.groupControl10.Controls.Add(this.labelControl69);
            this.groupControl10.Controls.Add(this.pictureEdit47);
            this.groupControl10.Controls.Add(this.labelControl70);
            this.groupControl10.Controls.Add(this.pictureEdit48);
            this.groupControl10.Controls.Add(this.labelControl71);
            this.groupControl10.Controls.Add(this.pictureEdit49);
            this.groupControl10.Controls.Add(this.labelControl72);
            this.groupControl10.Controls.Add(this.pictureEdit50);
            this.groupControl10.Controls.Add(this.labelControl73);
            this.groupControl10.Controls.Add(this.pictureEdit51);
            this.groupControl10.Controls.Add(this.labelControl74);
            this.groupControl10.Controls.Add(this.pictureEdit52);
            this.groupControl10.Controls.Add(this.labelControl75);
            this.groupControl10.Controls.Add(this.pictureEdit53);
            this.groupControl10.Controls.Add(this.labelControl76);
            this.groupControl10.Controls.Add(this.pictureEdit54);
            this.groupControl10.Controls.Add(this.labelControl77);
            this.groupControl10.Controls.Add(this.pictureEdit55);
            this.groupControl10.Controls.Add(this.labelControl78);
            this.groupControl10.Controls.Add(this.pictureEdit56);
            this.groupControl10.Controls.Add(this.labelControl79);
            this.groupControl10.Controls.Add(this.pictureEdit57);
            this.groupControl10.Controls.Add(this.labelControl80);
            this.groupControl10.Controls.Add(this.pictureEdit58);
            this.groupControl10.Controls.Add(this.labelControl49);
            this.groupControl10.Controls.Add(this.labelControl41);
            this.groupControl10.Controls.Add(this.pictureEdit27);
            this.groupControl10.Controls.Add(this.pictureEdit19);
            this.groupControl10.Controls.Add(this.labelControl50);
            this.groupControl10.Controls.Add(this.labelControl42);
            this.groupControl10.Controls.Add(this.pictureEdit28);
            this.groupControl10.Controls.Add(this.pictureEdit20);
            this.groupControl10.Controls.Add(this.labelControl51);
            this.groupControl10.Controls.Add(this.labelControl43);
            this.groupControl10.Controls.Add(this.pictureEdit29);
            this.groupControl10.Controls.Add(this.pictureEdit21);
            this.groupControl10.Controls.Add(this.labelControl52);
            this.groupControl10.Controls.Add(this.labelControl44);
            this.groupControl10.Controls.Add(this.pictureEdit30);
            this.groupControl10.Controls.Add(this.pictureEdit22);
            this.groupControl10.Controls.Add(this.labelControl53);
            this.groupControl10.Controls.Add(this.labelControl45);
            this.groupControl10.Controls.Add(this.pictureEdit31);
            this.groupControl10.Controls.Add(this.pictureEdit23);
            this.groupControl10.Controls.Add(this.labelControl54);
            this.groupControl10.Controls.Add(this.labelControl46);
            this.groupControl10.Controls.Add(this.pictureEdit32);
            this.groupControl10.Controls.Add(this.pictureEdit24);
            this.groupControl10.Controls.Add(this.labelControl55);
            this.groupControl10.Controls.Add(this.labelControl47);
            this.groupControl10.Controls.Add(this.pictureEdit33);
            this.groupControl10.Controls.Add(this.pictureEdit25);
            this.groupControl10.Controls.Add(this.labelControl56);
            this.groupControl10.Controls.Add(this.labelControl48);
            this.groupControl10.Controls.Add(this.pictureEdit34);
            this.groupControl10.Controls.Add(this.pictureEdit26);
            this.groupControl10.Controls.Add(this.labelControl57);
            this.groupControl10.Controls.Add(this.labelControl8);
            this.groupControl10.Controls.Add(this.pictureEdit35);
            this.groupControl10.Controls.Add(this.pictureEdit1);
            this.groupControl10.Controls.Add(this.labelControl58);
            this.groupControl10.Controls.Add(this.labelControl9);
            this.groupControl10.Controls.Add(this.pictureEdit36);
            this.groupControl10.Controls.Add(this.pictureEdit2);
            this.groupControl10.Controls.Add(this.labelControl59);
            this.groupControl10.Controls.Add(this.labelControl10);
            this.groupControl10.Controls.Add(this.pictureEdit37);
            this.groupControl10.Controls.Add(this.pictureEdit3);
            this.groupControl10.Controls.Add(this.labelControl60);
            this.groupControl10.Controls.Add(this.labelControl11);
            this.groupControl10.Controls.Add(this.pictureEdit38);
            this.groupControl10.Controls.Add(this.pictureEdit4);
            this.groupControl10.Controls.Add(this.labelControl61);
            this.groupControl10.Controls.Add(this.labelControl12);
            this.groupControl10.Controls.Add(this.pictureEdit39);
            this.groupControl10.Controls.Add(this.pictureEdit5);
            this.groupControl10.Controls.Add(this.labelControl62);
            this.groupControl10.Controls.Add(this.labelControl13);
            this.groupControl10.Controls.Add(this.pictureEdit40);
            this.groupControl10.Controls.Add(this.pictureEdit6);
            this.groupControl10.Controls.Add(this.labelControl63);
            this.groupControl10.Controls.Add(this.labelControl14);
            this.groupControl10.Controls.Add(this.pictureEdit41);
            this.groupControl10.Controls.Add(this.Mic_2);
            this.groupControl10.Controls.Add(this.labelControl64);
            this.groupControl10.Controls.Add(this.labelControl15);
            this.groupControl10.Controls.Add(this.pictureEdit42);
            this.groupControl10.Controls.Add(this.Mic_1);
            this.groupControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl10.Location = new System.Drawing.Point(0, 0);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(774, 585);
            this.groupControl10.TabIndex = 0;
            this.groupControl10.Text = "Определение кворума";
            this.groupControl10.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label92.Location = new System.Drawing.Point(18, 442);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(43, 16);
            this.label92.TabIndex = 277;
            this.label92.Text = "Ряд 6";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(18, 377);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 16);
            this.label15.TabIndex = 276;
            this.label15.Text = "Ряд 5";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(18, 318);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 16);
            this.label24.TabIndex = 275;
            this.label24.Text = "Ряд 4";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(18, 259);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 16);
            this.label27.TabIndex = 274;
            this.label27.Text = "Ряд 3";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(17, 195);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(43, 16);
            this.label36.TabIndex = 273;
            this.label36.Text = "Ряд 2";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(18, 135);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 16);
            this.label37.TabIndex = 272;
            this.label37.Text = "Ряд 1";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(5, 51);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(81, 16);
            this.label38.TabIndex = 271;
            this.label38.Text = "Президиум";
            // 
            // labelControl97
            // 
            this.labelControl97.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl97.Appearance.Options.UseFont = true;
            this.labelControl97.Location = new System.Drawing.Point(725, 460);
            this.labelControl97.Name = "labelControl97";
            this.labelControl97.Size = new System.Drawing.Size(18, 15);
            this.labelControl97.TabIndex = 270;
            this.labelControl97.Text = "112";
            // 
            // labelControl98
            // 
            this.labelControl98.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl98.Appearance.Options.UseFont = true;
            this.labelControl98.Location = new System.Drawing.Point(685, 460);
            this.labelControl98.Name = "labelControl98";
            this.labelControl98.Size = new System.Drawing.Size(18, 15);
            this.labelControl98.TabIndex = 268;
            this.labelControl98.Text = "111";
            // 
            // labelControl99
            // 
            this.labelControl99.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl99.Appearance.Options.UseFont = true;
            this.labelControl99.Location = new System.Drawing.Point(647, 459);
            this.labelControl99.Name = "labelControl99";
            this.labelControl99.Size = new System.Drawing.Size(18, 15);
            this.labelControl99.TabIndex = 266;
            this.labelControl99.Text = "110";
            // 
            // labelControl100
            // 
            this.labelControl100.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl100.Appearance.Options.UseFont = true;
            this.labelControl100.Location = new System.Drawing.Point(608, 460);
            this.labelControl100.Name = "labelControl100";
            this.labelControl100.Size = new System.Drawing.Size(18, 15);
            this.labelControl100.TabIndex = 264;
            this.labelControl100.Text = "109";
            // 
            // labelControl101
            // 
            this.labelControl101.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl101.Appearance.Options.UseFont = true;
            this.labelControl101.Location = new System.Drawing.Point(569, 459);
            this.labelControl101.Name = "labelControl101";
            this.labelControl101.Size = new System.Drawing.Size(18, 15);
            this.labelControl101.TabIndex = 262;
            this.labelControl101.Text = "108";
            // 
            // labelControl102
            // 
            this.labelControl102.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl102.Appearance.Options.UseFont = true;
            this.labelControl102.Location = new System.Drawing.Point(529, 460);
            this.labelControl102.Name = "labelControl102";
            this.labelControl102.Size = new System.Drawing.Size(18, 15);
            this.labelControl102.TabIndex = 260;
            this.labelControl102.Text = "107";
            // 
            // labelControl103
            // 
            this.labelControl103.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl103.Appearance.Options.UseFont = true;
            this.labelControl103.Location = new System.Drawing.Point(491, 460);
            this.labelControl103.Name = "labelControl103";
            this.labelControl103.Size = new System.Drawing.Size(18, 15);
            this.labelControl103.TabIndex = 258;
            this.labelControl103.Text = "106";
            // 
            // labelControl104
            // 
            this.labelControl104.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl104.Appearance.Options.UseFont = true;
            this.labelControl104.Location = new System.Drawing.Point(452, 459);
            this.labelControl104.Name = "labelControl104";
            this.labelControl104.Size = new System.Drawing.Size(18, 15);
            this.labelControl104.TabIndex = 256;
            this.labelControl104.Text = "105";
            // 
            // labelControl105
            // 
            this.labelControl105.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl105.Appearance.Options.UseFont = true;
            this.labelControl105.Location = new System.Drawing.Point(393, 460);
            this.labelControl105.Name = "labelControl105";
            this.labelControl105.Size = new System.Drawing.Size(18, 15);
            this.labelControl105.TabIndex = 254;
            this.labelControl105.Text = "104";
            // 
            // labelControl106
            // 
            this.labelControl106.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl106.Appearance.Options.UseFont = true;
            this.labelControl106.Location = new System.Drawing.Point(353, 460);
            this.labelControl106.Name = "labelControl106";
            this.labelControl106.Size = new System.Drawing.Size(18, 15);
            this.labelControl106.TabIndex = 252;
            this.labelControl106.Text = "103";
            // 
            // labelControl107
            // 
            this.labelControl107.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl107.Appearance.Options.UseFont = true;
            this.labelControl107.Location = new System.Drawing.Point(315, 459);
            this.labelControl107.Name = "labelControl107";
            this.labelControl107.Size = new System.Drawing.Size(18, 15);
            this.labelControl107.TabIndex = 250;
            this.labelControl107.Text = "102";
            // 
            // labelControl108
            // 
            this.labelControl108.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl108.Appearance.Options.UseFont = true;
            this.labelControl108.Location = new System.Drawing.Point(276, 460);
            this.labelControl108.Name = "labelControl108";
            this.labelControl108.Size = new System.Drawing.Size(18, 15);
            this.labelControl108.TabIndex = 248;
            this.labelControl108.Text = "101";
            // 
            // labelControl109
            // 
            this.labelControl109.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl109.Appearance.Options.UseFont = true;
            this.labelControl109.Location = new System.Drawing.Point(237, 459);
            this.labelControl109.Name = "labelControl109";
            this.labelControl109.Size = new System.Drawing.Size(18, 15);
            this.labelControl109.TabIndex = 246;
            this.labelControl109.Text = "100";
            // 
            // labelControl110
            // 
            this.labelControl110.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl110.Appearance.Options.UseFont = true;
            this.labelControl110.Location = new System.Drawing.Point(197, 460);
            this.labelControl110.Name = "labelControl110";
            this.labelControl110.Size = new System.Drawing.Size(12, 15);
            this.labelControl110.TabIndex = 244;
            this.labelControl110.Text = "99";
            // 
            // labelControl111
            // 
            this.labelControl111.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl111.Appearance.Options.UseFont = true;
            this.labelControl111.Location = new System.Drawing.Point(159, 460);
            this.labelControl111.Name = "labelControl111";
            this.labelControl111.Size = new System.Drawing.Size(12, 15);
            this.labelControl111.TabIndex = 242;
            this.labelControl111.Text = "98";
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl112.Appearance.Options.UseFont = true;
            this.labelControl112.Location = new System.Drawing.Point(120, 459);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(12, 15);
            this.labelControl112.TabIndex = 240;
            this.labelControl112.Text = "97";
            // 
            // labelControl113
            // 
            this.labelControl113.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl113.Appearance.Options.UseFont = true;
            this.labelControl113.Location = new System.Drawing.Point(725, 396);
            this.labelControl113.Name = "labelControl113";
            this.labelControl113.Size = new System.Drawing.Size(12, 15);
            this.labelControl113.TabIndex = 238;
            this.labelControl113.Text = "96";
            // 
            // labelControl114
            // 
            this.labelControl114.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl114.Appearance.Options.UseFont = true;
            this.labelControl114.Location = new System.Drawing.Point(685, 396);
            this.labelControl114.Name = "labelControl114";
            this.labelControl114.Size = new System.Drawing.Size(12, 15);
            this.labelControl114.TabIndex = 236;
            this.labelControl114.Text = "95";
            // 
            // labelControl115
            // 
            this.labelControl115.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl115.Appearance.Options.UseFont = true;
            this.labelControl115.Location = new System.Drawing.Point(647, 395);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(12, 15);
            this.labelControl115.TabIndex = 234;
            this.labelControl115.Text = "94";
            // 
            // labelControl116
            // 
            this.labelControl116.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl116.Appearance.Options.UseFont = true;
            this.labelControl116.Location = new System.Drawing.Point(608, 396);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(12, 15);
            this.labelControl116.TabIndex = 232;
            this.labelControl116.Text = "93";
            // 
            // labelControl117
            // 
            this.labelControl117.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl117.Appearance.Options.UseFont = true;
            this.labelControl117.Location = new System.Drawing.Point(569, 395);
            this.labelControl117.Name = "labelControl117";
            this.labelControl117.Size = new System.Drawing.Size(12, 15);
            this.labelControl117.TabIndex = 230;
            this.labelControl117.Text = "92";
            // 
            // labelControl118
            // 
            this.labelControl118.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl118.Appearance.Options.UseFont = true;
            this.labelControl118.Location = new System.Drawing.Point(529, 396);
            this.labelControl118.Name = "labelControl118";
            this.labelControl118.Size = new System.Drawing.Size(12, 15);
            this.labelControl118.TabIndex = 228;
            this.labelControl118.Text = "91";
            // 
            // labelControl119
            // 
            this.labelControl119.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl119.Appearance.Options.UseFont = true;
            this.labelControl119.Location = new System.Drawing.Point(491, 396);
            this.labelControl119.Name = "labelControl119";
            this.labelControl119.Size = new System.Drawing.Size(12, 15);
            this.labelControl119.TabIndex = 226;
            this.labelControl119.Text = "90";
            // 
            // labelControl120
            // 
            this.labelControl120.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl120.Appearance.Options.UseFont = true;
            this.labelControl120.Location = new System.Drawing.Point(452, 395);
            this.labelControl120.Name = "labelControl120";
            this.labelControl120.Size = new System.Drawing.Size(12, 15);
            this.labelControl120.TabIndex = 224;
            this.labelControl120.Text = "89";
            // 
            // labelControl121
            // 
            this.labelControl121.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl121.Appearance.Options.UseFont = true;
            this.labelControl121.Location = new System.Drawing.Point(393, 396);
            this.labelControl121.Name = "labelControl121";
            this.labelControl121.Size = new System.Drawing.Size(12, 15);
            this.labelControl121.TabIndex = 222;
            this.labelControl121.Text = "88";
            // 
            // labelControl122
            // 
            this.labelControl122.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl122.Appearance.Options.UseFont = true;
            this.labelControl122.Location = new System.Drawing.Point(353, 396);
            this.labelControl122.Name = "labelControl122";
            this.labelControl122.Size = new System.Drawing.Size(12, 15);
            this.labelControl122.TabIndex = 220;
            this.labelControl122.Text = "87";
            // 
            // labelControl123
            // 
            this.labelControl123.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl123.Appearance.Options.UseFont = true;
            this.labelControl123.Location = new System.Drawing.Point(315, 395);
            this.labelControl123.Name = "labelControl123";
            this.labelControl123.Size = new System.Drawing.Size(12, 15);
            this.labelControl123.TabIndex = 218;
            this.labelControl123.Text = "86";
            // 
            // labelControl124
            // 
            this.labelControl124.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl124.Appearance.Options.UseFont = true;
            this.labelControl124.Location = new System.Drawing.Point(276, 396);
            this.labelControl124.Name = "labelControl124";
            this.labelControl124.Size = new System.Drawing.Size(12, 15);
            this.labelControl124.TabIndex = 216;
            this.labelControl124.Text = "85";
            // 
            // labelControl125
            // 
            this.labelControl125.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl125.Appearance.Options.UseFont = true;
            this.labelControl125.Location = new System.Drawing.Point(237, 395);
            this.labelControl125.Name = "labelControl125";
            this.labelControl125.Size = new System.Drawing.Size(12, 15);
            this.labelControl125.TabIndex = 214;
            this.labelControl125.Text = "84";
            // 
            // labelControl126
            // 
            this.labelControl126.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl126.Appearance.Options.UseFont = true;
            this.labelControl126.Location = new System.Drawing.Point(197, 396);
            this.labelControl126.Name = "labelControl126";
            this.labelControl126.Size = new System.Drawing.Size(12, 15);
            this.labelControl126.TabIndex = 212;
            this.labelControl126.Text = "83";
            // 
            // labelControl127
            // 
            this.labelControl127.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl127.Appearance.Options.UseFont = true;
            this.labelControl127.Location = new System.Drawing.Point(159, 396);
            this.labelControl127.Name = "labelControl127";
            this.labelControl127.Size = new System.Drawing.Size(12, 15);
            this.labelControl127.TabIndex = 210;
            this.labelControl127.Text = "82";
            // 
            // labelControl128
            // 
            this.labelControl128.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl128.Appearance.Options.UseFont = true;
            this.labelControl128.Location = new System.Drawing.Point(120, 395);
            this.labelControl128.Name = "labelControl128";
            this.labelControl128.Size = new System.Drawing.Size(12, 15);
            this.labelControl128.TabIndex = 208;
            this.labelControl128.Text = "81";
            // 
            // labelControl129
            // 
            this.labelControl129.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl129.Appearance.Options.UseFont = true;
            this.labelControl129.Location = new System.Drawing.Point(725, 337);
            this.labelControl129.Name = "labelControl129";
            this.labelControl129.Size = new System.Drawing.Size(12, 15);
            this.labelControl129.TabIndex = 206;
            this.labelControl129.Text = "80";
            // 
            // labelControl130
            // 
            this.labelControl130.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl130.Appearance.Options.UseFont = true;
            this.labelControl130.Location = new System.Drawing.Point(685, 337);
            this.labelControl130.Name = "labelControl130";
            this.labelControl130.Size = new System.Drawing.Size(12, 15);
            this.labelControl130.TabIndex = 204;
            this.labelControl130.Text = "79";
            // 
            // labelControl131
            // 
            this.labelControl131.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl131.Appearance.Options.UseFont = true;
            this.labelControl131.Location = new System.Drawing.Point(647, 336);
            this.labelControl131.Name = "labelControl131";
            this.labelControl131.Size = new System.Drawing.Size(12, 15);
            this.labelControl131.TabIndex = 202;
            this.labelControl131.Text = "78";
            // 
            // labelControl132
            // 
            this.labelControl132.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl132.Appearance.Options.UseFont = true;
            this.labelControl132.Location = new System.Drawing.Point(608, 337);
            this.labelControl132.Name = "labelControl132";
            this.labelControl132.Size = new System.Drawing.Size(12, 15);
            this.labelControl132.TabIndex = 200;
            this.labelControl132.Text = "77";
            // 
            // labelControl133
            // 
            this.labelControl133.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl133.Appearance.Options.UseFont = true;
            this.labelControl133.Location = new System.Drawing.Point(569, 336);
            this.labelControl133.Name = "labelControl133";
            this.labelControl133.Size = new System.Drawing.Size(12, 15);
            this.labelControl133.TabIndex = 198;
            this.labelControl133.Text = "76";
            // 
            // labelControl134
            // 
            this.labelControl134.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl134.Appearance.Options.UseFont = true;
            this.labelControl134.Location = new System.Drawing.Point(529, 337);
            this.labelControl134.Name = "labelControl134";
            this.labelControl134.Size = new System.Drawing.Size(12, 15);
            this.labelControl134.TabIndex = 196;
            this.labelControl134.Text = "75";
            // 
            // labelControl135
            // 
            this.labelControl135.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl135.Appearance.Options.UseFont = true;
            this.labelControl135.Location = new System.Drawing.Point(491, 337);
            this.labelControl135.Name = "labelControl135";
            this.labelControl135.Size = new System.Drawing.Size(12, 15);
            this.labelControl135.TabIndex = 194;
            this.labelControl135.Text = "74";
            // 
            // labelControl136
            // 
            this.labelControl136.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl136.Appearance.Options.UseFont = true;
            this.labelControl136.Location = new System.Drawing.Point(452, 336);
            this.labelControl136.Name = "labelControl136";
            this.labelControl136.Size = new System.Drawing.Size(12, 15);
            this.labelControl136.TabIndex = 192;
            this.labelControl136.Text = "73";
            // 
            // labelControl137
            // 
            this.labelControl137.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl137.Appearance.Options.UseFont = true;
            this.labelControl137.Location = new System.Drawing.Point(393, 337);
            this.labelControl137.Name = "labelControl137";
            this.labelControl137.Size = new System.Drawing.Size(12, 15);
            this.labelControl137.TabIndex = 190;
            this.labelControl137.Text = "72";
            // 
            // labelControl138
            // 
            this.labelControl138.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl138.Appearance.Options.UseFont = true;
            this.labelControl138.Location = new System.Drawing.Point(353, 337);
            this.labelControl138.Name = "labelControl138";
            this.labelControl138.Size = new System.Drawing.Size(12, 15);
            this.labelControl138.TabIndex = 188;
            this.labelControl138.Text = "71";
            // 
            // labelControl139
            // 
            this.labelControl139.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl139.Appearance.Options.UseFont = true;
            this.labelControl139.Location = new System.Drawing.Point(315, 336);
            this.labelControl139.Name = "labelControl139";
            this.labelControl139.Size = new System.Drawing.Size(12, 15);
            this.labelControl139.TabIndex = 186;
            this.labelControl139.Text = "70";
            // 
            // labelControl140
            // 
            this.labelControl140.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl140.Appearance.Options.UseFont = true;
            this.labelControl140.Location = new System.Drawing.Point(276, 337);
            this.labelControl140.Name = "labelControl140";
            this.labelControl140.Size = new System.Drawing.Size(12, 15);
            this.labelControl140.TabIndex = 184;
            this.labelControl140.Text = "69";
            // 
            // labelControl141
            // 
            this.labelControl141.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl141.Appearance.Options.UseFont = true;
            this.labelControl141.Location = new System.Drawing.Point(237, 336);
            this.labelControl141.Name = "labelControl141";
            this.labelControl141.Size = new System.Drawing.Size(12, 15);
            this.labelControl141.TabIndex = 182;
            this.labelControl141.Text = "68";
            // 
            // labelControl142
            // 
            this.labelControl142.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl142.Appearance.Options.UseFont = true;
            this.labelControl142.Location = new System.Drawing.Point(197, 337);
            this.labelControl142.Name = "labelControl142";
            this.labelControl142.Size = new System.Drawing.Size(12, 15);
            this.labelControl142.TabIndex = 180;
            this.labelControl142.Text = "67";
            // 
            // labelControl143
            // 
            this.labelControl143.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl143.Appearance.Options.UseFont = true;
            this.labelControl143.Location = new System.Drawing.Point(159, 337);
            this.labelControl143.Name = "labelControl143";
            this.labelControl143.Size = new System.Drawing.Size(12, 15);
            this.labelControl143.TabIndex = 178;
            this.labelControl143.Text = "66";
            // 
            // labelControl144
            // 
            this.labelControl144.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl144.Appearance.Options.UseFont = true;
            this.labelControl144.Location = new System.Drawing.Point(120, 336);
            this.labelControl144.Name = "labelControl144";
            this.labelControl144.Size = new System.Drawing.Size(12, 15);
            this.labelControl144.TabIndex = 176;
            this.labelControl144.Text = "65";
            // 
            // labelControl81
            // 
            this.labelControl81.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl81.Appearance.Options.UseFont = true;
            this.labelControl81.Location = new System.Drawing.Point(725, 279);
            this.labelControl81.Name = "labelControl81";
            this.labelControl81.Size = new System.Drawing.Size(12, 15);
            this.labelControl81.TabIndex = 174;
            this.labelControl81.Text = "64";
            // 
            // labelControl82
            // 
            this.labelControl82.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl82.Appearance.Options.UseFont = true;
            this.labelControl82.Location = new System.Drawing.Point(685, 279);
            this.labelControl82.Name = "labelControl82";
            this.labelControl82.Size = new System.Drawing.Size(12, 15);
            this.labelControl82.TabIndex = 172;
            this.labelControl82.Text = "63";
            // 
            // labelControl83
            // 
            this.labelControl83.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl83.Appearance.Options.UseFont = true;
            this.labelControl83.Location = new System.Drawing.Point(647, 278);
            this.labelControl83.Name = "labelControl83";
            this.labelControl83.Size = new System.Drawing.Size(12, 15);
            this.labelControl83.TabIndex = 170;
            this.labelControl83.Text = "62";
            // 
            // labelControl84
            // 
            this.labelControl84.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl84.Appearance.Options.UseFont = true;
            this.labelControl84.Location = new System.Drawing.Point(608, 279);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(12, 15);
            this.labelControl84.TabIndex = 168;
            this.labelControl84.Text = "61";
            // 
            // labelControl85
            // 
            this.labelControl85.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl85.Appearance.Options.UseFont = true;
            this.labelControl85.Location = new System.Drawing.Point(569, 278);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(12, 15);
            this.labelControl85.TabIndex = 166;
            this.labelControl85.Text = "60";
            // 
            // labelControl86
            // 
            this.labelControl86.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl86.Appearance.Options.UseFont = true;
            this.labelControl86.Location = new System.Drawing.Point(529, 279);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(12, 15);
            this.labelControl86.TabIndex = 164;
            this.labelControl86.Text = "59";
            // 
            // labelControl87
            // 
            this.labelControl87.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl87.Appearance.Options.UseFont = true;
            this.labelControl87.Location = new System.Drawing.Point(491, 279);
            this.labelControl87.Name = "labelControl87";
            this.labelControl87.Size = new System.Drawing.Size(12, 15);
            this.labelControl87.TabIndex = 162;
            this.labelControl87.Text = "58";
            // 
            // labelControl88
            // 
            this.labelControl88.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl88.Appearance.Options.UseFont = true;
            this.labelControl88.Location = new System.Drawing.Point(452, 278);
            this.labelControl88.Name = "labelControl88";
            this.labelControl88.Size = new System.Drawing.Size(12, 15);
            this.labelControl88.TabIndex = 160;
            this.labelControl88.Text = "57";
            // 
            // labelControl89
            // 
            this.labelControl89.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl89.Appearance.Options.UseFont = true;
            this.labelControl89.Location = new System.Drawing.Point(393, 279);
            this.labelControl89.Name = "labelControl89";
            this.labelControl89.Size = new System.Drawing.Size(12, 15);
            this.labelControl89.TabIndex = 158;
            this.labelControl89.Text = "56";
            // 
            // labelControl90
            // 
            this.labelControl90.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl90.Appearance.Options.UseFont = true;
            this.labelControl90.Location = new System.Drawing.Point(353, 279);
            this.labelControl90.Name = "labelControl90";
            this.labelControl90.Size = new System.Drawing.Size(12, 15);
            this.labelControl90.TabIndex = 156;
            this.labelControl90.Text = "55";
            // 
            // labelControl91
            // 
            this.labelControl91.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl91.Appearance.Options.UseFont = true;
            this.labelControl91.Location = new System.Drawing.Point(315, 278);
            this.labelControl91.Name = "labelControl91";
            this.labelControl91.Size = new System.Drawing.Size(12, 15);
            this.labelControl91.TabIndex = 154;
            this.labelControl91.Text = "54";
            // 
            // labelControl92
            // 
            this.labelControl92.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl92.Appearance.Options.UseFont = true;
            this.labelControl92.Location = new System.Drawing.Point(276, 279);
            this.labelControl92.Name = "labelControl92";
            this.labelControl92.Size = new System.Drawing.Size(12, 15);
            this.labelControl92.TabIndex = 152;
            this.labelControl92.Text = "53";
            // 
            // labelControl93
            // 
            this.labelControl93.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl93.Appearance.Options.UseFont = true;
            this.labelControl93.Location = new System.Drawing.Point(237, 278);
            this.labelControl93.Name = "labelControl93";
            this.labelControl93.Size = new System.Drawing.Size(12, 15);
            this.labelControl93.TabIndex = 150;
            this.labelControl93.Text = "52";
            // 
            // labelControl94
            // 
            this.labelControl94.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl94.Appearance.Options.UseFont = true;
            this.labelControl94.Location = new System.Drawing.Point(197, 279);
            this.labelControl94.Name = "labelControl94";
            this.labelControl94.Size = new System.Drawing.Size(12, 15);
            this.labelControl94.TabIndex = 148;
            this.labelControl94.Text = "51";
            // 
            // labelControl95
            // 
            this.labelControl95.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl95.Appearance.Options.UseFont = true;
            this.labelControl95.Location = new System.Drawing.Point(159, 279);
            this.labelControl95.Name = "labelControl95";
            this.labelControl95.Size = new System.Drawing.Size(12, 15);
            this.labelControl95.TabIndex = 146;
            this.labelControl95.Text = "50";
            // 
            // labelControl96
            // 
            this.labelControl96.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl96.Appearance.Options.UseFont = true;
            this.labelControl96.Location = new System.Drawing.Point(120, 278);
            this.labelControl96.Name = "labelControl96";
            this.labelControl96.Size = new System.Drawing.Size(12, 15);
            this.labelControl96.TabIndex = 144;
            this.labelControl96.Text = "49";
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl65.Appearance.Options.UseFont = true;
            this.labelControl65.Location = new System.Drawing.Point(725, 215);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(12, 15);
            this.labelControl65.TabIndex = 142;
            this.labelControl65.Text = "48";
            // 
            // labelControl66
            // 
            this.labelControl66.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl66.Appearance.Options.UseFont = true;
            this.labelControl66.Location = new System.Drawing.Point(685, 215);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(12, 15);
            this.labelControl66.TabIndex = 140;
            this.labelControl66.Text = "47";
            // 
            // labelControl67
            // 
            this.labelControl67.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl67.Appearance.Options.UseFont = true;
            this.labelControl67.Location = new System.Drawing.Point(647, 214);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(12, 15);
            this.labelControl67.TabIndex = 138;
            this.labelControl67.Text = "46";
            // 
            // labelControl68
            // 
            this.labelControl68.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl68.Appearance.Options.UseFont = true;
            this.labelControl68.Location = new System.Drawing.Point(608, 215);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(12, 15);
            this.labelControl68.TabIndex = 136;
            this.labelControl68.Text = "45";
            // 
            // labelControl69
            // 
            this.labelControl69.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl69.Appearance.Options.UseFont = true;
            this.labelControl69.Location = new System.Drawing.Point(569, 214);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(12, 15);
            this.labelControl69.TabIndex = 134;
            this.labelControl69.Text = "44";
            // 
            // labelControl70
            // 
            this.labelControl70.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl70.Appearance.Options.UseFont = true;
            this.labelControl70.Location = new System.Drawing.Point(529, 215);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(12, 15);
            this.labelControl70.TabIndex = 132;
            this.labelControl70.Text = "43";
            // 
            // labelControl71
            // 
            this.labelControl71.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl71.Appearance.Options.UseFont = true;
            this.labelControl71.Location = new System.Drawing.Point(491, 215);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(12, 15);
            this.labelControl71.TabIndex = 130;
            this.labelControl71.Text = "42";
            // 
            // labelControl72
            // 
            this.labelControl72.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl72.Appearance.Options.UseFont = true;
            this.labelControl72.Location = new System.Drawing.Point(452, 214);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(12, 15);
            this.labelControl72.TabIndex = 128;
            this.labelControl72.Text = "41";
            // 
            // labelControl73
            // 
            this.labelControl73.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl73.Appearance.Options.UseFont = true;
            this.labelControl73.Location = new System.Drawing.Point(393, 215);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(12, 15);
            this.labelControl73.TabIndex = 126;
            this.labelControl73.Text = "40";
            // 
            // labelControl74
            // 
            this.labelControl74.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl74.Appearance.Options.UseFont = true;
            this.labelControl74.Location = new System.Drawing.Point(353, 215);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(12, 15);
            this.labelControl74.TabIndex = 124;
            this.labelControl74.Text = "39";
            // 
            // labelControl75
            // 
            this.labelControl75.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl75.Appearance.Options.UseFont = true;
            this.labelControl75.Location = new System.Drawing.Point(315, 214);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(12, 15);
            this.labelControl75.TabIndex = 122;
            this.labelControl75.Text = "38";
            // 
            // labelControl76
            // 
            this.labelControl76.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl76.Appearance.Options.UseFont = true;
            this.labelControl76.Location = new System.Drawing.Point(276, 215);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(12, 15);
            this.labelControl76.TabIndex = 120;
            this.labelControl76.Text = "37";
            // 
            // labelControl77
            // 
            this.labelControl77.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl77.Appearance.Options.UseFont = true;
            this.labelControl77.Location = new System.Drawing.Point(237, 214);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(12, 15);
            this.labelControl77.TabIndex = 118;
            this.labelControl77.Text = "36";
            // 
            // labelControl78
            // 
            this.labelControl78.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl78.Appearance.Options.UseFont = true;
            this.labelControl78.Location = new System.Drawing.Point(197, 215);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(12, 15);
            this.labelControl78.TabIndex = 116;
            this.labelControl78.Text = "35";
            // 
            // labelControl79
            // 
            this.labelControl79.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl79.Appearance.Options.UseFont = true;
            this.labelControl79.Location = new System.Drawing.Point(159, 215);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(12, 15);
            this.labelControl79.TabIndex = 114;
            this.labelControl79.Text = "34";
            // 
            // labelControl80
            // 
            this.labelControl80.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl80.Appearance.Options.UseFont = true;
            this.labelControl80.Location = new System.Drawing.Point(120, 214);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(12, 15);
            this.labelControl80.TabIndex = 112;
            this.labelControl80.Text = "33";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl49.Appearance.Options.UseFont = true;
            this.labelControl49.Location = new System.Drawing.Point(725, 156);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(12, 15);
            this.labelControl49.TabIndex = 110;
            this.labelControl49.Text = "32";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Location = new System.Drawing.Point(725, 64);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(12, 15);
            this.labelControl41.TabIndex = 78;
            this.labelControl41.Text = "16";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl50.Appearance.Options.UseFont = true;
            this.labelControl50.Location = new System.Drawing.Point(685, 156);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(12, 15);
            this.labelControl50.TabIndex = 108;
            this.labelControl50.Text = "31";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.Location = new System.Drawing.Point(685, 64);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(12, 15);
            this.labelControl42.TabIndex = 76;
            this.labelControl42.Text = "15";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl51.Appearance.Options.UseFont = true;
            this.labelControl51.Location = new System.Drawing.Point(647, 155);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(12, 15);
            this.labelControl51.TabIndex = 106;
            this.labelControl51.Text = "30";
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl43.Appearance.Options.UseFont = true;
            this.labelControl43.Location = new System.Drawing.Point(647, 63);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(12, 15);
            this.labelControl43.TabIndex = 74;
            this.labelControl43.Text = "14";
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl52.Appearance.Options.UseFont = true;
            this.labelControl52.Location = new System.Drawing.Point(608, 156);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(12, 15);
            this.labelControl52.TabIndex = 104;
            this.labelControl52.Text = "29";
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl44.Appearance.Options.UseFont = true;
            this.labelControl44.Location = new System.Drawing.Point(608, 64);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(12, 15);
            this.labelControl44.TabIndex = 72;
            this.labelControl44.Text = "13";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl53.Appearance.Options.UseFont = true;
            this.labelControl53.Location = new System.Drawing.Point(569, 155);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(12, 15);
            this.labelControl53.TabIndex = 102;
            this.labelControl53.Text = "28";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.Location = new System.Drawing.Point(569, 63);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(12, 15);
            this.labelControl45.TabIndex = 70;
            this.labelControl45.Text = "12";
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl54.Appearance.Options.UseFont = true;
            this.labelControl54.Location = new System.Drawing.Point(529, 156);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(12, 15);
            this.labelControl54.TabIndex = 100;
            this.labelControl54.Text = "27";
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl46.Appearance.Options.UseFont = true;
            this.labelControl46.Location = new System.Drawing.Point(529, 64);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(12, 15);
            this.labelControl46.TabIndex = 68;
            this.labelControl46.Text = "11";
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl55.Appearance.Options.UseFont = true;
            this.labelControl55.Location = new System.Drawing.Point(491, 156);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(12, 15);
            this.labelControl55.TabIndex = 98;
            this.labelControl55.Text = "26";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl47.Appearance.Options.UseFont = true;
            this.labelControl47.Location = new System.Drawing.Point(491, 64);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(12, 15);
            this.labelControl47.TabIndex = 66;
            this.labelControl47.Text = "10";
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl56.Appearance.Options.UseFont = true;
            this.labelControl56.Location = new System.Drawing.Point(452, 155);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(12, 15);
            this.labelControl56.TabIndex = 96;
            this.labelControl56.Text = "25";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl48.Appearance.Options.UseFont = true;
            this.labelControl48.Location = new System.Drawing.Point(452, 63);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(6, 15);
            this.labelControl48.TabIndex = 64;
            this.labelControl48.Text = "9";
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl57.Appearance.Options.UseFont = true;
            this.labelControl57.Location = new System.Drawing.Point(393, 156);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(12, 15);
            this.labelControl57.TabIndex = 94;
            this.labelControl57.Text = "24";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(393, 64);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(6, 15);
            this.labelControl8.TabIndex = 62;
            this.labelControl8.Text = "8";
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl58.Appearance.Options.UseFont = true;
            this.labelControl58.Location = new System.Drawing.Point(353, 156);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(12, 15);
            this.labelControl58.TabIndex = 92;
            this.labelControl58.Text = "23";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(353, 64);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(6, 15);
            this.labelControl9.TabIndex = 60;
            this.labelControl9.Text = "7";
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl59.Appearance.Options.UseFont = true;
            this.labelControl59.Location = new System.Drawing.Point(315, 155);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(12, 15);
            this.labelControl59.TabIndex = 90;
            this.labelControl59.Text = "22";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(315, 63);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(6, 15);
            this.labelControl10.TabIndex = 58;
            this.labelControl10.Text = "6";
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl60.Appearance.Options.UseFont = true;
            this.labelControl60.Location = new System.Drawing.Point(276, 156);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(12, 15);
            this.labelControl60.TabIndex = 88;
            this.labelControl60.Text = "21";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(276, 64);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(6, 15);
            this.labelControl11.TabIndex = 56;
            this.labelControl11.Text = "5";
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl61.Appearance.Options.UseFont = true;
            this.labelControl61.Location = new System.Drawing.Point(237, 155);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(12, 15);
            this.labelControl61.TabIndex = 86;
            this.labelControl61.Text = "20";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(237, 63);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(6, 15);
            this.labelControl12.TabIndex = 54;
            this.labelControl12.Text = "4";
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl62.Appearance.Options.UseFont = true;
            this.labelControl62.Location = new System.Drawing.Point(197, 156);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(12, 15);
            this.labelControl62.TabIndex = 84;
            this.labelControl62.Text = "19";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(197, 64);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(6, 15);
            this.labelControl13.TabIndex = 52;
            this.labelControl13.Text = "3";
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl63.Appearance.Options.UseFont = true;
            this.labelControl63.Location = new System.Drawing.Point(159, 156);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(12, 15);
            this.labelControl63.TabIndex = 82;
            this.labelControl63.Text = "18";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(154, 64);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(6, 15);
            this.labelControl14.TabIndex = 50;
            this.labelControl14.Text = "2";
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl64.Appearance.Options.UseFont = true;
            this.labelControl64.Location = new System.Drawing.Point(120, 155);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(12, 15);
            this.labelControl64.TabIndex = 80;
            this.labelControl64.Text = "17";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(120, 63);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(6, 15);
            this.labelControl15.TabIndex = 48;
            this.labelControl15.Text = "1";
            // 
            // groupControl3
            // 
            this.groupControl3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.groupControl3.Controls.Add(this.PropertiesControl_QPage);
            this.groupControl3.Controls.Add(this.groupControl7);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(235, 585);
            this.groupControl3.TabIndex = 32;
            this.groupControl3.Text = "Окно свойств";
            // 
            // PropertiesControl_QPage
            // 
            this.PropertiesControl_QPage.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControl_QPage.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControl_QPage.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControl_QPage.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControl_QPage.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControl_QPage.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControl_QPage.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControl_QPage.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControl_QPage.Location = new System.Drawing.Point(2, 20);
            this.PropertiesControl_QPage.Name = "PropertiesControl_QPage";
            this.PropertiesControl_QPage.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControl_QPage.RecordWidth = 122;
            this.PropertiesControl_QPage.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2,
            this.repositoryItemGridLookUpEdit3,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox7,
            this.repositoryItemComboBox8,
            this.repositoryItemComboBox17});
            this.PropertiesControl_QPage.RowHeaderWidth = 78;
            this.PropertiesControl_QPage.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow1,
            this.cat_q_Region,
            this.cat_q_Info,
            this.cat_q_Seat});
            this.PropertiesControl_QPage.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControl_QPage.Size = new System.Drawing.Size(231, 298);
            this.PropertiesControl_QPage.TabIndex = 10;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemGridLookUpEdit3
            // 
            this.repositoryItemGridLookUpEdit3.AutoHeight = false;
            this.repositoryItemGridLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit3.Name = "repositoryItemGridLookUpEdit3";
            this.repositoryItemGridLookUpEdit3.View = this.gridView8;
            // 
            // gridView8
            // 
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ImmediatePopup = true;
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox7
            // 
            this.repositoryItemComboBox7.AutoHeight = false;
            this.repositoryItemComboBox7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox7.ImmediatePopup = true;
            this.repositoryItemComboBox7.Name = "repositoryItemComboBox7";
            this.repositoryItemComboBox7.Sorted = true;
            // 
            // repositoryItemComboBox8
            // 
            this.repositoryItemComboBox8.AutoHeight = false;
            this.repositoryItemComboBox8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox8.Name = "repositoryItemComboBox8";
            // 
            // repositoryItemComboBox17
            // 
            this.repositoryItemComboBox17.AutoHeight = false;
            this.repositoryItemComboBox17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox17.ImmediatePopup = true;
            this.repositoryItemComboBox17.Name = "repositoryItemComboBox17";
            // 
            // categoryRow1
            // 
            this.categoryRow1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_q_LastName,
            this.row_q_FirstName,
            this.row_q_SecondName});
            this.categoryRow1.Height = 19;
            this.categoryRow1.Name = "categoryRow1";
            this.categoryRow1.Properties.Caption = "ФИО";
            // 
            // row_q_LastName
            // 
            this.row_q_LastName.Height = 27;
            this.row_q_LastName.Name = "row_q_LastName";
            this.row_q_LastName.Properties.Caption = "Фамилия";
            this.row_q_LastName.Properties.FieldName = "LastName";
            this.row_q_LastName.Properties.ReadOnly = true;
            this.row_q_LastName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.row_q_LastName.Properties.Value = "1111";
            // 
            // row_q_FirstName
            // 
            this.row_q_FirstName.Height = 20;
            this.row_q_FirstName.Name = "row_q_FirstName";
            this.row_q_FirstName.Properties.Caption = "Имя";
            this.row_q_FirstName.Properties.FieldName = "FirstName";
            this.row_q_FirstName.Properties.ReadOnly = true;
            this.row_q_FirstName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_q_SecondName
            // 
            this.row_q_SecondName.Height = 20;
            this.row_q_SecondName.Name = "row_q_SecondName";
            this.row_q_SecondName.Properties.Caption = "Отчество";
            this.row_q_SecondName.Properties.FieldName = "SecondName";
            this.row_q_SecondName.Properties.ReadOnly = true;
            // 
            // cat_q_Region
            // 
            this.cat_q_Region.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_q_RegionName,
            this.row_q_RegionNum});
            this.cat_q_Region.Name = "cat_q_Region";
            this.cat_q_Region.Properties.Caption = "Регион";
            // 
            // row_q_RegionName
            // 
            this.row_q_RegionName.Height = 20;
            this.row_q_RegionName.Name = "row_q_RegionName";
            this.row_q_RegionName.Properties.Caption = "Регион";
            this.row_q_RegionName.Properties.FieldName = "RegionName";
            this.row_q_RegionName.Properties.ReadOnly = true;
            this.row_q_RegionName.Properties.RowEdit = this.repositoryItemComboBox7;
            this.row_q_RegionName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_q_RegionNum
            // 
            this.row_q_RegionNum.Height = 20;
            this.row_q_RegionNum.Name = "row_q_RegionNum";
            this.row_q_RegionNum.Properties.Caption = "№ региона";
            this.row_q_RegionNum.Properties.FieldName = "RegionNum";
            this.row_q_RegionNum.Properties.ReadOnly = true;
            this.row_q_RegionNum.Properties.RowEdit = this.repositoryItemComboBox8;
            this.row_q_RegionNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // cat_q_Info
            // 
            this.cat_q_Info.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_q_Fraction,
            this.row_q_PartyName});
            this.cat_q_Info.Name = "cat_q_Info";
            this.cat_q_Info.Properties.Caption = "Дополнительная информация";
            // 
            // row_q_Fraction
            // 
            this.row_q_Fraction.Height = 25;
            this.row_q_Fraction.Name = "row_q_Fraction";
            this.row_q_Fraction.Properties.Caption = "Фракция";
            this.row_q_Fraction.Properties.FieldName = "Fraction";
            this.row_q_Fraction.Properties.ReadOnly = true;
            this.row_q_Fraction.Properties.RowEdit = this.repositoryItemComboBox17;
            this.row_q_Fraction.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_q_PartyName
            // 
            this.row_q_PartyName.Height = 23;
            this.row_q_PartyName.Name = "row_q_PartyName";
            this.row_q_PartyName.Properties.Caption = "Партия";
            this.row_q_PartyName.Properties.FieldName = "idParty.Name";
            this.row_q_PartyName.Properties.ReadOnly = true;
            this.row_q_PartyName.Properties.RowEdit = this.repositoryItemComboBox1;
            this.row_q_PartyName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.row_q_PartyName.Properties.Value = "Партия 1";
            // 
            // cat_q_Seat
            // 
            this.cat_q_Seat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_q_MicNum,
            this.row_q_RowNum,
            this.row_q_SeatNum});
            this.cat_q_Seat.Name = "cat_q_Seat";
            // 
            // row_q_MicNum
            // 
            this.row_q_MicNum.Height = 16;
            this.row_q_MicNum.Name = "row_q_MicNum";
            this.row_q_MicNum.Properties.Caption = "Мик. №";
            this.row_q_MicNum.Properties.ReadOnly = true;
            this.row_q_MicNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // row_q_RowNum
            // 
            this.row_q_RowNum.Name = "row_q_RowNum";
            this.row_q_RowNum.Properties.Caption = "Ряд №";
            this.row_q_RowNum.Properties.ReadOnly = true;
            this.row_q_RowNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // row_q_SeatNum
            // 
            this.row_q_SeatNum.Name = "row_q_SeatNum";
            this.row_q_SeatNum.Properties.Caption = "Место №";
            this.row_q_SeatNum.Properties.ReadOnly = true;
            this.row_q_SeatNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // groupControl7
            // 
            this.groupControl7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.groupControl7.Controls.Add(this.listBoxControl3);
            this.groupControl7.Controls.Add(this.label1);
            this.groupControl7.Controls.Add(this.label39);
            this.groupControl7.Controls.Add(this.label42);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl7.Location = new System.Drawing.Point(2, 360);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(231, 223);
            this.groupControl7.TabIndex = 9;
            this.groupControl7.Text = "События";
            // 
            // listBoxControl3
            // 
            this.listBoxControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxControl3.Items.AddRange(new object[] {
            "12:30 Регистрация"});
            this.listBoxControl3.Location = new System.Drawing.Point(2, 20);
            this.listBoxControl3.Name = "listBoxControl3";
            this.listBoxControl3.Size = new System.Drawing.Size(227, 201);
            this.listBoxControl3.TabIndex = 212;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label1.Location = new System.Drawing.Point(133, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 27);
            this.label1.TabIndex = 211;
            this.label1.Text = "16:43";
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label39.Location = new System.Drawing.Point(17, 28);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(138, 27);
            this.label39.TabIndex = 200;
            this.label39.Text = "Регистрация в";
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label42.Location = new System.Drawing.Point(109, 213);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(46, 17);
            this.label42.TabIndex = 210;
            this.label42.Text = "8";
            this.label42.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl2.Controls.Add(this.lblRegTime);
            this.panelControl2.Controls.Add(this.lblQuorumFinished);
            this.panelControl2.Controls.Add(this.btnRegCancel);
            this.panelControl2.Controls.Add(this.progressBarRegistration);
            this.panelControl2.Controls.Add(this.txtQuorumQnty);
            this.panelControl2.Controls.Add(this.labelControl20);
            this.panelControl2.Controls.Add(this.txtRegQnty);
            this.panelControl2.Controls.Add(this.labelControl19);
            this.panelControl2.Controls.Add(this.txtDelegateQnty);
            this.panelControl2.Controls.Add(this.labelControl18);
            this.panelControl2.Controls.Add(this.lblRegistration);
            this.panelControl2.Controls.Add(this.picRegTime);
            this.panelControl2.Controls.Add(this.btnRegStart);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 591);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1023, 90);
            this.panelControl2.TabIndex = 30;
            // 
            // lblRegTime
            // 
            this.lblRegTime.Appearance.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRegTime.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblRegTime.Appearance.Options.UseFont = true;
            this.lblRegTime.Appearance.Options.UseForeColor = true;
            this.lblRegTime.Location = new System.Drawing.Point(260, 39);
            this.lblRegTime.Name = "lblRegTime";
            this.lblRegTime.Size = new System.Drawing.Size(42, 15);
            this.lblRegTime.TabIndex = 243;
            this.lblRegTime.Text = "02 : 30";
            // 
            // lblQuorumFinished
            // 
            this.lblQuorumFinished.Appearance.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQuorumFinished.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblQuorumFinished.Appearance.Options.UseFont = true;
            this.lblQuorumFinished.Appearance.Options.UseForeColor = true;
            this.lblQuorumFinished.Location = new System.Drawing.Point(656, 54);
            this.lblQuorumFinished.Name = "lblQuorumFinished";
            this.lblQuorumFinished.Size = new System.Drawing.Size(115, 15);
            this.lblQuorumFinished.TabIndex = 242;
            this.lblQuorumFinished.Text = "Кворум достигнут!";
            // 
            // btnRegCancel
            // 
            this.btnRegCancel.ImageIndex = 7;
            this.btnRegCancel.ImageList = this.ButtonImages;
            this.btnRegCancel.Location = new System.Drawing.Point(811, 20);
            this.btnRegCancel.Name = "btnRegCancel";
            this.btnRegCancel.Size = new System.Drawing.Size(194, 47);
            this.btnRegCancel.TabIndex = 241;
            this.btnRegCancel.Text = "Отменить итоги регистрации";
            this.btnRegCancel.Click += new System.EventHandler(this.btnRegCancel_Click);
            // 
            // progressBarRegistration
            // 
            this.progressBarRegistration.Location = new System.Drawing.Point(316, 51);
            this.progressBarRegistration.Name = "progressBarRegistration";
            this.progressBarRegistration.Size = new System.Drawing.Size(104, 18);
            this.progressBarRegistration.TabIndex = 240;
            // 
            // txtQuorumQnty
            // 
            this.txtQuorumQnty.Location = new System.Drawing.Point(715, 18);
            this.txtQuorumQnty.Name = "txtQuorumQnty";
            this.txtQuorumQnty.Size = new System.Drawing.Size(49, 20);
            this.txtQuorumQnty.TabIndex = 239;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(656, 22);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(41, 13);
            this.labelControl20.TabIndex = 238;
            this.labelControl20.Text = "Кворум:";
            // 
            // txtRegQnty
            // 
            this.txtRegQnty.Location = new System.Drawing.Point(571, 50);
            this.txtRegQnty.Name = "txtRegQnty";
            this.txtRegQnty.Size = new System.Drawing.Size(49, 20);
            this.txtRegQnty.TabIndex = 237;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(458, 54);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(109, 13);
            this.labelControl19.TabIndex = 236;
            this.labelControl19.Text = "Зарегистрировалось:";
            // 
            // txtDelegateQnty
            // 
            this.txtDelegateQnty.Location = new System.Drawing.Point(571, 19);
            this.txtDelegateQnty.Name = "txtDelegateQnty";
            this.txtDelegateQnty.Size = new System.Drawing.Size(49, 20);
            this.txtDelegateQnty.TabIndex = 235;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(458, 22);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(97, 13);
            this.labelControl18.TabIndex = 234;
            this.labelControl18.Text = "Кол-во депутатов:";
            // 
            // lblRegistration
            // 
            this.lblRegistration.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRegistration.Appearance.Options.UseFont = true;
            this.lblRegistration.Location = new System.Drawing.Point(316, 25);
            this.lblRegistration.Name = "lblRegistration";
            this.lblRegistration.Size = new System.Drawing.Size(105, 13);
            this.lblRegistration.TabIndex = 229;
            this.lblRegistration.Text = "Идет регистрация...";
            // 
            // picRegTime
            // 
            this.picRegTime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picRegTime.EditValue = ((object)(resources.GetObject("picRegTime.EditValue")));
            this.picRegTime.Location = new System.Drawing.Point(220, 29);
            this.picRegTime.Name = "picRegTime";
            this.picRegTime.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picRegTime.Properties.Appearance.Options.UseBackColor = true;
            this.picRegTime.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picRegTime.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.picRegTime.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picRegTime.Properties.UseParentBackground = true;
            this.picRegTime.Size = new System.Drawing.Size(35, 47);
            this.picRegTime.TabIndex = 228;
            // 
            // btnRegStart
            // 
            this.btnRegStart.ImageIndex = 11;
            this.btnRegStart.ImageList = this.ButtonImages;
            this.btnRegStart.Location = new System.Drawing.Point(27, 20);
            this.btnRegStart.Name = "btnRegStart";
            this.btnRegStart.Size = new System.Drawing.Size(172, 47);
            this.btnRegStart.TabIndex = 230;
            this.btnRegStart.Text = "Начать регистрацию";
            this.btnRegStart.Click += new System.EventHandler(this.btnRegStart_Click);
            // 
            // pgResQuestions
            // 
            this.pgResQuestions.Controls.Add(this.splitContainerControl5);
            this.pgResQuestions.Name = "pgResQuestions";
            this.pgResQuestions.PageVisible = false;
            this.pgResQuestions.Size = new System.Drawing.Size(1023, 681);
            this.pgResQuestions.Text = "Итоги голосования";
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.panelControl5);
            this.splitContainerControl5.Panel1.Controls.Add(this.btnQuestVoteDetail);
            this.splitContainerControl5.Panel1.Controls.Add(this.btnQuestVoteCancel);
            this.splitContainerControl5.Panel1.Controls.Add(this.btnQuestNewRead);
            this.splitContainerControl5.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.grpResQuestProps);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(1023, 681);
            this.splitContainerControl5.SplitterPosition = 658;
            this.splitContainerControl5.TabIndex = 0;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // panelControl5
            // 
            this.panelControl5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl5.Controls.Add(this.navigatorResQuestions);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 591);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(654, 37);
            this.panelControl5.TabIndex = 240;
            // 
            // navigatorResQuestions
            // 
            this.navigatorResQuestions.Buttons.Append.Visible = false;
            this.navigatorResQuestions.Buttons.CancelEdit.Visible = false;
            this.navigatorResQuestions.Buttons.EndEdit.Visible = false;
            this.navigatorResQuestions.Buttons.Remove.Visible = false;
            this.navigatorResQuestions.DataSource = this.xpResQuestions;
            this.navigatorResQuestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigatorResQuestions.Location = new System.Drawing.Point(2, 2);
            this.navigatorResQuestions.Name = "navigatorResQuestions";
            this.navigatorResQuestions.Size = new System.Drawing.Size(650, 33);
            this.navigatorResQuestions.TabIndex = 235;
            this.navigatorResQuestions.Text = "dataNavigator4";
            this.navigatorResQuestions.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.navigatorQuestions_ButtonClick);
            this.navigatorResQuestions.PositionChanged += new System.EventHandler(this.navigatorResQuestions_PositionChanged);
            // 
            // xpResQuestions
            // 
            this.xpResQuestions.CriteriaString = "[IsResult] = True";
            this.xpResQuestions.DeleteObjectOnRemove = true;
            this.xpResQuestions.ObjectType = typeof(VoteSystem.S_QuestionObj);
            this.xpResQuestions.Sorting.AddRange(new DevExpress.Xpo.SortProperty[] {
            new DevExpress.Xpo.SortProperty("VoteDateTime", DevExpress.Xpo.DB.SortingDirection.Descending)});
            this.xpResQuestions.DisplayableProperties = @"This;id;idSession!;idSession!Key;idSession;idQuestion!;idQuestion.Caption;idQuestion;idVoteType!;idVoteType;idVoteKind!;idVoteKind;idVoteQnty!;idVoteQnty;idProcentDecisionType!;idProcentDecisionType;idQuotaProcentType!;idQuotaProcentType;idQuestion.CrDate;IsResult;idResultValue.Value;VoteDateTime";
            // 
            // btnQuestVoteDetail
            // 
            this.btnQuestVoteDetail.ImageIndex = 1;
            this.btnQuestVoteDetail.ImageList = this.ButtonImages;
            this.btnQuestVoteDetail.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnQuestVoteDetail.Location = new System.Drawing.Point(452, 635);
            this.btnQuestVoteDetail.Name = "btnQuestVoteDetail";
            this.btnQuestVoteDetail.Size = new System.Drawing.Size(172, 38);
            this.btnQuestVoteDetail.TabIndex = 239;
            this.btnQuestVoteDetail.Text = "Итоги подробнее...";
            this.btnQuestVoteDetail.Click += new System.EventHandler(this.btnQuestVoteDetail_Click);
            // 
            // btnQuestVoteCancel
            // 
            this.btnQuestVoteCancel.ImageIndex = 9;
            this.btnQuestVoteCancel.ImageList = this.ButtonImages;
            this.btnQuestVoteCancel.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnQuestVoteCancel.Location = new System.Drawing.Point(223, 635);
            this.btnQuestVoteCancel.Name = "btnQuestVoteCancel";
            this.btnQuestVoteCancel.Size = new System.Drawing.Size(207, 38);
            this.btnQuestVoteCancel.TabIndex = 238;
            this.btnQuestVoteCancel.Text = "Отменить итоги голосования";
            this.btnQuestVoteCancel.Click += new System.EventHandler(this.btnQuestVoteCancel_Click);
            // 
            // btnQuestNewRead
            // 
            this.btnQuestNewRead.ImageIndex = 4;
            this.btnQuestNewRead.ImageList = this.ButtonImages;
            this.btnQuestNewRead.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnQuestNewRead.Location = new System.Drawing.Point(26, 635);
            this.btnQuestNewRead.Name = "btnQuestNewRead";
            this.btnQuestNewRead.Size = new System.Drawing.Size(172, 38);
            this.btnQuestNewRead.TabIndex = 237;
            this.btnQuestNewRead.Text = "Новое чтение";
            this.btnQuestNewRead.Click += new System.EventHandler(this.btnQuestNewRead_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.xpResQuestions;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridControl1.EmbeddedNavigator.Name = "";
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView7;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(654, 591);
            this.gridControl1.TabIndex = 236;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn7,
            this.gridColumn8});
            this.gridView7.GridControl = this.gridControl1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView7.OptionsBehavior.Editable = false;
            this.gridView7.OptionsDetail.AutoZoomDetail = true;
            this.gridView7.OptionsDetail.EnableMasterViewMode = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "№";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Заголовок";
            this.gridColumn2.FieldName = "idQuestion.Caption";
            this.gridColumn2.MinWidth = 255;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 255;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Дата голос.";
            this.gridColumn7.FieldName = "VoteDateTime";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Результат";
            this.gridColumn8.FieldName = "idResultValue.Value";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            // 
            // grpResQuestProps
            // 
            this.grpResQuestProps.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.grpResQuestProps.Controls.Add(this.grpResQuestPropsIn);
            this.grpResQuestProps.Controls.Add(this.groupControl6);
            this.grpResQuestProps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpResQuestProps.Location = new System.Drawing.Point(0, 0);
            this.grpResQuestProps.Name = "grpResQuestProps";
            this.grpResQuestProps.Size = new System.Drawing.Size(355, 677);
            this.grpResQuestProps.TabIndex = 235;
            this.grpResQuestProps.Text = "Окно свойств";
            // 
            // grpResQuestPropsIn
            // 
            this.grpResQuestPropsIn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.grpResQuestPropsIn.Controls.Add(this.PropertiesControlQuest2);
            this.grpResQuestPropsIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpResQuestPropsIn.Location = new System.Drawing.Point(2, 20);
            this.grpResQuestPropsIn.Name = "grpResQuestPropsIn";
            this.grpResQuestPropsIn.ShowCaption = false;
            this.grpResQuestPropsIn.Size = new System.Drawing.Size(351, 411);
            this.grpResQuestPropsIn.TabIndex = 9;
            this.grpResQuestPropsIn.Text = "groupControl1";
            // 
            // PropertiesControlQuest2
            // 
            this.PropertiesControlQuest2.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest2.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest2.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest2.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlQuest2.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlQuest2.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest2.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControlQuest2.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlQuest2.Location = new System.Drawing.Point(2, 2);
            this.PropertiesControlQuest2.Name = "PropertiesControlQuest2";
            this.PropertiesControlQuest2.RecordWidth = 97;
            this.PropertiesControlQuest2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3,
            this.repositoryItemGridLookUpEdit4,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox18,
            this.repositoryItemComboBox19,
            this.repositoryItemComboBox20,
            this.repositoryItemComboBox21,
            this.repositoryItemComboBox22,
            this.repositoryItemDateEdit2,
            this.repositoryItemComboBox23,
            this.repositoryItemComboBox24,
            this.repositoryItemComboBox25,
            this.repositoryItemComboBox26,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckEdit4,
            this.repositoryItemDateEdit3,
            this.repositoryItemMemoEdit1,
            this.repositoryItemComboBox27,
            this.repositoryItemSpinEdit2,
            this.repositoryItemComboBox28,
            this.repositoryItemComboBox29});
            this.PropertiesControlQuest2.RowHeaderWidth = 103;
            this.PropertiesControlQuest2.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow2,
            this.categoryRow3});
            this.PropertiesControlQuest2.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlQuest2.Size = new System.Drawing.Size(347, 407);
            this.PropertiesControlQuest2.TabIndex = 13;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemGridLookUpEdit4
            // 
            this.repositoryItemGridLookUpEdit4.AutoHeight = false;
            this.repositoryItemGridLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit4.Name = "repositoryItemGridLookUpEdit4";
            this.repositoryItemGridLookUpEdit4.View = this.gridView9;
            // 
            // gridView9
            // 
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.ImmediatePopup = true;
            this.repositoryItemComboBox3.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox18
            // 
            this.repositoryItemComboBox18.AutoHeight = false;
            this.repositoryItemComboBox18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox18.ImmediatePopup = true;
            this.repositoryItemComboBox18.Name = "repositoryItemComboBox18";
            this.repositoryItemComboBox18.Sorted = true;
            // 
            // repositoryItemComboBox19
            // 
            this.repositoryItemComboBox19.AutoHeight = false;
            this.repositoryItemComboBox19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox19.Name = "repositoryItemComboBox19";
            // 
            // repositoryItemComboBox20
            // 
            this.repositoryItemComboBox20.AutoHeight = false;
            this.repositoryItemComboBox20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox20.ImmediatePopup = true;
            this.repositoryItemComboBox20.Name = "repositoryItemComboBox20";
            // 
            // repositoryItemComboBox21
            // 
            this.repositoryItemComboBox21.AutoHeight = false;
            this.repositoryItemComboBox21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox21.Name = "repositoryItemComboBox21";
            // 
            // repositoryItemComboBox22
            // 
            this.repositoryItemComboBox22.AutoHeight = false;
            this.repositoryItemComboBox22.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox22.Name = "repositoryItemComboBox22";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            this.repositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemComboBox23
            // 
            this.repositoryItemComboBox23.AutoHeight = false;
            this.repositoryItemComboBox23.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox23.Name = "repositoryItemComboBox23";
            // 
            // repositoryItemComboBox24
            // 
            this.repositoryItemComboBox24.AutoHeight = false;
            this.repositoryItemComboBox24.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox24.Name = "repositoryItemComboBox24";
            // 
            // repositoryItemComboBox25
            // 
            this.repositoryItemComboBox25.AutoHeight = false;
            this.repositoryItemComboBox25.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox25.Name = "repositoryItemComboBox25";
            // 
            // repositoryItemComboBox26
            // 
            this.repositoryItemComboBox26.AutoHeight = false;
            this.repositoryItemComboBox26.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox26.Name = "repositoryItemComboBox26";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            this.repositoryItemDateEdit3.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // repositoryItemComboBox27
            // 
            this.repositoryItemComboBox27.AutoHeight = false;
            this.repositoryItemComboBox27.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox27.Name = "repositoryItemComboBox27";
            // 
            // repositoryItemComboBox28
            // 
            this.repositoryItemComboBox28.AutoHeight = false;
            this.repositoryItemComboBox28.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox28.Name = "repositoryItemComboBox28";
            // 
            // repositoryItemComboBox29
            // 
            this.repositoryItemComboBox29.AutoHeight = false;
            this.repositoryItemComboBox29.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox29.Name = "repositoryItemComboBox29";
            // 
            // categoryRow2
            // 
            this.categoryRow2.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow1,
            this.editorRow2,
            this.editorRow3,
            this.editorRow4,
            this.editorRow5,
            this.editorRow6,
            this.editorRow7});
            this.categoryRow2.Height = 19;
            this.categoryRow2.Name = "categoryRow2";
            this.categoryRow2.Properties.Caption = "Характеристики";
            // 
            // editorRow1
            // 
            this.editorRow1.Height = 28;
            this.editorRow1.Name = "editorRow1";
            this.editorRow1.Properties.Caption = "№ вопроса";
            this.editorRow1.Properties.ReadOnly = true;
            this.editorRow1.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow2
            // 
            this.editorRow2.Height = 47;
            this.editorRow2.Name = "editorRow2";
            this.editorRow2.Properties.Caption = "Заголовок";
            this.editorRow2.Properties.ReadOnly = true;
            this.editorRow2.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow3
            // 
            this.editorRow3.Height = 28;
            this.editorRow3.Name = "editorRow3";
            this.editorRow3.Properties.Caption = "Дата/время создания";
            this.editorRow3.Properties.ReadOnly = true;
            this.editorRow3.Properties.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            // 
            // editorRow4
            // 
            this.editorRow4.Height = 120;
            this.editorRow4.Name = "editorRow4";
            this.editorRow4.Properties.Caption = "Заметки";
            this.editorRow4.Properties.RowEdit = this.repositoryItemMemoEdit1;
            this.editorRow4.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow5
            // 
            this.editorRow5.Name = "editorRow5";
            this.editorRow5.Properties.Caption = "Тип голосования";
            this.editorRow5.Properties.ReadOnly = true;
            this.editorRow5.Properties.RowEdit = this.repositoryItemComboBox28;
            this.editorRow5.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow6
            // 
            this.editorRow6.Name = "editorRow6";
            this.editorRow6.Properties.Caption = "Вид голосования";
            this.editorRow6.Properties.ReadOnly = true;
            this.editorRow6.Properties.RowEdit = this.repositoryItemComboBox27;
            this.editorRow6.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow7
            // 
            this.editorRow7.Height = 25;
            this.editorRow7.Name = "editorRow7";
            this.editorRow7.Properties.Caption = "Чтение";
            this.editorRow7.Properties.ReadOnly = true;
            this.editorRow7.Properties.RowEdit = this.repositoryItemComboBox29;
            // 
            // categoryRow3
            // 
            this.categoryRow3.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.multiEditorRow1,
            this.editorRow10});
            this.categoryRow3.Expanded = false;
            this.categoryRow3.Name = "categoryRow3";
            this.categoryRow3.Properties.Caption = "Критерий принятия решения";
            // 
            // multiEditorRow1
            // 
            this.multiEditorRow1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow8,
            this.editorRow9});
            this.multiEditorRow1.Name = "multiEditorRow1";
            multiEditorRowProperties7.Caption = "Порог принятия решения";
            multiEditorRowProperties7.CellWidth = 15;
            multiEditorRowProperties7.RowEdit = this.repositoryItemSpinEdit2;
            multiEditorRowProperties7.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            multiEditorRowProperties7.Width = 117;
            multiEditorRowProperties8.ReadOnly = true;
            multiEditorRowProperties8.UnboundType = DevExpress.Data.UnboundColumnType.String;
            multiEditorRowProperties8.Value = "%";
            multiEditorRowProperties8.Width = 15;
            this.multiEditorRow1.PropertiesCollection.AddRange(new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties[] {
            multiEditorRowProperties7,
            multiEditorRowProperties8});
            // 
            // editorRow8
            // 
            this.editorRow8.Name = "editorRow8";
            this.editorRow8.Properties.Caption = "От числа депутатов";
            this.editorRow8.Properties.ReadOnly = true;
            this.editorRow8.Properties.RowEdit = this.repositoryItemComboBox25;
            this.editorRow8.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow9
            // 
            this.editorRow9.Name = "editorRow9";
            this.editorRow9.Properties.Caption = "Число голосов";
            this.editorRow9.Properties.ReadOnly = true;
            this.editorRow9.Properties.RowEdit = this.repositoryItemComboBox26;
            this.editorRow9.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow10
            // 
            this.editorRow10.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow11,
            this.editorRow12});
            this.editorRow10.Name = "editorRow10";
            this.editorRow10.Properties.Caption = "Квота на кворум";
            this.editorRow10.Properties.ReadOnly = true;
            this.editorRow10.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // editorRow11
            // 
            this.editorRow11.Name = "editorRow11";
            this.editorRow11.Properties.Caption = "От присутствующих";
            this.editorRow11.Properties.RowEdit = this.repositoryItemCheckEdit3;
            this.editorRow11.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            // 
            // editorRow12
            // 
            this.editorRow12.Name = "editorRow12";
            this.editorRow12.Properties.Caption = "От общего списка";
            this.editorRow12.Properties.RowEdit = this.repositoryItemCheckEdit4;
            // 
            // groupControl6
            // 
            this.groupControl6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.groupControl6.Controls.Add(this.labelControl16);
            this.groupControl6.Controls.Add(this.lblVoteDateTime);
            this.groupControl6.Controls.Add(this.labelControl22);
            this.groupControl6.Controls.Add(this.lblProcQnty);
            this.groupControl6.Controls.Add(this.grpVoteResults);
            this.groupControl6.Controls.Add(this.lblDecision);
            this.groupControl6.Controls.Add(this.label77);
            this.groupControl6.Controls.Add(this.lblVoteQnty);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl6.Location = new System.Drawing.Point(2, 431);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(351, 244);
            this.groupControl6.TabIndex = 8;
            this.groupControl6.Text = "Итоги голосования";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(193, 27);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(34, 13);
            this.labelControl16.TabIndex = 228;
            this.labelControl16.Text = "Время:";
            // 
            // lblVoteDateTime
            // 
            this.lblVoteDateTime.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteDateTime.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblVoteDateTime.Location = new System.Drawing.Point(233, 27);
            this.lblVoteDateTime.Name = "lblVoteDateTime";
            this.lblVoteDateTime.Size = new System.Drawing.Size(168, 16);
            this.lblVoteDateTime.TabIndex = 227;
            this.lblVoteDateTime.Text = "16:30";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(34, 27);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(48, 13);
            this.labelControl22.TabIndex = 215;
            this.labelControl22.Text = "Решение:";
            // 
            // lblProcQnty
            // 
            this.lblProcQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcQnty.Location = new System.Drawing.Point(253, 215);
            this.lblProcQnty.Name = "lblProcQnty";
            this.lblProcQnty.Size = new System.Drawing.Size(46, 17);
            this.lblProcQnty.TabIndex = 212;
            this.lblProcQnty.Text = "70 %";
            this.lblProcQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // grpVoteResults
            // 
            this.grpVoteResults.Controls.Add(this.label107);
            this.grpVoteResults.Controls.Add(this.label106);
            this.grpVoteResults.Controls.Add(this.label105);
            this.grpVoteResults.Controls.Add(this.label104);
            this.grpVoteResults.Controls.Add(this.label103);
            this.grpVoteResults.Controls.Add(this.lblProcAye);
            this.grpVoteResults.Controls.Add(this.lblVoteAye);
            this.grpVoteResults.Controls.Add(this.label101);
            this.grpVoteResults.Controls.Add(this.lblNonProcQnty);
            this.grpVoteResults.Controls.Add(this.lblNonVoteQnty);
            this.grpVoteResults.Controls.Add(this.label95);
            this.grpVoteResults.Controls.Add(this.lblProcAgainst);
            this.grpVoteResults.Controls.Add(this.label79);
            this.grpVoteResults.Controls.Add(this.label80);
            this.grpVoteResults.Controls.Add(this.lblProcAbstain);
            this.grpVoteResults.Controls.Add(this.lblVoteAgainst);
            this.grpVoteResults.Controls.Add(this.label83);
            this.grpVoteResults.Controls.Add(this.label84);
            this.grpVoteResults.Controls.Add(this.label85);
            this.grpVoteResults.Controls.Add(this.label86);
            this.grpVoteResults.Controls.Add(this.label87);
            this.grpVoteResults.Controls.Add(this.label88);
            this.grpVoteResults.Controls.Add(this.lblVoteAbstain);
            this.grpVoteResults.Controls.Add(this.label90);
            this.grpVoteResults.Controls.Add(this.domainUpDown5);
            this.grpVoteResults.Controls.Add(this.label91);
            this.grpVoteResults.Location = new System.Drawing.Point(30, 41);
            this.grpVoteResults.Name = "grpVoteResults";
            this.grpVoteResults.Size = new System.Drawing.Size(301, 171);
            this.grpVoteResults.TabIndex = 203;
            this.grpVoteResults.TabStop = false;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Enabled = false;
            this.label107.ForeColor = System.Drawing.Color.DimGray;
            this.label107.Location = new System.Drawing.Point(6, 127);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(17, 13);
            this.label107.TabIndex = 184;
            this.label107.Text = "5.";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Enabled = false;
            this.label106.ForeColor = System.Drawing.Color.DimGray;
            this.label106.Location = new System.Drawing.Point(6, 106);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(17, 13);
            this.label106.TabIndex = 183;
            this.label106.Text = "4.";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 84);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(17, 13);
            this.label105.TabIndex = 182;
            this.label105.Text = "3.";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 62);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(17, 13);
            this.label104.TabIndex = 181;
            this.label104.Text = "2.";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 39);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(17, 13);
            this.label103.TabIndex = 180;
            this.label103.Text = "1.";
            // 
            // lblProcAye
            // 
            this.lblProcAye.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAye.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblProcAye.Location = new System.Drawing.Point(223, 39);
            this.lblProcAye.Name = "lblProcAye";
            this.lblProcAye.Size = new System.Drawing.Size(46, 17);
            this.lblProcAye.TabIndex = 179;
            this.lblProcAye.Text = "8";
            this.lblProcAye.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVoteAye
            // 
            this.lblVoteAye.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAye.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblVoteAye.Location = new System.Drawing.Point(124, 39);
            this.lblVoteAye.Name = "lblVoteAye";
            this.lblVoteAye.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAye.TabIndex = 178;
            this.lblVoteAye.Text = "8";
            this.lblVoteAye.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label101.ForeColor = System.Drawing.Color.DarkGreen;
            this.label101.Location = new System.Drawing.Point(44, 40);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(23, 14);
            this.label101.TabIndex = 177;
            this.label101.Text = "ЗА";
            // 
            // lblNonProcQnty
            // 
            this.lblNonProcQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNonProcQnty.Location = new System.Drawing.Point(223, 149);
            this.lblNonProcQnty.Name = "lblNonProcQnty";
            this.lblNonProcQnty.Size = new System.Drawing.Size(46, 17);
            this.lblNonProcQnty.TabIndex = 176;
            this.lblNonProcQnty.Text = "8";
            this.lblNonProcQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblNonVoteQnty
            // 
            this.lblNonVoteQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNonVoteQnty.Location = new System.Drawing.Point(141, 150);
            this.lblNonVoteQnty.Name = "lblNonVoteQnty";
            this.lblNonVoteQnty.Size = new System.Drawing.Size(10, 16);
            this.lblNonVoteQnty.TabIndex = 175;
            this.lblNonVoteQnty.Text = "8";
            this.lblNonVoteQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label95.ForeColor = System.Drawing.Color.Purple;
            this.label95.Location = new System.Drawing.Point(44, 150);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(94, 14);
            this.label95.TabIndex = 174;
            this.label95.Text = "Не голосовали";
            // 
            // lblProcAgainst
            // 
            this.lblProcAgainst.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAgainst.ForeColor = System.Drawing.Color.Maroon;
            this.lblProcAgainst.Location = new System.Drawing.Point(223, 61);
            this.lblProcAgainst.Name = "lblProcAgainst";
            this.lblProcAgainst.Size = new System.Drawing.Size(46, 17);
            this.lblProcAgainst.TabIndex = 173;
            this.lblProcAgainst.Text = "8";
            this.lblProcAgainst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label79.Location = new System.Drawing.Point(223, 127);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(46, 17);
            this.label79.TabIndex = 172;
            this.label79.Text = "9";
            this.label79.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label79.Visible = false;
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label80.Location = new System.Drawing.Point(223, 105);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(46, 17);
            this.label80.TabIndex = 171;
            this.label80.Text = "8";
            this.label80.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label80.Visible = false;
            // 
            // lblProcAbstain
            // 
            this.lblProcAbstain.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAbstain.ForeColor = System.Drawing.Color.Orange;
            this.lblProcAbstain.Location = new System.Drawing.Point(223, 83);
            this.lblProcAbstain.Name = "lblProcAbstain";
            this.lblProcAbstain.Size = new System.Drawing.Size(46, 17);
            this.lblProcAbstain.TabIndex = 170;
            this.lblProcAbstain.Text = "8";
            this.lblProcAbstain.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVoteAgainst
            // 
            this.lblVoteAgainst.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAgainst.ForeColor = System.Drawing.Color.Maroon;
            this.lblVoteAgainst.Location = new System.Drawing.Point(124, 61);
            this.lblVoteAgainst.Name = "lblVoteAgainst";
            this.lblVoteAgainst.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAgainst.TabIndex = 169;
            this.lblVoteAgainst.Text = "8";
            this.lblVoteAgainst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label83.Location = new System.Drawing.Point(124, 127);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(46, 17);
            this.label83.TabIndex = 168;
            this.label83.Text = "9";
            this.label83.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label83.Visible = false;
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label84.Location = new System.Drawing.Point(124, 105);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(46, 17);
            this.label84.TabIndex = 167;
            this.label84.Text = "8";
            this.label84.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label84.Visible = false;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label85.Location = new System.Drawing.Point(223, 15);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(62, 14);
            this.label85.TabIndex = 166;
            this.label85.Text = "Процент:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label86.Location = new System.Drawing.Point(124, 15);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(51, 14);
            this.label86.TabIndex = 165;
            this.label86.Text = "Голоса:";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(44, 127);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(48, 13);
            this.label87.TabIndex = 164;
            this.label87.Text = "Ответ 5";
            this.label87.Visible = false;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(44, 106);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(48, 13);
            this.label88.TabIndex = 163;
            this.label88.Text = "Ответ 4";
            this.label88.Visible = false;
            // 
            // lblVoteAbstain
            // 
            this.lblVoteAbstain.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAbstain.ForeColor = System.Drawing.Color.Orange;
            this.lblVoteAbstain.Location = new System.Drawing.Point(124, 83);
            this.lblVoteAbstain.Name = "lblVoteAbstain";
            this.lblVoteAbstain.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAbstain.TabIndex = 155;
            this.lblVoteAbstain.Text = "8";
            this.lblVoteAbstain.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label90.ForeColor = System.Drawing.Color.Maroon;
            this.label90.Location = new System.Drawing.Point(44, 62);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(57, 14);
            this.label90.TabIndex = 153;
            this.label90.Text = "ПРОТИВ";
            // 
            // domainUpDown5
            // 
            this.domainUpDown5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.domainUpDown5.Location = new System.Drawing.Point(80, 400);
            this.domainUpDown5.Name = "domainUpDown5";
            this.domainUpDown5.Size = new System.Drawing.Size(77, 20);
            this.domainUpDown5.TabIndex = 145;
            this.domainUpDown5.Text = "2";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label91.ForeColor = System.Drawing.Color.Orange;
            this.label91.Location = new System.Drawing.Point(44, 84);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(57, 14);
            this.label91.TabIndex = 139;
            this.label91.Text = "ВОЗДЕР.";
            // 
            // lblDecision
            // 
            this.lblDecision.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDecision.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblDecision.Location = new System.Drawing.Point(90, 26);
            this.lblDecision.Name = "lblDecision";
            this.lblDecision.Size = new System.Drawing.Size(113, 22);
            this.lblDecision.TabIndex = 200;
            this.lblDecision.Text = "Не принято";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(74, 215);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(70, 13);
            this.label77.TabIndex = 209;
            this.label77.Text = "Голосовали:";
            // 
            // lblVoteQnty
            // 
            this.lblVoteQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteQnty.Location = new System.Drawing.Point(149, 215);
            this.lblVoteQnty.Name = "lblVoteQnty";
            this.lblVoteQnty.Size = new System.Drawing.Size(64, 17);
            this.lblVoteQnty.TabIndex = 210;
            this.lblVoteQnty.Text = "8 из 12";
            this.lblVoteQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pgStatements
            // 
            this.pgStatements.Controls.Add(this.splitContainerControl6);
            this.pgStatements.Name = "pgStatements";
            this.pgStatements.PageVisible = false;
            this.pgStatements.Size = new System.Drawing.Size(1023, 681);
            this.pgStatements.Text = "Выступления";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl6.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.simpleButton4);
            this.splitContainerControl6.Panel1.Controls.Add(this.btnStatementMode);
            this.splitContainerControl6.Panel1.Controls.Add(this.navigatorStatements);
            this.splitContainerControl6.Panel1.Controls.Add(this.StatementGrid);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(1023, 681);
            this.splitContainerControl6.SplitterPosition = 623;
            this.splitContainerControl6.TabIndex = 1;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // simpleButton4
            // 
            this.simpleButton4.ImageIndex = 10;
            this.simpleButton4.ImageList = this.ButtonImages;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton4.Location = new System.Drawing.Point(273, 630);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(201, 38);
            this.simpleButton4.TabIndex = 237;
            this.simpleButton4.Text = "Поставить на голосование";
            // 
            // btnStatementMode
            // 
            this.btnStatementMode.ImageIndex = 12;
            this.btnStatementMode.ImageList = this.ButtonImages;
            this.btnStatementMode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnStatementMode.Location = new System.Drawing.Point(52, 630);
            this.btnStatementMode.Name = "btnStatementMode";
            this.btnStatementMode.Size = new System.Drawing.Size(201, 38);
            this.btnStatementMode.TabIndex = 236;
            this.btnStatementMode.Text = "Режим выступления...";
            this.btnStatementMode.Click += new System.EventHandler(this.btnStatementMode_Click);
            // 
            // navigatorStatements
            // 
            this.navigatorStatements.Buttons.Append.Visible = false;
            this.navigatorStatements.Buttons.CancelEdit.Visible = false;
            this.navigatorStatements.Buttons.EndEdit.Visible = false;
            this.navigatorStatements.Buttons.ImageList = this.ButtonImages;
            this.navigatorStatements.Buttons.Remove.Visible = false;
            this.navigatorStatements.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Добавить запись", "Append"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Удалить запись", "Remove"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Внести изменения", "Edit")});
            this.navigatorStatements.DataSource = this.xpStatements;
            this.navigatorStatements.Dock = System.Windows.Forms.DockStyle.Top;
            this.navigatorStatements.Location = new System.Drawing.Point(0, 583);
            this.navigatorStatements.Name = "navigatorStatements";
            this.navigatorStatements.Size = new System.Drawing.Size(619, 31);
            this.navigatorStatements.TabIndex = 235;
            this.navigatorStatements.Text = "dataNavigator1";
            // 
            // xpStatements
            // 
            this.xpStatements.DeleteObjectOnRemove = true;
            this.xpStatements.ObjectType = typeof(VoteSystem.StatementObj);
            // 
            // StatementGrid
            // 
            this.StatementGrid.DataSource = this.xpStatements;
            this.StatementGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.StatementGrid.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.StatementGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.StatementGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.StatementGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.StatementGrid.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.StatementGrid.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.StatementGrid.EmbeddedNavigator.Name = "";
            this.StatementGrid.Location = new System.Drawing.Point(0, 0);
            this.StatementGrid.MainView = this.gridView10;
            this.StatementGrid.Name = "StatementGrid";
            this.StatementGrid.Size = new System.Drawing.Size(619, 583);
            this.StatementGrid.TabIndex = 234;
            this.StatementGrid.UseEmbeddedNavigator = true;
            this.StatementGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView10.GridControl = this.StatementGrid;
            this.gridView10.GroupCount = 1;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView10.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView10.OptionsBehavior.Editable = false;
            this.gridView10.OptionsDetail.AutoZoomDetail = true;
            this.gridView10.OptionsDetail.EnableMasterViewMode = false;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "№";
            this.gridColumn9.FieldName = "id";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.FixedWidth = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 0;
            this.gridColumn9.Width = 42;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Тема выступления";
            this.gridColumn10.FieldName = "Name";
            this.gridColumn10.MinWidth = 255;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 1;
            this.gridColumn10.Width = 426;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Дата создания";
            this.gridColumn11.FieldName = "CrDate";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 2;
            this.gridColumn11.Width = 128;
            // 
            // gridColumn12
            // 
            this.gridColumn12.FieldName = "Status";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 3;
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.groupControl1.Controls.Add(this.vGridControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(390, 677);
            this.groupControl1.TabIndex = 238;
            this.groupControl1.Tag = "";
            this.groupControl1.Text = "Панель свойств";
            // 
            // vGridControl1
            // 
            this.vGridControl1.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.vGridControl1.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.vGridControl1.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.vGridControl1.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.vGridControl1.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.vGridControl1.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.vGridControl1.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.vGridControl1.Location = new System.Drawing.Point(2, 20);
            this.vGridControl1.Name = "vGridControl1";
            this.vGridControl1.OptionsView.FixRowHeaderPanelWidth = true;
            this.vGridControl1.RecordWidth = 117;
            this.vGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit5,
            this.repositoryItemGridLookUpEdit5,
            this.repositoryItemComboBox30,
            this.repositoryItemComboBox31,
            this.repositoryItemComboBox32,
            this.repositoryItemComboBox33,
            this.repositoryItemComboBox34,
            this.repositoryItemComboBox35,
            this.repositoryItemDateEdit5,
            this.repositoryItemComboBox36,
            this.repositoryItemComboBox37,
            this.repositoryItemComboBox38,
            this.repositoryItemComboBox39,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemDateEdit7,
            this.repositoryItemMemoEdit5,
            this.repositoryItemComboBox40,
            this.repositoryItemSpinEdit3,
            this.repositoryItemComboBox41,
            this.repositoryItemComboBox42,
            this.repositoryItemMemoEdit6,
            this.repositoryItemMemoEdit7});
            this.vGridControl1.RowHeaderWidth = 83;
            this.vGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow4,
            this.categoryRow5,
            this.categoryRow7,
            this.categoryRow6});
            this.vGridControl1.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.vGridControl1.Size = new System.Drawing.Size(386, 510);
            this.vGridControl1.TabIndex = 12;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // repositoryItemGridLookUpEdit5
            // 
            this.repositoryItemGridLookUpEdit5.AutoHeight = false;
            this.repositoryItemGridLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit5.Name = "repositoryItemGridLookUpEdit5";
            this.repositoryItemGridLookUpEdit5.View = this.gridView11;
            // 
            // gridView11
            // 
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox30
            // 
            this.repositoryItemComboBox30.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox30.ImmediatePopup = true;
            this.repositoryItemComboBox30.Items.AddRange(new object[] {
            "партия 3",
            "партия 4"});
            this.repositoryItemComboBox30.Name = "repositoryItemComboBox30";
            // 
            // repositoryItemComboBox31
            // 
            this.repositoryItemComboBox31.AutoHeight = false;
            this.repositoryItemComboBox31.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox31.ImmediatePopup = true;
            this.repositoryItemComboBox31.Name = "repositoryItemComboBox31";
            this.repositoryItemComboBox31.Sorted = true;
            // 
            // repositoryItemComboBox32
            // 
            this.repositoryItemComboBox32.AutoHeight = false;
            this.repositoryItemComboBox32.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox32.Name = "repositoryItemComboBox32";
            // 
            // repositoryItemComboBox33
            // 
            this.repositoryItemComboBox33.AutoHeight = false;
            this.repositoryItemComboBox33.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox33.ImmediatePopup = true;
            this.repositoryItemComboBox33.Name = "repositoryItemComboBox33";
            // 
            // repositoryItemComboBox34
            // 
            this.repositoryItemComboBox34.AutoHeight = false;
            this.repositoryItemComboBox34.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox34.Name = "repositoryItemComboBox34";
            // 
            // repositoryItemComboBox35
            // 
            this.repositoryItemComboBox35.AutoHeight = false;
            this.repositoryItemComboBox35.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox35.Name = "repositoryItemComboBox35";
            // 
            // repositoryItemDateEdit5
            // 
            this.repositoryItemDateEdit5.AutoHeight = false;
            this.repositoryItemDateEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit5.Name = "repositoryItemDateEdit5";
            this.repositoryItemDateEdit5.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemComboBox36
            // 
            this.repositoryItemComboBox36.AutoHeight = false;
            this.repositoryItemComboBox36.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox36.Name = "repositoryItemComboBox36";
            // 
            // repositoryItemComboBox37
            // 
            this.repositoryItemComboBox37.AutoHeight = false;
            this.repositoryItemComboBox37.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox37.Name = "repositoryItemComboBox37";
            // 
            // repositoryItemComboBox38
            // 
            this.repositoryItemComboBox38.AutoHeight = false;
            this.repositoryItemComboBox38.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox38.Name = "repositoryItemComboBox38";
            // 
            // repositoryItemComboBox39
            // 
            this.repositoryItemComboBox39.AutoHeight = false;
            this.repositoryItemComboBox39.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox39.Name = "repositoryItemComboBox39";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            // 
            // repositoryItemDateEdit7
            // 
            this.repositoryItemDateEdit7.AutoHeight = false;
            this.repositoryItemDateEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit7.Name = "repositoryItemDateEdit7";
            this.repositoryItemDateEdit7.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemMemoEdit5
            // 
            this.repositoryItemMemoEdit5.Name = "repositoryItemMemoEdit5";
            // 
            // repositoryItemComboBox40
            // 
            this.repositoryItemComboBox40.AutoHeight = false;
            this.repositoryItemComboBox40.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox40.Name = "repositoryItemComboBox40";
            // 
            // repositoryItemComboBox41
            // 
            this.repositoryItemComboBox41.AutoHeight = false;
            this.repositoryItemComboBox41.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox41.Name = "repositoryItemComboBox41";
            // 
            // repositoryItemComboBox42
            // 
            this.repositoryItemComboBox42.AutoHeight = false;
            this.repositoryItemComboBox42.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox42.Name = "repositoryItemComboBox42";
            // 
            // repositoryItemMemoEdit6
            // 
            this.repositoryItemMemoEdit6.Name = "repositoryItemMemoEdit6";
            // 
            // repositoryItemMemoEdit7
            // 
            this.repositoryItemMemoEdit7.Name = "repositoryItemMemoEdit7";
            // 
            // categoryRow4
            // 
            this.categoryRow4.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow13,
            this.editorRow14,
            this.editorRow15,
            this.editorRow16,
            this.editorRow17,
            this.editorRow19});
            this.categoryRow4.Height = 19;
            this.categoryRow4.Name = "categoryRow4";
            this.categoryRow4.Properties.Caption = "Характеристики";
            // 
            // editorRow13
            // 
            this.editorRow13.Height = 28;
            this.editorRow13.Name = "editorRow13";
            this.editorRow13.Properties.Caption = "№ выступления";
            this.editorRow13.Properties.ReadOnly = true;
            this.editorRow13.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow13.Properties.Value = "12";
            // 
            // editorRow14
            // 
            this.editorRow14.Height = 47;
            this.editorRow14.Name = "editorRow14";
            this.editorRow14.Properties.Caption = "Тема выступления";
            this.editorRow14.Properties.ReadOnly = true;
            this.editorRow14.Properties.RowEdit = this.repositoryItemMemoEdit7;
            this.editorRow14.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow14.Properties.Value = "Обсуждение квартирного вопроса";
            // 
            // editorRow15
            // 
            this.editorRow15.Height = 80;
            this.editorRow15.Name = "editorRow15";
            this.editorRow15.Properties.Caption = "Описание";
            this.editorRow15.Properties.RowEdit = this.repositoryItemMemoEdit5;
            this.editorRow15.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow15.Properties.Value = "Обсудить темы: 1) окончания строительства в калужском районе 2) расселение многок" +
                "вартирного дома №67";
            // 
            // editorRow16
            // 
            this.editorRow16.Height = 28;
            this.editorRow16.Name = "editorRow16";
            this.editorRow16.Properties.Caption = "Дата/время создания";
            this.editorRow16.Properties.ReadOnly = true;
            this.editorRow16.Properties.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.editorRow16.Properties.Value = new System.DateTime(2010, 6, 14, 12, 38, 0, 0);
            // 
            // editorRow17
            // 
            this.editorRow17.Name = "editorRow17";
            this.editorRow17.Properties.Caption = "Тип выступления";
            this.editorRow17.Properties.ReadOnly = true;
            this.editorRow17.Properties.RowEdit = this.repositoryItemComboBox41;
            this.editorRow17.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow17.Properties.Value = "Обсуждение";
            // 
            // editorRow19
            // 
            this.editorRow19.Height = 25;
            this.editorRow19.Name = "editorRow19";
            this.editorRow19.Properties.Caption = "Статус";
            this.editorRow19.Properties.ReadOnly = true;
            this.editorRow19.Properties.RowEdit = this.repositoryItemComboBox42;
            this.editorRow19.Properties.Value = "Завершено";
            // 
            // categoryRow5
            // 
            this.categoryRow5.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow20,
            this.editorRow18,
            this.editorRow21});
            this.categoryRow5.Name = "categoryRow5";
            this.categoryRow5.Properties.Caption = "Параметры";
            // 
            // editorRow20
            // 
            this.editorRow20.Name = "editorRow20";
            this.editorRow20.Properties.Caption = "Время на выступление";
            this.editorRow20.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.editorRow20.Properties.Value = "2 мин.";
            // 
            // editorRow18
            // 
            this.editorRow18.Name = "editorRow18";
            this.editorRow18.Properties.Caption = "Отключать микрофон";
            this.editorRow18.Properties.Value = true;
            // 
            // editorRow21
            // 
            this.editorRow21.Name = "editorRow21";
            this.editorRow21.Properties.Caption = "Кол-во деп. max";
            this.editorRow21.Properties.Value = "10";
            // 
            // categoryRow7
            // 
            this.categoryRow7.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow22,
            this.editorRow23,
            this.editorRow24});
            this.categoryRow7.Name = "categoryRow7";
            this.categoryRow7.Properties.Caption = "Итоги";
            // 
            // editorRow22
            // 
            this.editorRow22.Name = "editorRow22";
            this.editorRow22.Properties.Caption = "Выступило, деп.";
            this.editorRow22.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.editorRow22.Properties.Value = "7";
            // 
            // editorRow23
            // 
            this.editorRow23.Name = "editorRow23";
            this.editorRow23.Properties.Caption = "Общее время, мин";
            this.editorRow23.Properties.Value = "12";
            // 
            // editorRow24
            // 
            this.editorRow24.Name = "editorRow24";
            this.editorRow24.Properties.Caption = "Среднее время, мин";
            this.editorRow24.Properties.Value = "1.75";
            // 
            // categoryRow6
            // 
            this.categoryRow6.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow25});
            this.categoryRow6.Name = "categoryRow6";
            this.categoryRow6.Properties.Caption = "Заметки";
            // 
            // editorRow25
            // 
            this.editorRow25.Height = 80;
            this.editorRow25.Name = "editorRow25";
            this.editorRow25.Properties.RowEdit = this.repositoryItemMemoEdit6;
            this.editorRow25.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow25.Properties.Value = "В процессе обсуждения был вынесен вопрос на голосвание под №143 О приватизации кв" +
                "артир в дородовском районе";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1,
            this.dataTable2,
            this.dataTable3});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn2,
            this.dataColumn12});
            this.dataTable1.TableName = "Delegates";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "ФИО";
            this.dataColumn1.ColumnName = "FIO";
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "№ Мик.";
            this.dataColumn3.ColumnName = "Seat";
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Округ №";
            this.dataColumn4.ColumnName = "OkrugNum";
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "Название окргуа";
            this.dataColumn5.ColumnName = "OkrugName";
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Фракция";
            this.dataColumn2.ColumnName = "Party";
            // 
            // dataColumn12
            // 
            this.dataColumn12.Caption = "Присутствие";
            this.dataColumn12.ColumnName = "Registration";
            this.dataColumn12.MaxLength = 3;
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn6,
            this.dataColumn8,
            this.dataColumn7});
            this.dataTable2.TableName = "Questions";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "№ воп.";
            this.dataColumn6.ColumnName = "QuestNum";
            // 
            // dataColumn8
            // 
            this.dataColumn8.Caption = "Дата/время";
            this.dataColumn8.ColumnName = "DateTime";
            this.dataColumn8.DataType = typeof(System.DateTime);
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "Заголовок";
            this.dataColumn7.ColumnName = "Caption";
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.QuestNum,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn16});
            this.dataTable3.TableName = "Voting";
            // 
            // QuestNum
            // 
            this.QuestNum.Caption = "№ воп.";
            this.QuestNum.ColumnName = "Number";
            this.QuestNum.DataType = typeof(int);
            // 
            // dataColumn9
            // 
            this.dataColumn9.Caption = "Заголовок";
            this.dataColumn9.ColumnName = "Caption";
            // 
            // dataColumn10
            // 
            this.dataColumn10.Caption = "Дата/время гол.";
            this.dataColumn10.ColumnName = "DateTime";
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "Решение";
            this.dataColumn11.ColumnName = "Decision";
            // 
            // dataColumn16
            // 
            this.dataColumn16.Caption = "Кворум";
            this.dataColumn16.ColumnName = "Quorum";
            this.dataColumn16.DefaultValue = "Есть";
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(48, 48);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // gridView3
            // 
            this.gridView3.Name = "gridView3";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(538, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 28);
            this.button2.TabIndex = 198;
            this.button2.Text = "Подробнее...";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.DarkGreen;
            this.label4.Location = new System.Drawing.Point(407, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 17);
            this.label4.TabIndex = 189;
            this.label4.Text = "Принято";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(627, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 17);
            this.label5.TabIndex = 197;
            this.label5.Text = "8";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(544, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 196;
            this.label6.Text = "Проголосовали:";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(487, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 17);
            this.label7.TabIndex = 195;
            this.label7.Text = "8";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(407, 178);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 194;
            this.label8.Text = "Присутствуют:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.label56);
            this.groupBox7.Controls.Add(this.domainUpDown3);
            this.groupBox7.Controls.Add(this.label57);
            this.groupBox7.Location = new System.Drawing.Point(401, 39);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(261, 136);
            this.groupBox7.TabIndex = 192;
            this.groupBox7.TabStop = false;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(185, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 17);
            this.label9.TabIndex = 173;
            this.label9.Text = "8";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(185, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 17);
            this.label10.TabIndex = 172;
            this.label10.Text = "8";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(185, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 17);
            this.label11.TabIndex = 171;
            this.label11.Text = "8";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(185, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 17);
            this.label13.TabIndex = 170;
            this.label13.Text = "8";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(86, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 17);
            this.label16.TabIndex = 169;
            this.label16.Text = "8";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(86, 113);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 17);
            this.label17.TabIndex = 168;
            this.label17.Text = "8";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(86, 86);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 17);
            this.label18.TabIndex = 167;
            this.label18.Text = "8";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(185, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 166;
            this.label19.Text = "Процент:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(86, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 13);
            this.label20.TabIndex = 165;
            this.label20.Text = "Голоса:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 111);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 13);
            this.label21.TabIndex = 164;
            this.label21.Text = "Не голос. :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 86);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 13);
            this.label22.TabIndex = 163;
            this.label22.Text = "ВОЗДЕР. :";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(86, 58);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 17);
            this.label23.TabIndex = 155;
            this.label23.Text = "8";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 38);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(24, 13);
            this.label56.TabIndex = 153;
            this.label56.Text = "ЗА:";
            // 
            // domainUpDown3
            // 
            this.domainUpDown3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.domainUpDown3.Location = new System.Drawing.Point(80, 400);
            this.domainUpDown3.Name = "domainUpDown3";
            this.domainUpDown3.Size = new System.Drawing.Size(77, 20);
            this.domainUpDown3.TabIndex = 145;
            this.domainUpDown3.Text = "2";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(6, 62);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(58, 13);
            this.label57.TabIndex = 139;
            this.label57.Text = "ПРОТИВ :";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(407, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(34, 13);
            this.label58.TabIndex = 193;
            this.label58.Text = "Итог:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label59);
            this.groupBox8.Controls.Add(this.label60);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this.label62);
            this.groupBox8.Controls.Add(this.label63);
            this.groupBox8.Controls.Add(this.label64);
            this.groupBox8.Controls.Add(this.label65);
            this.groupBox8.Controls.Add(this.label66);
            this.groupBox8.Controls.Add(this.label67);
            this.groupBox8.Controls.Add(this.label68);
            this.groupBox8.Controls.Add(this.label69);
            this.groupBox8.Controls.Add(this.label70);
            this.groupBox8.Controls.Add(this.domainUpDown4);
            this.groupBox8.Controls.Add(this.label71);
            this.groupBox8.Location = new System.Drawing.Point(5, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(326, 181);
            this.groupBox8.TabIndex = 190;
            this.groupBox8.TabStop = false;
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label59.Location = new System.Drawing.Point(86, 67);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(169, 17);
            this.label59.TabIndex = 164;
            this.label59.Text = "Открытое";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 68);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(64, 13);
            this.label60.TabIndex = 163;
            this.label60.Text = "Тип голос.:";
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label61.Location = new System.Drawing.Point(85, 159);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(170, 16);
            this.label61.TabIndex = 162;
            this.label61.Text = "До 2 мин";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 159);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(43, 13);
            this.label62.TabIndex = 161;
            this.label62.Text = "Время:";
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label63.Location = new System.Drawing.Point(85, 129);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(235, 30);
            this.label63.TabIndex = 160;
            this.label63.Text = "Простое большинство, голосов \"За\" более 50%";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 129);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(58, 13);
            this.label64.TabIndex = 159;
            this.label64.Text = "Критерий:";
            // 
            // label65
            // 
            this.label65.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label65.Location = new System.Drawing.Point(85, 107);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(170, 26);
            this.label65.TabIndex = 158;
            this.label65.Text = "От присутствующих > 75";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 107);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(43, 13);
            this.label66.TabIndex = 157;
            this.label66.Text = "Квота :";
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label67.Location = new System.Drawing.Point(85, 86);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(170, 17);
            this.label67.TabIndex = 156;
            this.label67.Text = "Для голосования";
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label68.Location = new System.Drawing.Point(86, 49);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(169, 17);
            this.label68.TabIndex = 155;
            this.label68.Text = "Парламентское голос.";
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label69.Location = new System.Drawing.Point(6, 8);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(314, 36);
            this.label69.TabIndex = 154;
            this.label69.Text = "Утверждение расходной части Утверждение расходной части Утверждение расходной час" +
                "ти";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 86);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(80, 13);
            this.label70.TabIndex = 151;
            this.label70.Text = "Тип вопроса:  ";
            // 
            // domainUpDown4
            // 
            this.domainUpDown4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.domainUpDown4.Location = new System.Drawing.Point(80, 400);
            this.domainUpDown4.Name = "domainUpDown4";
            this.domainUpDown4.Size = new System.Drawing.Size(77, 20);
            this.domainUpDown4.TabIndex = 145;
            this.domainUpDown4.Text = "2";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(6, 50);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(64, 13);
            this.label71.TabIndex = 139;
            this.label71.Text = "Вид голос.:";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(20, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(138, 13);
            this.label72.TabIndex = 191;
            this.label72.Text = "Характеристика вопроса:";
            // 
            // gridControl5
            // 
            this.gridControl5.DataMember = "Voting";
            this.gridControl5.DataSource = this.dataSet1;
            this.gridControl5.EmbeddedNavigator.Name = "";
            this.gridControl5.Location = new System.Drawing.Point(3, 199);
            this.gridControl5.MainView = this.gridView6;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(705, 320);
            this.gridControl5.TabIndex = 5;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView6.GridControl = this.gridControl5;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "№ воп.";
            this.gridColumn3.FieldName = "Number";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Заголовок";
            this.gridColumn4.FieldName = "Caption";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Дата/время гол.";
            this.gridColumn5.FieldName = "DateTime";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Результат";
            this.gridColumn6.FieldName = "Result";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barbtnExit);
            this.ribbonPageGroup5.KeyTip = "";
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Сервис";
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanel1.FloatLocation = new System.Drawing.Point(584, 368);
            this.dockPanel1.FloatSize = new System.Drawing.Size(333, 200);
            this.dockPanel1.FloatVertical = true;
            this.dockPanel1.ID = new System.Guid("fa62cbc0-d92e-40cd-ad40-88288aa489c3");
            this.dockPanel1.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.SavedIndex = 0;
            this.dockPanel1.Size = new System.Drawing.Size(333, 200);
            this.dockPanel1.Text = "dockPanel1";
            this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Location = new System.Drawing.Point(2, 22);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(329, 176);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // PropertyDataSet
            // 
            this.PropertyDataSet.DataSetName = "NewDataSet";
            this.PropertyDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable4});
            // 
            // dataTable4
            // 
            this.dataTable4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15});
            this.dataTable4.TableName = "Delegates";
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "Фамилия";
            this.dataColumn13.ColumnName = "Family";
            // 
            // dataColumn14
            // 
            this.dataColumn14.Caption = "Имя";
            this.dataColumn14.ColumnName = "FirstName";
            // 
            // dataColumn15
            // 
            this.dataColumn15.Caption = "Отчество";
            this.dataColumn15.ColumnName = "SecondName";
            // 
            // repositoryItemPictureEdit4
            // 
            this.repositoryItemPictureEdit4.Name = "repositoryItemPictureEdit4";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // repositoryItemDateEdit4
            // 
            this.repositoryItemDateEdit4.AutoHeight = false;
            this.repositoryItemDateEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit4.Name = "repositoryItemDateEdit4";
            this.repositoryItemDateEdit4.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // editorRow35
            // 
            this.editorRow35.Height = 28;
            this.editorRow35.Name = "editorRow35";
            this.editorRow35.Properties.Caption = "Фамилия";
            this.editorRow35.Properties.FieldName = "Family";
            this.editorRow35.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow35.Properties.Value = "11111";
            // 
            // editorRow36
            // 
            this.editorRow36.Height = 28;
            this.editorRow36.Name = "editorRow36";
            this.editorRow36.Properties.Caption = "Имя";
            this.editorRow36.Properties.FieldName = "Name";
            this.editorRow36.Properties.ReadOnly = true;
            this.editorRow36.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // editorRow37
            // 
            this.editorRow37.Height = 26;
            this.editorRow37.Name = "editorRow37";
            this.editorRow37.Properties.Caption = "Отчество";
            this.editorRow37.Properties.FieldName = "SecondName";
            this.editorRow37.Properties.ReadOnly = true;
            // 
            // editorRow38
            // 
            this.editorRow38.Height = 80;
            this.editorRow38.Name = "editorRow38";
            this.editorRow38.Properties.Caption = "Фото";
            this.editorRow38.Properties.FieldName = "Photo";
            this.editorRow38.Properties.ReadOnly = true;
            this.editorRow38.Properties.RowEdit = this.repositoryItemPictureEdit4;
            // 
            // editorRow39
            // 
            this.editorRow39.Name = "editorRow39";
            this.editorRow39.Properties.Caption = "Фракция";
            this.editorRow39.Properties.FieldName = "Party";
            this.editorRow39.Properties.ReadOnly = true;
            // 
            // editorRow40
            // 
            this.editorRow40.Name = "editorRow40";
            this.editorRow40.Properties.Caption = "Округ";
            this.editorRow40.Properties.FieldName = "Okrug";
            this.editorRow40.Properties.ReadOnly = true;
            this.editorRow40.Properties.RowEdit = this.repositoryItemTextEdit4;
            this.editorRow40.Properties.Value = "Каменский";
            // 
            // editorRow41
            // 
            this.editorRow41.Name = "editorRow41";
            this.editorRow41.Properties.Caption = "Округ №";
            // 
            // editorRow42
            // 
            this.editorRow42.Name = "editorRow42";
            this.editorRow42.Properties.Caption = "Регистрация";
            // 
            // editorRow43
            // 
            this.editorRow43.Name = "editorRow43";
            this.editorRow43.Properties.Caption = "Партия";
            // 
            // editorRow44
            // 
            this.editorRow44.Name = "editorRow44";
            this.editorRow44.Properties.Caption = "Должность";
            // 
            // editorRow45
            // 
            this.editorRow45.Name = "editorRow45";
            this.editorRow45.Properties.Caption = "Дата рождения";
            this.editorRow45.Properties.Format.FormatString = "d";
            this.editorRow45.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.editorRow45.Properties.RowEdit = this.repositoryItemDateEdit4;
            this.editorRow45.Properties.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.editorRow45.Properties.Value = new System.DateTime(2010, 4, 8, 15, 36, 32, 0);
            // 
            // editorRow46
            // 
            this.editorRow46.Name = "editorRow46";
            this.editorRow46.Properties.Caption = "Образование";
            // 
            // editorRow47
            // 
            this.editorRow47.Name = "editorRow47";
            this.editorRow47.Properties.Caption = "Статус";
            // 
            // propertyGridControl4
            // 
            this.propertyGridControl4.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.propertyGridControl4.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.propertyGridControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.propertyGridControl4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSet1, "Delegates.Registration", true));
            this.propertyGridControl4.DefaultEditors.AddRange(new DevExpress.XtraVerticalGrid.Rows.DefaultEditor[] {
            new DevExpress.XtraVerticalGrid.Rows.DefaultEditor(null, null)});
            this.propertyGridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridControl4.Location = new System.Drawing.Point(2, 20);
            this.propertyGridControl4.Name = "propertyGridControl4";
            this.propertyGridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit4,
            this.repositoryItemTextEdit4,
            this.repositoryItemDateEdit4});
            this.propertyGridControl4.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow35,
            this.editorRow36,
            this.editorRow37,
            this.editorRow38,
            this.editorRow39,
            this.editorRow40,
            this.editorRow41,
            this.editorRow42,
            this.editorRow43,
            this.editorRow44,
            this.editorRow45,
            this.editorRow46,
            this.editorRow47});
            this.propertyGridControl4.ServiceProvider = null;
            this.propertyGridControl4.Size = new System.Drawing.Size(276, 659);
            this.propertyGridControl4.TabIndex = 6;
            // 
            // tmrRegStart
            // 
            this.tmrRegStart.Interval = 1000;
            this.tmrRegStart.Tick += new System.EventHandler(this.tmrRegStart_Tick);
            // 
            // TimerImages
            // 
            this.TimerImages.ImageSize = new System.Drawing.Size(96, 96);
            this.TimerImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("TimerImages.ImageStream")));
            // 
            // tmrRotation
            // 
            this.tmrRotation.Interval = 500;
            this.tmrRotation.Tick += new System.EventHandler(this.tmrRotation_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 899);
            this.Controls.Add(this.clientPanel);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Система голосования";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuMicSeat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit59.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit60.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit61.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit62.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit63.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit64.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit65.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit66.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit67.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit68.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit69.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit70.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit71.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit72.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit73.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit74.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit49.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit50.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit55.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit56.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit57.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit58.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit75.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit76.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit77.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit78.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit79.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit80.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit81.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit82.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit83.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit84.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit85.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit86.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit87.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit88.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit89.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit90.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit91.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit92.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit93.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit94.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit95.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit96.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit97.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit98.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit99.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit100.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit101.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit102.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit103.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit104.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit105.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit106.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit107.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit108.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit109.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit110.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit111.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit112.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit113.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit114.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit115.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit116.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit117.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit118.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit119.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit120.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit121.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit122.Properties)).EndInit();
            this.MicSeatMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clientPanel)).EndInit();
            this.clientPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainTabControl)).EndInit();
            this.MainTabControl.ResumeLayout(false);
            this.pgSession.ResumeLayout(false);
            this.pgSession.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHall.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQourum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption.Properties)).EndInit();
            this.pgDelegates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            this.pgSelQuestions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xpQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelQuestGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSelQuestPropsIn)).EndInit();
            this.grpSelQuestPropsIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_DecisionProcTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_VoteQntyTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cedt_QuotaProcType_Present)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cedt_QuotaProcType_Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_VoteKinds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_votetypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repository_cmb_ReadNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).EndInit();
            this.pgQuorumGraphic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl_QPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarRegistration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuorumQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRegTime.Properties)).EndInit();
            this.pgResQuestions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xpResQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpResQuestProps)).EndInit();
            this.grpResQuestProps.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpResQuestPropsIn)).EndInit();
            this.grpResQuestPropsIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            this.grpVoteResults.ResumeLayout(false);
            this.grpVoteResults.PerformLayout();
            this.pgStatements.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xpStatements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatementGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertyDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimerImages)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraEditors.PanelControl clientPanel;
        private DevExpress.XtraBars.BarButtonItem barbtnPredsedSendMsg;
        private DevExpress.XtraBars.BarButtonItem barbtnMonitorMsg;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barbtnExit;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem itmAgenda;
        private DevExpress.XtraNavBar.NavBarItem itmDelegates;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraNavBar.NavBarItem itmQuestions;
        private DevExpress.XtraNavBar.NavBarItem itmRegistration;
        private DevExpress.XtraNavBar.NavBarItem itmResults;
        private DevExpress.XtraNavBar.NavBarItem itmReport;
        private DevExpress.XtraNavBar.NavBarItem itmStatements;
        private DevExpress.XtraNavBar.NavBarItem navBarItem9;
        private DevExpress.XtraNavBar.NavBarItem navBarItem10;
        private DevExpress.XtraNavBar.NavBarItem navBarItem11;
        private DevExpress.XtraNavBar.NavBarItem navBarItem12;
        private DevExpress.Utils.ImageCollection RibbonImages;
        private DevExpress.Utils.ImageCollection NavBarImages;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataTable dataTable2;
        private System.Data.DataColumn dataColumn6;
        private DevExpress.XtraTab.XtraTabControl MainTabControl;
        private DevExpress.XtraTab.XtraTabPage pgSession;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.MemoEdit txtContent;
        private DevExpress.XtraEditors.TextEdit txtHall;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private System.Windows.Forms.CheckBox chbRegAfterTime;
        private DevExpress.XtraEditors.TextEdit txtRegTime;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtQourum;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit txtStartDate;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit txtCaption;
        private DevExpress.XtraTab.XtraTabPage pgSelQuestions;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataTable dataTable3;
        private System.Data.DataColumn QuestNum;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private DevExpress.XtraTab.XtraTabPage pgResQuestions;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.DomainUpDown domainUpDown3;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.DomainUpDown domainUpDown4;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraTab.XtraTabPage pgStatements;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraTab.XtraTabPage pgQuorumGraphic;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit txtRegQnty;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtDelegateQnty;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl lblRegistration;
        private DevExpress.XtraEditors.PictureEdit picRegTime;
        private DevExpress.XtraEditors.SimpleButton btnRegStart;
        private DevExpress.XtraEditors.TextEdit txtQuorumQnty;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private System.Data.DataColumn dataColumn2;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataSet PropertyDataSet;
        private System.Data.DataTable dataTable4;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn16;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.ListBoxControl lbHistory;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.Xpo.XPCollection xpDelegates;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow35;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow36;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow37;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow38;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow39;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow40;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow41;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow42;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow43;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow44;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow45;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow46;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow47;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridControl4;
        private DevExpress.Xpo.XPCollection xpQuestions;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label42;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.PictureEdit Mic_2;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.PictureEdit Mic_1;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.PictureEdit pictureEdit19;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.PictureEdit pictureEdit20;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.PictureEdit pictureEdit21;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.PictureEdit pictureEdit22;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.PictureEdit pictureEdit23;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.PictureEdit pictureEdit24;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.PictureEdit pictureEdit25;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.PictureEdit pictureEdit26;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.PictureEdit pictureEdit27;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.PictureEdit pictureEdit28;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.PictureEdit pictureEdit29;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.PictureEdit pictureEdit30;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.PictureEdit pictureEdit31;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.PictureEdit pictureEdit32;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.PictureEdit pictureEdit33;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.PictureEdit pictureEdit34;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.PictureEdit pictureEdit35;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.PictureEdit pictureEdit36;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.PictureEdit pictureEdit37;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.PictureEdit pictureEdit38;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.PictureEdit pictureEdit39;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.PictureEdit pictureEdit40;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.PictureEdit pictureEdit41;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.PictureEdit pictureEdit42;
        private DevExpress.XtraEditors.LabelControl labelControl81;
        private DevExpress.XtraEditors.PictureEdit pictureEdit59;
        private DevExpress.XtraEditors.LabelControl labelControl82;
        private DevExpress.XtraEditors.PictureEdit pictureEdit60;
        private DevExpress.XtraEditors.LabelControl labelControl83;
        private DevExpress.XtraEditors.PictureEdit pictureEdit61;
        private DevExpress.XtraEditors.LabelControl labelControl84;
        private DevExpress.XtraEditors.PictureEdit pictureEdit62;
        private DevExpress.XtraEditors.LabelControl labelControl85;
        private DevExpress.XtraEditors.PictureEdit pictureEdit63;
        private DevExpress.XtraEditors.LabelControl labelControl86;
        private DevExpress.XtraEditors.PictureEdit pictureEdit64;
        private DevExpress.XtraEditors.LabelControl labelControl87;
        private DevExpress.XtraEditors.PictureEdit pictureEdit65;
        private DevExpress.XtraEditors.LabelControl labelControl88;
        private DevExpress.XtraEditors.PictureEdit pictureEdit66;
        private DevExpress.XtraEditors.LabelControl labelControl89;
        private DevExpress.XtraEditors.PictureEdit pictureEdit67;
        private DevExpress.XtraEditors.LabelControl labelControl90;
        private DevExpress.XtraEditors.PictureEdit pictureEdit68;
        private DevExpress.XtraEditors.LabelControl labelControl91;
        private DevExpress.XtraEditors.PictureEdit pictureEdit69;
        private DevExpress.XtraEditors.LabelControl labelControl92;
        private DevExpress.XtraEditors.PictureEdit pictureEdit70;
        private DevExpress.XtraEditors.LabelControl labelControl93;
        private DevExpress.XtraEditors.PictureEdit pictureEdit71;
        private DevExpress.XtraEditors.LabelControl labelControl94;
        private DevExpress.XtraEditors.PictureEdit pictureEdit72;
        private DevExpress.XtraEditors.LabelControl labelControl95;
        private DevExpress.XtraEditors.PictureEdit pictureEdit73;
        private DevExpress.XtraEditors.LabelControl labelControl96;
        private DevExpress.XtraEditors.PictureEdit pictureEdit74;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.PictureEdit pictureEdit43;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.PictureEdit pictureEdit44;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.PictureEdit pictureEdit45;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.PictureEdit pictureEdit46;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.PictureEdit pictureEdit47;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraEditors.PictureEdit pictureEdit48;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraEditors.PictureEdit pictureEdit49;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraEditors.PictureEdit pictureEdit50;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraEditors.PictureEdit pictureEdit51;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraEditors.PictureEdit pictureEdit52;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraEditors.PictureEdit pictureEdit53;
        private DevExpress.XtraEditors.LabelControl labelControl76;
        private DevExpress.XtraEditors.PictureEdit pictureEdit54;
        private DevExpress.XtraEditors.LabelControl labelControl77;
        private DevExpress.XtraEditors.PictureEdit pictureEdit55;
        private DevExpress.XtraEditors.LabelControl labelControl78;
        private DevExpress.XtraEditors.PictureEdit pictureEdit56;
        private DevExpress.XtraEditors.LabelControl labelControl79;
        private DevExpress.XtraEditors.PictureEdit pictureEdit57;
        private DevExpress.XtraEditors.LabelControl labelControl80;
        private DevExpress.XtraEditors.PictureEdit pictureEdit58;
        private DevExpress.XtraEditors.LabelControl labelControl97;
        private DevExpress.XtraEditors.PictureEdit pictureEdit75;
        private DevExpress.XtraEditors.LabelControl labelControl98;
        private DevExpress.XtraEditors.PictureEdit pictureEdit76;
        private DevExpress.XtraEditors.LabelControl labelControl99;
        private DevExpress.XtraEditors.PictureEdit pictureEdit77;
        private DevExpress.XtraEditors.LabelControl labelControl100;
        private DevExpress.XtraEditors.PictureEdit pictureEdit78;
        private DevExpress.XtraEditors.LabelControl labelControl101;
        private DevExpress.XtraEditors.PictureEdit pictureEdit79;
        private DevExpress.XtraEditors.LabelControl labelControl102;
        private DevExpress.XtraEditors.PictureEdit pictureEdit80;
        private DevExpress.XtraEditors.LabelControl labelControl103;
        private DevExpress.XtraEditors.PictureEdit pictureEdit81;
        private DevExpress.XtraEditors.LabelControl labelControl104;
        private DevExpress.XtraEditors.PictureEdit pictureEdit82;
        private DevExpress.XtraEditors.LabelControl labelControl105;
        private DevExpress.XtraEditors.PictureEdit pictureEdit83;
        private DevExpress.XtraEditors.LabelControl labelControl106;
        private DevExpress.XtraEditors.PictureEdit pictureEdit84;
        private DevExpress.XtraEditors.LabelControl labelControl107;
        private DevExpress.XtraEditors.PictureEdit pictureEdit85;
        private DevExpress.XtraEditors.LabelControl labelControl108;
        private DevExpress.XtraEditors.PictureEdit pictureEdit86;
        private DevExpress.XtraEditors.LabelControl labelControl109;
        private DevExpress.XtraEditors.PictureEdit pictureEdit87;
        private DevExpress.XtraEditors.LabelControl labelControl110;
        private DevExpress.XtraEditors.PictureEdit pictureEdit88;
        private DevExpress.XtraEditors.LabelControl labelControl111;
        private DevExpress.XtraEditors.PictureEdit pictureEdit89;
        private DevExpress.XtraEditors.LabelControl labelControl112;
        private DevExpress.XtraEditors.PictureEdit pictureEdit90;
        private DevExpress.XtraEditors.LabelControl labelControl113;
        private DevExpress.XtraEditors.PictureEdit pictureEdit91;
        private DevExpress.XtraEditors.LabelControl labelControl114;
        private DevExpress.XtraEditors.PictureEdit pictureEdit92;
        private DevExpress.XtraEditors.LabelControl labelControl115;
        private DevExpress.XtraEditors.PictureEdit pictureEdit93;
        private DevExpress.XtraEditors.LabelControl labelControl116;
        private DevExpress.XtraEditors.PictureEdit pictureEdit94;
        private DevExpress.XtraEditors.LabelControl labelControl117;
        private DevExpress.XtraEditors.PictureEdit pictureEdit95;
        private DevExpress.XtraEditors.LabelControl labelControl118;
        private DevExpress.XtraEditors.PictureEdit pictureEdit96;
        private DevExpress.XtraEditors.LabelControl labelControl119;
        private DevExpress.XtraEditors.PictureEdit pictureEdit97;
        private DevExpress.XtraEditors.LabelControl labelControl120;
        private DevExpress.XtraEditors.PictureEdit pictureEdit98;
        private DevExpress.XtraEditors.LabelControl labelControl121;
        private DevExpress.XtraEditors.PictureEdit pictureEdit99;
        private DevExpress.XtraEditors.LabelControl labelControl122;
        private DevExpress.XtraEditors.PictureEdit pictureEdit100;
        private DevExpress.XtraEditors.LabelControl labelControl123;
        private DevExpress.XtraEditors.PictureEdit pictureEdit101;
        private DevExpress.XtraEditors.LabelControl labelControl124;
        private DevExpress.XtraEditors.PictureEdit pictureEdit102;
        private DevExpress.XtraEditors.LabelControl labelControl125;
        private DevExpress.XtraEditors.PictureEdit pictureEdit103;
        private DevExpress.XtraEditors.LabelControl labelControl126;
        private DevExpress.XtraEditors.PictureEdit pictureEdit104;
        private DevExpress.XtraEditors.LabelControl labelControl127;
        private DevExpress.XtraEditors.PictureEdit pictureEdit105;
        private DevExpress.XtraEditors.LabelControl labelControl128;
        private DevExpress.XtraEditors.PictureEdit pictureEdit106;
        private DevExpress.XtraEditors.LabelControl labelControl129;
        private DevExpress.XtraEditors.PictureEdit pictureEdit107;
        private DevExpress.XtraEditors.LabelControl labelControl130;
        private DevExpress.XtraEditors.PictureEdit pictureEdit108;
        private DevExpress.XtraEditors.LabelControl labelControl131;
        private DevExpress.XtraEditors.PictureEdit pictureEdit109;
        private DevExpress.XtraEditors.LabelControl labelControl132;
        private DevExpress.XtraEditors.PictureEdit pictureEdit110;
        private DevExpress.XtraEditors.LabelControl labelControl133;
        private DevExpress.XtraEditors.PictureEdit pictureEdit111;
        private DevExpress.XtraEditors.LabelControl labelControl134;
        private DevExpress.XtraEditors.PictureEdit pictureEdit112;
        private DevExpress.XtraEditors.LabelControl labelControl135;
        private DevExpress.XtraEditors.PictureEdit pictureEdit113;
        private DevExpress.XtraEditors.LabelControl labelControl136;
        private DevExpress.XtraEditors.PictureEdit pictureEdit114;
        private DevExpress.XtraEditors.LabelControl labelControl137;
        private DevExpress.XtraEditors.PictureEdit pictureEdit115;
        private DevExpress.XtraEditors.LabelControl labelControl138;
        private DevExpress.XtraEditors.PictureEdit pictureEdit116;
        private DevExpress.XtraEditors.LabelControl labelControl139;
        private DevExpress.XtraEditors.PictureEdit pictureEdit117;
        private DevExpress.XtraEditors.LabelControl labelControl140;
        private DevExpress.XtraEditors.PictureEdit pictureEdit118;
        private DevExpress.XtraEditors.LabelControl labelControl141;
        private DevExpress.XtraEditors.PictureEdit pictureEdit119;
        private DevExpress.XtraEditors.LabelControl labelControl142;
        private DevExpress.XtraEditors.PictureEdit pictureEdit120;
        private DevExpress.XtraEditors.LabelControl labelControl143;
        private DevExpress.XtraEditors.PictureEdit pictureEdit121;
        private DevExpress.XtraEditors.LabelControl labelControl144;
        private DevExpress.XtraEditors.PictureEdit pictureEdit122;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label92;
        private DevExpress.XtraEditors.ProgressBarControl progressBarRegistration;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControl_QPage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox7;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox17;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_LastName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_FirstName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_SecondName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_q_Region;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_RegionName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_RegionNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_q_Info;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_Fraction;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_PartyName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_q_Seat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_MicNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_RowNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_q_SeatNum;
        private DevExpress.XtraEditors.SimpleButton btnRegCancel;
        private System.Windows.Forms.ContextMenuStrip MicSeatMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private DevExpress.XtraBars.PopupMenu menuMicSeat;
        private DevExpress.XtraBars.BarButtonItem cntSeatFree;
        private DevExpress.XtraBars.BarButtonItem cntSeatSelect;
        private DevExpress.XtraBars.BarEditItem cntSwitherPhysical;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.BarEditItem cntSwitherLogical;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraEditors.LabelControl lblQuorumFinished;
        private DevExpress.XtraEditors.LabelControl lblRegTime;
        private System.Windows.Forms.Timer tmrRegStart;
        private DevExpress.Utils.ImageCollection TimerImages;
        private System.Windows.Forms.Timer tmrRotation;
        private DevExpress.Xpo.XPCollection xpResQuestions;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtVoteTime;
        private DevExpress.XtraEditors.LabelControl labelControl145;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.BarCheckItem barbtnClearMonitor;
        private DevExpress.XtraBars.BarCheckItem barbtnSplashToMonitor;
        private DevExpress.XtraBars.BarButtonItem barbtnInfoToMonitor;
        private DevExpress.XtraTab.XtraTabPage pgDelegates;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gridDelegates;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNum;
        private DevExpress.XtraGrid.Columns.GridColumn colFraction;
        private DevExpress.XtraGrid.Columns.GridColumn colPartyName;
        private DevExpress.XtraGrid.Columns.GridColumn colIsRegistered;
        private DevExpress.XtraEditors.DataNavigator navigatorDelegates;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl1;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControl;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catFIO;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLastName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFirstName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSecondName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catRegion;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catInfo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFraction;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPartyName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catSeat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMicNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRowNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSeatNum;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraEditors.SimpleButton btnQuestVoteMode;
        private DevExpress.XtraEditors.DataNavigator navigatorQuestions;
        private DevExpress.XtraGrid.GridControl SelQuestGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestCaption;
        private DevExpress.XtraGrid.Columns.GridColumn colCrDate;
        private DevExpress.XtraEditors.GroupControl grpSelQuestPropsIn;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlQuest;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox9;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox10;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox11;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox12;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox13;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox14;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox15;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox16;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repository_cmb_DecisionProcTypes;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repository_cmb_VoteQntyTypes;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repository_cedt_QuotaProcType_Present;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repository_cedt_QuotaProcType_Total;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repository_cmb_VoteKinds;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repository_cmb_votetypes;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repository_cmb_ReadNum;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit4;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_sq_Characteristics;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_Number;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_Caption;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_Description;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_CrDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_VoteType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_VoteKind;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_ReadNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_sq_Criteries;
        private DevExpress.XtraVerticalGrid.Rows.MultiEditorRow row_sq_DecisionProcValue;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_DecisionProcType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_VoteQnty;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_QuotaProcentValue;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_QuotaProcentType_Present;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_QuotaProcentType_Total;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_sq_Notes;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_Notes;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnPropQuestApply;
        private DevExpress.XtraEditors.SimpleButton btnPropQuestCancel;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraEditors.SimpleButton btnQuestVoteDetail;
        private DevExpress.XtraEditors.SimpleButton btnQuestVoteCancel;
        private DevExpress.XtraEditors.SimpleButton btnQuestNewRead;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.DataNavigator navigatorResQuestions;
        private DevExpress.XtraEditors.GroupControl grpResQuestProps;
        private DevExpress.XtraEditors.GroupControl grpResQuestPropsIn;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlQuest2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox18;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox19;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox20;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox21;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox22;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox23;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox24;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox25;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox26;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox27;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox28;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox29;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow5;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow6;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow7;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow3;
        private DevExpress.XtraVerticalGrid.Rows.MultiEditorRow multiEditorRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow8;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow9;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow10;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow11;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow12;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private System.Windows.Forms.Label lblVoteDateTime;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private System.Windows.Forms.Label lblProcQnty;
        private System.Windows.Forms.GroupBox grpVoteResults;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label lblProcAye;
        private System.Windows.Forms.Label lblVoteAye;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label lblNonProcQnty;
        private System.Windows.Forms.Label lblNonVoteQnty;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label lblProcAgainst;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label lblProcAbstain;
        private System.Windows.Forms.Label lblVoteAgainst;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label lblVoteAbstain;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.DomainUpDown domainUpDown5;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label lblDecision;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label lblVoteQnty;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarSubItem barSubItemSession;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem17;
        private DevExpress.XtraBars.BarButtonItem barButtonItem18;
        private DevExpress.XtraBars.BarButtonItem barButtonItem19;
        private DevExpress.XtraBars.BarSubItem barSubItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem20;
        private DevExpress.XtraBars.BarButtonItem barButtonItem21;
        private DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraBars.BarButtonItem barButtonItem23;
        private DevExpress.XtraBars.BarButtonItem barButtonItem24;
        private DevExpress.XtraBars.BarButtonItem barButtonItem25;
        private DevExpress.XtraBars.BarButtonItem barButtonItem26;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private DevExpress.XtraEditors.SimpleButton btnStatementMode;
        private DevExpress.XtraEditors.DataNavigator navigatorStatements;
        private DevExpress.XtraGrid.GridControl StatementGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraVerticalGrid.VGridControl vGridControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox30;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox31;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox32;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox33;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox34;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox35;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox36;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox37;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox38;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox39;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox40;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox41;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox42;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit7;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow13;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow14;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow15;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow16;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow17;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow19;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow6;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow25;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.Xpo.XPCollection xpStatements;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow5;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow20;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow18;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow21;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow7;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow22;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow23;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow24;

    }
}