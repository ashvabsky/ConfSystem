﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public interface IVoteQuestPage
    {
        IMsgMethods GetShowInterface();
        IQuestProperties GetQuestPropertiesInterface();
//      void ShowView();

        void InitSelQuestPage();
        int NavigatorPosition { get; }
        void SetNavigatorPos_QuestPage(int iPos);
    }

    partial class MainForm : IVoteQuestPage
    {
        SelQuestionsCntrler _selquestController;

        #region реализация интерфейса IVoteQuestPage
        IMsgMethods IVoteQuestPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        IQuestProperties IVoteQuestPage.GetQuestPropertiesInterface()
        {
            return (IQuestProperties)this;
        }


        void IVoteQuestPage.InitSelQuestPage()
        {
            grpSelQuestPropsIn.Controls.Add(PropertiesControlQuest);
            //          cat_sq_Criteries.Expanded = true;
            cat_sq_Notes.Expanded = true;

            PropertiesControlQuest.Dock = DockStyle.Fill;

            IQuestProperties qp = GetQuestPropertiesInterface();
            qp.SetViewMode();

            _selquestController.UpdatePropData(navigatorQuestions.Position);
        }

        int IVoteQuestPage.NavigatorPosition
        {
            get
            {
                return navigatorQuestions.Position;
            }
        }

        void IVoteQuestPage.SetNavigatorPos_QuestPage(int iPos)
        {
            navigatorQuestions.Position = iPos;
        }

        IQuestProperties GetQuestPropertiesInterface()
        {
            return (IQuestProperties)this;
        }

        #endregion

        private void navigatorQuestions_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Append")
                {
                    int i = _selquestController.AppendItem();
                    if (i >= 0)
                        navigatorQuestions.Position = i;

                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    DialogResult dr = _messages.ShowQuestion("Вы уверены, что хотите удалить выделенную запись?");
                    if (dr == DialogResult.Yes)
                    {
                        _selquestController.RemoveItem();
                        _selquestController.UpdatePropData(navigatorQuestions.Position);
                    }

                }
                else if (e.Button.Tag.ToString() == "Edit")
                {
                    StartEditQuestion();
                }
            }
        }

        private void StartEditQuestion()
        {
            int Pos = navigatorQuestions.Position;
            _selquestController.EditRecord(Pos);
        }

        private void navigatorQuestions_PositionChanged(object sender, EventArgs e)
        {
            ClearMonitor();

            DataNavigator dn = (DataNavigator)sender;
            if (dn == null)
                return;

            _selquestController.UpdatePropData(dn.Position);
        }

        private void btnQuestVoteMode_Click(object sender, EventArgs e)
        {
            EnableMonitor(false);

            _Controller.VoteModeRun();
        }


   }
}