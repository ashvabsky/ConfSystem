﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class TitleForm : Form, IDisposable
    {
        Controller _controller = new Controller();
        public TitleForm()
        {
            InitializeComponent();
            _controller.AssignMainForm(this);
        }

        public void Dispose()
        {
            _controller.Dispose();
        }

        private void TitleForm_Load(object sender, EventArgs e)
        {
            _controller.Init();
        }

        private void TitleForm_DoubleClick(object sender, EventArgs e)
        {
            if (this.FormBorderStyle == FormBorderStyle.None)
            {
                this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            }
            else
            {
                this.FormBorderStyle = FormBorderStyle.None;
            }

        }

        private void tmrCheckCmd_Tick(object sender, EventArgs e)
        {
            tmrCheckCmd.Stop();
            _controller.OnCheckNetCommand();
            tmrCheckCmd.Start();
        }

        private void tmrTimer_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToString("T");
            lblDate.Text = DateTime.Now.ToString("D");
        }

        public void SetInfo(Platform.SessionInfo info)
        {
            if (info.Caption != "")
            {
//                lblSessionCodeText.Text = info.Caption + " сессия";
//               lblSessionCode.Visible = true;
//                lblSessionCodeText.Visible = true;
            }
            else
            {
//                lblSessionCode.Visible = false;
//                lblSessionCodeText.Visible = false;
            }
        }
    }
}
