﻿namespace Screen
{
    partial class VoteResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VoteResultForm));
            this.label2 = new System.Windows.Forms.Label();
            this.lblVote_B = new System.Windows.Forms.Label();
            this.lblVote_C = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblVote_A = new System.Windows.Forms.Label();
            this.lblTimeLeftLabel = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDelegatenonvoted = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Cornsilk;
            this.label2.Location = new System.Drawing.Point(84, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 34);
            this.label2.TabIndex = 4;
            this.label2.Text = "Против:";
            // 
            // lblVote_B
            // 
            this.lblVote_B.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblVote_B.AutoSize = true;
            this.lblVote_B.BackColor = System.Drawing.Color.Transparent;
            this.lblVote_B.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVote_B.ForeColor = System.Drawing.Color.Gold;
            this.lblVote_B.Location = new System.Drawing.Point(339, 194);
            this.lblVote_B.Name = "lblVote_B";
            this.lblVote_B.Size = new System.Drawing.Size(51, 34);
            this.lblVote_B.TabIndex = 10;
            this.lblVote_B.Text = "50";
            // 
            // lblVote_C
            // 
            this.lblVote_C.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblVote_C.AutoSize = true;
            this.lblVote_C.BackColor = System.Drawing.Color.Transparent;
            this.lblVote_C.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVote_C.ForeColor = System.Drawing.Color.Gold;
            this.lblVote_C.Location = new System.Drawing.Point(339, 143);
            this.lblVote_C.Name = "lblVote_C";
            this.lblVote_C.Size = new System.Drawing.Size(51, 34);
            this.lblVote_C.TabIndex = 16;
            this.lblVote_C.Text = "50";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Cornsilk;
            this.label6.Location = new System.Drawing.Point(84, 245);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(239, 34);
            this.label6.TabIndex = 15;
            this.label6.Text = "Не голосовали:";
            // 
            // lblVote_A
            // 
            this.lblVote_A.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblVote_A.AutoSize = true;
            this.lblVote_A.BackColor = System.Drawing.Color.Transparent;
            this.lblVote_A.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVote_A.ForeColor = System.Drawing.Color.Gold;
            this.lblVote_A.Location = new System.Drawing.Point(339, 92);
            this.lblVote_A.Name = "lblVote_A";
            this.lblVote_A.Size = new System.Drawing.Size(51, 34);
            this.lblVote_A.TabIndex = 18;
            this.lblVote_A.Text = "50";
            // 
            // lblTimeLeftLabel
            // 
            this.lblTimeLeftLabel.AutoSize = true;
            this.lblTimeLeftLabel.BackColor = System.Drawing.Color.Transparent;
            this.lblTimeLeftLabel.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTimeLeftLabel.ForeColor = System.Drawing.Color.Cornsilk;
            this.lblTimeLeftLabel.Location = new System.Drawing.Point(84, 92);
            this.lblTimeLeftLabel.Name = "lblTimeLeftLabel";
            this.lblTimeLeftLabel.Size = new System.Drawing.Size(60, 34);
            this.lblTimeLeftLabel.TabIndex = 17;
            this.lblTimeLeftLabel.Text = "За:";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.BackColor = System.Drawing.Color.Transparent;
            this.lblResult.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblResult.ForeColor = System.Drawing.Color.Yellow;
            this.lblResult.Location = new System.Drawing.Point(63, 293);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(365, 39);
            this.lblResult.TabIndex = 21;
            this.lblResult.Text = "Решение не принято";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Cornsilk;
            this.label3.Location = new System.Drawing.Point(84, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(237, 34);
            this.label3.TabIndex = 22;
            this.label3.Text = "Воздержалось:";
            // 
            // lblDelegatenonvoted
            // 
            this.lblDelegatenonvoted.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblDelegatenonvoted.AutoSize = true;
            this.lblDelegatenonvoted.BackColor = System.Drawing.Color.Transparent;
            this.lblDelegatenonvoted.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDelegatenonvoted.ForeColor = System.Drawing.Color.Gold;
            this.lblDelegatenonvoted.Location = new System.Drawing.Point(339, 245);
            this.lblDelegatenonvoted.Name = "lblDelegatenonvoted";
            this.lblDelegatenonvoted.Size = new System.Drawing.Size(51, 34);
            this.lblDelegatenonvoted.TabIndex = 23;
            this.lblDelegatenonvoted.Text = "50";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(70, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(339, 39);
            this.label1.TabIndex = 24;
            this.label1.Text = "Итоги голосования";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // VoteResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(480, 390);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblDelegatenonvoted);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTimeLeftLabel);
            this.Controls.Add(this.lblVote_A);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblVote_C);
            this.Controls.Add(this.lblVote_B);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "VoteResultForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.VoteResultForm_Load);
            this.Controls.SetChildIndex(this.lblVote_B, 0);
            this.Controls.SetChildIndex(this.lblVote_C, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.lblVote_A, 0);
            this.Controls.SetChildIndex(this.lblTimeLeftLabel, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.lblDelegatenonvoted, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblVote_B;
        private System.Windows.Forms.Label lblVote_C;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblVote_A;
        private System.Windows.Forms.Label lblTimeLeftLabel;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDelegatenonvoted;
        private System.Windows.Forms.Label label1;

    }
}

