﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class QuestForm : BaseScreenForm
    {
        public QuestForm(Controller c)
            : base(c)
        {
            InitializeComponent();
            //tmrClose.Start();
        }


        public override void UpdateData(Object o)
        {
            Platform.QuestionInfo info = new Platform.QuestionInfo();
            if (o is Platform.QuestionInfo)
                info = (Platform.QuestionInfo)o;

            lblCaption.Text = info.Caption;
            lblDescription.Text = info.Description;
            lblVoteType.Text = info.Type;
        }

        private void QuestForm_Load(object sender, EventArgs e)
        {
            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;

            lblCaption.Text = "";
            lblDescription.Text = "";
            lblVoteType.Text = "";
        }

        private void lblDescription_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
