﻿namespace Screen
{
    partial class DelegateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DelegateForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFIO = new System.Windows.Forms.RichTextBox();
            this.lblRegion = new System.Windows.Forms.Label();
            this.lblParty = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblFraction = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblSeat = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Cornsilk;
            this.label1.Location = new System.Drawing.Point(190, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "Депутат:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Cornsilk;
            this.label2.Location = new System.Drawing.Point(60, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Регион:";
            // 
            // lblFIO
            // 
            this.lblFIO.BackColor = System.Drawing.Color.Navy;
            this.lblFIO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblFIO.DetectUrls = false;
            this.lblFIO.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFIO.ForeColor = System.Drawing.Color.Gold;
            this.lblFIO.Location = new System.Drawing.Point(49, 96);
            this.lblFIO.Name = "lblFIO";
            this.lblFIO.ReadOnly = true;
            this.lblFIO.Size = new System.Drawing.Size(390, 30);
            this.lblFIO.TabIndex = 8;
            this.lblFIO.Text = "Депутат";
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.BackColor = System.Drawing.Color.Transparent;
            this.lblRegion.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRegion.ForeColor = System.Drawing.Color.Gold;
            this.lblRegion.Location = new System.Drawing.Point(172, 141);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(175, 22);
            this.lblRegion.TabIndex = 10;
            this.lblRegion.Text = "Регион-название";
            // 
            // lblParty
            // 
            this.lblParty.AutoSize = true;
            this.lblParty.BackColor = System.Drawing.Color.Transparent;
            this.lblParty.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblParty.ForeColor = System.Drawing.Color.Gold;
            this.lblParty.Location = new System.Drawing.Point(172, 184);
            this.lblParty.Name = "lblParty";
            this.lblParty.Size = new System.Drawing.Size(197, 22);
            this.lblParty.TabIndex = 16;
            this.lblParty.Text = "Партяи - название";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Cornsilk;
            this.label6.Location = new System.Drawing.Point(60, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 23);
            this.label6.TabIndex = 15;
            this.label6.Text = "Партия:";
            // 
            // lblFraction
            // 
            this.lblFraction.AutoSize = true;
            this.lblFraction.BackColor = System.Drawing.Color.Transparent;
            this.lblFraction.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFraction.ForeColor = System.Drawing.Color.Gold;
            this.lblFraction.Location = new System.Drawing.Point(172, 239);
            this.lblFraction.Name = "lblFraction";
            this.lblFraction.Size = new System.Drawing.Size(208, 22);
            this.lblFraction.TabIndex = 18;
            this.lblFraction.Text = "Фракция - название";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.Cornsilk;
            this.label7.Location = new System.Drawing.Point(60, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 23);
            this.label7.TabIndex = 17;
            this.label7.Text = "Фракция:";
            // 
            // lblSeat
            // 
            this.lblSeat.AutoSize = true;
            this.lblSeat.BackColor = System.Drawing.Color.Transparent;
            this.lblSeat.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSeat.ForeColor = System.Drawing.Color.Gold;
            this.lblSeat.Location = new System.Drawing.Point(172, 292);
            this.lblSeat.Name = "lblSeat";
            this.lblSeat.Size = new System.Drawing.Size(318, 22);
            this.lblSeat.TabIndex = 20;
            this.lblSeat.Text = "Ряд, место и номер микрофона";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.Cornsilk;
            this.label9.Location = new System.Drawing.Point(60, 289);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 23);
            this.label9.TabIndex = 19;
            this.label9.Text = "Место:";
            // 
            // DelegateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(480, 390);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblSeat);
            this.Controls.Add(this.lblFIO);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblFraction);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblParty);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblRegion);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "DelegateForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.DelegateForm_Load);
            this.Controls.SetChildIndex(this.lblRegion, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.lblParty, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.lblFraction, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.lblFIO, 0);
            this.Controls.SetChildIndex(this.lblSeat, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox lblFIO;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.Label lblParty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblFraction;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblSeat;
        private System.Windows.Forms.Label label9;

    }
}

