﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class BaseScreenForm : Form
    {
        Controller _controller;
        public BaseScreenForm()
        {
            InitializeComponent();

            lblTime.Text = DateTime.Now.ToString("T");
            lblDate.Text = DateTime.Now.ToString("D");//("dd.MM.yy");

        }
        public BaseScreenForm(Controller c)
        {
            _controller = c;

            InitializeComponent();

            lblTime.Text = DateTime.Now.ToString("T");
            lblDate.Text = DateTime.Now.ToString("D");//("dd.MM.yy");

        }

        private void tmrTimer_Tick(object sender, EventArgs e)
        {
            lblTime.Text =  DateTime.Now.ToString("T");
            lblDate.Text = DateTime.Now.ToString("D");//("dd.MM.yy");
        }

        public virtual void UpdateData(Object info)
        {
//          tmrClose.Stop();
//          tmrClose.Start();
        }

        private void tmrClose_Tick(object sender, EventArgs e)
        {
             tmrClose.Stop();
            _controller.ShowMainScreen(null);
        }
    }
}
