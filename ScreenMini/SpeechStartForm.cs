﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class SpeechStartForm : BaseScreenForm
    {
        public SpeechStartForm(Controller c)
            : base(c)
        {
            InitializeComponent();
            tmrClose.Enabled = false;
        }



        public override void UpdateData(Object o)
        {
            Platform.SpeechInfoStart info = new Platform.SpeechInfoStart();
            if (info is Platform.SpeechInfoStart)
                info = (Platform.SpeechInfoStart)o;

            lblSpeechTime.Text = info.SpeechTime.ToString() + " мин.";
            if (info.leftMinutes > 0)
            {
                lblWarning.Visible = false;
                lblTimeLeft.Text = info.leftMinutes.ToString() + " мин.";
                lblTimeLeft.ForeColor = Color.Gold;
            }
            else
            {
                lblTimeLeft.Text = "0" ;
                lblWarning.Text = "Время истекло";
                lblWarning.Visible = true;
                lblTimeLeft.ForeColor = Color.Red;
            }
        }

        private void SpeechStartForm_Load(object sender, EventArgs e)
        {
            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;

            lblSpeechTime.Text = "";
            lblTimeLeft.Text = "";
        }

        private void lblIsQuorum_Click(object sender, EventArgs e)
        {

        }

        private void lblTimeLeftLabel_Click(object sender, EventArgs e)
        {

        }


    }
}
