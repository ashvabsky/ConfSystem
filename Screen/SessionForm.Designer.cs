﻿namespace Screen
{
    partial class SessionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.RichTextBox();
            this.lblDelegateQnty = new System.Windows.Forms.Label();
            this.lblQuorumQnty = new System.Windows.Forms.Label();
            this.lblCaption = new System.Windows.Forms.RichTextBox();
            this.lblSessionCode = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Cornsilk;
            this.label2.Location = new System.Drawing.Point(104, 325);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Описание:";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Cornsilk;
            this.label3.Location = new System.Drawing.Point(232, 606);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Депутатов в зале:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Cornsilk;
            this.label4.Location = new System.Drawing.Point(583, 606);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Кворум:";
            // 
            // lblDescription
            // 
            this.lblDescription.BackColor = System.Drawing.Color.Navy;
            this.lblDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblDescription.DetectUrls = false;
            this.lblDescription.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDescription.ForeColor = System.Drawing.Color.Gold;
            this.lblDescription.Location = new System.Drawing.Point(204, 365);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.ReadOnly = true;
            this.lblDescription.Size = new System.Drawing.Size(622, 96);
            this.lblDescription.TabIndex = 9;
            this.lblDescription.Text = "Текст для описания";
            // 
            // lblDelegateQnty
            // 
            this.lblDelegateQnty.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblDelegateQnty.AutoSize = true;
            this.lblDelegateQnty.BackColor = System.Drawing.Color.Transparent;
            this.lblDelegateQnty.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDelegateQnty.ForeColor = System.Drawing.Color.Gold;
            this.lblDelegateQnty.Location = new System.Drawing.Point(446, 610);
            this.lblDelegateQnty.Name = "lblDelegateQnty";
            this.lblDelegateQnty.Size = new System.Drawing.Size(36, 23);
            this.lblDelegateQnty.TabIndex = 10;
            this.lblDelegateQnty.Text = "53";
            // 
            // lblQuorumQnty
            // 
            this.lblQuorumQnty.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblQuorumQnty.AutoSize = true;
            this.lblQuorumQnty.BackColor = System.Drawing.Color.Transparent;
            this.lblQuorumQnty.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQuorumQnty.ForeColor = System.Drawing.Color.Gold;
            this.lblQuorumQnty.Location = new System.Drawing.Point(694, 611);
            this.lblQuorumQnty.Name = "lblQuorumQnty";
            this.lblQuorumQnty.Size = new System.Drawing.Size(36, 23);
            this.lblQuorumQnty.TabIndex = 11;
            this.lblQuorumQnty.Text = "53";
            // 
            // lblCaption
            // 
            this.lblCaption.BackColor = System.Drawing.Color.Navy;
            this.lblCaption.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblCaption.DetectUrls = false;
            this.lblCaption.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCaption.ForeColor = System.Drawing.Color.Gold;
            this.lblCaption.Location = new System.Drawing.Point(204, 170);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.ReadOnly = true;
            this.lblCaption.Size = new System.Drawing.Size(702, 125);
            this.lblCaption.TabIndex = 21;
            this.lblCaption.Text = "символьный код";
            // 
            // lblSessionCode
            // 
            this.lblSessionCode.AutoSize = true;
            this.lblSessionCode.BackColor = System.Drawing.Color.Transparent;
            this.lblSessionCode.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSessionCode.ForeColor = System.Drawing.Color.Cornsilk;
            this.lblSessionCode.Location = new System.Drawing.Point(103, 170);
            this.lblSessionCode.Name = "lblSessionCode";
            this.lblSessionCode.Size = new System.Drawing.Size(95, 25);
            this.lblSessionCode.TabIndex = 20;
            this.lblSessionCode.Text = "Сессия:";
            // 
            // SessionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblCaption);
            this.Controls.Add(this.lblSessionCode);
            this.Controls.Add(this.lblQuorumQnty);
            this.Controls.Add(this.lblDelegateQnty);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SessionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.SessionForm_Load);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.lblDescription, 0);
            this.Controls.SetChildIndex(this.lblDelegateQnty, 0);
            this.Controls.SetChildIndex(this.lblQuorumQnty, 0);
            this.Controls.SetChildIndex(this.lblSessionCode, 0);
            this.Controls.SetChildIndex(this.lblCaption, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox lblDescription;
        private System.Windows.Forms.Label lblDelegateQnty;
        private System.Windows.Forms.Label lblQuorumQnty;
        private System.Windows.Forms.RichTextBox lblCaption;
        private System.Windows.Forms.Label lblSessionCode;

    }
}

