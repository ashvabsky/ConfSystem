﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class SessionForm : BaseScreenForm
    {
        public SessionForm(Controller c)
            : base(c)
        {
            InitializeComponent();
            //tmrClose.Start();
        }


        public override void UpdateData(Object o)
        {
            Platform.SessionInfo info = new Platform.SessionInfo();
            if (o is Platform.SessionInfo)
                info = (Platform.SessionInfo)o;

            lblCaption.Text = info.Caption;
            lblDescription.Text = info.Description;
            lblDelegateQnty.Text = info.DelegateQnty.ToString();
            lblQuorumQnty.Text = info.QuorumQnty.ToString();
            Update();
        }

        private void SessionForm_Load(object sender, EventArgs e)
        {
            lblCaption.Text = "";
            lblDescription.Text = "";
            lblDelegateQnty.Text = "";
            lblQuorumQnty.Text = "";

            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;
        }

    }
}
