﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class VoteStartForm : BaseScreenForm
    {
        public VoteStartForm(Controller c)
            : base(c)
        {
            InitializeComponent();
            tmrClose.Enabled = false;
        }



        public override void UpdateData(Object o)
        {
            if (o == null)
                return;

            Platform.VoteInfoStart info = new Platform.VoteInfoStart();
            if (info is Platform.VoteInfoStart)
                info = (Platform.VoteInfoStart)o;

            lblQntyDelegates.Text = info.DelegateQnty.ToString();
            lblQntyVoted.Text = info.VotedQnty.ToString();

            if (info.Progress >= 0)
            {
                lblTimeLeft.Text = info.leftTime;

                progressBarControl1.Position = info.Progress;
            }
            else
            {
                lblTimeLeft.Text = "00:00";
                progressBarControl1.Position = 100;
                lblCaption.Text = "Подсчет результатов";
            }

            base.UpdateData(o);
        }

        private void VoteStartForm_Load(object sender, EventArgs e)
        {
            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;

            lblQntyDelegates.Text = "";
            lblQntyVoted.Text = "";
            lblTimeLeft.Text = "";
        }

        private void lblIsQuorum_Click(object sender, EventArgs e)
        {

        }


    }
}
