﻿namespace Screen
{
    partial class VotePrepareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VotePrepareForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCaption = new System.Windows.Forms.RichTextBox();
            this.lblDescription = new System.Windows.Forms.RichTextBox();
            this.lblVoteType = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblVoteTime = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Cornsilk;
            this.label1.Location = new System.Drawing.Point(92, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Вопрос:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Cornsilk;
            this.label2.Location = new System.Drawing.Point(92, 385);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Описание:";
            // 
            // lblCaption
            // 
            this.lblCaption.BackColor = System.Drawing.Color.Navy;
            this.lblCaption.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblCaption.DetectUrls = false;
            this.lblCaption.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCaption.ForeColor = System.Drawing.Color.Gold;
            this.lblCaption.Location = new System.Drawing.Point(121, 261);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.ReadOnly = true;
            this.lblCaption.Size = new System.Drawing.Size(800, 96);
            this.lblCaption.TabIndex = 8;
            this.lblCaption.Text = "Текст вопроса";
            // 
            // lblDescription
            // 
            this.lblDescription.BackColor = System.Drawing.Color.Navy;
            this.lblDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblDescription.DetectUrls = false;
            this.lblDescription.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDescription.ForeColor = System.Drawing.Color.Gold;
            this.lblDescription.Location = new System.Drawing.Point(121, 423);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.ReadOnly = true;
            this.lblDescription.Size = new System.Drawing.Size(800, 96);
            this.lblDescription.TabIndex = 9;
            this.lblDescription.Text = "Текст для описания";
            this.lblDescription.TextChanged += new System.EventHandler(this.lblDescription_TextChanged);
            // 
            // lblVoteType
            // 
            this.lblVoteType.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblVoteType.AutoSize = true;
            this.lblVoteType.BackColor = System.Drawing.Color.Transparent;
            this.lblVoteType.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteType.ForeColor = System.Drawing.Color.Gold;
            this.lblVoteType.Location = new System.Drawing.Point(351, 558);
            this.lblVoteType.Name = "lblVoteType";
            this.lblVoteType.Size = new System.Drawing.Size(76, 22);
            this.lblVoteType.TabIndex = 16;
            this.lblVoteType.Text = "Кол-во";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Cornsilk;
            this.label5.Location = new System.Drawing.Point(92, 557);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 23);
            this.label5.TabIndex = 15;
            this.label5.Text = "Тип вопроса:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(239, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(544, 29);
            this.label3.TabIndex = 22;
            this.label3.Text = "Внимание, приготовьтесь к голосованию!";
            // 
            // lblVoteTime
            // 
            this.lblVoteTime.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblVoteTime.AutoSize = true;
            this.lblVoteTime.BackColor = System.Drawing.Color.Transparent;
            this.lblVoteTime.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteTime.ForeColor = System.Drawing.Color.Gold;
            this.lblVoteTime.Location = new System.Drawing.Point(351, 629);
            this.lblVoteTime.Name = "lblVoteTime";
            this.lblVoteTime.Size = new System.Drawing.Size(65, 22);
            this.lblVoteTime.TabIndex = 24;
            this.lblVoteTime.Text = "Время";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Cornsilk;
            this.label6.Location = new System.Drawing.Point(92, 629);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(239, 23);
            this.label6.TabIndex = 23;
            this.label6.Text = "Время на голосование:";
            // 
            // QuestPrepareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblVoteTime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblVoteType);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblCaption);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "QuestPrepareForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.QuestPrepareForm_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblCaption, 0);
            this.Controls.SetChildIndex(this.lblDescription, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.lblVoteType, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.lblVoteTime, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox lblCaption;
        private System.Windows.Forms.RichTextBox lblDescription;
        private System.Windows.Forms.Label lblVoteType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblVoteTime;
        private System.Windows.Forms.Label label6;

    }
}

