﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class VotePrepareForm : BaseScreenForm
    {
        public VotePrepareForm()
        {
            InitializeComponent();
        }

        public VotePrepareForm(Platform.QuestionInfo info)
        {
            InitializeComponent();

            lblCaption.Text = info.Caption;
            lblDescription.Text = info.Description;
            lblVoteType.Text = info.Type;
            lblVoteTime.Text = info.VoteTime.ToString() + " мин.";
        }

        private void QuestPrepareForm_Load(object sender, EventArgs e)
        {
            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;
        }

        private void lblDescription_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
