﻿namespace Screen
{
    partial class RegStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.lblQntyReg = new System.Windows.Forms.Label();
            this.lblQntyDelegates = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTimeLeft = new System.Windows.Forms.Label();
            this.lblCaption = new System.Windows.Forms.Label();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Cornsilk;
            this.label2.Location = new System.Drawing.Point(124, 426);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(655, 68);
            this.label2.TabIndex = 4;
            this.label2.Text = "Зарегистрировалось:";
            // 
            // lblQntyReg
            // 
            this.lblQntyReg.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblQntyReg.AutoSize = true;
            this.lblQntyReg.BackColor = System.Drawing.Color.Transparent;
            this.lblQntyReg.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQntyReg.ForeColor = System.Drawing.Color.Gold;
            this.lblQntyReg.Location = new System.Drawing.Point(802, 426);
            this.lblQntyReg.Name = "lblQntyReg";
            this.lblQntyReg.Size = new System.Drawing.Size(101, 68);
            this.lblQntyReg.TabIndex = 10;
            this.lblQntyReg.Text = "50";
            // 
            // lblQntyDelegates
            // 
            this.lblQntyDelegates.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblQntyDelegates.AutoSize = true;
            this.lblQntyDelegates.BackColor = System.Drawing.Color.Transparent;
            this.lblQntyDelegates.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQntyDelegates.ForeColor = System.Drawing.Color.Gold;
            this.lblQntyDelegates.Location = new System.Drawing.Point(802, 534);
            this.lblQntyDelegates.Name = "lblQntyDelegates";
            this.lblQntyDelegates.Size = new System.Drawing.Size(101, 68);
            this.lblQntyDelegates.TabIndex = 16;
            this.lblQntyDelegates.Text = "50";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Cornsilk;
            this.label6.Location = new System.Drawing.Point(124, 534);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(522, 68);
            this.label6.TabIndex = 15;
            this.label6.Text = "Карт на пультах:";
            // 
            // lblTimeLeft
            // 
            this.lblTimeLeft.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblTimeLeft.AutoSize = true;
            this.lblTimeLeft.BackColor = System.Drawing.Color.Transparent;
            this.lblTimeLeft.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTimeLeft.ForeColor = System.Drawing.Color.Gold;
            this.lblTimeLeft.Location = new System.Drawing.Point(740, 318);
            this.lblTimeLeft.Name = "lblTimeLeft";
            this.lblTimeLeft.Size = new System.Drawing.Size(225, 68);
            this.lblTimeLeft.TabIndex = 18;
            this.lblTimeLeft.Text = "50 : 50";
            this.lblTimeLeft.Click += new System.EventHandler(this.lblTimeLeft_Click);
            // 
            // lblCaption
            // 
            this.lblCaption.AutoSize = true;
            this.lblCaption.BackColor = System.Drawing.Color.Transparent;
            this.lblCaption.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCaption.ForeColor = System.Drawing.Color.White;
            this.lblCaption.Location = new System.Drawing.Point(195, 164);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(634, 77);
            this.lblCaption.TabIndex = 21;
            this.lblCaption.Text = "Идет регистрация";
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(136, 334);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.LookAndFeel.SkinName = "Money Twins";
            this.progressBarControl1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progressBarControl1.Size = new System.Drawing.Size(598, 40);
            this.progressBarControl1.TabIndex = 23;
            // 
            // RegStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.progressBarControl1);
            this.Controls.Add(this.lblCaption);
            this.Controls.Add(this.lblTimeLeft);
            this.Controls.Add(this.lblQntyDelegates);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblQntyReg);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RegStartForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.RegStartForm_Load);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblQntyReg, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.lblQntyDelegates, 0);
            this.Controls.SetChildIndex(this.lblTimeLeft, 0);
            this.Controls.SetChildIndex(this.lblCaption, 0);
            this.Controls.SetChildIndex(this.progressBarControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblQntyReg;
        private System.Windows.Forms.Label lblQntyDelegates;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTimeLeft;
        private System.Windows.Forms.Label lblCaption;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;

    }
}

