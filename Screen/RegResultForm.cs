﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class RegResultForm : BaseScreenForm
    {
        public RegResultForm(Controller c)
            : base(c)
        {
            InitializeComponent();
            //tmrClose.Start();
        }


        public override void UpdateData(Object o)
        {
            Platform.RegInfoFinish info = new Platform.RegInfoFinish();
            if (o is Platform.RegInfoFinish)
                info = (Platform.RegInfoFinish)o;

            if (info.IsQuorum == true)
            {
                lblIsQuorum.Text = "Кворум имеется";
                lblIsQuorum.ForeColor = Color.Yellow;
            }
            else
            {
                lblIsQuorum.Text = "Кворума нет";
                lblIsQuorum.ForeColor = Color.Red;
            }

            lblQntyReg.Text = info.RegQnty.ToString();
            lblQntyDelegates.Text = info.DelegateQnty.ToString();
        }

        private void RegResultForm_Load(object sender, EventArgs e)
        {
            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;

            lblIsQuorum.Text = "";
            lblQntyReg.Text = "";
            lblQntyDelegates.Text = "";

        }
    }
}
