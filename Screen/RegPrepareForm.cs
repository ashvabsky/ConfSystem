﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class RegPrepareForm : BaseScreenForm
    {
        public RegPrepareForm()
        {
            InitializeComponent();
            tmrClose.Enabled = false;
        }


        public RegPrepareForm(Controller c)
            : base(c)
        {
            InitializeComponent();
        }

        public override void UpdateData(Object info)
        {
        }

        private void RegPrepareForm_Load(object sender, EventArgs e)
        {
            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;
        }


    }
}
