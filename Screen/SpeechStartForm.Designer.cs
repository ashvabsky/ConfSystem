﻿namespace Screen
{
    partial class SpeechStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.lblSpeechTime = new System.Windows.Forms.Label();
            this.lblTimeLeft = new System.Windows.Forms.Label();
            this.lblTimeLeftLabel = new System.Windows.Forms.Label();
            this.lblCaption = new System.Windows.Forms.Label();
            this.lblWarning = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Cornsilk;
            this.label2.Location = new System.Drawing.Point(44, 370);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(717, 68);
            this.label2.TabIndex = 4;
            this.label2.Text = "Время на выступление:";
            // 
            // lblSpeechTime
            // 
            this.lblSpeechTime.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblSpeechTime.AutoSize = true;
            this.lblSpeechTime.BackColor = System.Drawing.Color.Transparent;
            this.lblSpeechTime.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSpeechTime.ForeColor = System.Drawing.Color.Gold;
            this.lblSpeechTime.Location = new System.Drawing.Point(783, 370);
            this.lblSpeechTime.Name = "lblSpeechTime";
            this.lblSpeechTime.Size = new System.Drawing.Size(225, 68);
            this.lblSpeechTime.TabIndex = 16;
            this.lblSpeechTime.Text = "50 : 50";
            // 
            // lblTimeLeft
            // 
            this.lblTimeLeft.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblTimeLeft.AutoSize = true;
            this.lblTimeLeft.BackColor = System.Drawing.Color.Transparent;
            this.lblTimeLeft.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTimeLeft.ForeColor = System.Drawing.Color.Gold;
            this.lblTimeLeft.Location = new System.Drawing.Point(783, 466);
            this.lblTimeLeft.Name = "lblTimeLeft";
            this.lblTimeLeft.Size = new System.Drawing.Size(225, 68);
            this.lblTimeLeft.TabIndex = 18;
            this.lblTimeLeft.Text = "50 : 50";
            // 
            // lblTimeLeftLabel
            // 
            this.lblTimeLeftLabel.AutoSize = true;
            this.lblTimeLeftLabel.BackColor = System.Drawing.Color.Transparent;
            this.lblTimeLeftLabel.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTimeLeftLabel.ForeColor = System.Drawing.Color.Cornsilk;
            this.lblTimeLeftLabel.Location = new System.Drawing.Point(176, 466);
            this.lblTimeLeftLabel.Name = "lblTimeLeftLabel";
            this.lblTimeLeftLabel.Size = new System.Drawing.Size(585, 68);
            this.lblTimeLeftLabel.TabIndex = 17;
            this.lblTimeLeftLabel.Text = "Осталось времени:";
            // 
            // lblCaption
            // 
            this.lblCaption.AutoSize = true;
            this.lblCaption.BackColor = System.Drawing.Color.Transparent;
            this.lblCaption.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCaption.ForeColor = System.Drawing.Color.White;
            this.lblCaption.Location = new System.Drawing.Point(189, 208);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(654, 77);
            this.lblCaption.TabIndex = 22;
            this.lblCaption.Text = "Идет выступление";
            // 
            // lblWarning
            // 
            this.lblWarning.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblWarning.AutoSize = true;
            this.lblWarning.BackColor = System.Drawing.Color.Transparent;
            this.lblWarning.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWarning.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Location = new System.Drawing.Point(350, 557);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(225, 68);
            this.lblWarning.TabIndex = 23;
            this.lblWarning.Text = "50 : 50";
            this.lblWarning.Visible = false;
            // 
            // SpeechStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.lblCaption);
            this.Controls.Add(this.lblTimeLeft);
            this.Controls.Add(this.lblTimeLeftLabel);
            this.Controls.Add(this.lblSpeechTime);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SpeechStartForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.SpeechStartForm_Load);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblSpeechTime, 0);
            this.Controls.SetChildIndex(this.lblTimeLeftLabel, 0);
            this.Controls.SetChildIndex(this.lblTimeLeft, 0);
            this.Controls.SetChildIndex(this.lblCaption, 0);
            this.Controls.SetChildIndex(this.lblWarning, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblSpeechTime;
        private System.Windows.Forms.Label lblTimeLeft;
        private System.Windows.Forms.Label lblTimeLeftLabel;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.Label lblWarning;

    }
}

