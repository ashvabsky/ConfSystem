﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Platform;
using System.IO;
using System.Runtime.Serialization;

namespace Screen
{
    public class Controller: IDisposable
    {
        TitleForm _mainForm;
/*
        SessionForm _sessionForm; // = new SessionForm();
        DelegateForm _delegateForm;
        RegStartForm _regstartForm;
        RegPrepareForm _regprepareForm;
        RegResultForm _regresultForm;
        QuestForm _questinfoForm;
        VotePrepareForm _voteprepareForm;
        VoteStartForm _votestartForm;
*/
        BaseScreenForm _currentForm;
        string _currentScreenName = "";

        NetHelper _netHelper = new NetHelper();

        bool _IsActive = true; // включает или выключает режим отображения

        public void Init()
        {
            _netHelper.ClearScreenCommands(true);
        }

        public void OnCheckNetCommand()
        {
            NetObject no = new NetObject();
            _netHelper.TryGetNetCommand(ref no, true);
            if (no.ID != 0)
            {
                if (no.enCmdStateMaxi == 0)
                    ProcessNetCmd(no);

/*
                if (bRes == true)
                    _netHelper.AcceptNetCommand(no);
 */ 
            }
        }


        public void ProcessNetCmd(NetObject no)
        {

            if (no == null)
                return;

            byte[] body = (byte[])no.Info;
            Object ResultObj = null;

            if (body != null)
                ResultObj= Deserialize(ref body);

            int enCmdType = no.enCmdType;
            string ScreenType = no.ScreenType;



            if (enCmdType == 1) // отобразить экран
            {
                if (_IsActive == true)
                {
                    UpdateCurrentScreen(ScreenType, ResultObj); //ShowSessionScreen(ResultObj);
                }
            }
            else if (enCmdType == 3) // отобразить заставку
            {
                if (_IsActive == true)
                    ShowMainScreen(ResultObj);
            }
            else if (enCmdType == 4) // выключить вывод
            {
                ShowMainScreen(ResultObj);
                _IsActive = false;
            }
            else if (enCmdType == 5) // включить вывод
            {
                ShowMainScreen(ResultObj);
                _IsActive = true;
            }
            else if (enCmdType == 2) // сообщение от оператора
            {
                if (_IsActive == true)
                {
                    ShowOperatorScreen();
                }
            }

            ResultObj = null;
//          GC.Collect();
        }

        public Object Deserialize(ref byte[] Body)
        {
            if (Body.Length == 0)
                return null;

            Object ResultObj;
            MemoryStream ms = new MemoryStream(Body);
            // Construct a BinaryFormatter and use it to serialize the data to the stream.
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            try
            {
                ResultObj = formatter.Deserialize(ms);
            }
            catch (System.Runtime.Serialization.SerializationException e)
            {
                Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                ms.Close();
            }

            return ResultObj;
        }

        public void AssignMainForm(TitleForm tf)
        {
            _mainForm = tf;
            
        }

        private BaseScreenForm CreateScreenForm(string ScreenType)
        {
            switch (ScreenType)
            {
                case "screen_titul":
                    {
                        return null;
    
                        break;
                    }
                case "screen_session":
                    {
                        return new SessionForm(this);
                        break;
                    }
                case "screen_delegate":
                    {
                        return new DelegateForm(this); //ShowDelegateScreen(ResultObj);
                        break;
                    }
                case "screen_question":
                    {
                        return new QuestForm(this); //ShowQuestionScreen(ResultObj);
                        break;
                    }

                case "screen_regstart":
                    {
                        return new RegStartForm(this); //ShowRegStartScreen(ResultObj);
                        break;
                    }
                case "screen_reginfo":
                    {
                        return new RegResultForm(this); //ShowRegFinishScreen(ResultObj);
                        break;
                    }
                case "screen_regprepare":
                    {
                        return new RegPrepareForm(this); // ShowRegPrepareScreen(ResultObj);
                        break;
                    }
                case "screen_voteprepare":
                    {
                        return new VotePrepareForm(this); //ShowVotePrepareScreen(ResultObj);
                        break;
                    }

                case "screen_votestart":
                    {
                        return new VoteStartForm(this); //ShowVoteStartScreen(ResultObj);
                        break;
                    }
                case "screen_votefinish":
                    {
                        return new VoteResultForm(this);
                        break;
                    }

                case "screen_speechstart":
                    {
                        return new SpeechStartForm(this);
                        break;
                    }
            }

            return null;
        }

        private void UpdateCurrentScreen( string newscreenname, object info)
        {
            if (newscreenname == "")
            {
                if (_currentForm != null )
                {
//                  _mainForm.Visible = true;
                    _currentForm.Close();
                    _currentForm = null;
                    _currentScreenName = "";
                    return;
                }
            }

            if (_currentForm != null)
            {
                if (newscreenname != "")
                {
                    if (newscreenname != _currentScreenName)
                    {
                        //                  _mainForm.Visible = false;
                        BaseScreenForm newcurrent = CreateScreenForm(newscreenname);
                        newcurrent.Show(_mainForm);
                        newcurrent.Refresh();
//                      System.Threading.Thread.Sleep(2000);
                        _currentForm.Close();
                        _currentForm.Dispose();
                        _currentForm = newcurrent;
                        _currentScreenName = newscreenname;
                    }
                }
            }
            else
            {

                BaseScreenForm newcurrent = CreateScreenForm(newscreenname);
                newcurrent.Show(_mainForm);
                _currentForm = newcurrent;
                _currentScreenName = newscreenname;
            }

            if (_currentForm != null)
                _currentForm.UpdateData(info);
        }

        public void ShowMainScreen(object info)
        {
            _netHelper.ClearScreenCommands(false);

            if (info != null)
            {
                SessionInfo si = (SessionInfo)info;
                _mainForm.SetInfo(si);
            }

            _mainForm.Visible = true;

            if (_currentForm != null)
            {
                _currentForm.Close();
                _currentForm = null;
                _currentScreenName = "";
            }

        }


        private void ShowOperatorScreen()
        {
            ShowMainScreen(null);
        }

        public void Dispose()
        {
            _netHelper.Dispose();
        }
    }
}
