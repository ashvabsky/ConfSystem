﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class RegStartForm : BaseScreenForm
    {
        public RegStartForm(Controller c)
            : base(c)
        {
            InitializeComponent();
            tmrClose.Enabled = false;
        }


        public override void UpdateData(Object o)
        {
            Platform.RegInfoStart info = new Platform.RegInfoStart();
            if (o is Platform.RegInfoStart)
                info = (Platform.RegInfoStart)o;

            //          lblFIO.Text = info.CurrDelegate;
/*
            if (info.IsQuorum == true)
            {
                lblIsQuorum.Text = "Кворум имеется";
                lblIsQuorum.ForeColor = Color.Gold;
            }
            else
            {
                lblIsQuorum.Text = "";
            }
*/
            lblQntyReg.Text = info.RegQnty.ToString();
            lblQntyDelegates.Text = info.DelegateQnty.ToString();
            lblTimeLeft.Text = info.leftTime;
            progressBarControl1.Position = info.Progress;
        }

        private void RegStartForm_Load(object sender, EventArgs e)
        {
            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;

//          lblIsQuorum.Text = "";
            lblQntyReg.Text = "";
            lblQntyDelegates.Text = "";
            lblTimeLeft.Text = "";
        }

        private void lblIsQuorum_Click(object sender, EventArgs e)
        {

        }

        private void lblTimeLeft_Click(object sender, EventArgs e)
        {

        }

    }
}
